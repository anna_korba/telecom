\documentclass[8pt]{article}
% PACKAGES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\usepackage{aistats2016}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{epsfig}
\usepackage{geometry}
\usepackage{stmaryrd}
\usepackage[noadjust]{cite}
\usepackage{natbib}
\usepackage{enumerate}

% ENVIRONMENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{assumption}[theorem]{Assumption}
%\newtheorem{remark}[theorem]{Remark}
\newtheorem{rema}{Remark}
%\newenvironment{remark}{\begin{rema} \rm}{\end{rema}}
%\newtheorem{example}[theorem]{Example}
\newtheorem{exam}{Example}
\newenvironment{example}{\begin{exam} \rm}{\end{exam}}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\newenvironment{proof}[1][Proof]{\textbf{#1.} }{\ \rule{0.5em}{0.5em}}


% Notations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\n}{\llbracket n \rrbracket}
\newcommand{\DN}{\mathcal{D}_{N}}
\newcommand{\Sn}{\mathfrak{S}_n}
\def\e{\mathbf e}
\def\i{\mathbf i}
\def\j{\mathbf j}



% If your paper is accepted, change the options for the package
% aistats2016 as follows:
%
%\usepackage[accepted]{aistats2016}
%
% This option will print headings for the title of your paper and
% headings for the authors names, plus a copyright note at the end of
% the first column of the first page.
\appendix
\usepackage{xr}
\externaldocument{328}
\usepackage{bibentry}

\begin{document}
	
	% If your paper is accepted and the title of your paper is very long,
	% the style will print as headings an error message. Use the following
	% command to supply a shorter title of your paper so that it can be
	% used as headings.
	%
	%\runningtitle{I use this title instead because the last one was very long}
	
	% If your paper is accepted and the number of authors is large, the
	% style will print as headings an error message. Use the following
	% command to supply a shorter version of the authors names so that
	% they can be used as headings (for example, use only the surnames)
	%
	%\runningauthor{Surname 1, Surname 2, Surname 3, ...., Surname n}
	
	%\twocolumn[
	
	%\aistatstitle{Supplementary material for \\
	%		A Learning Theory of Ranking Aggregation}
	
	%\aistatsauthor{ Anonymous Author 1 \And Anonymous Author 2 \And Anonymous Author 3 }
	
	%\aistatsaddress{ Unknown Institution 1 \And Unknown Institution 2 \And Unknown Institution 3 } ]



\hrule 
\begin{center}
	\Large{Supplementary material for \\
		A Learning Theory of Ranking Aggregation}
\end{center}
\hrule
%\title{Supplementary material for \\
%			A Learning Theory of Ranking Aggregation}

\section{Optimality}


\subsubsection{Proof of Remark \ref{remark:borda}}

Suppose $P$ satisfies the \textit{strongly stochastically transitive} condition. According to Theorem 5, there exists $\sigma^* \in \Sn$ satisfying (\ref{eq:cond_opt}) and (\ref{eq:cond_opt2}).
We already know that $\sigma^*$ is a Kemeny consensus since it minimizes the loss with respect to the Kendall's $\tau$ distance. Then, Copeland's method order the items by the number of their pairwise victories, which corresponds to sort them according to the mapping $s^*$ and thus $\sigma^*$ is a Copeland consensus. Finally, the Borda score for an item is: $s(i) = \sum_{\sigma \in \Sn}\sigma(i)P(\sigma)$. Firstly observe that for any $\sigma \in \Sn$,
\begin{equation} 
\label{eq:pairwise_sigma}
\sum_{k\ne i}\mathbb{I}\{ \sigma(k) < \sigma(i) \} - \sum_{k\ne i}\mathbb{I}\{ \sigma(k) > \sigma(i) \} = \sigma(i)-1 - (n-\sigma(i))= 2 \sigma(i)-(n+1).
\end{equation}
According to \eqref{eq:pairwise_sigma}, we have the following calculations:
\begin{align*}
s(i)&= \sum_{\sigma \in \Sn} \frac{1}{2}\left(n+1 +  \sum_{k\ne i}\left(2 \mathbb{I}\{ \sigma(k) < \sigma(i) \} -1\right) \right)   P(\sigma)\\
&= \frac{n+1}{2} + \frac{1}{2} \left( \sum_{\sigma \in \Sn} \sum_{k\ne i} 2 \mathbb{I}\{ \sigma(k) < \sigma(i) \} - (n-1) \right) P(\sigma)\\
&= \frac{n+1}{2} - \frac{n-1}{2} + \sum_{k\ne i} \sum_{\sigma \in \Sn} \mathbb{I}\{ \sigma(k) < \sigma(i) \}P(\sigma)\\
&= 1+ \sum_{k\ne i} p_{k,i}.
\end{align*}
Let $i,j$ such that $p_{i,j}>1/2$ ($\Leftrightarrow s^*(i)< s^*(j)$ under \textit{stochastic transitivity}).
\begin{align*}
s(j)-s(i)&= \sum_{k\ne j} p_{k,j} - \sum_{k\ne i} p_{k,i}\\
&= \sum_{k\ne i,j} p_{k,j} - \sum_{k\ne i,j} p_{k,i}  +p_{i,j} - p_{j,i}\\
&= \sum_{k\ne i,j} p_{k,j}- p_{k,i} + (2 p_{i,j}- 1)
\end{align*}
With $(2 p_{i,j}- 1)>0$. Now we focus on the first term, and consider $k \ne i,j$. 
\begin{enumerate}[(i)]
	\item First case: $p_{j,k} \ge 1/2$. The strong stochastic transitivity condition implies that :
	\begin{align*}
	p_{i,k}&\ge \max(p_{i,j}, p_{j,k})\\
	1-p_{k,i}& \ge \max(p_{i,j}, p_{j,k})\\
	p_{k,j}-p_{k,i}&\ge p_{k,j}-1 +\max(p_{i,j}, p_{j,k})\\
	p_{k,j}-p_{k,i}&\ge -p_{j,k}+ \max(p_{i,j}, p_{j,k})\\
	p_{k,j}-p_{k,i}&\ge \max(p_{i,j}-p_{j,k}, 0)\\	
	p_{k,j}-p_{k,i}&\ge 0.\\	
	\end{align*}
	\item Second case: $p_{k,j} > 1/2$. If $p_{k,i} \le 1/2$, $p_{k,j}-p_{k,i} >0$. Now if $p_{k,i}>1/2$, having $p_{i,j}>1/2$, the strong stochastic transitivity condition implies that $p_{k,j}\ge max(p_{k,i}, p_{i,j})$.
\end{enumerate}
Therefore in any case, $\forall k \ne i,j$, $p_{k,j}- p_{k,i}\ge 0$ and $ s(j)-s(i)>0$.


\section{Empirical consensus}

\subsection{Universal rates}


\noindent We can obtain upper bounds using \eqref{eq:classic} and some calculations on $L$ as follows. First notice that same as in \eqref{eq:loss-1} one has for any $\sigma\in\Sn$:
\[
\widehat{L}_{N}(\sigma)=\binom{n}{2}\mathbb{E}\left[ \widehat{p}_{\i,\j}\mathbb{I}\{\sigma(\i)>\sigma(\j) \}  + (1-\widehat{p}_{\i,\j})\mathbb{I}\{\sigma(\i)<\sigma(\j) \}\right]
\]
so that
\begin{align*}
\vert\widehat{L}_{N}(\sigma) - L(\sigma)\vert &= \binom{n}{2}\vert\mathbb{E}\left[(\widehat{p}_{\i,\j} - p_{\i,\j})\mathbb{I}\{\sigma(\i)>\sigma(\j)\} - (\widehat{p}_{\i,\j} - p_{\i,\j})\mathbb{I}\{\sigma(\i)<\sigma(\j) \}\right]\vert\\
&\leq \binom{n}{2}\mathbb{E}_{\i,\j}\left[ \vert p_{\i,\j}-\widehat{p}_{\i,\j} \vert  \right].
\end{align*}\\

\subsubsection{Proof of Proposition \ref{prop:ERM}}

\begin{itemize}
	\item[(i)] By the Cauchy-Schwartz inequality,
\begin{equation*}
\mathbb{E}_{\i,\j}\left[ \vert p_{\i,\j}-\widehat{p}_{\i,\j} \vert  \right] \le \sqrt{\mathbb{E}_{\i,\j}\left[ ( p_{\i,\j}-\widehat{p}_{\i,\j} )^2 \right]}= 
 \sqrt{Var( \widehat{p}_{\i,\j})}.
\end{equation*}
Since $\mathbb{E}_{\i,\j}\left[  p_{\i,\j}-\widehat{p}_{\i,\j} \right]=0$.
Then, for $i<j$, $N \widehat{p}_{i,j} \sim \mathcal{B}(N,p_{i,j}))$ so $Var( \widehat{p}_{i,j})= \frac{ p_{i,j}(1- p_{i,j})}{N} \le \frac{1}{4N}$. Finally, we can upper bound the expectation of the excess of risk as follows:
\begin{equation*}
\mathbb{E}\left[L(\widehat{\sigma}_N)-L^*\right]\leq 2 \mathbb{E}\left[\max_{\sigma \in \Sn}\vert\widehat{L}_{N}(\sigma) - L(\sigma)\vert\right]\le 2 \binom{n}{2} \frac{1}{\sqrt{4N}}=\frac{n(n-1)}{2\sqrt{N}}.
\end{equation*}\\


	\item[(ii)]  By \eqref{eq:classic} one has for any $t > 0$
\begin{equation}
\label{eq:risk1}
\mathbb{P}\Big\{L(\widehat{\sigma}_{N})-L^{\ast} > t\Big\} 
\leq \mathbb{P}\Big\{2\binom{n}{2}\mathbb{E}_{\i,\j}\left[ \vert p_{\i,\j}-\widehat{p}_{\i,\j} \vert  \right] > t\Big\}
= \mathbb{P}\Big\{\sum_{1\leq i < j\leq n}\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2}\Big\},
\end{equation}
and the other hand, it holds that
\begin{equation}
\label{eq:risk2}
\mathbb{P}\Big\{\sum_{1\leq i < j\leq n}\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2}\Big\}\leq \mathbb{P}\Big\{\bigcup_{1\leq i < j\leq n}\Big\{\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2\binom{n}{2}}\Big\}\Big\}
\leq \sum_{1\leq i < j \leq n}\mathbb{P}\Big\{\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2\binom{n}{2}}\Big\}.
\end{equation}
Now, Hoeffding's inequality to $\widehat{p}_{i,j} = (1/N)\sum_{t=1}^{N}\mathbb{I}\{\Sigma_{t}(i) < \Sigma_{t}(j)\}$ gives
\begin{equation}
\label{eq:Hoeffding-pairwise}
\mathbb{P}\Big\{ \vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2\binom{n}{2}} \Big\} \leq 2e^{-2N(t/2\binom{n}{2})^{2}}.
\end{equation}
Therefore, combining \eqref{eq:risk1}, \eqref{eq:risk2} and \eqref{eq:Hoeffding-pairwise} we get 
\begin{equation*}
\mathbb{P}\Big\{L(\widehat{\sigma}_{N})-L^{\ast} > t\Big\} \leq 2\binom{n}{2}e^{-\frac{Nt^{2}}{2\binom{n}{2}^{2}}}.
\end{equation*}
Setting $\delta = 2\binom{n}{2}e^{-\frac{Nt^{2}}{2\binom{n}{2}^{2}}}$ one obtains that with probability greater than $1-\delta$,
\begin{equation*}
\label{eq:Hoeffding-bound}
L(\widehat{\sigma}_{N}) - L^{\ast} \leq  \binom{n}{2}\sqrt{\frac{2\log(n(n-1)/\delta)}{N}}.
\end{equation*}
\end{itemize}

\vspace{0.5cm}
\subsubsection{Proof of Proposition \ref{prop:minimax_bound}}
In the following proof, we follow Le Cam's method, see section 2.3 in \cite{tsybakov2009introduction}.\\

\noindent Consider two Mallows models $P_{\theta_0}$ and $P_{\theta_1}$ where $\theta_k=(\sigma^*_k,\phi)\in \mathfrak{S}_n\times (0,1)$ and $\sigma^*_0\neq \sigma^*_1$. We clearly have:
\begin{align*}
\mathcal{R}_N\geq& \inf_{\sigma_N}\max_{k=0,\; 1} \mathbb{E}_{P_{\theta_k}}\left[L_{P_{\theta_k}}(\sigma_N)-L_{P_{\theta_k}}^*  \right]\\
= & \inf_{\sigma_N}\max_{k=0,\; 1}\sum_{i<j} \mathbb{E}_{P_{\theta_k}}\left[2\vert p_{i,j}-\frac{1}{2} \vert \times  \mathbb{I}\{ (\sigma_N(i)-\sigma_N(j)(\sigma_k^*(i)-\sigma_k^*(j))<0  \} \right]\\
\ge & \inf_{\sigma_N} \frac{\vert \phi-1 \vert}{(1+\phi)}\max_{k=0,\; 1}\sum_{i<j} \mathbb{E}_{P_{\theta_k}}\left[\mathbb{I}\{ (\sigma_N(i)-\sigma_N(j)(\sigma_k^*(i)-\sigma_k^*(j))<0  \} \right]\\
\geq & \frac{\vert \phi-1 \vert}{2} \inf_{\sigma_N}\max_{k=0,\; 1} \mathbb{E}_{P_{\theta_k}}\left[d_{\tau} (\sigma_N,\sigma_k^*)  \right],
\end{align*}
using the fact that $\vert p_{i,j} - \frac{1}{2}\vert \geq \frac{\vert \phi - 1 \vert}{2(1+\phi)}$ (based on Corollary 3 from \cite{BFHS14}, see Remark \ref{Rk:Mallows}). Set $ \Delta=d_{\tau}(\sigma^*_0,\sigma^*_1)\geq 1$, and consider the test statistic related to $\sigma_N$:
\begin{equation*}
\psi(\Sigma_1,\; \ldots,\; \Sigma_N)=\mathbb{I}\{ d_{\tau}(\sigma_N,\sigma^*_1)\leq d_{\tau}(\sigma_N,\sigma^*_0)\ \}.
\end{equation*}
If $\psi=1$, by triangular inequality, we have:
\begin{equation*}
\Delta \leq d_{\tau}(\sigma_N,\sigma^*_0)+d_{\tau}(\sigma_N,\sigma^*_1)\leq 2d_{\tau}(\sigma_N,\sigma^*_0).
\end{equation*}
Hence, we have
\begin{equation*}
\mathbb{E}_{P_{\theta_0}}\left[d_{\tau} (\sigma_N,\sigma_0^*)  \right]\geq \mathbb{E}_{P_{\theta_0}}\left[d_{\tau} (\sigma_N,\sigma_0^*) \mathbb{I}\{ \psi=+1 \} \right]\geq \frac{\Delta}{2}\mathbb{P}_{\theta_0}\{\psi=+1  \}
\end{equation*}
and similarly
\begin{equation*}
\mathbb{E}_{P_{\theta_1}}\left[d_{\tau} (\sigma_N,\sigma_1^*)  \right]\geq \mathbb{E}_{P_{\theta_1}}\left[d_{\tau} (\sigma_N,\sigma_1^*) \mathbb{I}\{ \psi=0 \} \right]
\geq \frac{\Delta}{2}\mathbb{P}_{\theta_1}\{\psi=0  \}.
\end{equation*}
Bounding by below the maximum by the average, we have:
\begin{align*}
\inf_{\sigma_N}\max_{k=0,\; 1} \mathbb{E}_{P_{\theta_k}}\left[d_{\tau} (\sigma_N,\sigma_k^*)  \right]&\geq   
\inf_{\sigma_N}\frac{\Delta}{2} \frac{1}{2}\left\{ \mathbb{P}_{\theta_1}\{\psi=0  \}  + \mathbb{P}_{\theta_0}\{\psi=1  \} \right\} \\
&\geq \frac{\Delta}{4}\min_{k=0,\; 1}\left\{ \mathbb{P}_{\theta_1}\{\psi^*=0  \}  + \mathbb{P}_{\theta_0}\{\psi^*=1  \} \right\},
\end{align*}
where the last inequality follows from a standard Neyman-Pearson argument, denoting by
\begin{equation*}
\psi^*(\Sigma_1,\; \ldots,\; \Sigma_N)=\mathbb{I}\left\{\prod_{i=1}^N\frac{P_{\theta_1}(\Sigma_i)}{P_{\theta_0}(\Sigma_i)}\geq 1\right\}
\end{equation*}
the likelihood ratio test statistic. We deduce that 
\begin{equation*}
\mathcal{R}_N\geq \frac{\Delta\vert \phi_-1 \vert }{8}
\sum_{\sigma_i\in \mathfrak{S}_N,\; 1\leq i\leq N}\min\left\{\prod_{i=1}^NP_{\theta_0}(\sigma_i),\;  \prod_{i=1}^NP_{\theta_1}(\sigma_i) \right\},
\end{equation*}
and with Le Cam's inequality that:
\begin{equation*}
\mathcal{R}_N \geq \frac{\Delta\vert \phi_-1 \vert}{16} e^{-NK(P_{\theta_0}\vert\vert P_{\theta_1})},
\end{equation*}
where $K(P_{\theta_0}\vert\vert P_{\theta_1})=\sum_{\sigma\in \mathfrak{S}_N}P_{\theta_0}(\sigma)\log(P_{\theta_0}(\sigma)/P_{\theta_1}(\sigma))$ denotes the Kullback-Leibler divergence. In order to establish a minimax lower bound of order $1/\sqrt{N}$, one should choose $\theta_0=(\phi_0,\sigma_0)$ and $\theta_1=(\phi_1,\sigma_1)$ so that, for $k\in\{0,\; 1  \}$, $\phi_k\rightarrow 1$ and $K(P_{\theta_0}\vert\vert P_{\theta_1})\rightarrow 0$ as $N\rightarrow +\infty$ at appropriate rates.\\

\noindent We consider the special case where $\phi_0=\phi_1=\phi$, which results in $Z_0=Z_1=Z$ for the normalization constant, and we fix $\sigma_0 \in \Sn$.
 Let $i<j$ such that $\sigma_0(i)+1=\sigma_0(j)$. We consider $\sigma_1 = (i,j) \sigma_0$ the permutation where the adjacent pair $(i,j)$ has been transposed, so that $\sigma_1(i)=\sigma_1(j)+1$ and $\Delta=1$. For any $\sigma \in \Sn$, notice that
 \begin{equation}
 \label{eq:distance_difference}
d_{\tau}(\sigma_0,\sigma)-d_{\tau}(\sigma_1,\sigma)= \mathbb{I}\{ (\sigma(i)>\sigma(j) \}-\mathbb{I}\{ (\sigma(i)<\sigma(j) \}
 \end{equation}
According to \eqref{eq:Mallows}, the Kullback-Leibler divergence is given by
\begin{equation*}
K(P_{\theta_0}\vert\vert P_{\theta_1})
=\sum_{\sigma\in \mathfrak{S}_n}P_{\theta_0}(\sigma)\log \left(\phi^{d_{\tau}(\sigma_0,\sigma)-d_{\tau}(\sigma_1,\sigma)}\right) 
\end{equation*}
And combining it with \eqref{eq:distance_difference} yields
\begin{equation*}
K(P_{\theta_0}\vert\vert P_{\theta_1})=\log(\phi)\sum_{\sigma\in \mathfrak{S}_n}P_{\theta_0}(\sigma)\left( \mathbb{I}\{ (\sigma(i)>\sigma(j) \}-\mathbb{I}\{ (\sigma(i)<\sigma(j) \} \right)
\end{equation*}
By denoting $p_{j,i}^0=P_{\theta_0}\left[\Sigma(i)<\Sigma(j)\right]$, this gives us
\begin{equation}
\label{eq:Kullback_mallows}
K(P_{\theta_0}\vert\vert P_{\theta_1})= \log(\phi) \left(p_{j,i}^0- p_{i,j}^0 \right)
= \log(\frac{1}{\phi}) \left(2p_{i,j}^0- 1 \right)
= \log(\frac{1}{\phi}) \frac{1-\phi}{1+\phi}
\end{equation}
\noindent Where the last equality comes from \cite{BFHS14} (Corollary 3 for adjacent items in the central permutation, see also Remark \ref{Rk:Mallows}).\\


\noindent By taking $\phi=1-1/\sqrt{N}$, we firstly have $\vert\phi-1 \vert=1/\sqrt{N}$ and 
\begin{equation*}
K(P_{\theta_0}\vert\vert P_{\theta_1}) =  -\log(1-1/\sqrt{N}) \frac{1/\sqrt{N}}{2-1/\sqrt{N}}.
\end{equation*}
 Then, since for all $x<1$, $x\ne 0$, $-log(1-x)>x$ and for all $N\ge 1$, $2-\frac{1}{\sqrt{N}}\ge 1$, the Kullback-Leibler divergence can be upper bounded as follows:\\
\begin{equation*}
K(P_{\theta_0}\vert\vert P_{\theta_1})\le \frac{1}{\sqrt{N}}. \frac{1}{\sqrt{N}}=\frac{1}{N}
\end{equation*}\\
\noindent and thus the exponential term $e^{-N K(P_{\theta_0}\vert\vert P_{\theta_1})}$ is lower bounded by $e^{-1}$.
\noindent Finally:
\begin{equation*}
\mathcal{R}_N \geq \frac{\Delta}{32}\min_{k=0,\; 1}\vert \phi_k-1 \vert e^{-NK(P_{\theta_0}\vert\vert P_{\theta_1})}\ge \frac{1}{16e\sqrt{N}}
\end{equation*}


\subsection{Fast Rates}

\subsubsection{Proof of Proposition \ref{prop:low_noise}}
Let  $\mathcal{A}_N=\bigcap_{i<j} \{  (p_{i,j}-\frac{1}{2})(\widehat{p}_{i,j}-\frac{1}{2})>0 \}$.
On the event $\mathcal{A}_N$, $p$ and $\widehat{p}$ satisfy the strongly stochastic transitivity property, and agree on each pair, therefore $\widehat{\sigma}_N=\sigma^*$ and	$L(\widehat{\sigma}_N)-L^*=0$. We can suppose without loss of generality that for any $i<j$, $\frac{1}{2}+h \le p_{i,j} \le 1$, and we have $N\widehat{p}_{i,j}\sim \mathcal{B}(N,p_{i,j})$. We thus have:
\begin{equation}
\label{eq:binomial_proba}
\mathbb{P}\Big\{ \widehat{p}_{i,j}\le\frac{1}{2}  \Big\} = \mathbb{P}\Big\{ N\widehat{p}_{i,j}\le\frac{N}{2}  \Big\}
= \sum_{k=0}^{\lfloor \frac{N}{2} \rfloor}\binom{N}{k}p_{i,j}^k (1-p_{i,j})^{N-k}
\end{equation}
As $k \mapsto p_{i,j}^k (1-p_{i,j})^{N-k}$ is an increasing function of $k$ since $p_{i,j}>\frac{1}{2}$, we have
\begin{equation}
\label{eq:binom_increasing1}
\sum_{k=0}^{\lfloor \frac{N}{2} \rfloor}\binom{N}{k}p_{i,j}^k (1-p_{i,j})^{N-k} \le \sum_{k=0}^{\lfloor \frac{N}{2} \rfloor}\binom{N}{k} . p_{i,j}^{\frac{N}{2}} (1-p_{i,j})^{\frac{N}{2}}
\end{equation}
Then, since $\sum_{k=0}^{\lfloor{\frac{N}{2}}\rfloor }\binom{N}{k}+\sum_{k=\lfloor{\frac{N}{2}}\rfloor}^{N}\binom{N}{k}= \sum_{k=0}^{N}\binom{N}{k}= 2^N$ and $p_{i,j}\ge \frac{1}{2}+h$, we obtain
\begin{equation}
\label{eq:binom_increasing2}
\sum_{k=0}^{\frac{N}{2}}\binom{N}{k} . p_{i,j}^{\frac{N}{2}} (1-p_{i,j})^{\frac{N}{2}} \le  2^{N-1}. \left( \frac{1}{4}- h^2\right)^{\frac{N}{2}}
= \frac{1}{2}\left(1- 4h^2\right)^{\frac{N}{2}}
= \frac{1}{2} e^{-\frac{N}{2}log \left(\frac{1}{1-4h^2}\right)},
\end{equation}
\noindent Combining \eqref{eq:binomial_proba}, \eqref{eq:binom_increasing1} and \eqref{eq:binom_increasing2}, yields
\begin{equation}
\label{eq:binomial_bound}
\mathbb{P}\Big\{ \widehat{p}_{i,j}\le\frac{1}{2}  \Big\} \le \frac{1}{2}e^{-\frac{N}{2}log \left(\frac{1}{1-4h^2}\right)}
\end{equation}
Since the probability of the complementary of $\mathcal{A}_N$ is 
\begin{equation}
\label{eq:an_c}
\mathbb{P}\Big\{ \mathcal{A}_N^c  \Big\}= \mathbb{P}\Big\{ \bigcup_{i<j}\{ (p_{i,j}-\frac{1}{2})(\widehat{p}_{i,j}-\frac{1}{2})<0 \} \Big\}
=  \mathbb{P}\Big\{ \bigcup_{i<j}\{ \widehat{p}_{i,j}\le\frac{1}{2}\}  \Big\},
\end{equation}
combining \eqref{eq:binomial_bound} and  Boole's inequality on \eqref{eq:an_c} yields
\begin{equation}
\label{eq:an_c_bound}
\mathbb{P}\Big\{ \mathcal{A}_N^c  \Big\} \le \sum_{i<j} \mathbb{P}\Big\{ \widehat{p}_{i,j}\le\frac{1}{2}  \Big\}
 \le \frac{n(n-1)}{4} e^{-\frac{N}{2}log \left(\frac{1}{1-4h^2}\right)}.
\end{equation}
As the expectation of the excess of risk can be written 
\begin{equation*}
\mathbb{E}\Big\{L(\widehat{\sigma}_N)-L^*\Big\}= \mathbb{E}\Big\{(L(\widehat{\sigma}_N)-L^*) \mathbb{I}\{ \mathcal{A}_N \} +\ (L(\widehat{\sigma}_N)-L^*) \mathbb{I}\{ \mathcal{A}_N^c \}\Big\},
\end{equation*}
using successively the fact that $L(\widehat{\sigma}_N)-L^*=0$ on $\mathcal{A}_N$ and \eqref{eq:an_c_bound} we obtain finally
\begin{equation*}
\mathbb{E}\Big\{L(\widehat{\sigma}_N)-L^*\Big\}\leq \frac{n(n-1)}{2} \mathbb{P} \Big\{ \mathcal{A}_N^c  \Big\}
\leq \frac{n^2(n-1)^2}{8} e^{-\frac{N}{2}log \left(\frac{1}{1-4h^2}\right)}.
\end{equation*}

\vspace{0.2cm}
\subsubsection{Remark 12}
 According to \eqref{eq:Kendall_risk_min} and \eqref{eq:Kendall_risk_min_empirical} we have
 \begin{equation*}
 \mathbb{E}[(\widehat{L}^*-L^*  )^2]= \mathbb{E}\left[ \left( \sum_{i<j}\left\{  \frac{1}{2}-\left\vert \widehat{p}_{i,j}-\frac{1}{2}\right\vert  \right\}  %\right.\right.\\
 %& \left. \left.
 -\sum_{i<j}\left\{  \frac{1}{2}-\left\vert p_{i,j}-\frac{1}{2}\right\vert  \right\} \right)^2 \right],
 \end{equation*}
 and pushing further the calculus gives
\begin{equation*}
\mathbb{E}[(\widehat{L}^*-L^*  )^2]= \mathbb{E}\left[ \left(\sum_{i<j}\left\vert p_{i,j}-\frac{1}{2}\right\vert -\left\vert \widehat{p}_{i,j}-\frac{1}{2}\right\vert \right)^2 \right] 
=\mathbb{E}\left[ \left(\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert\right)^2 \right].
\end{equation*}
Firstly, with the bias-variance decomposition we obtain
\begin{equation}
\label{eq:bias_variance}
\mathbb{E}[(\widehat{L}^*-L^*  )^2]=Var\left(\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right) + 
\left(\mathbb{E}\left[\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right] \right)^2.
\end{equation}
The bias in \eqref{eq:bias_variance} can be written as
\begin{equation}
\label{eq:bias}
\mathbb{E}\left[\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right]=\sum_{\substack{i<j\\p_{i,j}>\widehat{p}_{i,j} }}\mathbb{E}\left[p_{i,j}-\widehat{p}_{i,j}\right] + \sum_{\substack{i<j\\p_{i,j}<\widehat{p}_{i,j} }}\mathbb{E}\left[\widehat{p}_{i,j}-p_{i,j}\right]\\
=0
\end{equation}
And the variance in \eqref{eq:bias_variance} is  
\begin{align}
\label{eq:variance_upper_bound1}
Var\left(\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right)&= \sum_{i<j} \sum_{i'<j'} Cov\left(\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert , \left\vert p_{i',j'}-\widehat{p}_{i',j'}\right\vert \right)\\
& \le \sum_{i<j} \sum_{i'<j'} \sqrt{Var(\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert)Var(\left\vert p_{i',j'}-\widehat{p}_{i',j'}\right\vert )}.
\end{align}
Since for $i<j$, $\widehat{p}_{i,j} \sim \mathcal{B}(N,p_{i,j})$, we have
\begin{equation}
\label{eq:variance_product}
Var(\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert)Var(\left\vert p_{i',j'}-\widehat{p}_{i',j'}\right\vert )= \frac{p_{i,j}(1-p_{i,j})p_{i',j'}(1-p_{i',j'})}{N^2} \le \frac{1}{16N^2}.
\end{equation}
Therefore combining \eqref{eq:variance_product} with \eqref{eq:variance_upper_bound1} gives
\begin{equation}
\label{eq:variance_upper_bound2}
Var\left(\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right) \le \left(\frac{n(n-1)}{2}\right)^2 \frac{1}{4N}.
\end{equation}
Finally according to \eqref{eq:bias_variance}, \eqref{eq:bias} and \eqref{eq:variance_upper_bound2} we obtain: $ \mathbb{E}[(\widehat{L}^*-L^*  )^2]\le \frac{n^2(n-1)^2}{16N}$.

\vspace{0.5cm}
\subsubsection{Proof of Proposition \ref{prop:lower_bound_low_noise}}

Similarly to Proposition \ref{prop:minimax_bound}, we use Le Cam's method and consider two Mallows models $P_{\theta_0}$ and $P_{\theta_1}$ where $\theta_k=(\sigma^*_k,\phi)\in \mathfrak{S}_n\times (0,1)$ and $\sigma^*_0\neq \sigma^*_1$. We can lower bound the minimax risk as follows
\begin{align*}
\mathcal{R}_N\geq& \inf_{\sigma_N}\max_{k=0,\; 1} \mathbb{E}_{P_{\theta_k}}\left[L_{P_{\theta_k}}(\sigma_N)-L_{P_{\theta_k}}^*  \right]\\
= & \inf_{\sigma_N}\max_{k=0,\; 1}\sum_{i<j} \mathbb{E}_{P_{\theta_k}}\left[2\vert p_{i,j} - \frac{1}{2} \vert\times
% \right. \\
%& \left. 
\mathbb{I}\{ (\sigma_N(i)-\sigma_N(j)(\sigma^*(i)-\sigma_k^*(j))<0  \} \right]\\
\geq & \inf_{\sigma_N}\max_{k=0,\; 1} h \mathbb{E}_{P_{\theta_k}}\left[ d_{\tau}(\sigma_N, \sigma^*)\right]\\
\geq & h \frac{\Delta}{4} e^{-N K(P_{\theta_0\vert \vert \theta_1})}
\end{align*}
With $ K(P_{\theta_0\vert \vert \theta_1})= \log(\frac{1}{\phi}) \frac{1-\phi}{1+\phi}$ accordig to \eqref{eq:Kullback_mallows} and $\Delta=1$, choosing $\sigma_0$ and $\sigma_1$ as in the proof of Proposition \ref{prop:minimax_bound}. Now we take $\phi= \frac{1-2h}{1+2h}$ so that both $P_{\theta_0}$ and $P_{\theta_1}$ satisfy {\bf NA}$(h)$, and we have $K(P_{\theta_0\vert \vert \theta_1})= 2h \log(\frac{1+2h}{1-2h})$, which gives us finally:
\begin{equation*}
\mathcal{R}_N\geq \frac{h}{4} e^{-N 2h \log(\frac{1+2h}{1-2h})}
\end{equation*}

\bibliographystyle{apalike}
\nobibliography{biblio-thesis}

\end{document}
