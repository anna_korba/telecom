
\section{EMPIRICAL CONSENSUS}\label{sec:rates}

Here, our goal is to establish sharp bounds for the excess of risk of  \textit{empirical Kemeny medians}, of solutions $\widehat{\sigma}_N$ of \eqref{eq:ERM1} in the Kendall's $\tau$ distance case namely. Beyond the study of universal rates for the convergence of the expected distance $L(\widehat{\sigma}_N)$ to $L^*$, we prove that, under the stochastic transitivity condition, exponentially fast convergence occurs, if the $p_{i,j}$'s are bounded away from $1/2$, similarly to the phenomenon exhibited in \cite{KB05} for binary classification under extremely low noise assumption.
\subsection{Universal Rates}
 Such rate bounds are classically based on the fact that any minimizer $\widehat{\sigma}_n$ of \eqref{eq:ERM1} fulfills
 \begin{equation}\label{eq:classic}
 L(\widehat{\sigma}_N)-L^*\leq 2\max_{\sigma\in \mathfrak{S}_n}\vert \widehat{L}_N(\sigma)- L(\sigma)\vert.
 \end{equation}
 As the cardinality of the set $\Sn$ of median candidates is finite, they can be directly derived from bounds (tail probabilities or expectations) for the absolute deviations of i.i.d. sample means $\widehat{L}_N(\sigma)$ from their expectations, $\vert\widehat{L}_N(\sigma)- L(\sigma)\vert$. Let $\widehat{p}_{\i,\j}=(1/N)\sum_{m=1}^N\mathbb{I}\{  \Sigma_m(i)<\Sigma_m(j)\}$ and $p_{\i,\j}$ the r.v. defined in Remark \ref{remark:connection_binary}. First notice that same as in \eqref{eq:loss-1} one has for any $\sigma\in\Sn$:
 \begin{multline}\label{eq:classic2}
 \widehat{L}_{N}(\sigma)
 =\frac{n(n-1)}{2}\mathbb{E}\left[ \widehat{p}_{\i,\j}\mathbb{I}\{\sigma(\i)>\sigma(\j) \}  \right.\\
 + \left. (1-\widehat{p}_{\i,\j})\mathbb{I}\{\sigma(\i)<\sigma(\j) \}\right]
 \end{multline}
which, combined with \eqref{eq:classic}, gives
\begin{equation}
\label{eq:classic2}
\vert\widehat{L}_{N}(\sigma) - L(\sigma)\vert 
\leq \frac{n(n-1)}{2}\mathbb{E}_{\i,\j}\left[ \vert p_{\i,\j}-\widehat{p}_{\i,\j} \vert  \right].
\end{equation}
This leads to the bounds in expectation and probability for ERM in the context of Kemeny ranking aggregation stated below, unsurprisingly of order $O(1/\sqrt{N})$.
% \begin{equation}
 %\mathbb{P}\left\{  \vert\widehat{L}_N(\sigma)- L(\sigma)\vert  >t \right\}.
 %\end{equation}
\begin{proposition}\label{prop:ERM}
Let $N\geq 1 $ and $\widehat{\sigma}_N$ be any Kemeny empirical median based on i.i.d. training data $\Sigma_1,\; \ldots,\; \Sigma_N$, \textit{i.e.} a minimizer of \eqref{eq:emp_risk} over $\Sn$ with $d=d_{\tau}$. The excess risk of $\widehat{\sigma}_N$ is upper bounded:
\begin{itemize}
\item[(i)] In expectation by
\[
\mathbb{E}\left[L(\widehat{\sigma}_N)-L^*\right]\leq \frac{n(n-1)}{2\sqrt{N}}
\]
\item[(ii)] With probability higher than $1-\delta$ for any $\delta\in(0,1)$ by
\begin{equation*}
L(\widehat{\sigma}_{N}) - L^{\ast} \leq  \frac{n(n-1)}{2}\sqrt{\frac{2\log(n(n-1)/\delta)}{N}}.
\end{equation*}
\end{itemize}
\end{proposition}

The proof is given in the Appendix section.

 
%Notice that, as an examination of the related argument shows, the rate bound \eqref{eq:ERM_bound_gen} hold true for any distance $d$ on $\Sn$, except that $n(n-1)$ must be then replaced by $2\max_{(\sigma,\sigma')\in \Sn^2}d(\sigma,\sigma')$.

\begin{remark}
As the problem \eqref{eq:ERM1} is NP-hard in general, one uses in practice an optimization algorithm to produce an approximate solution $\widetilde{\sigma}_N$ of the original minimization problem, with a control of the form: $\widehat{L}_N(\widetilde{\sigma}_N)\leq \min_{\sigma\in \Sn}\widehat{L}_N(\sigma)+\rho$, where $\rho>0$ is a tolerance fixed in advance, see \textit{e.g.} \cite{JKS16}. As pointed out in \cite{BB08nips}, a bound for the expected excess of risk of $\widetilde{\sigma}_N$ is then obtained by adding the quantity $\rho$ to the estimation error given in Proposition \ref{prop:ERM}.
\end{remark}

%For any empirical median $\widehat{\sigma}_N$, Hoeffding inequality combined with \eqref{eq:classic} and the union bound yields: $\forall t>0$,
%\begin{align*}
%	\mathbb{P}\left\{L(\widehat{\sigma}_N)-L^*  >t  \right\}&\leq  \sum_{\sigma\in \mathfrak{S}_n}\mathbb{P}\left\{ \frac{1}{N}\left\vert\sum_{i=1}^N  a_i \right\vert     >\frac{t}{2} \right\}\\
%	&\text{with } a_i= \left\{ d(\Sigma_i, \sigma)-L(\sigma)    \right\}\\
%	&\leq  2n!\exp\left(-\frac{Nt^2}{2\vert\vert d\vert\vert_{\infty}^2}  \right),
%\end{align*}

%where $\vert\vert d\vert\vert_{\infty}=\max_{(\sigma,\sigma')\in \mathfrak{S}_n^2}d(\sigma,\sigma')$. In expectation this leads to:
%\begin{equation*}
%	\mathbb{E}\left\{\sup_{\widehat{\sigma}_N} \vert L(\widehat{\sigma}_N)-L^*\vert  \right\}\leq \sqrt{\frac{2}{N}} \vert\vert d\vert\vert_{\infty} \left[  \sqrt{log(2n!)} +\sqrt{\pi} \right]
%\end{equation*}

%This immediately leads to the universal bound for ERM stated below.

%\begin{proposition}
%	Consider a distance $d$ on the symmetric group $\mathfrak{S}_n$. Let $\widehat{\sigma}_N$ be any empirical median based on i.i.d. training data $\Sigma_1,\; \ldots,\; \Sigma_N$ w.r.t. distance $d$. For any $\delta\in (0,1)$, we have with probability at least $1-\delta$: $\forall N\geq 1$,
%	\begin{equation}\label{eq:ERM_bound_gen}
%	L(\widehat{\sigma}_N)-L^*\leq \sqrt{2}\vert\vert d\vert\vert_{\infty}\sqrt{\frac{\log(2n! /\delta)}{N}}.
%	\end{equation}
%\end{proposition}

We now establish the tightness of the upper bound for empirical Kemeny aggregation stated in Proposition \ref{prop:ERM}. Precisely, the next theorem provides a lower bound of order $O(1\sqrt{N})$ for the quantity below, referred to as the \textit{minimax risk},
\begin{equation}\label{eq:minimax}
\mathcal{R}_N\overset{def}{=}\inf_{\sigma_N}\sup_P \mathbb{E}_P\left[L_P(\sigma_N)-L_P^*  \right],
\end{equation}
where the supremum is taken over all probability distributions on $\mathfrak{S}_n$ and the infimum is taken over all mappings $\sigma_{N}$ that maps a dataset $(\Sigma_1,\; \ldots,\, \Sigma_N)$ composed of $N$ independent realizations of $P$ to an empirical median candidate .
%This quantity, referred to as the \textit{minimax risk}, quantifies the difficulty of the learning problem since it is the smallest possible maximum (worst case) risk.
%We use the classical Le Cam method for bouding the minimax risk of the ranking aggregation problem in the case of Kendall's tau distance.


\begin{proposition}
\label{prop:minimax_bound}
The minimax risk for Kemeny aggregation is lower bounded as follows:
	\begin{equation*}
	\mathcal{R}_N \geq  \frac{1}{16e\sqrt{N}}.
	\end{equation*}	
\end{proposition}
The proof of Proposition \ref{prop:minimax_bound} relies on the classical Le Cam's method, it is detailed in the Supplementary Material. The result shows that no matter the method used for picking a median candidate from $\Sn$ based on the training data, one may find a distribution such that the expected excess of risk is larger than $1/(16e\sqrt{N})$. If the upper bound from Proposition \ref{prop:ERM} depends on $n$, it is also of order $O(1/\sqrt{N})$ when $N$ goes to infinity. Empirical Kemeny aggregation is thus optimal in this sense.

\begin{remark}{\sc (Dispersion estimates)} In the stochastically transitive case, one may get an estimator of $L^*$ by plugging the empirical estimates $\widehat{p}_{i,j}$ into Formula \eqref{eq:Kendall_risk_min}:
	\begin{eqnarray}
	\label{eq:Kendall_risk_min_empirical}
	\widehat{L}^*
	&=&\sum_{i<j}\min\{\widehat{p}_{i,j},\; 1-\widehat{p}_{i,j}  \}\\
	&=&\sum_{i<j}\left\{  \frac{1}{2}-\left\vert \widehat{p}_{i,j}-\frac{1}{2}\right\vert  \right\}. \nonumber
	\end{eqnarray}
	One may easily show that the related MSE is of order $O(1/N)$: $\mathbb{E}[(\widehat{L}^*-L^*  )^2]\leq  n^2(n-1)^2/(16N)$, see the Supplementary Material.
	Notice also that, in the Kendall's $\tau$ case, the alternative dispersion measure \eqref{eq:disp_meas2} can be expressed as $\gamma(P)=\sum_{i<j}p_{i,j}(1-p_{i,j})$
	and that the plugin estimator of $\gamma(P)$ based on the $\widehat{p}_{i,j}$'s coincides with \eqref{eq:emp_disp_meas2}.
\end{remark}

While Proposition \ref{prop:ERM} makes no assumption about the underlying distribution $P$, it is also desirable to understand the circumstances under which the excess risk of empirical Kemeny medians is small. Following in the footsteps of results obtained in binary classification, it is the purpose of the subsequent analysis to exhibit conditions guaranteeing exponential convergence rates in Kemeny aggregation.

\subsection{Fast Rates in Low Noise}
The result proved in this subsection shows that the bound stated in Proposition \ref{prop:ERM} can be significantly improved under specific conditions. In binary classification, it is now well-known that (super) fast rate bounds can be obtained for empirical risk minimizers, see \cite{MasNed06}, \cite{Tsy04}, and for certain \textit{plug-in} rules, see \cite{Audibert1}. 
As shown below, under the stochastic transitivity hypothesis and the following \textit{low noise assumption} (then implying strict stochastic transitivity), the risk of empirical minimizers in Kemeny aggregation converges exponentially fast to $L^*$ and remarkably, with overwhelming probability, empirical Kemeny aggregation has a unique solution that coincides with a natural \textit{plug-in} estimator of the true median (namely $s^*$ in this situation, see Theorem \ref{thm:optimal}). For $h>0$, we define condition:
\begin{center}
{\bf NA}$(h)$: $\min_{i<j}\vert p_{i,j}-1/2 \vert\geq h$.
\end{center}


\begin{remark}{\sc (Low noise for parametric models )}
	Condition {\bf NA}$(h)$ is fulfilled by many parametric models. For example, the Mallows model \eqref{eq:Mallows} parametrized by $\theta=(\sigma_0, \phi)\in \mathfrak{S}_n\times [0,1]$ satisfies {\bf NA}$(h)$ iff $ \phi \le (1-2h)/(1+2h)$. For the Bradley-Terry-Luce-Plackett model with preference vector $w = [w_i ]_{1\le i \le n}$, condition {\bf NA}$(h)$ is satisfied iff $\min_{1\leq i \leq n} \vert w_i-w_{i+1}\vert \ge (4h)/(1-2h)$, see \cite{CS15} where minimax bounds are obtained for the problem of identifying top-K items.
\end{remark}
 This condition may be considered as analogous to that introduced in \cite{KB05} in binary classification, and was used in \cite{shah2015stochastically} to prove fast rates for the estimation of the matrix of pairwise probabilities.
 
%\begin{proposition}
%	\label{prop:fast_rate}
%	Suppose that the \textit{margin condition} \eqref{eq:margin_condition} is fulfilled by the underlying probability distribution $P$. For any empirical median $\widehat{\sigma}_N$, for any $\delta\in (0,1)$, we have with probability at least $1-\delta$: $\forall N\geq 1$, $\forall N\geq 1$,
%	\begin{equation}\label{eq:Bern_bound}
%	L(\widehat{\sigma}_N)-L^*  \leq 2 \frac{2^{n(n-1)/2-1}}{Nh}\log(n!/\delta).
%	\end{equation}
%\end{proposition}	


 %But moreover we can prove that under the \eqref{eq:margin_condition} and the strongly stochastic transitivity conditions, the rate of convergence is exponential. 
\begin{proposition}
	\label{prop:low_noise}
	Assume that $P$ is stochastically transitive and fulfills condition {\bf NA}$(h)$ for some $h>0$. The following assertions hold true.
	\begin{itemize}
\item[(i)] For any empirical Kemeny median $\widehat{\sigma}_N$, we have: $\forall N\geq 1$,
	\[
	\mathbb{E}\left[L(\widehat{\sigma}_N)-L^*\right]\leq \frac{n^2(n-1)^2}{8} e^{-\frac{N}{2}\log \left(\frac{1}{1-4h^2}\right)}.
	\]
\item[(ii)]	With probability at least $1-(n(n-1)/4) e^{-\frac{N}{2}log \left(\frac{1}{1-4h^2}\right)}$, the mapping
$$\widehat{s}_N(i)=1+\sum_{k\ne i}\mathbb{I}\{\widehat{p}_{i,k}<\frac{1}{2} \}$$ for $1\leq i\leq n$ belongs to $\Sn$ and is the unique solution of the empirical Kemeny aggregation problem \eqref{eq:ERM1}. It is then referred to as the plug-in Kemeny median.
\end{itemize}
\end{proposition}	


%\subsection{The Kendall's $\tau$ case.}
%The bound \eqref{eq:ERM_bound_gen} naturally applies to the specific case $d=d_{\tau}$, with $\vert\vert d_{\tau}\vert\vert_{\infty}=n(n-1)/2$. But it may be refined with other hypothesis. We suppose from now on that none of the pairwise probabilities $p_{i,j}$ is equal to $1/2$ and that the strongly stochastic transitivity property is fulfilled. We recall that the (unique) median can be expressed as a function of the $p_{i,j}$'s only, by virtue of Theorem 5.
%\medskip
%\noindent {\bf Plug-in empirical Kendall's $\tau$ median.} This may provide a motivation for considering \textit{plug-in} empirical medians, defined as follows. For any $(i,j)\in \mathcal{E}_n$, let $\widehat{p}_{i,j}\neq 1/2$ be an estimate of $p_{i,j}$ such that the collection $(\widehat{p}_{i,j})_{(i,j)\in \mathcal{E}_n}$ satisfies the strongly stochastic transitivity property. The mapping defined for $i\in\{1,\; \ldots,\; n  \}$ by:
%\begin{equation}\label{eq:Kendall_emp_med}
%\widehat{\sigma}^*(i)=1+\sum_{i<k}\mathbb{I}\{\widehat{p}_{i,k}<\frac{1}{2}  \}+\sum_{l<i}\mathbb{I}\{\widehat{p}_{l,i}>\frac{1}{2}  \} 
%\end{equation}
%thus defines a permutation.  In the particular case where the empirical distribution $\widehat{P}_N=(1/N)\sum_{m=1}^N\delta_{\Sigma_m}$ fulfills the strongly stochastic transitivity condition, one may consider the empirical estimates for all $i<j$,
%\begin{equation}\label{eq:emp_pair}
%\widehat{p}_{i,j}=\frac{1}{N}\sum_{m=1}^N\mathbb{I}\{ \Sigma_m(i)<\Sigma_m(j)  \}.
%\end{equation}
The technical proof is given in the Supplementary Material. The main argument consists in showing that, under the hypotheses stipulated, with very large probability, the empirical distribution $\widehat{P}_N=(1/N)\sum_{i=1}^N\delta_{\Sigma_i}$ is strictly stochastically transitive and Theorem \ref{thm:optimal} applies to it. 
%In this case, applying Copeland method with $\widehat{s}_N$ enables to retrieve the empirical Kemeny median. In the case where the empirical distribution does not satisfy this condition, there exists a cycle in the empirical preferences ($\exists i,j,k$ s.t. $\widehat{p}_{i,j}>1/2,\widehat{p}_{j,k}>1/2,$ and $\widehat{p}_{i,k}>1/2$). Finding a minimum of the empirical Kemeny aggregation problem can then be viewed as looking for a minimum of the Feedback Arc Set problem on weighted tournaments, see \cite{KMS07}.
Proposition \ref{prop:low_noise} gives a rate in $O(e^{-\alpha_{h}N})$ with $\alpha_{h} = \frac{1}{2}log \left(1/(1-4h^2)\right)$. Notice that $\alpha_{h}\rightarrow+\infty$ as $ h\rightarrow 1/2$, which corresponds to the situation where the distribution converges to a Dirac $\delta_{\sigma}$ since $P$ is supposed to be stochastically transitive. Therefore the greatest $h$ is, the easiest is the problem and the strongest is the rate. On the other hand, the rate decreases when $h$ gets smaller.
The next result proves that, in the low noise setting, the rate of Proposition \ref{prop:low_noise} is almost sharp in the minimax sense.
\begin{proposition}\label{prop:lower_bound_low_noise} Let $h>0$ and define 
$$
\widetilde{\mathcal{R}}_N(h)=\inf_{\sigma_N}\sup_P \mathbb{E}_P\left[L_P(\sigma_N)-L_P^*  \right],
$$
where the supremum is taken over all stochastically transitive probability distributions $P$ on $\Sn$ satisfying {\bf NA}$(h)$. We have: $\forall N\geq 1$,
\begin{equation}
\label{eq:minimax_lownoise}
\widetilde{\mathcal{R}}_N(h)\geq \frac{h}{4} e^{-N 2h \log(\frac{1+2h}{1-2h})}  .
\end{equation}
\end{proposition}

The proof of Proposition \ref{prop:lower_bound_low_noise} is provided in the Supplementary Material. It shows that the minimax rate is lower bounded by a rate in $O(e^{-\beta_{h}N})$ with $\beta_{h} = 2h\log((1+2h)/(1-2h))$. Notice that $\alpha_{h}\sim\beta_{h}/2$ when $h\rightarrow 1/2$. The rate obtained for empirical Kemeny aggregation in Proposition \ref{prop:low_noise} is thus almost optimal in this case. The bound from Proposition \ref{prop:lower_bound_low_noise} is however too small when $h\rightarrow 0$ as it goes to $0$. Improving the minimax lower bound in this situation is left for future work.

\subsection{Computational Issues}

As mentioned previously, the computation of an empirical Kemeny consensus is NP-hard and therefore usually not tractable in practice. Proposition \ref{prop:ERM} and \ref{prop:low_noise} can therefore be seen as providing theoretical guarantees for the ideal estimator $\widehat{\sigma}_{N}$. Under the low noise assumption however, Proposition \ref{prop:low_noise} also has a practical interest. Part $(ii)$ says indeed that in this case, the Copeland method (ordering items by decreasing score $\widehat{s}_{N}$), which has complexity in $O(N\binom{n}{2})$, outputs the exact Kemeny consensus with high probability. Furthermore, part $(i)$ actually applies to any empirical median $\tilde{\sigma}_{N}$ that is equal to $\widehat{\sigma}_{N}$ with probability at least $1-(n(n-1)/4)e^{-(N/2)\log(1/(1-4h^{2}))}$ thus in particular to the Copeland method. In summary, under assumption {\bf NA}$(h)$ with $h>0$, the tractable Copeland method outputs the exact Kemeny consensus with high probability and has almost optimal excess risk convergence rate.

%\begin{proposition}
%\label{prop:plugin_empirical}
%Consider a plug-in median $\widehat{\sigma}_{N}=\widehat{\sigma}^*$ defined by (\ref{eq:Kendall_emp_med}), based on stochastically transitive pairwise probabilities $\widehat{p}_{i,j}$, $(i,j)\in \mathcal{E}_n$. For any $\delta\in (0,1)$, we have with probability at least $1-\delta$: $\forall N\geq 1$,
%\begin{equation}
%\label{eq:Hoeffding-bound}
%L(\widehat{\sigma}_{N}) - L^{\ast} \leq  \binom{n}{2}\sqrt{\frac{\log(n(n-1)/\delta)}{2N}}.
%\end{equation}
%Moreover, under the margin condition : 
%\begin{equation}
%\label{eq:margin_condition}
%\exists h>0 \text{ such that }\vert p_{i,j}-1/2\vert \geq h \text{ for all } (i,j)\in\mathcal{E}_n ,
%\end{equation}
%one has with probability greater than $1-\delta$
%\begin{multline}
	%	\label{eq:Bernstein-bound}
	%	L(\widehat{\sigma}_{N}) - L^{\ast} \leq  2\binom{n}{2}\left[\sqrt{\frac{2(1/4-h^{2})\log(\frac{n(n-1)}{\delta})}{N}}  \right.\\ +\left. \frac{2\log(n(n-1)/\delta)}{3N}\right].
%\end{multline}
%\end{proposition}
%Notice that the first term in \eqref{eq:Bernstein-bound} is equal to $2\sqrt{1/4 - h^{2}}$ times the term in \eqref{eq:Hoeffding-bound}. This factor being $\leq 1$ for any $0 \leq h \leq 1/2$, the bound \eqref{eq:Bernstein-bound} is always asymptotically better than the bound \eqref{eq:Hoeffding-bound} and much better when $h$ is close to $1/2$.  The major drawback of the plugin approach lies in the fact that it may happen that $\widehat{P}_N$ does not fulfill the stochastically transitive condition, even if $P$ does. However, as shown by the subsequent analysis, this happen with very small probability (vanishing at an exponential rate).




