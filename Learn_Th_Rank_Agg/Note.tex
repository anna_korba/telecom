\documentclass[12pt]{article}
\usepackage{amssymb}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{epsfig}
\usepackage{geometry}
\usepackage{stmaryrd}

\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{assumption}[theorem]{Assumption}
%\newtheorem{remark}[theorem]{Remark}
\newtheorem{rema}{Remark}
%\newenvironment{remark}{\begin{rema} \rm}{\end{rema}}
%\newtheorem{example}[theorem]{Example}
\newtheorem{exam}{Example}
\newenvironment{example}{\begin{exam} \rm}{\end{exam}}

\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\newenvironment{proof}[1][Proof]{\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\def\R{\mathbb{R}}
\def\Rd{\R^d}
\def\E{\mathbb E}
\def\e{\mathbf e}
\def\i{\mathbf i}
\def\j{\mathbf j}
\def\EXP{{\E}}
\def\expec{{\EXP}}
\def\PROB{{\mathbb P}}
\def\S{\mathcal{S}}
\def\var{{\rm Var}}
\def\shat{{\mathbb S}}
\def\IND#1{{\mathbb I}_{{\left[ #1 \right]}}}
%\def\IND#1{{\mathbb I}_{{ #1 }}}
\def\I{{\mathbb I}}
\def\pr{\PROB}
\def\prob{\PROB}
\def\wh{\widehat}
\def\ol{\overline}
\newcommand{\deq}{\stackrel{\scriptscriptstyle\triangle}{=}}
\newcommand{\defeq}{\stackrel{\rm def}{=}}
\def\isdef{\defeq}
\newcommand{\xp}{\mbox{$\{X_i\}$}}
\def\diam{\mathop{\rm diam}}
\def\argmax{\mathop{\rm arg\, max}}
\def\argmin{\mathop{\rm arg\, min}}
\def\essinf{\mathop{\rm ess\, inf}}
\def\esssup{\mathop{\rm ess\, sup}}
\def\supp{\mathop{\rm supp}}
\def\cent{\mathop{\rm cent}}
\def\dim{\mathop{\rm dim}}
\def\sgn{\mathop{\rm sgn}}
\def\logit{\mathop{\rm logit}}
\def\proof{\medskip \par \noindent{\sc proof.}\ }
\def\proofsketch{\medskip \par \noindent{\sc Sketch of proof.}\ }
%% Not in latex2e:
%  \def\qed{\hfill $\Box$ \medskip}
%  \def\qed{$\Box$}
\def\blackslug{\hbox{\hskip 1pt \vrule width 4pt height 8pt depth 1.5pt
\hskip 1pt}}
\def\qed{\quad\blackslug\lower 8.5pt\null\par}
\newcommand{\F}{{\cal F}}
\newcommand{\G}{{\cal G}}
\newcommand{\X}{{\cal X}}
\newcommand{\D}{{\cal D}}
\newcommand{\cR}{{\cal R}}
\newcommand{\COND}{\bigg\vert} % for conditional expectations
%
\def\C{{\cal C}}
\def\roc{\rm ROC}
\def\auc{\rm AUC}
\def\B{{\cal B}}
\def\A{{\cal A}}
\def\N{{\cal N}}
\def\F{{\cal F}}
\def\L{{\cal L}}
\def\eps{{\varepsilon}}
\def\Var{{\rm Var}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\Prob}[1]{\mathbb{P}\left\{ #1 \right\} }
\newcommand{\Sn}{\mathfrak{S}_{n}}

\allowdisplaybreaks

\begin{document}
\begin{center}
\large{Statistical Recovery of the Median of a Ranking Distribution}
\end{center}
\hrule

\bigskip


\noindent {\bf Statistical Framework.} Consider a generic random variable $\Sigma$ defined on a probability space $(\Omega,\; \mathcal{F},\; \mathbb{P})$ taking its values in the symmetric group $\mathfrak{S}_n$, with $n\geq 1$. Denote by $P$ its probability distribution (\textit{i.e.} $P(\sigma)=\mathbb{P}\{ \Sigma=\sigma \}$ for any $\sigma\in \mathfrak{S}_n$) and suppose that we observe $\Sigma_1,\; \ldots,\; \Sigma_N$, $N\geq 1$ independent copies of $\Sigma$. Let $d$ be a certain distance on $\mathfrak{S}_n$ (\textit{e.g.} the Kendall tau distance). The expected distance between any permutation $\sigma$ and $\Sigma$ is denoted by:
$$
L(\sigma)=\mathbb{E}_{\Sigma \sim P}[d(\Sigma,\sigma)  ].
$$
We are interested in recovering approximately a median of distribution $P$, namely a solution $\sigma^*$ of the minimization problem:
\begin{equation}\label{eq:median_pb}
\min_{\sigma \in \mathfrak{S}_n}L(\sigma),
\end{equation}
plus an estimate of this minimum, as accurate as possible.
Observe that the minimization problem has always a solution since the cardinality of $\mathfrak{S}_n$ is finite (however exploding with $n$) but can be multimodal. A median permutation $\sigma^*$ is a central value for $P$, a crucial \textit{location parameter}, whereas the minimum $L(\sigma^*)$ can be viewed as a dispersion measure.
However, the functional $L(.)$ is unknown in practice, just like the distribution $P$ (in order to avoid any ambiguity, we write $L_P(.)$ when needed). We only have access to the dataset $\{\Sigma_1,\; \ldots,\; \Sigma_N  \}$ to find a resonable approximant of a median and would like to avoid rigid assumptions on $P$ (such as those stipulated by the Mallows model, see \cite{Mallows57} and Remark \ref{Rk:Mallows}). Following the Empirical Risk Minimization (ERM) paradigm (see \textit{e.g.} \cite{Vapnik}), one replaces the quantity $L(\sigma)$ by a statistical version based on the sampling data such as the unbiased estimator
\begin{equation}
\widehat{L}_N(\sigma)=\frac{1}{N}\sum_{i=1}^Nd(\Sigma_i,\sigma),
\end{equation}
 or a possibly 'smoothed' and/or penalized variant $\widetilde{L}_N(\sigma)$ (a smoothing, convexification, relaxation recipe should permit to make optimization easier, while penalization may guarantee certain desirable properties of the solution, such as sparsity in the euclidian setting).
 \begin{remark} {\sc (Alternative dispersion measure)}
 An alternative measure of dispersion is given by
 \begin{equation}\label{eq:disp_meas2}
 \gamma(P)=\frac{1}{2}\mathbb{E}[ d(\Sigma,\Sigma') ],
 \end{equation}
 where $\Sigma'$ is an independent copy of $\Sigma$, which can be more easily estimated than $L^*=L(\sigma^*)$. The estimator of \eqref{eq:disp_meas2} with minimum variance among all unbiased estimators is given by the $U$-statistic
 \begin{equation}\label{eq:emp_disp_meas2}
 \widehat{\gamma}_N=\frac{2}{N(N-1)}\sum_{i<j}d(\Sigma_i,\Sigma_j).
 \end{equation}
 Confidence intervals for the parameter $\gamma(P)$ can be constructed by means of Hoeffding/Bernstein type deviation inequalities for $U$-statistics and a direct bootstrap procedure can be applied for this purpose (in contrast, a bootstrap technique for building CI's for $L^*$ would require to solve several times an empirical version of \eqref{eq:median_pb} based on bootstrap samples).
 \begin{itemize}
 \item A validity framework for bootstrapping the $U$-statistic \eqref{eq:emp_disp_meas2} may require to implement a smoothing procedure (see \cite{Lahiri93}), {\bf find a bootstrap technique and conditions guaranteeing its asymptotic validity}.
\item By means of the triangular inequality, we have $\gamma(P)\leq L^*$ and, by definition, $\mathbb{E}[d(\sigma^*,\Sigma)]\leq \mathbb{E}[d(\Sigma',\Sigma)\mid \Sigma'  ]$ almost-surely, so that $L^*\leq 2\gamma(P)$ by taking the expectation w.r.t. to $\Sigma'$. Considering the analogy with the Hilbertian case, {\bf find conditions ensuring that \eqref{eq:disp_meas2} is an even more accurate approximant of $L^*$}.
 \end{itemize}
 \end{remark}
\bigskip



\noindent {\bf Objectives.} Our goal is to investigate the accuracy of a permutation $\widehat{\sigma}_n\in\mathfrak{S}_N$ built from data $\Sigma_1,\; \ldots,\; \Sigma_N$ by solving the ERM problem
\begin{equation}\label{eq:ERM1}
\min_{\sigma\in \mathfrak{S}_n}\widehat{L}_N(\sigma),
\end{equation}
or a surrogate problem (\textit{e.g.} 'smoothing', penalization, embedding relaxation). In the statistical learning folklore, one tries to establish sharp confidence probability bounds for the excess of risk. Such bounds are based on the fact that the solution $\widehat{\sigma}_n$ of \eqref{eq:ERM1} fulfills
\begin{equation}\label{eq:classic}
L(\widehat{\sigma}_N)-L^*\leq 2\max_{\sigma\in \mathfrak{S}_n}\vert \widehat{L}_N(\sigma)- L(\sigma)\vert,
\end{equation}
so that rate bounds can be derived from bounds for the tail probabilities
\begin{equation}
\mathbb{P}\left\{  \vert\widehat{L}_N(\sigma)- L(\sigma)\vert  >t \right\}.
\end{equation}
As the problem \eqref{eq:ERM1} is NP-hard, one considers an approximate solution $\widetilde{\sigma}_N$ of this problem, which leads us to control ({\bf in probability or in expectation}) the sum of the \textit{estimation erro}r and the \textit{optimization error}:
\begin{equation}\label{eq:risk_excess2}
L(\widetilde{\sigma}_N)-L^*=\left\{ L(\widehat{\sigma}_N)-L^*\right\} + \left\{ L(\widetilde{\sigma}_N)-L(\widehat{\sigma}_N)\right\}.
\end{equation}
Bounds should be ideally universal but it would be also desirable to understand the circumstances under which the risk excess \eqref{eq:risk_excess2} is small (\textit{cf} margin conditions in the classification problem).
\bigskip

\noindent {\bf Kendall's $\tau$ distance.} It coincides with the number of discording pairs: $\forall (\sigma,\sigma')\in \mathfrak{S}_n^2$,
\begin{equation}
d_{\tau}(\sigma,\sigma')=\sum_{i<j}\mathbb{I}\{(\sigma(i)-\sigma(j))\cdot (\sigma'(i)-\sigma'(j))<0   \}.
\end{equation}
Up to the factor $n(n-1)/2$, it can be rewritten as an expectation taken w.r.t. a random variable $\e=(\i,\j)$ defined on $(\Omega,\; \mathcal{F},\; \mathbb{P})$, uniformly distributed on the set of all pairs of instances $\mathcal{E}_n=\{ (i,j),\; i<j\}$ and independent from $\Sigma$:
\begin{equation}
d_{\tau}(\sigma,\sigma')=\frac{n(n-1)}{2}\mathbb{E}\left[ l_{\e}(\sigma,\sigma') \right],
\end{equation}
where we set $l_e(\sigma,\sigma')=\mathbb{I}\{ (\sigma(i)-\sigma(j))\cdot (\sigma'(i)-\sigma'(j))<0   \}$ for all $e=(i,j)\in \mathcal{E}_n$. We also introduce the quantities $p_{i,j}=\mathbb{P}\{\Sigma(i)<\Sigma(j)  \}=p_{j,i}$ for all $e=(i,j)\in \mathcal{E}_n$. 

 \begin{remark}\label{Rk:Mallows}{\sc (Mallows model)} The Mallows model introduced in the seminal contribution \cite{Mallows57} is an unimodal distribution $P_{\theta_0}$ on $\mathfrak{S}_n$ parametrized by $\theta=(\sigma_0, \phi_0)\in \mathfrak{S}_n\times (0,1]$: $\forall \sigma \in \mathfrak{S}_n$,
 \begin{equation}\label{eq:Mallows}
 P_{\theta_0}(\sigma)=\frac{1}{Z_0}\phi_0^{ d_{\tau}(\sigma_0,\sigma) },
 \end{equation}
 where $Z_0=\sum_{\sigma\in \mathfrak{S}_n} \phi_0^{ d_{\tau}(\sigma_0,\sigma) }$ is a normalization constant. One may easily show that $Z_0$ is independent from $\sigma_0$ and that $Z_0=\prod_{i=1}^{n-1}\sum_{j=0}^i \phi_0^j$. The permutation $\sigma_0$ of reference is the mode of distribution $ P_{\theta_0}$, as well as its unique median. Observe in addition that the smallest the parameter $\phi_0$, the spikiest the distribution $ P_{\theta_0}$. In contrast, $ P_{\theta_0}$ is the uniform distribution on $\mathfrak{S}_n$ when $\phi_0=1$. For any $(i,j)\in \mathcal{E}_n$, we have:
 \begin{equation}
 \mathbb{P}_{\Sigma\sim P_{\theta_0}}\left\{  \left(\Sigma(i)-\Sigma(j)  \right)\left( \sigma_0(i)-\sigma_0(j) \right)<0    \right\}=\frac{\phi_0}{1+\phi_0}\leq \frac{1}{2}.
 \end{equation}
 Equipped with the notations introduced above, for all $(i,j)\in \mathcal{E}_n$, we thus have:
 \begin{equation}\label{eq:Mallows_pair}
 p_{i,j}=\frac{\phi_0}{1+\phi_0}\mathbb{I}\{\sigma_0(i)>\sigma_0(j)  \}+ \frac{1}{1+\phi_0}\mathbb{I}\{\sigma_0(i)<\sigma_0(j)   \}.
 \end{equation}
\end{remark}
\bigskip


Integrating the conditional expectation upon $\e$, one may write the risk related to a permutation candidate $\sigma\in\mathfrak{S}_n$ as
\begin{equation}
\label{eq:loss-1}
L(\sigma)=\frac{n(n-1)}{2}\mathbb{E}\left[  p_{\i,\j}\mathbb{I}\{\sigma(\i)>\sigma(\j) \}  + (1-p_{\i,\j})\mathbb{I}\{\sigma(\i)<\sigma(\j) \}\right].
\end{equation}
Hence, the minimum inside the expectation is achieved for $\sigma$ such that: $\forall i<j$,
\begin{equation}\label{eq:min_strong}
\begin{array}{lll}
\sigma(i)<\sigma(j) & \text{ if } & p_{i,j}\geq 1/2,\\
\sigma(i)>\sigma(j) & \text{ otherwise.} &
\end{array}
\end{equation}
In particular, we have $L^*\geq \sum_{i<j}\min\{p_{i,j},\; 1-p_{i,j}  \}$.
If a permutation fulfills this property, it is of course a median for $P$. If in addition none of the $p_{i,j}$ is equal to $1/2$, the median is necessarily unique and defined as: $\forall i\in\{1,\; \ldots,\; n  \}$,
\begin{equation}\label{eq:Kendall_med}
\sigma^*(i)=1+\sum_{i<k}\mathbb{I}\{p_{i,k}<\frac{1}{2}  \}+\sum_{l<i}\mathbb{I}\{p_{l,i}>\frac{1}{2}  \}.
\end{equation}
Observe in addition that condition \eqref{eq:min_strong} can be then expressed as: $\forall i<j$,
\begin{equation}
p_{i,j}>\frac{1}{2} \Leftrightarrow \sum_{i<k}\mathbb{I}\{p_{i,k}<\frac{1}{2}  \}+\sum_{l<i}\mathbb{I}\{p_{l,i}>\frac{1}{2}  \}< \sum_{j<k}\mathbb{I}\{p_{j,k}<\frac{1}{2}  \}+\sum_{l<j}\mathbb{I}\{p_{l,j}>\frac{1}{2}  \}.
\end{equation}
The following result provides a necessary and sufficient condition for the mapping $\sigma^*$ defined by \eqref{eq:Kendall_med} to be the median for $P$ w.r.t. Kendall $\tau$ distance when no $p_{i,j}$ is equal to $1/2$.

\begin{proposition}\label{prop:Kendall_opt} Suppose that $p_{i,j}\neq 1/2$ for all $i<j$. Then, the mapping $\sigma^*$ defined by \eqref{eq:Kendall_med} belongs to $\mathfrak{S}_N$ and is the unique median for $P$ w.r.t. distance $d_{\tau}$ if and only if the pairwise probabilities satisfy the stochastic transitivity property, \textit{i.e}: for all $i<j<k$,
\begin{equation}\label{transitivity} p_{i,j}>\frac{1}{2} \text{ and } p_{j,k}>\frac{1}{2} \Rightarrow p_{i,k}>\frac{1}{2}.
\end{equation}
In this case, we have:
$$
L^*=\sum_{i<j}\min\{p_{i,j},\; 1-p_{i,j}  \}=\frac{n(n-1)}{2}\mathbb{E}_{(\i,\j)}[ \min\{p_{\i,\j},\; 1-p_{\i,\j}  \} ].
$$
\end{proposition}
\begin{proof} The pairwise probabilities can be represented as a complete directed graph on the $n$ items. An edge is drawn from $i$ to $j$ whenever $p_{i,j}>\frac{1}{2}$. We thus consider a complete directed graph.

\noindent {\bf Sufficiency.} Suppose we have transitivity (\ref{transitivity}). The graph is a directed acyclic graph and since we have non-reflexivity, this graph induces a partial order on the items (cf {\it topological sorting}). We thus know that there is a strict total order on the vertices, which corresponds to the mapping ($\sigma^{*}(i)=1+\text{in-degree(i)}$).
\medskip

\noindent {\bf Necessity.} Suppose that the mapping $\sigma^*$ defined by \eqref{eq:Kendall_med} belongs to $\mathfrak{S}_N$: $\exists i_1, \dots, i_n$, such that $1=\sigma^*(i_1)> \dots > \sigma^*(i_n)=n$. We denote $S_{i_k}$ the union of the set of vertices which have an edge pointing towards $i_k$ and the singleton $\lbrace{i_k}\rbrace$. We thus have $\llbracket n \rrbracket = S_{i_n} \supsetneq \dots \supsetneq S_{i_2} \supsetneq S_{i_1}= \lbrace{i_1}\rbrace$. Let $(i,j) \in \llbracket n \rrbracket$ such that $p_{i,j}>\frac{1}{2} \text{ and } p_{j,k}>\frac{1}{2}$. Thus $j \not\in S_i$ so $|S_i|<|S_j|$ and $k \not\in S_j$ so $|S_j|<|S_k|$. We have $|S_i|<|S_j|<|S_k|$ which means that $S_k\supsetneq S_{i}$, so there is an edge from $i$ to $k$ and $p_{ik}>\frac{1}{2}$.
\end{proof}
\begin{remark}
Observe that, as soon as $\phi_0<1$, the Mallows model $P_{\theta_0}$ fulfills the stochastic transitivity property. Indeed, it follows from $\eqref{eq:Mallows_pair}$ that for any $i<j$, we have: $p_{i,j}>1/2 \Leftrightarrow   \sigma_0(i)<\sigma_0(j)$. In addition, we have in this case:
\begin{equation}\label{eq:Mallows_disp}
L^*_{P_{\theta_0}}=\frac{n(n-1)}{2}\frac{\phi_0}{1+\phi_0}.
\end{equation}
\end{remark}
\bigskip


\begin{center} {\bf Empirical Risk Minimization - The general case.}
\end{center}
\medskip

For any empirical median $\widehat{\sigma}_N$, Hoeffding inequality combined with \eqref{eq:classic} and the union bound yields: $\forall t>0$,
\begin{eqnarray*}
\mathbb{P}\left\{L(\widehat{\sigma}_N)-L^*  >t  \right\}&\leq & \sum_{\sigma\in \mathfrak{S}_n}\mathbb{P}\left\{ \frac{1}{N}\left\vert\sum_{i=1}^N\left\{ d(\Sigma_i, \sigma)-L(\sigma)    \right\}  \right\vert     >t/2 \right\}\\
&\leq & 2n!\exp\left(-\frac{Nt^2}{2\vert\vert d\vert\vert_{\infty}^2}  \right),
\end{eqnarray*}
where $\vert\vert d\vert\vert_{\infty}=\max_{(\sigma,\sigma')\in \mathfrak{S}_n^2}d(\sigma,\sigma')$. This immediately leads to the universal bound for ERM stated below.

\begin{proposition}
Consider a distance $d$ on the symmetric group $\mathfrak{S}_n$. Let $\widehat{\sigma}_N$ be any empirical median based on i.i.d. training data $\Sigma_1,\; \ldots,\; \Sigma_N$ w.r.t. distance $d$. For any $\delta\in (0,1)$, we have with probability at least $1-\delta$: $\forall N\geq 1$,
\begin{equation}\label{eq:ERM_bound_gen}
L(\widehat{\sigma}_N)-L^*\leq \sqrt{2}\vert\vert d\vert\vert_{\infty}\sqrt{\frac{\log(2n! /\delta)}{N}}.
\end{equation}
\end{proposition}

\bigskip

\begin{center}
 {\bf Empirical Risk Minimization - The Kendall's $\tau$ case.}
\end{center}
\medskip


The bound \eqref{eq:ERM_bound_gen} naturally applies to the specific case $d=d_{\tau}$, with $\vert\vert d_{\tau}\vert\vert_{\infty}=n(n-1)/2$. Recall in addition that, if none of the pairwise probabilities $p_{i,j}$ is equal to $1/2$ and the stochastic transitivity property \eqref{transitivity} is fulfilled, the (unique) median can be expressed as a function of the $p_{i,j}$'s only, by virtue of Proposition \ref{prop:Kendall_opt}. 
\medskip

\noindent {\bf Plug-in empirical Kendall's $\tau$ median.} This may provide a motivation for considering \textit{plug-in} empirical medians, defined as follows. For any $(i,j)\in \mathcal{E}_n$, let $\widehat{p}_{i,j}\neq 1/2$ be an estimate of $p_{i,j}$ such that the collection $(p_{i,j})_{(i,j)\in \mathcal{E}_n}$ satisfies the property \eqref{transitivity}. The mapping
\begin{equation}\label{eq:Kendall_emp_med}
\widehat{\sigma}^*(i)=1+\sum_{i<k}\mathbb{I}\{\widehat{p}_{i,k}<\frac{1}{2}  \}+\sum_{l<i}\mathbb{I}\{\widehat{p}_{l,i}>\frac{1}{2}  \} \text{ with } i\in\{1,\; \ldots,\; n  \}
\end{equation}
thus defines a permutation (\textit{cf} Proposition \ref{prop:Kendall_opt}).  In the particular case where the empirical distribution $\widehat{P}_N=(1/N)\sum_{m=1}^N\delta_{\Sigma_m}$ fulfills the pairwise stochastic transitivity condition \eqref{transitivity}, one may consider the empirical estimates for all $i<j$,
\begin{equation}\label{eq:emp_pair}
\widehat{p}_{i,j}=\frac{1}{N}\sum_{m=1}^N\mathbb{I}\{ \Sigma_m(i)<\Sigma_m(j)  \}.
\end{equation}
In this case, the plugin empirical median \eqref{eq:Kendall_emp_med} is the Kendall's $\tau$ median related to $\widehat{P}_N$.\\


\begin{remark}
The mapping $\sigma^{*}$ correspond to the Kemeny/Borda/Copeland consensus. Since it minimizes the loss with the Kendall's tau distance it is a Kemeny consensus. Then, by (\ref{eq:Kendall_med}) the obtained order is the one obtained by Copeland method. Finally, the Borda score for an alternative $i$ is $s(i)= n - \sum\limits_{j\ne i}\mathbb{I}\{p_{j,i}>\frac{1}{2}  \}= n+1-\sigma^*(i)$.
\end{remark}
\medskip

Consider a plug-in median $\widehat{\sigma}^*$, based on stochastically transitive pairwise probabilities $\widehat{p}_{i,j}$, $(i,j)\in \mathcal{E}_n$. Observe that, by virtue of \eqref{eq:excess_Kendall}, we have:
\begin{equation}\label{eq:excess_plugin}
L(\widehat{\sigma}^*)-L^*=\frac{n(n-1)}{2}\mathbb{E}_{\i,\j}\left[ \vert p_{\i,\j}-1/2 \vert \mathbb{I}\{ (\widehat{p}_{\i,\j}-1/2)(p_{\i,\j}-1/2)<0  \}  \right].
\end{equation}
Observe that:
\begin{equation*}
\widehat{p}_{\i,\j}<1/2\text{ and } p_{\i,\j}>1/2 \Rightarrow \vert p_{\i,\j}-1/2 \vert\leq \vert p_{\i,\j}- \widehat{p}_{\i,\j}\vert
\end{equation*}
and 
\begin{equation*}
\widehat{p}_{\i,\j}>1/2\text{ and } p_{\i,\j}<1/2 \Rightarrow \vert p_{\i,\j}-1/2 \vert\leq \vert p_{\i,\j}- \widehat{p}_{\i,\j}\vert.
\end{equation*}
Combined with \eqref{eq:excess_plugin}, this yields:
\begin{equation}\label{eq:plugin_bound}
L(\widehat{\sigma}^*)-L^*\leq \frac{n(n-1)}{2}\mathbb{E}_{\i,\j}\left[ \vert p_{\i,\j}-\widehat{p}_{\i,\j} \vert  \right].
\end{equation}
\medskip

\noindent {\bf A refined bound for the plugin median.} It hus follows from bound \eqref{eq:plugin_bound} that, for any $t > 0$,
\begin{align*}
\mathbb{P}\Big\{L(\widehat{\sigma}_{N})-L^{\ast} > t\Big\} 
&\leq \mathbb{P}\Big\{\binom{n}{2}\mathbb{E}_{\i,\j}\left[ \vert p_{\i,\j}-\widehat{p}_{\i,\j} \vert  \right] > t\Big\}\\ 
&= \mathbb{P}\Big\{\sum_{1\leq i < j\leq n}\vert p_{i,j}-\widehat{p}_{i,j} \vert > t\Big\}\\
&\leq \mathbb{P}\Big\{\bigcup_{1\leq i < j\leq n}\Big\{\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{\binom{n}{2}}\Big\}\Big\}\\
&\leq \sum_{1\leq i < j \leq n}\mathbb{P}\Big\{\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{\binom{n}{2}}\Big\}.
\end{align*}
Now, in the case where the $\widehat{p}_{i,j}$'s are given by \eqref{eq:emp_pair}, Hoeffding's inequality applied to $\widehat{p}_{i,j} = (1/N)\sum_{t=1}^{N}\mathbb{I}\{\Sigma_{t}(i) < \Sigma_{t}(j)\}$ gives
\begin{equation}
\label{eq:Hoeffding-pairwise}
\mathbb{P}\Big\{ \vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{\binom{n}{2}} \Big\} \leq 2e^{-2N(t/\binom{n}{2})^{2}},
\end{equation}
so that 
\begin{equation}
\mathbb{P}\Big\{L(\widehat{\sigma}_{N})-L^{\ast} > t\Big\} \leq 2\binom{n}{2}e^{-\frac{2Nt^{2}}{\binom{n}{2}^{2}}}.
\end{equation}
Setting $\delta = 2\binom{n}{2}e^{-\frac{2Nt^{2}}{\binom{n}{2}^{2}}}$ one obtains that, with probability greater than $1-\delta$,
\begin{equation}
\label{eq:Hoeffding-bound}
L(\widehat{\sigma}_{N}) - L^{\ast} \leq  \binom{n}{2}\sqrt{\frac{\log(n(n-1)/\delta)}{2N}}.
\end{equation}
Instead of using Hoeffding's inequality to obtain \eqref{eq:Hoeffding-pairwise} we can use Bernstein inequality to keep the variance in the bound and make it small with some assumption:
\begin{equation}
\label{eq:Bernstein-pairwise}
\mathbb{P}\Big\{ \vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2\binom{n}{2}} \Big\} \leq 2\exp\left(-\frac{\frac{1}{2}N(t/2\binom{n}{2})^{2}}{p_{i,j}(1-p_{i,j}) + \frac{1}{3}(t/2\binom{n}{2})}\right).
\end{equation}
For instance under the margin condition
\begin{equation}\label{eq:margin_cond}
 \exists h>0 \text{ such that }\vert p_{i,j}-1/2\vert \geq h \text{ for all } (i,j)\in\mathcal{E}_n,
\end{equation}
 one has $p_{i,j}(1-p_{i,j}) = 1/4 - (p_{i,j}-1/2)^{2} \leq 1/4 - h^{2}$. Therefore, for $\delta >0$, one has with probability greater than $1-\delta$
\begin{equation}
\label{eq:Bernstein-bound}
L(\widehat{\sigma}_{N}) - L^{\ast} \leq  2\binom{n}{2}\left[\sqrt{\frac{2(1/4-h^{2})\log(n(n-1)/\delta)}{N}} + \frac{2\log(n(n-1)/\delta)}{3N}\right].
\end{equation}
Notice that the first term in \eqref{eq:Bernstein-bound} is equal to $2\sqrt{1/4 - h^{2}}$ times the term in \eqref{eq:Hoeffding-bound}. This factor being $\leq 1$ for any $0 \leq h \leq 1/2$, the bound \eqref{eq:Bernstein-bound} is always asymptotically better than the bound \eqref{eq:Hoeffding-bound} and much better when $h$ is close to $1/2$.  The major drawback of the plugin approach lies in the fact that it may happen that $\widehat{P}_N$ does not fulfill \eqref{transitivity}, even if $P$ does. 
\medskip

\noindent {\bf Fast Rate for ERM in the Kendall's $\tau$ case.} Suppose from now on that the \textit{margin condition} \eqref{eq:margin_cond} and condition \eqref{transitivity} are fulfilled by the underlying probability distribution $P$. Observe first that the  risk of any median candidate $\sigma$ can be written as:
\begin{multline*}
L(\sigma)=\frac{n(n-1)}{2}\times\\ 
\left(1+\mathbb{E}\left[  \left(p_{\i,\j}-\frac{1}{2}\right)\mathbb{I}\{\sigma(\i)>\sigma(\j) \}  + \left(\frac{1}{2}-p_{\i,\j}\right)\mathbb{I}\{\sigma(\i)<\sigma(\j) \} \right]\right).
\end{multline*}
Its excess of risk can be thus expressed as
\begin{eqnarray}\label{eq:excess_Kendall}
L(\sigma)-L^*&=&\frac{n(n-1)}{2}\mathbb{E}\left[ \vert p_{\i,\j}-1/2 \vert \mathbb{I}\{ (\sigma(\j)-\sigma(\i))(\sigma^*(\j)-\sigma^*(\i))<0  \}  \right]\\
&=& \sum_{i<j}\mathbb{E}\left[ \vert p_{i,j}-1/2 \vert \mathbb{I}\{ (\sigma(j)-\sigma(i))(\sigma^*(j)-\sigma^*(i))<0  \}  \right].
\end{eqnarray}
Notice next that:
\begin{eqnarray}\label{eq:var_control}
Var\left(d_{\tau}(\Sigma, \sigma)-d_{\tau}(\Sigma,\sigma^*)  \right)&\leq& 2^{n(n-1)/2-1}\sum_{i<j}Var\left(l_{i,j}(\Sigma,\sigma)-  l_{i,j}(\Sigma,\sigma^*) \right)\\
&\leq& 2^{n(n-1)/2-1}\sum_{i<j}\mathbb{E}\left[ \mathbb{I}\{ (\sigma(j)-\sigma(i))\cdot(\sigma^*(j)-\sigma^*(i) )<0  \}  \right]\nonumber\\
&\leq& \frac{2^{n(n-1)/2-1}}{h}\left( L(\sigma)-L^*  \right).\nonumber
\end{eqnarray}
In addition, by virtue of Bernstein's inequality, for any $\sigma\in \mathfrak{S}_n$, we have with probability at least $1-\delta$
\begin{multline*}
 L(\sigma)-L^*  \leq \widehat{L}_N(\sigma)-\widehat{L}_N(\sigma^*) +\sqrt{2Var \left(  \widehat{L}_N(\sigma)-\widehat{L}_N(\sigma^*)\right)\log(1/\delta)}+\frac{2\log(1/\delta)}{3N}\\
\leq \widehat{L}_N(\sigma)-\widehat{L}_N(\sigma^*) +\sqrt{2 \frac{2^{n(n-1)/2-1}}{Nh}\left( L(\sigma)-L^*  \right)\log(1/\delta)}+\frac{2\log(1/\delta)}{3N}.
\end{multline*}
The union bound over $\mathfrak{S}_n$ then implies that, with probability larger than $1-\delta$, we have: $\forall \sigma\in \mathfrak{S}_n$,
\begin{multline*}
 L(\sigma)-L^*  \leq \widehat{L}_N(\sigma)-\widehat{L}_N(\sigma^*) +\sqrt{2 \frac{2^{n(n-1)/2-1}}{Nh}\left( L(\sigma)-L^*  \right)\log(n!/\delta)}\\+\frac{2\log(n!/\delta)}{3N}.
\end{multline*}
Since $\widehat{L}_N(\widehat{\sigma}_N)-\widehat{L}_N(\sigma^*)\leq 0$ with probability one for any empirical median $\widehat{\sigma}_N$, we deduce that, with probability at least $1-\delta$,
\begin{equation*}
 L(\widehat{\sigma}_N)-L^*  \leq  \sqrt{2 \frac{2^{n(n-1)/2-1}}{Nh}\left( L(\widehat{\sigma}_N)-L^*  \right)\log(n!/\delta)}+\frac{2\log(n!/\delta)}{3N}.
\end{equation*}

We deduce the following rate bound, of the order $O_{\mathbb{P}}(1/N)$, for the risk excess of any empirical median in the Kendall's $\tau$ case. With probability larger than $1-\delta$: $\forall N\geq 1$,
\begin{equation}\label{eq:Bern_bound}
 L(\widehat{\sigma}_N)-L^*  \leq 2 \frac{2^{n(n-1)/2-1}}{Nh}\log(n!/\delta).
\end{equation}
Note also that \eqref{eq:var_control} can be possibly refined in order to avoid the constant of order $O(\exp(n^2 ))$.
\bigskip


\bigskip

\noindent {\bf Fast rate with low noise.} Suppose $p$ satisfies (\ref{transitivity}) and the low noise condition : $\forall (i,j)$, $\vert p_{i,j}- \frac{1}{2} \vert \ge h$. If $\vert p_{\i,\j}-\widehat{p}_{\i,\j} \vert \le t $ with $t <h$, then $\widehat{p}$ also satisfies (\ref{transitivity}) and $(p_{\i,\j}- \frac{1}{2})(\widehat{p}_{\i,\j}- \frac{1}{2} ) >0$.

\begin{proof}
	Let $(i,j) \in \llbracket n \rrbracket$ such that $\widehat{p}_{i,j}>\frac{1}{2} \text{ and } \widehat{p}_{j,k}>\frac{1}{2}$. We have: $p_{i,j} \ge \widehat{p}_{i,j} - t > \frac{1}{2} - t >  \frac{1}{2} - h$, so $p_{i,j} \ge \frac{1}{2}+h > \frac{1}{2}$. Similarly, $p_{j,k} > \frac{1}{2}$ and since $p$ satisfies (\ref{transitivity}), $p_{i,k}>\frac{1}{2}$ so $p_{i,k}\ge \frac{1}{2}+h$. Now, $\widehat{p}_{i,k} \ge p_{i,k} - t \ge \frac{1}{2}+h -t > \frac{1}{2}$ so $\widehat{p}$ satisfies (\ref{transitivity}). Then, suppose $p_{i,j}>\frac{1}{2}$ so $p_{i,j}\ge\frac{1}{2}+h$. We thus have $ \widehat{p}_{i,j}\ge p_{i,j}- t \ge  \frac{1}{2}+h -t >\frac{1}{2}$.  \end{proof}

Hoeffding gives:
\begin{equation}
\mathbb{P}\Big\{ \vert p_{i,j}-\widehat{p}_{i,j} \vert > t \Big\} \leq 2e^{-2Nt^2}
\end{equation}



\bigskip



\noindent {\bf Minimax lower bounds.} We shall now establish a lower bound for the quantity
\begin{equation}\label{eq:minimax}
\mathcal{R}_N\overset{def}{=}\inf_{\sigma_N}\sup_P \mathbb{E}_P\left[L_P(\sigma_N)-L_P^*  \right],
\end{equation}
where the supremum is taken over all probability distributions on $\mathfrak{S}_n$ and the infimum is taken over all empirical median candidate $\sigma_N=\sigma_N(\Sigma_1,\; \ldots,\, \Sigma_N)$ in $\mathfrak{S}_n$ based on the observation of $N$ independent realizations of $P$: $\Sigma_1,\; \ldots,\, \Sigma_N$.
Consider two Mallows models $P_{\theta_0}$ and $P_{\theta_1}$ where $\theta_k=(\sigma^*_k,\phi_k)\in \mathfrak{S}_n\times (0,1)$ and $\sigma^*_0\neq \sigma^*_1$. We clearly have:
\begin{eqnarray*}
\mathcal{R}_N&\geq& \inf_{\sigma_N}\max_{k=0,\; 1} \mathbb{E}_{P_{\theta_k}}\left[L_{P_{\theta_k}}(\sigma_N)-L_{P_{\theta_k}}^*  \right]\\
&= & \inf_{\sigma_N}\max_{k=0,\; 1}\sum_{i<j} \mathbb{E}_{P_{\theta_k}}\left[\frac{\vert \phi_k-1 \vert}{2(1+\phi_k)}\mathbb{I}\{ (\sigma_N(i)-\sigma_N(j)(\sigma^*(i)-\sigma_k^*(j))<0  \} \right]\\
&\geq & \frac{1}{4} \inf_{\sigma_N}\max_{k=0,\; 1}\vert \phi_k-1 \vert \mathbb{E}_{P_{\theta_k}}\left[d_{\tau} (\sigma_N,\sigma_k^*)  \right],
\end{eqnarray*}
using \eqref{eq:excess_Kendall} and \eqref{eq:Mallows_pair}. Set $ \Delta=d_{\tau}(\sigma^*_0,\sigma^*_1)\geq 1$, and consider the test statistic related to $\sigma_N$:
\begin{equation}
\psi(\Sigma_1,\; \ldots,\; \Sigma_N)=\mathbb{I}\{ d_{\tau}(\sigma_N,\sigma^*_1)\leq d_{\tau}(\sigma_N,\sigma^*_0)\ \}.
\end{equation}
If $\psi=1$, by triangular inequality, we have:
\begin{equation}
\Delta \leq d_{\tau}(\sigma_N,\sigma^*_0)+d_{\tau}(\sigma_N,\sigma^*_1)\leq 2d_{\tau}(\sigma_N,\sigma^*_0).
\end{equation}
Hence, we have
\begin{equation}
\mathbb{E}_{P_{\theta_0}}\left[d_{\tau} (\sigma_N,\sigma_0^*)  \right]\geq \mathbb{E}_{P_{\theta_0}}\left[d_{\tau} (\sigma_N,\sigma_0^*) \mathbb{I}\{ \psi=+1 \} \right]\geq \frac{\Delta}{2}\mathbb{P}_{\theta_0}\{\psi=+1  \}
\end{equation}
and similarly
\begin{equation}
\mathbb{E}_{P_{\theta_1}}\left[d_{\tau} (\sigma_N,\sigma_1^*)  \right]\geq \mathbb{E}_{P_{\theta_1}}\left[d_{\tau} (\sigma_N,\sigma_1^*) \mathbb{I}\{ \psi=0 \} \right]\geq \frac{\Delta}{2}\mathbb{P}_{\theta_1}\{\psi=0  \}.
\end{equation}
Bounding by below the maximum by the average, we have:
\begin{multline*}
 \inf_{\sigma_N}\max_{k=0,\; 1}\vert \phi_k-1 \vert \mathbb{E}_{P_{\theta_k}}\left[d_{\tau} (\sigma_N,\sigma_k^*)  \right]\geq  \\ 
 \inf_{\sigma_N}\frac{\Delta}{2} \frac{1}{2}\left\{ \vert \phi_1-1 \vert \mathbb{P}_{\theta_1}\{\psi=0  \}  + \vert \phi_0-1 \vert \mathbb{P}_{\theta_0}\{\psi=1  \} \right\}\geq \\
 \inf_{\sigma_N}\frac{\Delta}{4}\min_{k=0,\; 1}\vert \phi_k-1 \vert \left\{ \mathbb{P}_{\theta_1}\{\psi=0  \}  + \mathbb{P}_{\theta_0}\{\psi=1  \} \right\}\geq\\
\frac{\Delta}{4}\min_{k=0,\; 1}\vert \phi_k-1 \vert \left\{ \mathbb{P}_{\theta_1}\{\psi^*=0  \}  + \mathbb{P}_{\theta_0}\{\psi^*=1  \} \right\},
 \end{multline*}
where the last inequality follows from a standard Neyman-Pearson argument, denoting by
\begin{equation}
\psi^*(\Sigma_1,\; \ldots,\; \Sigma_N)=\mathbb{I}\left\{\prod_{i=1}^N\frac{P_{\theta_1}(\Sigma_i)}{P_{\theta_0}(\Sigma_i)}\geq 1\right\}
\end{equation}
the likelihood ratio test statistic. We deduce that 
\begin{multline*}
\mathcal{R}_N\geq \frac{\Delta}{4}\min_{k=0,\; 1}\vert \phi_k-1 \vert \sum_{\sigma_i\in \mathfrak{S}_N,\; 1\leq i\leq N}\min\left\{\prod_{i=1}^NP_{\theta_0}(\sigma_i),\;  \prod_{i=1}^NP_{\theta_1}(\sigma_i) \right\}\\
\geq \frac{\Delta}{8}\min_{k=0,\; 1}\vert \phi_k-1 \vert e^{-NK(P_{\theta_0}\vert\vert P_{\theta_1})},
\end{multline*}
where $K(P_{\theta_0}\vert\vert P_{\theta_1})=\sum_{\sigma\in \mathfrak{S}_N}P_{\theta_0}(\sigma)\log(P_{\theta_0}(\sigma)/P_{\theta_1}(\sigma))$ denotes the Kullback-Leibler divergence. In order to establish a minimax lower bound of order $1/\sqrt{N}$, one should choose $\theta_0$ and $\theta_1$ so that, for $k\in\{0,\; 1  \}$, $\phi_k\rightarrow 1$ and $K(P_{\theta_0}\vert\vert P_{\theta_1})\rightarrow 0$ as $N\rightarrow +\infty$ at appropriate rates. Take
\begin{eqnarray}
\phi_0=1-1/\sqrt{N} \text{ and }  \phi_1=(1-1/\sqrt{N})(1-1/N).
\end{eqnarray}
Observe that we then have $\min_{k=0,\; 1}\vert\phi_k-1 \vert\geq 2/\sqrt{N}$ and
\begin{eqnarray*}
K(P_{\theta_0}\vert\vert P_{\theta_1})&=&\sum_{\sigma\in \mathfrak{S}_n}\frac{1}{Z_0}\phi_0^{d_{\tau}(\sigma_0,\sigma)}\log\left(  \frac{Z_1}{Z_0}\frac{\phi_0^{d_{\tau}(\sigma_0,\sigma)}}{\phi_1^{d_{\tau}(\sigma_1,\sigma)}}\right)\\
&=&\frac{Z_1}{Z_0} +\sum_{\sigma\in \mathfrak{S}_n}\frac{1}{Z_0}\phi_0^{d_{\tau}(\sigma_0,\sigma)}\log\left(  \left(\frac{\phi_0}{\phi_1}\right)^{d_{\tau}(\sigma_0,\sigma)} \frac{1}{\phi_1^{d_{\tau}(\sigma_1,\sigma)-d_{\tau}(\sigma_0,\sigma)}}\right)\\
&=& \frac{Z_1}{Z_0} + \log\left(\frac{\phi_0}{\phi_1}\right)\sum_{\sigma\in \mathfrak{S}_n}P_{\theta_0}(\sigma) d_{\tau}(\sigma_0,\sigma)\\
&+& \log\left(\frac{1}{\phi_1}\right)\sum_{\sigma\in \mathfrak{S}_n}P_{\theta_0}(\sigma) \left(-d_{\tau}(\sigma_0,\sigma)+d_{\tau}(\sigma_1,\sigma)\right)\\
&=&\frac{Z_1}{Z_0} +\log (\phi_0/\phi_1)L_{P_{\theta_0}}(\sigma_0)+\log(1/\phi_1)(L_{P_{\theta_0}}(\sigma_1)-L_{P_{\theta_0}}(\sigma_0))\\
&=&\frac{Z_1}{Z_0} +\log (\phi_0/\phi_1)\frac{n(n-1)}{2}\frac{\phi_0}{1+\phi_0}+\log(1/\phi_1)\frac{\vert \phi_0-1 \vert}{2(\phi_0+1)}\Delta. 
%&\leq&\sum_{\sigma\in \mathfrak{S}_n}\frac{1}{Z_0}\phi_0^{d_{\tau}(\sigma_0,\sigma)}\left[ \log \left(\frac{Z_1}{Z_0}\right)+{d_{\tau}(\sigma_0,\sigma)}\log\left(\frac{\phi_0}{\phi_1}\right)+ \left(d_{\tau}(\sigma_0,\sigma)-d_{\tau}(\sigma_1,\sigma)\right) \log \left(\phi_1\right)\right]\\
%&\leq& \log \left(\frac{Z_1}{Z_0}\right)+\frac{n(n-1)}{2}\log\left(\frac{\phi_0}{\phi_1}\right)+ d_{\tau}(\sigma_0,\sigma_1) \log \left(\phi_1\right)\\
%&\leq& \log \left(\frac{Z_1}{Z_0}\right)+\frac{n(n-1)}{2}\log\left(\frac{\phi_0}{\phi_1}\right)\\
\end{eqnarray*}


\noindent The last inequality holds because $\phi_1 \le 1$ so $\log(\phi_1) \le 0$.
Then we have:
\begin{equation*}
\log \left(\frac{Z_1}{Z_0}\right)	=\log\left( \frac{\prod_{i=1}^{n-1} \sum_{j=0}^{i} \phi_1^j}{\prod_{i=1}^{n-1} \sum_{j=0}^{i} \phi_0^j}\right)= \sum_{i=1}^{n-1} \log \left( \frac{\sum_{j=0}^{i} \phi_1^j}{\sum_{j=0}^{i} \phi_0^j} \right)
\end{equation*}


\noindent We can use the log-sum inequality: for $a_1, \dots, a_n, b_1, \dots, b_n$ non-negative numbers:
\begin{equation*}
 \sum_{i=1}^n a_i\log\frac{a_i}{b_i}\geq \left(\sum_{i=1}^{n} a_i\right) \log\frac{\sum_{i=1}^{n} a_i}{\sum_{i=1}^{n} b_i}
\end{equation*}

\begin{eqnarray*}
\log \left( \frac{\sum_{j=0}^{i} \phi_1^j}{\sum_{j=0}^{i} \phi_0^j} \right) & \le & \frac{1}{\sum_{j=0}^{i} \phi_1^j} \sum_{j=0}^{i} \phi_1^j \log \left( \frac{\phi_1^j}{\phi_0^j} \right)\\
& \le &  \frac{1}{\sum_{j=0}^{i} \phi_1^j} \sum_{j=0}^{i} \phi_1^j j \log \left( \frac{\phi_1}{\phi_0} \right)\\
& \le &  i \log \left( \frac{\phi_1}{\phi_0} \right)
\end{eqnarray*}

\noindent So:

\begin{equation*}
\log \left(\frac{Z_1}{Z_0}\right) \le \sum_{i=1}^{n-1} i  \log \left( \frac{\phi_1}{\phi_0} \right) = \frac{n(n-1)}{2} \log \left( \frac{\phi_1}{\phi_0} \right)
\end{equation*}

\noindent Finally:

\begin{equation*}
K(P_{\theta_0}\vert\vert P_{\theta_1}) \le n(n-1) \log \left( \frac{\phi_1}{\phi_0} \right) = - n(n-1) \log \left( 1 - \frac{1}{N} \right) = \mathcal{O} (\frac{1}{N} ).
\end{equation*}
\bigskip

\noindent {\bf Inequalities}. Generally, we have for any $\sigma_1,\sigma_2 \in \mathfrak{S}_n$ and probability $f$: 
\begin{align}
L(\sigma_1)-L(\sigma_2)&= \sum_{\pi \in \mathfrak{S}_n} d(\sigma_1, \pi) f(\pi) -\sum_{\pi \in \mathfrak{S}_n} d(\sigma_2, \pi) f(\pi) \\
L(\sigma_1)-L(\sigma_2)&\le d(\sigma_1, \sigma_2)
\label{up_distance}
\end{align} 
We use a triangular inequality and the fact that probabilities sum to one.\\
Firstly, we can upper bound the {\it estimation error} (according to [Sibony15]) since $\widehat{\sigma}_N= argmin_{\sigma \in \mathfrak{S}_n}\widehat{L}_N(\sigma)$ and $\sigma^{*}=argmin_{\sigma \in \mathfrak{S}_n}L(\sigma)$:
\begin{equation*}
L(\widehat{\sigma}_N)-L^{*}\le 2 \|T(P-\widehat{P}_N)\|_{\infty}
\end{equation*}
where $T(\sigma, \sigma')= \binom{n}{2}-d(\sigma,\sigma')$. Actually, also true is the inequality
\begin{equation*}
L(\widehat{\sigma}_N)-L^{*}\le 2 \|D(P-\widehat{P}_N)\|_{\infty}
\end{equation*}
(where $D$ is the matrix with $D(\sigma,\sigma') = d(\sigma,\sigma')$), which is exactly the same as \eqref{eq:classic}.\\
Secondly, let's consider the {\it optimization error}:
\begin{align*}
L(\widetilde{\sigma}_N)-L(\widehat{\sigma}_N)&= [L(\widetilde{\sigma}_N)-\widehat{L}_N(\widetilde{\sigma}_N)]
+ [\widehat{L}_N(\widetilde{\sigma}_N) - \widehat{L}_N(\widehat{\sigma}_N)]
+ [\widehat{L}_N(\widehat{\sigma}_N)-L(\widehat{\sigma}_N)]
\end{align*}
We can upper bound the first and last term because, for any $\sigma \in \mathfrak{S}_n$:
\begin{align*}
L(\sigma)-\widehat{L}_N(\sigma)&=\sum_{\pi \in \mathfrak{S}_n} d(\sigma, \pi) P(\pi)- \sum_{\pi \in \mathfrak{S}_n} d(\sigma, \pi) \widehat{P}_N(\pi) =D(P-\widehat{P}_N)(\sigma)\\
&\le \|D(P-\widehat{P}_N)\|_{\infty}
\end{align*}
So the {\it optimization error} is upper bounded by:
\begin{align*}
L(\widetilde{\sigma}_N)-L(\widehat{\sigma}_N)&\le \|D(P-\widehat{P}_N)\|_{\infty}
+ d(\widetilde{\sigma}_N,\widehat{\sigma}_N)
+ \|D(P-\widehat{P}_N)\|_{\infty}\\
&= 2\|D(P-\widehat{P}_N)\|_{\infty}+d(\widetilde{\sigma}_N,\widehat{\sigma}_N).
\end{align*}
In summary, one can thus write
\begin{equation}
L(\tilde{\sigma}_{N}) - L^{\ast} \leq 4\|D(P-\widehat{P}_N)\|_{\infty}+d(\widetilde{\sigma}_N,\widehat{\sigma}_N).
\end{equation}
The first term can be bounded by some classic analysis (see before). The second term can be bounded on a given dataset using the method introduced in the ICML 2016 paper.

\bibliographystyle{plain}
\bibliography{biblio-thesis}

\end{document}
