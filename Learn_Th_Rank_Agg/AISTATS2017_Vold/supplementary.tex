\documentclass[10pt]{article}
% PACKAGES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\usepackage{aistats2016}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{epsfig}
\usepackage{geometry}
\usepackage{stmaryrd}
\usepackage[noadjust]{cite}
\usepackage{natbib}


% ENVIRONMENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{assumption}[theorem]{Assumption}
%\newtheorem{remark}[theorem]{Remark}
\newtheorem{rema}{Remark}
%\newenvironment{remark}{\begin{rema} \rm}{\end{rema}}
%\newtheorem{example}[theorem]{Example}
\newtheorem{exam}{Example}
\newenvironment{example}{\begin{exam} \rm}{\end{exam}}
\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\newenvironment{proof}[1][Proof]{\textbf{#1.} }{\ \rule{0.5em}{0.5em}}


% Notations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\n}{\llbracket n \rrbracket}
\newcommand{\DN}{\mathcal{D}_{N}}
\newcommand{\Sn}{\mathfrak{S}_n}
\def\e{\mathbf e}
\def\i{\mathbf i}
\def\j{\mathbf j}



% If your paper is accepted, change the options for the package
% aistats2016 as follows:
%
%\usepackage[accepted]{aistats2016}
%
% This option will print headings for the title of your paper and
% headings for the authors names, plus a copyright note at the end of
% the first column of the first page.

\usepackage{xr}
\externaldocument{Medians_V2}

\begin{document}
	
	% If your paper is accepted and the title of your paper is very long,
	% the style will print as headings an error message. Use the following
	% command to supply a shorter title of your paper so that it can be
	% used as headings.
	%
	%\runningtitle{I use this title instead because the last one was very long}
	
	% If your paper is accepted and the number of authors is large, the
	% style will print as headings an error message. Use the following
	% command to supply a shorter version of the authors names so that
	% they can be used as headings (for example, use only the surnames)
	%
	%\runningauthor{Surname 1, Surname 2, Surname 3, ...., Surname n}
	
	%\twocolumn[
	
	%\aistatstitle{Supplementary material for \\
	%		A Learning Theory of Ranking Aggregation}
	
	%\aistatsauthor{ Anonymous Author 1 \And Anonymous Author 2 \And Anonymous Author 3 }
	
	%\aistatsaddress{ Unknown Institution 1 \And Unknown Institution 2 \And Unknown Institution 3 } ]


\begin{center}
	\large{Supplementary material for \\
		A Learning Theory of Ranking Aggregation}
\end{center}
\hrule
%\title{Supplementary material for \\
%			A Learning Theory of Ranking Aggregation}


\section{Empirical consensus}

\subsection*{Proof of Proposition 3}

\noindent We can obtain the upper bound using \eqref{eq:classic} and some calculations on $L$ as follows. First notice that same as in \eqref{eq:loss-1} one has for any $\sigma\in\Sn$:
\[
\widehat{L}_{N}(\sigma)=\binom{n}{2}\mathbb{E}\left[ \widehat{p}_{\i,\j}\mathbb{I}\{\sigma(\i)>\sigma(\j) \}  + (1-\widehat{p}_{\i,\j})\mathbb{I}\{\sigma(\i)<\sigma(\j) \}\right]
\]
so that
\begin{align*}
\vert\widehat{L}_{N}(\sigma) - L(\sigma)\vert &= \binom{n}{2}\vert\mathbb{E}\left[(\widehat{p}_{\i,\j} - p_{\i,\j})\mathbb{I}\{\sigma(\i)>\sigma(\j)\} - (\widehat{p}_{\i,\j} - p_{\i,\j})\mathbb{I}\{\sigma(\i)<\sigma(\j) \}\right]\vert\\
&\leq \binom{n}{2}\mathbb{E}_{\i,\j}\left[ \vert p_{\i,\j}-\widehat{p}_{\i,\j} \vert  \right].
\end{align*}\\

\noindent {\bf Bound in expectation.} By the Cauchy-Schwartz inequality,
\begin{equation*}
\mathbb{E}_{\i,\j}\left[ \vert p_{\i,\j}-\widehat{p}_{\i,\j} \vert  \right] \le \sqrt{\mathbb{E}_{\i,\j}\left[ ( p_{\i,\j}-\widehat{p}_{\i,\j} )^2 \right]}= 
 \sqrt{Var( \widehat{p}_{\i,\j})}.
\end{equation*}
Since $\mathbb{E}_{\i,\j}\left[  p_{\i,\j}-\widehat{p}_{\i,\j} \right]=0$.
Then, $N \widehat{p}_{\i,\j} \sim \mathcal{B}(N,p_{\i,\j}))$ so $Var( \widehat{p}_{\i,\j})= \frac{ p_{\i,\j}(1- p_{\i,\j})}{N} \le \frac{1}{4N}$.\\
Finally:
\begin{equation*}
\mathbb{E}\left[L(\widehat{\sigma}_N)-L^*\right]\leq 2 \mathbb{E}\left[\max_{\sigma \in \Sn}\vert\widehat{L}_{N}(\sigma) - L(\sigma)\vert\right]\le 2 \binom{n}{2} \frac{1}{\sqrt{4N}}=\frac{n(n-1)}{2\sqrt{N}}.
\end{equation*}\\

\noindent {\bf Bound in probability.} By \eqref{eq:classic} one has for any $t > 0$
\begin{align*}
\mathbb{P}\Big\{L(\widehat{\sigma}_{N})-L^{\ast} > t\Big\} 
&\leq \mathbb{P}\Big\{2\binom{n}{2}\mathbb{E}_{\i,\j}\left[ \vert p_{\i,\j}-\widehat{p}_{\i,\j} \vert  \right] > t\Big\}\\ 
&= \mathbb{P}\Big\{\sum_{1\leq i < j\leq n}\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2}\Big\}\\
&\leq \mathbb{P}\Big\{\bigcup_{1\leq i < j\leq n}\Big\{\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2\binom{n}{2}}\Big\}\Big\}\\
&\leq \sum_{1\leq i < j \leq n}\mathbb{P}\Big\{\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2\binom{n}{2}}\Big\}.
\end{align*}
Now, Hoeffding's inequality to $\widehat{p}_{i,j} = (1/N)\sum_{t=1}^{N}\mathbb{I}\{\Sigma_{t}(i) < \Sigma_{t}(j)\}$ gives
\begin{equation*}
\label{eq:Hoeffding-pairwise}
\mathbb{P}\Big\{ \vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2\binom{n}{2}} \Big\} \leq 2e^{-2N(t/2\binom{n}{2})^{2}},
\end{equation*}
so that 
\begin{equation*}
\mathbb{P}\Big\{L(\widehat{\sigma}_{N})-L^{\ast} > t\Big\} \leq 2\binom{n}{2}e^{-\frac{Nt^{2}}{2\binom{n}{2}^{2}}}.
\end{equation*}
Setting $\delta = 2\binom{n}{2}e^{-\frac{Nt^{2}}{2\binom{n}{2}^{2}}}$ one obtains that with probability greater than $1-\delta$,
\begin{equation*}
\label{eq:Hoeffding-bound}
L(\widehat{\sigma}_{N}) - L^{\ast} \leq  \binom{n}{2}\sqrt{\frac{2\log(n(n-1)/\delta)}{N}}.
\end{equation*}\\


\subsection*{Proof of Proposition 5}
In the following proof, we consider Le Cam method. Consider two Mallows models $P_{\theta_0}$ and $P_{\theta_1}$ where $\theta_k=(\sigma^*_k,\phi_k)\in \mathfrak{S}_n\times (0,1)$ and $\sigma^*_0\neq \sigma^*_1$. We clearly have:
\begin{align*}
\mathcal{R}_N\geq& \inf_{\sigma_N}\max_{k=0,\; 1} \mathbb{E}_{P_{\theta_k}}\left[L_{P_{\theta_k}}(\sigma_N)-L_{P_{\theta_k}}^*  \right]\\
= & \inf_{\sigma_N}\max_{k=0,\; 1}\sum_{i<j} \mathbb{E}_{P_{\theta_k}}\left[\frac{\vert \phi_k-1 \vert}{2(1+\phi_k)}\times  \mathbb{I}\{ (\sigma_N(i)-\sigma_N(j)(\sigma^*(i)-\sigma_k^*(j))<0  \} \right]\\
\geq & \frac{1}{4} \inf_{\sigma_N}\max_{k=0,\; 1}\vert \phi_k-1 \vert \mathbb{E}_{P_{\theta_k}}\left[d_{\tau} (\sigma_N,\sigma_k^*)  \right],
\end{align*}
using \eqref{eq:Mallows_pair}. Set $ \Delta=d_{\tau}(\sigma^*_0,\sigma^*_1)\geq 1$, and consider the test statistic related to $\sigma_N$:
\begin{equation*}
\psi(\Sigma_1,\; \ldots,\; \Sigma_N)=\mathbb{I}\{ d_{\tau}(\sigma_N,\sigma^*_1)\leq d_{\tau}(\sigma_N,\sigma^*_0)\ \}.
\end{equation*}
If $\psi=1$, by triangular inequality, we have:
\begin{equation*}
\Delta \leq d_{\tau}(\sigma_N,\sigma^*_0)+d_{\tau}(\sigma_N,\sigma^*_1)\leq 2d_{\tau}(\sigma_N,\sigma^*_0).
\end{equation*}
Hence, we have
\begin{align*}
\mathbb{E}_{P_{\theta_0}}\left[d_{\tau} (\sigma_N,\sigma_0^*)  \right]&\geq \mathbb{E}_{P_{\theta_0}}\left[d_{\tau} (\sigma_N,\sigma_0^*) \mathbb{I}\{ \psi=+1 \} \right]\\
&\geq \frac{\Delta}{2}\mathbb{P}_{\theta_0}\{\psi=+1  \}
\end{align*}
and similarly
\begin{align*}
\mathbb{E}_{P_{\theta_1}}\left[d_{\tau} (\sigma_N,\sigma_1^*)  \right]&\geq \mathbb{E}_{P_{\theta_1}}\left[d_{\tau} (\sigma_N,\sigma_1^*) \mathbb{I}\{ \psi=0 \} \right]\\
&\geq \frac{\Delta}{2}\mathbb{P}_{\theta_1}\{\psi=0  \}.
\end{align*}
Bounding by below the maximum by the average, we have:
\begin{align*}
\inf_{\sigma_N}\max_{k=0,\; 1}\vert \phi_k-1 \vert \mathbb{E}_{P_{\theta_k}}\left[d_{\tau} (\sigma_N,\sigma_k^*)  \right]&\geq   
\inf_{\sigma_N}\frac{\Delta}{2} \frac{1}{2}\left\{ \vert \phi_1-1 \vert \mathbb{P}_{\theta_1}\{\psi=0  \}  + \vert \phi_0-1 \vert \mathbb{P}_{\theta_0}\{\psi=1  \} \right\} \\
& \geq \inf_{\sigma_N}\frac{\Delta}{4}\min_{k=0,\; 1}\vert \phi_k-1 \vert \left\{ \mathbb{P}_{\theta_1}\{\psi=0  \}  + \mathbb{P}_{\theta_0}\{\psi=1  \} \right\}\\
&\geq \frac{\Delta}{4}\min_{k=0,\; 1}\vert \phi_k-1 \vert \left\{ \mathbb{P}_{\theta_1}\{\psi^*=0  \}  + \mathbb{P}_{\theta_0}\{\psi^*=1  \} \right\},
\end{align*}
where the last inequality follows from a standard Neyman-Pearson argument, denoting by
\begin{equation*}
\psi^*(\Sigma_1,\; \ldots,\; \Sigma_N)=\mathbb{I}\left\{\prod_{i=1}^N\frac{P_{\theta_1}(\Sigma_i)}{P_{\theta_0}(\Sigma_i)}\geq 1\right\}
\end{equation*}
the likelihood ratio test statistic. We deduce that 
\begin{align*}
\mathcal{R}_N&\geq \frac{\Delta}{16}\min_{k=0,\; 1}\vert \phi_k-1 \vert \times
\sum_{\sigma_i\in \mathfrak{S}_N,\; 1\leq i\leq N}\min\left\{\prod_{i=1}^NP_{\theta_0}(\sigma_i),\;  \prod_{i=1}^NP_{\theta_1}(\sigma_i) \right\}\\
&\geq \frac{\Delta}{32}\min_{k=0,\; 1}\vert \phi_k-1 \vert e^{-NK(P_{\theta_0}\vert\vert P_{\theta_1})},
\end{align*}
where $K(P_{\theta_0}\vert\vert P_{\theta_1})=\sum_{\sigma\in \mathfrak{S}_N}P_{\theta_0}(\sigma)\log(P_{\theta_0}(\sigma)/P_{\theta_1}(\sigma))$ denotes the Kullback-Leibler divergence. In order to establish a minimax lower bound of order $1/\sqrt{N}$, one should choose $\theta_0$ and $\theta_1$ so that, for $k\in\{0,\; 1  \}$, $\phi_k\rightarrow 1$ and $K(P_{\theta_0}\vert\vert P_{\theta_1})\rightarrow 0$ as $N\rightarrow +\infty$ at appropriate rates. Take
$\phi_0=1-1/\sqrt{N}$ and  $\phi_1=(1-1/\sqrt{N})(1-1/N)$.
Observe that we then have $\min_{k=0,\; 1}\vert\phi_k-1 \vert\geq 1/\sqrt{N}$ and
\begin{align*}
K(P_{\theta_0}\vert\vert P_{\theta_1})&=\sum_{\sigma\in \mathfrak{S}_n}\frac{1}{Z_0}\phi_0^{d_{\tau}(\sigma_0,\sigma)}\log\left(  \frac{Z_1}{Z_0}\frac{\phi_0^{d_{\tau}(\sigma_0,\sigma)}}{\phi_1^{d_{\tau}(\sigma_1,\sigma)}}\right)\\
=&\log(\frac{Z_1}{Z_0}) +\sum_{\sigma\in \mathfrak{S}_n}\frac{1}{Z_0}\phi_0^{d_{\tau}(\sigma_0,\sigma)}\left( \log \left(\frac{\phi_0}{\phi_1}\right)^{d_{\tau}(\sigma_0,\sigma)}
%\right.\\
%&\left.
+ \log \left(\frac{1}{\phi_1^{d_{\tau}(\sigma_1,\sigma)-d_{\tau}(\sigma_0,\sigma)}}\right)\right)\\
=& \log(\frac{Z_1}{Z_0}) + \log\left(\frac{\phi_0}{\phi_1}\right)\sum_{\sigma\in \mathfrak{S}_n}P_{\theta_0}(\sigma) d_{\tau}(\sigma_0,\sigma)
+ \log\left(\frac{1}{\phi_1}\right)\sum_{\sigma\in \mathfrak{S}_n}P_{\theta_0}(\sigma) \left(-d_{\tau}(\sigma_0,\sigma)+d_{\tau}(\sigma_1,\sigma)\right)\\
=&\log(\frac{Z_1}{Z_0}) +\log (\phi_0/\phi_1)L_{P_{\theta_0}}(\sigma_0)
+\log(\frac{1}{\phi_1})(L_{P_{\theta_0}}(\sigma_1)-L_{P_{\theta_0}}(\sigma_0))\\
=&\log(\frac{Z_1}{Z_0}) +\log (\frac{\phi_0}{\phi_1})\frac{n(n-1)}{2}\frac{\phi_0}{1+\phi_0}
+\log(\frac{1}{\phi_1})\frac{\vert \phi_0-1 \vert}{2(\phi_0+1)}\Delta. 
\end{align*}\\

\noindent Consider the first term in the preceding equality. We have:
\begin{equation*}
\log \left(\frac{Z_1}{Z_0}\right)	=\log\left( \frac{\prod_{i=1}^{n-1} \sum_{j=0}^{i} \phi_1^j}{\prod_{i=1}^{n-1} \sum_{j=0}^{i} \phi_0^j}\right)
=\sum_{i=1}^{n-1} \log \left( \frac{\sum_{j=0}^{i} \phi_1^j}{\sum_{j=0}^{i} \phi_0^j} \right)
\end{equation*}

\noindent Then, by using the log-sum inequality, the first term gives 
\begin{align*}
\log \left(\frac{Z_1}{Z_0}\right)\le & \sum_{i=1}^{n-1} \frac{1}{\sum_{j=0}^{i} \phi_1^j} \sum_{j=0}^{i} \phi_1^j \log \left( \frac{\phi_1^j}{\phi_0^j} \right)\\
\le & \sum_{i=1}^{n-1} \frac{1}{\sum_{j=0}^{i} \phi_1^j} \sum_{j=0}^{i} \phi_1^j j \log \left( \frac{\phi_1}{\phi_0} \right)\\
\le & \sum_{i=1}^{n-1} i \log \left( \frac{\phi_1}{\phi_0} \right)\\
= & \frac{n(n-1)}{2} \log \left( \frac{\phi_1}{\phi_0} \right).
\end{align*}
\noindent So adding the first and second term of the Kullback-Leibler divergence gives:
\begin{align*}
\log(\frac{Z_1}{Z_0}) +\log (\frac{\phi_0}{\phi_1})\frac{n(n-1)}{2}\frac{\phi_0}{1+\phi_0}&= \frac{n(n-1)}{2} \log \left( \frac{\phi_1}{\phi_0} \right)-\log (\frac{\phi_1}{\phi_0})\frac{n(n-1)}{2}\frac{\phi_0}{1+\phi_0}\\
&=\frac{n(n-1)}{2} \log \left( \frac{\phi_1}{\phi_0} \right) \frac{1}{1+\phi_0} \\
%&= \frac{n(n-1)}{2}\log \left( 1 - \frac{1}{N} \right)\frac{1}{2-\frac{1}{\sqrt{N}}}
\end{align*}
\noindent And adding this with the third term gives:
\begin{align*}
K(P_{\theta_0}\vert\vert P_{\theta_1})&=\frac{n(n-1)}{2} \log \left( \frac{\phi_1}{\phi_0} \right) \frac{1}{1+\phi_0} +\log(\frac{1}{\phi_1})\frac{\vert \phi_0-1 \vert}{2(\phi_0+1)}\Delta\\
&=\frac{1}{2(1+\phi_0)} \left[ n(n-1) (\log(\phi_1)-\log(\phi_0)) +(\phi_0-1)\Delta \log(\phi_1) \right]\\
&= \frac{\left[ (n(n-1)+ (\phi_0-1)\Delta) \log(\phi_1) - n(n-1)\log(\phi_0) \right]}{2(1+\phi_0)}\\
&\le \frac{n(n-1)\log\left(\frac{\phi_1}{\phi_0}\right)}{2(1+\phi_0)}\\
&= \frac{n(n-1)\log\left(1-\frac{1}{N}\right)}{2(2-\frac{1}{\sqrt{N}})}\\
&\le - \frac{n(n-1)}{2N}
\end{align*}
\noindent Because for all $x<1$, $x\ne 0$, $-log(1-x)>x$ and $2-\frac{1}{\sqrt{N}}\ge 1$.\\

\noindent Finally:
\begin{equation*}
e^{-N K(P_{\theta_0}\vert\vert P_{\theta_1})}\ge e^{ \binom{n}{2}}.
\end{equation*}
Then, by taking $\Delta= \frac{n(n-1)}{2}$, we get the bound of the proposition.


\section{Stochastic transitivity}

\subsection*{Proof of Theorem 10}

Suppose $P$ is stochastically transitive. The pairwise probabilities can be represented as a directed graph on the $n$ items. An edge is drawn from $i$ to $j$ whenever $p_{i,j}>\frac{1}{2}$ (no edge is drawn if $p_{i,j}=\frac{1}{2}$). This graph is thus a directed acyclic graph, since the stochastic transitivity prevents any cycle. So there exists a partial order on the graph (also called "topological ordering", the vertices representing the items are sorted by their in-degree). So any permutation $\sigma^*$ extending this partial order satisfies (\ref{eq:cond_opt}). In addition, under the stochastic transitivity condition, for $i<j$, $p_{i,j}<1/2 \Leftrightarrow s^*(i)<s^*(j)$. So $s^*$ belongs to $\Sn$ iff $P$ is stricly stochastically transitive and in this case, $s^*$ is the unique median of $P$.

\subsection*{Proof of Corollary 11}

Suppose $P$ satisfies the \textit{strongly stochastically transitive} condition. According to Theorem 5, there exists $\sigma^* \in \Sn$ satisfying (\ref{eq:cond_opt}) and (\ref{eq:cond_opt2}).
We already know that $\sigma^*$ is a Kemeny consensus since it minimizes the loss with respect to the Kendall's $\tau$ distance. Then, Copeland's method order the items by the number of their pairwise victories, which corresponds to sort them according to the mapping $s^*$ and thus $\sigma^*$ is a Copeland consensus. Finally, the Borda score for an item is: $s(i) = \sum_{\sigma \in \Sn}\sigma(i)P(\sigma)$. With simple calculations, we have $s(i) = 1+ \sum_{k\ne i} p_{k,i}$. \\
Let $i,j$ such that $p_{i,j}>1/2$ ($\Leftrightarrow s^*(i)< s^*(j)$ under \textit{stochastic transitivity}).
\begin{align*}
s(j)-s(i)&= \sum_{k\ne j} p_{k,j} - \sum_{k\ne i} p_{k,i}\\
&= \sum_{k\ne i,j} p_{k,j} - \sum_{k\ne i,j} p_{k,i}  +p_{i,j} - p_{j,i}\\
&= \sum_{k\ne i,j} p_{k,j}- p_{k,i} + (2 p_{i,j}- 1)
\end{align*}
With $(2 p_{i,j}- 1)>0$. Now we focus on the first term.\\
Let $k \ne i,j$. 
\begin{itemize}
	\item First case: $p_{j,k} \ge 1/2$. The strong stochastic transitivity condition implies that :
	\begin{align*}
	p_{i,k}&\ge \max(p_{i,j}, p_{j,k})\\
	1-p_{k,i}& \ge \max(p_{i,j}, p_{j,k})\\
	p_{k,j}-p_{k,i}&\ge p_{k,j}-1 +\max(p_{i,j}, p_{j,k})\\
	p_{k,j}-p_{k,i}&\ge -p_{j,k}+ \max(p_{i,j}, p_{j,k})\\
	p_{k,j}-p_{k,i}&\ge \max(p_{i,j}-p_{j,k}, 0)\\	
	p_{k,j}-p_{k,i}&\ge 0\\	
	\end{align*}
	\item Second case: $p_{k,j} > 1/2$. If $p_{k,i} \le 1/2$, $p_{k,j}-p_{k,i} >0$. Now if $p_{k,i}>1/2$, the strong stochastic transitivity condition implies that $p_{k,j}\ge max(p_{k,i}, p_{i,j})$
\end{itemize}
So in any case, $\forall k \ne i,j$, $p_{k,j}- p_{k,i}\ge 0$ and $ s(j)-s(i)>0$.


\section{Fast Rates}

\subsection*{Proposition \ref{prop:low_noise}}
Let $\mathcal{A}_N=\{ \bigcap_{i<j} (p_{i,j}-\frac{1}{2})(\widehat{p}_{i,j}-\frac{1}{2})>0 \}$.
On the event $\mathcal{A}_N$, $p$ and $\widehat{p}$ satisfy the strongly stochastic transitivity property, so we can write the excess of risk as (\ref{eq:excess_plugin}) and	$L(\widehat{\sigma}_N)-L^*=0$. Moreover,
\begin{align*}
\mathbb{P}\Big\{ \mathcal{A}_N^c  \Big\}&= \mathbb{P}\Big\{ \bigcup_{i<j}(p_{i,j}-\frac{1}{2})(\widehat{p}_{i,j}-\frac{1}{2})<0  \Big\}\\
&\le \sum_{i<j} \mathbb{P}\Big\{ (p_{i,j}-\frac{1}{2})(\widehat{p}_{i,j}-\frac{1}{2})<0  \Big\}\\
&\le \sum_{i<j} \mathbb{P}\Big\{ \vert p_{i,j} - \widehat{p}_{i,j} \vert > 2h/\sqrt{1-2h} \Big\}\\
&\le \sum_{i<j} 2 e^{-2N4h^2/(1-2h)}\\
&\le  2 \binom{n}{2} e^{-8h^2/(1-2h)N}
\end{align*}
So the expectation of the excess of risk is: 
\begin{align*}
\mathbb{E}\Big\{L(\widehat{\sigma}_N)-L^*\Big\}&= \mathbb{E}\Big\{(L(\widehat{\sigma}_N)-L^*) \mathbb{I}\{ \mathcal{A}_N \} +\\ &(L(\widehat{\sigma}_N)-L^*) \mathbb{I}\{ \mathcal{A}_N^c \}\Big\}\\
& \leq \frac{n(n-1)}{4} \mathbb{P} \Big\{ \mathcal{A}_N^c  \Big\}\\
&\leq \binom{n}{2}^2 e^{-8h^2/(1-2h)N}
\end{align*}

\subsection*{Remark 12}
The first step is to simplify the expression of the expectation of the excess of risk square and then to use the decomposition expectation-variance.
\begin{align*}
\mathbb{E}[(\widehat{L}^*-L^*  )^2]&= \mathbb{E}\left[ \left( \sum_{i<j}\left\{  \frac{1}{2}-\left\vert \widehat{p}_{i,j}-\frac{1}{2}\right\vert  \right\}  %\right.\right.\\
%& \left. \left.
 -\sum_{i<j}\left\{  \frac{1}{2}-\left\vert p_{i,j}-\frac{1}{2}\right\vert  \right\} \right)^2 \right] \\
&= \mathbb{E}\left[ \left(\sum_{i<j}\left\vert p_{i,j}-\frac{1}{2}\right\vert -\left\vert \widehat{p}_{i,j}-\frac{1}{2}\right\vert \right)^2 \right] \\
&=\mathbb{E}\left[ \left(\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert\right)^2 \right]\\
&=Var\left(\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right) + 
 \left(\mathbb{E}\left[\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right] \right)^2
\end{align*}
The second term gives:
\begin{equation*}
\mathbb{E}\left[\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right]=\sum_{\substack{i<j\\p_{i,j}>\widehat{p}_{i,j} }}\mathbb{E}\left[p_{i,j}-\widehat{p}_{i,j}\right] + \sum_{\substack{i<j\\p_{i,j}<\widehat{p}_{i,j} }}\mathbb{E}\left[\widehat{p}_{i,j}-p_{i,j}\right]\\
=0
\end{equation*}
The first term developped gives: 
\begin{equation*}
Var\left(\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right)= \sum_{i<j} \sum_{i'<j'} Cov\left(\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert , \left\vert p_{i',j'}-\widehat{p}_{i',j'}\right\vert \right)
\end{equation*}
%With:
%\begin{align*}
%Var\left(\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right) &= Var\left( p_{i,j}-\widehat{p}_{i,j} \right) +\\
%&\mathbb{E}\left[ p_{i,j}-\widehat{p}_{i,j} \right]^2 - \mathbb{E}\left[ \left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right]\\
%& \le Var\left(\widehat{p}_{i,j} \right)\\
%&= \frac{p_{i,j}(1-p_{i,j})}{N}
%\end{align*}
With: 
\begin{align*}
Cov\left(\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert , \left\vert p_{i',j'}-\widehat{p}_{i',j'}\right\vert \right)&\le
\sqrt{Var(\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert)Var(\left\vert p_{i',j'}-\widehat{p}_{i',j'}\right\vert )}\\
&\le \sqrt{Var(\widehat{p}_{i,j})Var(\widehat{p}_{i',j'})}\\
&\le \sqrt{\frac{p_{i,j}(1-p_{i,j})p_{i',j'}(1-p_{i',j'})}{N^2}}\\
&\le \frac{1}{4N}\\
\end{align*}
So $ \mathbb{E}[(\widehat{L}^*-L^*  )^2]\le 2 \binom{\binom{n}{2}}{2} \frac{1}{4N}\le \frac{ \binom{n}{2}^2}{4N}$.

\subsection*{Proposition 13}

Same method than for Theorem 5. We define $\theta_0 =(1 \dots n)$, $\theta_1= (n \dots 1)$ and $P_{\theta_0}(\theta_0)=P_{\theta_1}(\theta_1)=\frac{1}{2}+h$, $P_{\theta_0}(\theta_1)=P_{\theta_1}(\theta_0)=\frac{1}{2}-h$. We have :

\begin{align*}
\mathcal{R}_N\geq& \inf_{\sigma_N}\max_{k=0,\; 1} \mathbb{E}_{P_{\theta_k}}\left[L_{P_{\theta_k}}(\sigma_N)-L_{P_{\theta_k}}^*  \right]\\
= & \inf_{\sigma_N}\max_{k=0,\; 1}\sum_{i<j} \mathbb{E}_{P_{\theta_k}}\left[\vert p_{i,j} - \frac{1}{2} \vert\times
% \right. \\
%& \left. 
\mathbb{I}\{ (\sigma_N(i)-\sigma_N(j)(\sigma^*(i)-\sigma_k^*(j))<0  \} \right]\\
\geq & \inf_{\sigma_N}\max_{k=0,\; 1} h \mathbb{E}_{P_{\theta_k}}\left[ d_{\tau}(\sigma_N, \sigma^*)\right]\\
\geq & h \frac{\Delta}{8} e^{-N K(P_{\theta_0\vert \vert \theta_1})}
\end{align*}
With:
\begin{align*}
K(P_{\theta_0\vert \vert \theta_1})&= P_{\theta_0}(\theta_0) log \left( \frac{P_{\theta_0}(\theta_0)}{P_{\theta_1}(\theta_0)} \right) + P_{\theta_0}(\theta_1) log \left( \frac{P_{\theta_0}(\theta_1)}{P_{\theta_1}(\theta_1)} \right)\\
&= (\frac{1}{2}+h) log \left( \frac{\frac{1}{2}+h}{\frac{1}{2}-h}\right) + 
(\frac{1}{2}-h) log \left( \frac{\frac{1}{2}-h}{\frac{1}{2}+h}\right) \\
&=2h log \left(1+ \frac{4h}{1-2h}\right) \\
&\leq \frac{8 h^2}{1-2h}\\
\end{align*}



\end{document}
