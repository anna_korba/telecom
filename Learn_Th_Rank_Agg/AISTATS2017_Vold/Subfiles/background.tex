

\section{BACKGROUND AND NEW FRAMEWORK}\label{sec:background}

We start by describing the two classic approaches to state the rank aggregation problem. We first focus on full rankings but the extension to pairwise comparisons is provided in Section ... / Supplementary. % A choisir
Let $\n := \{1,\; \ldots,\; n \}$ be a set of $n\geq 1$ items to be ranked. A full ranking on $\n$ is usually seen as a permutation $\sigma$ of $\n$ that maps an item $i$ to its rank $\sigma(i)$. Here and throughout, the set of permutations of $\n$ is denoted by $\Sn$, the indicator function of any event $\mathcal{E}$ is denoted by $\mathbb{I}\{ \mathcal{E} \}$, the Dirac mass at any point $a$ by $\delta_a$, the maximum of $(a,b) \in \mathbb{R}^2$ by $a \vee b$, and we set $sgn(x)=2\mathbb{I}\{x\geq 0 \}-1$ for all $x\in\mathbb{R}$.

\subsection{Existing formulations of the rank aggregation problem}
\label{subsec:existing-formulations}

\paragraph{Looking for a deterministic consensus.} 

In the first approach, one is given a collection of $N\geq 1$ (deterministic) permutations $\sigma_{1},\; \ldots,\; \sigma_{N}$ and the goal is to find $\sigma^*\in \Sn$ that best summarizes it. In social choice theory, elements of $\n$ are candidates and permutation $\sigma_{i}$ for $i= 1,\dots, N$ represents the vote of the $i^{th}$ voter, expressing that $\sigma_{i}^{-1}(1)$ is his favorite candidate, $\sigma_{i}^{-1}(2)$ his second favorite candidate, and so on. The permutation $\sigma^*$ is the result of the election and the aggregation procedure that maps $(\sigma_{1},\; \ldots,\; \sigma_{N})$ to $\sigma^*$ is thus called the voting rule. The quality of a voting rule can be characterized by its axiomatic properties, as it was first done in \citet{Borda} and \citet{Condorcet} and formalized in \citet{Arrow}. Many voting rules have been introduced in the literature and analyzed in this framework, such as ..., and in particular Kemeny's rule. Introduced in \citet{Kemeny59}, it defines $\sigma^{\ast}$ as a solution of the following optimization problem:
\begin{equation}
\label{eq:ranking_aggregation}
\min_{\sigma\in \mathfrak{S}_n}\sum_{i=1}^N d(\sigma,\sigma_{i}),
\end{equation}
where $d$ is the Kendall's $\tau$ distance defined for $\sigma,\sigma'\in\mathfrak{S}_n$ as the number of pairwise disagreements between $\sigma$ and $\sigma'$:
\begin{equation*}
d(\sigma,\sigma')=\sum_{1\leq i < j\leq n}\mathbb{I}\{(\sigma(i)-\sigma(j)) (\sigma'(i)-\sigma'(j)) < 0\}.
\end{equation*}
Notice that problem \eqref{eq:ranking_aggregation} always has a solution because $\Sn$ is finite, but it is not necessarily unique. Kemeny's rule satisfies many good axiomatic properties \citep[see][]{YL78} but it is NP-hard to compute \citep[see][]{BTT89, DKNS01}. Many contributions in the literature have thus introduced approximation procedures of Kemeny's rule (see ...) or established guarantees about the approximation cost of existing ones (see ...), the latter being naturally defined for any permutation $\sigma\in\Sn$ by $\sum_{i=1}^N d(\sigma,\sigma_{i})$. Many empirical experiments are also provided in \citet{VZW09} and \citet{AM12}.

\paragraph{Looking for the ground truth in the presence of noise.}

The second approach takes another point of view. It assumes that an ideal ranking $\sigma^{\ast}\in\Sn$ exists but the observer only has access to noisy samples of it. Formally the dataset is modeled as a collection of $N\geq 1$ random permutations $\sigma_{1},\; \ldots,\; \sigma_{N}$ drawn IID from a probability distribution on $\Sn$ centered around $\sigma^{\ast}$ that belongs to a parametric model. Mainly two models have been used in the literature. The first one is the Mallows model \citet{Mallows57}, defined by
\begin{equation}
\label{eq:Mallows-definition}
p_{\sigma^{\ast},\phi}(\sigma) = \frac{1}{Z(\phi)}\phi^{-d(\sigma^{\ast},\sigma)},
\end{equation}
where $\phi\in[0,1]$ is a ``dispersion parameter'' and $Z(\phi) = \prod_{i=1}^{n-1}\sum_{j=0}^{i}\phi^{j}$ is a normalization constant. The permutation $\sigma^{\ast}$ is the mode of the distribution and a sampling from $p_{\sigma^{\ast},\phi}$ can be seen as a noisy version of it. This interpretation can be used to effectively sample from $p_{\sigma^{\ast},\phi}$ and was already present in \citet{Condorcet} \citep[see][]{Young88}. The second one is the Bradley-Terry-Luce-Plackett model \citet{BT52, Luce59, Plackett75}, defined by
\begin{equation}
\label{eq:PL-definition}
p_{w}(\sigma) = \prod_{i=1}^{n}\frac{w_{\sigma^{-1}(i)}}{\sum_{j=i}^{n}w_{\sigma^{-1}(j)}},
\end{equation}
where $w = (w_{1},\dots,w_{n})\in\mathbb{R}_{+}^{n}$ is a vector of weights that each measures the ``quality'' of the associated item. It is easy to see that the mode of $p_{w}$ is the full ranking $\sigma^{\ast} := \argsort(w)$ obtained by sorting the items in decreasing order of the weights (the item with higher weight in first position and so on). Again, a sampling from $p_{w}$ can be seen as a noisy version of $\sigma^{\ast}$. In both cases, the true ranking is a parameter of the probabilistic model and rank aggregation is stated as the problem of its statistical estimation. Many approaches have been introduced to tackle it for either the Mallows model (see ...) or the Plackett-Luce model (see ...)

\paragraph{Theoretical limitations.}


\subsection{Our new probabilistic nonparametric framework}

\paragraph{Definition.}

Following a statistical approach, we assume that the observed permutations are random samples $\Sigma_{1},\dots,\Sigma_{N}$ drawn IID from a probability distribution $p$ over $\Sn$. In a model-free approach, we do not make any assumption on $p$, and thus not on the existence of a ``central'' permutation that would be the true ranking. Instead, we claim that the objective of summarizing the dataset $\DN := \{\Sigma_{1},\dots,\Sigma_{N}\}$ should naturally be extended as summarizing the distribution $p$. We therefore define the goal of rank aggregation as finding a permutation $\sigma^{\ast}$ solution of the following optimization problem:
\begin{equation}
\label{eq:theoretical-consensus}
\min_{\sigma\in\Sn}\sum_{\pi\in\Sn}d(\sigma,\pi)p(\pi).
\end{equation}
Problem \eqref{eq:theoretical-consensus} is the natural analogue of Problem \eqref{eq:ranking_aggregation} in our probabilistic setting. The difference is that now $p$ is not known and only observed through its samples $\Sigma_{1},\dots,\Sigma_{N}$. The output $\widehat{\sigma}_{N}$ of any aggregation procedure on the dataset $\DN$ must therefore be considered as a stochastic approximation of the solutions of Problem \ref{eq:theoretical-consensus}.

The next step in the definition of our framework is then the choice of the approximation cost for $\widehat{\sigma}_{N}$. In an estimation setting, we would opt for the distance $d(\widehat{\sigma}_{N},\sigma^{\ast})$ between the output of the aggregation procedure and any permutation $\sigma^{\ast}$ solution of Problem \eqref{eq:theoretical-consensus}. On the other hand, as the optimal permutation(s) $\sigma^{\ast}$ minimize the cost function $\sigma \mapsto \sum_{\pi\in\Sn}d(\sigma,\pi)p(\pi)$ it is also natural to take the latter to measure the approximation cost of $\widehat{\sigma}_{N}$. We choose the second option for the two following reasons. First, Problem \eqref{eq:theoretical-consensus} can have several solutions $\sigma^{\ast}$, that can have large distances between them. In such a situation, it does not make much sense to look for an approximation of ``any of the $\sigma^{\ast}$'s''. Second, in many applications, the cost $\sum_{\pi\in\Sn}d(\widehat{\sigma}_{N},\pi)p(\pi)$ of an aggregate ranking matters most than its distance $d(\widehat{\sigma}_{N},\sigma^{\ast})$ to an optimal ranking. It can be far from all optimal ranking, if its cost is close to the minimum, it can be considered as good as an optimal ranking. Even in an election setting with a unique optimal ranking $\sigma^{\ast}$, if an aggregate ranking is very different from $\sigma^{\ast}$ but still has a low cost then it means that it represents an outcome that does induce too much dissatisfaction (compared to $\sigma^{\ast}$). This is why we measure the approximation cost of an aggregate ranking $\widehat{\sigma}_{N}$ by $\sum_{\pi\in\Sn}d(\widehat{\sigma}_{N},\pi)p(\pi)$. 

\begin{remark}{\sc (Difference with estimation of the mean in a Euclidean space)}
Let $X_{1}, \dots, X_{N}$ be $N$ IID samples of a random variable $X$ on a Euclidean space $(V,\Vert\cdot\Vert)$, with $\mathbb{E}[\Vert X\Vert^{2}] < \infty$. It is classic to look for a quantity that best summarizes the dataset $\{X_{1}, \dots, X_{N}\}$ or the distribution of $X$. The first problem is naturally stated as finding the solution of the least-square regression $\min_{x\in V}\sum_{i=1}^{N}\Vert x - X_{i}\Vert^{2}$ and the second one is naturally solved by taking the estimator $1/N\sum_{i=1}^{N}X_{i}$ of $\mathbb{E}[X]$. As it is well-known, the latter is exactly the solution of the former problem. This fact is however due to the Euclidean structure and does not remain valid for ranking data \citep[see][for more insights]{JKS16}. %In addition, assuming $X$ has probability density function $f$, one has $\Vert x - \mathbb{E}[X]\Vert^{2} = \int_{V}\Vert x - x'\Vert^{2} f(x')dx' + cst$, so that measuring the performance of 
\end{remark}

\paragraph{Interpretation as a learning theory for rank aggregation.}

It turns out that the framework we have introduced can be interpreted as a learning theory for aggregation. The central observation is that the cost function of Problem \eqref{eq:theoretical-consensus} can be re-written as
\begin{equation}
L(\sigma) := \mathbb{E}_{\Sigma\sim p}\left[d(\sigma,\Sigma)\right],
\end{equation}
where $\Sigma\sim p$ means that the expectation is taken over a random permutation $\Sigma$ drawn from $p$. The value $L(\sigma)$ is the expected distance between the permutation $\sigma$ and a permutation drawn from $p$. Seeing $d$ as a ``loss function'', $L(\sigma)$ is then the ``risk'' of the permutation $\sigma$ seen as a predictor of $\Sigma$. Rewriting Problem \eqref{eq:theoretical-consensus} as 
\begin{equation}
\label{eq:median_pb}
\min_{\sigma \in \mathfrak{S}_n}L(\sigma),
\end{equation}
our framework for rank aggregation consists in looking for aggregate rankings with smallest possible risk. Of course, one does not have access to the function $L$ and the goal is therefore to build a procedure that ``learns'' an aggregate ranking $\widehat{\sigma}_{N}$ with smallest possible risk $L(\widehat{\sigma}_{N})$. 

A natural approach is to follow the Empirical Risk Minimization (ERM) paradigm \citep[see for instance][]{Vapnik}. In the present framework, the empirical risk naturally writes as
\begin{equation}\label{eq:emp_risk}
\widehat{L}_N(\sigma):=\frac{1}{N}\sum_{i=1}^N d(\Sigma_i,\sigma)
\end{equation}
and the ERM ranking are the solutions of
\begin{equation}\label{eq:ERM1}
\min_{\sigma\in \mathfrak{S}_n}\widehat{L}_N(\sigma).
\end{equation}
As it turns out, Problem \eqref{eq:ERM1} is the same as Problem \eqref{eq:ranking_aggregation}. The ERM rankings are thus the Kemeny consensuses of the dataset $\DN$.

This last observation means that our framework encompass the deterministic approach to rank aggregation. The latter indeed corresponds to look for the ERM rankings and measure the approximation cost by $\widehat{L}_{N}$. Same as in classic learning theory, measuring the performance of a ranking $\widehat{\sigma}_{N}$ by its empirical risk on the same set used to train it, $\widehat{L}_{N}(\widehat{\sigma}_{N})$ is subject to a large instability. Our framework measures the performance by the theoretical risk $L(\widehat{\sigma}_{N})$ and provide bounds for the generalization error. The parametric probabilistic approach to rank aggregation is also encompassed by our framework, as shown by the following proposition.

\begin{proposition}
\label{prop:generalization-Mallows-PL}
For $p_{\theta}$ be a Mallows or Plackett-Luce distribution (see Subsection \ref{subsec:existing-formulations}) the associated central ranking $\sigma^{\ast}$ is also the unique solution of $\min_{\sigma\in\Sn}L_{p_{\theta}}(\sigma)$.
\end{proposition}

While Proposition \ref{prop:generalization-Mallows-PL} is immediate for a Mallows distribution, it is not for a Plackett-Luce one. The proof is postponed to Section \ref{sec:stochastic-transitivity}. Proposition \ref{prop:generalization-Mallows-PL} shows that our framework coincides with the classic parametric probabilistic approach to rank aggregation under the Mallows and Plackett-Luce models. It offers however more flexibility by measuring the quality of an aggregation procedure by $\mathbb{E}[L(\widehat{\sigma}_{N})]$ instead of $\mathbb{P}[\widehat{\sigma}_{N} = \sigma^{\ast}]$. 


\begin{remark}{\sc (Related work)}
\end{remark}
 
\begin{remark}{\sc (Classic voting rules as regularized ERM)}
\end{remark}
  