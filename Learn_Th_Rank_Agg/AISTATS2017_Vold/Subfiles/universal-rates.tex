
\section{Universal rates}\label{sec:universal-rates}

Here we establish theoretical guarantees about convergence rates without any assumption on the probability distribution $p$.

%Here, our goal is to establish sharp bounds for the excess of risk of  \textit{empirical Kemeny medians}, of solutions $\widehat{\sigma}_N$ of \eqref{eq:ERM1} in the Kendall's $\tau$ distance case namely. Beyond the study of universal rates for the convergence of the expected distance $L(\widehat{\sigma}_N)$ to $L^*$, we prove that under the stochastic transitivity condition, exponentially fast convergence occurs when the $p_{i,j}$'s are bounded away from $1/2$, similarly to the phenomenon exhibited in \cite{KB05} for binary classification under extremely low noise assumption.

\begin{lemma}
\[
L_{p}(\sigma) = \sum_{1\leq i < j \leq n}\left[P_{i,j}\mathbb{I}\{\sigma(i) > \sigma(j)\} + P_{j,i}\mathbb{I}\{\sigma(i) < \sigma(j)\}\right]
\]
\end{lemma}

\subsection{Generalization error}

\begin{theorem}{\sc (Generalization bound)}
Let $\widehat{\sigma}_{N}$ be the output of any aggregation procedure on the dataset $\DN$. Then for any $\delta >0$, one has with probability higher than $1-\delta$
\[
\widehat{L}_{N}(\widehat{\sigma}_{N}) - L(\widehat{\sigma}_{N}) \leq \binom{n}{2}\sqrt{\frac{\log(n(n-1)/\delta)}{2N}}.
\]
\end{theorem}

\subsection{Excess risk}

Such rate bounds are classically based on the fact that any minimizer $\widehat{\sigma}_n$ of \eqref{eq:ERM1} fulfills
\begin{equation}\label{eq:classic}
L(\widehat{\sigma}_N)-L^*\leq 2\max_{\sigma\in \mathfrak{S}_n}\vert \widehat{L}_N(\sigma)- L(\sigma)\vert.
\end{equation}
As the cardinality of the set $\Sn$ of median candidates is finite, they can be directly derived from bounds (tail probabilities or expectations) for the absolute deviations of i.i.d. sample means $\widehat{L}_N(\sigma)$ from their expectations, $\vert\widehat{L}_N(\sigma)- L(\sigma)\vert$. This immediately leads to the universal bound in expectation for ERM in the context of Kemeny ranking aggregation stated below, of order $O(1/\sqrt{N})$ unsurprisingly (see the Supplementary Material for upper confidence probability bounds).
% \begin{equation}
 %\mathbb{P}\left\{  \vert\widehat{L}_N(\sigma)- L(\sigma)\vert  >t \right\}.
 %\end{equation}
\begin{proposition}\label{prop:ERM}
	Let $\widehat{\sigma}_N$ be any Kemeny empirical median based on i.i.d. training data $\Sigma_1,\; \ldots,\; \Sigma_N$, \textit{i.e.} minimizer of \eqref{eq:emp_risk} over $\Sn$ with $d=d_{\tau}$. For all $N\geq 1$, we have:
	\begin{equation}\label{eq:ERM_bound_gen}
	\mathbb{E}\left[L(\widehat{\sigma}_N)-L^*\right]\leq \frac{n(n-1)}{2\sqrt{N}}.
	\end{equation}
\end{proposition}

The (sketch of) proof is given in the Appendix section, and the bound in probability is given in the supplementary material (see Proposition \ref{prop:ERM} bis). Notice that, as an examination of the related argument shows, the rate bound \eqref{eq:ERM_bound_gen} hold true for any distance $d$ on $\Sn$, except that $n(n-1)$ must be then replaced by $2\max_{(\sigma,\sigma')\in \Sn^2}d(\sigma,\sigma')$.

\begin{remark} % A reformuler
 As the problem \eqref{eq:ERM1} is NP-hard in general, one uses in practice an optimization algorithm to produce an approximate solution $\widetilde{\sigma}_N$ of the original minimization problem, with a control of the form: $\widehat{L}_N(\widetilde{\sigma}_N)\leq \min_{\sigma\in \Sn}\widehat{L}_N(\sigma)+\rho$, where $\rho>0$ is a tolerance fixed in advance, see \textit{e.g.} \cite{JKS16}. As pointed out in \cite{BB08nips}, a bound for the expected excess of risk of $\widetilde{\sigma}_N$ is then obtained by adding the quantity $\rho$ to the estimation error estimate $n(n-1)\sqrt{\log(2n!)/(2N)}$ given in Proposition \ref{prop:ERM}.
\end{remark}


We now establish the tightness of the upper bound for empirical Kemeny aggregation stated in Proposition \ref{prop:ERM}. Precisely, the next theorem provides a lower bound of order $1\sqrt{N}$ for the quantity below, referred to as the \textit{minimax risk},
\begin{equation}\label{eq:minimax}
\mathcal{R}_N\overset{def}{=}\inf_{\sigma_N}\sup_P \mathbb{E}_P\left[L_P(\sigma_N)-L_P^*  \right],
\end{equation}
where the supremum is taken over all probability distributions on $\mathfrak{S}_n$ and the infimum is taken over all empirical median candidate $\sigma_N=\sigma_N(\Sigma_1,\; \ldots,\, \Sigma_N)$ in $\mathfrak{S}_n$ based on the observation of $N$ independent realizations of $P$: $\Sigma_1,\; \ldots,\, \Sigma_N$.
%This quantity, referred to as the \textit{minimax risk}, quantifies the difficulty of the learning problem since it is the smallest possible maximum (worst case) risk.
%We use the classical Le Cam method for bouding the minimax risk of the ranking aggregation problem in the case of Kendall's tau distance.


\begin{proposition}
	\label{prop:minimax_bound}
	Considering Kemeny aggregation w.r.t. the Kendall's $\tau$ distance, the minimax risk is lower bounded as follows:
	\begin{equation*}
	\mathcal{R}_N \geq  \frac{n(n-1)}{64 \sqrt{N}}.
	\end{equation*}	
\end{proposition}
The result above shows that no matter the method used for picking a median candidate from $\Sn$ based on the training data, one may find a distribution such that the expected excess of risk is larger than $n(n-1)/(64\sqrt{N})$, a bound of the same order as that achieved by empirical Kemeny medians. Its proof relies on the classical Le Cam's method and is sketched in the Appendix section.
Hence, the minimax learning rate for Kemeny aggregation is thus of order $O(1/\sqrt{N})$ and empirical Kemeny aggregation is a minimax strategy.
The result stated in Proposition \ref{prop:ERM} is universal and cannot be improved in a certain sense. However, it is also desirable to understand the circumstances under which the risk excess of empirical Kemeny medians is small. Following in the footsteps of results obtained in binary classification, it is the purpose of the subsequent analysis to exhibit conditions guaranteeing exponential convergence rates in Kemeny aggregation.

\subsection{Dispersion}

\begin{remark} {\sc (Alternative dispersion measure)}
	An alternative measure of dispersion which can be more easily estimated than $L^*=L(\sigma^*)$ is given by
	\begin{equation}\label{eq:disp_meas2}
	\gamma(P)=\frac{1}{2}\mathbb{E}[ d(\Sigma,\Sigma') ],
	\end{equation}
	where $\Sigma'$ is an independent copy of $\Sigma$. One may easily show that $\gamma(P)\leq L^*\leq 2\gamma(P)$. The estimator of \eqref{eq:disp_meas2} with minimum variance among all unbiased estimators is given by the $U$-statistic
	\begin{equation}\label{eq:emp_disp_meas2}
	\widehat{\gamma}_N=\frac{2}{N(N-1)}\sum_{i<j}d(\Sigma_i,\Sigma_j).
	\end{equation}
	In addition, we point out that confidence intervals for the parameter $\gamma(P)$ can be constructed by means of Hoeffding/Bernstein type deviation inequalities for $U$-statistics and a direct (smoothed) bootstrap procedure can be applied for this purpose, see \cite{Lahiri93}. In contrast, a bootstrap technique for building CI's for $L^*$ would require to solve several times an empirical version of \eqref{eq:median_pb} based on bootstrap samples.
	%\begin{itemize}
	% 	\item A validity framework for bootstrapping the $U$-statistic \eqref{eq:emp_disp_meas2} may require to implement a smoothing procedure (see \cite{Lahiri93}), {\bf find a bootstrap technique and conditions guaranteeing its asymptotic validity}.
	%\item By means of the triangular inequality, we have $\gamma(P)\leq L^*$ and, by definition, $\mathbb{E}[d(\sigma^*,\Sigma)]\leq \mathbb{E}[d(\Sigma',\Sigma)\mid \Sigma'  ]$ almost-surely, so that $L^*\leq 2\gamma(P)$ by taking the expectation w.r.t. to $\Sigma'$. Considering the analogy with the Hilbertian case, {\bf find conditions ensuring that \eqref{eq:disp_meas2} is an even more accurate approximant of $L^*$}.
	%\end{itemize}
\end{remark}

\begin{remark}{\sc (Dispersion estimates)} % A reformuler, pas besoin de la stochastic transitivity
In the stochastically transitive case, one may get an estimator of $L^*$ by plugging the empirical estimates $\widehat{p}_{i,j}$ into \eqref{eq:Kendall_risk_min}:
	\begin{eqnarray}
	\widehat{L}^*&=&\sum_{i<j}\min\{\widehat{p}_{i,j},\; 1-\widehat{p}_{i,j}  \}\\
	&=&\sum_{i<j}\left\{  \frac{1}{2}-\left\vert \widehat{p}_{i,j}-\frac{1}{2}\right\vert  \right\}. 
	\end{eqnarray}
	One may easily show that the related MSE is of order $O(1/N)$: $\mathbb{E}[(\widehat{L}^*-L^*  )^2]\leq \frac{ n^2(n-1)^2}{16N}$, see the Supplementary Material.
	Notice also that, in the Kendall's $\tau$ case, the alternative dispersion measure \eqref{eq:disp_meas2} can be expressed as
	\begin{equation}
	\gamma(P)=\sum_{i<j}p_{i,j}(1-p_{i,j}),
	\end{equation}
	and that the plugin estimator of $\gamma(P)$ based on the $\widehat{p}_{i,j}$'s coincides with \eqref{eq:emp_disp_meas2}.
\end{remark}






