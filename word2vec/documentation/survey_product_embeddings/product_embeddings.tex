\documentclass[12pt]{article}
\usepackage{amssymb}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{epsfig}
\usepackage{stmaryrd}
\usepackage{url}
\usepackage{algorithm}
\usepackage{algorithmic}




\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{assumption}[theorem]{Assumption}
%\newtheorem{remark}[theorem]{Remark}
\newtheorem{rema}{Remark}
\newenvironment{remark}{\begin{rema} \rm}{\end{rema}}
%\newtheorem{example}[theorem]{Example}
\newtheorem{exam}{Example}
\newenvironment{example}{\begin{exam} \rm}{\end{exam}}

\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\newenvironment{proof}[1][Proof]{\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\def\R{\mathbb{R}}
\def\Rd{\R^d}
\def\E{\mathbb E}
\def\e{\mathbf e}
\def\i{\mathbf i}
\def\j{\mathbf j}
\def\EXP{{\E}}
\def\expec{{\EXP}}
\def\PROB{{\mathbb P}}
\def\S{\mathcal{S}}
\def\var{{\rm Var}}
\def\shat{{\mathbb S}}
\def\IND#1{{\mathbb I}_{{\left[ #1 \right]}}}
%\def\IND#1{{\mathbb I}_{{ #1 }}}
\def\I{{\mathbb I}}
\def\pr{\PROB}
\def\prob{\PROB}
\def\wh{\widehat}
\def\ol{\overline}
\newcommand{\deq}{\stackrel{\scriptscriptstyle\triangle}{=}}
\newcommand{\defeq}{\stackrel{\rm def}{=}}
\def\isdef{\defeq}
\newcommand{\xp}{\mbox{$\{X_i\}$}}
\def\diam{\mathop{\rm diam}}
\def\argmax{\mathop{\rm arg\, max}}
\def\argmin{\mathop{\rm arg\, min}}
\def\essinf{\mathop{\rm ess\, inf}}
\def\esssup{\mathop{\rm ess\, sup}}
\def\supp{\mathop{\rm supp}}
\def\cent{\mathop{\rm cent}}
\def\dim{\mathop{\rm dim}}
\def\sgn{\mathop{\rm sgn}}
\def\logit{\mathop{\rm logit}}
\def\proof{\medskip \par \noindent{\sc proof.}\ }
\def\proofsketch{\medskip \par \noindent{\sc Sketch of proof.}\ }
%% Not in latex2e:
%  \def\qed{\hfill $\Box$ \medskip}
%  \def\qed{$\Box$}
\def\blackslug{\hbox{\hskip 1pt \vrule width 4pt height 8pt depth 1.5pt
\hskip 1pt}}
\def\qed{\quad\blackslug\lower 8.5pt\null\par}
\newcommand{\F}{{\cal F}}
\newcommand{\G}{{\cal G}}
\newcommand{\X}{{\cal X}}
\newcommand{\D}{{\cal D}}
\newcommand{\cR}{{\cal R}}
\newcommand{\COND}{\bigg\vert} % for conditional expectations
%
\def\C{{\cal C}}
\def\roc{\rm ROC}
\def\auc{\rm AUC}
\def\B{{\cal B}}
\def\A{{\cal A}}
\def\N{{\cal N}}
\def\F{{\cal F}}
\def\L{{\cal L}}
\def\eps{{\varepsilon}}
\def\Var{{\rm Var}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\Prob}[1]{\mathbb{P}\left\{ #1 \right\} }

\allowdisplaybreaks

\begin{document}
\begin{center}
\large{Product Embeddings for Recommender Systems}
\end{center}
%\hline

\bigskip

\section{Recommender systems}

\noindent Recommendation
systems predict which products a user will most
likely be interested in either by exploiting purchase behavior
of users with similar interests (referred to as collaborative
filtering \cite{linden2003amazon}) or by using user’s historical interaction with
other products (i.e., context-based recommendation \cite{zhang2013predicting}).\\

\noindent \textbf{State of the art.} \\
Existing methods for recommender systems can roughly be categorized into collaborative filtering (CF) based methods, content-based (CB) methods and hybrid methods. 

Content-based methods make use of the user or product content
profiles. In practice, CF methods are more popular
because they can discover interesting associations between
products without requiring the heavy knowledge collection
needed by the content-based methods. However, CF methods
suffer from cold-start problem in which no or few interaction
are available with niche or new items in the system.
In recent years, more sophisticated methods, namely latent
factor models, have been developed to address the data sparsity
problem of CF methods.\\

\noindent \textbf{Latent Factor Models.}\\

Matrix factorization (MF) methods became popular
after their success in the Netflix competition. These
methods learn low-rank decompositions of a sparse user-item
interaction matrix by minimizing the square loss over the reconstruction
error. The dot product between the resulting
user and item latent vectors is then used to perform recommendation.
Several modifications have been proposed to better align
MF methods with the recommendation objective, for instance,
Bayesian Personalized Ranking (\cite{rendle2009bpr}) and Logistic MF
(to read). The former learns user and item latent vectors through
pairwise ranking loss to emphasize the relevance-based ranking
of items. The latter models the probability that a user
would interact with an item by replacing the square loss in
MF method with the logistic loss .
One of the first methods that learns user and item latent
representations through neural network was proposed
in \cite{salakhutdinov2007restricted}. The authors utilized Restricted Boltzmann Machines
to explain user-item interaction and perform recommendations.
Recently, shallow neural networks has been gaining
attention thanks to the success of word embeddings in various
NLP tasks, the focus being on Word2Vec model \cite{mikolov2013efficient}. An
application of Word2Vec to the recommendation task was
proposed in \cite{grbovic2015commerce}, called Prod2Vec model. It generates product
embeddings from sequences of purchases and performs
recommendation based on most similar products in the embedding
space. 
To further help overcoming cold-start problem, recent
works focused on developing hybrid methods by combining
latent factor models with content information.\\

\noindent\textbf{Latent Factor Models with content information.}\\

Many techniques have been used recently to create unified
representations from latent factors and content information.
One way to integrate user and item content information is
to use it to estimate user and item latent factors through
regression \cite{agarwal2009regression}. Another approach is to learn latent factors
for both CF and content features, known as Factorization
Machines \cite{rendle2011fast}.
Tensor factorization have been suggested as a generalization
of MF for considering additional information \cite{karatzoglou2010multiverse}.
In this approach, user-item-content matrix is factorized in
a common latent space. The authors in \cite{fang2011matrix} propose cofactorization
approach where the latent user and item factors
are shared between factorizations of user-item matrix
and user and item content matrices. Similar to \cite{li2010improving}, they
also assigned weights to negative examples based on useritem
content-based dissimilarity.
Graph-based models have also been used to create unified
representations. In particular, in \cite{yang2011like} user-item interactions
and side information are modeled jointly through user and
item latent factors. User factors are shared by the user-item
interaction component and the side information component.
\cite{gunawardana2009unified} leans the interaction weights between
user actions and various features such as user and
item metadata. The authors use a unified Boltzmann Machines
to make a prediction.



\section{Word embeddings}

Neural language models have been proposed to address
these issues, inducing low-dimensional, distributed embeddings
of words by means of neural networks (\cite{bengio2003neural}, \cite{collobert2011natural}, \cite{turian2010word}). Such
approaches take advantage of the word order in text documents,
explicitly modeling the assumption called Distributional Hypothesis, that closer words
in the word sequence are statistically more dependent. Historically,
inefficient training of the neural network-based models
has been an obstacle to their wider applicability, given
that the vocabulary size may grow to several millions in
practical tasks. However, this issue has been successfully
addressed by recent advances in the field, particularly with
the development of highly scalable continuous bag-of-words
(CBOW) and skip-gram (SG) language models (\cite{mikolov2013distributed}, \cite{mikolov2013efficient}) for
learning word representations. These powerful, efficient models
have shown very promising results in capturing both syntactic
and semantic relationships between words in largescale
text corpora, obtaining state-of-the-art results on a
plethora of NLP tasks.\\

Despite many variants but each neural languague model reflects the same main idea: words are placed in a low-dimensional latent space , and a word's probability depends on the distance to other words.


\section{Product embeddings with neural language models}

\subsubsection{First paper}

\noindent "E-commerce in Your Inbox:
Product Recommendations at Scale" (\cite{grbovic2015commerce}).\\


The authors propose a methodology for
the task of product recommendations, which leverages information
about prior purchases determined from e-mail receipts.
To address this task they propose to learn representation
of products in a real-valued,low-dimensional space, by using a neural language model applied to a time series of user purchases. As a result, products with similar contexts (i.e., their surrounding purchases) are mapped to vectors that are nearby in the embedding space. They further cluster the product vectors and model transition probabilities between clusters. The closest products in the embedding space from the most probable clusters are used to form final recommendations for a product.\\

\noindent \textbf{Prod2vec} \\
\noindent Given a set $S$ of e-mail receipt logs obtained
from $N$ users, where user’s log $s = (e_1,\dots,  e_M) \in S
$ is defined as an uninterupted sequence of $M$ receipts, and
each e-mail receipt $em = (p_{m1}, p_{m2},\dots p_{m T_m})$ consists of $T_m$
purchased products, our objective is to find D-dimensional
real-valued representation $v_p \in \mathbb{R}^D$ of each product $p$ such
that similar products lie nearby in the vector space. The prod2vec model involves learning vector
representations of products from e-mail receipt logs by
using a notion of a purchase sequence as a “sentence” and
products within the sequence as “words”, borrowing the terminology
from the NLP domain. The goal is to maximize the objective function:
\begin{equation*}
L = \sum_{s \in S} \sum_{p_i \in S} \sum_{-c \le j \le c} log(\mathbb{P}[p_{i+j}| p_i])
\end{equation*}

where products from the same e-mail receipt are ordered arbitrarily.
Probability $\mathbb{P}[p_{i+j}| p_i]$ of observing a neighboring
product $p_{i+j}$ given the current product $p_i$ is defined using
the soft-max function:
\begin{equation}
\label{eq:predicted_conditional_probability}
\mathbb{P}[p_{i+j}| p_i] = \frac{exp(v_{p_i}^T v^{'}_{p_{i+j}})}{\sum_{p=1}^{P} exp(v_{p_i}^T v^{'}_{p})}
\end{equation}

\noindent \textbf{Bagged-Prod2vec} \\
\noindent In order to account for the fact that
multiple products may be purchased at the same time, they
propose a modified skip-gram model that introduces a notion
of a shopping bag. As depicted in Figure 3, the model
operates at the level of e-mail receipts instead at the level
of products. Product vector representations are learned by
maximizing a modified objective function over e-mail sequences
$s$, defined as follows
\begin{equation*}
L = \sum_{s \in S} \sum_{e_m \in s} \sum_{1 \le j \le n} \sum_{k=1, \dots, T_m} log(\mathbb{P}[e_{m+j}| p_{mk}])
\end{equation*}

\noindent Probability $\mathbb{P}[e_{m+j}| p_{mk}]$ of observing products from neighboring
e-mail receipt $e_{m+j} , e_{m+j} = (p_{m+j,1},\dots, p_{m+j,T_m})$,
given the $k$-th product from $m$-th e-mail receipt reduces to
a product of probabilities $\mathbb{P}[e_{m+j}| p_{mk}]= \mathbb{P}[p_{m+j,1}| p_{mk}]\times \dots \times \mathbb{P}[p_{m+j, T_m}| p_{mk}]$.\\


\noindent \textbf{Product recommendation}\\
\noindent  Given a purchased product, the first possibilitity is to compute the cosine similarities  between products in their low-dimensional representations and recommend the top $K$ most similar products. But to be able to make meaningful and diverse
suggestions about the next product to be purchased,
the authors further cluster the product vectors (with a K-means based on their cosine similarities) and model transition
probabilities between clusters as the following: they assume that purchasing a product from any of the $C$ clusters after a purchase from cluster $c_i$ follows
a multinomial distribution $M(\theta_{i1}, \theta_{i2},\dots \theta_{iC})$, where $\theta_{ij}$ is the probability that a purchase from cluster $c_i$ is followed
by a purchase from cluster $c_j$. These parameters are estimated with the MLE approach:
\begin{equation*}
\hat{\theta}_{ij}=\frac{\text{count of times $c_i$ purchase was followed by $c_j$}}{\text{count of $c_i$ purchases}}
\end{equation*}

\noindent The closest products in the
embedding space from the most probable clusters are used
to form final recommendations for a product: in order to recommend a new product given a purchased
product $p$, we first identify which cluster $p$ belongs to (e.g.,
$p \in c_i$). Next, we rank all clusters $c_j , j = 1, \dots , C$, by the
value of $\theta_{ij}$ and consider the top ones as top-related clusters
to cluster $c_i$. Finally, products from the top clusters are
sorted by their cosine similarity to $p$, and we use the top $K$
products as recommendations.\\

\noindent \textbf{Note 1.} They also test a user-and-product embedding but the accuracy decreases quickly.


\noindent \textbf{Note 2.} The authors have proposed an extension to the model by taking into account the textual metadata of the products.

\subsubsection{Second paper}

"Meta-Prod2Vec - Product Embeddings Using
Side-Information for Recommendation"
(\cite{vasile2016meta}).\\


 
\noindent The authors propose an extension of \cite{grbovic2015commerce}, with the algorithm \textbf{Meta-Prod2Vec}, which is
a general approach for adding categorical side-information
to the Prod2Vec model in a simple and efficient way.

\noindent In \cite{pennington2014glove} the authors show that the Word2Vec (and so the Prod2Vec) objective can be written as the optimization problem of minimizing the weighted cross entropy between the empirical and the modeled conditional distributions of \textbf{context products given the target product} (this represents the Word2Vec skip-gram model which usually does better on large dataset).:
\begin{align*}
L_{P2V} & = L_{J|I}\\
&= \sum_{i,j} \left( -X_{ij}^{POS} log q_{j|i}(\theta) - \left(X_i - X_{ij}^{POS}\right) log (1-q_{j|i}(\theta))\right)\\
&=\sum_{i,j} X_i \left( -p_{j|i} log q_{j|i}(\theta) - p_{-j|i} log q_{-j|i}(\theta)\right)\\
&=\sum_{i} X_i H\left(p_{.|i}, q_{.|i}(\theta)\right)
\end{align*}
\noindent where $H\left(p_{.|i}, q_{.|i}(\theta)\right)$ is the cross entropy between the empirical probability $p_{.|i}$ of seeing any product in the output space $J$ conditioned on the input product $i \in I$ and the predicted conditional probability $q_{.|i}$ defined as in \eqref{eq:predicted_conditional_probability}:
\begin{equation*}
q_{j|i} = \frac{exp(v_{i}^T v_{j})}{\sum_{p=1}^{P} exp(v_{i}^T v_{j})}
\end{equation*}
\noindent and $X_i$ is the input frequency of product $i$ and $X_{i,j}^{POS}$ is the number of times the pair of products $(i,j)$ has been observed in the training data.\\

\noindent However, the product embeddings generated by Prod2Vec only take into account the information of the user purchase sequence, that is the local co-occurence information. The Meta-Prod2Vec loss extends the Prod2Vec loc by taking into account four additional interaction terms involving the item's metadata:
\begin{equation*}
L_{MP2V}= L_{J|I} + \lambda \left( L_{M|I} +  L_{J|M} +  L_{M|M} + L_{I|M} \right) 
\end{equation*}
\noindent where, to summarize, , $L_{J|I}$ and $L_{M|M}$ encode the loss terms coming
from modeling the likelihood of the sequences of items
and metadata separately, $L_{I|M}$ represents the conditional
likelihood of the item id given its metadata and $L_{J|M}$ and
$L_{M|I}$ represent the cross-item interaction terms between the
item ids and the metadata.

\section{Product embeddings with exponential families}

"Exponential Family Embeddings" Maja Rudolph, Francisco Ruiz and Stephan Mandt, David Blei.\\

\noindent The authors describe the \textbf{Exponential family embeddings}, which generalize the idea of Word2Vec to other types of high-dimensional data.
Their motivation is that the former can also benefit from the same assumption, i.e a data point is governed by the other data in its context. In particular, they apply it to shopping baskets. Their class of model relies on three ingredients: the \textit{context}, the  \textit{conditional exponential family}, and the \textit{embedding structure}.  The \textbf{context} function determines which other data points are at play: each data point $i$ has a context $c_i$, i.e a set of indices of other data points. The conditional distribution is an appropriate exponential family. The embedding structure determines which embeddings are used whether the data point is considered as data or in the context of another point.\\



\noindent In their model, there are two latent variables per data index $i$, an embedding ($\rho[i]$) and a context vector ($\alpha[i]$). Each data point $x_i$ is modeled conditioned on its context $x_{c_i}$ and latent variables:
\begin{equation*}
x_i | x_{c_i} \sim Exp Fam (\eta_i(x_{c_i}),t(x_i))
\end{equation*}
\noindent where the $Exp Fam$ law can be Poisson for counts, Gaussian for reals, Bernoulli for binary... And the natural parameter combines the embedding with the context vectors:
\begin{equation*}
\eta(x_{c_i})= f \left( \rho[i]^T \sum_{j \in c_i} \alpha[j] x_j \right)
\end{equation*}
\noindent To summarize, the embedding of the ith element $\rho[i]$ helps govern its distribution, and the context vector $\alpha[i]$ helps govern the distribution of elements for which $i$ appear in their context. The latent variables interact in the conditional: but how depends on which indices are in context and which one is modeled.\\



\noindent \textbf{Shopping basket example.} Imagine a grocery store with the following items available: jam, coca, peanut butter 1, peanut butter2, bread, and pizza. Customers often either buy (coca, pizza) or (peanut butter, jam, bread); and they only buy one type of peanut butter at a time. Items bought together are \textbf{complements}, the peanut butters are \textbf{substitutes}. The authors want to capture this purchase behavior.
They endow each item with two locations: an embedding $\rho$ and a context vector $\alpha$.The conditional probability of each item depends on its embedding and the context vectors of the other items in the basket:
\begin{equation*}
x_{b,i} |x_{b,-i} \sim Poisson(exp \left\{ \rho_i^T \sum_{j \ne i}\alpha_j x_{b,j}\right\})
\end{equation*}
\noindent They fit the embeddings and context vectors to data from the grocery store. When looking at the representations of the items in the latent space, we can see \ref{fig:exponential_family} that bread and jelly are bought together whereas peanut butter 1 is never bought with peanut butter 2


\begin{figure}[ht!]
	\label{fig:exponential_family}
	\centering
	\includegraphics[width=80mm]{../../images/graph5.png}
	%\caption{A simple caption \label{overflow}}
\end{figure}



%\noindent \textbf{Evaluation.} The authors consider time ordered sequences of user interactions with the items. They fit the embedding model on the first (n-2) elements of each user sequence, use the performance on the (n-1)th element to bench the hyperparameters; and report their final by training on the first (n-1) items of each sequence and predicting the nth item. They use the last item in the training sequnce as the query item and recommend the most similar products 


\section{What we propose}

\subsection{The model}

\noindent Let $\llbracket n \rrbracket=\left\{ 1, 2, \dots, n \right\}$ be the set of objects and $U$ the set of users. Suppose that for each user $u \in U$, we observe his preferences in the form of $k_u$ pairwise comparisons : $i_1^{u} \succ j_1^{u}$, $\dots$, $i_{k_u}^{u} \succ j_{k_u}^{u}$. We can learn a general model involving vector representations for both users and pairwise preferences. Our objective is to find $D$-dimensional vector representations $w_{i,j}$ of pairwise preferences $i \succ j$ and $w_u$ of users $u$, so that similar preferences and users lie nearby in the embedding space (this leads to $n(n-1)\times |U| \times D$ parameters). The goal would then be to maximize such an objective function:
\begin{equation*}
L= \sum_{u \in U} \sum_{l=1}^{k_u} log(\mathbb{P}[w_{i_l^{u}, j_l^{u}}|w_u])
\end{equation*}
To do so, we define the individual probability that a user prefers item $i$ over item $j$ as :
\begin{equation*}
\mathbb{P}[w_{i, j}|w_u]=\frac{e^{\langle w_{i,j} \vert w_u \rangle}}{e^{\langle w_{i,j} \vert w_u \rangle}+e^{\langle w_{j,i} \vert w_u \rangle}}= \sigma(x_{uij})
\end{equation*}
where $\sigma$ is the logistic sigmoid:
\begin{equation*}
\sigma(x)=\frac{1}{1+e^{-x}}
\end{equation*}
And $x_{uij}=\langle w_{i,j} - w_{j,i} \vert w_u \rangle=\sum_{f=1}^{d} (w_{i,j, f}-w_{j,i,f}).w_{u,f}$
The gradient of the objective with respect to model parameters is:
\begin{align*}
\frac{\partial L}{\partial \Theta}&= \sum_{u \in U} \sum_{l=1}^{k_u} \frac{\partial log(\mathbb{P}[w_{i_l^{u}, j_l^{u}}|w_u])}{\partial \Theta}\\
&\propto \sum_{u \in U} \sum_{l=1}^{k_u} \frac{-e^{-x_{u i_{l}^{u} j_{l}^{u}}}}{1+e^{-x_{u i_{l}^{u} j_{l}^{u}}}}. \frac{\partial x_{u i_{l}^{u} j_{l}^{u}}}{\partial \Theta}
\end{align*}
The model parameters  are $\Theta=(W_1, W_2)$ where $W_1=[w_{u}]_{ \left\{ u\in U\right\} }$ is the matrix of representations of users and $W_2=[w_{i\succ j}]_{\left\{ (i,j) \in \llbracket n \rrbracket, i\ne j\right\}}$ is the matrix of representations pairwise preferences. In this case, the derivatives are: 
\begin{equation*}
\frac{\partial x_{uij}}{\partial \Theta}=\begin{cases}
(w_{i,j,f}-w_{j,i,f}) &\text{ if } \theta=w_{u,f}\\
w_{u,f} &\text{ if } \theta=w_{i,j,f}\\
-w_{u,f} &\text{ if } \theta=w_{j,i,f}\\
0 &\text{ else }
\end{cases}
\end{equation*}

\noindent To train the model, we use a classical stochastic gradient descent algorithm (see Algorithm~\ref{alg:learning}). We denote by $S_{train}$ the training set, composed of samples which represent a user $u$ and its pairwise preference $i \succ j$ for $(i,j) \in \llbracket n \rrbracket, i\ne j$ as the triple $(u,i,j)$.

\begin{algorithm}
	\caption{Optimize the model with stochastic gradient descent with learning rate $\lambda$ }
	\label{alg:learning}
	\begin{algorithmic}[1]
		\STATE Initialize $\Theta=(W_1, W_2)$
		\FOR{$i=1\dots n_{iter}$}
		\STATE \text{draw ($u,i,j$) from $S_{train}$}
		\STATE \text{update} $\Theta \leftarrow \Theta - \lambda \frac{-e^{-\hat{x}_{u i j}}}{1+e^{-\hat{x}_{u i j}}}. \frac{\partial \hat{x}_{uij}}{\partial \Theta}$
		\ENDFOR
		\STATE 
		\STATE
	\end{algorithmic}
\end{algorithm}


\subsection{Experiments}

The first dataset we consider is the \textit{Cars preference dataset}\footnote{\url{http://users.cecs.anu.edu.au/~u4940058/CarPreferences.html}, First experiment.}. It consists of pairwise comparisons of sixty users between ten different cars. The dataset is composed of aroung 3000 samples of the form $(u,i,j)$: user $u$ prefers item $i$ over item $j$. The training process is the following:
\begin{itemize}
	\item We generate random vectorial embeddings for each user $w_u$ and each pairwise comparison $w_{i,j}$ and $w_{j,i}$. At this time we take  $D=10$.
	\item We divide the dataset in a train set $S_{train}$ and test set $S_{test}$(80\% - 20\% of the samples). We could do it without any constraints or by removing for each user, one pairwise comparison.
 	\item We maximize the log-likelihood on the train set with a classical learning algorithm, stochastic gradient descent (see Figure~\ref{fig:likelihood}).
\end{itemize}


\begin{figure}[ht!]
	\label{fig:likelihood}
	\centering
	\includegraphics[width=80mm]{../../expes/figures/likelihood_training_cars}
	\caption{Convergence of the log-likelihood.}
\end{figure}


\begin{figure}[ht!]
	\label{fig:acp}
	\centering
	\includegraphics[width=80mm]{../../expes/figures/results_acp_cars}
	\caption{PCA of the embedding vectors after training.}
\end{figure}

\noindent For evaluation, we take the ratio of good personalized predictions as the score. For each sample  $(u,i,j)$ in the test set, we predict the preference $i \succ j$ or $j \succ i$ conditionally to user $u$, according to the embeddings $\hat{w}_u$, $\hat{w}_{i,j}$ and $\hat{w}_{j,i}$ fitted on the train set. For each user $u$ in the test set, if $\mathbb{P}[\hat{w}_{i, j}|\hat{w}_u]> \mathbb{P}[\hat{w}_{i, j}|\hat{w}_u]$ then we predict $i\succ j$ and inversely otherwise. The ratio of good personalized predictions is the number of predictions which were correct for the test set. The results of the model (average and standard deviation of the score on 20 launches of the experiment) after 20~000 iterations are reported Table\ref{tab:exp1}, along with the results of a Bradley-Terry-Luce model trained on 100 iterations.

\begin{table}[h!]
	\centering
	\caption{Results on 20 different train/test splits of the datasets.}
	\label{tab:exp1}
	\begin{tabular}{ccc}
		\hline
		Model & Average score & Standard deviation\\
		\hline
		Embedding model & 0.8359 & 0.01535\\
		BTL & 0.6450 & 0.01634\\
		\hline
	\end{tabular}
\end{table}


\noindent Role of the dimension? See Figure~\ref{fig:score_dimension}. At this time I have no intuition of why the score is not so sensitive to the dimension.

\begin{figure}[ht!]
	\label{fig:score_dimension}
	\centering
	\includegraphics[width=80mm]{../../expes/figures/scores_dimensions}
	\caption{Convergence of the log-likelihood.}
\end{figure}


%\cite{rudolph2016exponential}.

\newpage
\bibliographystyle{apalike}
\bibliography{biblio}


\end{document}
 