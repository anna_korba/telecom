# -*- coding: utf-8 -*-
import gensim
from gensim.models import word2vec
from collections import defaultdict
import numpy as np
import seaborn as sb  
#http://www.folgertkarsdorp.nl/word2vec-an-introduction/
#http://rare-technologies.com/word2vec-tutorial/
#https://www.tensorflow.org/versions/r0.7/tutorials/word2vec/index.html

def Vocabulary():  
    dictionary = defaultdict()
    dictionary.default_factory = lambda: len(dictionary)
    return dictionary

def docs2bow(docs, dictionary):  
    """Transforms a list of strings into a list of lists where 
    each unique item is converted into a unique integer."""
    for doc in docs:
        yield [dictionary[word] for word in doc.split()]

sentences = [  
    'the king loves the queen', 
    'the queen loves the king',
    'the dwarf hates the king', 
    'the queen hates the dwarf',
    'the dwarf poisons the king', 
    'the dwarf poisons the queen']

#vocab = [s.split(' ') for s in sentences]

vocabulary = Vocabulary()  
sentences_bow = list(docs2bow(sentences, vocabulary)) 

V, N = len(vocabulary), 2  
WI = (np.random.random((V, N)) - 0.5) / N  
WO = (np.random.random((N, V)) - 0.5) / N  

# distance between two words
print np.dot(WI[vocabulary['dwarf']],  
             WO.T[vocabulary['hates']])

# posterior probability
p = (np.exp(-np.dot(WI[vocabulary['dwarf']],  
                   WO.T[vocabulary['hates']])) /
     sum(np.exp(-np.dot(WI[vocabulary['dwarf']],
                       WO.T[vocabulary[w]])) 
         for w in vocabulary))
print 'p '+str(p)  


##### TRAINING #####

target_word = 'king'  
input_word = 'queen'  
learning_rate = 1.0

#print 'iteration 0'
##words=vocabulary.keys()
#vectors = WI
#sb.plt.plot(vectors[:,0], vectors[:,1], 'o')  
#sb.plt.xlim(-0.6, 0.3)  
#sb.plt.ylim(-0.3, 0.5)  
#for word, x, y in zip(words, vectors[:,0], vectors[:,1]):  
#    sb.plt.annotate(word, (x, y), size=12)
#sb.plt.show()

# update hidden-output weights

for i in range(2):
    for word in vocabulary:  
        p_word_queen = (
            np.exp(-np.dot(WO.T[vocabulary[word]],
                          WI[vocabulary[input_word]])) / 
            sum(np.exp(-np.dot(WO.T[vocabulary[w]],
                              WI[vocabulary[input_word]]))
                for w in vocabulary))
        t = 1 if word == target_word else 0
        error = t - p_word_queen
        WO.T[vocabulary[word]] = (
            WO.T[vocabulary[word]] - learning_rate * 
            error * WI[vocabulary[input_word]])
    #print WO  
    
    # update input-hidden weights
    WI[vocabulary[input_word]] = WI[vocabulary[input_word]] - learning_rate * WO.sum(1)  
    #print WI
    #print 'new wi'+str(WI[vocabulary[input_word]] - learning_rate * WO.sum(1) )
    #iter+=1
    
    # display on a graph
    #words=vocabulary.keys()
    #vectors = WI
    #sb.plt.plot(vectors[:,0], vectors[:,1], 'o')  
    #sb.plt.xlim(-0.6, 0.3)  
    #sb.plt.ylim(-0.3, 0.5)  
    #for word, x, y in zip(words, vectors[:,0], vectors[:,1]):  
    #    sb.plt.annotate(word, (x, y), size=12)
    #sb.plt.show()
 
   
    
print '\n'
# computes the probability of each word given the input word
for word in vocabulary:  
    p = (np.exp(-np.dot(WO.T[vocabulary[word]],
                       WI[vocabulary[input_word]])) / 
         sum(np.exp(-np.dot(WO.T[vocabulary[w]],
                           WI[vocabulary[input_word]])) 
             for w in vocabulary))
    print word, p




##### on sushi

f = open('/Users/Anna/Desktop/RankAgg_and_kernels/experiments/datasets/sushi100.csv','r') 
sentences=[]
for line in f:
    l = map(int,line.split(' '))
    sentence=' '.join(str(e) for e in l)
    sentences.append(sentence)

vocabulary = Vocabulary()  
sentences_bow = list(docs2bow(sentences, vocabulary)) 

V, N = len(vocabulary), 2  
WI = (np.random.random((V, N)) - 0.5) / N  
WO = (np.random.random((N, V)) - 0.5) / N  

print 'before training'
print 'WO'+str(WO)+'\n'
print 'WI'+str(WI)+'\n'

words=vocabulary.keys()
vectors = WI
sb.plt.plot(vectors[:,0], vectors[:,1], 'o')  
sb.plt.xlim(-0.6, 0.3)  
sb.plt.ylim(-0.3, 0.5)  
for word, x, y in zip(words, vectors[:,0], vectors[:,1]):  
    sb.plt.annotate(word, (x, y), size=12)
sb.plt.show()

target_word = '3'  
input_word = '8'  
learning_rate = 1.0

for i in range(100):
    for word in vocabulary:  
        p_word_queen = (
            np.exp(-np.dot(WO.T[vocabulary[word]],
                          WI[vocabulary[input_word]])) / 
            sum(np.exp(-np.dot(WO.T[vocabulary[w]],
                              WI[vocabulary[input_word]]))
                for w in vocabulary))
        t = 1 if word == target_word else 0
        error = t - p_word_queen
        WO.T[vocabulary[word]] = (
            WO.T[vocabulary[word]] - learning_rate * 
            error * WI[vocabulary[input_word]])
    #print WO  
    
    # update input-hidden weights
    WI[vocabulary[input_word]] = WI[vocabulary[input_word]] - learning_rate * WO.sum(1)  
#print WI

print 'after training'
print 'WO'+str(WO)+'\n'
print 'WI'+str(WI)+'\n'
words=vocabulary.keys()
vectors = WI
sb.plt.plot(vectors[:,0], vectors[:,1], 'o')  
sb.plt.xlim(-0.6, 0.3)  
sb.plt.ylim(-0.3, 0.5)  
for word, x, y in zip(words, vectors[:,0], vectors[:,1]):  
    sb.plt.annotate(word, (x, y), size=12)
sb.plt.show()

test=dict(vocabulary)
for word in vocabulary:  
    p = (np.exp(-np.dot(WO.T[vocabulary[word]],
                       WI[vocabulary[input_word]])) / 
         sum(np.exp(-np.dot(WO.T[vocabulary[w]],
                           WI[vocabulary[input_word]])) 
             for w in vocabulary))
    print word, p
    test[word]=p

print 'sorted test'+str(sorted(test.iteritems(), key=lambda (k,v):(v,k), reverse=True))




##### multi word context

f = open('/Users/Anna/Desktop/RankAgg_and_kernels/experiments/datasets/sushi.csv','r') 
sentences=[]
for line in f:
    l = map(int,line.split(' '))
    sentence=' '.join(str(e) for e in l)
    sentences.append(sentence)


#vocab = [s.split(' ') for s in sentences]
vocabulary = Vocabulary()  
sentences_bow = list(docs2bow(sentences, vocabulary)) 

V, N = len(vocabulary), 2  
WI = (np.random.random((V, N)) - 0.5) / N  
WO = (np.random.random((N, V)) - 0.5) / N  

target_word = '1'  
context = ['8', '3']

# average context vectors
WI_context=[WI[vocabulary[word]] for word in context]
h = sum(WI_context)*(1/(len(WI_context)*1.0))

# update hidden-output weights
for word in vocabulary:  
    p = (np.exp(-np.dot(WO.T[vocabulary[word]], h)) / 
         sum(np.exp(-np.dot(WO.T[vocabulary[w]], h)) 
             for w in vocabulary))
    t = 1 if word == target_word else 0
    error = t - p
    WO.T[vocabulary[word]] = (
        WO.T[vocabulary[word]] - learning_rate * 
        error * h)


# update input-hidden weights (for each input word in the context)
for word in context:  
    WI[vocabulary[word]] = (
        WI[vocabulary[word]] - (1. / len(context)) *
        learning_rate * WO.sum(1))

print '\n'
# computes the probability of each word given the input word
test=dict(vocabulary)
for word in vocabulary:  
    p = (np.exp(-np.dot(WO.T[vocabulary[word]],
                       h)) / 
         sum(np.exp(-np.dot(WO.T[vocabulary[w]],
                           h)) 
             for w in vocabulary))
    test[word]=p
    print word, p
print 'sorted test'+str(sorted(test.iteritems(), key=lambda (k,v):(v,k), reverse=True))



#model_word2vec = gensim.models.Word2Vec(sentences, size=10, window=5, min_count=1, workers=1)
#model_word2vec.build_vocab(sentences)
#model_word2vec.train(sentences)
 

 