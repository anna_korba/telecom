# -*- coding: utf-8 -*-
import numpy as np
import os
import pandas as pd
import seaborn as sb  
import csv
import codecs
import math
from collections import Counter
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from gensim.models import Word2Vec


############################
#                          #
#          Data            #
#                          #
############################

# sushi 


Items=range(1, 101)
f = open('/Users/Anna/Desktop/RankAgg_and_kernels/experiments/datasets/sushi100.csv','r') 
sentences=[]
all_rankings=[]
for line in f:
    l = map(int,line.split(' '))
    l1=l[2:7]
    l2=l[7:]
    all_rankings.append(tuple([a+1 for a in l[2:]]))
    #sentence=' '.join(str(e+1) for e in l[2:])
   # sentences.append(sentence)
    sentence=' '.join(str(e+1) for e in l1)
    sentences.append(sentence)
    sentence=' '.join(str(e+1) for e in l2)
    sentences.append(sentence)
my_df = pd.DataFrame(sentences)
my_df.to_csv('/Users/Anna/Desktop/Experiments/word2vec/dataset2/sushi100clean.csv', index=False, header=False)


# movielens


############################
#                          #
#         Model            #
#                          #
############################

min_count = 1 #Terms that occur less than min_count number of times are ignored in the calculations.
size = 10 #nb of dimensions
window = 2 # Only terms hat occur within a window-neighbourhood of a term, in a sentence, are associated with it during training.
#sg – This defines the algorithm. If equal to 1, the skip-gram technique is used. DEFAULT=1. 


#csvReader = csv.reader(codecs.open('file.csv', 'rU', 'utf-16'))
def mycsv_reader(csv_reader): 
  while True: 
    try: 
      yield next(csv_reader) 
    except csv.Error: 
      # error handling what you want.
      pass
    continue 

class MySentences(object):
     def __init__(self, dirname):
         self.dirname = dirname
 
     def __iter__(self):
         for fname in os.listdir(self.dirname):
             reader = mycsv_reader(csv.reader(open('/Users/Anna/Desktop/Experiments/word2vec/dataset2/sushi100clean.csv', 'rU')))
             for line in reader:
                 yield line[0].split()
sentences = MySentences('/Users/Anna/Desktop/Experiments/word2vec/dataset2')



model = Word2Vec(sentences, min_count=min_count, size=size, window=window)


vocab = list(model.vocab.keys())
print vocab[:10]




#print model['king']
print model.similarity('20', '9') #both good
print model.similarity('70', '98') #both bad
print model.similarity('20', '98') 


############################
#                          #
#        Tests             #
#                          #
############################

def PL_fitting_list(L,n,nb_iterations):
    Gamma = dict.fromkeys(range(nb_iterations+1))
    Gamma[0] = dict.fromkeys(range(1,n+1),1/(1.0*n))
    w = Counter()
    for pi in L:
        for a in pi[:len(pi)-1]:
            w[a] += 1
    for k in range(nb_iterations):
        gamma = dict(Gamma[k])
        z = dict.fromkeys(range(1,n+1),0.0)
        for pi in L:
            x = []
            y = gamma[pi[len(pi)-1]]
            for i in range(2,len(pi)+1):
                y += gamma[pi[len(pi)-i]]
                x.append(y)
            y = 0
            for i in range(len(pi)-1):
                y += 1/x[len(x)-i-1]
                z[pi[i]] += y
            z[pi[len(pi)-1]] += y
        for t in range(1,n+1):
            if t in w:
                if z[t] != 0:
                    gamma[t] = w[t]/z[t]
        Gamma[k+1] = dict(gamma)
    return Gamma[nb_iterations]

scores_PL=PL_fitting_list(all_rankings,len(Items),10)

PL_consensus=sorted(scores_PL.iteritems(), key=lambda (k,v):(v,k), reverse=True)
PL_consensus=[str(a) for (a,b) in PL_consensus]


def acp_info(data):
    pca = PCA(n_components=2)
    pca.fit(data)
    #plt.bar(np.arange(len(pca.explained_variance_ratio_))+0.5, pca.explained_variance_ratio_)
    #plt.title("Variance expliquée")
    #plt.savefig("acp_var_axes",format='png')
    
    matrix_reduced = pca.transform(data)
    #plt.figure(figsize=(18,6))
    #plt.scatter(matrix_reduced[:, 0], matrix_reduced[:, 1])
    #plt.savefig("acp",format='png')
    return matrix_reduced


words=vocab
vectors = [model[word] for word in words]

test=acp_info(vectors)
#test=vectors

X=[test[i][0] for i in range(len(vectors))]
Y=[test[i][1] for i in range(len(vectors))]
sb.plt.plot(X, Y, 'o')  
colors=['red', 'orange', 'yellow', 'green', 'blue']

for word, x, y in zip(words, X, Y): 
    i= (PL_consensus.index(word))/20
    sb.plt.annotate(word, (x, y), size=12, color=colors[i])
sb.plt.show()