#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri May 12 11:35:36 2017

@author: Anna
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import csv
import pandas as pd
import numpy as np
from itertools import combinations
import random
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from Plackett_Luce import PL_fitting_list, PL_predict
import math


df=pd.read_csv('prefs1.csv')
df_g=df.groupby(['User ID'])
#df_g.get_group(1)
groups = dict(list(df_g))

subset = df[['User ID', ' Item1 ID', ' Item2 ID']]
dataset = [tuple(x) for x in subset.values]
random.shuffle(dataset)

split=int(0.8*len(dataset))
train_set=dataset[:split]
test_set=dataset[split:]


n_items=10
n_pairs= (n_items*(n_items-1)/2)
n_users=len(groups.keys())
d=10


# dictionaries with indexes
dict_users={v:u for (u,v) in enumerate(np.unique(df['User ID']))}
dict_pairs={v:u for (u,v) in enumerate(combinations(range(1,n_items+1),2))}
dict_pairs_={(j,i):u for ((i,j),u) in dict_pairs.items()}


# compute proba that user i prefers item i over item j

def compute_proba(W_users, W_pairs, W_pairs_,data):
    (u,i,j)=data
    idx_user=dict_users[u]
    #print 'index user '+str(idx_user)
    w_u=W_users[:, idx_user]
    #print 'w_u '+str(w_u)
    pair=(i,j)
     
    #print 'pair '+str(pair)
    if i<j:
        idx_pair=dict_pairs[pair]
        w_pair=W_pairs[:, idx_pair]
        w_pair_=W_pairs_[:, idx_pair]
        #w_pair_=-w_pair
    if i>j:
        idx_pair=dict_pairs_[pair]
        w_pair=W_pairs_[:, idx_pair]
        w_pair_=W_pairs[:, idx_pair]
        #w_pair_=-w_pair
    #print 'w_pair '+str(w_pair)
    #print 'w_pair_ '+str(w_pair_)
    sc=np.dot(w_pair, w_u)
    sc_=np.dot(w_pair_, w_u)
    p=(np.exp(sc))/((np.exp(sc)+np.exp(sc_))*1.0)
    return p

    
def compute_lnlikelihood(W_users, W_pairs, W_pairs_,dataset):
    L=0
    for data in dataset:
        p=compute_proba(W_users, W_pairs, W_pairs_,data)
        if math.isnan(p):
            print data
            raise Exception('proba is nan')
        L+=np.log(p)
    return L

def compute_derivative_coeff(w_u, w_pair, w_pair_):
    #(u,i,j)=data
    #pair=(i,j)
    #idx_user=dict_users_idx[u]
    #idx_pair=dict_pairs_idx[pair]
    
    #w_u=W_users[:, idx_user]
    #w_pair=W_pairs[:, idx_pair]
    #w_pair_=W_pairs_[:, idx_pair]
    
    x_uij=np.dot(w_pair-w_pair_,w_u)
    coeff=(-np.exp(-x_uij))/((1+np.exp(-x_uij))*1.0)
    return coeff

    
    
# exp


V_users=np.random.random((d, n_users))
V_pairs=np.random.random((d, n_pairs))
V_pairs_=np.random.random((d, n_pairs))

N=0
good=0

for data in test_set:
    (u,i,j)=data
    preference=(i,j)

    p_ij=compute_proba(V_users, V_pairs, V_pairs_,(u,i,j))
    p_ji=compute_proba(V_users, V_pairs, V_pairs_,(u,j,i))
    
    if p_ij>p_ji:
        prediction=(i,j)
    else:
        prediction=(j,i)

    if prediction==preference:
        good+=1

    N+=1
    
tx=good/(N*1.0)
print 'success rate of our model before training '+str(tx)



n_iter=100
lambd= 0.1
x=[]
listL=[]

for k in range(n_iter):
     print k
     gradient=0
     for data in train_set:
         (u,i,j)=data
         
         idx_user=dict_users[u]
         w_u=V_users[:, idx_user]
         
         pair=(i,j)
         
         if i<j:
             idx_pair=dict_pairs[pair]
             w_pair=V_pairs[:, idx_pair]
             w_pair_=V_pairs_[:, idx_pair]
             #w_pair_=-w_pair
         if i>j:
             idx_pair=dict_pairs_[pair]
             w_pair=V_pairs_[:, idx_pair]
             w_pair_=V_pairs[:, idx_pair]
             #w_pair_=-w_pair
              
         coeff= compute_derivative_coeff(w_u, w_pair, w_pair_)
         if math.isnan(coeff):
             print data
             raise Exception('coeff is nan')

         gradient+=coeff
     gradient=gradient/(1.0*len(train_set))
     # gradient descent
     w_u=w_u-lambd*gradient*(w_pair-w_pair_)
     w_pair=w_pair-lambd*gradient*w_u
     w_pair_=w_pair_-lambd*gradient*(-w_u)    
    
     print 'norm w_u '+str(np.linalg.norm(w_u))
     print 'norm w_pair '+str(np.linalg.norm(w_pair))
     print 'norm w_pair_ '+str(np.linalg.norm(w_pair_))
     # replace
     V_users[:, idx_user]=w_u
     
     if i<j:
         V_pairs[:, idx_pair]=w_pair
         V_pairs_[:, idx_pair]=w_pair_
     if i>j:
         V_pairs_[:, idx_pair]=w_pair
         V_pairs[:, idx_pair]=w_pair_
         
     if np.mod(k, 50)==0:
         #print k
         L=compute_lnlikelihood(V_users, V_pairs, V_pairs_,train_set)
         listL.append(L)
         x.append(k)


# to plot likelihood
plt.plot(x,listL)
plt.savefig('likelihood_training')
plt.show()

"""
vocab_users=[str(k) for k in dict_users.keys()]
vocab_pairs=[str(k) for k in dict_pairs.keys()]+[str(k) for k in dict_pairs_.keys()]
vocab=vocab_users+vocab_pairs
vectors_users = [W_users[:,i] for i in range(W_users.shape[1])]
vectors_pairs=[W_pairs[:,i] for i in range(W_pairs.shape[1])]
vectors_pairs_=[W_pairs_[:,i] for i in range(W_pairs.shape[1])]
vectors=vectors_users+vectors_pairs+vectors_pairs_


# ACP
def acp_info(data):
    pca = PCA(n_components=2)
    pca.fit(data)    
    matrix_reduced = pca.transform(data)
    return matrix_reduced


test=acp_info(vectors)
X=[test[i][0] for i in range(len(vectors))]
Y=[test[i][1] for i in range(len(vectors))]

for word, x, y in zip(vocab, X, Y): 
    text=plt.annotate(word, (x, y), size=12)
    text.set_fontsize(8)
    if word in vocab_users:
        plt.plot(x, y, 'o', markersize=4, color='red')  
    if word in vocab_pairs:
        plt.plot(x, y, 'o', markersize=4, color='blue')  
plt.savefig('results_acp')
plt.show()
"""
    
# success on test set

N=0
good=0


probabilities=[]
for data in test_set:
    (u,i,j)=data
    preference=(i,j)
    #print 'data :'+str(data)
    p_ij=compute_proba(V_users, V_pairs, V_pairs_,(u,i,j))
    p_ji=compute_proba(V_users, V_pairs, V_pairs_,(u,j,i))
    
    if p_ij>p_ji:
        prediction=(i,j)
    else:
        prediction=(j,i)
    
    probabilities.append(prediction)

    if prediction==preference:
        good+=1

    N+=1
    
tx=good/(N*1.0)
print 'success rate of our model after training '+str(tx)

# comparison to Plackett Luce?

train_plackett=[(i,j) for (u,i,j) in train_set]
Gamma = PL_fitting_list(train_plackett,n_items,50)


N=0
good=0

for data in test_set:
    (u,i,j)=data
    preference=(i,j)
    
    p_ij=PL_predict(Gamma,(i,j))
    p_ji=PL_predict(Gamma,(j,i))
    
    if p_ij>p_ji:
        prediction=(i,j)
    else:
        prediction=(j,i)
    if prediction==preference:
        good+=1
    #else:
        #print 'data, prediction:'
        #print data, prediction
        #print max(p_ij, p_ji)
    N+=1
    
tx=good/(N*1.0)
print 'success rate of PlackettLuce '+str(tx)