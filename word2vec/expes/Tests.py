#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import csv
import pandas as pd
import numpy as np
from itertools import combinations
import random
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from Plackett_Luce import PL_fitting_list, PL_predict
from Embedding_model import compute_proba,compute_lnlikelihood,compute_derivative_coeff


##############################
#                            #
#           Data             #
#                            #
##############################

df=pd.read_csv('datasets/prefs2.csv')
df_g=df.groupby(['User ID'])
#df_g.get_group(1)
groups = dict(list(df_g))

n_items=20
n_pairs= (n_items*(n_items-1)/2)
n_users=len(groups.keys())

# dictionaries with indexes
dict_users={v:u for (u,v) in enumerate(np.unique(df['User ID']))}
dict_pairs={v:u for (u,v) in enumerate(combinations(range(1,n_items+1),2))}
dict_pairs_={(j,i):u for ((i,j),u) in dict_pairs.items()}


subset = df[['User ID', ' Item1 ID', ' Item2 ID']]
dataset = [tuple(x) for x in subset.values]


##############################
#                            #
#       Functions            #
#                            #
##############################


def fit_embeddings(d,l, n_iter):
    W_users=np.random.random((d, n_users))
    W_pairs=np.random.random((d, n_pairs))
    W_pairs_=np.random.random((d, n_pairs))
        
    lambd= 0.1
    print 'fit model dimension '+str(d)
    for k in range(n_iter):
         
         if np.mod(k,1000)==0:
             print k
         data=random.choice(l)
         (u,i,j)=data
         
         idx_user=dict_users[u]
         w_u=W_users[:, idx_user]
         
         pair=(i,j)
         
         if i<j:
             idx_pair=dict_pairs[pair]
             w_pair=W_pairs[:, idx_pair]
             w_pair_=W_pairs_[:, idx_pair]
         if i>j:
             idx_pair=dict_pairs_[pair]
             w_pair=W_pairs_[:, idx_pair]
             w_pair_=W_pairs[:, idx_pair]
              
         coeff= compute_derivative_coeff(w_u, w_pair, w_pair_)
         
         # gradient descent
         w_u=w_u-lambd*coeff*(w_pair-w_pair_)
         w_pair=w_pair-lambd*coeff*w_u
         w_pair_=w_pair_-lambd*coeff*(-w_u)    
        
         # replace
         W_users[:, idx_user]=w_u
         
         if i<j:
             W_pairs[:, idx_pair]=w_pair
             W_pairs_[:, idx_pair]=w_pair_
         if i>j:
             W_pairs_[:, idx_pair]=w_pair
             W_pairs[:, idx_pair]=w_pair_

    return W_users, W_pairs, W_pairs_



def compute_score_embeddings(W1, W2, W3,l):
    N=0
    good=0
    
    for data in l:
        (u,i,j)=data
        preference=(i,j)
        
        p_ij=compute_proba(W1, W2, W3,(u,i,j))
        p_ji=compute_proba(W1, W2, W3,(u,j,i))
        
        if p_ij>p_ji:
            prediction=(i,j)
        else:
            prediction=(j,i)
        if prediction==preference:
            good+=1
        N+=1
        
    tx=good/(N*1.0)
    return tx

def compute_score_pl(Gamma,l):
    N=0
    good=0
    
    for data in l:
        (u,i,j)=data
        preference=(i,j)
        
        p_ij=PL_predict(Gamma,(i,j))
        p_ji=PL_predict(Gamma,(j,i))
        
        if p_ij>p_ji:
            prediction=(i,j)
        else:
            prediction=(j,i)
        if prediction==preference:
            good+=1
        N+=1
    tx=good/(N*1.0)
    return tx

##############################
#                            #
#           Exp1             #
#                            #
##############################
# Average results on several random splits train/test


n_exp1=20
scores_exp1=[]
scores_pl_exp1=[]

d=10
n_iter=20000
for i in range(n_exp1):
    
    random.shuffle(dataset)
    split=int(0.8*len(dataset))
    train_set=dataset[:split]
    test_set=dataset[split:]
    
    W_users, W_pairs, W_pairs_=fit_embeddings(d,train_set, n_iter)
    s=compute_score_embeddings(W_users, W_pairs, W_pairs_, test_set)
    scores_exp1.append(s)
    
    #PL
    train_plackett=[(i,j) for (u,i,j) in train_set]
    Gamma = PL_fitting_list(train_plackett,n_items,100)
    s=compute_score_pl(Gamma, test_set)
    scores_pl_exp1.append(s)

print scores_exp1
print scores_pl_exp1



##############################
#                            #
#           Exp2             #
#                            #
##############################

"""
# Test results for several dimensions

random.shuffle(dataset)
split=int(0.8*len(dataset))
train_set=dataset[:split]
test_set=dataset[split:]

dimensions=range(10,210,10)
scores_exp2=[]

n_iter=30000  
for d in dimensions:
    W_users, W_pairs, W_pairs_=fit_embeddings(d,train_set, n_iter)
    s=compute_score_embeddings(W_users, W_pairs, W_pairs_, test_set)
    scores_exp2.append(s)
    
print scores_exp2


plt.ylim(0,1)
plt.xlabel('dimension')
plt.ylabel('score')
plt.plot(dimensions, scores_exp2, color='red')
plt.savefig('figures/scores_dimensions')

plt.show()
"""