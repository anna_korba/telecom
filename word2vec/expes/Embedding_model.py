#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import csv
import pandas as pd
import numpy as np
from itertools import combinations
import random
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from Plackett_Luce import PL_fitting_list, PL_predict
import math

##############################
#                            #
#           Data             #
#                            #
##############################


######  CARS

df=pd.read_csv('datasets/prefs2.csv')
#df=df[df[' Is Control']==0] # remove control questions
df_g=df.groupby(['User ID'])
#df_g.get_group(1)
groups = dict(list(df_g))

subset = df[['User ID', ' Item1 ID', ' Item2 ID']]
dataset = [tuple(x) for x in subset.values]

unique_users=np.unique(df['User ID'])
n_items=20

df_users=pd.read_csv('datasets/users2.csv')


###### COMMON FOR ALL DATASETS

random.shuffle(dataset)

split=int(0.6*len(dataset))
train_set=dataset[:split]
test_set=dataset[split:]


n_pairs= (n_items*(n_items-1)/2)
n_users=len(unique_users)
d=10


# dictionaries with indexes
dict_users={v:u for (u,v) in enumerate(unique_users)}
dict_pairs={v:u for (u,v) in enumerate(combinations(range(1,n_items+1),2))}
dict_pairs_={(j,i):u for ((i,j),u) in dict_pairs.items()}

##############################
#                            #
#       Functions            #
#                            #
##############################

# compute proba that user i prefers item i over item j
def compute_proba(W_users, W_pairs, W_pairs_,data):
    (u,i,j)=data
    idx_user=dict_users[u]
    w_u=W_users[:, idx_user]
    
    pair=(i,j)
     
    if i<j:
        idx_pair=dict_pairs[pair]
        w_pair=W_pairs[:, idx_pair]
        w_pair_=W_pairs_[:, idx_pair]
        #w_pair_=-w_pair
    if i>j:
        idx_pair=dict_pairs_[pair]
        w_pair=W_pairs_[:, idx_pair]
        w_pair_=W_pairs[:, idx_pair]
        #w_pair_=-w_pair
        
    sc=np.dot(w_pair, w_u)
    sc_=np.dot(w_pair_, w_u)
    p=(np.exp(sc))/((np.exp(sc)+np.exp(sc_))*1.0)
    return p

    
def compute_lnlikelihood(W_users, W_pairs, W_pairs_,dataset):
    L=0
    for data in dataset:
        p=compute_proba(W_users, W_pairs, W_pairs_,data)
        L+=np.log(p)
    return L

def compute_derivative_coeff(w_u, w_pair, w_pair_):
    x_uij=np.dot(w_pair-w_pair_,w_u)
    coeff=(-np.exp(-x_uij))/((1+np.exp(-x_uij))*1.0)
    return coeff

    
    
##############################
#                            #
#         Experiment         #
#                            #
##############################

if __name__ == '__main__':
    
    # INIT

    V_users=np.random.random((d, n_users))
    V_pairs=np.random.random((d, n_pairs))
    V_pairs_=np.random.random((d, n_pairs))
    
    N=0
    good=0
    
    for data in test_set:
        (u,i,j)=data
        preference=(i,j)
    
        p_ij=compute_proba(V_users, V_pairs, V_pairs_,(u,i,j))
        p_ji=compute_proba(V_users, V_pairs, V_pairs_,(u,j,i))
        
        if p_ij>p_ji:
            prediction=(i,j)
        else:
            prediction=(j,i)
    
        if prediction==preference:
            good+=1
    
        N+=1
        
    tx_init=good/(N*1.0)
    print 'success rate of our model before training '+str(tx_init)
    
    
    # TRAIN
    
    n_iter=30000
    lambd= 0.1
    listI=[]
    listL=[]
    
    for k in range(n_iter):
         data=random.choice(train_set)
         (u,i,j)=data
         
         idx_user=dict_users[u]
         w_u=V_users[:, idx_user]
         
         pair=(i,j)
         
         if i<j:
             idx_pair=dict_pairs[pair]
             w_pair=V_pairs[:, idx_pair]
             w_pair_=V_pairs_[:, idx_pair]
         if i>j:
             idx_pair=dict_pairs_[pair]
             w_pair=V_pairs_[:, idx_pair]
             w_pair_=V_pairs[:, idx_pair]
              
         coeff= compute_derivative_coeff(w_u, w_pair, w_pair_)
         
         if math.isnan(coeff):
             print data
             raise Exception('coeff is nan')
             
         # gradient descent
         w_u=w_u-lambd*coeff*(w_pair-w_pair_)
         w_pair=w_pair-lambd*coeff*w_u
         w_pair_=w_pair_-lambd*coeff*(-w_u)    
         
         #print 'norm w_u '+str(np.linalg.norm(w_u))
         #print 'norm w_pair '+str(np.linalg.norm(w_pair))
         #print 'norm w_pair_ '+str(np.linalg.norm(w_pair_))
         # replace
         V_users[:, idx_user]=w_u
         
         if i<j:
             V_pairs[:, idx_pair]=w_pair
             V_pairs_[:, idx_pair]=w_pair_
         if i>j:
             V_pairs_[:, idx_pair]=w_pair
             V_pairs[:, idx_pair]=w_pair_
             
         if np.mod(k, 200)==0:
             print k
             L=compute_lnlikelihood(V_users, V_pairs, V_pairs_,train_set)
             listL.append(L)
             listI.append(k)
    
    
    # to plot likelihood
    plt.figure() 
    plt.plot(listI,listL)
    plt.xlabel('number of iterations')
    plt.ylabel('log-likelihood')
    plt.savefig('figures/likelihood_training_cars')
    plt.show()
    plt.close()
    
    print 'acp'
    vocab_users=[str(k) for k in dict_users.keys()]
    vocab_pairs=[str(k) for k in dict_pairs.keys()]+[str(k) for k in dict_pairs_.keys()]
    vocab=vocab_users+vocab_pairs
    vectors_users = [V_users[:,i] for i in range(V_users.shape[1])]
    vectors_pairs=[V_pairs[:,i] for i in range(V_pairs.shape[1])]
    vectors_pairs_=[V_pairs_[:,i] for i in range(V_pairs.shape[1])]
    vectors=vectors_users+vectors_pairs+vectors_pairs_
    
    
    # PCA
    """
    def acp_info(data):
        pca = PCA(n_components=2)
        pca.fit(data)    
        matrix_reduced = pca.transform(data)
        return matrix_reduced
    
    
    matrix_reduced=acp_info(vectors)
    X=[matrix_reduced[i][0] for i in range(len(vectors))]
    Y=[matrix_reduced[i][1] for i in range(len(vectors))]
    
    plt.figure()
    for word, x, y in zip(vocab, X, Y): 
        text=plt.annotate(word, (x, y), size=12)
        text.set_fontsize(8)
        if word in vocab_users:
            plt.plot(x, y, 'o', markersize=4, color='blue')  
        if word in vocab_pairs:
            plt.plot(x, y, 'o', markersize=4, color='red')  
    plt.savefig('figures/results_acp_cars')
    plt.show()
    plt.close()
     
    # users by gender
    plt.figure()
    for word, x, y in zip(vocab, X, Y): 
        if word in vocab_users:
            text=plt.annotate(word, (x, y), size=12)
            text.set_fontsize(8)
            idx_word=vocab_users.index(word)
            if df_users.iloc[idx_word][' Gender']==2:
                plt.plot(x, y, 'o', markersize=4, color='yellow')  
            elif df_users.iloc[idx_word][' Gender']==1:
                plt.plot(x, y, 'o', markersize=4, color='green')  
            else:
                plt.plot(x, y, 'o', markersize=4, color='black')  
    plt.savefig('figures/results_acp_cars_gender')
    plt.show()
    plt.close()
    """
        
    # EVAL
    print 'eval'
    N=0
    good=0
    
    for data in test_set:
        (u,i,j)=data
        preference=(i,j) # the true one

        p_ij=compute_proba(V_users, V_pairs, V_pairs_,(u,i,j))
        p_ji=compute_proba(V_users, V_pairs, V_pairs_,(u,j,i))
        
        if p_ij>p_ji:
            prediction=(i,j)
        else:
            prediction=(j,i)
            
        if prediction==preference:
            good+=1
        N+=1
    tx_final=good/(N*1.0)
    print 'success rate of our model before training '+str(tx_init)
    print 'success rate of our model after training '+str(tx_final)
    
    # comparison to Plackett Luce?
    
    train_plackett=[(i,j) for (u,i,j) in train_set]
    Gamma = PL_fitting_list(train_plackett,n_items,50)
    
    N=0
    good=0
    
    for data in test_set:
        (u,i,j)=data
        preference=(i,j)
        
        p_ij=PL_predict(Gamma,(i,j))
        p_ji=PL_predict(Gamma,(j,i))
        
        if p_ij>p_ji:
            prediction=(i,j)
        else:
            prediction=(j,i)
        if prediction==preference:
            good+=1
        N+=1
    tx_pl=good/(N*1.0)
    print 'success rate of PlackettLuce '+str(tx_pl)
