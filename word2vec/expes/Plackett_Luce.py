# -*- coding: utf-8 -*-
from itertools import *
from math import *
from collections import Counter


"""
This file contains the functions to compute and learn a Plackett-Luce (PL) model from observations.
The learning algorithm is the MM algorithm introduced in Hunter, 2004.
"""



# Computes the probability of an incomplete ranking pi with for a PL model of parameter gamma
def PL_predict(gamma,pi):
    p = 1
    for i in range(len(pi)):
        x = 0
        for a in pi[i:]:
            x += gamma[a]
        p *= gamma[pi[i]]/(1.0*x)
    return p

# Computes the Plackett-Luce distribution of parameter gamma on Sn
def PL_distribution(gamma,n):
    p = dict.fromkeys(permutations(range(1,n+1)))
    for pi in p:
        p[pi] = PL_predict(gamma,pi)
    return p

# Learns a PL model from a list of incomplete rankings L
def PL_fitting_list(L,n,nb_iterations):
    Gamma = dict.fromkeys(range(nb_iterations+1))
    Gamma[0] = dict.fromkeys(range(1,n+1),1/(1.0*n))
    w = Counter()
    for pi in L:
        for a in pi[:len(pi)-1]:
            w[a] += 1 # w[a] is the number of rankings where a is ranked higher than last
    for k in range(nb_iterations):
        gamma = dict(Gamma[k])
        z = dict.fromkeys(range(1,n+1),0.0)
        for pi in L:
            x = [] # x va contenir tous les denomitateurs necessaires dans la proba
            # le dernier element de x (tout a droite) est la somme de tous les parametres des items dans le ranking
            y = gamma[pi[len(pi)-1]]
            for i in range(2,len(pi)+1):
                y += gamma[pi[len(pi)-i]]
                x.append(y)
            y = 0
            for i in range(len(pi)-1):
                y += 1/x[len(x)-i-1]
                z[pi[i]] += y
            z[pi[len(pi)-1]] += y
        for t in range(1,n+1):
            if t in w:
                if z[t] != 0:
                    gamma[t] = w[t]/z[t]
        Gamma[k+1] = dict(gamma)
    return Gamma[nb_iterations]

# Computes the estimated marginals on the Design for the PL model fitted from the Dataset
def PL_estimator(Dataset, Design, n, parameter): # Parameter = nb_iterations for PL model
    L = []
    for (A,pi) in Dataset:
        L.append(pi)
    Gamma = PL_fitting_list(L,n,parameter)
    M = dict.fromkeys(Design)
    for A in Design:
        M[A] = {}
        for pi in permutations(A):
            M[A].update({pi:PL_predict(Gamma,pi)})
    return M
    





# Learns a PL model from a distribution of full rankings D
def PL_fitting_dict(D,n,nb_iterations):
    Gamma = dict.fromkeys(range(nb_iterations+1))
    Gamma[0] = dict.fromkeys(range(1,n+1),1/(1.0*n))
    w = {}
    for pi in D:
        for a in pi[:len(pi)-1]:
            if a in w:
                w[a] += D[pi]
            else:
                w.update({a:D[pi]})
    for k in range(nb_iterations):
        gamma = dict(Gamma[k])
        z = dict.fromkeys(range(1,n+1),0.0)
        for pi in D:
            x = []
            y = gamma[pi[len(pi)-1]]
            for i in range(2,len(pi)+1):
                y += gamma[pi[len(pi)-i]]
                x.append(y)
            y = 0
            for i in range(len(pi)-1):
                y += 1/x[len(x)-i-1]
                z[pi[i]] += y*D[pi]
            z[pi[len(pi)-1]] += y*D[pi]
        for t in range(1,n+1):
            if t in w:
                if z[t] != 0:
                    gamma[t] = w[t]/z[t]
        Gamma[k+1] = dict(gamma)
    return Gamma

