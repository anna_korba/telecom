\subsection{Background and Preliminaries}\label{sec:background_esanns}
A few concepts involved in the description of the ensemble learning methods we promote here and in its analysis in the subsequent section are summarized below.\\

\noindent {\bf Probabilistic framework for Kemeny aggregation.}
Whereas problem \eqref{eq:median} is NP-hard in general, exact solutions, referred to as \textit{Kemeny medians}, can be explicited when the pairwise probabilities $p_{i,j}=\mathbb{P}\{  \Sigma(i)<\Sigma(j)\}$, $1\leq i\neq j\leq n$, fulfill the following property, referred to as \textit{stochastic transitivity}:
	$\forall (i,j,k)\in \n^3:\;\;  p_{i,j}\geq 1/2 \text{ and } p_{j,k}\geq 1/2 \; \Rightarrow\; p_{i,k}\geq 1/2$.
	If, in addition, none of the pairwise probabilities is equal to $1/2$ ($p_{i,j}\neq 1/2$ for all $i<j$), distribution $P$ is said to be strictly stochastically transitive.
When stochastic transitivity holds true, the set of Kemeny medians is the set $\{\sigma\in \mathfrak{S}_n:\; (p_{i,j}-1/2)(\sigma(j)-\sigma(i ))>0 \text{ for all } i<j \text{ s.t. } p_{i,j}\neq 1/2  \}$,
%the minimum $L^*_P=L_P(\sigma^*)$ is given by
%\begin{equation}\label{eq:inf}
%L^*_P=\sum_{i<j}\min\{p_{i,j},1-p_{i,j}  \}=\sum_{i<j}\{  1/2-\vert  p_{i,j}-1/2\vert\}
%\end{equation}
%and, for any $\sigma\in \mathfrak{S}_n$,
%$L_P(\sigma)-L^*_P=2\sum_{i<j}\left\vert p_{i,j}-1/2 \right\vert\cdot \mathbb{I}\{ (\sigma(i)-\sigma(j))\left(p_{i,j}-1/2\right)<0\}.$
and, if the strict version of stochastic transitivity is fulfilled, the Kemeny median is unique and given by the Copeland ranking:
$\sigma^*_P(i)=1+\sum_{k\neq i}\mathbb{I}\{p_{i,k}<1/2  \}$ for  $1\leq i\leq n$ (see Theorem 5 in \cite{CKS17}).
The set of strictly stochastically transitive distributions on $\mathfrak{S}_n$ is denoted by $\mathcal{T}$. Assume that we observe i.i.d. copies $\Sigma_1,\dots, \Sigma_N$ of a generic r.v. $\Sigma \sim P$ and let  $\widehat{P}_N=(1/N)\sum_{i=1}^N\delta_{\Sigma_i}$, where $\delta_x$ denotes the Dirac mass at any point $x$.  Suppose that $P\in\mathcal{T}$ and satisfies the low-noise condition {\bf NA}$(h)$ for a given $h>0$: $\min_{i<j}\left\vert p_{i,j}-1/2 \right\vert \ge h$.
It is also shown in \cite{CKS17} that under the former hypotheses the empirical distribution $\widehat{P}_N \in \mathcal{T}$ as well, with overwhelming probability, and that the expectation of the excess of risk of empirical Kemeny medians (denoted by $\sigma^*_{\widehat{P}_N}$) decays at an exponential rate, see Proposition 14 therein. In this case, the nearly optimal solution $\sigma^*_{\widehat{P}_N}$ can be made explicit and straightforwardly computed based on the empirical pairwise probabilities $ \widehat{p}_{i,j}=(1/N)\sum_{k=1}^N\mathbb{I}\{ \Sigma_k(i)<\Sigma_k(j)  \}$, $i<j$. Otherwise, solving the NP-hard problem $\min_{\sigma \in \mathfrak{S}_n}L_{\widehat{P}_N}(\sigma)$ is required to get an empirical Kemeny median, refer also to \cite{JKS16} and the references therein for a description of methods dedicated to approximate Kemeny median computation. However, as can be seen by examining Proposition 14's proof in \cite{CKS17}, the exponential rate bound holds true for any candidate $\widetilde{\sigma}_N$ in $\mathfrak{S}_n$ that coincides with $\sigma^*_{\widehat{P}_N}$ when the empirical distribution lies in $\mathcal{T}$ and takes arbitrary values in $\mathfrak{S}_n$ otherwise. 
%In practice, when $\widehat{P}_N$ does not belong to $\mathcal{T}$, we propose to consider as a pseudo-empirical median any permutation $\widetilde{\sigma}^*_{\widehat{P}_N}$ that ranks the objects as the empirical Borda count:
%$$ 
%\left( \sum_{k=1}^N\Sigma_k(i)-\sum_{k=1}^N\Sigma_k(j)\right)\cdot \left( \widetilde{\sigma}^*_{\widehat{P}_N}(i)-\widetilde{\sigma}^*_{\widehat{P}_N}(j)   \right)>0
%$$ for all } $i<j$ \text{ s.t. } $\sum_{k=1}^N\Sigma_k(i) \ne \sum_{k=1}^N\Sigma_k(j)$,
%breaking possible ties arbitrarily.
\\

\noindent {\bf Ranking median regression.} As, for all $s\in \mathcal{S}$, $\mathcal{R}(s)=\mathbb{E}_{X\sim \mu}[L_{P_X}(s(X))]$, the optimal RMR rules $s$ are those such that $s(X)$ is a median of $P_X$ with probability one and the minimum risk is $\mathcal{R}^*=\mathbb{E}_{X\sim \mu}[L^*_{P_X}]$, where $L_P^*$ denotes the minimum of \eqref{eq:median} for any distribution $P$.  In the case where $P_X$ is strictly stochastically transitive with probability one, the optimal RMR rule is $\mu$-almost surely unique, \textit{i.e.} we $\mu$-a.s. have $s^*(X)=\sigma^*_{P_X}$. Statistical learning being based on a finite sample $(X_1,\; \Sigma_1)\; \ldots,\; (X_1,\; \Sigma_N)$ of independent copies of the pair $(X,\; \Sigma)$, nearly optimal RMR rules cannot be built by trying to statistically recover a Kemeny median of $P_x$ for each possible input point $x$, except when $\X$ is of finite cardinality, small compared to $N$. Nevertheless, under the additional assumption that the pairwise probabilities $p_{i,j}(x)$, related to the conditional distribution $P_x$, are Lipschitz, local learning techniques based on the solving of a few well-chosen Kemeny consensus problems have been proved to be consistent\footnote{A sequence of RMR rules $s_N$ is said to be consistent if $\mathcal{R}(s_N)\rightarrow \mathcal{R}^*$ in probability, as $N\rightarrow \infty$.}, nearest neighbor and decision tree methods namely. However, these methods exhibit high instability in practice. In the spirit of ensemble learning methods, it is proposed in the next section to apply Kemeny aggregation again, to randomized versions of such RMR rules this time, in order to increase stability.

% and, based on these training data, the objective is to build a predictive ranking rule $s$ that nearly minimizes $\mathcal{R}(s)$ over the class $\mathcal{S}$ of measurable mappings $s:\mathcal{X}\rightarrow \mathfrak{S}_n$.
 %Of course, the Empirical Risk Minimization (ERM) paradigm encourages to consider solutions of the optimization problem:
 %\begin{equation}\label{eq:ERM}
 %\min_{s\in \mathcal{S}_0}\widehat{\mathcal{R}}_N(s),
 %\end{equation}
 %where $\mathcal{S}_0$ is a subset of $\mathcal{S}$, supposed to be rich enough for containing approximate versions of elements of $\mathcal{S}^*$ (\textit{i.e.} so that $\inf_{s\in \mathcal{S}_0}\mathcal{R}(s)- \mathcal{R}^*$ is 'small') and ideally appropriate for continuous or greedy optimization, and
 %\begin{equation}\label{eq:emp_reg_risk}
 %\widehat{\mathcal{R}}_N(s)=\frac{1}{N}\sum_{k=1}^N d_{\tau}(s(X_k),\;  \Sigma_k)
 %\end{equation}
 %is a statistical version of \eqref{eq:risk_theo} based on the $(X_i,\Sigma_i)$'s. Extending those established by \cite{CKS17} in the context of ranking aggregation, statistical results describing the generalization capacity of minimizers of \eqref{eq:emp_reg_risk} can be established under classic complexity assumptions for the class $\mathcal{S}_0$, as revealed by the result stated below.
 
% One may also prove that rates of convergence for the excess of risk of empirical Kemeny medians can be much faster than $O_{\mathbb{P}}(1/\sqrt{N})$ under the following %hypothesis, involved in the subsequent analysis (\cite{CKS17bis}). 
 
 %\begin{assumption}\label{hyp:margin} $\forall x\in \mathcal{X}$, $P_x\in \mathcal{T}$ and 
 %	$ H=\inf_{x\in \mathcal{X}}\min_{i<j}\left\vert p_{i,j}(x)-1/2 \right\vert >0$.
 %\end{assumption}
% This condition is checked in many situations, including most conditional parametric models (see Remark~13 in \cite{CKS17}), and generalizes condition \eqref{eq:hyp_margin0}, which corresponds to Assumption \ref{hyp:margin} when $X$ and $\Sigma$ are independent. The result stated below reveals that a similar fast rate phenomenon occurs for minimizers of the empirical risk \eqref{eq:emp_reg_risk} if Assumption \ref{hyp:margin} is satisfied.
 
