
\subsection{Aggregation of Ranking Median Regression Rules}\label{sec:aggregation}

We now investigate RMR rules that compute their predictions by aggregating those of \textit{randomized RMR rules}. Let $Z$ be a r.v. defined on the same probability space as $(X,\Sigma)$, valued in a measurable space $\mathcal{Z}$ say, describing the randomization mechanism. A randomized RMR algorithm is then any function $S: \bigcup_{N\geq 1}(\X\times \mathfrak{S}_n)^N \times \mathcal{Z} \rightarrow \mathcal{S}$
 that maps any pair $(z,\mathcal{D}_N)$ to a RMR rule $S(.,z, \mathcal{D}_N)$. Given the training sample $\mathcal{D}_N$, its risk is $\mathcal{R}(S(.,., \mathcal{D}_N))=\mathbb{E}_{(X,\Sigma, Z)}[d_{\tau}(\Sigma, S(X,Z,\mathcal{D}_N))  ]$. Given any RMR algorithm and any training set $\mathcal{D}_N$, one may compute an aggregated rule as follows.
 
 \begin{figure}[!h]
 
 \begin{center}
 \fbox{
 \begin{minipage}[t]{11cm}
 \smallskip
 
 \begin{center}
 {\sc Kemeny Aggregated RMR rule}
 \end{center}
 
 %\smallskip
 {\small
 
 \begin{enumerate}
 \item[] {\bf Inputs.} Training dataset $\mathcal{D}_N=\{(X_1,\Sigma_1),\; \ldots,\, (X_N,\Sigma_N)  \}$. RMR randomized algorithm $S$, randomization mechanism $Z$, query point $x\in \mathcal{X}$. Number $B\geq 1$ of randomized RMR rules involved in the consensus.
 \item ({\sc Randomization.}) Conditioned upon $\mathcal{D}_N$, draw independent copies $Z_1,\; \ldots,\; Z_B$ of the r.v. $S$ and compute the individual predictions
 $$
 S(x,Z_1, \mathcal{D}_n),\; \ldots,\; S(x,Z_B,\mathcal{D}_n).
 $$
 
 \medskip
 
 \item ({\sc Kemeny consensus.}) Compute the empirical distribution on $\mathfrak{S}_n$
 $$
 P_B(x)=\frac{1}{B}\sum_{b=1}^B\delta_{S(x,Z_b,\mathcal{D}_n)}
 $$
 and output a Kemeny consensus (or an approximate median):
 $$\bar{s}_{B}(x)\in \argmin_{\sigma\in\mathfrak{S}_n}L_{P_B(x)}.$$
 \smallskip
 \end{enumerate}
 }
 \end{minipage}
 }
 \label{fig:trpseudoagg}
 \caption{Pseudo-code for the aggregation of RMR rules.}
 \end{center}
 \end{figure}
The result stated below shows that, provided that $P_X$ fulfills the strict stochastic transitivity property and that the $p_{i,j}(X)$'s satisfy the noise condition {\bf NA}$(h)$ for some $h>0$ with probability one (we recall incidentally that it is shown in \cite{CKS17bis} that fast learning rates are attained by empirical risk minimizers in this case, see Proposition 7 therein), consistency is preserved by Kemeny aggregation, as well as the learning rate.
\begin{theorem}
Let $h>0$. Assume that the sequence of RMR rules $(S(.,Z,\mathcal{D}_N))_{N\geq 1}$ is consistent for a certain distribution of $(X,\Sigma)$. Suppose also that $P_X$ is strictly stochastically transitive and satisfies condition {\bf NA}$(h)$ with probability one. Then, for any $B\geq 1$, any Kemeny aggregated RMR rule $\bar{s}_{B}$ is consistent as well and its learning rate is at least that of $S(.,Z,\mathcal{D}_N)$.
\end{theorem}
\begin{proof} 
Recall the following formula for the risk excess: $\forall s\in \mathcal{S}$,
\begin{multline*}
\mathcal{R}(s)- \mathcal{R}^*=
\sum_{i<j}\mathbb{E}_X[\vert p_{i,j}(X)-1/2 \vert\mathbb{I}\{ (s(X)(j)-s(X)(i))(\sigma^*_{P_X}(j)-\sigma^*_{P_X}(i))<0  \}]\\
\leq \mathbb{E}_X[d_{\tau}(s(X), \sigma^*_{P_X})  ]\leq (\mathcal{R}(s)- \mathcal{R}^*)/h,
\end{multline*}
see section 3 in \cite{CKS17bis}.  In addition, the definition of the Kemeny median combined with triangular inequality implies that we a.s. have:
\begin{multline*}
Bd_{\tau}(\bar{s}_B(X), \sigma^*_{P_X})\leq \sum_{b=1}^B d_{\tau}(\bar{s}_B(X), S(X,Z_b,\mathcal{D}_N))+\sum_{b=1}^B d_{\tau}(S(X,Z_b,\mathcal{D}_N), \sigma^*_{P_X})\\
\leq 2\sum_{b=1}^B d_{\tau}(S(X,Z_b,\mathcal{D}_N), \sigma^*_{P_X}).
\end{multline*}
Combined with the formula/bound above, we obtain that
\begin{multline*}
\mathcal{R}(\bar{s}_B)- \mathcal{R}^*\leq \mathbb{E}[d_{\tau}(\bar{s}_B, \sigma^*_{P_X}  )]\leq \frac{2}{B}\sum_{b=1}^B\mathbb{E}_X[ d_{\tau}(S(X,Z_b,\mathcal{D}_N), \sigma^*_{P_X}) ]\\
\leq 
(2/h)\frac{1}{B}\sum_{b=1}^B (\mathcal{R}(S(.,Z_b,\mathcal{D}_N))- \mathcal{R}^*).
\end{multline*}
The proof is then immediate.
\end{proof}



%the sequence $\mathcal{R}(S(.,Z,\mathcal{D}_N))$ is uniformly bounded (by $n(n-1)/2$), we also have $\mathbb{E}[ \mathcal{R}(S(.,Z,\mathcal{D}_N)) ]-\mathcal{R}^*\rightarrow 0$ as $N\rightarrow \infty$. Recall also that 
%$\mathbb{E}[ \mathcal{R}(S(.,Z,\mathcal{D}_N)) ]-\mathcal{R}^*=\sum_{i<j}\mathbb{E}_X[\vert p_{i,j}(X)-1/2\vert\mathbb{I}\{(S(X,Z,\mathcal{D}_N))(j)-S(X,Z,\mathcal{D}_N))(i))(\sigma^*_{P_X}(j)-\sigma^*_{P_X}(i))<0 \mid X \}]$ and that, for all $i<j$ and any $x\in \mathcal{X}$, $\mathbb{P}\{(S(X,Z,\mathcal{D}_N))(j)-S(X,Z,\mathcal{D}_N))(i))(\Sigma(j)-\Sigma(i))<0 \mid X=x \}\geq \mathbb{P}\{\sigma^*_{P_X}(j)-\sigma^*_{P_X}(j))(\Sigma(j)-\Sigma(i))<0 \mid X=x \}=\min\{p_{i,j}(x),1-p_{i,j}(x)  \}$. Hence, consistency means that, for $\mu$-almost $x$ and for all $i<j$, $\mathbb{P}\{(S(X,Z,\mathcal{D}_N))(j)-S(X,Z,\mathcal{D}_N))(i))(\Sigma(j)-\Sigma(i))<0 \mid X=x \}=(2p_{i,j}(x)-1)\mathbb{P}\{ S(x,Z,\mathcal{D}_N))(j)<S(x,Z,\mathcal{D}_N))(i) \}+1-p_{i,j}(x)\rightarrow \min\{p_{i,j}(x),1-p_{i,j}(x)  \}$ as $N\rightarrow \infty$.
 %Fix $x$ in $\X$, $i<j$ and suppose that $p_{i,j}(x)>1/2$ without loss of generality. Hence, we have $\mathbb{P}\{ S(x,Z,\mathcal{D}_N))(j)<S(x,Z,\mathcal{D}_N))(i) \}\rightarrow 0$ as $N\rightarrow \infty$. In addition, observe that $\mathbb{P}\{ \bar{s}_B(x)(j)<\bar{s}_B(x)(i) \}=1/2\mathbb{P}\{ = \} + \mathbb{P}\{(1/B)\sum_{b=1}^B >1/2 \}$
 
 % such that $\sum_{b=1}^BS(x, Z_b,\mathcal{D}_N)(i)\neq \sum_{b=1}^BS(x, Z_b,\mathcal{D}_N)(j)$
%Since Assumption \ref{hyp:margin} is satisfied, recall that we have: $\forall s\in \mathcal{S}$,
%$$
%\mathcal{R}(s)-\mathcal{R}^*=\sum_{i<j}\int_{x\in \mathcal{X}}\vert p_{i,j}(x)-1/2\vert \mathbb{I}\{(p_{i,j}(x)-1/2)(s(j)-s(i))<0  \}\mu(dx).
%$$



