\chapter{More on Chapter~\ref{chap:background_ranking_data}}

\section*{Links with Hodge theory}
\label{sec:hodge_links}

 Let $\delta_0$ be the gradient operator and $\delta_1$ the curl operator. Their adjoints operators are respectively the negative divergence $\delta_0^*$ and the boundary operator $\delta_1^*$. Let $\Delta_1= \delta_1 \circ \delta_1^* - \delta_0^* \circ \delta_0$. Let $C_1$ be the space of pairwise rankings represented as skew-symmetric matrices $W$, that is to say such that for all $i<j$, $w_{i,j}=-w_{j,i}$.\\
 
  \noindent According to the Hodge decomposition theorem, the space of pairwise rankings $C^1$ admits an orthogonal decomposition onto three components: $C^1= Im(\delta_0) \bigoplus H_1 \bigoplus Im(\delta_1^*)$, with $H_1=Ker(\delta_1) \cap Ker(\delta_0^*)=Ker(\Delta_1)$. The first two components actually corresponds to the \textit{transitivity subspace} $Ker(\delta_1)$, that is to say the flows that satisfy the "curl-free" property (or local consistency):
 \begin{equation}\label{eq:curl_free}
 W : w_{ij} + w_{jk} + w_{ki} = 0, \forall \text{ 3-clique } {i, j, k} 
 \end{equation}
 
 (and thus global consistency on a complete graph
\noindent In particular, the authors show that the $l_2$ projection of the pairwise rankings matrix $(w_{i,j})_{i,j}$ onto the space of gradients flows $Im(\delta_0)$ actually yields the Borda count (up to a positive multiplicative constant), given by:
 \begin{equation}\label{eq:Borda}
 s(i)=\sum_{k\ne i}w_{i,k}
 \end{equation}
 

\noindent Let $p \in \mathbb{R}^{n\times n}$ be the matrix of pairwise probabilities $(p_{i,j})_{i, j}$ (with the convention that $p_{i,i}=0$). Let $\mathcal{T}'$ the space of probability distribution satisfying the stochastic transitivity condition (not strictly, so $\mathcal{T} \subset \mathcal{T}'$). The problem of finding the best collection of pairwise probabilities $q=(q_{i,j})_{i<j} \in \mathcal{T}'$ approximating $p=(p_{i,j})_{i<j}$ in the least squares sense can be written as : 
\begin{equation}\label{eq:min_problem1}
\min_{\substack{0\le q_{i,j}\le 1 \\q_{i,j}+q_{j,i}=1\\ q_{i,j}\ge 1/2 \text{ and } q_{j,k}\ge 1/2 \Rightarrow q_{i,k}\ge 1/2}} \| p - q \|
\end{equation}

\noindent For any matrix of  pairwise probabilities $(p_{i,j})_{i,j}$, we can define the associated skew-symmetric matrix $W$ by: for all $i<j$, $w_{i,j}=p_{i,j}-1/2$. Let $T$ be the preceding operation on matrices. The problem \eqref{eq:min_problem1} then becomes (by changing the variable $T(q)$ into $y$):
\begin{equation}\label{eq:min_problem2}
\min_{\substack{ | y_{i,j}|  \le 1/2 \\ y_{i,j}=-y_{j,i}\\ y_{i,j}\ge 0 \text{ and } y_{j,k}\ge 0 \Rightarrow y_{i,k}\ge 0}} \| T(p) - y \|
\end{equation}
Thus the stochastic transitivity property for a skew-symmetric matrix $W$ can be written as:
\begin{equation}\label{eq:transitivity_skew_matrix}
 w_{i,j}\ge 0 \text{ and } w_{j,k}\ge 0 \Rightarrow w_{i,k}\ge 0
\end{equation}
It is trivial to show that the curl-free property \eqref{eq:curl_free} implies the transitivity property \eqref{eq:transitivity_skew_matrix}. 
So one way to solve approximately \eqref{eq:min_problem2} is to replace the last constraint \eqref{eq:transitivity_skew_matrix} by the curl-free constraint \eqref{eq:curl_free}:
\begin{equation}\label{eq:min_problem3}
\min_{\substack{ | y_{i,j}|  \le 1/2 \\ y_{i,j}=-y_{j,i}\\ y_{i,j}+y_{j,k}+y_{k,i}= 0}} \| T(p) - y \|
\end{equation}
This corresponds to firstly project the matrix $T(p)$ onto the space $Ker(\Delta_1)$ and thus obtain a matrix $W=proj_{Ker(\Delta_1)}(T(p))$, and maybe rescale it so that the coefficients $w_{i,j}$ of $W$ satisfy $| w_{i,j} | \le 1/2$. In this case $W$ would satisfy all the constraints in \eqref{eq:min_problem2}. If we are only interested in the global ranking induced by $W$, this output is given by Borda count (projection on $Im(\delta_0)$) as explained in \eqref{eq:Borda}. \\

\noindent To conclude, solving \eqref{eq:min_problem3} approximately solves \eqref{eq:min_problem2}. Actually, the problem of finding best collection of pairwise probabilities $p''=(p''_{i,j})_{i<j} \in \mathcal{T}$ approximating $p=(p_{i,j})_{i<j}$ boils down to solve \eqref{eq:min_problem1}, let $q^*$ be the solution, and modify arbitrarily $q^*$ when $q^*_{i,j}=1/2$ so that it lies in $\mathcal{T}$. This justifies that we can arbitrarily break the ties in Borda count to get a ranking.