
\section{Appendix - On Aggregation in Ranking Median Regression}\label{sec:esann}


%\subsection{Aggregation of Ranking Median Regression Rules}\label{sec:aggregation}

\noindent \textbf{Aggregation of Ranking Median Regression Rules.} We now investigate RMR rules that compute their predictions by aggregating those of \textit{randomized RMR rules}. Let $Z$ be a r.v. defined on the same probability space as $(X,\Sigma)$, valued in a measurable space $\mathcal{Z}$ say, describing the randomization mechanism. A randomized RMR algorithm is then any function $S: \bigcup_{N\geq 1}(\X\times \mathfrak{S}_n)^N \times \mathcal{Z} \rightarrow \mathcal{S}$
that maps any pair $(z,\mathcal{D}_N)$ to a RMR rule $S(.,z, \mathcal{D}_N)$. Given the training sample $\mathcal{D}_N$, its risk is $\mathcal{R}(S(.,., \mathcal{D}_N))=\mathbb{E}_{(X,\Sigma, Z)}[d_{\tau}(\Sigma, S(X,Z,\mathcal{D}_N))  ]$. Given any RMR algorithm and any training set $\mathcal{D}_N$, one may compute an aggregated rule as follows.

\begin{figure}[!h]
	
	\begin{center}
		\fbox{
			\begin{minipage}[t]{11cm}
				\smallskip
				
				\begin{center}
					{\sc Kemeny Aggregated RMR rule}
				\end{center}
				
				%\smallskip
				{\small
					
					\begin{enumerate}
						\item[] {\bf Inputs.} Training dataset $\mathcal{D}_N=\{(X_1,\Sigma_1),\; \ldots,\, (X_N,\Sigma_N)  \}$. RMR randomized algorithm $S$, randomization mechanism $Z$, query point $x\in \mathcal{X}$. Number $B\geq 1$ of randomized RMR rules involved in the consensus.
						\item ({\sc Randomization.}) Conditioned upon $\mathcal{D}_N$, draw independent copies $Z_1,\; \ldots,\; Z_B$ of the r.v. $S$ and compute the individual predictions
						$$
						S(x,Z_1, \mathcal{D}_n),\; \ldots,\; S(x,Z_B,\mathcal{D}_n).
						$$
						
						\medskip
						
						\item ({\sc Kemeny consensus.}) Compute the empirical distribution on $\mathfrak{S}_n$
						$$
						P_B(x)=\frac{1}{B}\sum_{b=1}^B\delta_{S(x,Z_b,\mathcal{D}_n)}
						$$
						and output a Kemeny consensus (or an approximate median):
						$$\bar{s}_{B}(x)\in \argmin_{\sigma\in\mathfrak{S}_n}L_{P_B(x)}.$$
						\smallskip
					\end{enumerate}
				}
			\end{minipage}
		}
		\label{fig:trpseudoagg}
		\caption{Pseudo-code for the aggregation of RMR rules.}
	\end{center}
\end{figure}
The result stated below shows that, provided that $P_X$ fulfills the strict stochastic transitivity property and that the $p_{i,j}(X)$'s satisfy the noise condition {\bf NA}$(h)$ for some $h>0$ with probability one (we recall that fast learning rates are attained by empirical risk minimizers in this case), consistency is preserved by Kemeny aggregation, as well as the learning rate.
\begin{theorem}
	Let $h>0$. Assume that the sequence of RMR rules $(S(.,Z,\mathcal{D}_N))_{N\geq 1}$ is consistent for a certain distribution of $(X,\Sigma)$. Suppose also that $P_X$ is strictly stochastically transitive and satisfies condition {\bf NA}$(h)$ with probability one. Then, for any $B\geq 1$, any Kemeny aggregated RMR rule $\bar{s}_{B}$ is consistent as well and its learning rate is at least that of $S(.,Z,\mathcal{D}_N)$.
\end{theorem}
\begin{proof} 
	Recall the following formula for the risk excess: $\forall s\in \mathcal{S}$,
	\begin{multline*}
		\mathcal{R}(s)- \mathcal{R}^*=
		\sum_{i<j}\mathbb{E}_X[\vert p_{i,j}(X)-1/2 \vert\mathbb{I}\{ (s(X)(j)-s(X)(i))(\sigma^*_{P_X}(j)-\sigma^*_{P_X}(i))<0  \}]\\
		\leq \mathbb{E}_X[d_{\tau}(s(X), \sigma^*_{P_X})  ]\leq (\mathcal{R}(s)- \mathcal{R}^*)/h,
	\end{multline*}
	see section 3 in \cite{CKS17bis}.  In addition, the definition of the Kemeny median combined with triangular inequality implies that we a.s. have:
	\begin{multline*}
		Bd_{\tau}(\bar{s}_B(X), \sigma^*_{P_X})\leq \sum_{b=1}^B d_{\tau}(\bar{s}_B(X), S(X,Z_b,\mathcal{D}_N))+\sum_{b=1}^B d_{\tau}(S(X,Z_b,\mathcal{D}_N), \sigma^*_{P_X})\\
		\leq 2\sum_{b=1}^B d_{\tau}(S(X,Z_b,\mathcal{D}_N), \sigma^*_{P_X}).
	\end{multline*}
	Combined with the formula/bound above, we obtain that
	\begin{multline*}
		\mathcal{R}(\bar{s}_B)- \mathcal{R}^*\leq \mathbb{E}[d_{\tau}(\bar{s}_B, \sigma^*_{P_X}  )]\leq \frac{2}{B}\sum_{b=1}^B\mathbb{E}_X[ d_{\tau}(S(X,Z_b,\mathcal{D}_N), \sigma^*_{P_X}) ]\\
		\leq 
		(2/h)\frac{1}{B}\sum_{b=1}^B (\mathcal{R}(S(.,Z_b,\mathcal{D}_N))- \mathcal{R}^*).
	\end{multline*}
	The proof is then immediate.
\end{proof}




%\subsection{Experimental Results}\label{sec:exp}

\begin{table}[ht]
	%\noindent\makebox[\textwidth]{
    \centering
	%\resizebox{\columnwidth}{!}{%
	\begin{tabular}{|c |ccc |} 
		\hline
		\multirow{2}{2.2cm}{Level of Noise}&\multicolumn{3}{ c |}{Number of items}\\
		&n=3 & n=5 & n=8  \\
		\hline
		\multirow{3}{1.1cm}{$\phi=2$}& 0.534 +/ - 0.167 & 1.454 +/ - 0.427 & 3.349 +/ - 0.952 \\
		&0.385 +/ - 0.085*   & 1.001 +/ - 0.232* & 2.678 +/ - 0.615*\\
		&0.379 +/ - 0.057**   & 0.961 +/ - 0.218** & 	2.281 +/ - 0.589** \\
		\hline
		\multirow{3}{1.1cm}{$\phi=1$}& 0.875 +/ - 0.108 & 2.346 +/ - 0.269 & 5.638+/ - 1.688
		\\ 
		& 0.807+/ - 0.061* &2.064 +/ - 0.130 * & 4.499 +/ - 0.574* \\ 
		& 0.756 +/ - 0.063** & 2.011 +/ - 0.110** & 4.061 +/ - 0.259**\\ 
		\hline
	\end{tabular}
	%}
	\caption{Empirical risk averaged on 50 trials on simulated data for aggregation of RMR rules.}
	\label{table:simulated_data}
\end{table}


\noindent \textbf{Experimental Results.} For illustration purpose, experimental results based on simulated data are displayed. Datasets of full rankings on $n$ items are generated according to $p$=2 explanatory variables. We carried out several experiments by varying the number of items ($n=3,5,8$) and the "level of noise" of the distribution of permutations.  For a given setting, one considers a fixed partition on the feature space, so that on each cell, the rankings/preferences are drawn from a certain Mallows distribution centered around a permutation with a fixed dispersion parameter $\phi$. We recall that the greater $\phi$, the spikiest the distribution (so closest to piecewise constant and less noisy in this sense). In each trial, the dataset of $N=1000$ samples is divided into a training set ($70\%$) and a test set ($30\%$). We compare the results of (a randomized variant of) the CRIT algorithm vs the aggregated version: in our case, the randomization is a boostrap procedure. Concerning the CRIT algorithm, since the true partition is known and can be recovered by means of a tree-structured recursive partitioning of depth 3, the maximum depth is set to 3 and the minimum size in a leaf is set to the number of samples in the training set divided by 10. 
For each configuration (number of items $n$ and distribution of the dataset parameterized by $\Phi$), the empirical risk, denoted as $\widehat{\mathcal{R}}_N(s)$, is averaged over 50 replications of the experiment. Results of the aggregated version of the (randomized) CRIT algorithm (one star * indicates the aggregate over 10 models, two stars over 30 models **) and of the CRIT algorithm (without stars) in the various configurations are provided in Table~\ref{table:simulated_data}. In practice, for $n=8$, the outputs of the randomized algorithms are aggregated with the Copeland procedure so that the running time remains reasonable. The results show notably that the noisier the data (smaller $\phi$) and the larger the number of items $n$ to be ranked, the more difficult the problem and the higher the risk. In a nutshell, and as confirmed by additional experiments, the results show that aggregating the randomized rules globally improves the average performance and reduces the standard deviation of the risk.
