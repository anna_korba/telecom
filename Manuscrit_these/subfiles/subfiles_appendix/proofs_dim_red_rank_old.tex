
\section{Appendix}

\subsection*{Proof of Lemma \ref{lem:Kendall}}

Consider two probability distributions $P$ and $P'$ on $\mathfrak{S}_n$. Fix $i\neq j$ and let $(\Sigma,\Sigma')$ be a pair of random variables defined on a same probability space, valued in $\mathfrak{S}_n$ and such that $p_{i,j}=~\mathbb{P}_{\Sigma\sim P}\{\Sigma(i)<~\Sigma(j)\}$ and $p'_{i,j}=\mathbb{P}_{\Sigma'\sim P'}\{\Sigma'(i)<\Sigma'(j)\}$.
Set
$$
\pi_{i,j}=\mathbb{P}\left\{ \Sigma'(i)<\Sigma'(j)\mid  \Sigma(i)<\Sigma(j) \right\}.
$$
Equipped with this notation, by the law of total probability, we have:
\begin{equation}\label{eq:margin}
p'_{i,j}=p_{i,j}\pi_{i,j}+(1-p_{i,j})(1-\pi_{j,i}).
\end{equation}
In addition, we may write
\begin{multline*}
\mathbb{E}\left[\mathbb{I}\{\Sigma(i)<\Sigma(j) \}\mathbb{I}\{\Sigma'(i)>\Sigma'(j) \} + \mathbb{I}\{\Sigma(i)>\Sigma(j) \}\mathbb{I}\{\Sigma'(i)<\Sigma'(j) \}\right]
=\\ p_{i,j}(1-\pi_{i,j})+(1-p_{i,j})(1-\pi_{j,i}) .
\end{multline*}
Suppose that $p_{i,j}<p'_{i,j}$. Using \eqref{eq:margin}, we have $p_{i,j}(1-\pi_{i,j})+(1-p_{i,j})(1-\pi_{j,i})=p'_{i,j}+(1-2\pi_{i,j})p_{i,j}$, which quantity is minimum when $\pi_{i,j}=1$ (and in this case $\pi_{j,i}=(1-p'_{i,j})/(1-p_{i,j})$), and then equal to $\vert p_{i,j}-p'_{i,j}\vert$. We recall that we can only set $\pi_{i,j}=1$ if the initial assumption $p_{i,j}<p'_{i,j}$ holds. In a similar fashion, if $p_{i,j}>p'_{i,j}$, we have $p_{i,j}(1-\pi_{i,j})+(1-p_{i,j})(1-\pi_{j,i})=2(1-p_{i,j})(1-\pi_{j,i})+p_{i,j}-p'_{i,j}$, which is minimum for $\pi_{j,i}=1$ (we have incidentally $\pi_{i,j}=p'_{i,j}/p_{i,j}$ in this case) and then equal to $\vert p_{i,j}-p'_{i,j}\vert$. Since we clearly have
\begin{multline*}
W_{d_{\tau},1}\left(P,P'  \right) \ge  \\\sum_{i<j} \inf_{(\Sigma,\Sigma') \text { s.t. } \mathbb{P}\{\Sigma(i)<\Sigma(j)\}=p_{i,j} \text{ and } \mathbb{P}\{\Sigma'(i)<\Sigma'(j)\}=p'_{i,j} } \left\{ \mathbb{P}\{ (\Sigma(i)-\Sigma(j))(\Sigma'(i)-\Sigma'(j))<0 \} \right\},
\end{multline*}
this proves that
$$
W_{d_{\tau},1}\left(P,P'  \right) \ge \sum_{i<j}\vert p'_{i,j}-p_{i,j} \vert.
$$
As a remark, given a distribution $P$ on $\mathfrak{S}_n$, when $P'=P_{\C}$ with $\C$ a bucket order of $\n$ with $K$ buckets, the optimality conditions on the $\pi_{i,j}$'s are fulfilled by the coupling $(\Sigma,\Sigma_{\C})$,
which implies that:
$$
W_{d_{\tau},1}\left(P,P_{\C} \right) = \sum_{i<j}\vert p'_{i,j}-p_{i,j} \vert = \sum_{1\leq k<l\leq K}\sum_{(i,j)\in \C_k\times \C_l}p_{j,i},
$$
where $p'_{i,j} = \mathbb{P}_{\Sigma_{\C}\sim P_{\C}}(\Sigma_{\C}(i)<\Sigma_{\C}(j)) = p_{i,j}\mathbb{I}\{k=l\} + \mathbb{I}\{k<l\}$,
with $(k,l)\in\{1,\dots,K\}^2$ such that $(i,j)\in\C_k\times\C_l$.

\subsection*{Proof of Proposition \ref{prop:kendall_prop}}
Let $\C$ be a bucket order of $\n$ with $K$ buckets. Then, for $P'\in \mathbf{P}_{\C}$, Lemma \ref{lem:Kendall} says:
\begin{equation*}
    W_{d_{\tau},1}\left(P,P'  \right) \ge \sum_{i<j}\vert p'_{i,j}-p_{i,j} \vert = \sum_{k=1}^K \sum_{i<j, (i,j)\in \C_k^2} \vert p'_{i,j}-p_{i,j} \vert + \sum_{1\leq k<l\leq K}\sum_{(i,j)\in \C_k\times \C_l}p_{j,i},
\end{equation*}
where the equality holds because $p'_{i,j}=1$ when $(i,j)\in\C_k\times \C_l$ with $k<l$.
Conclude observing that the intra-bucket terms are all zero for $P'=P_{\C}$ as $p'_{i,j}=p_{i,j}$ when $(i,j)\in \C_k^2$ ($k\in\{1,\dots,K\}$),
which implies:
$$
\Lambda_P(\C) = W_{d_{\tau},1}\left(P,P_{\C}  \right) = \sum_{1\leq k<l\leq K}\sum_{(i,j)\in \C_k\times \C_l}p_{j,i}.
$$


\subsection*{Proof of Theorem \ref{thm:EDM}}
Observe first that the excess of distortion can be bounded as follows:
$$
\Lambda_P(\widehat{C}_{K,\lambda})-\inf_{\C\in \mathbf{C}_K}\Lambda_P(\C)\leq 2\max_{\C\in \mathbf{C}_{K,\lambda}}\sum_{1\leq k<l\leq K}\sum_{(i,j)\in \C_k\times \C_l}\left\vert \widehat{p}_{j,i}-p_{j,i}\right\vert + \left\{ \inf_{\C\in \mathbf{C}_{K,\lambda}}\Lambda_P(\C)-\inf_{\C\in \mathbf{C}_{K}}\Lambda_P(\C)\right\}.
$$
Apply next Hoeffding's inequality for all $i\neq j$, so as to get, for all $t>0$,
\begin{equation*}
\mathbb{P}\Big\{ \vert p_{i,j}-\widehat{p}_{i,j} \vert > t \Big\} \leq 2e^{-2Nt^{2}},
\end{equation*}
which implies that, for all $\C\in \mathbf{C}_{K,\lambda}$,
$$
\mathbb{P}\Big\{ |\widehat{\Lambda}_N(\C)-\Lambda_P(\C)| > t \Big\} \le 2\kappa(\lambda) e^{-2N\left(t/\kappa(\lambda)\right)^2}.
$$
The desired result then follows from the bound above combined with the union bound and the fact that $\# \mathbf{C}_{K,\lambda}=\binom{n}{\lambda}=n!/(\# \C_1 ! \times \cdots \times \# \C_K !)$.

\begin{remark}
Observe that, for all $\delta\in (0,1)$, we also have with probability at least $1-\delta$:
\begin{equation*}
\max_{\C\in \mathbf{C}_{K,\lambda}}|\widehat{\Lambda}_N(\C)-\Lambda_P(\C)| \leq  \sum_{i<j}\vert p_{i,j}-\widehat{p}_{i,j} \vert \leq \binom{n}{2}\sqrt{\frac{\log \left(2\binom{n}{2}/\delta\right)}{2N}}.
\end{equation*}
\end{remark}

\subsection*{Data Dependent Rate Bounds for the Excess of Distorsion}

Define the Rademacher average
$$
\mathcal{R}_N(\lambda)=\frac{1}{N}\max_{\C\in\mathbf{C}_{K,\lambda}}\left\vert \sum_{m=1}^N\epsilon_m\left( \sum_{i\prec_{\C} j}\mathbb{I}\{\Sigma(j)<\Sigma(i)\}\right) \right\vert,
$$
where $\sigma_1,\; \ldots,\; \sigma_N$ are i.i.d. Rademacher r.v.'s (\textit{i.e.} symmetric sign random variables), independent from the $\Sigma_m$'s. The following lemma follows from classical symmetrization/randomization tricks.

\begin{lemma}
For any nondecreasing convex function $\psi: \mathbb{R}\rightarrow \mathbb{R}$, we have:
$$
\mathbb{E}\left[ \psi\left(\max_{\C\in \mathbf{C}_{K,\lambda}} \left\vert \widehat{\Lambda}_N(\C)-\Lambda_P(\C)\right\vert \right) \right]\leq \mathbb{E}\left[ \psi\left( 2 \mathcal{R}_N(\lambda) \right)\right].
$$
\end{lemma}
Using this result with $\psi(x)=\exp(\gamma x)$ for some $\gamma>0$ combined with McDiarmid's bounded differences inequality applied to the $N$ independent pairs $(\epsilon_m,\Sigma_m)$, we have:
$$
\mathbb{E}\left[ \exp\left(\gamma \max_{\C\in \mathbf{C}_{K,\lambda}} \left\vert \widehat{\Lambda}_N(\C)-\Lambda_P(\C)\right\vert \right) \right]\leq \exp\left( 2\gamma \mathbb{E}[\mathcal{R}_N(\lambda)] +\frac{\gamma^2\kappa(\lambda)^2}{2N} \right),
$$
observing that the value of $\mathcal{R}_N(\lambda)$ cannot change by more than $\kappa(\lambda)/N$ when one modifies one of the $(\epsilon_m,\Sigma_m)$'s, while the others are kept fixed.
Markov inequality with $\gamma=N(t-2\mathbb{E}[\mathcal{R}_N(\lambda)])/\kappa(\lambda)^2$ gives
$$
\mathbb{P}\left\{ \max_{\C\in \mathbf{C}_{K,\lambda}} \left\vert \widehat{\Lambda}_N(\C)-\Lambda_P(\C)\right\vert>t \right\}\leq \exp\left(-N\frac{\left(t-2\mathbb{E}[\mathcal{R}_N(\lambda)]\right)^2}{2\kappa(\lambda)^2}\right).
$$
We finally get that, for any $\delta\in (0,1)$, we have with probability at least $1-\delta$,
$$
\max_{\C\in \mathbf{C}_{K,\lambda}} \left\vert \widehat{\Lambda}_N(\C)-\Lambda_P(\C)\right\vert \leq
2\mathbb{E}[\mathcal{R}_N(\lambda)]+\kappa(\lambda)\sqrt{\frac{2\log(1/\delta)}{N}}.
$$
Introduce also
$$
R_N(\lambda)=\mathbb{E}_{\epsilon}\left[ \mathcal{R}_N(\lambda) \right].
$$
By virtue of McDiarmid's bounded differences inequality, we have with probability larger than $1-\delta$:
$$
\mathbb{E}\left[ \mathcal{R}_N(\lambda) \right]\leq R_N(\lambda)+ \kappa(\lambda)\sqrt{\frac{\log(1/\delta)}{2N}}.
$$
Using the union bound, we deduce that, with probability at least $1-\delta$,
$$
\max_{\C\in \mathbf{C}_{K,\lambda}} \left\vert \widehat{\Lambda}_N(\C)-\Lambda_P(\C)\right\vert \leq
2R_N(\lambda)+\kappa(\lambda)\sqrt{\frac{2\log(2/\delta)}{N}}.
$$
In contrast with that stated in Theorem \ref{thm:EDM}, the bound above depends on the training sample. This result paves the way for choosing a data-driven penalty.
Set
$$
pen(\lambda,N)=3R_N(\lambda)+??
$$
\subsection*{Proof of Theorem \ref{thm:select}}

We first recall the following result, whose proof can be found in \cite{Gabor} for instance (see Lemmas 1.2 and 1.3 therein).
\begin{lemma}\label{lem:max} The following assertions hold true.
\begin{itemize}
\item[(i)] Hoeffding's lemma. Let $Z$ be an integrable r.v. with mean zero such that $a\leq Z \leq b$ almost-surely. Then, we have: $\forall s>0$
$$
\mathbb{E}[\exp(sZ)]\leq \exp\left(s^2(b-a)^2/8\right).
$$
\item[(ii)] Let $M\geq 1$ and $Z_1,\; \ldots,\; Z_M$ be real valued random variables. Suppose that there exists $\sigma>0$ such that $\forall s \in \mathbb{R}$: $\mathbb{E}[\exp(sZ_i)]\leq e^{s^2\sigma^2/2}$ for all $i\in \{1,\; \ldots,\; M \}$. Then, we have:
\begin{equation}
\mathbb{E}\left[ \max_{1\leq i \leq M}\vert Z_i\vert \right]\leq \sigma \sqrt{2\log (2M)}.
\end{equation}
\end{itemize}
\end{lemma}

Observe that we can write, for any bucket order $\C$,
$$
\widehat{\Lambda}_N(\C)-\Lambda_P(\C)=\frac{1}{N}\sum_{m=1}^N\{Z_m(\C)-\mathbb{E}[Z_m(\C)]\},
$$
where we set for all $m\in\{1,\; \ldots,\; N\}$
$$
Z_m(\C)=\sum_{i\prec_{\C}j} \mathbb{I}\{\Sigma_m(j)<\Sigma_m(i)\}.
$$
If $\C$ is of shape $\lambda$, we have $0\leq Z_m(\C)\leq \kappa(\lambda)$. Hence, by virtue of assertion (i) of Lemma \ref{lem:max}, we have for all $s\in \mathbb{R}$:
$\mathbb{E}[\exp(s(Z_1(\C) -\mathbb{E}[Z_1(\C) ] )) ]\leq \exp(s^2\kappa(\lambda)^2/2)$ and, consequently
$$
\mathbb{E}\left[ \exp\left( s\left( \widehat{\Lambda}_N(\C)-\Lambda_P(\C) \right) \right) \right]\leq \exp\left(s^2\kappa(\lambda)^2/(2N)\right).
$$
Now, applying assertion (ii) of Lemma \ref{lem:max} yields:
$$
\mathbb{E}\left[\max_{\C\in \mathbf{C}_{K,\lambda}}\left\vert \widehat{\Lambda}_N(\C)-\Lambda_P(\C)\right\vert \right]\leq \frac{\kappa(\lambda)}{\sqrt{N}}\sqrt{2\log\left( 2\binom{n}{\lambda} \right)  }.
$$
The oracle inequality can be then deduced by following line by line the argument of Theorem 1.20 in \cite{Gabor}. Details are left to the reader.

\subsection*{Proof of Theorem \ref{thm:bucket_median}}
Consider a bucket order $\C=(\C_1,\; \ldots,\; \C_K)$ of shape $\lambda$, different from \eqref{eq:opt_bucket}. Hence, there exists at least a pair $\{i,j\}$ such that $j\prec_{\C}i$ and $\sigma_P^*(j)<\sigma_P^*(i)$ (or equivalently $p_{i,j}<1/2$). Consider such a pair $\{i,j\}$. Hence, there exist $1\leq k<l\leq K$ s.t. $(i,j)\in\C_k\times C_l$.
Define the bucket order $\C'$ which is the same as $\C$ except that the buckets of $i$ and $j$ are 
swapped: $\C'_k=\{j\}\cup\C_k\setminus\{i\}$, $\C'_l=\{i\}\cup\C_l\setminus\{j\}$ and $\C'_m=\C_m$ if $m\in\{1,\dots,K\}\setminus\{k,l\}$. Observe that
\begin{multline*}
\Lambda_{P}(\C')-\Lambda_{P}(\C)= 
p_{i,j}-p_{j,i} + \sum_{a\in\C_k\setminus\{i\}} p_{i,a}-p_{j,a} + \sum_{a\in\C_l\setminus\{j\}} p_{a,j}-p_{a,i}\\
+ \sum_{m=k+1}^{l-1} \sum_{a\in\C_m} p_{a,j}-p_{a,i} + p_{i,a}-p_{j,a} \leq 2( p_{i,j}-1/2)< 0.
\end{multline*}
Considering now all the pairs $\{i,j\}$ such that $j\prec_{\C}i$ and $p_{i,j}<1/2$, it follows by induction that
\begin{equation}\label{eq:lowerb}
\Lambda_{P}(\C)-\Lambda(\C^{*(K,\lambda)})\geq 2\sum_{j\prec_{\C}i}( 1/2-p_{i,j}) \cdot \mathbb{I}\{ p_{i,j}<1/2 \}.
\end{equation}

%For given number of buckets $K\in\n$ and bucket shape $\lambda$, we want to prove that $\argmin_{\C'\in\mathbf{C}_{K,\lambda}} \Lambda_P(\C')=\{\C^{*(K,\lambda)}\}$ or, equivalently, the following implication:
%$$
%\C\in\argmin_{\C'\in\mathbf{C}_{K,\lambda}} \Lambda_{P}(\C') \Rightarrow \C=\C^{*(K,\lambda)}.
%$$
%We proceed by contraposition: let $\C\in\mathbf{C}_{K,\lambda}\setminus\{\C^{*(K,\lambda)}\}$,
%then $\C$ does not agree with consensus $\sigma^*_P$ i.e. there exist $1\le k<l\le K$ and $(i,j)\in\C_k\times C_l$ such that
%$p_{i,j}<\frac{1}{2}$ (see Property \ref{prop:comp}).
%Hence, the bucket order $\C'\in\mathbf{C}_{K,\lambda}$ obtained from $\C$ by switching the pair $(i,j)$
%(i.e. $\C'_k=\{j\}\cup\C_k\setminus\{i\}$, $\C'_l=\{i\}\cup\C_l\setminus\{j\}$ and $\C'_m=\C_m$ if $m\in\{1,\dots,K\}\setminus\{k,l\}$) verifies:
%\begin{equation*}
 % \begin{split}
%&\Lambda_{P}(\C')-\Lambda_{P}(\C)= \\
%&p_{i,j}-p_{j,i} + \sum_{a\in\C_k\setminus\{i\}} p_{i,a}-p_{j,a} + \sum_{a\in\C_l\setminus\{j\}} p_{a,j}-p_{a,i}
%+ \sum_{m=k+1}^{l-1} \sum_{a\in\C_m} p_{a,j}-p_{a,i} + p_{i,a}-p_{j,a} < 0,
 % \end{split}
%\end{equation*}
%where the inequality holds under the strongly stochastically transitivity assumption.



\section*{Proof of Theorem \ref{thm:fast}}

The fast rate analysis essentially relies on the following lemma providing a control of the variance of the empirical excess of distortion
$$
\widehat{\Lambda}_N(\C)-\widehat{\Lambda}_N(\C^{*(K,\lambda)})=\frac{1}{N}\sum_{m=1}^N\sum_{i\neq j}\mathbb{I}\{\Sigma_m(j)<\Sigma_m(i)\}\cdot \left(  \mathbb{I}\left\{ i\prec_{\C}j\}-  \mathbb{I}\{i<_{\C^{*(K,\lambda)}}j \right\} \right).
$$
Set $D(\C)=\sum_{i\neq j}\mathbb{I}\{\Sigma(j)<\Sigma(i)\}\cdot \left(  \mathbb{I}\left\{ i\prec_{\C}j\}-  \mathbb{I}\{i<_{\C^{*(K,\lambda)}} j\right\} \right)$. Observe that $\mathbb{E}[D(\C)]=\Lambda_P(\C)-\Lambda_P(\C^{*(K,\lambda)})$.
\begin{lemma}\label{lem:var_control} Let $\lambda$ be a given bucket order shape. We have:
$$
var\left(D(\C)\right)\leq 2^{\binom{n}{2}}(n^2/h)\cdot \mathbb{E}[D(\C)].
$$
\end{lemma}
\begin{proof}
As in the proof of Theorem \ref{thm:bucket_median}, consider a bucket order $\C=(\C_1,\; \ldots,\; \C_K)$ of shape $\lambda$, different from \eqref{eq:opt_bucket}, a pair $\{i,j\}$ such that there exist $1\leq k<l\leq K$ s.t. $(i,j)\in\C_k\times C_l$ and $\sigma_P^*(j)<\sigma_P^*(i)$ and the bucket order $\C'$ which is the same as $\C$ except that the buckets of $i$ and $j$ are 
swapped. We have:
\begin{multline*}
D(\C')-D(\C)=\mathbb{I}\{\Sigma(i)<\Sigma(j)  \} - \mathbb{I}\{\Sigma(j)<\Sigma(i)  \} + \sum_{a\in\C_k\setminus\{i\}} \mathbb{I}\{\Sigma(i)<\Sigma(a)  \} -\mathbb{I}\{\Sigma(j)<\Sigma(a)  \}\\  + \sum_{a\in\C_l\setminus\{j\}} \mathbb{I}\{\Sigma(a)<\Sigma(j)  \} -\mathbb{I}\{\Sigma(a)<\Sigma(i)  \} \\
+ \sum_{m=k+1}^{l-1} \sum_{a\in\C_m} \mathbb{I}\{\Sigma(a)<\Sigma(j)  \} -\mathbb{I}\{\Sigma(a)<\Sigma(i)  \}  + \mathbb{I}\{\Sigma(i)<\Sigma(a)  \} -\mathbb{I}\{\Sigma(j)<\Sigma(a)  \}.
\end{multline*}

Hence, we have: $var(D(\C')-D(\C))\leq 4n^2$. By induction, we then obtain that:
\begin{multline*}
var\left(D(\C)\right)\leq 2^{\binom{n}{2}-1}(4n^2)\#\left\{(i,j):\; i\prec_{\C}j \text{ and } p_{j,i}>1/2  \right\}\\
\leq 2^{\binom{n}{2}-1}(4n^2/h)\sum_{j\prec_{\C}i}( 1/2-p_{i,j}) \cdot \mathbb{I}\{ p_{i,j}<1/2 \}\leq 2^{\binom{n}{2}}(n^2/h)\mathbb{E}[D(\C)],
\end{multline*}
by combining \eqref{eq:excess_distort} with condition \eqref{eq:hyp_margin0}.
\end{proof}

Applying Bernstein's inequality to the i.i.d. average $(1/N)\sum_{m=1}^ND_m(\C)$, where
\begin{equation*}
D_m(\C)=\sum_{i\neq j}\mathbb{I}\{\Sigma_m(j)<\Sigma_m(i)\}\cdot \left(  \mathbb{I}\left\{ i\prec_{\C}j\}-  \mathbb{I}\{i<_{\C^{*(K,\lambda)}} j\right\} \right),
\end{equation*}
for $1\leq m\leq N$ and the union bound over the bucket orders $\C$ in $\mathbf{C}_{K,\lambda}$ (recall that $\#\mathbf{C}_{K,\lambda}= \binom{n-1}{K-1}$), we obtain that, for all $\delta\in (0,1)$, we have with probability larger than $1-\delta$: $\forall \C\in \mathbf{C}_{K,\lambda}$,
\begin{multline*}
\mathbb{E}[D(\C)]= \Lambda_P(\C)-\Lambda_P(\C^{*(K,\lambda)}) \leq \widehat{\Lambda}_N(\C)-\widehat{\Lambda}_N(\C^{*(K,\lambda)})+\sqrt{\frac{2 var(D(\C))\log\left(\binom{n-1}{K-1}/\delta\right)}{N}} \\+\frac{4\log(\binom{n-1}{K-1}/\delta)}{3N}.
\end{multline*}
Since $\widehat{\Lambda}_N(\widehat{C}_{K,\lambda})-\widehat{\Lambda}_N(\C^{*(K,\lambda)}) \leq 0$ by assumption and using the variance control provided by Lemma \ref{lem:var_control} above, we obtain that, with probability at least $1-\delta$, we have:
\begin{multline*}
 \Lambda_P(\widehat{C}_{K,\lambda})-\Lambda_P(\C^{*(K,\lambda)})\leq\sqrt{\frac{2^{\binom{n}{2}-2}4n^2\left( \Lambda_P(\widehat{C}_{K,\lambda})-\Lambda_P(\C^{*(K,\lambda)})\right)/h \times \log(\binom{n-1}{K-1}/\delta)}{N}}\\+\frac{4\log(\binom{n-1}{K-1}/\delta)}{3N}.
\end{multline*}
Finally, solving this inequality in $ \Lambda_P(\widehat{C}_{K,\lambda})-\Lambda_P(\C^{*(K,\lambda)})$ yields the desired result.


\section*{The Spearman $\rho$ case}

The approach developed in the article mainly relies on the choice of the Kendall's $\tau$ distance as cost function involved in the Wasserstein metric.
The following result shows that the alternative study based on the $2$-nd Wasserstein metric with the Spearman $\rho$ distance $d_2$ as cost function
would lead to a different distortion measure: $\Lambda'_{P}(\C)=\min_{P'\in \mathbf{P}_{\C}}W_{d_2,2}(P,P')$,
whose explicit formula writes in terms of the triplet-wise probabilities $p_{i,j,k}=\mathbb{P}_{\Sigma\sim P}\{\Sigma(i)<\Sigma(j)<\Sigma(k)\}$ and $p'_{i,j,k}=~\mathbb{P}_{\Sigma'\sim P'}\{\Sigma'(i)<\Sigma'(j)<\Sigma'(k)\}$.
Moreover, the coupling $(\Sigma,\Sigma_{\C})$ is also optimal in this case as the distortion verifies $\Lambda'_{P}(\C) = \mathbb{E}\left[d_2^2\left(\Sigma,\Sigma_{\C} \right)\right]$.

\begin{lemma}\label{lem:squared_spearman} Let $n\ge 3$ and $P$ be a probability distribution on $\mathfrak{S}_n$.
  \begin{itemize}
  \item[(i)] For any probability distribution $P'$ on $\mathfrak{S}_n$:
  $$
  W_{d_2,2}\left(P,P'  \right)
  \ge \frac{2}{n-2}\sum_{a<b<c}\left\{ \sum_{(i,j,k)\in\sigma(a,b,c)} \max(p_{i,j,k}, p'_{i,j,k}) - 1 \right\},
  $$
  where $\sigma(a,b,c)$ is the set of permutations of triplet $(a,b,c)$.
  \item[(ii)] If $P' \in \mathbf{P}_{\C}$ with $\C$ a bucket order of $\n$ with $K$ buckets:
  \begin{equation}
    \label{eq:Wspearman}
    \begin{split}
      &W_{d_2,2}\left(P,P'  \right) \ge\\
      &\frac{2}{n-2} \sum_{1\le k<l<m\le K} \sum_{(a,b,c)\in\C_k\times\C_l\times\C_m} (n+1)p_{c,b,a}+n(p_{b,c,a}+p_{c,a,b})+p_{b,a,c}+p_{a,c,b}\\
      &+ \frac{2}{n-2} \sum_{1\le k<l\le K} \Biggl\{ \sum_{(a,b,c)\in\C_k\times\C_l\times\C_l} n(p_{b,c,a}+p_{c,b,a})+p_{b,a,c}+p_{c,a,b}\\
      &\qquad\qquad\qquad\qquad\qquad\qquad+\sum_{(a,b,c)\in\C_k\times\C_k\times\C_l} n(p_{c,a,b}+p_{c,b,a})+p_{a,c,b}+p_{b,c,a}\Biggr\},
    \end{split}
  \end{equation}
  with equality when $P'=P_{\C}$ is the distribution of $\Sigma_{\C}$.
\end{itemize}
\end{lemma}

\begin{proof}

\noindent{\bf (i).}
Consider a coupling $(\Sigma,\Sigma')$ of two probability distributions $P$ and $P'$ on $\mathfrak{S}_n$.
For clarity's sake, we will assume that $\tilde{p}_{i,j,k} = \min(p_{i,j,k},p'_{i,j,k}) >0$ for all triplets $(i,j,k)$, the extension to the general case being straightforward.
We also denote $\bar{p}_{i,j,k} = \max(p_{i,j,k},p'_{i,j,k})$.
Given two pairs of three distinct elements of $\n$, $(i, j, k)$ and $(a, b, c)$, we define the following quantities:
$$
\pi_{a,b,c|i,j,k} = \mathbb{P}\left\{ \Sigma'(a)<\Sigma'(b)<\Sigma'(c)\mid \Sigma(i)<\Sigma(j)<\Sigma(k) \right\},
$$
$$
\pi'_{a,b,c|i,j,k} = \mathbb{P}\left\{ \Sigma(a)<\Sigma(b)<\Sigma(c)\mid \Sigma'(i)<\Sigma'(j)<\Sigma'(k) \right\},
$$
$$
\tilde{\pi}_{a,b,c|i,j,k} = \pi_{a,b,c|i,j,k}\mathbb{I}\{p_{i,j,k}\le p'_{i,j,k}\} + \pi'_{a,b,c|i,j,k}\mathbb{I}\{p_{i,j,k} > p'_{i,j,k}\},
$$
$$
\bar{\pi}_{a,b,c|i,j,k} = \pi_{a,b,c|i,j,k}\mathbb{I}\{p_{i,j,k} > p'_{i,j,k}\} + \pi'_{a,b,c|i,j,k}\mathbb{I}\{p_{i,j,k} \le p'_{i,j,k}\}.
$$
The interest of defining the $\tilde{\pi}_{a,b,c|i,j,k}$'s is that it will allow us to choose $\tilde{\pi}_{i,j,k|i,j,k}=1$ at the end of the proof, which implies $\bar{\pi}_{i,j,k|i,j,k} = \frac{\tilde{p}_{i,j,k}}{\bar{p}_{i,j,k}}$.
Throughout the proof, the triplets $(a, b, c)$ will always be permutations of $(i, j, k)$.
Now write
$$
\mathbb{E}\left[ d_2\left( \Sigma,\Sigma' \right)^2\right] = \sum_{i=1}^n \E[\Sigma(i)^2] + \E[\Sigma'(i)^2] - 2 \E[\Sigma(i) \Sigma'(i)]\\,
$$
where
$$
\E[\Sigma(i)^2] = \E[(1+\sum_{j\neq i}\mathbb{I}\{\Sigma(j)<\Sigma(i)\})^2] = 1 + \sum_{j\neq i} (n+1) p_{j,i} - \sum_{k\neq i,j} p_{j,i,k}
$$
and
\begin{equation*}
  \begin{split}
    \E[\Sigma(i) \Sigma'(i)] &= 1 + \sum_{j\neq i} p_{j,i}+p'_{j,i}+\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\} \\
    &+ \sum_{k\neq i,j} \mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\}.
  \end{split}
\end{equation*}

Hence,
\begin{equation}
  \label{eq:spearman_triplets}
  \begin{split}
    \mathbb{E}\left[ d_2\left( \Sigma,\Sigma' \right)^2\right] =
    \sum_{a<b<c} \sum_{(i,j,k)\in \sigma(a,b,c)} & \frac{1}{n-2}\left\{(n-1) (p_{j,i}+p'_{j,i})-2\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\}\right\}\\
    &-p_{j,i,k}-p'_{j,i,k}-2\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\},
  \end{split}
\end{equation}
where $\sigma(a,b,c)$ is the set of the $6$ permutations of triplet $(a,b,c)$.
Some terms simplify in Eq. \ref{eq:spearman_triplets} when summing over $\sigma(a,b,c)$, namely:
$$
\sum_{(i,j,k)\in \sigma(a,b,c)} \frac{n-1}{n-2}(p_{j,i}+p'_{j,i})-p_{j,i,k}-p'_{j,i,k} = \frac{4n-2}{n-2}.
$$
We now simply have:
\begin{equation}
  \begin{split}
    \mathbb{E}\left[ d_2\left( \Sigma,\Sigma' \right)^2\right] =
    \sum_{a<b<c} \frac{4n-2}{n-2} - 2\sum_{(i,j,k)\in \sigma(a,b,c)} & \frac{1}{n-2}\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\}\\
    &+\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\}.
  \end{split}
\end{equation}

Observe that for all triplets $(a,b,c)$ and $(i,j,k)$,
\begin{equation*}
  \begin{split}
    &\mathbb{P}(\Sigma'(a)<\Sigma'(b)<\Sigma'(c), \Sigma(i)<\Sigma(j)<\Sigma(k))
    + \mathbb{P}(\Sigma'(i)<\Sigma'(j)<\Sigma'(k), \Sigma(a)<\Sigma(b)<\Sigma(c))\\
    &= \pi_{a,b,c|i,j,k}p_{i,j,k} + \pi'_{a,b,c|i,j,k}p'_{i,j,k}.
  \end{split}
\end{equation*}
Then, by the law of total probability, we have for all distinct $i,j,k$,
\begin{equation*}
  \begin{split}
    &\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\}\\
    &= \frac{1}{2}\{\pi_{j,k,i|j,k,i}p_{j,k,i}+\pi'_{j,k,i|j,k,i}p'_{j,k,i}\}\\
    &+ \frac{1}{2}\{\pi_{k,j,i|k,j,i}p_{k,j,i}+\pi'_{k,j,i|k,j,i}p'_{k,j,i}\}\\
    &+ \frac{1}{2}\{\pi_{j,i,k|j,i,k}p_{j,i,k}+\pi'_{j,i,k|j,i,k}p'_{j,i,k}\}\\
    &+ \frac{1}{2}\{\pi_{j,i,k|j,k,i}p_{j,k,i}+\pi'_{j,i,k|j,k,i}p'_{j,k,i}+\pi_{j,k,i|j,i,k}p_{j,i,k}+\pi'_{j,k,i|j,i,k}p'_{j,i,k}\}\\
    &+ \frac{1}{2}\{\pi_{k,j,i|j,k,i}p_{j,k,i}+\pi'_{k,j,i|j,k,i}p'_{j,k,i}+\pi_{j,k,i|k,j,i}p_{k,j,i}+\pi'_{j,k,i|k,j,i}p'_{k,j,i}\}\\
    &+ \frac{1}{2}\{\pi_{j,i,k|k,j,i}p_{k,j,i}+\pi'_{j,i,k|k,j,i}p'_{k,j,i} + \pi_{k,j,i|j,i,k}p_{j,i,k}+\pi'_{k,j,i|j,i,k}p'_{j,i,k}\},
  \end{split}
\end{equation*}
and
\begin{equation*}
  \begin{split}
    &\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\}\\
    &= \frac{1}{2}\{\pi_{j,k,i|j,k,i}p_{j,k,i} + \pi'_{j,k,i|j,k,i}p'_{j,k,i}\}\\
    &+ \frac{1}{2}\{\pi_{k,j,i|k,j,i} p_{k,j,i} + \pi'_{k,j,i|k,j,i} p'_{k,j,i}\}\\
    &+ \frac{1}{2}\{\pi_{k,j,i|j,k,i}p_{j,k,i} + \pi'_{k,j,i|j,k,i}p'_{j,k,i} + \pi_{j,k,i|k,j,i}p_{k,j,i} + \pi'_{j,k,i|k,j,i}p'_{k,j,i}\}\\
    &+ \mathbb{P}(\Sigma'(j)<\Sigma'(k)<\Sigma'(i), \Sigma(j)<\Sigma(i)<\Sigma(k))\\
    &+ \mathbb{P}(\Sigma'(k)<\Sigma'(i)<\Sigma'(j), \Sigma(j)<\Sigma(k)<\Sigma(i))\\
    &+ \mathbb{P}(\Sigma'(k)<\Sigma'(j)<\Sigma'(i), \Sigma(j)<\Sigma(i)<\Sigma(k))\\
    &+ \mathbb{P}(\Sigma'(k)<\Sigma'(i)<\Sigma'(j), \Sigma(k)<\Sigma(j)<\Sigma(i))\\
    &+ \mathbb{P}(\Sigma'(k)<\Sigma'(i)<\Sigma'(j), \Sigma(j)<\Sigma(i)<\Sigma(k)),
  \end{split}
\end{equation*}
which implies:
\begin{equation*}
\begin{split}
  &\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\} + \mathbb{P}\{\Sigma(k)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\}\\
  &= \pi_{j,k,i|j,k,i}p_{j,k,i} + \pi'_{j,k,i|j,k,i}p'_{j,k,i}\\
  &+ \pi_{k,j,i|k,j,i} p_{k,j,i} + \pi'_{k,j,i|k,j,i} p'_{k,j,i}\\
  &+ \pi_{k,j,i|j,k,i}p_{j,k,i} + \pi'_{k,j,i|j,k,i}p'_{j,k,i} + \pi_{j,k,i|k,j,i}p_{k,j,i} + \pi'_{j,k,i|k,j,i}p'_{k,j,i}\\
  &+ \frac{1}{2}\left\{\pi_{j,k,i|j,i,k}p_{j,i,k} + \pi'_{j,k,i|j,i,k}p'_{j,i,k} + \pi_{j,i,k|j,k,i}p_{j,k,i} + \pi'_{j,i,k|j,k,i}p'_{j,k,i}\right\}\\
  &+ \frac{1}{2}\left\{\pi_{k,i,j|j,k,i}p_{j,k,i} + \pi'_{k,i,j|j,k,i}p'_{j,k,i} + \pi_{j,k,i|k,i,j}p_{k,i,j} + \pi'_{j,k,i|k,i,j}p'_{k,i,j}\right\}\\
  &+ \frac{1}{2}\left\{\pi_{k,j,i|j,i,k}p_{j,i,k} + \pi'_{k,j,i|j,i,k}p'_{j,i,k} + \pi_{j,i,k|k,j,i}p_{k,j,i} + \pi'_{j,i,k|k,j,i}p'_{k,j,i}\right\}\\
  &+ \frac{1}{2}\left\{\pi_{k,i,j|k,j,i}p_{k,j,i} + \pi'_{k,i,j|k,j,i}p'_{k,j,i} + \pi_{k,j,i|k,i,j}p_{k,i,j} + \pi'_{k,j,i|k,i,j}p'_{k,i,j}\right\}\\
  &+ \frac{1}{2}\left\{\pi_{k,i,j|j,i,k}p_{j,i,k} + \pi'_{k,i,j|j,i,k}p'_{j,i,k} + \pi_{j,i,k|k,i,j}p_{k,i,j} + \pi'_{j,i,k|k,i,j}p'_{k,i,j}\right\},
\end{split}
\end{equation*}
which is symmetric by permuting indices $j$ and $k$.
Hence,
\begin{equation}
  \label{eq:Habc}
  \begin{split}
    &H(a,b,c) = \sum_{(i,j,k)\in \sigma(a,b,c)} \frac{1}{n-2}\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\}
    +\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\}\\
    &= \sum_{(i,j,k)\in \sigma(a,b,c)} \Biggl\{\frac{2n-1}{2(n-2)}\tilde{\pi}_{j,k,i|j,k,i} + \frac{n-1}{n-2}(\tilde{\pi}_{k,j,i|j,k,i}+ \tilde{\pi}_{j,i,k|j,k,i})\\
    &\qquad\qquad\qquad\qquad + \frac{n-1}{2(n-2)}( \tilde{\pi}_{k,i,j|j,k,i} + \tilde{\pi}_{i,j,k|j,k,i} ) + \frac{1}{2}\tilde{\pi}_{i,k,j|j,k,i} \Biggr\}\tilde{p}_{j,k,i}\\
    &+ \Biggl\{\frac{2n-1}{2(n-2)}\bar{\pi}_{j,k,i|j,k,i} + \frac{n-1}{n-2}(\bar{\pi}_{k,j,i|j,k,i} + \bar{\pi}_{j,i,k|j,k,i}) + \frac{n-1}{2(n-2)}( \bar{\pi}_{k,i,j|j,k,i} + \bar{\pi}_{i,j,k|j,k,i} ) + \frac{1}{2}\bar{\pi}_{i,k,j|j,k,i} \Biggr\}\bar{p}_{j,k,i},
  \end{split}
\end{equation}
which is maximized when $\tilde{\pi}_{j,k,i|j,k,i}=1$ (which implies $\bar{\pi}_{j,k,i|j,k,i}=\frac{\tilde{p}_{j,k,i}}{\bar{p}_{j,k,i}}$) and $\bar{\pi}_{k,j,i|j,k,i}+\bar{\pi}_{j,i,k|j,k,i} = 1 - \frac{\tilde{p}_{j,k,i}}{\bar{p}_{j,k,i}}$
for all $(i,j,k)\in\sigma(a,b,c)$ and then verifies:
\begin{equation}
\label{eq:Habc_max}
\begin{split}
  H(a,b,c) &\le \sum_{(i,j,k)\in \sigma(a,b,c)} \frac{n}{n-2}\tilde{p}_{i,j,k} + \frac{n-1}{n-2}\bar{p}_{i,j,k}
  = \frac{1}{n-2}\sum_{(i,j,k)\in \sigma(a,b,c)} n(p_{i,j,k}+p'_{i,j,k}) - \bar{p}_{i,j,k}\\
  &= \frac{1}{n-2}\left\{ 2n - \sum_{(i,j,k)\in \sigma(a,b,c)} \bar{p}_{i,j,k}\right\},
\end{split}
\end{equation}
which concludes the first part of the proof.

\noindent{\bf (ii).}
Now we consider the particular case of $P'\in \mathbf{P}_{\C}$, with $\C$ a bucket order of $\n$ with $K$ buckets.
We propose to prove that $\min_{P'\in\mathbf{P}_{\C}} W_{d_2,2}(P,P')= W_{d_2,2}(P,P_{\C}) =\E[d_2^2(\Sigma,\Sigma_{\C})]$ and to obtain an explicit expression.
Given three distinct indices $1\le a<b<c\le n$, we analyze the following four possible situations
to reveal what are the optimal values of the conditional probabilities in Eq. \ref{eq:Habc}:
\begin{itemize}
  \item $(a,b,c)\in \C_k$ are in the same bucket: the maximizing conditions are $\tilde{\pi}_{j,k,i|j,k,i}=1$ and $\bar{\pi}_{k,j,i|j,k,i}+\bar{\pi}_{j,i,k|j,k,i} = 1 - \frac{\tilde{p}_{j,k,i}}{\bar{p}_{j,k,i}}$.
  Both are verified when $P'=P_{\C}$ and $\Sigma'=\Sigma_{\C}$ as $\Sigma(j)<\Sigma(k)<\Sigma(i)$ iff $\Sigma_{\C}(j)<\Sigma_{\C}(k)<\Sigma_{\C}(i)$.
  Hence, using Eq. \ref{eq:Habc_max}:
  $$
  H(a,b,c)\le \frac{1}{n-2}\left\{ 2n - \sum_{(i,j,k)\in \sigma(a,b,c)} \bar{p}_{i,j,k}\right\}\le \frac{2n-1}{n-2}.
  $$
  Moreover, this upper bound is attained when $\Sigma'=\Sigma_{\C}$: $H(a,b,c)=\frac{2n-1}{n-2}$.
  \item $(a,b,c)\in \C_k\times\C_l\times\C_m$ are in three different buckets ($k<l<m$):
  this situation is fully characterized by the bucket structure and is hence independent of the coupling $(\Sigma,\Sigma')$.
  For all $(j,k,i)\in\sigma(a,b,c)\setminus\{(a,b,c)\}$, $p'_{j,k,i}=\tilde{p}_{j,k,i}=0$ so Eq. \ref{eq:Habc} is not completely defined
  but $H(a,b,c)$ rewrites more simply without the terms corresponding to the five impossible events $\Sigma'(j)<\Sigma'(k)<\Sigma'(i)$.
  If $(j,k,i)\neq(a,b,c)$, $\bar{p}_{j,k,i}=p_{j,k,i}$ and $\bar{\pi}_{a,b,c|j,k,i}=1$ so the sum of these contributions in $H(a,b,c)$ is:
  \begin{equation}
  \label{eq:3diff_buckets_1}
    \frac{n-1}{n-2}(p_{b,a,c}+p_{a,c,b}) + \frac{n-1}{2(n-2)}(p_{b,c,a}+p_{c,a,b}) + \frac{1}{2}p_{c,b,a}.
  \end{equation}
  We have $p_{a,b,c}\le p'_{a,b,c}=1$ so the condition $\tilde{\pi}_{a,b,c|a,b,c}=1$ is realized
  and for all $(i,j,k)\in\sigma(a,b,c)$, $\bar{\pi}_{i,j,k|a,b,c}=p_{i,j,k}$. The sum of the corresponding contributions in $H(a,b,c)$ is:
  \begin{equation}
    \label{eq:3diff_buckets_2}
    \frac{2n-1}{n-2}p_{a,b,c} + \frac{n-1}{n-2}(p_{b,a,c}+p_{a,c,b}) + \frac{n-1}{2(n-2)}(p_{b,c,a}+p_{c,a,b}) + \frac{1}{2}p_{c,b,a}.
  \end{equation}
  Finally, by combining equations \ref{eq:3diff_buckets_1} and \ref{eq:3diff_buckets_2},
  $$
  H(a,b,c) = \frac{2n-1}{n-2}p_{a,b,c} + \frac{2(n-1)}{n-2}(p_{b,a,c}+p_{a,c,b}) + \frac{n-1}{n-2}(p_{b,c,a}+p_{c,a,b}) + p_{c,b,a}.
  $$
  \item $(a,b,c)\in \C_k\times\C_l\times\C_l$ are in two different buckets ($k<l$) such that $a$ is ranked first among the triplet.
  For all $(j,k,i)\in\sigma(a,b,c)\setminus\{(a,b,c),(a,c,b)\}$, $p'_{j,k,i}=\tilde{p}_{j,k,i}=0$ so Eq. \ref{eq:Habc} is not completely defined
  but $H(a,b,c)$ rewrites more simply without the terms corresponding to the four impossible events $\Sigma'(j)<\Sigma'(k)<\Sigma'(i)$.
  For all $(j,k,i)\in\sigma(a,b,c)$, $\pi_{a,b,c|j,k,i}+\pi_{a,c,b|j,k,i}=1$, and the sum of their contributions in $H(a,b,c)$ is:
  \begin{equation}
    \begin{split}
      &\left(\frac{2n-1}{2(n-2)}\pi_{a,b,c|a,b,c} + \frac{n-1}{n-2}\pi_{a,c,b|a,b,c}\right)p_{a,b,c}
      +\left(\frac{2n-1}{2(n-2)}\pi_{a,c,b|a,c,b} + \frac{n-1}{n-2}\pi_{a,b,c|a,c,b}\right)p_{a,c,b}\\
      &+\left(\frac{n-1}{2(n-2)}\pi_{a,b,c|b,c,a} + \frac{1}{2}\pi_{a,c,b|b,c,a}\right)p_{b,c,a}
      +\left(\frac{n-1}{n-2}\pi_{a,b,c|b,a,c} + \frac{n-1}{2(n-2)}\pi_{a,c,b|b,a,c}\right)p_{b,a,c}\\
      &+\left(\frac{n-1}{2(n-2)}\pi_{a,c,b|c,b,a} + \frac{1}{2}\pi_{a,b,c|c,b,a}\right)p_{c,b,a}
      +\left(\frac{n-1}{n-2}\pi_{a,c,b|c,a,b} + \frac{n-1}{2(n-2)}\pi_{a,b,c|c,a,b}\right)p_{c,a,b}.
    \end{split}
  \end{equation}
  Observe that the expression above is maximized when $\pi_{a,b,c|a,b,c}=\pi_{a,c,b|a,c,b}=\bar{\pi}_{a,b,c|b,c,a}=\bar{\pi}_{a,b,c|b,a,c}=\bar{\pi}_{a,c,b|c,b,a}=\bar{\pi}_{a,c,b|c,a,b}=1$,
  which is verified by $\Sigma'=\Sigma_{\C}$. In this case, Eq. \ref{eq:2diff_buckets_1} becomes:
  \begin{equation}
  \label{eq:2diff_buckets_1}
      \frac{2n-1}{2(n-2)}(p_{a,b,c}+p_{a,c,b})+\frac{n-1}{n-2}(p_{b,a,c}+p_{c,a,b}) + \frac{n-1}{2(n-2)}(p_{b,c,a}+p_{c,b,a})
  \end{equation}
  Now consider $(j,k,i)\in\{(a,b,c),(a,c,b)\}$: $p'_{a,b,c} + p'_{a,c,b} = 1$ and the corresponding contributions to $H(a,b,c)$ sum as follows:
  \begin{equation*}
    \begin{split}
      &\Biggl\{\frac{2n-1}{2(n-2)}\pi'_{a,b,c|a,b,c} + \frac{n-1}{n-2}(\pi'_{b,a,c|a,b,c}+\pi'_{a,c,b|a,b,c})\\
      &+ \frac{n-1}{2(n-2)}(\pi'_{b,c,a|a,b,c}+\pi'_{c,a,b|a,b,c}) + \frac{1}{2}\pi'_{c,b,a|a,b,c}\Biggr\}p'_{a,b,c}\\
      & +\Biggl\{\frac{2n-1}{2(n-2)}\pi'_{a,c,b|a,c,b} + \frac{n-1}{n-2}(\pi'_{c,a,b|a,c,b}+\pi'_{a,b,c|a,c,b})\\
      &+ \frac{n-1}{2(n-2)}(\pi'_{c,b,a|a,c,b}+\pi'_{b,a,c|a,c,b}) + \frac{1}{2}\pi'_{b,c,a|a,c,b}\Biggr\}p'_{a,c,b},
    \end{split}
  \end{equation*}
  which is maximized when $\pi'_{a,c,b|a,b,c}=\pi'_{c,a,b|a,b,c}=\pi'_{c,b,a|a,b,c}=0$
  and $\pi'_{a,b,c|a,c,b}=\pi'_{b,a,c|a,c,b}=\pi'_{b,c,a|a,c,b}=0$: both conditions are true for $\Sigma'=\Sigma_{\C}$.
  Then, the expression above is upper bounded by:
  \begin{equation}
    \label{eq:2diff_buckets_2}
    \frac{2n-1}{2(n-2)}(p_{a,b,c}+p_{a,c,b})+\frac{n-1}{n-2}(p_{b,a,c}+p_{c,a,b}) + \frac{n-1}{2(n-2)}(p_{b,c,a}+p_{c,b,a})
  \end{equation}
  with equality when $\Sigma'=\Sigma_{\C}$.
  Finally, by summing the terms in \ref{eq:2diff_buckets_1} and \ref{eq:2diff_buckets_2},
  $$
  H(a,b,c) \le \frac{2n-1}{n-2}(p_{a,b,c}+p_{a,c,b}) + \frac{2(n-1)}{n-2}(p_{b,a,c}+p_{c,a,b}) + \frac{n-1}{n-2}(p_{b,c,a}+p_{c,b,a}),
  $$
  where the equality holds for $\Sigma'=\Sigma_{\C}$.
  \item $(a,b,c)\in \C_k\times\C_k\times\C_l$ are in two different buckets ($k<l$) such that $c$ is ranked last among the triplet.
  Similarly as in the previous situation, we obtain:
  $$
  H(a,b,c) \le \frac{2n-1}{n-2}(p_{a,b,c}+p_{b,a,c}) + \frac{2(n-1)}{n-2}(p_{a,c,b}+p_{b,c,a}) + \frac{n-1}{n-2}(p_{c,a,b}+p_{c,b,a}),
  $$
  where the equality holds for $\Sigma'=\Sigma_{\C}$.
\end{itemize}
As a conclusion, we proved that: $\min_{P'\in\mathbf{P}_{\C}} W_{d_2,2}(P,P')= W_{d_2,2}(P,P_{\C}) =\E[d_2^2(\Sigma,\Sigma_{\C})]$.

\end{proof}

\section*{The Hamming case}

We provide a lower bound on the $1$-st Wasserstein metric with the Hamming distance $d_H$ as cost function.

\begin{lemma}\label{lem:hamming} For any probability distributions $P$ and $P'$ on $\mathfrak{S}_n$:
	\begin{equation*}
W_{d_H,1}(P,P')\ge \sum_{i=1}^{n}\left\{1 - \sum_{j=1}^{n}\min(q_{i,j}, q'_{i,j})\right\},
	\end{equation*}
	where $q_{i,j}=\mathbb{P}_{\Sigma\sim P}\{ \Sigma(i)=j \}$ and $q'_{i,j}=\mathbb{P}_{\Sigma'\sim P'}\{ \Sigma'(i)=j \}$.
\end{lemma}

\begin{proof}

Consider a coupling $(\Sigma,\Sigma')$ of two probability distributions $P$ and $P'$ on $\mathfrak{S}_n$. For all $i,j,k$, set
$$
\rho_{i,j,k}=\mathbb{P}\left\{ \Sigma'(i)= k\mid  \Sigma(i)=j \right\} \text{ and } \rho'_{i,j,k}=\mathbb{P}\left\{ \Sigma(i)= k\mid  \Sigma'(i)=j \right\}.$$
For simplicity, we assume throughout the proof that $\min(q_{i,j}, q'_{i,j})>0$ for all $(i,j)\in\n^2$, the generalization being straightforward.
We may write
\begin{equation}
\label{eq:hamming}
\begin{split}
\mathbb{E}\left[ d_{H}\left( \Sigma,\Sigma' \right)\right]
&=\sum_{i=1}^{n} \Prob{\Sigma(i)\ne\Sigma'(i)} = \sum_{i=1}^{n} \sum_{j=1}^n\sum_{k\neq j} \Prob{\Sigma(i)=j, \Sigma'(i)=k}\\
&= \sum_{i=1}^{n}\sum_{j=1}^{n}\sum_{k\ne j}\rho_{i,j,k} q_{i,j}
= \sum_{i=1}^{n}\sum_{j=1}^{n}q_{i,j}\left( 1 - \rho_{i,j,j} \right)
= n - \sum_{i,j=1}^{n} \rho_{i,j,j}q_{i,j}.
\end{split}
\end{equation}
For $(i,j)\in\n^2$, the quantity $\rho_{i,j,j} q_{i,j}$ is maximized when $\rho_{i,j,j}=1$, which requires that $q_{i,j}\le~ q'_{i,j}$.
If $q_{i,j}> q'_{i,j}$, rather write in a similar fashion:
\begin{align*}
\mathbb{E}\left[ d_{H}\left( \Sigma,\Sigma' \right)\right]&= n - \sum_{i,j=1}^{n} \rho'_{i,j,j}q'_{i,j},
\end{align*}
and set $\rho'_{i,j,j}=1$. We thus have from Eq. \eqref{eq:hamming}:
\begin{equation*}
\begin{split}
W_{d_H,1}(P,P') &\ge
\sum_{i=1}^n \inf_{(\Sigma,\Sigma') \text { s.t. } \mathbb{P}\{\Sigma(i)=j\}=q_{i,j} \text{ and } \mathbb{P}\{\Sigma'(i)=j\}=q'_{i,j} } \left\{1 - \sum_{j=1}^{n} \Prob{\Sigma(i)=\Sigma'(i)=j} \right\}\\
&= \sum_{i=1}^{n}\left\{1 - \sum_{j=1}^{n}\min(q_{i,j}, q'_{i,j})\right\}.
\end{split}
\end{equation*}

\end{proof}
