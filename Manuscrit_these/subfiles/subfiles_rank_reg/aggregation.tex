\section{Aggregation of piecewise constant predictive ranking rules}
 Let $\mathcal{P}$ be a partition of the input space $\mathcal{X}$. By definition, a subpartition of $\mathcal{P}$ is any partition $\mathcal{P}'$ of $\mathcal{X}$ with the property that, for any $\mathcal{C}'\in \mathcal{P}'$, there exists $\mathcal{C}\in \mathcal{P}$ such that $\mathcal{C}'\subset \mathcal{C}$. Given a collection $\mathcal{P}_1,\;  \ldots,\; \mathcal{P}_B$ of $B\geq 1$ partitions of $\mathcal{X}$, we call the 'largest' subpartition $\bar{\mathcal{P}}_B$ of the $\mathcal{P}_b$'s the partition of $\mathcal{X}$ that is a subpartition of each $\mathcal{P}_b$ and is such that, any subpartition of all the $\mathcal{P}_b$'s is also a subpartition of $\bar{\mathcal{P}}_B$ (notice incidentally that the cells of $\bar{\mathcal{P}}_B$ are of the form $\mathcal{C}_1\cap \cdots\cap \mathcal{C}_B$, where $(\mathcal{C}_1,\; \ldots,\; \mathcal{C}_B)\in \mathcal{P}_1\times \cdots\times \mathcal{P}_B$).
 Considering now $B\geq 1$ piecewise constant ranking rules $s_1,\; \ldots,\; s_B$ associated with partitions $\mathcal{P}_1,\;  \ldots,\; \mathcal{P}_B$ respectively, we observe that the $s_b$'s are constant on each cell of $\bar{\mathcal{P}}_B$. One may thus write: $\forall b\in \{1,\; \ldots,\; B \}$,
 $$
 s_b(x)= \sum_{\mathcal{C}\in \bar{\mathcal{P}}_B}\sigma_{\mathcal{C}, b}\cdot \mathbb{I}\{x\in \mathcal{C}, b  \}.
$$
For any arbitrary $s(x)=\sum_{\mathcal{C}\in \bar{\mathcal{P}}_{B}}\sigma_{\mathcal{C}}\mathbb{I}\{ x\in \mathcal{C} \}$ in $\mathcal{S}_{\bar{\mathcal{P}}_B}$, we have:
\begin{equation*}
\mathbb{E}_{X\sim \mu}\left[\sum_{b=1}^B d_{\tau}\left(s(X),s_b(X)  \right)  \right]=\sum_{b=1}^B\sum_{\mathcal{C}\in \bar{\mathcal{P}}_B}\mu(\mathcal{C}  )d_{\tau}\left(\sigma_{\mathcal{C}}, \sigma_{\mathcal{C},b} \right)=\sum_{\mathcal{C}\in \bar{\mathcal{P}}_B}\sum_{b=1}^B\mu(\mathcal{C}  )d_{\tau}\left(\sigma_{\mathcal{C}}, \sigma_{\mathcal{C},b} \right).
\end{equation*}
Hence, the quantity above is minimum when, for each $\mathcal{C}$, the permutation $\sigma_{\mathcal{C}}$ is a Kemeny median of the probability distribution $(1/B)\sum_{b=1}^B\delta_{\sigma_{\mathcal{C},b}}$.

\subsection*{Consistency preservation and aggregation}
We now state and prove a result showing that the (possibly randomized) aggregation procedure previously proposed is theoretically founded. Precisely, mimicking the argument in \citet{BDL08} for standard regression/classification and borrowing some of their notations, consistency of ranking rules that are obtained through empirical Kemeny aggregation over a profile of consistent \textit{randomized ranking median regression rules} is investigated. Here, a randomized scoring function is of the form $\mathbf{S}_{\mathcal{D}_n}(., Z)$, where $\mathcal{D}_N=\{(X_1,\Sigma_1),\; \ldots,\; (X_N,\Sigma_N)\}$ is the training dataset and $Z$ is a r.v. taking its values in some measurable space $\mathcal{Z}$ that describes the randomization mechanism. 
A randomized ranking rule $\mathbf{S}_{\mathcal{D}_n}(., Z):\mathcal{X}\rightarrow \mathfrak{S}_N$ is given and consider its ranking median regression risk, $\mathcal{R}(\mathbf{S}_{\mathcal{D}_N}(., Z))$ namely, which is given by:
\begin{equation*}
\mathcal{R}(\mathbf{S}_{\mathcal{D}_N}(., Z))=\sum_{i<j}\mathbb{P}\left\{ \left(\mathbf{S}_{\mathcal{D}_N}(X, Z)(j)-\mathbf{S}_{\mathcal{D}_N}(X, Z)(i)  \right)\left( \Sigma(j)-\Sigma(i)\right)<0   \right\},
\end{equation*}
where the conditional probabilities above are taken over a random pair $(X,\Sigma)$, independent from $\mathcal{D}_N$. It is said to be consistent iff, as $N\rightarrow \infty$,
$$
\mathcal{R}(\mathbf{S}_{\mathcal{D}_N}(., Z))\rightarrow \mathcal{R}^*,
$$
 in probability. When the convergence holds with probability one, one says that the randomized ranking rule is strongly consistent. Fix $B\geq 1$. Given $\mathcal{D}_N$, one can draw $B$ independent copies $Z_1,\; \ldots,\; Z_B$ of $Z$, yielding the ranking rules $\mathbf{S}_{\mathcal{D}_N}(., Z_b)$, $1\leq b\leq B$. Suppose that the ranking rule $\bar{\mathbf{S}}_B(.)$ minimizes 
 \begin{equation}\label{eq:sum_scoring}
 \sum_{b=1}^B\mathbb{E}\left[ d_{\tau}\left(s(X),\mathbf{S}_{\mathcal{D}_N}(X, Z_b)\right)\mid \mathcal{D}_N,\; Z_1,\; \ldots,\; Z_B \right]
 \end{equation}
 over $s\in \mathcal{S}$. The next result shows that, under Assumption \ref{hyp:margin} (strong) consistency is preserved for the ranking rule $\bar{\mathbf{S}}_B(X)$ (and the convergence rate as well, by examining its proof).
 \begin{theorem}\label{thm:consist} ({\sc Consistency and aggregation.})
 Assume that the randomized ranking rule $\mathbf{S}_{\mathcal{D}_N}(., Z)$ is consistent (respectively, strongly consistent) and suppose that Assumption \ref{hyp:margin} is satisfied. Let $B\geq 1$ and, for all $N\geq 1$,  let $\bar{\mathbf{S}}_B(x)$ be a Kemeny median of $B$ independent replications of $\mathbf{S}_{\mathcal{D}_N}(x, Z)$ given $\mathcal{D}_N$. Then, the aggregated ranking rule $\bar{\mathbf{S}}_B(X)$ is consistent (respectively, strongly consistent).
 \end{theorem}
 \begin{proof}
Let $s^*(x)$ be an optimal ranking rule, \textit{i.e} $\mathcal{R}(s^*)=\mathcal{R}^*$. Observe that, with probability one, we have: $\forall b\in\{1,\; \ldots,\; B  \}$,
 \begin{equation*}
 d_{\tau}\left(s^*(X),  \bar{\mathbf{S}}_B(X) \right) \leq  d_{\tau}\left(s^*(X),  \mathbf{S}_{\mathcal{D}_N}(X, Z_b) \right)+  d_{\tau}\left(\mathbf{S}_{\mathcal{D}_N}(X, Z_b),  \bar{\mathbf{S}}_B(X) \right),
 \end{equation*}
 and thus, by averaging,
  \begin{multline*}
  d_{\tau}\left(s^*(X),  \bar{\mathbf{S}}_B(X) \right) \leq \frac{1}{B}\sum_{b=1}^B  d_{\tau}\left(s^*(X),  \mathbf{S}_{\mathcal{D}_N}(X, Z_b) \right)+  \frac{1}{B}\sum_{b=1}^B  d_{\tau}\left(\mathbf{S}_{\mathcal{D}_N}(X, Z_b),  \bar{\mathbf{S}}_B(X) \right)\\
  \leq \frac{2}{B}\sum_{b=1}^B  d_{\tau}\left(s^*(X),  \mathbf{S}_{\mathcal{D}_N}(X, Z_b) \right).
  \end{multline*}
  Taking the expectation w.r.t. to $X$, one gets:
  \begin{multline*}
   \mathcal{R}(\bar{\mathbf{S}}_B)-\mathcal{R}^*\leq \mathbb{E}\left[  d_{\tau}\left(s^*(X),  \bar{\mathbf{S}}_B(X) \right)  \mid \mathcal{D}_N,\; Z_1,\; \ldots,\; Z_B \right]\\
   \leq \frac{1}{B}\sum_{b=1}^B\mathbb{E}\left[ d_{\tau}\left(s^*(X),  \mathbf{S}_{\mathcal{D}_N}(X, Z_b) \right) \mid \mathcal{D}_N,\; Z_1,\; \ldots,\; Z_B \right]
   \leq \frac{1}{BH}\sum_{b=1}^B\left\{\mathcal{R}(\mathbf{S}_{\mathcal{D}_N}(., Z_b)) -\mathcal{R}^* \right\} 
 \end{multline*}
 using \eqref{eq:excess_risk} and Assumption \ref{hyp:margin}. This bound combined with the (strong) consistency assumption yields the desired result.
 \end{proof}

Of course, the quantity \eqref{eq:sum_scoring} is unknown in practice, just like distribution $\mu$, and should be replaced by a statistical version based on an extra sample composed of independent copies of $X$. Statistical guarantees for the minimizer of the empirical variant over a subclass $\mathcal{S}_0\subset \mathcal{S}$ of controlled complexity (\textit{i.e.} under Assumption \ref{hyp:complex}) could be established by adapting the argument of Theorem \ref{thm:consist}'s proof. However, from a practical perspective, aggregating the predictions (at a given point $x\in \mathcal{X}$) rather than the predictors would be much more tractable from a computational perspective (since it would permit to exploit the machinery for approximate Kemeny aggregation of subsection \ref{subsec:best}).% This is investigated Appendix~\ref{chap:esann}.
