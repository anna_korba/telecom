
\section{Preliminaries}\label{sec:background}
As a first go, we start with investigating further (empirical) stochastic transitivity, and introduce the \textit{ranking median regression} problem.

\subsection{Best Strictly Stochastically Transitive Approximation} \label{subsec:best}
\begin{sloppypar}
Let $\mathcal{T}$ be the set of strictly stochastically transitive distributions on $\mathfrak{S}_n$, and consider $P\in \mathcal{T}$. It was proven Chapter~\ref{chap:stat_fram} that under the additional low-noise condition
 on the pairwise marginals of $P$, the empirical distribution $\widehat{P}_N \in \mathcal{T}$ as well with overwhelming probability, and that the expectation of the excess of risk of empirical Kemeny medians decays at an exponential rate. In this case, the nearly optimal solution $\sigma^*_{\widehat{P}_N}$ can be made explicit and straightforwardly computed using Eq. \eqref{eq:copeland_formula}, namely Copeland method, based on the empirical pairwise probabilities
$$
\widehat{p}_{i,j}=\frac{1}{N}\sum_{k=1}^N\mathbb{I}\{ \Sigma_k(i)<\Sigma_k(j)  \}, \; i<j.
$$ 

If the empirical estimation $\widehat{P}_N$ of $P$ does not belong to $\mathcal{T}$, solving the NP-hard problem $\min_{\sigma \in \mathfrak{S}_n}L_{\widehat{P}_N}(\sigma)$ requires to get an empirical Kemeny median. A natural strategy would consist in approximating it by a strictly stochastically transitive probability distribution $\widetilde{P}$ as accurately as possible (in a sense that is specified below) and consider the (unique) Kemeny median of the latter as an approximate median for $\widehat{P}_N$ (for $P$, respectively). It is legitimated by the result below, whose proof is given in the section~\ref{sec:proofs_rank_reg}.
\begin{lemma}\label{lem:bounds}
Let $P'$ and $P^{''}$ be two probability distributions on $\mathfrak{S}_n$. 
\begin{itemize}
\item[(i)] Let $\sigma_{P''}$ be any Kemeny median of distribution $P^{''}$. Then, we have: 
\begin{equation}
L^*_{P'}\leq L_{P'}(\sigma_{P''})\leq L^*_{P'}+2\sum_{i<j}\vert p'_{i,j}-p''_{i,j} \vert ,
\end{equation}
where $p'_{i,j}=\mathbb{P}_{\Sigma \sim P'}\{  \Sigma(i)<\Sigma(j)\}$ and $p''_{i,j}=\mathbb{P}_{\Sigma \sim P''}\{  \Sigma(i)<\Sigma(j)\}$  for any $i<j$.
\item[(ii)] Suppose that $(P',P^{''})\in \mathcal{T}^2$ and set $h=\min_{i<j}\vert p''_{i,j}-1/2\vert$. Then, we have:
\begin{equation}
d_{\tau}(\sigma^*_{P'}, \sigma^*_{P''})\leq (1/h)\sum_{i<j}\vert p'_{i,j}-p''_{i,j} \vert .
\end{equation}
\end{itemize}
\end{lemma}
\end{sloppypar}

%\begin{remark}\label{rk:wassertein}{\sc (Mass transportation)} We point out that the quantity $\sum_{i<j}\vert p'_{i,j}-p''_{i,j} \vert$ involved in the lemma stated above can be interpreted as a Wasserstein metric with Kendall $\tau$ distance as cost function. Indeed, a natural way of measuring the distance between two probability distributions $P$ and $P'$ on $\mathfrak{S}_n$ is to compute the quantity
%\begin{equation} \label{eq:metric}
%D_{\tau}\left(P,P'  \right)=\inf_{\Sigma\sim P,\; \Sigma' \sim P' }\mathbb{E}\left[ d_{\tau}(\Sigma,\Sigma') \right],
%\end{equation}
%where the infimum is taken over all possible couplings\footnote{Recall that a coupling of two probability distributions $Q$ and $Q'$ is a pair $(U,U')$ of random variables defined on the same probability space such that the marginal distributions of $U$ and $U'$ are $Q$ and $Q'$.} $(\Sigma,\Sigma')$ of $(P,P')$, see \textit{e.g.} \citet{CJ10}. As can be shown by means of a straightforward computation (see the section~\ref{sec:proofs_rank_reg} for further details), we have:
%$
%D_{\tau}(P,P' )=\sum_{i<j}\vert p'_{i,j}-p_{i,j} \vert$.
%Observe that, equipped with this notation, we have: $\forall \sigma\in \mathfrak{S}_n$,
%$L_{P}(\sigma)=D_{\tau}\left(P,\delta_{\sigma}  \right)$.
%Hence, the Kemeny medians $\sigma^*$ of a probability distribution $P$ correspond to the Dirac distributions $\delta_{\sigma^*}$ closest to $P$ in the sense of the Wasserstein metric \eqref{eq:metric}. 
%\end{remark}

We go back to the approximate Kemeny aggregation problem and suppose that it is known \textit{a priori} that the underlying probability $P$ belongs to a certain subset $\mathcal{T}'$ of $\mathcal{T}$, on which the quadratic minimization problem
\begin{equation}\label{eq:min_ls}
\min_{P'\in \mathcal{T}'}\sum_{i<j}(p'_{i,j}-\widehat{p}_{i,j})^2
\end{equation}
can be solved efficiently (by orthogonal projection typically, when $\mathcal{T}'$ is a vector space or a convex set, up to an appropriate reparametrization).  Denoting by $\widetilde{P}$ the solution of \eqref{eq:min_ls}, we deduce from Lemma \ref{lem:bounds} combined with Cauchy-Schwarz inequality that
\begin{multline*}
L^*_{\widehat{P}_N}\leq L_{\widehat{P}_N}(\sigma^*_{\widetilde{P}})\leq L^*_{\widehat{P}_N}+\sqrt{2n(n-1)}\left(\sum_{i<j}(\widetilde{p}_{i,j}-\widehat{p}_{i,j})^2\right)^{1/2}\\ \leq
L^*_{\widehat{P}_N}+\sqrt{2n(n-1)}\left(\sum_{i<j}(p_{i,j}-\widehat{p}_{i,j})^2\right)^{1/2},
\end{multline*}
where the final upper bound can be easily shown to be of order $O_{\mathbb{P}}(1/\sqrt{N})$.

In \citet{JLYY11}, the case $$\mathcal{T}'=\{ P':\;\; (p_{i,j}-1/2)+(p_{j,k}-1/2)+(p_{k,i}-1/2)=0 \text{ for all 3-tuple } (i,j,k)\}\subset \mathcal{T}$$ has been investigated at length in particular. Indeed, it is shown there that the Borda count corresponds to the least squares projection of the pairwise probabilities onto this space, namely the space of gradient flows. In practice, when $\widehat{P}_N$ does not belong to $\mathcal{T}$, we thus propose to consider as a pseudo-empirical median any permutation $\widetilde{\sigma}^*_{\widehat{P}_N}$ that ranks the objects as the empirical Borda count:
$$ 
\left( \sum_{k=1}^N\Sigma_k(i)-\sum_{k=1}^N\Sigma_k(j)\right)\cdot \left( \widetilde{\sigma}^*_{\widehat{P}_N}(i)-\widetilde{\sigma}^*_{\widehat{P}_N}(j)   \right)>0 \text{ for all } i<j \text{ s.t. } \sum_{k=1}^N\Sigma_k(i) \ne \sum_{k=1}^N\Sigma_k(j) ,
$$
breaking possible ties in an arbitrary fashion.

%Let $p''=(p^{''}_{i,j})_{i<j}$ the best collection of pairwise probabilities verifying the strict stochastic transitiviy condition, approximating  $(p'_{i,j})_{i<j}$ in the least squares sense, \textit{i.e.} such that $\sum_{i<j}(p'_{i,j}-p^{''}_{i,j})^2$ is minimum. Denoting by $\widetilde{\sigma}^*_{P'}$ the permutation defined by the $p^{''}_{i,j}$'s (namely, $\widetilde{\sigma}^*_{P'}(i)=1+\sum_{k\neq i}\mathbb{I}\{p^{''}_{i,k}<1/2  \}$ for all $i\in \n$), we deduce from Lemma \ref{lem:bounds} combined with Cauchy-Schwarz inequality that:
%\begin{equation}\label{eq:bound_DAG}
%L^*_{P'}\leq L_{P'}(\widetilde{\sigma}^*_{P'})\leq L^*_{P'}+\sqrt{2n(n-1)}\min_{p'':\; G_{p''} \text{  acyclic}}\left(\sum_{i<j}(p'_{i,j}-p^{''}_{i,j})^2\right)^{1/2}.
%\end{equation}
%When $\widehat{P}\notin \mathcal{T}$, the bound above advocates for using the permutation $\widetilde{\sigma}^*_{\widehat{P}}$ as approximate median of the underlying distribution $P$. The following theorem reveals that its excess of risk is of order $O_{\mathbb{P}}(1/\sqrt{N})$. Refer to the Appendix for the technical proof.
%\begin{theorem}\label{thm:approx_med}
%Suppose that $P\in \mathcal{T}$. Then, for all $\delta\in (0,1)$, we have with probability at least $1-\delta$: $\forall N\geq 1$,
%\begin{equation}
%L_{P}(\widetilde{\sigma}^*_{\widehat{P}})-L^*_{P}\leq \left(  n(n-1)\right)^{3/2}\sqrt{\frac{\log(n(n-1)/\delta)}{N}}
%\end{equation}
%\end{theorem}
%\noindent As shall be seen in Section~\ref{sec:main}, this regression approach to ranking aggregation shall play a crucial role in the algorithms we propose for ranking median regression (in order to compute approximate local medians more precisely), which statistical learning problem is formulated in what follows as an extension of consensus ranking. We now discuss how to compute $\widetilde{\sigma}^*_{\widehat{P}}$, given $\widehat{P}$, in practice.\\
%\noindent The issue of finding a sharp approximation of a probability distribution $P'$ by a strictly stochastically transitive distribution comes back to the problem of approximating a weighted, complete graph by a weighted, complete \textit{acyclic} graph. Indeed, any probability distribution $P''$ in $\mathcal{T}$ defines a complete directed acyclic graph (DAG) whose vertices are the objects $1,\; \ldots,\; n$ and the directed edges are defined by: $\forall i\neq j$,
%\begin{equation}\label{eq:DAG}
%i \rightarrow j \Leftrightarrow p''_{i,j}>1/2.
%\end{equation}
%The problem of finding the best collection of pairwise probabilities $p''=(p^{''}_{i,j})_{i<j}$ defining  a DAG $G_{p''}$ (see \eqref{eq:DAG}) approximating $(p'_{i,j})_{i<j}$ in the least squares sense has been adressed in \citet{JLYY10} by means of the Hodge combinatorial theory recalled in Appendix.
%is connected with the \textit{feedback set} problem for directed graphs, see \citet{DETT99} (to complete). A feedback set (FS) is a set of edges whose removal makes the directed graph acyclic. Whereas it is difficult in general to find a FS with specific properties (\textit{e.g.} a minimum cardinality FS).\\
%As shown in section 5 therein, the computation of the solution boils down to solving a $n\times n$ least-squares problem (with cost $O(n^3)$). 

\subsection{Predictive Ranking and Statistical Conditional Models}\label{subsec:cdt_models}

We suppose now that, in addition to the ranking $\Sigma$, one observes a random vector $X$, defined on the same probability space $(\Omega,\; \mathcal{F},\; \mathbb{P})$, valued in a feature space $\mathcal{X}$ (of possibly high dimension, typically a subset of $\mathbb{R}^d$ with $d\geq 1$) and modelling some information hopefully useful to predict $\Sigma$ (or at least to recover some of its characteristics). The joint distribution of the r.v. $(\Sigma,\; X)$ is described by $(\mu,\; P_X)$, where $\mu$ denotes $X$'s marginal distribution and $P_X$ means the conditional probability distribution of $\Sigma$ given $X$: $\forall \sigma\in \mathfrak{S}_n$,
$P_X(\sigma)=\mathbb{P}\{ \Sigma=\sigma \mid X\}$ almost-surely.
The marginal distribution of $\Sigma$ is then $P(\sigma)=\int_{\mathcal{X}}P_{x}(\sigma)\mu(x)$. Whereas ranking aggregation methods applied to the $\Sigma_i$'s would ignore the information carried by the $X_i$'s for prediction purpose, our goal is to learn a predictive function $s$ that maps any point $X$ in the input space to a permutation $s(X)$ in $\mathfrak{S}_n$. This problem can be seen as a generalization of multiclass classification and has been referred to as \textit{label ranking} in \citet{tsoumakas2009mining} and \citet{vembu2010label} for instance. Some approaches are rule-based (see \citet{gurrieri2012label}), while certain others adapt classic algorithms such as those investigated in section \ref{sec:main} to this problem (see \citet{YWL10}), but most of the methods documented in the literature rely on parametric modeling (see \citet{cheng2009new}, \citet{cheng2009decision}, \citet{cheng2010label}). In parallel,  several authors proposed to model explicitly the dependence of the parameter $\theta$ w.r.t. the covariate $X$ and rely next on MLE or Bayesian techniques to compute a predictive rule. One may refer to \citet{rendle2009bpr} or \citet{lu2015individualized}. In contrast, the approach we develop in the next section aims at formulating the ranking regression problem, free of any parametric assumptions, in a general statistical framework. In particular, we show that it can be viewed as an extension of statistical ranking aggregation. 

%This problem can be seen as a specific form of multilabel classification (see \citet{tsoumakas2009mining}). These methods have many real-world applications, such as tagging messages, categorizing items of a catalog or annotating images. Recent works considered more structured output prediction problems, assuming that each class label is presented with an ordered scale for instance, see \textit{e.g.} \citet{brinker2014graded} or \citet{cheng2010graded}. However, to the best of our knowledge, in none of these nonparametric methods, the output vector is required to be a permutation of a set of objects. 
%as classical approaches address the ranking aggregation problem by stipulating a rigid 'parametric' model $P_{\theta}$ for the distribution on $\mathfrak{S}_n$ (see \textit{e.g.} \citet{Mallows57} or \citet{fligner1986distance}),

