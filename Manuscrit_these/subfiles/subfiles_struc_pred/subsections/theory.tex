
\subsection{Theoretical Guarantees}
In this section, we give some statistical guarantees for the estimators obtained by following the steps described in section~\ref{sec:structured_prediction}. To this end, we build upon recent results in the framework of
Surrogate Least Square by \cite{ciliberto2016consistent}. Consider one of the embeddings $\phi$ on permutations presented in the previous section, which defines a loss $\Delta$ as in Eq. \eqref{loss_embedding}. Let $c_{\phi}=\max_{\sigma \in \Sn}\| \phi(\sigma)\|$. We will denote by $s^*$ a minimizer of the true risk \eqref{eq:risk_theo}, $g^*$ a minimizer of the surrogate risk \eqref{eq:surrogate_pb}, and $d$ a decoding function as~\eqref{eq:decoding_function}\footnote{Note that $d=\phi_L^{-1} \circ d_L$ for $\phi_L$ and is obtained as the composition of two steps for $\phi_{\tau}$ and $\phi_H$: solving an optimization problem and compute the inverse of the embedding.}. Given an estimator $\widehat{g}$ of $g^*$ from Step~1, i.e. a minimizer of the empirical surrogate risk~\eqref{eq:emp_surrogate_pb} we can then consider in Step~2 an estimator $\widehat{s}=d\circ \widehat{g}$. The following theorem reveals how the performance of the estimator $\widehat{s}$ we propose can be related to a solution $s^*$ of~\eqref{eq:risk_theo} for the considered embeddings.
\begin{theorem}\label{theo:consistency} The excess risks of the proposed predictors are linked to the excess surrogate risks as:
	\vspace{-\topsep}
\begin{itemize}%[noitemsep,topsep=0pt]
	\itemsep0em	%\setlength\itemsep{0.5em}
	\item[(i)] For the loss \eqref{loss_embedding} defined by the Kemeny and Hamming embedding $\phi_{\tau}$ and $\phi_H$ respectively:
	\begin{equation*}
	 \mathcal{R}(d \circ  \widehat{g}) - \mathcal{R}(s^*) \le c_{\phi}\sqrt{\mathcal{L}(\widehat{g}) - \mathcal{L}(g^*)}
	 \end{equation*}
	 with $c_{\phi_{\tau}}= \sqrt{\frac{n(n-1)}{2}}$ and $c_{\phi_{H}}=\sqrt{n}$.
	\item [(ii)] For the loss  \eqref{loss_embedding} defined by the Lehmer embedding $\phi_L$:
	\begin{equation*}
	\mathcal{R}(d \circ  \widehat{g}) - \mathcal{R}(s^*) \le \sqrt{\frac{n(n-1)}{2}} \sqrt{\mathcal{L}(\widehat{g}) - \mathcal{L}(g^*)} + \mathcal{R}(d \circ  g^*) - \mathcal{R}(s^*) + \mathcal{O}(n \sqrt{n})
	\end{equation*}
\end{itemize}
\vspace{-\topsep}
\end{theorem}
The full proof is given section~\ref{sec:lehmer_proof}. Assertion (i) is a direct application of Theorem~2 in \cite{ciliberto2016consistent}. In particular, it comes from a preliminary consistency result which shows that $\mathcal{R}(d \circ  g^*)=\mathcal{R}(s^*)$ for both embeddings. Concerning the Lehmer embedding, it is not possible to apply their consistency results immediately; however a large part of the arguments of their proof is used to bound the estimation error for the surrogate risk, and we remain with an approximation error  $\mathcal{R}(d \circ  g^*) - \mathcal{R}(s^*) + \mathcal{O}(n \sqrt{n})$ resulting in Assertion (ii). In Remark~\ref{rk:consistency_lehmer} in section~\ref{sec:lehmer_proof}, we give several insights about this approximation error. Firstly we show that it can be upper bounded by $2 \sqrt{2}\sqrt{n(n-1)} \mathcal{R}(s^*)+ \mathcal{O}(n \sqrt{n})$. Then, we explain how this term results from using $\phi_L$ in the learning procedure. The Lehmer embedding thus have weaker statistical guarantees, but has the advantage of being more computationnally efficient, as we explain in the next subsection.

Notice that for Step 1, one can choose a consistent regressor with vector values $\widehat{g}$, i.e such that $\mathcal{L}(\widehat{g})\rightarrow \mathcal{L}(g^*)$ when the number of training points tends to infinity. Examples of such methods that we use in our experiments to learn $\widehat{g}$, are the k-nearest neighbors (kNN) or kernel ridge regression  ~\citep{micchelli2005learning} methods whose consistency have been proved (see Chapter~5 in \cite{DGL96} and \cite{caponnetto2007optimal}). In this case the control of the excess of the surrogate risk $\mathcal{L}(\widehat{g}) - \mathcal{L}(g^*)$ implies the control of $\mathcal{R}(\widehat{s}) - \mathcal{R}(s^*)$ where $\widehat{s}=d \circ \widehat{g}$ by Theorem~\ref{theo:consistency}. 

\begin{remark}
	%\al{Prediction of incomplete/partial rankings -> loss of consistency results (non-normalized kernel, ie the norm of the embedding wouldn't be constant anymore) $\rightarrow$ 
	We clarify that the consistency results of Theorem~1 are established for the task of predicting full rankings which is adressed in this paper.
	In the case of predicting partial or incomplete rankings, these results are not guaranteed to hold. Providing theoretical guarantees for this task is left for future work. %Indeed, it seems tricky to find a loss on partial rankings which can be written as in Eq. \eqref{loss_embedding} and which could satisfy 
	
	%For instance, it can be seen that the embedding (\ref{eq:kemeny_partial}) is not of constant norm for partial rankings, while this is a crucial hypothesis to write the loss as a scalar product (see Assumption 1 in the Supplementary) used to derive the bound. However, a similar proof 
	
	 %a similar proof (as for the Lehmer embedding which is also of non-constant norm) and one remains with a similar approximation error.
	 %Providing theoretical guarantees for such embeddings is left for future work.%} 
\end{remark}
%What happens for partial rankings is that Assumption 1 is not verified anymore since the kernel is not normalized (i.e., the norm of the embedding of any partial ranking on $n$ items is not constant). However, we can derive a similar proof (as for the Lehmer embedding which is also of non-constant norm) and one remains with a similar approximation error.