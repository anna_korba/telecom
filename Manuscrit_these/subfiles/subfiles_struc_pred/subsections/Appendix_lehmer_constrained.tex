\subsection{Initial proposal for the Lehmer code - Dual optimization problem}

We propose to solve problem \eqref{eq:least_squares} with the additional constraint that $h$ takes its values in $\widetilde{\mathcal{C}}_n \delequal \{0\} \times [0,1] \times \dots \times [0,n-1]$, where for each $a,b$, $[a,b]$ is the real interval between $a$ and $b$. If $h_j(x)$ denotes the $j$-th coordinate of the vector $h(x)$, the constraint $h(x) \in \widetilde{\mathcal{C}}_n$ for any $x$ can be rewritten as a constraint on each interval, i.e: $ 0 \le h_j(x) \le j$ for each $j$. If $(e_1, \dots, e_n)$ denotes the canonical base of $\mathbb{R}^n$/$\mathcal{F}_{\mathcal{Y}}$, this condition can be rewritten:
\begin{equation*}
\forall k \in \{1,\dots, N \}, j \in \{ 1, \dots, n\}, 0 \le \langle h(x_k),e_j\rangle \le j
\end{equation*}
In this case, the solution of equation~\eqref{eq:inverse_image} is trivial, it is the vector obtained by rounding each coordinate of $\widehat{h}(x)$. We now explain our proposal for the two steps formally.

\noindent \textbf{Step 1}. The first regression step can thus be written:
\begin{align}\label{eq:ls_constrained}
&\min_{h \in \mathcal{H}_K} \sum_{k=1}^{N} \|h(x_k)- \phi(\sigma_k)\|^2 + \lambda \| h \|^2_{\mathcal{H}_K}\\
& s.c. \enspace \forall k \in \{1,\dots, N \}, j \in \{ 1, \dots, n\}, \enspace 0 \le \langle h(x_k),e_j\rangle \le j%\\
%& \text{ and } m_{i,j} \in \{ 0, 1\}
\end{align}
The dual solutions of this problem satisfy the following equations:
\begin{equation}
2 \lambda \widetilde{y}_k = \sum_{l=1}^{N}( K(x_k, x_l)+ \lambda \delta_{k,l})\alpha_l -\lambda\sum_{j=1}^{n}j e_j \text{ for } k=1, \dots, N
\end{equation}
where $\delta_{k,l}$ is the Kronecker product. The solution of~\eqref{eq:ls_constrained} thus has the following closed-form solution:
\begin{equation}
\widehat{h}(.)= \psi_x(.) (K_x + \lambda I_N)^{-1} (Y_N +J)
\end{equation} 

\noindent \textbf{Step 2.} We want to solve:
\begin{equation}\label{eq:inverse_image2}
\widehat{s}(x)=\argmin_{\sigma \in \Sn} \| \phi(\sigma) - \widehat{h}(x)\|^2
\end{equation}
Given that $\widehat{h}(x) \in \widetilde{C}_n$ for any $x$, the solution of problem \eqref{eq:inverse_image2} is trivial and is:
\begin{equation}
\widehat{s}(x)=d \circ \widehat{h}(x)\in \mathcal{C}_n
\end{equation}
where $d$ is the rounding function:
\begin{align*}
d \colon \mathbb{R}^n &\to \mathbb{Z}^n\\
z=(z_i)_{i=1, \dots, n} &\mapsto ([z_i])_{i=1, \dots, n}.
\end{align*}


\textbf{Proof.} The first regression step can thus be written:
\begin{align*}
&\min_{h \in \mathcal{H}_K} \sum_{k=1}^{N} \|h(x_k)- \phi(\sigma_k)\|^2 + \lambda \| h \|^2_{\mathcal{H}_K}\\
& s.c. \enspace \forall k \in \{1,\dots, N \}, j \in \{ 1, \dots, n\}, \enspace 0 \le \langle h(x_k),e_j\rangle \le j%\\
%& \text{ and } m_{i,j} \in \{ 0, 1\}
\end{align*}
Let $\widetilde{y}_k=\phi(\sigma_k)$. This problem can be rewritten by introducing the constraint $u_k = h(x_k)$ and the function $L_k: \mathcal{F}_{\mathcal{Y}}  \rightarrow \mathbb{R}$ defined as $L_k(u_k) = \|u_k- \widetilde{y}_k\|^2$ for $k \in \{1, \dots, N\}$:
\begin{align*}
&\min_{h \in \mathcal{H}_K, \{u_k \in \mathcal{F}_{\mathcal{Y}}\}_{k=1, \dots, N}} \sum_{k=1}^{N}L_k(u_k) + \lambda \| h \|^2_{\mathcal{H}_K}\\
& s.c. \enspace \forall k \in \{1,\dots, N \},  \begin{cases}
u_k=h(x_k): \alpha_k\\
\forall j \in \{1, \dots, n\}, 0 \le \langle u_k, e_j \rangle \le j :\beta_{k,j}, \gamma_{k,j}
\end{cases}
%j \in \{ 1, \dots, n\}, \enspace 0 \le \langle h(x_k),e_j\rangle \le j%\\
%& \text{ and } m_{i,j} \in \{ 0, 1\}
\end{align*}
The Lagrangian can be written as:
\begin{multline*}
\mathcal{L}(h,u_k,\alpha_k, (\beta_{k,j})_{j=1, \dots, n}, (\gamma_{k,j})_{j=1, \dots, n})=\sum_{k=1}^{N}L_k(u_k) + \lambda \| h \|^2_{\mathcal{H}_K} +\sum_{k=1}^{N}\langle \alpha_k, u_k - h(x_k)\rangle_{\mathcal{F}_{\mathcal{Y}}}\\
- \sum_{k=1}^{N}\sum_{j=1}^{n}\beta_{k,j}  \langle u_k, e_j \rangle_{\mathcal{F}_{\mathcal{Y}}} -\sum_{k=1}^{N}\sum_{j=1}^{n} \gamma_{k,j}(j- \langle u_k, e_j \rangle_{\mathcal{F}_{\mathcal{Y}}})\\
= \sum_{k=1}^{N}L_k(u_k) + \lambda \| h \|^2_{\mathcal{H}_K} +\sum_{k=1}^{N}\langle \alpha_k, u_k \rangle_{\mathcal{F}_{\mathcal{Y}}} - \sum_{k=1}^{N}\langle K(.,x_k)\alpha_k, h\rangle_{\mathcal{H}}
+ \sum_{k=1}^{N}\sum_{j=1}^{n}(\gamma_{k,j} - \beta_{k,j} ) \langle u_k, e_j \rangle_{\mathcal{F}_{\mathcal{Y}}} -\sum_{k=1}^{N}\sum_{j=1}^{n} \gamma_{k,j} j\\
= \sum_{k=1}^{N}L_k(u_k) + \lambda \| h \|^2_{\mathcal{H}_K} +\sum_{k=1}^{N}\langle \alpha_k, u_k \rangle_{\mathcal{F}_{\mathcal{Y}}} - \sum_{k=1}^{N}\langle K(.,x_k)\alpha_k, h\rangle_{\mathcal{H}}
+ \sum_{k=1}^{N}\langle u_k, \gamma_k - \beta_k \rangle_{\mathcal{F}_{\mathcal{Y}}} -\sum_{k=1}^{N}\langle \gamma_{k}, \sum_{j=1}^{n} j e_j \rangle_{\mathcal{F}_{\mathcal{Y}}}\\
\end{multline*}
where $\beta_k=\sum_{j=1}^{n}\beta_{k,j} \in \mathcal{F}_{\mathcal{Y}}$ and $\gamma_k=\sum_{j=1}^{n}\gamma_{k,j} \in \mathcal{F}_{\mathcal{Y}}$.
Then the dual function can be written:
\begin{align*}
g(\alpha,\beta, \gamma )&= \inf_{h \in \mathcal{H},\{u_k \in \mathcal{F}_{\mathcal{Y}}\}_{k=1, \dots, N} } \mathcal{L}(h,u_k,\alpha_k, \beta_k, \gamma_k)\\
&= \sum_{k=1}^{N} \inf_{\{u_k \in \mathcal{F}_{\mathcal{Y}}\}_{k=1, \dots, N}}\left(L_k(u_k) + \langle \alpha_k, u_k \rangle_{\mathcal{F}_{\mathcal{Y}}} +  \langle u_k, \gamma_k - \beta_k \rangle_{\mathcal{F}_{\mathcal{Y}}}\right)  \\
& + \inf_{h \in \mathcal{H}} \left( \lambda \| h \|^2_{\mathcal{H}_K} - \sum_{k=1}^{N}\langle K(.,x_k)\alpha_k, h\rangle_{\mathcal{H}}\right) -\sum_{k=1}^{N}\langle \gamma_{k}, \sum_{j=1}^{n} j e_j \rangle_{\mathcal{F}_{\mathcal{Y}}}\\
&= \sum_{k=1}^{N} \inf_{\{u_k \in \mathcal{F}_{\mathcal{Y}}\}_{k=1, \dots, N}}\left(L_k(u_k) + \langle \alpha_k + (\gamma_k -\beta_k), u_k \rangle_{\mathcal{F}_{\mathcal{Y}}}\right) \\
& - \frac{1}{4 \lambda} \sum_{k,l=1}^{N} \langle \alpha_k, K(x_k, x_l) \alpha_l \rangle_{\mathcal{F}_{\mathcal{Y}}} -\sum_{k=1}^{N}\langle \gamma_{k}, \sum_{j=1}^{n} j e_j \rangle_{\mathcal{F}_{\mathcal{Y}}}\\
&=  - \sum_{k=1}^{N} \sup_{\{u_k \in \mathcal{F}_{\mathcal{Y}}\}_{k=1, \dots, N}}\left( - L_k(u_k) + \langle  -\alpha_k -(\gamma_k - \beta_k), u_k \rangle_{\mathcal{F}_{\mathcal{Y}}}\right) \\
& - \frac{1}{4 \lambda} \sum_{k,l=1}^{N} \langle \alpha_k, K(x_k, x_l) \alpha_l \rangle_{\mathcal{F}_{\mathcal{Y}}} -\sum_{k=1}^{N}\langle \gamma_{k}, \sum_{j=1}^{n} j e_j \rangle_{\mathcal{F}_{\mathcal{Y}}}\\
&= - \sum_{k=1}^{N} L_k^*(-\alpha_k -(\gamma_k - \beta_k)) - \frac{1}{4 \lambda} \sum_{k,l=1}^{N} \langle \alpha_k, K(x_k, x_l) \alpha_l \rangle_{\mathcal{F}_{\mathcal{Y}}} -\sum_{k=1}^{N}\langle \gamma_{k}, \sum_{j=1}^{n} j e_j \rangle_{\mathcal{F}_{\mathcal{Y}}}
\end{align*}
where $L_k^*$ denotes the Fenchel conjugate of the function $L_k$.
The dual optimization problem thus writes as follows:
\begin{equation*}
\max_{\{\alpha_k, \beta_{k}, \gamma_{k} \in \mathcal{F}_{\mathcal{Y}}\}} - \sum_{k=1}^{N} L_k^*(-\alpha_k -(\gamma_k - \beta_k)) - \frac{1}{4 \lambda} \sum_{k,l=1}^{N} \langle \alpha_k, K(x_k, x_l) \alpha_l \rangle_{\mathcal{F}_{\mathcal{Y}}} -\sum_{k=1}^{N}\langle \gamma_{k}, \sum_{j=1}^{n} j e_j \rangle_{\mathcal{F}_{\mathcal{Y}}}
\end{equation*}
That we can rewrite, since $L_k^*(-\eta_k)= \frac{1}{4}\| \eta_k \|_{\mathcal{F}_{\mathcal{Y}}}^2 - \langle \eta_k, \widetilde{y}_k \rangle_{\mathcal{F}_{\mathcal{Y}}}$:
\begin{multline*}
\max_{\{\alpha_k, \beta_{k}, \gamma_{k}\in \mathcal{F}_{\mathcal{Y}}\}} - \sum_{k=1}^{N} \frac{1}{4}\| \alpha_k +(\gamma_k - \beta_k) \|_{\mathcal{F}_{\mathcal{Y}}}^2
+ \sum_{k=1}^{N} \langle \alpha_k +(\gamma_k - \beta_k), \widetilde{y}_k \rangle_{\mathcal{F}_{\mathcal{Y}}}\\
- \frac{1}{4 \lambda} \sum_{k,l=1}^{N} \langle \alpha_k, K(x_k, x_l) \alpha_l \rangle_{\mathcal{F}_{\mathcal{Y}}}  -\sum_{k=1}^{N}\langle \gamma_{k}, \sum_{j=1}^{n} j e_j \rangle_{\mathcal{F}_{\mathcal{Y}}}
\end{multline*}
Or:
\begin{multline*}
\max_{\{\alpha_k, \beta_{k,j}, \gamma_{k,j}\in \mathcal{F}_{\mathcal{Y}}\}} - \sum_{k=1}^{N} \frac{1}{4}\left( \| \alpha_k\|_{\mathcal{F}_{\mathcal{Y}}}^2+ \|\gamma_k \|_{\mathcal{F}_{\mathcal{Y}}}^2 + \|\beta_k \|_{\mathcal{F}_{\mathcal{Y}}}^2 - 2 \langle \gamma_k,\beta_k\rangle_{\mathcal{F}_{\mathcal{Y}}} + 2 \langle \alpha_k,(\gamma_k - \beta_k)\rangle_{\mathcal{F}_{\mathcal{Y}}} \right)\\
+ \sum_{k=1}^{N} \langle \alpha_k +(\gamma_k - \beta_k), \widetilde{y}_k \rangle_{\mathcal{F}_{\mathcal{Y}}}
- \frac{1}{4 \lambda} \sum_{k,l=1}^{N} \langle \alpha_k, K(x_k, x_l) \alpha_l \rangle_{\mathcal{F}_{\mathcal{Y}}}  -\sum_{k=1}^{N}\langle \gamma_{k}, \sum_{j=1}^{n} j e_j \rangle_{\mathcal{F}_{\mathcal{Y}}}
\end{multline*}

We derive with respect to the $\alpha_k, \beta_{k}, \gamma_{k}$ and find that:
\begin{align}\label{eq:grad_gamma}
\nabla_{\gamma_{k}} g(\dots)= - \frac{1}{4}(2 \gamma_{k} - 2 \beta_k +2 \alpha_k) + \widetilde{y}_k - \sum_{j=1}^{n} j e_j  =0 \Longleftrightarrow 4 \widetilde{y}_k=4\sum_{j=1}^{n} j e_j +2(\alpha_k+\gamma_k -\beta_k)
\end{align}
\begin{align}\label{eq:grad_beta}
\nabla_{\beta_{k}} g(\dots)= - \frac{1}{4}( -2 \gamma_k + 2 \beta_{k} - 2 \alpha_k) - \widetilde{y}_k =0 \Longleftrightarrow 4 \widetilde{y}_k=2 (\alpha_k +\gamma_k - \beta_{k})
\end{align}
\begin{align}\label{eq:grad_alpha}
\nabla_{\alpha_{k}} g(\dots)= - \frac{1}{4}(2 \alpha_k + 2 (\gamma_k-\beta_k) + \widetilde{y}_k - \frac{1}{2\lambda} \sum_{l=1}^{N}K(x_k, x_l) \alpha_l=0
\end{align}
Equations~\eqref{eq:grad_gamma} and~\eqref{eq:grad_beta} give us:
\begin{equation}
(\beta_{k}-\gamma_{k})=2 \sum_{j=1}^{n}j e_j
\end{equation}
So~\eqref{eq:grad_alpha} can be rewritten:
\begin{equation}
2 \lambda \widetilde{y}_k = \sum_{l=1}^{N}( K(x_k, x_l)+ \lambda \delta_{k,l})\alpha_l -\lambda\sum_{j=1}^{n}j e_j \text{ for } k=1, \dots, N
\end{equation}
where $\delta_{k,l}$ is the Kronecker product.