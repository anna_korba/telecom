

\subsection{Proof of Theorem 1}\label{sec:lehmer_proof}

We borrow the notations of \cite{ciliberto2016consistent} and recall their main result Theorem~\ref{th:ciliberto}. They firstly exhibit the following assumption for a given loss $\Delta$, see Assumption~1 therein:


\noindent \textbf{Assumption 1.} There exists a separable Hilbert space $\mathcal{F}$ with inner product $\langle ., . \rangle_{\mathcal{F}}$, a
continuous embedding  $\psi: \mathcal{Y} \rightarrow \mathcal{F}$ and a bounded linear operator $V : \mathcal{F} \rightarrow \mathcal{F}$, such that:
\begin{equation}
\Delta(y,y')=\langle \psi(y), V \psi(y') \rangle_{\mathcal{F}} \quad \forall y,y' \in \mathcal{Y}
\end{equation}

\begin{theorem}\label{th:ciliberto}
	Let $\Delta: \mathcal{Y} \rightarrow \mathcal{Y}$ satisfying Assumption~1 with $\mathcal{Y}$ a compact set. Then, for every measurable $g:\mathcal{X}\rightarrow \mathcal{F}$ and $d:\mathcal{F}\rightarrow \mathcal{Y}$ such that $\forall h \in \mathcal{F}$, $d(h)=\argmin_{y \in \mathcal{Y}} \langle \phi(y), h \rangle_{\mathcal{F}}$, the following holds:
	\begin{itemize}
		\setlength\itemsep{0.5em}
		\item[(i)]Fisher Consistency: $ \mathcal{E}(d\circ g^*) = \mathcal{E}(s^*)$
		\item [(ii)] Comparison Inequality: $ \mathcal{E}(d\circ g) - \mathcal{E}(s^*) \le 2 c_{\Delta}\sqrt{\mathcal{R}(g)- \mathcal{R}(g^*)}$
	\end{itemize}
with $c_{\Delta}=\|V\| \max_{y \in \mathcal{Y}}\|\phi(y)\|$.
\end{theorem}
Notice that any discrete set $\mathcal{Y}$ is compact and $\phi:\mathcal{Y}\rightarrow \mathcal{F}$ is continuous. We now prove the two assertions of Theorem~\ref{theo:consistency}.\\

\noindent \textit{Proof of Assertion(i) in Theorem~\ref{theo:consistency}}. Firstly, $\mathcal{Y}=\Sn$ is finite. Then, for the Kemeny and Hamming embeddings, $\Delta$ satisfies Assumption~1 with $V= -id$ (where $id$ denotes the identity operator) , and $\psi=\phi_K$ and $\psi=\phi_H$ respectively. Theorem~\ref{th:ciliberto} thus applies directly.

\noindent \textit{Proof of Assertion(ii) in Theorem~\ref{theo:consistency}}. In the following proof, $\mathcal{Y}$ denotes $\Sn$, $\phi$ denotes $\phi_L$ and $d=\phi_{L}^{-1} \circ d_L$ with $d_L$ as defined in~\eqref{eq:decoding_lehmer}. Our goal is to control the excess risk $\mathcal{E}(s)-\mathcal{E}(s^*)$.
\begin{align*}
\mathcal{E}(s)-\mathcal{E}(s^*)&= \mathcal{E}(d \circ \widehat{g}) - \mathcal{E}(s^*)\\
& = \underbrace{\mathcal{E}(d \circ \widehat{g}) - \mathcal{E}(d \circ g^*)}_{(A)} +  \underbrace{\mathcal{E}(d \circ g^*) - \mathcal{E}(s^*)}_{(B)}
\end{align*}
Consider the first term (A). 
\begin{align*}
\mathcal{E}(d \circ \widehat{g}) - \mathcal{E}(d \circ g^*)&= \int_{\mathcal{X}\times\mathcal{Y}} \Delta(d \circ \widehat{g}(x), \sigma) - \Delta(d \circ g^*(x), \sigma) dP(x,\sigma)\\
&= \int_{\mathcal{X}\times\mathcal{Y}} \| \phi(d\circ \widehat{g}(x)) - \phi(\sigma)\|^2_{\mathcal{F}}  - \| \phi(d\circ g^* (x)) - \phi(\sigma)\|^2_{\mathcal{F}}dP(x,\sigma)\\
%&= \int_{\mathcal{X}\times\mathcal{Y}} \| \phi(d\circ \widehat{g}(x)) \|^2_{\mathcal{F}} - \|  \phi(d\circ g^* (x))\|^2_{\mathcal{F}} + 2 \langle  \phi(d\circ g^* (x)) - \phi(d\circ \widehat{g}(x)), \phi(\sigma) \rangle dP(\sigma, x)\\
&= \underbrace{\int_{\mathcal{X}} \| \phi(d\circ \widehat{g}(x)) \|^2_{\mathcal{F}} - \|  \phi(d\circ g^* (x))\|^2_{\mathcal{F}} dP(x)}_{(A1)} +\\
& \quad \quad \quad \underbrace{2 \int_{\mathcal{X}} \langle  \phi(d\circ g^* (x)) - \phi(d\circ \widehat{g}(x)), \int_{\mathcal{Y}}\phi(\sigma)dP(\sigma, x) \rangle dP(x)}_{(A2)}
\end{align*}
The first term (A1) can be upper bounded as follows:
\begin{align*}
\int_{\mathcal{X}} \| \phi(d\circ \widehat{g}(x)) \|^2_{\mathcal{F}} - \|  \phi(d\circ g^* (x))\|^2_{\mathcal{F}} dP(x) & \le \int_{\mathcal{X}} \langle \phi(d\circ \widehat{g}(x)) - \phi(d\circ g^* (x)), \phi(d\circ \widehat{g}(x)) + \phi(d\circ g^* (x)) \rangle_{\mathcal{F}} dP(x)\\
&\le  2 c_{\Delta} \int_{\mathcal{X}} \| \phi(d\circ \widehat{g}(x)) - \phi(d\circ g^* (x)) \|_{\mathcal{F}} dP(x)\\
&\le  2 c_{\Delta} \sqrt{ \int_{\mathcal{X}} \| d_L(\widehat{g}(x)) - d_L(g^* (x)) \|^2_{\mathcal{F}} dP(x)}\\
&\le  2 c_{\Delta} \sqrt{ \int_{\mathcal{X}} \| g^*(x)- \widehat{g}(x)\|^2_{\mathcal{F}}dP(x)} + \mathcal{O}(n \sqrt{n})\\
%& \le 2 c_{\Delta} \| \phi \circ d \| \int_{\mathcal{X}} \| g^*(x)- \widehat{g}(x)\|^2_{\mathcal{F}}dP(x)
\end{align*}
with $c_{\Delta}=\max_{\sigma \in \mathcal{Y}} \| \phi(\sigma)\|_{\mathcal{F}}=\sqrt{\frac{(n-1)(n-2)}{2}}$ and since $\| d_L(u) - d_L(v) \| \le \| u - v\| + \sqrt{n}$. Since  $\int_{\mathcal{X}} \| g^*(x)- \widehat{g}(x)\|^2_{\mathcal{F}}dP(x) = \mathcal{R}(\widehat{g}) - \mathcal{R}(g^*)$ (see \cite{ciliberto2016consistent}) we get the first term of Assertion (i).
For the second term (A2), we can actually follow the proof of Theorem 12 in~\cite{ciliberto2016consistent} and we get:
\begin{equation*}
\int_{\mathcal{X}} \langle  \phi(d\circ g^* (x)) - \phi(d\circ \widehat{g}(x)), \int_{\mathcal{Y}}\phi(\sigma)dP(\sigma, x) \rangle dP(x) \le 2 c_{\Delta} \sqrt{\mathcal{R}(\widehat{g})- \mathcal{R}(g^*)}
\end{equation*}
Consider the second term (2). By Lemma 8 in \citep{ciliberto2016consistent}, we have that:
\begin{equation}\label{eq:minimizer_surrogate}
g^*(x)=\int_{\mathcal{Y}} \phi(\sigma) dP(\sigma|x)
\end{equation}
and then:
\begin{align*}
\mathcal{E}(d \circ g^*) - \mathcal{E}(s^*)&= \int_{\mathcal{X}\times \mathcal{Y}} \| \phi(d\circ g^* (x)) - \phi(\sigma)\|^2_{\mathcal{F}} - \| \phi(s^*(x)) - \phi(\sigma)\|^2_{\mathcal{F}} dP(x, \sigma)\\
& \le \int_{\mathcal{X}\times \mathcal{Y}} \langle \phi(d\circ \widehat{g}(x)) - \phi(s^* (x)), \phi(d\circ \widehat{g}(x)) + \phi(s^* (x)) - 2\phi(\sigma) \rangle_{\mathcal{F}} dP(x, \sigma)\\
& \le 4 c_{\Delta} \int_{\mathcal{X}} \|  \phi(d\circ g^* (x)) - \phi(s^*(x)) \|_{\mathcal{F}} dP(x)\\
&\le 4 c_{\Delta} \int_{\mathcal{X}} \|  d_L\circ g^* (x)) -d_L \circ \phi(s^*(x)) \|_{\mathcal{F}} dP(x)\\
&\le 4 c_{\Delta} \int_{\mathcal{X}} \| g^* (x)) - \phi(s^*(x)) \|_{\mathcal{F}} dP(x) + \mathcal{O}(n \sqrt{n})\\
\end{align*}
where we used that $\phi(s^*(x))\in \mathcal{C}_n$ so $d_L\circ\phi(s^*(x))=\phi(s^*(x))$. Then we can plug~\eqref{eq:minimizer_surrogate} in the right term:
\begin{align*}
\mathcal{E}(d \circ g^*) - \mathcal{E}(s^*)&\le  4 c_{\Delta} \int_{\mathcal{X}} \| \int_{\mathcal{Y}} \phi(\sigma) dP(\sigma|x) - \phi(s^*(x)) \|_{\mathcal{F}} dP(x) + \mathcal{O}(n \sqrt{n})\\
& \le  4 c_{\Delta}\int_{\mathcal{X} \times \mathcal{Y}} \| \phi(\sigma) - \phi(s^*(x)) \|_{\mathcal{F}} dP(x) + \mathcal{O}(n \sqrt{n})\\
&\le 4 c_{\Delta} \mathcal{E}(s^*) + \mathcal{O}(n \sqrt{n})
\end{align*}



\begin{remark}\label{rk:consistency_lehmer}
	As proved in Theorem~19 in \citep{ciliberto2016consistent}, since the space of rankings $\mathcal{Y}$ is finite, $\Delta_L$  necessarily satisfies Assumption~1 with some continuous embedding $\psi$. If the approach we developped was relying on this $\psi$, we would have consistency for the minimizer $g^*$ of the Lehmer loss~\eqref{eq:loss_lehmer}. However, the choice of $\phi_L$ is relevant because it yields a pre-image problem with low computational complexity.
\end{remark}
