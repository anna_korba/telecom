\subsection{The Kemeny Embedding} 

\begin{sloppypar}
Motivated by the minimization of the Kendall's $\tau$ distance $\Delta_\tau$, we study the Kemeny embedding, previously introduced for the ranking aggregation problem (see \citet{JKS16}):% which maps a permutation $\sigma$ onto a vector $\phi_K(\sigma)\in \mathbb{R}^{n(n-1)/2}$, more formally:
\begin{align*}
\phi_{\tau} \colon \Sn &\to \mathbb{R}^{n(n-1)/2}\\
\sigma & \mapsto \left(
\sign(\sigma(j)-\sigma(i))\right)_{1\leq i < j \leq n}  \,.
\end{align*}
which maps any permutation $\sigma \in \Sn$ into $Im(\phi_{\tau})\subsetneq \{-1,1\}^{n(n-1)/2}$ (that we have embedded into the Hilbert space $(\mathbb{R}^{n(n-1)/2}, \langle ., . \rangle)$). One can show that the square of the euclidean distance between the mappings of two permutations $\sigma, \sigma' \in \Sn$  recovers their Kendall's $\tau$ distance (proving at the same time that $\phi_{\tau}$ is injective) up to a constant: $\| \phi_{\tau}(\sigma)- \phi_{\tau}(\sigma')\|^2= 4 \Delta_{\tau}(\sigma, \sigma')$. The Kemeny embedding then naturally appears to be a good candidate to build a surrogate loss related to $\Delta_\tau$. By noticing that $\phi_{\tau}$ has a constant norm  ($\forall \sigma \in \Sn $, $\|\phi_{\tau}(\sigma)\|=\sqrt{n(n-1)/2}$), we can rewrite the pre-image problem (\ref{eq:emp_inverse_image}) under the form:
\begin{equation}\label{kemeny:pre-image}
\widehat{s}(x)=\argmin_{\sigma \in \Sn} - \langle \phi_{\tau}(\sigma), \widehat{g}(x) \rangle.
\end{equation}
To compute \eqref{kemeny:pre-image}, one can first solve an Integer Linear Program (ILP) to find $\widehat{\phi_\sigma} = \argmin_{\phi_\sigma \in Im(\phi_{\tau})} - \langle \phi_\sigma, \widehat{g}(x) \rangle$, and then find the output object $\sigma=\phi_{\tau}^{-1}(\widehat{\phi_\sigma})$. The latter step, i.e. inverting $\phi_{\tau}$, can be performed in $\mathcal{O}(n^2)$ by means of the Copeland method (see \cite{merlin1997copeland}), which ranks the items by their number of pairwise victories\footnote{Copeland method firstly affects a score $s_i$ for item $i$ as: $s_i=\sum_{j \ne i }\mathbb{I}\{\sigma(i)<\sigma(j)\}$ and then ranks the items by decreasing score.}. In contrast, the ILP problem is harder to solve since it involves a minimization over $Im(\phi_{\tau})$, a set of structured vectors since their coordinates are strongly correlated by the \textit{transitivity} property of rankings. Indeed, consider a vector $v \in Im(\phi_{\tau})$, so $\exists \sigma \in \Sn$ such that $v=\phi_{\tau}(\sigma)$. Then, for any $1 \le i<j<k\le n$, if its coordinates corresponding to the pairs $(i,j)$ and $(j,k)$ are equal to one (meaning that $\sigma(i)<\sigma(j)$ and $\sigma(j)<\sigma(k)$), then the coordinate corresponding to the pair $(i,k)$ cannot contradict the others and must be set to one as well.  Since $\phi_{\sigma}= (\phi_\sigma)_{i,j} \in Im(\phi_{\tau})$ is only defined for $1\leq i < j \leq n$, one cannot directly encode the transitivity constraints that take into account the components $(\phi_\sigma)_{i,j}$ with $j>i$. Thus to encode the transitivity constraint we introduce $\phi_\sigma'=(\phi'_\sigma)_{i,j} \in \mathbb{R}^{n(n-1)}$ defined by $(\phi'_\sigma)_{i,j} = (\phi_\sigma)_{i,j} \; \text{if} \; 1\leq i < j \leq n \; \text{and} \; (\phi'_\sigma)_{i,j} = - (\phi_\sigma)_{i,j} \; \text{else}$, and write the ILP problem as follows:
 \begin{align}\label{eq:ILP_Kendall}
 \begin{split}
 &\widehat{\phi_\sigma}=\argmin_{\phi_\sigma'}\sum_{1\leq i, j \leq n} {\widehat{g}(x)_{i,j}}(\phi'_\sigma)_{i,j},\\ %+ (1-{g(x)_{i,j}})(1-{\phi_y}_{i,j}'), \\
 & s.c. \enspace  \begin{cases}
 (\phi'_\sigma)_{i,j}\in\{-1,1\} \quad \forall \; i,j\\
 (\phi'_\sigma)_{i,j}+(\phi'_\sigma)_{j,i}=0 \quad \forall \; i,j\\
 -1 \le (\phi'_\sigma)_{i,j}+(\phi'_\sigma)_{j,k}+(\phi'_\sigma)_{k,i} \le 1 \text{ } \quad \forall \; i,j,k \; \text{ s.t. } \; i\neq j \neq k.
 \end{cases}
 \end{split}
 \end{align}
Such a problem is NP-Hard. In previous works (see \cite{calauzenes2012non,ramaswamy2013convex}), the complexity of designing calibrated surrogate losses for the Kendall's $\tau$ distance had already been investigated. In particular, \cite{calauzenes2012non} proved that there exists no convex $n$-dimensional calibrated surrogate loss for Kendall's $\tau$ distance. As a consequence, optimizing this type of loss has an inherent computational cost. %that cannot be reduced by optimizing instead some surrogates enabling efficient optimization. 
However, in practice, branch and bound based ILP solvers find the solution of~\eqref{eq:ILP_Kendall} in a reasonable time for a reduced number of labels $n$. We discuss the computational implications of choosing the Kemeny embedding section~\ref{sec:complexity}. We now turn to the study of an embedding devoted to build a surrogate loss for the Hamming distance.
\end{sloppypar}


% calauzenes: Our main contribution is a definitive and negative answer to that question. We prove that if a surrogateloss is convex, then it cannot be calibrated with respect to any of the AP, the ERR or the PD. Thus, if one of these metrics should be optimized, the price to pay for the computational advantage of convex losses is an inconsistent learning procedure, which may converge to non-optimal predictions as the number of examples increases
