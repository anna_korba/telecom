\section{Structured Prediction for Label Ranking}\label{sec:structured_prediction}


\subsection{Learning Problem}

Our goal is to learn a function $s: \X \rightarrow \Y$ between a feature space $\X$ %(e.g, characteristics of a user) 
and a structured output space $\Y$, that we set to be $\Sn$ the space of full rankings over the set of items $\n$. %\footnote{In Section 4.4, we will extend the task to partial and incomplete rankings}.% (representing preferences/rankings over a set of items). 
The quality of a prediction $s(x)$ is measured using a loss function $\Delta: \Sn \times \Sn \rightarrow \mathbb{R}$, where $\Delta(s(x), \sigma)$  is the cost suffered by predicting $s(x)$ for the true output $\sigma$. We suppose that the input/output pairs $(x,\sigma)$ come from some fixed distribution $P$ on $\mathcal{X}\times \Sn$. The label ranking problem is then defined as:
\begin{equation}\label{eq:risk_theo}
\text{minimize}_{s:\mathcal{X} \rightarrow \Sn} \mathcal{R}(s), \quad \text{ with } \quad 
\mathcal{R}(s)=\int_{\mathcal{X}\times \Sn} \Delta(s(x),\sigma) dP(x,\sigma).
\end{equation}
In this paper, we propose to study how to solve this problem and its empirical counterpart for a family of  loss functions based on some ranking embedding $\phi:\Sn \rightarrow \mathcal{F}$ that maps the permutations $\sigma \in \Sn$ into a Hilbert space $\mathcal{F}$:
\begin{equation}\label{loss_embedding}
\Delta(\sigma, \sigma')=\| \phi(\sigma)- \phi(\sigma')\|_{\mathcal{F}}^2.
 \end{equation}
This loss presents two main advantages: first, there exists popular losses for ranking data that can take this form within a finite dimensional Hilbert Space $\mathcal{F}$, second, this choice benefits from the theoretical results on Surrogate Least Square problems for structured prediction using Calibration Theory of \cite{ciliberto2016consistent} %et osokin 
and of works of \cite{brouard2016input} on Structured Output Prediction within vector-valued Reproducing Kernel Hilbert Spaces. These works approach Structured Output Prediction along a common angle by introducing
a surrogate problem involving a function $g:\mathcal{X}\rightarrow \mathcal{F}$ (with values in $\mathcal{F}$) and a surrogate loss $L(g(x),\sigma)$ to be minimized instead of Eq. \ref{eq:risk_theo}. The surrogate loss is said to be calibrated if a minimizer for the surrogate loss is always optimal for the true loss  \citep{calauzenes2012non}. In the context of true risk minimization, the surrogate problem for our case writes as:
\begin{equation}\label{eq:surrogate_pb}
\text{ minimize }_{g: \mathcal{X}\rightarrow \mathcal{F}} \mathcal{L}(g), \quad \text{ with } \quad \mathcal{L}(g)=\int_{\mathcal{X}\times \Sn} L(g(x),\phi(\sigma)) dP(x,\sigma).
\end{equation}
with the following surrogate loss:
\begin{equation}
L(g(x),\phi(\sigma))=\| g(x)-\phi(\sigma) \|^2_{\mathcal{F}}.
\end{equation}
Problem of Eq. \eqref{eq:surrogate_pb} is in general easier to optimize since $g$ has values in $\mathcal{F}$ instead of the set of structured objects $\mathcal{Y}$, here $\Sn$. The solution of~\eqref{eq:surrogate_pb}, denoted as $g^*$, can be written for any $x\in \mathcal{X}$:  $g^*(x)=\mathbb{R}[\phi(\sigma)|x]$.
Eventually, a candidate $s(x)$ pre-image for $g^*(x)$ can then be obtained by solving:
\begin{equation}\label{eq:pre-image}
s(x)= \argmin_{\sigma \in \Sn} L(g^*(x), \phi(\sigma)).
\end{equation}
In the context of Empirical Risk Minimization, a training sample $\mathcal{S}=\{(x_i,\sigma_i), i=1, \ldots, N\}$, with $N$ i.i.d. copies of the random variable $(x,\sigma)$ is available. The Surrogate Least Square approach for Label Ranking Prediction decomposes into two steps: 
\begin{itemize}
\item Step 1: minimize a regularized empirical risk to provide an estimator of the minimizer of the regression problem in Eq. \eqref{eq:surrogate_pb}:
\begin{equation}\label{eq:emp_surrogate_pb}
\text{ minimize }_{g \in \mathcal{H}}\ \mathcal{L}_{\mathcal{S}}(g), \quad \text{ with } \quad \mathcal{L}_{\mathcal{S}}(g)=\frac{1}{N}\sum_{i=1}^N  L(g(x_i),\phi(\sigma_i)) + \Omega(g).
\end{equation}
with an appropriate choice of hypothesis space $\mathcal{H}$ and complexity term $\Omega(g)$. We denote by $\widehat{g}$ a solution of~\eqref{eq:emp_surrogate_pb}.
\item Step 2: 
solve, for any $x$ in $\mathcal{X}$, the pre-image problem that provides a prediction in the original space $\Sn$:
\begin{equation}\label{eq:emp_inverse_image}%\argmin_{y \in \mathcal{Y}} \| \phi(y) - \widehat{g}(x)\|^2
\widehat{s}(x)= \argmin_{\sigma \in \Sn} \| \phi(\sigma) - \widehat{g}(x)\|_{\mathcal{F}}^{2}.
\end{equation}
The pre-image operation can be written as $\widehat{s}(x)=d\circ \widehat{g}(x)$ with $d$ the decoding function:
\begin{equation}\label{eq:decoding_function}
d(h)=\argmin_{\sigma \in \Sn} \|\phi(\sigma)- h \|_{\mathcal{F}}^{2}\text{ for all } h \in \mathcal{F},
\end{equation} 
applied on $\widehat{g}$ for any $x \in \mathcal{X}$.
\end{itemize}
This paper studies how to leverage the choice of the embedding $\phi$ to obtain a good compromise between computational complexity and theoretical guarantees. Typically, the pre-image problem on the discrete set $\Sn$ (of cardinality $n!$) can be eased for appropriate choices of $\phi$ as we show  in section 4, leading to efficient solutions. In the same time, one would like to benefit from theoretical guarantees and control the excess risk of the proposed predictor $\widehat{s}$.

In the following subsection we exhibit popular losses for ranking data that we will use for the label
ranking problem.

\subsection{Losses for Ranking}


We now present losses $\Delta$ on $\Sn$ that we will consider for the label ranking task. A natural loss for full rankings, i.e. permutations in $\Sn$, is a distance between permutations.  Several distances on $\Sn$ are widely used in the literature \citep{Deza}, one of the most popular being the \textit{Kendall's $\tau$ distance}, which counts the number of pairwise disagreements between two permutations $\sigma,\sigma' \in \Sn$:
\begin{equation}\label{eq:kendall_dist}
\Delta_{\tau}(\sigma, \sigma')=\sum_{i<j}\mathbb{I}[(\sigma(i)-\sigma(j))(\sigma'(i)-\sigma'(j))<0].
\end{equation}
The maximal Kendall's $\tau$ distance is thus $n(n-1)/2$, the total number of pairs.  % between $\widetilde{\sigma}$ and $\widetilde{\sigma}'$.
Another well-spread distance between permutations is the \textit{Hamming distance}, which counts the number of entries on which two permutations $\sigma,\sigma' \in \Sn$ disagree:
\begin{equation}\label{eq:hamming_dist}
\Delta_{H}(\sigma, \sigma')=\sum_{i=1}^{n}\mathbb{I}[\sigma(i)\ne \sigma'(i)].
\end{equation}
The maximal Hamming distance is thus $n$, the number of labels or items. 

The Kendall’s $\tau$ distance is a natural discrepancy measure when permutations are interpreted as rankings and is thus the most widely used in the preference learning literature. In contrast, the Hamming distance is particularly used when permutations represent matching of bipartite graphs and is thus also very popular (see \cite{fathony2018efficient}). %\al{Moreover the connection between the Hamming distance in the context of label ranking and the underlying optimal transport problem on permutations have been highlighted in \cite{clemenccon2010kantorovich}}. 
In the next section we show how these distances can be written as Eq. \eqref{loss_embedding} for a well chosen embedding $\phi$.%\anna{modif: motiver kendall et hamming. (+justifier absence de NDCG?... cf article de Chen)}
% @inproceedings{chen2009ranking,
%title={Ranking measures and loss functions in learning to rank},
%author={Chen, Wei and Liu, Tie-Yan and Lan, Yanyan and Ma, Zhi-Ming and Li, Hang},
%booktitle={Advances in Neural Information Processing Systems},
%pages={315--323},
%year={2009}
%}





%, some of them having been already used in the litterature on ranking data but for other learning tasks. We also discuss their pros and cons for the output kernel regression approach we adopt.
