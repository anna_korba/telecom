\section*{APPENDIX - PROOFS}

In this section, we denote by $\binom{n}{k}$ the binomial coefficient indexed by $n$ and $k$, by $\mathcal{B}(N,p)$ the binomial distribution indexed by parameters $N,p$, by $(i,j)$ the transposition that swaps item $i$ and $j$, and by $K(P\vert\vert Q)$ the Kullback-Leibler divergence between two probability distributions $P$ and $Q$.

\subsection*{Proof of Theorem \ref{thm:optimal}}

Suppose that distribution $P$ is stochastically transitive. The pairwise probabilities can be then represented as a directed graph on the $n$ items with the following definition: each item $i$ is represented by a vertex, and an edge is drawn from $i$ to $j$ whenever $p_{i,j}>\frac{1}{2}$ (no edge is drawn if $p_{i,j}=\frac{1}{2}$). This graph is thus a directed acyclic graph, since the stochastic transitivity prevents the occurrence of any cycle. Hence, there exists a partial order on the graph (also referred to as \textit{topological ordering}, the vertices representing the items are sorted by their in-degree) and any permutation $\sigma^*$ extending this partial order satisfies \eqref{eq:cond_opt}. In addition, under the stochastic transitivity condition, for $i<j$, $p_{i,j}<1/2 \Leftrightarrow s^*(i)<s^*(j)$. So $s^*$ belongs to $\Sn$ iff $P$ is stricly stochastically transitive and in this case, $s^*$ is the unique median of $P$.

\subsection*{Proof of Remark \ref{remark:borda}}

With simple calculations, the Borda score of any item $i\in \n$ can be written as $s(i) = 1+ \sum_{k\ne i} p_{k,i}$. Suppose that $p_{i,j}>1/2$ ($\Leftrightarrow s^*(i)< s^*(j)$ under \textit{stochastic transitivity}). We have $s(j)-s(i)=\sum_{k\ne i,j} p_{k,j}- p_{k,i} + (2 p_{i,j}- 1)$ with $2 p_{i,j}- 1>0$. We prove that for any $k \ne i,j$, we have $p_{k,j}- p_{k,i}\ge 0$ under the strong stochastic transitivity condition (by considering firstly the case where $p_{j,k} \ge 1/2$ and next the case where $p_{k,j} > 1/2$). We obtain that $(s(i)-s(j))(s^*(i)-s^*(j))>0$, the scoring functions $s$ and $s^*$ yield exactly the same ranking on the set of items.

\subsection*{Proof of Proposition \ref{prop:ERM}}

Apply first Cauchy-Schwarz inequality, so as to get
\begin{equation*}
\mathbb{E}_{\i,\j}\left[ \vert p_{\i,\j}-\widehat{p}_{\i,\j} \vert  \right] \le \sqrt{\mathbb{E}_{\i,\j}\left[ ( p_{\i,\j}-\widehat{p}_{\i,\j} )^2 \right]}= 
\sqrt{Var( \widehat{p}_{\i,\j})},
\end{equation*}
since $\mathbb{E}_{\i,\j}\left[  p_{\i,\j}-\widehat{p}_{\i,\j} \right]=0$.
Then, for $i<j$, $N \widehat{p}_{i,j} \sim \mathcal{B}(N,p_{i,j}))$ and thus $Var( \widehat{p}_{\i,\j})\le \frac{1}{4N}. $
%\begin{equation}
%\label{eq:var_binomial}
%Var( \widehat{p}_{\i,\j})= \frac{ p_{\i,\j}(1- p_{\i,\j})}{N} \le \frac{1}{4N}.
%\end{equation}
Combining \eqref{eq:classic2} with the last upper bound on the variance %\eqref{eq:var_binomial}
 finally gives the upper bound stated.

A related probability bound can also be established as follows.
By \eqref{eq:classic} and \eqref{eq:classic2}, we have: for any $t > 0$,
\begin{equation*}
\mathbb{P}\Big\{L(\widehat{\sigma}_{N})-L^{\ast} > t\Big\} 
\leq \mathbb{P}\Big\{\sum_{i < j}\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2}\Big\}.
\end{equation*}
On the other hand, we have:
\begin{equation*}
\mathbb{P}\Big\{\sum_{i < j}\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{2}\Big\}\leq \sum_{i < j}\mathbb{P}\Big\{\vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{n(n-1)}\Big\}.
\end{equation*}
The last step consists in applying Hoeffding's inequality to each $p_{i,j}$ for $i<j$ 
\begin{equation*}
\mathbb{P}\Big\{ \vert p_{i,j}-\widehat{p}_{i,j} \vert > \frac{t}{n(n-1)} \Big\} \leq 2e^{-2N(\frac{t}{n(n-1)})^{2}}.
\end{equation*}
Combining the three preceding inequalities gives the bound.

\subsection*{Proof of Proposition \ref{prop:minimax_bound}}

The proof of the minimax lower bound is based on Le Cam's method, see section 2.3 in \cite{tsybakov2009introduction}.
 Consider two Mallows models $P_{\theta_0}$ and $P_{\theta_1}$ where $\theta_k=(\sigma^*_k,\phi)\in \mathfrak{S}_n\times (0,1)$ and $\sigma^*_0\neq \sigma^*_1$. We clearly have:
\begin{align*}
\mathcal{R}_N\geq & \inf_{\sigma_N} \frac{\vert \phi-1 \vert}{2(1+\phi)} \times\\
\max_{k=0,\; 1}&\sum_{i<j} \mathbb{E}_{P_{\theta_k}}\left[\mathbb{I}\{ (\sigma_N(i)-\sigma_N(j)(\sigma_k^*(i)-\sigma_k^*(j))<0  \} \right]\\
\geq & \frac{\vert \phi-1 \vert}{4} \inf_{\sigma_N}\max_{k=0,\; 1} \mathbb{E}_{P_{\theta_k}}\left[d_{\tau} (\sigma_N,\sigma_k^*)  \right].
\end{align*}
Following line by line Le Cam's method, we obtain
\begin{equation*}
\mathcal{R}_N \geq \frac{\vert \phi_-1 \vert}{32} e^{-NK(P_{\theta_0}\vert\vert P_{\theta_1})}.
\end{equation*}
%where 
%\begin{equation*}
%K(P_{\theta_0}\vert\vert P_{\theta_1})=\sum_{\sigma\in \mathfrak{S}_N}P_{\theta_0}(\sigma)\log \left(\frac{P_{\theta_0}(\sigma)}{P_{\theta_1}(\sigma)}\right)
%\end{equation*}
%
Some calculations show that $K(P_{\theta_0}\vert\vert P_{\theta_1})= \log(\frac{1}{\phi}) (1-\phi)/(1+\phi)$, for $\sigma_1=(i,j)\sigma_0$ where $i<j$ verify $\sigma_0(i)=\sigma_0(j)-1$. The desired lower bound is then obtained by taking $\phi=1-1/\sqrt{N}$. More details can be found in the Supplementary Material.

\subsection*{Proposition \ref{prop:low_noise}}

Let $\mathcal{A}_N=\bigcap_{i<j} \{  (p_{i,j}-\frac{1}{2})(\widehat{p}_{i,j}-\frac{1}{2})>0 \}$. On the event $\mathcal{A}_N$, both distributions $p$ and $\widehat{p}$ satisfy the strong stochastic transitivity property, and agree on each pair: $\widehat{\sigma}_N=\sigma$ and	$L(\widehat{\sigma}_N)-L^*=0$. Therefore we only have to bound $\mathbb{P}\Big\{ \mathcal{A}_N^c  \Big\}$. Since for $i<j$, $N \widehat{p}_{i,j} \sim \mathcal{B}(N,p_{i,j}))$, we have 
\begin{equation}
\label{eq:binomial_probability_appendix}
\mathbb{P}\Big\{ \widehat{p}_{i,j}\le\frac{1}{2}  \Big\} 
= \sum_{k=0}^{\lfloor \frac{N}{2} \rfloor}\binom{N}{k}p_{i,j}^k (1-p_{i,j})^{N-k}.
\end{equation}
Then, $\sum_{k=0}^{\lfloor \frac{N}{2} \rfloor}\binom{N}{k}\le 2^{N-1}$ and since $p_{i,j}> 1/2$, for $k \le \frac{N}{2}$, we have
\begin{equation*}
p_{i,j}^k (1-p_{i,j})^{N-k} \le p_{i,j}^{\frac{N}{2}} (1-p_{i,j})^{\frac{N}{2}} \le \left( \frac{1}{4}- h^2\right)^{\frac{N}{2}}
\end{equation*}
Finally, we have $\mathbb{P}\Big\{ \mathcal{A}_N^c  \Big\}\le \sum_{i<j} \mathbb{P}\Big\{ \widehat{p}_{i,j}\le\frac{1}{2}  \Big\}$ and the desired bound is proved.

\subsection*{Proof of Proposition \ref{prop:lower_bound_low_noise}}

Similarly to Proposition \ref{prop:minimax_bound}, we can bound by below the minimax risk as follows
\begin{align*}
\mathcal{R}_N\geq& \inf_{\sigma_N}\max_{k=0,\; 1} h \mathbb{E}_{P_{\theta_k}}\left[ d_{\tau}(\sigma_N, \sigma^*)\right]\\
\geq & \frac{h}{8} e^{-N K(P_{\theta_0}\vert \vert P_{\theta_1})},
\end{align*}
with $K(P_{\theta_0}\vert\vert P_{\theta_1})= \log(\frac{1}{\phi}) (1-\phi)/(1+\phi)$. 
Now we take $\phi= (1-2h)/(1+2h)$ so that both $P_{\theta_0}$ and $P_{\theta_1}$ satisfy {\bf NA}$(h)$, and we have 
\begin{equation*}
K(P_{\theta_0}\vert \vert P_{\theta_1})= 2h \log\left(\frac{1+2h}{1-2h}\right),
\end{equation*}
which finally gives us the bound.

