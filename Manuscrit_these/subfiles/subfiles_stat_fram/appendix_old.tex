
\section*{APPENDIX - PROOFS}

\subsection*{Proof of Theorem \ref{thm:optimal} (Sketch of)}

Suppose $P$ is stochastically transitive. The pairwise probabilities can be represented as a directed graph on the $n$ items. An edge is drawn from $i$ to $j$ whenever $p_{i,j}>\frac{1}{2}$ (no edge is drawn if $p_{i,j}=\frac{1}{2}$). This graph is thus a directed acyclic graph, since the stochastic transitivity prevents any cycle. So there exists a partial order on the graph (also called "topological ordering", the vertices representing the items are sorted by their in-degree). So any permutation $\sigma^*$ extending this partial order satisfies (\ref{eq:cond_opt}). In addition, under the stochastic transitivity condition, for $i<j$, $p_{i,j}<1/2 \Leftrightarrow s^*(i)<s^*(j)$. So $s^*$ belongs to $\Sn$ iff $P$ is stricly stochastically transitive and in this case, $s^*$ is the unique median of $P$.


\subsection*{Proposition \ref{prop:ERM}}

For any empirical median $\widehat{\sigma}_N$, \eqref{eq:classic} combined with the union bound and Hoeffding inequality yields: $\forall t>0$,
\begin{multline*}
	\mathbb{P}\left\{L(\widehat{\sigma}_N)-L^*  >t  \right\}
	%\\ \leq
%	 \sum_{\sigma\in \mathfrak{S}_n}\mathbb{P}\left\{ \frac{1}{N}\left\vert\sum_{i=1}^N\left\{ d(\Sigma_i, \sigma)-L(\sigma)    \right\}  \right\vert     >t/2 \right\}\\
	\leq  2n!\exp\left(-\frac{Nt^2}{2\vert\vert d\vert\vert_{\infty}^2}  \right).
\end{multline*}
%where $\vert\vert d\vert\vert_{\infty}=\max_{(\sigma,\sigma')\in \mathfrak{S}_n^2}d(\sigma,\sigma')$. 

\subsection*{Proof of Proposition \ref{prop:minimax_bound}}

In the following proof, we use Le Cam's method, see section 2.3 in \cite{Tsybakov}. Consider two Mallows models $P_{\theta_0}$ and $P_{\theta_1}$ where $\theta_k=(\sigma^*_k,\phi_k)\in \mathfrak{S}_n\times (0,1)$ and $\sigma^*_0\neq \sigma^*_1$. We clearly have:
\begin{align*}
	\mathcal{R}_N\geq& \inf_{\sigma_N}\max_{k=0,\; 1} \mathbb{E}_{P_{\theta_k}}\left[L_{P_{\theta_k}}(\sigma_N)-L_{P_{\theta_k}}^*  \right]\\
	= & \inf_{\sigma_N}\max_{k=0,\; 1}\sum_{i<j} \mathbb{E}_{P_{\theta_k}}\left[\frac{\vert \phi_k-1 \vert}{2(1+\phi_k)}\times \right. \\
	& \left. \mathbb{I}\{ (\sigma_N(i)-\sigma_N(j)(\sigma^*(i)-\sigma_k^*(j))<0  \} \right]\\
	\geq & \frac{1}{4} \inf_{\sigma_N}\max_{k=0,\; 1}\vert \phi_k-1 \vert \mathbb{E}_{P_{\theta_k}}\left[d_{\tau} (\sigma_N,\sigma_k^*)  \right],
\end{align*}
using \eqref{eq:Mallows_pair}. Set $ \Delta=d_{\tau}(\sigma^*_0,\sigma^*_1)\geq 1$, and consider the test statistic related to $\sigma_N$:
\begin{equation}
\psi(\Sigma_1,\; \ldots,\; \Sigma_N)=\mathbb{I}\{ d_{\tau}(\sigma_N,\sigma^*_1)\leq d_{\tau}(\sigma_N,\sigma^*_0)\ \}.
\end{equation}
If $\psi=1$, by triangular inequality, we have:
\begin{equation}
\Delta \leq d_{\tau}(\sigma_N,\sigma^*_0)+d_{\tau}(\sigma_N,\sigma^*_1)\leq 2d_{\tau}(\sigma_N,\sigma^*_0).
\end{equation}
Hence, we have
\begin{align*}
\mathbb{E}_{P_{\theta_0}}\left[d_{\tau} (\sigma_N,\sigma_0^*)  \right]&\geq \mathbb{E}_{P_{\theta_0}}\left[d_{\tau} (\sigma_N,\sigma_0^*) \mathbb{I}\{ \psi=+1 \} \right]\\
&\geq \frac{\Delta}{2}\mathbb{P}_{\theta_0}\{\psi=+1  \}
\end{align*}
and similarly
\begin{align*}
\mathbb{E}_{P_{\theta_1}}\left[d_{\tau} (\sigma_N,\sigma_1^*)  \right]&\geq \mathbb{E}_{P_{\theta_1}}\left[d_{\tau} (\sigma_N,\sigma_1^*) \mathbb{I}\{ \psi=0 \} \right]\\
&\geq \frac{\Delta}{2}\mathbb{P}_{\theta_1}\{\psi=0  \}.
\end{align*}
Bounding by below the maximum by the average, we have:
\begin{multline*}
\inf_{\sigma_N}\max_{k=0,\; 1}\vert \phi_k-1 \vert \mathbb{E}_{P_{\theta_k}}\left[d_{\tau} (\sigma_N,\sigma_k^*)  \right]\geq  \\ 
\inf_{\sigma_N}\frac{\Delta}{2} \frac{1}{2}\left\{ \vert \phi_1-1 \vert \mathbb{P}_{\theta_1}\{\psi=0  \}  + \vert \phi_0-1 \vert \mathbb{P}_{\theta_0}\{\psi=1  \} \right\}\geq \\
\inf_{\sigma_N}\frac{\Delta}{4}\min_{k=0,\; 1}\vert \phi_k-1 \vert \left\{ \mathbb{P}_{\theta_1}\{\psi=0  \}  + \mathbb{P}_{\theta_0}\{\psi=1  \} \right\}\geq\\
\frac{\Delta}{4}\min_{k=0,\; 1}\vert \phi_k-1 \vert \left\{ \mathbb{P}_{\theta_1}\{\psi^*=0  \}  + \mathbb{P}_{\theta_0}\{\psi^*=1  \} \right\},
\end{multline*}
where the last inequality follows from a standard Neyman-Pearson argument, denoting by
\begin{multline*}
\psi^*(\Sigma_1,\; \ldots,\; \Sigma_N)=\mathbb{I}\left\{\prod_{i=1}^N\frac{P_{\theta_1}(\Sigma_i)}{P_{\theta_0}(\Sigma_i)}\geq 1\right\}
\end{multline*}

the likelihood ratio test statistic. We deduce that 
\begin{multline*}
\mathcal{R}_N\geq \frac{\Delta}{16}\min_{k=0,\; 1}\vert \phi_k-1 \vert \times\\
\sum_{\sigma_i\in \mathfrak{S}_N,\; 1\leq i\leq N}\min\left\{\prod_{i=1}^NP_{\theta_0}(\sigma_i),\;  \prod_{i=1}^NP_{\theta_1}(\sigma_i) \right\}\\
\geq \frac{\Delta}{32}\min_{k=0,\; 1}\vert \phi_k-1 \vert e^{-NK(P_{\theta_0}\vert\vert P_{\theta_1})},
\end{multline*}
where $K(P_{\theta_0}\vert\vert P_{\theta_1})=\sum_{\sigma\in \mathfrak{S}_N}P_{\theta_0}(\sigma)\log(P_{\theta_0}(\sigma)/P_{\theta_1}(\sigma))$ denotes the Kullback-Leibler divergence. In order to establish a minimax lower bound of order $1/\sqrt{N}$, one should choose $\theta_0$ and $\theta_1$ so that, for $k\in\{0,\; 1  \}$, $\phi_k\rightarrow 1$ and $K(P_{\theta_0}\vert\vert P_{\theta_1})\rightarrow 0$ as $N\rightarrow +\infty$ at appropriate rates. Take
$\phi_0=1-1/\sqrt{N}$ and  $\phi_1=(1-1/\sqrt{N})(1-1/N)$.
Observe that we then have $\min_{k=0,\; 1}\vert\phi_k-1 \vert\geq 1/\sqrt{N}$ and
\begin{align*}
 K(P_{\theta_0}\vert\vert P_{\theta_1})&=\sum_{\sigma\in \mathfrak{S}_n}\frac{1}{Z_0}\phi_0^{d_{\tau}(\sigma_0,\sigma)}\log\left(  \frac{Z_1}{Z_0}\frac{\phi_0^{d_{\tau}(\sigma_0,\sigma)}}{\phi_1^{d_{\tau}(\sigma_1,\sigma)}}\right)\\
=&\log(\frac{Z_1}{Z_0}) +\sum_{\sigma\in \mathfrak{S}_n}\frac{1}{Z_0}\phi_0^{d_{\tau}(\sigma_0,\sigma)}\left( \log \left(\frac{\phi_0}{\phi_1}\right)^{d_{\tau}(\sigma_0,\sigma)}\right.\\
 &\left.+ \log \left(\frac{1}{\phi_1^{d_{\tau}(\sigma_1,\sigma)-d_{\tau}(\sigma_0,\sigma)}}\right)\right)\\
=& \log(\frac{Z_1}{Z_0}) + \log\left(\frac{\phi_0}{\phi_1}\right)\sum_{\sigma\in \mathfrak{S}_n}P_{\theta_0}(\sigma) d_{\tau}(\sigma_0,\sigma)\\
+& \log\left(\frac{1}{\phi_1}\right)\sum_{\sigma\in \mathfrak{S}_n}P_{\theta_0}(\sigma) \left(-d_{\tau}(\sigma_0,\sigma)+d_{\tau}(\sigma_1,\sigma)\right)\\
=&\log(\frac{Z_1}{Z_0}) +\log (\phi_0/\phi_1)L_{P_{\theta_0}}(\sigma_0)\\
+&\log(\frac{1}{\phi_1})(L_{P_{\theta_0}}(\sigma_1)-L_{P_{\theta_0}}(\sigma_0))\\
=&/log(\frac{Z_1}{Z_0}) +\log (\frac{\phi_0}{\phi_1})\frac{n(n-1)}{2}\frac{\phi_0}{1+\phi_0}\\
+&\log(\frac{1}{\phi_1})\frac{\vert \phi_0-1 \vert}{2(\phi_0+1)}\Delta. 
\end{align*}


\noindent The last inequality holds because $\phi_1 \le 1$ so $\log(\phi_1) \le 0$.
Then, by using the log-sum inequality :
\begin{align*}
\log \left(\frac{Z_1}{Z_0}\right)	=&\log\left( \frac{\prod_{i=1}^{n-1} \sum_{j=0}^{i} \phi_1^j}{\prod_{i=1}^{n-1} \sum_{j=0}^{i} \phi_0^j}\right)\\
=& \sum_{i=1}^{n-1} \log \left( \frac{\sum_{j=0}^{i} \phi_1^j}{\sum_{j=0}^{i} \phi_0^j} \right)\\
 \le & \sum_{i=1}^{n-1} \frac{1}{\sum_{j=0}^{i} \phi_1^j} \sum_{j=0}^{i} \phi_1^j \log \left( \frac{\phi_1^j}{\phi_0^j} \right)\\
 \le & \sum_{i=1}^{n-1} \frac{1}{\sum_{j=0}^{i} \phi_1^j} \sum_{j=0}^{i} \phi_1^j j \log \left( \frac{\phi_1}{\phi_0} \right)\\
 \le & \sum_{i=1}^{n-1} i \log \left( \frac{\phi_1}{\phi_0} \right)\\
 = & \frac{n(n-1)}{2} \log \left( \frac{\phi_1}{\phi_0} \right)
\end{align*}
\noindent Finally:
\begin{align*}
K(P_{\theta_0}\vert\vert P_{\theta_1}) &\le n(n-1) \log \left( \frac{\phi_1}{\phi_0} \right)\\ 
=& - n(n-1) \log \left( 1 - \frac{1}{N} \right)\\
=& \mathcal{O} (\frac{1}{N} ).
\end{align*}
Then, by taking $\Delta= \frac{n(n-1)}{2}$, we get the bound of the proposition.



\subsection*{Proposition \ref{prop:low_noise}}
	Let $\mathcal{A}_N=\{ \bigcap_{i<j} (p_{i,j}-\frac{1}{2})(\widehat{p}_{i,j}-\frac{1}{2})>0 \}$.
	On the event $\mathcal{A}_N$, $p$ and $\widehat{p}$ satisfy the strongly stochastic transitivity property, so we can write the excess of risk as (\ref{eq:excess_plugin}) and	$L(\widehat{\sigma}_N)-L^*=0$. Moreover,
	\begin{align*}
	\mathbb{P}\Big\{ \mathcal{A}_N^c  \Big\}&= \mathbb{P}\Big\{ \bigcup_{i<j}(p_{i,j}-\frac{1}{2})(\widehat{p}_{i,j}-\frac{1}{2})<0  \Big\}\\
	&\le \sum_{i<j} \mathbb{P}\Big\{ (p_{i,j}-\frac{1}{2})(\widehat{p}_{i,j}-\frac{1}{2})<0  \Big\}\\
	&\le \sum_{i<j} \mathbb{P}\Big\{ \vert p_{i,j} - \widehat{p}_{i,j} \vert > 2h/\sqrt{1-2h} \Big\}\\
	&\le \sum_{i<j} 2 e^{-2N4h^2/(1-2h)}\\
	&\le  2 \binom{n}{2} e^{-8h^2/(1-2h)N}
	\end{align*}
	So the expectation of the excess of risk is: 
	\begin{align*}
	\mathbb{E}\Big\{L(\widehat{\sigma}_N)-L^*\Big\}&= \mathbb{E}\Big\{(L(\widehat{\sigma}_N)-L^*) \mathbb{I}\{ \mathcal{A}_N \} +\\ &(L(\widehat{\sigma}_N)-L^*) \mathbb{I}\{ \mathcal{A}_N^c \}\Big\}\\
	& \leq \frac{n(n-1)}{4} \mathbb{P} \Big\{ \mathcal{A}_N^c  \Big\}\\
	&\leq \binom{n}{2}^2 e^{-8h^2/(1-2h)N}
	\end{align*}

\subsection*{Remark 12}
The first step is to simplify the expression of the expectation of the excess of risk square and then to use the decomposition expectation-variance.
\begin{align*}
\mathbb{E}[(\widehat{L}^*-L^*  )^2]&= \mathbb{E}\left[ \left( \sum_{i<j}\left\{  \frac{1}{2}-\left\vert \widehat{p}_{i,j}-\frac{1}{2}\right\vert  \right\} \right. \right.\\
& \left. \left. -\sum_{i<j}\left\{  \frac{1}{2}-\left\vert p_{i,j}-\frac{1}{2}\right\vert  \right\} \right)^2 \right] \\
&= \mathbb{E}\left[ \left(\sum_{i<j}\left\vert p_{i,j}-\frac{1}{2}\right\vert -\left\vert \widehat{p}_{i,j}-\frac{1}{2}\right\vert \right)^2 \right] \\
&=\mathbb{E}\left[ \left(\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert\right)^2 \right]\\
&=Var\left(\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right) + \\
& \left(\mathbb{E}\left[\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right] \right)^2
\end{align*}
The second term gives:
\begin{align*}
\mathbb{E}\left[\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right]&=\sum_{\substack{i<j\\p_{i,j}>\widehat{p}_{i,j} }}\mathbb{E}\left[p_{i,j}-\widehat{p}_{i,j}\right]\\
& + \sum_{\substack{i<j\\p_{i,j}<\widehat{p}_{i,j} }}\mathbb{E}\left[\widehat{p}_{i,j}-p_{i,j}\right]\\
&=0
\end{align*}
The first term developped gives: 
\begin{multline*}
Var\left(\sum_{i<j}\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right)=\\  \sum_{i<j} \sum_{i'<j'} Cov\left(\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert , \left\vert p_{i',j'}-\widehat{p}_{i',j'}\right\vert \right)
\end{multline*}
%With:
%\begin{align*}
%Var\left(\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right) &= Var\left( p_{i,j}-\widehat{p}_{i,j} \right) +\\
%&\mathbb{E}\left[ p_{i,j}-\widehat{p}_{i,j} \right]^2 - \mathbb{E}\left[ \left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert \right]\\
%& \le Var\left(\widehat{p}_{i,j} \right)\\
%&= \frac{p_{i,j}(1-p_{i,j})}{N}
%\end{align*}
With: 
\begin{multline*}
Cov\left(\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert , \left\vert p_{i',j'}-\widehat{p}_{i',j'}\right\vert \right)\le\\
 \sqrt{Var(\left\vert p_{i,j}-\widehat{p}_{i,j}\right\vert)Var(\left\vert p_{i',j'}-\widehat{p}_{i',j'}\right\vert )}\\
\le \sqrt{Var(\widehat{p}_{i,j})Var(\widehat{p}_{i',j'})}\\
 \le \sqrt{\frac{p_{i,j}(1-p_{i,j})p_{i',j'}(1-p_{i',j'})}{N^2}}\\
 \le \frac{1}{4N}\\
\end{multline*}
So $ \mathbb{E}[(\widehat{L}^*-L^*  )^2]\le 2 \binom{\binom{n}{2}}{2} \frac{1}{4N}\le \frac{ \binom{n}{2}^2}{4N}$.

\subsection*{Proposition 13}

Same method than for Theorem 5. We define $\theta_0 =(1 \dots n)$, $\theta_1= (n \dots 1)$ and $P_{\theta_0}(\theta_0)=P_{\theta_1}(\theta_1)=\frac{1}{2}+h$, $P_{\theta_0}(\theta_1)=P_{\theta_1}(\theta_0)=\frac{1}{2}-h$. We have :

\begin{align*}
\mathcal{R}_N\geq& \inf_{\sigma_N}\max_{k=0,\; 1} \mathbb{E}_{P_{\theta_k}}\left[L_{P_{\theta_k}}(\sigma_N)-L_{P_{\theta_k}}^*  \right]\\
= & \inf_{\sigma_N}\max_{k=0,\; 1}\sum_{i<j} \mathbb{E}_{P_{\theta_k}}\left[\vert p_{i,j} - \frac{1}{2} \vert\times \right. \\
& \left. \mathbb{I}\{ (\sigma_N(i)-\sigma_N(j)(\sigma^*(i)-\sigma_k^*(j))<0  \} \right]\\
\geq & \inf_{\sigma_N}\max_{k=0,\; 1} h \mathbb{E}_{P_{\theta_k}}\left[ d_{\tau}(\sigma_N, \sigma^*)\right]\\
\geq & h \frac{\Delta}{8} e^{-N K(P_{\theta_0\vert \vert \theta_1})}
\end{align*}
With:
\begin{multline*}
K(P_{\theta_0\vert \vert \theta_1})= P_{\theta_0}(\theta_0) log \left( \frac{P_{\theta_0}(\theta_0)}{P_{\theta_1}(\theta_0)} \right) +\\ P_{\theta_0}(\theta_1) log \left( \frac{P_{\theta_0}(\theta_1)}{P_{\theta_1}(\theta_1)} \right)\\
= (\frac{1}{2}+h) log \left( \frac{\frac{1}{2}+h}{\frac{1}{2}-h}\right) + \\
(\frac{1}{2}-h) log \left( \frac{\frac{1}{2}-h}{\frac{1}{2}+h}\right) \\
=2h log \left(1+ \frac{4h}{1-2h}\right) \\
\leq \frac{8 h^2}{1-2h}\\
\end{multline*}

\subsection*{Proof of Corollary 6}

Suppose $P$ satisfies the \textit{strongly stochastically transitive} condition. According to Theorem 5, there exists $\sigma^* \in \Sn$ satisfying (\ref{eq:cond_opt}) and (\ref{eq:cond_opt2}).
 We already know that $\sigma^*$ is a Kemeny consensus since it minimizes the loss with respect to the Kendall's $\tau$ distance. Then, Copeland's method order the items by the number of their pairwise victories, which corresponds to sort them according to the mapping $s^*$ and thus $\sigma^*$ is a Copeland consensus. Finally, the Borda score for an item is: $s(i) = \sum_{\sigma \in \Sn}\sigma(i)P(\sigma)$. With simple calculations, we have $s(i) = 1+ \sum_{k\ne i} p_{k,i}$. \\
 Let $i,j$ such that $p_{i,j}>1/2$ ($\Leftrightarrow s^*(i)< s^*(j)$ under \textit{stochastic transitivity}).
 \begin{align*}
 s(j)-s(i)&= \sum_{k\ne j} p_{k,j} - \sum_{k\ne i} p_{k,i}\\
 &= \sum_{k\ne i,j} p_{k,j} - \sum_{k\ne i,j} p_{k,i}  +p_{i,j} - p_{j,i}\\
 &= \sum_{k\ne i,j} p_{k,j}- p_{k,i} + (2 p_{i,j}- 1)
 \end{align*}
With $(2 p_{i,j}- 1)>0$. Now we focus on the first term.\\
Let $k \ne i,j$. 
\begin{itemize}
	\item First case: $p_{j,k} \ge 1/2$. The strong stochastic transitivity condition implies that :
	\begin{align*}
	p_{i,k}&\ge \max(p_{i,j}, p_{j,k})\\
	1-p_{k,i}& \ge \max(p_{i,j}, p_{j,k})\\
	p_{k,j}-p_{k,i}&\ge p_{k,j}-1 +\max(p_{i,j}, p_{j,k})\\
	p_{k,j}-p_{k,i}&\ge -p_{j,k}+ \max(p_{i,j}, p_{j,k})\\
	p_{k,j}-p_{k,i}&\ge \max(p_{i,j}-p_{j,k}, 0)\\	
	p_{k,j}-p_{k,i}&\ge 0\\	
	\end{align*}
	\item Second case: $p_{k,j} > 1/2$. If $p_{k,i} \le 1/2$, $p_{k,j}-p_{k,i} >0$. Now if $p_{k,i}>1/2$, the strong stochastic transitivity condition implies that $p_{k,j}\ge max(p_{k,i}, p_{i,j})$
\end{itemize}
So in any case, $\forall k \ne i,j$, $p_{k,j}- p_{k,i}\ge 0$ and $ s(j)-s(i)>0$.
