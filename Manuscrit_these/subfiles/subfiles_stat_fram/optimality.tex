
\section{Optimality}\label{sec:optimality}

As recalled above, the discrete optimization problem \eqref{eq:median_pb} always has a solution, whatever the metric $d$ chosen. In the case of the Kendall's $\tau$ distance however, the optimal elements can be explicitly characterized in certain situations. It is the goal of this section to describe the set of Kemeny medians under specific conditions. As a first go, observe that the risk of a permutation candidate $\sigma\in\mathfrak{S}_n$ can be then written as
\begin{equation}\label{eq:loss-1}
L(\sigma)
%=\sum_{i<j}\mathbb{P}\left\{ \left(\Sigma(j)-\Sigma(i)\right)\left(\sigma(j)-\sigma(i)\right)<0 \right\}\\
=\sum_{i<j} p_{i,j}\mathbb{I}\{\sigma(i)>\sigma(j) \} +\sum_{i<j} (1-p_{i,j})\mathbb{I}\{\sigma(i)<\sigma(j) \}.
\end{equation}
%For notational convenience, we also set $p_{i,i}=1/2$ for $1\leq i\leq n$.

\begin{remark}{\sc (Connection to binary classification)}\label{remark:connection_binary} Let $(\i,\j)$ be a random pair defined on $(\Omega, \mathcal{F},\; \mathbb{P})$, uniformly distributed on the set $\{(i,j):\; 1\leq i<j\leq n\}$ and independent from $\Sigma$. Up to the factor $n(n-1)/2$, the risk \eqref{eq:loss-1} can be rewritten as the expectation of the error made when predicting the sign variable $sgn(\Sigma(\j)-\Sigma(\i))$ by the specific classifier $sgn(\sigma(\j)-\sigma(\i))$:
\begin{equation}
L(\sigma)=\frac{n(n-1)}{2}\mathbb{E}\left[ l_{\i,\j}(\Sigma,\sigma) \right],
\end{equation}
where we set $l_{i,j}(\sigma,\sigma')=\mathbb{I}\{ (\sigma(i)-\sigma(j))\cdot (\sigma'(i)-\sigma'(j))<0   \}$ for all $i<j$, $(\sigma,\sigma')\in \Sn^2$. The r.v. $p_{\i,\j}$ can be viewed as the posterior related to this classification problem.
\end{remark}

We deduce from \eqref{eq:loss-1} that $L^*\geq \sum_{i<j}\min\{p_{i,j},\; 1-p_{i,j}  \}$. In addition, if there exists a permutation $\sigma$ with the property that $\forall i<j$ s.t. $p_{i,j}\neq 1/2$,
\begin{equation}\label{eq:cond_opt}
\left(\sigma(j)-\sigma(i)\right)\cdot \left( p_{i,j}- 1/2\right)>0,
\end{equation}
it would be necessarily a median for $P$ (notice incidentally that $L^*=\sum_{i<j}\min\{p_{i,j},\; 1-p_{i,j}  \}$ in this case).

\begin{definition}
The probability distribution $P$ on $\Sn$ is said to be \textit{stochastically transitive} if it fulfills the condition: $\forall (i,j,k)\in\n^3$,
\begin{equation*}
p_{i,j} \ge 1/2 \text{ and } p_{j,k}\ge 1/2 \Rightarrow
p_{i,k}\ge 1/2
\end{equation*}
%\begin{itemize}
%\item[$(i)$] For all $i<j<k$:
%$\left(p_{i,j}-1/2\right)(p_{j,k}-1/2)0$ $\Rightarrow$
%$\left(p_{i,j}-1/2\right)(p_{i,k}-1/2)>0$
%\end{multline*}
%\item[$(ii)$] For all $i<j$ s.t. $p_{i,j}=1/2$: $p_{j,k}=1/2 \Leftrightarrow  p_{i,k}=1/2$ for all $k$.
%\end{itemize}
%Notice that this condition implies: For all $i,j,k$, $p_{i,j}=1/2 \text{ and }p_{j,k}=1/2 \Rightarrow  p_{i,k}=1/2$.\\
In addition, if $p_{i,j}\neq 1/2$ for all $i<j$, $P$ is said to be \textit{strictly stochastically transitive}.
% condition and this condition can be written: 
%\begin{multline*}
%\left(p_{i,j}-1/2\right)(p_{j,k}-1/2) >0 \Rightarrow\\
%\left(p_{i,j}-1/2\right)(p_{i,k}-1/2) > 0
%\end{multline*}
\end{definition}

 
Let $s^*:\n \rightarrow \n$ be the mapping defined by:
\begin{equation}\label{eq:copeland_formula}
s^*(i)=1+\sum_{k\ne i}\mathbb{I}\{p_{i,k}<\frac{1}{2}  \}
\end{equation}
for all $i\in\n$, which induces the same ordering as the Copeland method (see Subsection \ref{subsec:voting-rules}). Observe that, if the \textit{stochastic transitivity} is fulfilled, then: $p_{i,j}< 1/2 \Leftrightarrow s^*(i)<s^*(j)$. Equipped with this notation, property \eqref{eq:cond_opt} can be also formulated as follows: $\forall i<j$ s.t. $s^*(i) \ne s^*(j)$,
\begin{equation}\label{eq:cond_opt2}
\left(\sigma(j)-\sigma(i)\right)\cdot \left( s^*(j)-s^*(i)\right)>0.
\end{equation}
The result stated below describes the set of Kemeny median rankings under the conditions introduced above, and states the equivalence between the Copeland method and Kemeny aggregation in this setting.
\begin{theorem}\label{thm:optimal}
If the distribution $P$ is stochastically transitive, there exists $\sigma^*\in \Sn$ such that \eqref{eq:cond_opt} holds true. In this case, we have
\begin{eqnarray}\label{eq:Kendall_risk_min}
L^*&=&\sum_{i<j}\min\{p_{i,j},\; 1-p_{i,j}  \}\\
&=&\sum_{i<j}\left\{  \frac{1}{2}-\left\vert p_{i,j}-\frac{1}{2}\right\vert  \right\},\nonumber
\end{eqnarray} the excess of risk of any $\sigma\in \Sn$ is given by
\begin{equation*}
L(\sigma)-L^*=2\sum_{i<j}\vert p_{i,j}-1/2 \vert \cdot \mathbb{I}\{ (\sigma(j)-\sigma(i))(p_{i,j}-1/2)<0 \}
\end{equation*}
and the set of medians of $P$ is the class of equivalence of $\sigma^*$ w.r.t. the equivalence relationship:
\begin{equation}
\sigma \mathcal{R}_P \sigma' \Leftrightarrow (\sigma(j)-\sigma(i))(\sigma'(j)-\sigma'(i) )>0 \text{, for all } i<j \text{ such that } p_{i,j}\neq 1/2.
\end{equation}
In addition, the mapping $s^*$ belongs to $\mathfrak{S}_n$ iff $P$ is strictly stochastically positive. In this case, $s^*$ is the unique median of $P$.
\end{theorem}
The proof is detailed in section~\ref{sec:proofs_stat_fram}. Before investigating the accuracy of empirical Kemeny medians, a few remarks are in order.
% Up to the factor $n(n-1)/2$, it can be rewritten as an expectation taken w.r.t. a random variable $\e=(\i,\j)$ defined on $(\Omega,\; \mathcal{F},\; \mathbb{P})$, uniformly distributed on the set of all pairs of instances $\mathcal{E}_n=\{ (i,j),\; i<j\}$ and independent from $\Sigma$:
 %\begin{equation}
 %d_{\tau}(\sigma,\sigma')=\frac{n(n-1)}{2}\mathbb{E}\left[ l_{\e}(\sigma,\sigma') \right],
% \end{equation}
% where we set $l_e(\sigma,\sigma')=\mathbb{I}\{ (\sigma(i)-\sigma(j))\cdot (\sigma'(i)-\sigma'(j))<0   \}$ for all $e=(i,j)\in \mathcal{E}_n$. In this case, we also have: $\forall \sigma\in \mathfrak{S}_n$,
 %\begin{equation}
 %L(\sigma)=\sum_{i<j}\mathbb{P}\left\{ \left(\Sigma(j)-\Sigma(i)\right)\left(\sigma(j)-\sigma(i)\right)<0 \right\}.
 %\end{equation}
 %We also introduce the quantities $p_{i,j}=\mathbb{P}\{\Sigma(i)<\Sigma(j)  \}=p_{j,i}$ for all $e=(i,j)\in \mathcal{E}_n$.
 
\begin{remark}{\sc (Borda consensus)}\label{rk:borda}
\label{remark:borda}
We say that the distribution $P$ is \textit{strongly stochastically transitive} if $\forall (i,j,k)\in\n^3$:
\begin{equation*}
p_{i,j} \ge 1/2 \text{ and } p_{j,k}\ge 1/2 \Rightarrow p_{i,k}\ge \max(p_{i,j},p_{j,k}).
\end{equation*}
Then under this condition, and for $i<j$, $p_{i,j}\ne \frac{1}{2}$, there exists a unique $\sigma^{*} \in \Sn$ such that \eqref{eq:cond_opt} holds true, corresponding to the Kemeny and Borda consensus both at the same time (see the section~\ref{sec:proofs_stat_fram} for the proof).
\end{remark}
 

\begin{remark}\label{Rk:Mallows}{\sc (Mallows model)} The Mallows model introduced in the seminal contribution \cite{Mallows57} is a probability distribution $P_{\theta}$ on $\mathfrak{S}_n$ parametrized by $\theta=(\sigma_0, \psi)\in \mathfrak{S}_n\times [0,1]$: $\forall \sigma \in \mathfrak{S}_n$,
\begin{equation}\label{eq:Mallows}
P_{\theta_0}(\sigma)=\frac{1}{Z}\psi^{ d_{\tau}(\sigma_0,\sigma) },
\end{equation}
where $Z=\sum_{\sigma\in \mathfrak{S}_n} \psi^{ d_{\tau}(\sigma_0,\sigma) }$ is a normalization constant. One may easily show that $Z$ is independent from $\sigma$ and that $Z=\prod_{i=1}^{n-1}\sum_{j=0}^i \psi^j$.  Observe firstly that the smallest the parameter $\psi$, the spikiest the distribution $P_{\theta}$ (equal to a Dirac distribution for $\psi = 0$). In contrast, $P_{\theta}$ is the uniform distribution on $\mathfrak{S}_n$ when $\psi=1$. 
%For all $i<j$, we thus have:
%\begin{multline}\label{eq:Mallows_pair}
%p_{i,j}=\frac{\phi_0}{1+\phi_0}\mathbb{I}\{\sigma_0(i)>\sigma_0(j)  \}\\
% \quad{} + \frac{1}{1+\phi_0}\mathbb{I}\{\sigma_0(i)<\sigma_0(j)   \}.
%\end{multline}
Observe in addition that, as soon as $\psi<1$, the Mallows model $P_{\theta}$ fulfills the strict stochastic transitivity property. Indeed, it follows in this case from Corollary 3 in \cite{BFHS14} that for any $i<j$, we have: 
\begin{enumerate}[(i)]
	\item $\sigma_0(i)<\sigma_0(j) \Leftarrow p_{i,j}\ge \frac{1}{1+\psi}>\frac{1}{2}$ with equality holding iff $\sigma_0(i)=\sigma_0(j)-1$,
	\item $\sigma_0(i)>\sigma_0(j) \Leftarrow p_{i,j}\le \frac{\psi}{1+\psi}<\frac{1}{2}$ with equality holding iff $\sigma_0(i)=\sigma_0(j)+1$,
	\item $p_{i,j}>\frac{1}{2}$ iff $\sigma_0(i)<\sigma_0(j)$ and $p_{i,j}<\frac{1}{2}$ iff $\sigma_0(i)>\sigma_0(j)$.
\end{enumerate}
This directly implies that for any $i<j$:
\begin{equation*}
	\vert p_{i,j}-\frac{1}{2} \vert \ge \frac{\vert \psi -1 \vert }{2(1+\psi)}
\end{equation*}
Therefore, according to \eqref{eq:Kendall_risk_min}, we have in this setting:
\begin{equation}\label{eq:Mallows_disp}
L^*_{P_{\theta}}\le\frac{n(n-1)}{2}\frac{\psi}{1+\psi}.
\end{equation}
The permutation $\sigma_0$ of reference is then the unique mode of distribution $ P_{\theta}$, as well as its unique median.
\end{remark}

\begin{remark}\label{Rk:BTL model}{\sc (Bradley-Terry-Luce-Plackett model)}
	The Bradley-Terry-Luce-Plackett model \citep{bradley1952rank, Luce59, Plackett75} assumes the existence of some hidden preference vector $w = [w_i ]_{1\le i \le n}$, where $w_i$ represents the underlying preference score of item $i$. For all $i<j$, $p_{ij}= \frac{w_i}{w_i+w_j}$. If $w_1 \le \dots \le w_n$, we have in this case $L^*_{P_{\theta}}=\sum_{i<j}w_i/(w_i+w_j)$. Observe in addition that as soon as for all $i<j$, $w_i \ne w_j$, the model fulfills the strict stochastic transitivity property. The permutation $\sigma_0$ of reference is then the one which sorts the vector $w$ in decreasing order.
\end{remark}


