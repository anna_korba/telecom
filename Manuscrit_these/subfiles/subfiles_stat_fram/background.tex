

\section{Background}\label{sec:background_stat_fram}
We start with a rigorous formulation of (the metric approach of) consensus ranking and describe next the probabilistic framework for ranking aggregation we consider in this chapter. Here and throughout, the indicator function of any event $\mathcal{E}$ is denoted by $\mathbb{I}\{ \mathcal{E} \}$, the Dirac mass at any point $a$ by $\delta_a$, and we set $sgn(x)=2\mathbb{I}\{x\geq 0 \}-1$ for all $x\in\mathbb{R}$. At last, the set of permutations of the ensemble $\n =\{1,\; \ldots,\; n  \}$, $n\geq 1$ is denoted by $\mathfrak{S}_n$.

\subsection{Consensus Ranking}
In the simplest formulation, a (full) ranking on a set of items $\n$ is seen as the permutation $\sigma\in \Sn$ that maps an item $i$ to its rank $\sigma(i)$. Given a collection of $N\geq 1$ permutations $\sigma_{1},\; \ldots,\; \sigma_{N}$, the goal of ranking aggregation is to find $\sigma^*\in \Sn$ that best summarizes it. A popular approach consists in solving the following optimization problem:
\begin{equation}
	\label{eq:ranking_aggregation}
	\min_{\sigma\in \mathfrak{S}_n}\sum_{i=1}^N d(\sigma,\sigma_{i}),
\end{equation}
where $d(.,\; .)$ is a given metric on $\Sn$. Such a barycentric permutation, referred to as a \textit{consensus/median ranking} sometimes, always exists, since $\Sn$ is finite, but is not necessarily unique.
In the most studied version of this problem, termed Kemeny ranking aggregation, the metric considered is equal to the Kendall's $\tau$ distance \citep[see][]{Kemeny59}: $\forall (\sigma,\sigma')\in \mathfrak{S}_n^2$,
\begin{equation*}
d_{\tau}(\sigma,\sigma')=\sum_{i<j}\mathbb{I}\{(\sigma(i)-\sigma(j)) (\sigma'(i)-\sigma'(j))<0   \},
\end{equation*}
\textit{i.e.} the number of pairwise disagreements between $\sigma$ and $\sigma'$.
Such a consensus has many interesting properties, but is NP-hard to compute. Various algorithms have been proposed in the literature to compute acceptably good solutions in a reasonable amount of time, their description is beyond the scope of the chapter, see for example \cite{Ali2012Experiments} or Chapter~\ref{chap:rank_agg} for references.




\subsection{Statistical Framework}
In the probabilistic setting we consider here, the collection of rankings to be aggregated is supposed to be composed of $N\geq 1$ i.i.d. copies $\Sigma_1,\; \ldots,\; \Sigma_N$ of a generic random variable $\Sigma$ defined on a probability space $(\Omega,\; \mathcal{F},\; \mathbb{P})$ drawn from an unknown probability distribution $P$ on $\mathfrak{S}_n$ (\textit{i.e.} $P(\sigma)=\mathbb{P}\{ \Sigma=\sigma \}$ for any $\sigma\in \mathfrak{S}_n$). With respect to a certain metric $d(.,\; .)$ on $\mathfrak{S}_n$ (\textit{e.g.} the Kendall $\tau$ distance), a (true) median of distribution $P$ w.r.t. $d$ is any solution of the minimization problem:
\begin{equation}\label{eq:median_pb}
\min_{\sigma \in \mathfrak{S}_n}L_P(\sigma),
\end{equation}
where $L_P(\sigma)=\mathbb{E}_{\Sigma \sim P}[d(\Sigma,\sigma)  ]
$ denotes the expected distance between any permutation $\sigma$ and $\Sigma$ and shall be referred to as the \textit{risk} of the median candidate $\sigma$ throughout the thesis.
The objective pursued is to recover approximately a solution $\sigma^*$ of this minimization problem, plus an estimate of this minimum $L_P^*=L_P(\sigma^*)$, as accurate as possible, based on the observations $\Sigma_1,\; \ldots,\; \Sigma_N$. The minimization problem \eqref{eq:median_pb} always has a solution since the cardinality of $\mathfrak{S}_n$ is finite (however exploding with $n$) but can be multimodal, see Section \ref{sec:optimality}. A median permutation $\sigma^*$ can be interpreted as a central value for $P$, a crucial \textit{location parameter}, whereas the quantity $L_P^*$ can be viewed as a dispersion measure.
However, the functional $L_P(.)$ is unknown in practice, just like distribution $P$. When there is no ambiguity on the distribution considered, we write $L(.)$ for $L_P(.)$, and $L*$ for $L_P^*$ in this chapter. We only have access to the dataset $\{\Sigma_1,\; \ldots,\; \Sigma_N  \}$ to find a reasonable approximant of a median and would like to avoid rigid assumptions on $P$ such as those stipulated by the Mallows model, see \citet{Mallows57} and Remark \ref{Rk:Mallows}. Following the Empirical Risk Minimization (ERM) paradigm \citep[see \textit{e.g.}][]{Vapnik}, one replaces the quantity $L(\sigma)$ by a statistical version based on the sampling data, typically the unbiased estimator
\begin{equation}\label{eq:emp_risk}
\widehat{L}_N(\sigma)=\frac{1}{N}\sum_{i=1}^Nd(\Sigma_i,\sigma).
\end{equation}
Notice that $\widehat{L}_N=L_{\widehat{P}_N}$ where $\widehat{P}_N=1/N \sum_{t=1}^N \delta_{\Sigma_t}$ is the empirical distribution. It is the goal of the subsequent analysis to assess the performance of solutions $\widehat{\sigma}_N$ of
\begin{equation}\label{eq:ERM1}
 \min_{\sigma\in \mathfrak{S}_n}\widehat{L}_N(\sigma),
 \end{equation}
 by establishing (minimax) bounds for the excess of risk $L(\widehat{\sigma}_N)-L^*$ in probability/expectation, when $d$ is the Kendall's $\tau$ distance. In this case, any solution of problem \eqref{eq:median_pb} (resp., of problem \eqref{eq:ERM1}) is called a \textit{Kemeny median} (resp., an \textit{empirical Kemeny median}) throughout the thesis. 
%or a possibly 'smoothed' and/or penalized variant $\widetilde{L}_N(\sigma)$ (a smoothing, convexification, relaxation recipe should permit to make optimization easier, while penalization may guarantee certain desirable properties of the solution, such as sparsity in the euclidian setting).
\begin{remark} {\sc (Alternative dispersion measure)}\label{rk:dispersion_measure_bis}
 An alternative measure of dispersion which can be more easily estimated than $L^*=L(\sigma^*)$ is given by
\begin{equation}\label{eq:disp_meas2}
\gamma(P)=\frac{1}{2}\mathbb{E}[ d(\Sigma,\Sigma') ],
\end{equation}
where $\Sigma'$ is an independent copy of $\Sigma$. One may easily show that $\gamma(P)\leq L^*\leq 2\gamma(P)$. The estimator of \eqref{eq:disp_meas2} with minimum variance among all unbiased estimators is given by the $U$-statistic
\begin{equation}\label{eq:emp_disp_meas2}
\widehat{\gamma}_N=\frac{2}{N(N-1)}\sum_{i<j}d(\Sigma_i,\Sigma_j).
\end{equation}
In addition, we point out that confidence intervals for the parameter $\gamma(P)$ can be constructed by means of Hoeffding/Bernstein type deviation inequalities for $U$-statistics and a direct (smoothed) bootstrap procedure can be applied for this purpose, see \cite{Lahiri93}. In contrast, a bootstrap technique for building CI's for $L^*$ would require to solve several times an empirical version of \eqref{eq:median_pb} based on bootstrap samples.
%\begin{itemize}
% 	\item A validity framework for bootstrapping the $U$-statistic \eqref{eq:emp_disp_meas2} may require to implement a smoothing procedure (see \cite{Lahiri93}), {\bf find a bootstrap technique and conditions guaranteeing its asymptotic validity}.
 	%\item By means of the triangular inequality, we have $\gamma(P)\leq L^*$ and, by definition, $\mathbb{E}[d(\sigma^*,\Sigma)]\leq \mathbb{E}[d(\Sigma',\Sigma)\mid \Sigma'  ]$ almost-surely, so that $L^*\leq 2\gamma(P)$ by taking the expectation w.r.t. to $\Sigma'$. Considering the analogy with the Hilbertian case, {\bf find conditions ensuring that \eqref{eq:disp_meas2} is an even more accurate approximant of $L^*$}.
 %\end{itemize}
 \end{remark}
 
 
\begin{remark}\label{rk:alt_setup}{\sc (Alternative framework)} Since the computation of Kendall's $\tau$ distance involves pairwise comparisons only, one could compute empirical versions of the risk functional $L$ in a statistical framework stipulating that the observations are less complete than $\{\Sigma_1,\; \ldots,\; \Sigma_N  \}$ and formed by i.i.d. pairs $\{(\mathbf{e}_k,\; \epsilon_k),\;  k=1,\; \ldots,\; N\}$, where the $\mathbf{e}_k=(\i_k,\j_k)$'s are independent from the $\Sigma_k$'s  and drawn from an unknown distribution $\nu$ on the set $\mathcal{E}_n$ such that $\nu(e)>0$ for all $e\in \mathcal{E}_n$ and $\epsilon_k=sgn( \Sigma_k(\j_k)- \Sigma_k(\i_k) )$ with $\mathbf{e}_k=(\i_k,\j_k)$ for $1\leq k\leq N$. Based on these observations, an estimate of the risk $\mathbb{E}_{\nu}\mathbb{E}_{\Sigma \sim P}[\mathbb{I}\{ \e=(i,j),\;  \epsilon(\sigma(j)-\sigma(i))<0\}]$ of any median candidate $\sigma\in \mathfrak{S}_n$ is given by:
\begin{equation*}
\sum_{i<j}\frac{1}{N_{i,j}}\sum_{k=1}^N\mathbb{I}\{ \e_k=(i,j),\;  \epsilon_k(\sigma(j)-\sigma(i))<0 \},
\end{equation*}
where $N_{i,j}=\sum_{k=1}^N\mathbb{I}\{ \e_k=(i,j)\}$, see for instance \citet{LB14a} or \citet{rajkumar2014statistical} for ranking aggregation results in this setting.
\end{remark}
  
  
\subsection{Connection to Voting Rules}
\label{subsec:voting-rules}
  
In Social Choice, we have a collection of votes under the form of rankings $\DN= \left(\sigma_{1},\; \ldots,\; \sigma_{N}\right)$. Such a collection of votes $\DN \in \Sn^N$ is called a \textit{profile} and a voting rule, which outputs a consensus ranking on this profile, is classically defined as follows:
\begin{equation*}
\sigma_{P_N}= \argmin_{\sigma \in \Sn} g(\sigma,\DN)
\end{equation*}
where $g : \Sn \times \bigcup_{t=1}^{\infty} \Sn^t \rightarrow \mathbb{R}$. This definition can be easily translated in order to be applied to any given distribution $P$ instead of a profile. Indeed, the authors of \cite{PPR15} define a \textit{distributional rank aggregation procedure} as follows:
\begin{equation*}
\sigma_P= \argmin_{\sigma \in \Sn} g(\sigma,P)
\end{equation*}
where $g : \Sn \times \mathcal{P}_n \rightarrow \mathbb{R}$ where $\mathcal{P}_n$ is the set of all distributions
on $\Sn$. Many classic aggregation procedures are naturally extended through this definition and thus to our statistical framework, as we have seen for Kemeny ranking aggregation previously. To detail some examples, we denote by $p_{i,j}=\mathbb{P}\{\Sigma(i)<\Sigma(j)  \}=1-p_{j,i}$ for $1\leq i\neq j\leq n$ and define the associated empirical estimator by $\widehat{p}_{i,j}=(1/N)\sum_{m=1}^N\mathbb{I}\{  \Sigma_m(i)<\Sigma_m(j)\}$. The Copeland method \citep{Copeland51} consists on $\DN$ in ranking the items by decreasing order of their Copeland score, calculated for each one as the number of items it beats in pairwise duels minus the number of items it looses against: $s_{N}(i) = \sum_{k\neq i}\mathbb{I}\{\widehat{p}_{i,k} \leq 1/2\} - \mathbb{I}\{\widehat{p}_{i,k} > 1/2\}$. It thus naturally applies to a distribution $P$ using the scores $s(i) = \sum_{k\neq i}\mathbb{I}\{p_{i,k} \leq 1/2\} - \mathbb{I}\{p_{i,k} > 1/2\}$. Similarly, Borda aggregation \citep{Borda} which consists in ranking items in increasing order of their score $s_{N}(i)=\sum_{m=1}^{N}\Sigma_m(i)$ when applied on $P_{N}$, naturally extends to $P$ using the scores $s(i)=\mathbb{E}_{P}[ \Sigma(i)]$.

  