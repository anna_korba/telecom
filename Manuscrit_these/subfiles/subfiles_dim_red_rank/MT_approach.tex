
\subsection{A Mass Transportation Approach to Dimensionality Reduction on $\Sn$}
We now develop a framework, that is shown to extend consensus ranking, for \textit{dimensionality reduction} fully tailored to ranking data exhibiting a specific type of \textit{sparsity}. For this purpose, we consider  the so-termed \textit{mass
transportation} approach to defining metrics on the set of probability distributions on $\mathfrak{S}_n$ as follows, see \cite{Rachev91} (incidentally, this approach is also used in \cite{CJ10} to introduce a specific relaxation of the consensus ranking problem).

\begin{definition}
Let $d:\Sn^2\rightarrow \mathbb{R}_+$ be a metric on $\Sn$ and $q\geq 1$. The $q$-th Wasserstein metric with $d$ as cost function between two probability distributions $P$ and $P'$ on $\mathfrak{S}_n$ is given by:
\begin{equation} \label{eq:metric}
W_{d,q}\left(P,P'  \right)=\inf_{\Sigma\sim P,\; \Sigma' \sim P' }\mathbb{E}\left[ d^q(\Sigma,\Sigma') \right],
\end{equation}
where the infimum is taken over all possible couplings\footnote{Recall that a coupling of two probability distributions $Q$ and $Q'$ is a pair $(U,U')$ of random variables defined on the same probability space such that the marginal distributions of $U$ and $U'$ are $Q$ and $Q'$.} $(\Sigma,\Sigma')$ of $(P,P')$.
\end{definition}
% Observe that the quantity $W_{d_1,1}$ (respectively $W_{d_2,2}$) is the classic $L_1$-Mallows distance (respectively, $L_2$-Mallows distance).
As revealed by the following result, when the cost function $d$ is equal to the Kendall's $\tau$ distance, which case the subsequent analysis focuses on, the Wasserstein metric is bounded by below by the $l_1$ distance between the pairwise probabilities.

\begin{lemma}\label{lem:Kendall} For any probability distributions $P$ and $P'$ on $\mathfrak{S}_n$:
\begin{equation}\label{eq:lower}
W_{d_{\tau},1}\left(P,P'  \right)\geq \sum_{i<j}\vert p_{i,j}-p'_{i,j} \vert.
\end{equation}
The equality holds true when the distribution $P'$ is deterministic (\textit{i.e.} when $\exists \sigma\in \Sn$ s.t. $P'=\delta_{\sigma}$).
\end{lemma}
The proof of Lemma \ref{lem:Kendall} as well as discussions on alternative cost functions (the Spearman $\rho$ distance) are deferred to section~\ref{sec:proofs_dim_red_rank}. As shown below, \eqref{eq:lower} is actually an equality for various distributions $P'$ built from $P$ that are of special interest regarding dimensionality reduction.\\

%\noindent{\bf Connection with Consensus Ranking.} Observe that, equipped with this notation, we have: $\forall \sigma\in \mathfrak{S}_n$, $L_{P}(\sigma)=W_{d_{\tau},1}\left(P,\delta_{\sigma}  \right)$. Hence, medians $\sigma^*$ of a probability distribution $P$ (\textit{i.e.} solutions of \eqref{eq:median_pb}) correspond to the Dirac distributions $\delta_{\sigma^*}$ closest to $P$ in the sense of the Wasserstein metric \eqref{eq:metric}. Whereas the space of probability measures on $\Sn$ is of explosive dimension $n!-1$, consensus ranking can be thus somehow viewed as a radical dimension reduction technique, where the original distribution is summarized by a median permutation $\sigma^*$.

\noindent {\bf Sparsity and Bucket Orders.} Here, we propose a way of describing a distribution $P$ on $\Sn$, originally described by $n!-1$ parameters, by finding a much simpler distribution that approximates $P$ in the sense of the Wasserstein metric introduced above under specific assumptions, extending somehow the consensus ranking concept.
Let $2 \leq K\leq n$ and $\C=(\C_1,\; \ldots,\; \C_K)$ be a \textit{bucket order} of $\n$ with $K$ buckets. In order to gain insight into the rationale behind the approach we promote, observe that a distribution $P'$ can be naturally said to be \textit{sparse} if, for all $1\leq k<l\leq K$ and all $(i,j)\in \C_k\times \C_l$ (i.e. $i \prec_{\C} j $), we have $p'_{j,i}=0$, which means that with probability one $\Sigma'(i)<\Sigma'(j)$, when $\Sigma' \sim P'$. In other words, the relative order of two items belonging to two different buckets is deterministic. Throughout the paper, such a probability distribution is referred to as a \textit{bucket distribution} associated to $\C$. Since the variability of a bucket distribution corresponds to the variability of its marginals within the buckets $\C_k$'s, the set $\P_{\C}$ of all bucket distributions associated to $\C$ is of dimension $d_{\C}=\prod_{k\leq K}\#\C_k!-1\leq n!-1$.
A best summary in $\mathbf{P}_{\C}$ of a distribution $P$ on $\Sn$, in the sense of the Wasserstein metric \eqref{eq:metric}, is then given by any solution $P^*_{\C}$ of the minimization problem
\begin{equation}\label{eq:min_transp}
\min_{P'\in \mathbf{P}_{\C}}W_{d_{\tau},1}(P,P').
\end{equation}
Set $\Lambda_{P}(\C)=\min_{P'\in \mathbf{P}_{\C}}W_{d_{\tau},1}(P,P')$ for any bucket order $\C$.\\

\begin{sloppypar}
\noindent {\bf Dimensionality Reduction.} Let $K\leq n$. We denote by $\mathbf{C}_K$ the set of all bucket orders $\C$ of $\n$ with $K$ buckets. If $P$ can be accurately approximated by a probability distribution associated to a bucket order with $K$ buckets, a natural dimensionality reduction approach consists in finding a solution $\C^{*(K)}$ of
 \begin{equation}\label{eq:best_bucket}
\min_{\C\in \mathbf{C}_K}\Lambda_{P}(\C),
\end{equation}
as well as a solution $P^*_{\C^{*(K)}}$ of
\eqref{eq:min_transp} for $\C=\C^{*(K)}$ and a coupling $(\Sigma,\Sigma_{\C^{*(K)}})$ s.t. $\mathbb{E}[d_{\tau}(\Sigma,\Sigma_{\C^{*(K)}})]=\Lambda_{P}(\C^{*(K)})$.\\
\end{sloppypar}

\noindent{\bf Connection with Consensus Ranking.}
Observe that $\cup_{\C\in \mathbf{C}_n}\mathbf{P}_{\C}$ is the set of all Dirac distributions $\delta_{\sigma}$, $\sigma\in \Sn$. Hence, in the case $K=n$, dimensionality reduction as formulated above boils down to solve Kemeny consensus ranking. Indeed, we have:
$\forall \sigma\in \mathfrak{S}_n$, $W_{d_{\tau},1}\left(P,\delta_{\sigma}  \right)=L_{P}(\sigma)$.
Hence, medians $\sigma^*$ of a probability distribution $P$ (\textit{i.e.} solutions of \eqref{eq:median_pb}) correspond to the Dirac distributions $\delta_{\sigma^*}$ closest to $P$ in the sense of the Wasserstein metric \eqref{eq:metric}: $P^*_{\C^{*(n)}}=\delta_{\sigma^*}$ and $\Sigma_{\C^{*(n)}}=\sigma^*$.
Whereas the space of probability measures on $\Sn$ is of explosive dimension $n!-1$, consensus ranking can be thus somehow viewed as a radical dimension reduction technique, where the original distribution is summarized
by a median permutation $\sigma^*$. In constrast, the other extreme case $K=1$ corresponds to no dimensionality reduction at all, \textit{i.e.} $\Sigma_{\C^{*(1)}}=\Sigma$.

\subsection{Optimal Couplings and Minimal Distortion}
Fix a bucket order $\C=(\C_1,\; \ldots,\; \C_K)$. A simple way of building a distribution in $\mathbf{P}_{\C}$ based on $P$ consists in considering the random ranking $\Sigma_{\C}$ coupled with $\Sigma$, that ranks the elements of any bucket $\C_k$ in the same order as $\Sigma$ and whose distribution $P_{\C}$ belongs to $\mathbf{P}_{\C}$:
\begin{equation}
\forall k\in\{1,\; \ldots,\; K  \},\; \forall i\in\C_k,\;\; \Sigma_{\C}(i)=1+\sum_{l<k}\#\C_l+\sum_{j\in \C_k}\mathbb{I}\{\Sigma(j)<\Sigma(i) \},
\end{equation}
which defines a permutation. Distributions $P$ and $P_{\C}$ share the same marginals within the $\C_k$'s and thus have the same intra-bucket pairwise probabilities $(p_{i,j})_{(i,j)\in \mathcal{C}_k^2}$, for all $k\in\{1,\dots,K\}$. Observe that the expected Kendall $\tau$ distance between $\Sigma$ and $\Sigma_{\C}$ is given by:
\begin{equation}\label{eq:expect_dist_coupling}
\mathbb{E}\left[d_{\tau}\left(\Sigma,\Sigma_{\C} \right)\right]=\sum_{i\prec_{\C}j}p_{j,i}=\sum_{1\leq k<l\leq K}\sum_{(i,j)\in \C_k\times \C_l}p_{j,i},
\end{equation}
which can be interpreted as the expected number of pairs for which $\Sigma$ violates the (partial) strict order defined by the bucket order $\mathcal{C}$. The result stated below shows that $(\Sigma,\Sigma_{\C})$ is \textit{optimal} among all couplings between $P$ and distributions in $\mathbf{P}_{\C}$ in the sense where \eqref{eq:expect_dist_coupling} is equal to the minimum of~\eqref{eq:min_transp}, namely $\Lambda_{P}(\C)$.
\begin{proposition}\label{prop:kendall_prop}
Let $P$ be any distribution on $\Sn$. For any bucket order $\C=(\C_1,\; \ldots,\; \C_K)$, we have:
\begin{equation}\label{eq:mt_criterion}
\Lambda_{P}(\C)=\sum_{i\prec_{\C}j}p_{j,i}.
\end{equation}
\end{proposition}
The proof, given in section~\ref{sec:proofs_dim_red_rank}, reveals that \eqref{eq:lower} in Lemma \ref{lem:Kendall} is actually an equality when $P'=P_{\C}$
and that $W_{d_{\tau},1}\left(P,P_{\C}  \right)=\mathbb{E}\left[d_{\tau}\left(\Sigma,\Sigma_{\C} \right)\right]$. Attention must be paid that it is quite remarkable that, when the Kendall $\tau$ distance is chosen as cost function, the distortion measure introduced admits a simple closed-analytical form, depending on elementary marginals solely, the pairwise probabilities namely. Hence, the distortion of any bucket order can be straightforwardly estimated from independent copies of $\Sigma$, opening up to the design of practical dimensionality reduction techniques based on empirical distortion minimization, as investigated in the next section.
The case where the cost is the Spearman $\rho$ distance is also discussed in section~\ref{sec:proofs_dim_red_rank}: it is worth noticing that, in this situation as well,  the distortion can be expressed in a simple manner, as a function of triplet-wise probabilities namely.

\begin{property}\label{prop:comp}
Let $P$ be stochastically transitive. A bucket order $\C=(\C_1,\; \ldots,\; \C_K)$ is said to agree with Kemeny consensus iff we have: $i \prec_{\C} j$ (i.e. $\exists k<l$, $(i,j)\in \C_k\times \C_l$) $\Rightarrow p_{j,i}\leq 1/2$. % $\forall 1\leq k<l\leq K$, $\forall (i,j)\in \C_k\times \C_l$, $p_{j,i}\leq 1/2$.
\end{property}
As recalled in the previous subsection, the quantity $L^*_P$ can be viewed as a natural dispersion measure of distribution $P$ and can be expressed as a function of the $p_{i,j}$'s as soon as $P$ is stochastically transitive. The remarkable result stated below shows that, in this case and for any bucket order $\C$ satisfying Property \ref{prop:comp}, $P$'s dispersion can be decomposed as the sum of the (reduced) dispersion of the simplified distribution $P_{\C}$ and the minimum distortion $\Lambda_P(\C)$.

\begin{corollary}
\label{cor:median_lambda}
Suppose that $P$ is stochastically transitive. Then, for any bucket order $\C$ that agrees with Kemeny consensus, we have:
\begin{equation}
L^*_{P}=L^*_{P_{\C}}+\Lambda_P(\C).
\end{equation}
\end{corollary}
In the case where $P$ is strictly stochastically transitive, the Kemeny median $\sigma^*_{P}$ of $P$ is unique (see \cite{CKS17}). If $\C$ fulfills Property \ref{prop:comp}, it is also obviously the Kemeny median of the bucket distribution $P_{\C}$.
As shall be seen in the next section, when $P$ fulfills a strong version of the stochastic transitivity property, optimal bucket orders $\C^{*(K)}$ necessarily agree with the Kemeny consensus, which may greatly facilitates their statistical recovery.

\subsection{Related Work}

The dimensionality reduction approach developed in this paper is connected with
the \textit{optimal bucket order} (OBO) problem considered in the literature, see \textit{e.g.} \cite{aledo2017utopia}, \cite{aledo2018approaching}, \cite{feng2008discovering}, \cite{gionis2006algorithms}, \cite{ukkonen2009randomized}. Given the pairwise probabilities ($p_{i,j})_{1 \le i\ne j\le n}$ of a distribution $P$ over $\Sn$, solving the OBO problem consists in finding a bucket order $\C=(\C_1,\; \ldots,\; \C_K)$ that minimizes the following cost:
\begin{equation}\label{eq:OBO2}
\widetilde{\Lambda}_P(\C) = \sum_{i \ne j} |p_{i,j} - \widetilde{p}_{i,j}|,
\end{equation}
%where $\widetilde{p}_{i,j}$ are the pairwise marginals of $\widetilde{P}_{\C}$ and verify:
where $\widetilde{p}_{i,j}=1$ if $i \prec_{\C} j$, $\widetilde{p}_{i,j}=0$ if $j \prec_{\C} i$ and $\widetilde{p}_{i,j}=1/2$ if $i \sim_{\C}j$.
%This implies that the intra-bucket pairwise probabilities $(p_{i,j})_{(i,j) \in \C_k}$ for $1 \le k\le K$ are set to 1/2, when $\widetilde{\Sigma}\sim\widetilde{P}_{\C}$
In other words, the $\widetilde{p}_{i,j}$'s are the pairwise marginals of the bucket distribution $\widetilde{P}_{\C}$ related to $\C$
with independent and uniformly distributed partial rankings $\Pi_{\C_k}(\widetilde{\Sigma}_{\C})$'s for $\widetilde{\Sigma}_{\C}\sim\widetilde{P}_{\C}$.
%One may easily check that the quantity~\eqref{eq:OBO2} is actually equal to twice the distortion $W_{d_{\tau},1}(P,\widetilde{P}_{\C})$, and that:
Moreover, this cost verifies:
\begin{equation}\label{eq:OBO}
\widetilde{\Lambda}_P(\C) =  2\Lambda_P(\C) + \sum_{k=1}^K\sum_{(i,j)\in \C_k^2}\vert p_{i,j}-1/2\vert.
\end{equation}
Observe that solving the OBO problem is much more restrictive than the framework we developed, insofar as no constraint is set about the intra-bucket marginals of the summary distributions solutions of \eqref{eq:best_bucket}.
Another related work is documented in \cite{shah2016feeling, pananjady2017worst} and develops the concept of \textit{indifference sets}. Formally, a family of pairwise probabilities $(\widetilde{p}_{i,j})$ is said to satisfy the indifference set partition (or bucket order) $\C$ when:
\begin{equation}\label{eq:indifference_set_cdt}
\widetilde{p}_{i,j}=\widetilde{p}_{i',j'} \text{ for all quadruples } (i,j,i',j') \text{ such that } i \sim_{\C} i' \text{ and } j \sim_{\C} j',
\end{equation}
which condition also implies that the intra-bucket marginals are s.t. $\widetilde{p}_{i,j}=1/2$ for $i \sim_{\C} j$ (take $i'=j$ and $j'=i$ in~\eqref{eq:indifference_set_cdt}).
Though related, our approach significantly differs from these works, since it avoids stipulating arbitrary distributional assumptions. For instance, it permits in contrast to test \textit{a posteriori}, once the best bucket order $\C^{*(K)}$ is determined for a fixed $K$, statistical hypotheses such as the independence of the bucket marginal components (\textit{i.e.} $\Pi_{\C_k^{*(K)}}(\Sigma)$'s ) or the uniformity of certain bucket marginal distributions. % According to our experience of ranking data processing,
A summary distribution, often very informative and of small dimension both at the same time, is the marginal of the first bucket $\C_1^{*(K)}$ (the top-$m$ rankings where $m=|\C_1^{*(K)}|$).
