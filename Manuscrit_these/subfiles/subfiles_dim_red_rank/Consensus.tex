\subsection{Consensus Ranking - A Statistical Learning View}

Given a collection of $N\geq 1$ rankings $\sigma_{1},\; \ldots,\; \sigma_{N}$, consensus ranking, also referred to as ranking aggregation, aims at finding a ranking $\sigma^*\in \Sn$ that best summarizes it. A popular way of tackling this problem, the metric-based consensus approach, consists in solving:
\begin{equation}
	\label{eq:ranking_aggregation}
	\min_{\sigma\in \mathfrak{S}_n}\sum_{s=1}^N d(\sigma,\sigma_{s}),
\end{equation}
where $d(.,\; .)$ is a certain metric on $\Sn$. As the set $\Sn$ is of finite cardinality, though not necessarily unique, such a barycentric permutation, called \textit{consensus/median ranking}, always exists.
In Kemeny ranking aggregation, the most widely documented version in the literature, one considers the number of pairwise disagreements as metric, namely the Kendall's $\tau$ distance, see \cite{Kemeny59}:
\begin{equation}\label{eq:Kendall_tau}
\forall (\sigma,\sigma')\in \mathfrak{S}_n^2,\;\; d_{\tau}(\sigma,\sigma')=\sum_{i<j}\mathbb{I}\{(\sigma(i)-\sigma(j)) (\sigma'(i)-\sigma'(j))<0\}.
\end{equation}
\begin{remark}
Many other distances are considered in the literature (see \textit{e.g.} Chapter 11 in \cite{Deza}). In particular, the following distances, originally introduced in the context of nonparametric hypothesis testing, are also widely used.
\begin{itemize}
\item[$\bullet$] {\bf The Spearman $\rho$ distance.} $\forall (\sigma,\sigma')\in \mathfrak{S}_n^2$,
$
d_2(\sigma,\sigma')=\left(\sum_{i=1}^n\left(\sigma(i)-\sigma'(i)
  \right)^2\right)^{1/2}$
\item[$\bullet$] {\bf The Spearman footrule distance.} $\forall (\sigma,\sigma')\in
  \mathfrak{S}_n^2$,
$d_1(\sigma,\sigma')=\sum_{i=1}^n\left\vert\sigma(i)-\sigma'(i)
\right\vert$
\item[$\bullet$] {\bf The Hamming distance.} $\forall (\sigma,\sigma')\in
\mathfrak{S}_n^2$,
$d_H(\sigma,\sigma')=\sum_{i=1}^n\mathbb{I}\{ \sigma(i)\ne \sigma'(i)\}$
\end{itemize}
\end{remark}

%Such a consensus has many interesting properties, but is NP-hard to compute. Various algorithms have been proposed in the literature to compute acceptably good solutions in a reasonable amount of time, their description is beyond the scope of the paper, see for example \cite{AM12} and the references therein.
The problem \eqref{eq:ranking_aggregation} can be viewed as a $M$-estimation problem in the probabilistic framework stipulating that  the collection of rankings to be aggregated/summarized is composed of $N\geq 1$ independent copies $\Sigma_1,\; \ldots,\; \Sigma_N$ of a generic r.v. $\Sigma$, defined on a probability space $(\Omega,\; \mathcal{F},\; \mathbb{P})$ and drawn from an unknown probability distribution $P$ on $\mathfrak{S}_n$ (\textit{i.e.} $P(\sigma)=\mathbb{P}\{ \Sigma=\sigma \}$ for any $\sigma\in \mathfrak{S}_n$). Just like a median of a real valued r.v. $Z$ is any scalar closest to $Z$ in the $L_1$ sense, a (true) median of distribution $P$ w.r.t. a certain metric $d$ on $\mathfrak{S}_n$ is any solution of the minimization problem:
\begin{equation}\label{eq:median_pb}
\min_{\sigma \in \mathfrak{S}_n}L_P(\sigma),
\end{equation}
where $L_P(\sigma)=\mathbb{E}_{\Sigma \sim P}[d(\Sigma,\sigma)  ]
$ denotes the expected distance between any permutation $\sigma$ and $\Sigma$. In this framework, statistical ranking aggregation consists in recovering a solution $\sigma^*$ of this minimization problem, plus an estimate of this minimum $L^*_P=L_P(\sigma^*)$, as accurate as possible, based on the observations $\Sigma_1,\; \ldots,\; \Sigma_N$. A median permutation $\sigma^*$ can be interpreted as a central value for distribution $P$, while the quantity $L^*_P$ may be viewed as a dispertion measure.
Like problem \eqref{eq:ranking_aggregation}, the minimization problem \eqref{eq:median_pb} has always a solution but can be multimodal.
However, the functional $L_P(.)$ is unknown in practice, just like distribution $P$. Suppose that we would like to avoid rigid parametric assumptions on $P$ and only have access to the dataset $(\Sigma_1,\; \ldots,\; \Sigma_N )$ to find a reasonable approximant of a median. Following the Empirical Risk Minimization (ERM) paradigm \cite{Vapnik}, one substitutes in \eqref{eq:median_pb} the quantity $L_P(\sigma)$ with its statistical version
\begin{equation}\label{eq:emp_risk}
\widehat{L}_N(\sigma)=\frac{1}{N}\sum_{s=1}^Nd(\Sigma_s,\sigma)=L_{\widehat{P}_N}(\sigma),
\end{equation}
where $\widehat{P}_N=(1/N)\sum_{s=1}^N\delta_{\Sigma_s}$ denotes the empirical measure.
The performance of empirical consensus rules, solutions $\widehat{\sigma}_N$ of
 $\min_{\sigma\in \mathfrak{S}_n}\widehat{L}_N(\sigma)$,
 has been investigated in \cite{CKS17}.  Precisely, rate bounds of order $O_{\mathbb{P}}(1/\sqrt{N})$ for the excess of risk $L_P(\widehat{\sigma}_N)-L^*_P$ in probability/expectation have been established and proved to be sharp in the minimax sense, when $d$ is the Kendall's $\tau$ distance. Whereas problem \eqref{eq:ranking_aggregation} is NP-hard in general (see \textit{e.g.} \cite{Hudry08}), in the Kendall's $\tau$ case, exact solutions, referred to as \textit{Kemeny medians}, can be explicited when the pairwise probabilities $p_{i,j}=\mathbb{P}\{  \Sigma(i)<\Sigma(j)\}$, $1\leq i\neq j\leq n$ (so $p_{i,j}+p_{j,i}=1$), fulfill the following property, referred to as \textit{stochastic transitivity}.
 \begin{definition}\label{def:stoch_trans} Let $P$ be a probability distribution on $\mathfrak{S}_n$.
 \begin{itemize}
 \item[(i)] Distribution $P$ is said to be (weakly) stochastically transitive iff
 $$
\forall (i,j,k)\in \n^3:\;\;  p_{i,j}\geq 1/2 \text{ and } p_{j,k}\geq 1/2 \; \Rightarrow\; p_{i,k}\geq 1/2.
 $$
 If, in addition, $p_{i,j}\neq 1/2$ for all $i<j$, one says that $P$ is strictly stochastically transitive.
 \item[(ii)] Distribution $P$ is said to be strongly stochastically transitive iff
 $$
 \forall (i,j,k)\in \n^3:\;\;  p_{i,j}\geq 1/2 \text{ and } p_{j,k}\geq 1/2 \; \Rightarrow\; p_{i,k}\geq max(p_{i,j}, p_{j,k}).
 $$ which is equivalent to the following condition (see \cite{davidson1959experimental}):
 $$
\forall (i,j)\in \n^2:\;\;  p_{i,j}\geq 1/2 \; \Rightarrow\; p_{i,k}\geq p_{j,k} \text{ for all } k\in\n\setminus\{i,j\}.
 $$
 \end{itemize}
 \end{definition}
These conditions were firstly introduced in the psychology literature (\cite{fishburn1973binary}, \cite{davidson1959experimental}) and were used recently for the estimation of pairwise probabilities and ranking from pairwise comparisons (\cite{shah2015stochastically}, \cite{shah2015simple}). When stochastic transitivity holds true, the set of Kemeny medians (see Theorem 5 in \cite{CKS17}) is the set
$\{\sigma\in \mathfrak{S}_n:\; (p_{i,j}-1/2)(\sigma(j)-\sigma(i ))>0 \text{ for all } i<j \text{ s.t. } p_{i,j}\neq 1/2  \}$, and
 the minimum is given by
 \begin{equation}\label{eq:inf}
 L^*_P=\sum_{i<j}\min\{p_{i,j},1-p_{i,j}  \}=\sum_{i<j}\{  1/2-\vert  p_{i,j}-1/2\vert\}.
 \end{equation}
 The quantity \eqref{eq:inf}, the expected Kendall's $\tau$ distance between the random permutation $\Sigma$ and one of its Kemeny medians, provides a natural measure of the variability of distribution $P$.
  %and, for any $\sigma\in \mathfrak{S}_n$,
 %$
 %L_P(\sigma)-L^*_P=2\sum_{i<j}\left\vert p_{i,j}-1/2 \right\vert\cdot \mathbb{I}\{ (\sigma(i)-\sigma(j))\left(p_{i,j}-1/2\right)<0\}.
 %$
 If a strict version of stochastic transitivity is fulfilled, we denote by $\sigma^*_P$ the Kemeny median which is unique and given by the Copeland ranking, which assigns for each $i$ its rank as:
 \begin{equation}\label{eq:sol_SST}
 \sigma^*_P(i)=1+\sum_{j\neq i}\mathbb{I}\{p_{i,j}<1/2  \} \text{ for } 1\leq i\leq n.
 \end{equation}
 %and the minimum expected Kendall $\tau$ distance is equal to
 %\begin{equation}\label{eq:inf}
 %L^*_P=L_P(\sigma_P^*)=\sum_{i<j}\min\{p_{i,j},1-p_{i,j}  \}=\sum_{i<j}\left\{ 1/2-\left\vert  p_{i,j}-1/2\right\vert\right\}.
 %\end{equation}
 Recall also that examples of stochastically transitive distributions on $\mathfrak{S}_n$ are numerous and include most popular parametric models such as Mallows or Bradley-Terry-Luce-Plackett models, see \textit{e.g.} \cite{Mallows57} or \cite{Plackett75}.
 %\begin{remark}\label{rk:dispersion}{\sc (Measuring dispersion)} As noticed in \cite{CKS17}, an alternative measure of dispersion is given by
 % $\gamma(P)=(1/2)\mathbb{E}[ d(\Sigma,\Sigma') ]$,
 % where $\Sigma'$ is an independent copy of $\Sigma$. When $P$ is not strictly stochastically transitive, the latter can be much more easily estimated than $L_P^*$, insofar as no (approximate) median computation is needed: indeed, a natural estimator is given by the $U$-statistic
 %$\widehat{\gamma}_N=2/(N(N-1))\sum_{i<j}d(\Sigma_i,\Sigma_j)$. For this reason, this empirical dispersion measure will be used as a splitting criterion in the partitioning algorithm proposed in subsection \ref{subsec:algo}. Observe in addition that
 % $\gamma(P)\leq L^*_P\leq 2\gamma(P)$ and, when $d=d_{\tau}$, we have $\gamma(P)=\sum_{i<j}p_{i,j}(1-p_{i,j})$.
 %\end{remark}
Assume that the underlying distribution $P$ is strictly stochastically transitive and verifies a certain low-noise condition {\bf NA}$(h)$, defined for $h>0$ by:
\begin{equation}\label{eq:hyp_margin0}
 \min_{i<j}\left\vert p_{i,j}-1/2 \right\vert \ge h.
\end{equation}
This condition is checked in many situations, including most conditional parametric models (see Remark~13 in \cite{CKS17}) under simple assumptions on their parameters. It may be considered as analogous to that introduced in \cite{KB05} in binary classification, and was used to prove fast rates also in ranking, for the estimation of the matrix of pairwise probabilities (see  \cite{shah2015stochastically}) or ranking aggregation (see \cite{CKS17}). Indeed it is shown in \cite{CKS17} that under condition \eqref{eq:hyp_margin0}, the empirical distribution $\widehat{P}_N$ is also strictly stochastically transitive with overwhelming probability, and that the expectation of the excess of risk of empirical Kemeny medians decays at an exponential rate, see Proposition 14 therein. In this case, the nearly optimal solution $\sigma^*_{\widehat{P}_N}$ can be made explicit and straightforwardly computed using Eq. \eqref{eq:sol_SST} based on the empirical pairwise probabilities
$\widehat{p}_{i,j}=\frac{1}{N}\sum_{s=1}^N\mathbb{I}\{ \Sigma_s(i)<\Sigma_s(j)  \}$.
%\begin{equation}\label{eq:pair_prob}
%\widehat{p}_{i,j}=\frac{1}{N}\sum_{k=1}^N\mathbb{I}\{ \Sigma_k(i)<\Sigma_k(j)  \}, \; 1\le i\neq j\le n.
%\end{equation}
 %Otherwise, solving the NP-hard problem $\min_{\sigma \in \mathfrak{S}_n}L_{\widehat{P}_N}(\sigma)$ requires to get an empirical Kemeny median. However, as can be seen by examining the argument of Proposition 14's proof in \cite{CKS17} (which exhibits that the probability of $P$ not being transitive decreases exponentially), the exponential rate bound holds true for any candidate $\widetilde{\sigma}_N$ in $\mathfrak{S}_n$ such that  $\widetilde{\sigma}_N=\sigma^*_{\widehat{P}_N}$ when the empirical distribution is strictly stochastically transitive, and $\widetilde{\sigma}_N=\sigma$ with $\sigma \in \Sn$ arbitrarly chosen otherwise.
