\section{Empirical Distortion Minimization - Rate Bounds and Model Selection}\label{sec:learning}
\begin{sloppypar}
In order to recover optimal bucket orders, based on the observation of a training sample $\Sigma_1,\; \ldots,\; \Sigma_N$ of independent copies of $\Sigma$,
Empirical Risk Minimization, the major paradigm of statistical learning, naturally suggests to consider bucket orders $\C=(\C_1,\; \ldots,\; \C_K)$ minimizing the empirical version of the distortion \eqref{eq:mt_criterion}
\begin{equation}\label{eq:emp_distort}
\widehat{\Lambda}_N(\C)=\sum_{i\prec_{\C}j}\widehat{p}_{j,i}=\Lambda_{\widehat{P}_N}(\C),
\end{equation}
where the $\widehat{p}_{i,j}$'s are the pairwise probabilities of the empirical distribution.
For a given shape $\lambda$, we define the Rademacher average
$$
\mathcal{R}_N(\lambda)=\mathbb{E}_{\epsilon_1,\dots,\epsilon_N}\left[\max_{\C\in\mathbf{C}_{K,\lambda}}\frac{1}{N}\left\vert \sum_{s=1}^N\epsilon_s \sum_{i\prec_{\C} j}\mathbb{I}\{\Sigma_s(j)<\Sigma_s(i)\} \right\vert\right],
$$
where $\epsilon_1,\; \ldots,\; \epsilon_N$ are i.i.d. Rademacher r.v.'s (\textit{i.e.} symmetric sign random variables), independent from the $\Sigma_s$'s.
Fix the number of buckets $K\in\{1,\; \ldots,\; n\}$, as well as the bucket order shape $\lambda=(\lambda_1,\; \ldots,\; \lambda_K)\in \mathbb{N}^{*K}$ such that $\sum_{k=1}^K\lambda_k=n$. We recall that $\mathbf{C}_K=\cup_{\lambda'=(\lambda'_1,\dots,\lambda'_K) \in \mathbb{N}^{*K} \text{ s.t. } \sum_{k=1}^K\lambda'_k=n} \mathbf{C}_{K,\lambda'}$. The result stated below describes the generalization capacity of solutions of the minimization problem
\begin{equation}\label{eq:EDM}
\min_{\C\in \mathbf{C}_{K,\lambda}}\widehat{\Lambda}_N(\C),
\end{equation}
over the class $\mathbf{C}_{K,\lambda}$ of bucket orders $\C= (\mathcal{C}_1, \dots, \mathcal{C}_K)$ of shape $\lambda$ (\textit{i.e.} s.t. $\lambda=(\#\mathcal{C}_1, \dots, \#\mathcal{C}_K)$), through a rate bound for their excess of distortion. Its proof is given in section~\ref{sec:proofs_dim_red_rank}.
\begin{theorem}\label{thm:EDM}
Let $\widehat{C}_{K,\lambda}$ be any empirical distortion minimizer over $\mathbf{C}_{K,\lambda}$, i.e solution of~\eqref{eq:EDM}. Then, for all $\delta\in (0,1)$, we have with probability at least $1-\delta$:
%$$
%\Lambda_P(\widehat{C}_{K,\lambda})-\inf_{\C\in \mathbf{C}_K}\Lambda_P(\C)\leq 2\kappa(\lambda)\sqrt{\frac{\log\left(2\binom{n}{\lambda}\kappa(\lambda)/\delta\right)}{2N}}+\left\{ \inf_{\C\in \mathbf{C}_{K,\lambda}}\Lambda_P(\C)-\inf_{\C\in \mathbf{C}_{K}}\Lambda_P(\C)\right\},
%$$
%where $\kappa(\lambda)=\sum_{k=1}^{K-1}\lambda_k\times (n-\lambda_1-\ldots-\lambda_k)$, $\binom{n}{\lambda}=n!/(\# \C_1 ! \times \cdots \times \# \C_K !) = \# \mathbf{C}_{K,\lambda}$.
$$
\Lambda_P(\widehat{C}_{K,\lambda})-\inf_{\C\in \mathbf{C}_K}\Lambda_P(\C) \le 4\mathbb{E}\left[\mathcal{R}_N(\lambda)\right] + \kappa(\lambda)\sqrt{\frac{2\log(\frac{1}{\delta})}{N}} +\left\{ \inf_{\C\in \mathbf{C}_{K,\lambda}}\Lambda_P(\C)-\inf_{\C\in \mathbf{C}_{K}}\Lambda_P(\C)\right\},
$$
where $\kappa(\lambda)=\sum_{k=1}^{K-1}\lambda_k\times (n-\lambda_1-\ldots-\lambda_k)$.
\end{theorem}
We point out that the Rademacher average is of order $O(1/\sqrt{N})$:
$\mathcal{R}_N(\lambda)\le \kappa(\lambda)\sqrt{2\log\left(\binom{n}{\lambda}\right)/N}$ with $\binom{n}{\lambda}=n!/(\# \C_1 ! \times \cdots \times \# \C_K !) = \# \mathbf{C}_{K,\lambda}$, where $\kappa(\lambda)$ is the number of terms involved in~\eqref{eq:mt_criterion}-\eqref{eq:emp_distort} and $\binom{n}{\lambda}$ is the multinomial coefficient, \textit{i.e.} the number of bucket orders of shape $\lambda$.
Putting aside the approximation error, the rate of decay of the distortion excess is classically of order $O_{\mathbb{P}}(1/\sqrt{N})$.
\begin{sloppypar}
\begin{remark}{\sc (Empirical Distortion Minimization over $\mathbf{C}_{K}$)} We point out that rate bounds describing the generalization ability of minimizers of \eqref{eq:emp_distort}
over the whole class $\mathbf{C}_{K}$ can be obtained using a similar argument. A slight modification of Theorem \ref{thm:EDM}'s proof shows that, with probability larger than $1-\delta$, their excess of distortion is less than $n^2(K-~1)/K\sqrt{\log(n^2(K-1)\#\mathbf{C}_{K}/(K\delta))/(2N)}$.
Indeed, denoting by $\lambda_{\C}$ the shape of any bucket order $\C$ in $\mathbf{C}_K$, $\max_{\C\in \mathbf{C}_K}\kappa(\lambda_{\C})\le n^2(K-1)/(2K)$, the upper bound being attained when $K$ divides $n$ for $\lambda_1=\dots=\lambda_K=n/K$.
In addition, we have: $\#\mathbf{C}_{K}=\sum_{k=0}^K(-1)^{K-k}\binom{K}{k}k^n$.
\end{remark}
\end{sloppypar}
\begin{sloppypar}
\begin{remark}\label{rk:alt_setup}{\sc (Alternative statistical framework)} Since the distortion \eqref{eq:mt_criterion}  involves pairwise comparisons solely, an empirical version could be computed in a statistical framework stipulating that the observations are of pairwise nature, $(\mathbb{I}\{\Sigma_1(i_1)<\Sigma_1(j_1)\},\; \ldots,\; \mathbb{I}\{\Sigma_N(i_N)<\Sigma_N(j_N)\})$, where $\{(i_s,\; j_s),\;  s=1,\; \ldots,\; N\}$, are i.i.d. pairs, independent from the $\Sigma_s$'s, drawn from an unknown distribution $\nu$ on the set $\{(i,j):\; 1\leq i<j\leq n\}$ such that $\nu(\{(i,j)\})>0$ for all $i<j$. Based on these observations, more easily available in most practical applications (see e.g. \cite{chen2013pairwise}, \cite{park2015preference}), the pairwise probability $p_{i,j}$, $i<j$, can be estimated by:
\begin{equation*}
\frac{1}{N_{i,j}}\sum_{s=1}^N\mathbb{I}\{ (i_s,j_s)=(i,j),\;  \Sigma_s(i_s)<\Sigma_s(j_s) \},
\end{equation*}
with $N_{i,j}=\sum_{s=1}^N\mathbb{I}\{ (i_s,j_s)=(i,j)\}$ and the convention $0/0=0$.
\end{remark}
\end{sloppypar}
\noindent {\bf Selecting the shape of the bucket order.} A crucial issue in dimensionality reduction is to determine the dimension of the simpler representation of the distribution of interest.  Here we consider a complexity regularization method to select the bucket order shape $\lambda$ that uses a data-driven penalty based on Rademacher averages. %avoids to have recourse to resampling techniques and
Suppose that a sequence $\{(K_m,\lambda_m)\}_{1\le m\le M}$ of bucket order sizes/shapes is given (observe that $M\le \sum_{K=1}^n \binom{n-1}{K-1}=2^{n-1}$). In order to avoid overfitting, consider the complexity penalty given by
\begin{equation}\label{eq:penalty}
%{\sc pen}(n,\lambda)=\frac{\kappa(\lambda)}{\sqrt{N}}\sqrt{2\log\left(2\binom{n}{\lambda}\right)}
%{\sc pen}(n,m)=\frac{\kappa(\lambda_m)}{\sqrt{N}}\sqrt{2\log\left(2\binom{n}{\lambda_m}\right)} + \sqrt{\frac{\log(m)}{N}}
{\sc pen}(\lambda_m,N)=2\mathcal{R}_N(\lambda_m)
\end{equation}
and the minimizer $\widehat{\C}_{K_{\widehat{m}}, \lambda_{\widehat{m}}}$ of the penalized empirical distortion, with
\begin{equation}\label{eq:EDM_penalty}
\widehat{m}=\argmin_{1\le m\le M}\left\{\widehat{\Lambda}_N(\widehat{\C}_{K_m,\lambda_m})+ {\sc pen}(\lambda_m,N)\right\} \text{ and } \widehat{\Lambda}_N(\widehat{\C}_{K,\lambda})=\min_{\C\in \mathbf{C}_{K,\lambda}} \widehat{\Lambda}_N(\C).
\end{equation}
The next result shows that the bucket order thus selected nearly achieves the performance that would be obtained with the help of an oracle, revealing the value of the index $m$ ruling the bucket order size/shape that minimizes $\mathbb{E}[\Lambda_P(\widehat{\C}_{K_m,\lambda_m})]$.
%The proof essentially relies on a distribution-free upper bound for $\mathbb{E}[\max_{\C\in \mathbf{C}_{K,\lambda}}\vert\widehat{\Lambda}_N(\C)-\Lambda_P(\C)\vert]$ and is given in the Supplementary Material.

\begin{theorem} \label{thm:select}{\sc (An oracle inequality)} Let $\widehat{\C}_{K_{\widehat{m}}, \lambda_{\widehat{m}}}$ be any penalized empirical distortion minimizer over $\mathbf{C}_{K_{\widehat{m}},\lambda_{\widehat{m}}}$, i.e solution of~\eqref{eq:EDM_penalty}. Then, for all $\delta\in (0,1)$, we have with probability at least $1-\delta$:
\begin{equation*}
%\mathbb{E}\left[\Lambda_P(\widehat{\C}_{K_{\widehat{m}},\lambda_{\widehat{m}}})\right]\leq \min_{m\geq 1}\left\{\min_{\C\in \mathbf{C}_{K_m,\lambda_m}}\Lambda_P(\C)+\frac{\kappa(\lambda_m)}{\sqrt{N}}\sqrt{2\log\left(2\binom{n}{\lambda_m}\right)}   +\sqrt{\frac{\log(m)}{N}}  \right\}+\sqrt{\frac{1}{2N}}.
\mathbb{E}\left[\Lambda_P(\widehat{\C}_{K_{\widehat{m}},\lambda_{\widehat{m}}})\right]\leq \min_{1\le m\le M}\left\{\min_{\C\in \mathbf{C}_{K_m,\lambda_m}}\Lambda_P(\C)+ 2\mathbb{E}\left[\mathcal{R}_N(\lambda_m)\right] \right\}+ 5M \binom{n}{2} \sqrt{\frac{\pi}{2N}}.
\end{equation*}
\end{theorem}

\noindent {\bf The Strong Stochastic Transitive Case.}
The theorem below shows that, when strong/strict stochastic transitivity properties hold for the considered distribution $P$,  optimal buckets are those which agree with the Kemeny median.
\begin{theorem}
\label{thm:bucket_median}
Suppose that $P$ is strongly/strictly stochastically transitive. Let $K\in\{1,\; \ldots,\; n  \}$ and $\lambda=(\lambda_1,\; \ldots,\; \lambda_K)$ be a given bucket size and shape. Then, the minimizer of the distortion $\Lambda_{P}(\C)$ over $\mathbf{C}_{K,\lambda}$ is unique and given by  $\C^{*(K,\lambda)}=(\C^{*(K,\lambda)}_1,\; \ldots,\; \C^{*(K,\lambda)}_K)$, where
\begin{equation}\label{eq:opt_bucket}
\C^{*(K,\lambda)}_{k}=\left\{i\in \n: \;\; \sum_{l<k} \lambda_l< \sigma^*_P(i)\leq  \sum_{l\leq k} \lambda_l \right\} \text{ for } k\in\{1,\; \ldots,\; K\}.
\end{equation}
In addition, for any $\C\in \mathbf{C}_{K,\lambda}$, we have:
\begin{equation}\label{eq:excess_distort}
\Lambda_{P}(\C)-\Lambda_P(\C^{*(K,\lambda)})\geq 2\sum_{j\prec_{\C}i}( 1/2-p_{i,j}) \cdot \mathbb{I}\{ p_{i,j}<1/2 \}.
\end{equation}
%Let $\C=(\C_1,\; \ldots,\; \C_K)$ be a bucket order of $\n$ with $K$ buckets that does not satisfy Property \ref{prop:comp}. Then,
%$$
%\Lambda_{P}(\C^{*})<\Lambda_{P}(\C),
%$$
%where $\C^{*(K,\lambda)}=(\C^{*(K,\lambda)}_1,\; \ldots,\; \C^{*(K,\lambda)}_K)$ is the bucket order defined by
%$$
%\C^{*(K,\lambda)}_{k}=\left\{i\in \n: \;\; \sum_{l<k}\# \C_l< \sigma^*_P(i)\leq  \sum_{l\leq k}\# \C_l \right\} \text{ for } k\in\{1,\; \ldots,\; K\}.
%$$
\end{theorem}
%This result can be explained as follows. We recall that under strict stochastic transitivity, the Kemeny median $\sigma^*_P$ is unique and given by the Copeland ranking (see section~\ref{sec:background}). Then, the strongly stochastic transitivity is required to prove that the criterion $\Lambda_{P}(\C)$ is minimized by the unique bucket order in $\mathbf{C}_{K,\lambda}$ satisfying Property~\ref{prop:comp}, refer to the Supplementary Material for the detailed proof.
%If the hypotheses of Theorem \ref{thm:bucket_median} and condition \eqref{eq:hyp_margin0} are fulfilled, when $\widehat{P}_N$ is strictly stochastically transitive (which then happens with overwhelming probability, see Proposition 14 in \cite{CKS17}), the computation of $\sigma^*_{\widehat{P}_N}$ is immediate, as well as that of $\widehat{C}_{K,\lambda}$, plugging the empirical Kemeny median into \eqref{eq:opt_bucket}.
In other words, $\C^{*(K,\lambda)}$ is the unique bucket in $\mathbf{C}_{K,\lambda}$ that agrees with $\sigma^*_P$ (\textit{cf} Property \ref{prop:comp}).
Hence, still under the hypotheses of Theorem \ref{thm:bucket_median}, the minimizer $\C^{*(K)}$ of \eqref{eq:best_bucket} also agrees with $\sigma^*_P$
and corresponds to one of the $\binom{n-1}{K-1}$ possible segmentations of the ordered list $(\sigma_P^{*-1}(1),\dots,\sigma_P^{*-1}(n))$ into $K$ segments. This property paves the way
to design efficient algorithms for recovering bucket order representations with a fixed distortion rate of minimal dimension, avoiding to specify the size/shape in advance, see section~\ref{sec:appendix_dim_red_rank} for further details. If, in addition, a low-noise condition for $h>0$: 
\begin{equation}\label{eq:low_noise}
\min_{i<j}\vert p_{i,j}-1/2 \vert\geq h
\end{equation}
is verified by $P$, then $\widehat{P}_N$ is strictly stochastically transitive (which then happens with overwhelming probability (see Proposition~\ref{prop:low_noise} in Chapter~\ref{chap:stat_fram}), the computation of the empirical Kemeny median $\sigma^*_{\widehat{P}_N}$ is immediate from formula \eqref{eq:copeland_formula} (replacing $P$ by $\widehat{P}_N$), as well as an estimate of $\C^{*(K,\lambda)}$, plugging $\sigma^*_{\widehat{P}_N}$ into \eqref{eq:opt_bucket} as implemented in the experiments below. When the empirical distribution $\widehat{P}_N$ is not stochastically transitive, which happens with negligible probability, the empirical median can be classically replaced by any permutation obtained from the Copeland score by breaking ties at random.
The following result shows that, in the strict/strong stochastic transitive case, when the low-noise condition $\textbf{NA}(h)$ is fulfilled, the excess of distortion of the empirical minimizers is actually of order $O_{\mathbb{P}}(1/N)$.

\begin{theorem}\label{thm:fast}{(\sc Fast rates)}
Let $\lambda$ be a given bucket order shape and $\widehat{C}_{K,\lambda}$ any empirical distortion minimizer over $\mathbf{C}_{K,\lambda}$. Suppose that $P$ is strictly/strongly stochastically transitive and fulfills condition \eqref{eq:low_noise}. Then, for any $\delta>0$, we have with probability $1-\delta$:
$$
\Lambda_P(\widehat{\C}_{K,\lambda})-\Lambda_P(\C^{*(K,\lambda)})\leq \left(\frac{2^{\binom{n}{2}+1}n^2}{h}\right) \times \frac{\log\left( \binom{n}{\lambda}/\delta \right)}{N}.
$$
\end{theorem}
The proof is given section~\ref{sec:proofs_dim_red_rank}.
\end{sloppypar}
%In the following numerical experiments, we apply the empirical version of this procedure, \textit{i.e.} using $\sigma^*_{\widehat{P}_N}$ instead of $\sigma^*_P$,
%to compute, for given bucket size $K$, the $\binom{n-1}{K-1}$ estimators of $\C^{*(K,\lambda)}$.
%%Indeed, for fixed size $K$ and shape $\lambda$, a natural estimator of $\C^{*(K,\lambda)}$ is the one agreeing (in the sense of Property~\ref{prop:comp}) with the empirical Kemeny median $\sigma^*_{\widehat{P}_N}$.
%%It is simply obtained by splitting the ordered list $(\sigma_{\widehat{P}_N}^{*-1}(1),\dots,\sigma_{\widehat{P}_N}^{*-1}(n))$ into $K$ buckets with shape $\lambda$.
%%More generally, given if no shape is specified, there are $\binom{n-1}{K-1}$ different combinations.
%When $P\in \mathcal{T}$, it follows from the result above that the solution of \eqref{eq:best_bucket} can be obtained in two steps, as follows:
%\begin{enumerate}
%\item sort the elements of $\n$ according to $\sigma^*_P$: $\sigma_P^{*-1}(1)\succ \cdots \succ \sigma_P^{*-1}(n)$,
%\item segment the list thus sorted into $K$ buckets, \textit{i.e.} find $1\leq i_1<i_2<\ldots<i_{K-1}\leq n$ so as to minimize:
%\begin{equation}\label{eq:criterion}
%\lambda_P(i_1,\; \ldots,\; i_{K-1})=\sum_{k=1}^{i_K}\sum_{i=1+i_{k-1}}^{i_k}\sum_{j=i_{k+1}}^n p_{j,i},
%\end{equation}
%with $i_0=0$ and $i_K=n$ by convention.
%\end{enumerate}
%Denoting by $(i_1^*,\; \ldots,\; i_{K-1}^*)$ the sequence minimizing \eqref{eq:criterion}, we finally get the optimal bucket order $\C^{*(K)}$ with buckets given by
%$$
%\C^{*(K)}_k=\left\{i\in \n: \;\; i_{k-1}< \sigma^*_P(i)\leq  i_k\right\},
%$$
%for $k\in\{1,\; \ldots,\; K\}$.
