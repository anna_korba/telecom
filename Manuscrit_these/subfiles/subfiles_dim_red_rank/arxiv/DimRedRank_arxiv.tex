\documentclass[twoside,11pt]{article}
\usepackage{jmlr2ee}
\usepackage[utf8]{inputenc}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{stmaryrd}
\usepackage{ragged2e}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{epsfig}
\usepackage{color}
\usepackage{dsfont}
\usepackage{hyperref}

\DeclareMathOperator*{\argmin}{argmin}
\def\n{\llbracket n \rrbracket}
\newcommand{\DN}{\mathcal{D}_{N}}
\newcommand{\Sn}{\mathfrak{S}_n}
\newcommand{\An}{\mathcal{A}_n}
\def\e{\mathbf e}
\def\i{\mathbf i}
\def\I{\mathcal I}
\def\C{\mathcal C}
\def\j{\mathbf j}
\newcommand{\un}{\mathds{1}}
\usepackage[dvipsnames]{xcolor}
\def\R{\mathbb{R}}
\def\Rd{\R^d}
\def\E{\mathbb E}
\def\EXP{{\E}}
\def\expec{{\EXP}}
\def\PROB{{\mathbb P}}
\def\var{{\rm Var}}
\def\shat{{\mathbb S}}
\def\IND#1{{\mathbb I}_{{\left[ #1 \right]}}}
\def\pr{\PROB}
\def\prob{\PROB}
\def\wh{\widehat}
\def\ol{\overline}
\newcommand{\deq}{\stackrel{\scriptscriptstyle\triangle}{=}}
\newcommand{\defeq}{\stackrel{\rm def}{=}}
\def\isdef{\defeq}
\newcommand{\xp}{\mbox{$\{X_i\}$}}
\def\diam{\mathop{\rm diam}}
\def\argmax{\mathop{\rm arg\, max}}
\def\argmin{\mathop{\rm arg\, min}}
\def\essinf{\mathop{\rm ess\, inf}}
\def\esssup{\mathop{\rm ess\, sup}}
\def\supp{\mathop{\rm supp}}
\def\cent{\mathop{\rm cent}}
\def\dim{\mathop{\rm dim}}
\def\sgn{\mathop{\rm sgn}}
\def\logit{\mathop{\rm logit}}
\def\proof{\medskip \par \noindent{\sc proof.}\ }
\def\proofsketch{\medskip \par \noindent{\sc Sketch of proof.}\ }
\def\blackslug{\hbox{\hskip 1pt \vrule width 4pt height 8pt depth 1.5pt
\hskip 1pt}}
\def\qed{\quad\blackslug\lower 8.5pt\null\par}
\newcommand{\COND}{\bigg\vert} % for conditional expectations
\def\C{{\cal C}}
\def\P{\mathbf P}
\def\B{{\cal B}}
\def\A{{\cal A}}
\def\N{{\cal N}}
\def\F{{\cal F}}
\def\L{{\cal L}}
\def\eps{{\varepsilon}}
\def\Var{{\rm Var}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\rset}{\RR}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\EE}[1]{\mathbb{E}\left\{ #1 \right\}}
\newcommand{\Prob}[1]{\mathbb{P}\left\{ #1 \right\} }
\newcommand{\PP}{\Prob}
\newcommand{\given}[1][{}]{\;\middle\vert\;{#1} }
\newcommand{\point}{\,\cdot\,}
\newcommand{\ud}{\,\mathrm{d}}

\newtheorem{property}{{\bf Property}}

%commentaires
\usepackage[textwidth=4.cm, textsize=tiny]{todonotes}
\newcommand{\anna}[1]{\todo[color=red!20]{{\bf Anna:} #1}}



\title{Dimensionality Reduction and (Bucket) Ranking: \\
a Mass Transportation Approach}

\author{\name{Mastane Achab} \email{mastane.achab@telecom-paristech.fr }
	\AND
	\name{Anna Korba} \email{anna.korba@telecom-paristech.fr}
	\AND
	\name{Stephan Cl\'emen\c{c}on} \email{stephan.clemencon@telecom-paristech.fr}\\
	\addr LTCI, T\'el\'ecom ParisTech, Paris, France\\
}


\begin{document}


\maketitle

\begin{abstract}
Whereas most dimensionality reduction techniques (\textit{e.g.} PCA, ICA, NMF) for multivariate data essentially rely on linear algebra to a certain extent, summarizing ranking data, viewed as realizations of a random permutation $\Sigma$ on a set of items indexed by $i\in \{1,\ldots,\; n\}$,  is a great statistical challenge, due to the absence of vector space structure for the set of permutations $\mathfrak{S}_n$. It is the goal of this article to develop an original framework for possibly reducing the number of parameters required to describe the distribution of a statistical population composed of rankings/permutations, on the premise that the collection of items under study can be partitioned into subsets/buckets, such that, with high probability, items in a certain bucket are either all ranked higher or else all ranked lower than items in another bucket. In this context, $\Sigma$'s distribution can be hopefully represented in a sparse manner by a \textit{bucket distribution}, \textit{i.e.} a bucket ordering plus the ranking distributions within each bucket. More precisely, we introduce a dedicated distortion measure, based on a mass transportation metric, in order to quantify the accuracy of such representations. The performance of buckets minimizing an empirical version of the distortion is investigated through a rate bound analysis. Complexity penalization techniques are also considered to select the shape of a bucket order with minimum expected distortion. Beyond theoretical concepts and results, numerical experiments on real ranking data are displayed in order to provide empirical evidence of the relevance of the approach promoted.
\end{abstract}

%\begin{keywords}
%Integral approximation, exponential inequalities, Kernel estimation, Monte-Carlo method, projection method, rate bounds, statistical sampling, $U$-statistics
%\end{keywords}


\input{Introduction}

\input{Preliminaries}

\input{Learning}

\input{Experiments}

\input{Conclusion}

\section*{Acknowledgments}
This work was supported by the industrial chair \textit{Machine Learning for Big Data} from T\'el\'ecom ParisTech
and by a public grant (\textit{Investissement d'avenir} project, reference ANR-11-LABX-0056-LMH, LabEx LMH).

\bibliographystyle{plain}

\bibliography{biblio}

\newpage

\input{Appendix}

\end{document}
