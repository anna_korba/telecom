
\section{Appendix}\label{sec:appendix_dim_red_rank}

\subsection*{A - Hierarchical Recovery of a Bucket Distribution}\label{sec:dim_red_rank_algo}


Motivated by Theorem \ref{thm:bucket_median}, we propose a hierarchical 'bottom-up' procedure to recover, from ranking data, a bucket order representation (agreeing with Kemeny consensus) of smallest dimension for a fixed level of distortion, that does not requires to specify in advance the bucket size $K$ and thus avoids computing the optimum $\eqref{eq:opt_bucket}$ for all possible shape/size.

Suppose for simplicity that $P$ is strictly/strongly stochastically transitive. One starts with the bucket order of size $n$ defined by its Kemeny median $\sigma^*_P$:
$$
\C(0)=(\{\sigma_P^{*-1}(1)\},\; \dots,\; \{\sigma_P^{*-1}(n)\}).
$$
The initial representation has minimum dimension, \textit{i.e.} $d_{\C(0)}=0$, and maximal distortion among all bucket order representations agreeing with $\sigma^*_P$, \textit{i.e.} $\Lambda_P(\C(0))=L^*_P$, see Corollary \ref{cor:median_lambda}.
The binary agglomeration strategy we propose consists in recursively merging two adjacent buckets $\C_k(j)$ and $\C_{k+1}(j)$ of the current bucket order $\C(j)=(\C_1(j),\; \ldots,\; C_K(j)$ into a single bucket, yielding the 'coarser' bucket order
\begin{equation}
\label{eq:merged_bucket}
\C(j+1)=(\C_1(j), \dots, \C_{k-1}(j), \C_k(j)\cup\C_{k+1}(j), \C_{k+2}(j), \dots, \C_{K}(j)).
\end{equation}
The pair $(\C_k(j), \C_{k+1}(j))$ chosen corresponds to that maximizing the quantity
\begin{equation}
\label{eq:delta}
\Delta_P^{(k)}(\C(j)) = \sum_{i\in\C_k(j), j\in\C_{k+1}(j)} p_{j, i}.
\end{equation}
The agglomerative stage $\C(j) \to \C(j+1)$ increases the dimension of the representation,
\begin{equation}
\label{eq:merged_dim}
d_{\C(j+1)} = (d_{\C(j)}+1)\times\binom{\#\C_k(j)+\#\C_{k+1}(j)}{\#\C_k(j)}-1,
\end{equation}
while reducing the distortion by $\Lambda_P(\C(j))-\Lambda_P(\C(j+1))=\Delta_P^{(k)}(\C(j))$.

\medskip


\fbox{
	\begin{minipage}[t]{13.5cm}
		\medskip
		
		{\small
			\begin{center}
				{\sc Agglomerative Algorithm}
			\end{center}
			
			
			\begin{enumerate}
				\item {\bf Input.} Training data $\{\Sigma_i\}_{i=1}^N$, maximum dimension $d_\text{max}\ge 0$, distortion tolerance $\epsilon\ge 0$.
				\item {\bf Initialization.} Compute empirical Kemeny median $\sigma^*_{\widehat{P}_N}$ and $\C(0) = (\{\sigma_{\widehat{P}_N}^{*-1}(1)\}, \dots, \{\sigma_{\widehat{P}_N}^{*-1}(n)\}$. Set $K\leftarrow n$.
				\item {\bf Iterations.} While $K\ge 3$ and $\widehat{\Lambda}_N(\C(n-K))>\epsilon$,
				\begin{enumerate}
					\item Compute $k\in\argmax_{1\le l\le K-1}\Delta_{\widehat{P}_N}^{(l)}(\C(n-K))$ and $\C(n-K+1)$.
					\item If $d_{\C(n-K+1)}>d_\text{max}$: go to 4. Else: set $K\leftarrow K-1$.
				\end{enumerate}
				\item {\bf Output.} Bucket order $\C(n-K)$.
			\end{enumerate}
		}
		\medskip
		
	\end{minipage}
}
\medskip

This algorithm is specifically designed for finding the bucket order $\C$ of minimal dimension $d_\C$ (i.e. of maximal size $K$)
such that a bucket distribution in $\P_{\C}$ approximates well the original distribution $P$ (i.e. with small distortion $\Lambda_P(\C)$).
The next result formally supports this idea in the limit case of $P$ being a bucket distribution.

\begin{theorem}
	\label{thm:algo}
	Let $P$ be a strongly/strictly stochastically transitive bucket distribution and denote $K^*=\max\{K\in\{2,\dots,n\}, \exists \text{ bucket order } \C \text{ of size } K \text{ s.t. } P\in\P_{\C}\}$.\\
	\noindent $(i)$ There exists a unique $K^*$-shape $\lambda^*$ such that $\Lambda_P(\C^{*(K^*,\lambda^*)})=0$.\\
	\noindent $(ii)$ For any bucket order $\C$ such that $P\in\P_\C$: $\C\neq\C^{*(K^*,\lambda^*)} \Rightarrow d_\C>d_{\C^{*(K^*,\lambda^*)}}$.\\
	\noindent $(iii)$ The agglomerative algorithm, runned with $d_{\text{max}}=n!-1$, $\epsilon=0$ and theoretical quantities ($\sigma^*_P$, $\Delta_P^{(k)}$'s and $\Lambda_P$) instead of estimates,
	outputs $\C^{*(K^*,\lambda^*)}$.
\end{theorem}

\begin{proof}
	Straightforward if $K^*=n$: assume $K^*<n$ in the following.\\
	(i). Existence is ensured by definition of $K^*$ combined with Theorem \ref{thm:bucket_median}.
	Assume there exist two distinct $K^*$-shapes $\lambda$ and $\lambda'$ such that $\Lambda_P(\C^{*(K^*,\lambda)})=\Lambda_P(\C^{*(K^*,\lambda')})=0$.
	Necessarily, there exists $k\in\{1,\dots,K-1\}$ such that, for example, $\C^{*(K^*,\lambda)}_k \cap \C^{*(K^*,\lambda')}_{k+1} \neq \emptyset$ and $\C^{*(K^*,\lambda')}_{k+1} \nsubseteq \C^{*(K^*,\lambda)}_k$.
	Then, define a new bucket order $\widetilde{\C}$ of size $K^*+1$ as follows:
	\begin{multline*}
		\widetilde{\C}=\Biggl(\C^{*(K^*,\lambda')}_1,\dots,\C^{*(K^*,\lambda')}_k,\C^{*(K^*,\lambda)}_k\cap\C^{*(K^*,\lambda')}_{k+1},\\
		\C^{*(K^*,\lambda')}_{k+1}\setminus\left(\C^{*(K^*,\lambda)}_k\cap\C^{*(K^*,\lambda')}_{k+1}\right),\C^{*(K^*,\lambda')}_{k+2},\dots,\C^{*(K^*,\lambda')}_{K^*}\Biggr).
	\end{multline*}
	Conclude observing that $\Lambda_P(\widetilde{\C})=0$ i.e. $P\in\P_{\widetilde{\C}}$, which contradicts the definition of $K^*$.\\
	(ii). By Theorem \ref{thm:bucket_median}, any bucket order $\C$ such that $P\in\P_{\C}$ agrees with the Kemeny median.
	Then, observe that such bucket order $\C$ of size $K<K^*$ is obtained by iteratively merging adjacent buckets of $\C^{*(K^*,\lambda^*)}$:
	otherwise, following the proof of (i), we could define a new bucket order $\widetilde{\C}$ of size $K^*+1$ such that $P\in\P_{\widetilde{\C}}$.
	When $K=K^*-1$, Eq. (\ref{eq:merged_dim}) proves that $d_{\C}>d_{\C^{*(K^*,\lambda^*)}}$. The general result follows by induction.\\
	(iii). By induction on $n-K^*\in\{0,\dots,n-2\}$. Initialization is straightforward for $K^*=n$.
	Let $m\in\{3,\dots,n\}$ and assume that the proposition is true for any strongly/strictly stochastically transitive bucket distribution with $K^*=m$.
	Let $P$ be a strongly/strictly stochastically transitive bucket distribution with $K^*=m-1$.
	By definition of $K^*$, the algorithm runned with distribution $P$ cannot stop before computing $\C(n-m+1)$, which results from merging the adjacent buckets $\C_k(n-m)$ and $\C_{k+1}(n-m)$ (with $k\in\{1,\dots,m-1\}$).
	Then consider a distribution $\widetilde{P}$ with pairwise marginals $\widetilde{p}_{i,j}=1$ if $(i,j)\in\C_k(n-m)\times\C_{k+1}(n-m)$, $\widetilde{p}_{i,j}=0$ if $(i,j)\in\C_{k+1}(n-m)\times\C_k(n-m)$ and $\widetilde{p}_{i,j}=p_{i,j}$ otherwise.
	Hence, $\widetilde{P}$ is a strongly/strictly stochastically transitive bucket distribution and $\C(n-m)$ is, by construction of $\widetilde{P}$, returned by the algorithm when runned with distribution $\widetilde{P}$.
	Hence by induction hypothesis: $\widetilde{P}\in\P_{\C(n-m)}$.
	Conclude observing that $\Lambda_P(\C(n-m))=\Lambda_{\widetilde{P}}(\C(n-m))+\sum_{i\in\C_k(n-m), j\in\C_{k+1}(n-m)} p_{j, i} = \Delta_P^{(k)}(\C(n-m))$,
	which implies that $\Lambda_P(\C(n-m+1))=\Lambda_P(\C(n-m))-\Delta_P^{(k)}(\C(n-m))=0$.
\end{proof}




\subsection*{B - Experiments on toy datasets}

\begin{figure}[h!]
	\centering
	\includegraphics[width=.325\textwidth]{figures/true_bucket_distribution.pdf}
	\includegraphics[width=.325\textwidth]{figures/20_noisy_bucket_distribution.pdf}\hfill
	\includegraphics[width=.335\textwidth]{figures/50_noisy_bucket_distribution.pdf}\hfill
	\caption{Dimension-Distortion plot for different bucket sizes on simulated datasets.}
	\label{fig:simulated_data}
\end{figure}

\noindent We now provide an illustration of the notions we introduced in this paper, in particular of a bucket distribution and of our distortion criteria. For $n=6$ items, we fixed a bucket order $\C=(\C_1, \C_2, \C_3)$ of shape $\lambda=(2,3,1)$ and considered a bucket distribution $P\in\P_{\C}$. Specifically, $P$ is the uniform distribution over all the permutations extending the bucket order $\C$ and has thus its pairwise marginals such that $p_{j,i}=0$ as soon as $(i,j)\in \C_k \times \C_l$ with $k<l$. In Figure \ref{fig:simulated_data}, the first plot on the left is a scatterplot of all buckets of size $K\in\{2,3\}$ where for any bucket $\C'$ of size $K$, the horizontal axis is the distortion $\Lambda_{P}(\C')$ (see \eqref{eq:mt_criterion}) and the vertical axis is the dimension of $\P_{\C'}$ in log scale. On the left plot, one can see that one bucket of size $K=3$ attains a null distortion, i.e. when $\C'=\C$, and two buckets of size $K=2$ as well, i.e. when $\C'=(\C_1 \cup \C_2, \C_3)$ and when $\C'=(\C_1, \C_2 \cup \C_3)$. Then, a dataset of 2000 samples from $P$ was drawn, and for a certain part of the samples, a pair of items was randomly swapped within the sample. The middle and right plot thus represent the empirical distortions $\widehat{\Lambda}_N(\C')$ for any $\C'$ computed on these datasets, where respectively 20\% and 50\% of the samples were contaminated. One can notice that the datapoints shift more and more to the right, i.e. the distortion is increasing with the noise, still, the best bucket of size $3$ remains $\C'=\C$. However, the buckets $\C'$ attaining the minimum distortion in the noisy case are of size $2$, because the distortion involves a smaller number of terms $\kappa(\lambda_{\C'})$ for a smaller size.

\begin{figure}[h!]
	\centering
	\begin{tabular}{cc}
		\includegraphics[width=.325\textwidth]{figures/true_bucket_distribution_10_items.pdf} &
		\includegraphics[width=.325\textwidth]{figures/uniform_distribution_10_items.pdf}\hfill\\
		\includegraphics[width=.325\textwidth]{figures/true_bucket_distribution_20_items.pdf}&
		\includegraphics[width=.325\textwidth]{figures/uniform_distribution_20_items.pdf}\hfill
	\end{tabular}
	\caption{Dimension-Distortion plot for a true bucket distribution versus a uniform distribution ($n=10$ on top and $n=20$ below).}
	\label{fig:bucket_vs_uniform}
\end{figure}


\noindent We now perform a second experiment. We want to compare the distortion versus dimension graph for a true bucket distribution (i.e., for a collection of pairwise marginals that respect a bucket order) and for a uniform distribution (i.e., a collection of pairwise marginals where $p_{j,i}=0.5$ for all $i,j$). This corresponds to the plots on Figure~\ref{fig:bucket_vs_uniform}. One can notice that the points are much more spread for a true bucket distribution, since some buckets will attain a very low distortion (those who agree with the true one) while some have a high distortion. In contrast, for a uniform distribution, all the buckets will perform relatively in the same way, and the scatter plot is much more compact.


\subsection*{C - Mass Transportation for Other Distances}\label{sec:other_distances}

The approach developed in the chapter mainly relies on the choice of the Kendall's $\tau$ distance as cost function involved in the Wasserstein metric. We now investigate two other well-known distances for permutations, the Spearman $\rho$ distance and the Hamming distance (see section~\ref{sec:distances} Chapter~\ref{chap:background_ranking_data}).


\textbf{The Spearman $\rho$ case.} The following result shows that the alternative study based on the $2$-nd Wasserstein metric with the Spearman $\rho$ distance $d_2$ as cost function
would lead to a different distortion measure: $\Lambda'_{P}(\C)=\min_{P'\in \mathbf{P}_{\C}}W_{d_2,2}(P,P')$,
whose explicit formula, given by the right hand side of Eq. (\ref{eq:Wspearman}), writes in terms of the triplet-wise probabilities $p_{i,j,k}=\mathbb{P}_{\Sigma\sim P}\{\Sigma(i)<\Sigma(j)<\Sigma(k)\}$.
Moreover, the coupling $(\Sigma,\Sigma_{\C})$ is also optimal in this case as the distortion verifies $\Lambda'_{P}(\C) = \mathbb{E}\left[d_2^2\left(\Sigma,\Sigma_{\C} \right)\right]$.

\begin{lemma}\label{lem:squared_spearman} Let $n\ge 3$ and $P$ be a probability distribution on $\mathfrak{S}_n$.
	\begin{itemize}
		\item[(i).] For any probability distribution $P'$ on $\mathfrak{S}_n$:
		$$
		W_{d_2,2}\left(P,P'  \right)
		\ge \frac{2}{n-2}\sum_{a<b<c}\left\{ \sum_{(i,j,k)\in\sigma(a,b,c)} \max(p_{i,j,k}, p'_{i,j,k}) - 1 \right\},
		$$
		where $\sigma(a,b,c)$ is the set of permutations of triplet $(a,b,c)$.
		\item[(ii).] If $P' \in \mathbf{P}_{\C}$ with $\C$ a bucket order of $\n$ with $K$ buckets:
		\begin{equation}
		\label{eq:Wspearman}
		\begin{split}
		&W_{d_2,2}\left(P,P'  \right) \ge\\
		&\frac{2}{n-2} \sum_{1\le k<l<m\le K} \sum_{(a,b,c)\in\C_k\times\C_l\times\C_m} (n+1)p_{c,b,a}+n(p_{b,c,a}+p_{c,a,b})+p_{b,a,c}+p_{a,c,b}\\
		&+ \frac{2}{n-2} \sum_{1\le k<l\le K} \Biggl\{ \sum_{(a,b,c)\in\C_k\times\C_l\times\C_l} n(p_{b,c,a}+p_{c,b,a})+p_{b,a,c}+p_{c,a,b}\\
		&\qquad\qquad\qquad\qquad\qquad\qquad+\sum_{(a,b,c)\in\C_k\times\C_k\times\C_l} n(p_{c,a,b}+p_{c,b,a})+p_{a,c,b}+p_{b,c,a}\Biggr\},
		\end{split}
		\end{equation}
		with equality when $P'=P_{\C}$ is the distribution of $\Sigma_{\C}$.
	\end{itemize}
\end{lemma}

\begin{proof}
	
	\noindent{\bf (i).}
	Consider a coupling $(\Sigma,\Sigma')$ of two probability distributions $P$ and $P'$ on $\mathfrak{S}_n$.
	Define the triplet-wise probabilities $p_{i,j,k}=\mathbb{P}_{\Sigma\sim P}\{\Sigma(i)<\Sigma(j)<\Sigma(k)\}$ and $p'_{i,j,k}=~\mathbb{P}_{\Sigma'\sim P'}\{\Sigma'(i)<\Sigma'(j)<\Sigma'(k)\}$.
	For clarity's sake, we will assume that $\tilde{p}_{i,j,k} = \min(p_{i,j,k},p'_{i,j,k}) >0$ for all triplets $(i,j,k)$, the extension to the general case being straightforward.
	We also denote $\bar{p}_{i,j,k} = \max(p_{i,j,k},p'_{i,j,k})$.
	Given two pairs of three distinct elements of $\n$, $(i, j, k)$ and $(a, b, c)$, we define the following quantities:
	$$
	\pi_{a,b,c|i,j,k} = \mathbb{P}\left\{ \Sigma'(a)<\Sigma'(b)<\Sigma'(c)\mid \Sigma(i)<\Sigma(j)<\Sigma(k) \right\},
	$$
	$$
	\pi'_{a,b,c|i,j,k} = \mathbb{P}\left\{ \Sigma(a)<\Sigma(b)<\Sigma(c)\mid \Sigma'(i)<\Sigma'(j)<\Sigma'(k) \right\},
	$$
	$$
	\tilde{\pi}_{a,b,c|i,j,k} = \pi_{a,b,c|i,j,k}\mathbb{I}\{p_{i,j,k}\le p'_{i,j,k}\} + \pi'_{a,b,c|i,j,k}\mathbb{I}\{p_{i,j,k} > p'_{i,j,k}\},
	$$
	$$
	\bar{\pi}_{a,b,c|i,j,k} = \pi_{a,b,c|i,j,k}\mathbb{I}\{p_{i,j,k} > p'_{i,j,k}\} + \pi'_{a,b,c|i,j,k}\mathbb{I}\{p_{i,j,k} \le p'_{i,j,k}\}.
	$$
	The interest of defining the $\tilde{\pi}_{a,b,c|i,j,k}$'s is that it will allow us to choose $\tilde{\pi}_{i,j,k|i,j,k}=1$ at the end of the proof, which implies $\bar{\pi}_{i,j,k|i,j,k} = \frac{\tilde{p}_{i,j,k}}{\bar{p}_{i,j,k}}$.
	Throughout the proof, the triplets $(a, b, c)$ will always be permutations of $(i, j, k)$.
	Now write
	$$
	\mathbb{E}\left[ d_2\left( \Sigma,\Sigma' \right)^2\right] = \sum_{i=1}^n \E[\Sigma(i)^2] + \E[\Sigma'(i)^2] - 2 \E[\Sigma(i) \Sigma'(i)]\\,
	$$
	where
	$$
	\E[\Sigma(i)^2] = \E[(1+\sum_{j\neq i}\mathbb{I}\{\Sigma(j)<\Sigma(i)\})^2] = 1 + \sum_{j\neq i} (n+1) p_{j,i} - \sum_{k\neq i,j} p_{j,i,k}
	$$
	and
	\begin{equation*}
		\begin{split}
			\E[\Sigma(i) \Sigma'(i)] &= 1 + \sum_{j\neq i} p_{j,i}+p'_{j,i}+\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\} \\
			&+ \sum_{k\neq i,j} \mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\}.
		\end{split}
	\end{equation*}
	
	Hence,
	\begin{equation}
	\label{eq:spearman_triplets}
	\begin{split}
	\mathbb{E}\left[ d_2\left( \Sigma,\Sigma' \right)^2\right] =
	\sum_{a<b<c} \sum_{(i,j,k)\in \sigma(a,b,c)} & \frac{1}{n-2}\left\{(n-1) (p_{j,i}+p'_{j,i})-2\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\}\right\}\\
	&-p_{j,i,k}-p'_{j,i,k}-2\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\},
	\end{split}
	\end{equation}
	where $\sigma(a,b,c)$ is the set of the $6$ permutations of triplet $(a,b,c)$.
	Some terms simplify in Eq. (\ref{eq:spearman_triplets}) when summing over $\sigma(a,b,c)$, namely:
	$$
	\sum_{(i,j,k)\in \sigma(a,b,c)} \frac{n-1}{n-2}(p_{j,i}+p'_{j,i})-p_{j,i,k}-p'_{j,i,k} = \frac{4n-2}{n-2}.
	$$
	We now simply have:
	\begin{equation}
	\begin{split}
	\mathbb{E}\left[ d_2\left( \Sigma,\Sigma' \right)^2\right] =
	\sum_{a<b<c} \frac{4n-2}{n-2} - 2\sum_{(i,j,k)\in \sigma(a,b,c)} & \frac{1}{n-2}\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\}\\
	&+\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\}.
	\end{split}
	\end{equation}
	
	Observe that for all triplets $(a,b,c)$ and $(i,j,k)$,
	\begin{equation*}
		\begin{split}
			&\mathbb{P}(\Sigma'(a)<\Sigma'(b)<\Sigma'(c), \Sigma(i)<\Sigma(j)<\Sigma(k))
			+ \mathbb{P}(\Sigma'(i)<\Sigma'(j)<\Sigma'(k), \Sigma(a)<\Sigma(b)<\Sigma(c))\\
			&= \pi_{a,b,c|i,j,k}p_{i,j,k} + \pi'_{a,b,c|i,j,k}p'_{i,j,k}.
		\end{split}
	\end{equation*}
	Then, by the law of total probability, we have for all distinct $i,j,k$,
	\begin{equation*}
		\begin{split}
			&\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\}\\
			&= \frac{1}{2}\{\pi_{j,k,i|j,k,i}p_{j,k,i}+\pi'_{j,k,i|j,k,i}p'_{j,k,i}\}\\
			&+ \frac{1}{2}\{\pi_{k,j,i|k,j,i}p_{k,j,i}+\pi'_{k,j,i|k,j,i}p'_{k,j,i}\}\\
			&+ \frac{1}{2}\{\pi_{j,i,k|j,i,k}p_{j,i,k}+\pi'_{j,i,k|j,i,k}p'_{j,i,k}\}\\
			&+ \frac{1}{2}\{\pi_{j,i,k|j,k,i}p_{j,k,i}+\pi'_{j,i,k|j,k,i}p'_{j,k,i}+\pi_{j,k,i|j,i,k}p_{j,i,k}+\pi'_{j,k,i|j,i,k}p'_{j,i,k}\}\\
			&+ \frac{1}{2}\{\pi_{k,j,i|j,k,i}p_{j,k,i}+\pi'_{k,j,i|j,k,i}p'_{j,k,i}+\pi_{j,k,i|k,j,i}p_{k,j,i}+\pi'_{j,k,i|k,j,i}p'_{k,j,i}\}\\
			&+ \frac{1}{2}\{\pi_{j,i,k|k,j,i}p_{k,j,i}+\pi'_{j,i,k|k,j,i}p'_{k,j,i} + \pi_{k,j,i|j,i,k}p_{j,i,k}+\pi'_{k,j,i|j,i,k}p'_{j,i,k}\},
		\end{split}
	\end{equation*}
	and
	\begin{equation*}
		\begin{split}
			&\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\}\\
			&= \frac{1}{2}\{\pi_{j,k,i|j,k,i}p_{j,k,i} + \pi'_{j,k,i|j,k,i}p'_{j,k,i}\}\\
			&+ \frac{1}{2}\{\pi_{k,j,i|k,j,i} p_{k,j,i} + \pi'_{k,j,i|k,j,i} p'_{k,j,i}\}\\
			&+ \frac{1}{2}\{\pi_{k,j,i|j,k,i}p_{j,k,i} + \pi'_{k,j,i|j,k,i}p'_{j,k,i} + \pi_{j,k,i|k,j,i}p_{k,j,i} + \pi'_{j,k,i|k,j,i}p'_{k,j,i}\}\\
			&+ \mathbb{P}(\Sigma'(j)<\Sigma'(k)<\Sigma'(i), \Sigma(j)<\Sigma(i)<\Sigma(k))\\
			&+ \mathbb{P}(\Sigma'(k)<\Sigma'(i)<\Sigma'(j), \Sigma(j)<\Sigma(k)<\Sigma(i))\\
			&+ \mathbb{P}(\Sigma'(k)<\Sigma'(j)<\Sigma'(i), \Sigma(j)<\Sigma(i)<\Sigma(k))\\
			&+ \mathbb{P}(\Sigma'(k)<\Sigma'(i)<\Sigma'(j), \Sigma(k)<\Sigma(j)<\Sigma(i))\\
			&+ \mathbb{P}(\Sigma'(k)<\Sigma'(i)<\Sigma'(j), \Sigma(j)<\Sigma(i)<\Sigma(k)),
		\end{split}
	\end{equation*}
	which implies:
	\begin{equation*}
		\begin{split}
			&\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\} + \mathbb{P}\{\Sigma(k)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\}\\
			&= \pi_{j,k,i|j,k,i}p_{j,k,i} + \pi'_{j,k,i|j,k,i}p'_{j,k,i}\\
			&+ \pi_{k,j,i|k,j,i} p_{k,j,i} + \pi'_{k,j,i|k,j,i} p'_{k,j,i}\\
			&+ \pi_{k,j,i|j,k,i}p_{j,k,i} + \pi'_{k,j,i|j,k,i}p'_{j,k,i} + \pi_{j,k,i|k,j,i}p_{k,j,i} + \pi'_{j,k,i|k,j,i}p'_{k,j,i}\\
			&+ \frac{1}{2}\left\{\pi_{j,k,i|j,i,k}p_{j,i,k} + \pi'_{j,k,i|j,i,k}p'_{j,i,k} + \pi_{j,i,k|j,k,i}p_{j,k,i} + \pi'_{j,i,k|j,k,i}p'_{j,k,i}\right\}\\
			&+ \frac{1}{2}\left\{\pi_{k,i,j|j,k,i}p_{j,k,i} + \pi'_{k,i,j|j,k,i}p'_{j,k,i} + \pi_{j,k,i|k,i,j}p_{k,i,j} + \pi'_{j,k,i|k,i,j}p'_{k,i,j}\right\}\\
			&+ \frac{1}{2}\left\{\pi_{k,j,i|j,i,k}p_{j,i,k} + \pi'_{k,j,i|j,i,k}p'_{j,i,k} + \pi_{j,i,k|k,j,i}p_{k,j,i} + \pi'_{j,i,k|k,j,i}p'_{k,j,i}\right\}\\
			&+ \frac{1}{2}\left\{\pi_{k,i,j|k,j,i}p_{k,j,i} + \pi'_{k,i,j|k,j,i}p'_{k,j,i} + \pi_{k,j,i|k,i,j}p_{k,i,j} + \pi'_{k,j,i|k,i,j}p'_{k,i,j}\right\}\\
			&+ \frac{1}{2}\left\{\pi_{k,i,j|j,i,k}p_{j,i,k} + \pi'_{k,i,j|j,i,k}p'_{j,i,k} + \pi_{j,i,k|k,i,j}p_{k,i,j} + \pi'_{j,i,k|k,i,j}p'_{k,i,j}\right\},
		\end{split}
	\end{equation*}
	which is symmetric by permuting indices $j$ and $k$.
	Hence,
	\begin{equation}
	\label{eq:Habc}
	\begin{split}
	&H(a,b,c) = \sum_{(i,j,k)\in \sigma(a,b,c)} \frac{1}{n-2}\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(j)<\Sigma'(i)\}
	+\mathbb{P}\{\Sigma(j)<\Sigma(i),\Sigma'(k)<\Sigma'(i)\}\\
	&= \sum_{(i,j,k)\in \sigma(a,b,c)} \Biggl\{\frac{2n-1}{2(n-2)}\tilde{\pi}_{j,k,i|j,k,i} + \frac{n-1}{n-2}(\tilde{\pi}_{k,j,i|j,k,i}+ \tilde{\pi}_{j,i,k|j,k,i})\\
	&\qquad\qquad\qquad\qquad + \frac{n-1}{2(n-2)}( \tilde{\pi}_{k,i,j|j,k,i} + \tilde{\pi}_{i,j,k|j,k,i} ) + \frac{1}{2}\tilde{\pi}_{i,k,j|j,k,i} \Biggr\}\tilde{p}_{j,k,i}\\
	&+ \Biggl\{\frac{2n-1}{2(n-2)}\bar{\pi}_{j,k,i|j,k,i} + \frac{n-1}{n-2}(\bar{\pi}_{k,j,i|j,k,i} + \bar{\pi}_{j,i,k|j,k,i})\\
	& \qquad\qquad\qquad\qquad+ \frac{n-1}{2(n-2)}( \bar{\pi}_{k,i,j|j,k,i} + \bar{\pi}_{i,j,k|j,k,i} ) + \frac{1}{2}\bar{\pi}_{i,k,j|j,k,i} \Biggr\}\bar{p}_{j,k,i},
	\end{split}
	\end{equation}
	which is maximized when $\tilde{\pi}_{j,k,i|j,k,i}=1$ (which implies $\bar{\pi}_{j,k,i|j,k,i}=\frac{\tilde{p}_{j,k,i}}{\bar{p}_{j,k,i}}$) and $\bar{\pi}_{k,j,i|j,k,i}+\bar{\pi}_{j,i,k|j,k,i} = 1 - \frac{\tilde{p}_{j,k,i}}{\bar{p}_{j,k,i}}$
	for all $(i,j,k)\in\sigma(a,b,c)$ and then verifies:
	\begin{equation}
	\label{eq:Habc_max}
	\begin{split}
	H(a,b,c) &\le \sum_{(i,j,k)\in \sigma(a,b,c)} \frac{n}{n-2}\tilde{p}_{i,j,k} + \frac{n-1}{n-2}\bar{p}_{i,j,k}
	= \frac{1}{n-2}\sum_{(i,j,k)\in \sigma(a,b,c)} n(p_{i,j,k}+p'_{i,j,k}) - \bar{p}_{i,j,k}\\
	&= \frac{1}{n-2}\left\{ 2n - \sum_{(i,j,k)\in \sigma(a,b,c)} \bar{p}_{i,j,k}\right\},
	\end{split}
	\end{equation}
	which concludes the first part of the proof.
	
	\noindent{\bf (ii).}
	Now we consider the particular case of $P'\in \mathbf{P}_{\C}$, with $\C$ a bucket order of $\n$ with $K$ buckets.
	We propose to prove that $\min_{P'\in\mathbf{P}_{\C}} W_{d_2,2}(P,P')= W_{d_2,2}(P,P_{\C}) =\E[d_2^2(\Sigma,\Sigma_{\C})]$ and to obtain an explicit expression.
	Given three distinct indices $1\le a<b<c\le n$, we analyze the following four possible situations
	to reveal what are the optimal values of the conditional probabilities in Eq. (\ref{eq:Habc}):
	\begin{itemize}
		\item $(a,b,c)\in \C_k$ are in the same bucket: the maximizing conditions are $\tilde{\pi}_{j,k,i|j,k,i}=1$ and $\bar{\pi}_{k,j,i|j,k,i}+\bar{\pi}_{j,i,k|j,k,i} = 1 - \frac{\tilde{p}_{j,k,i}}{\bar{p}_{j,k,i}}$.
		Both are verified when $P'=P_{\C}$ and $\Sigma'=\Sigma_{\C}$ as $\Sigma(j)<\Sigma(k)<\Sigma(i)$ iff $\Sigma_{\C}(j)<\Sigma_{\C}(k)<\Sigma_{\C}(i)$.
		Hence, using Eq. (\ref{eq:Habc_max}):
		$$
		H(a,b,c)\le \frac{1}{n-2}\left\{ 2n - \sum_{(i,j,k)\in \sigma(a,b,c)} \bar{p}_{i,j,k}\right\}\le \frac{2n-1}{n-2}.
		$$
		Moreover, this upper bound is attained when $\Sigma'=\Sigma_{\C}$: $H(a,b,c)=\frac{2n-1}{n-2}$.
		\item $(a,b,c)\in \C_k\times\C_l\times\C_m$ are in three different buckets ($k<l<m$):
		this situation is fully characterized by the bucket structure and is hence independent of the coupling $(\Sigma,\Sigma')$.
		For all $(j,k,i)\in\sigma(a,b,c)\setminus\{(a,b,c)\}$, $p'_{j,k,i}=\tilde{p}_{j,k,i}=0$ so Eq. (\ref{eq:Habc}) is not completely defined
		but $H(a,b,c)$ rewrites more simply without the terms corresponding to the five impossible events $\Sigma'(j)<\Sigma'(k)<\Sigma'(i)$.
		If $(j,k,i)\neq(a,b,c)$, $\bar{p}_{j,k,i}=p_{j,k,i}$ and $\bar{\pi}_{a,b,c|j,k,i}=1$ so the sum of these contributions in $H(a,b,c)$ is:
		\begin{equation}
		\label{eq:3diff_buckets_1}
		\frac{n-1}{n-2}(p_{b,a,c}+p_{a,c,b}) + \frac{n-1}{2(n-2)}(p_{b,c,a}+p_{c,a,b}) + \frac{1}{2}p_{c,b,a}.
		\end{equation}
		We have $p_{a,b,c}\le p'_{a,b,c}=1$ so the condition $\tilde{\pi}_{a,b,c|a,b,c}=1$ is realized
		and for all $(i,j,k)\in\sigma(a,b,c)$, $\bar{\pi}_{i,j,k|a,b,c}=p_{i,j,k}$. The sum of the corresponding contributions in $H(a,b,c)$ is:
		\begin{equation}
		\label{eq:3diff_buckets_2}
		\frac{2n-1}{n-2}p_{a,b,c} + \frac{n-1}{n-2}(p_{b,a,c}+p_{a,c,b}) + \frac{n-1}{2(n-2)}(p_{b,c,a}+p_{c,a,b}) + \frac{1}{2}p_{c,b,a}.
		\end{equation}
		Finally, by combining equations \ref{eq:3diff_buckets_1} and \ref{eq:3diff_buckets_2},
		$$
		H(a,b,c) = \frac{2n-1}{n-2}p_{a,b,c} + \frac{2(n-1)}{n-2}(p_{b,a,c}+p_{a,c,b}) + \frac{n-1}{n-2}(p_{b,c,a}+p_{c,a,b}) + p_{c,b,a}.
		$$
		\item $(a,b,c)\in \C_k\times\C_l\times\C_l$ are in two different buckets ($k<l$) such that $a$ is ranked first among the triplet.
		For all $(j,k,i)\in\sigma(a,b,c)\setminus\{(a,b,c),(a,c,b)\}$, $p'_{j,k,i}=\tilde{p}_{j,k,i}=0$ so Eq. (\ref{eq:Habc}) is not completely defined
		but $H(a,b,c)$ rewrites more simply without the terms corresponding to the four impossible events $\Sigma'(j)<\Sigma'(k)<\Sigma'(i)$.
		For all $(j,k,i)\in\sigma(a,b,c)$, $\pi_{a,b,c|j,k,i}+\pi_{a,c,b|j,k,i}=1$, and the sum of their contributions in $H(a,b,c)$ is:
		\begin{equation}
		\begin{split}
		&\left(\frac{2n-1}{2(n-2)}\pi_{a,b,c|a,b,c} + \frac{n-1}{n-2}\pi_{a,c,b|a,b,c}\right)p_{a,b,c}
		+\left(\frac{2n-1}{2(n-2)}\pi_{a,c,b|a,c,b} + \frac{n-1}{n-2}\pi_{a,b,c|a,c,b}\right)p_{a,c,b}\\
		&+\left(\frac{n-1}{2(n-2)}\pi_{a,b,c|b,c,a} + \frac{1}{2}\pi_{a,c,b|b,c,a}\right)p_{b,c,a}
		+\left(\frac{n-1}{n-2}\pi_{a,b,c|b,a,c} + \frac{n-1}{2(n-2)}\pi_{a,c,b|b,a,c}\right)p_{b,a,c}\\
		&+\left(\frac{n-1}{2(n-2)}\pi_{a,c,b|c,b,a} + \frac{1}{2}\pi_{a,b,c|c,b,a}\right)p_{c,b,a}
		+\left(\frac{n-1}{n-2}\pi_{a,c,b|c,a,b} + \frac{n-1}{2(n-2)}\pi_{a,b,c|c,a,b}\right)p_{c,a,b}.
		\end{split}
		\end{equation}
		Observe that the expression above is maximized when $\pi_{a,b,c|a,b,c}=\pi_{a,c,b|a,c,b}=\bar{\pi}_{a,b,c|b,c,a}=\bar{\pi}_{a,b,c|b,a,c}=\bar{\pi}_{a,c,b|c,b,a}=\bar{\pi}_{a,c,b|c,a,b}=1$,
		which is verified by $\Sigma'=\Sigma_{\C}$. In this case, Eq. (\ref{eq:2diff_buckets_1}) becomes:
		\begin{equation}
		\label{eq:2diff_buckets_1}
		\frac{2n-1}{2(n-2)}(p_{a,b,c}+p_{a,c,b})+\frac{n-1}{n-2}(p_{b,a,c}+p_{c,a,b}) + \frac{n-1}{2(n-2)}(p_{b,c,a}+p_{c,b,a})
		\end{equation}
		Now consider $(j,k,i)\in\{(a,b,c),(a,c,b)\}$: $p'_{a,b,c} + p'_{a,c,b} = 1$ and the corresponding contributions to $H(a,b,c)$ sum as follows:
		\begin{equation*}
			\begin{split}
				&\Biggl\{\frac{2n-1}{2(n-2)}\pi'_{a,b,c|a,b,c} + \frac{n-1}{n-2}(\pi'_{b,a,c|a,b,c}+\pi'_{a,c,b|a,b,c})\\
				&+ \frac{n-1}{2(n-2)}(\pi'_{b,c,a|a,b,c}+\pi'_{c,a,b|a,b,c}) + \frac{1}{2}\pi'_{c,b,a|a,b,c}\Biggr\}p'_{a,b,c}\\
				& +\Biggl\{\frac{2n-1}{2(n-2)}\pi'_{a,c,b|a,c,b} + \frac{n-1}{n-2}(\pi'_{c,a,b|a,c,b}+\pi'_{a,b,c|a,c,b})\\
				&+ \frac{n-1}{2(n-2)}(\pi'_{c,b,a|a,c,b}+\pi'_{b,a,c|a,c,b}) + \frac{1}{2}\pi'_{b,c,a|a,c,b}\Biggr\}p'_{a,c,b},
			\end{split}
		\end{equation*}
		which is maximized when $\pi'_{a,c,b|a,b,c}=\pi'_{c,a,b|a,b,c}=\pi'_{c,b,a|a,b,c}=0$
		and $\pi'_{a,b,c|a,c,b}=\pi'_{b,a,c|a,c,b}=\pi'_{b,c,a|a,c,b}=0$: both conditions are true for $\Sigma'=\Sigma_{\C}$.
		Then, the expression above is upper bounded by:
		\begin{equation}
		\label{eq:2diff_buckets_2}
		\frac{2n-1}{2(n-2)}(p_{a,b,c}+p_{a,c,b})+\frac{n-1}{n-2}(p_{b,a,c}+p_{c,a,b}) + \frac{n-1}{2(n-2)}(p_{b,c,a}+p_{c,b,a})
		\end{equation}
		with equality when $\Sigma'=\Sigma_{\C}$.
		Finally, by summing the terms in \ref{eq:2diff_buckets_1} and \ref{eq:2diff_buckets_2},
		$$
		H(a,b,c) \le \frac{2n-1}{n-2}(p_{a,b,c}+p_{a,c,b}) + \frac{2(n-1)}{n-2}(p_{b,a,c}+p_{c,a,b}) + \frac{n-1}{n-2}(p_{b,c,a}+p_{c,b,a}),
		$$
		where the equality holds for $\Sigma'=\Sigma_{\C}$.
		\item $(a,b,c)\in \C_k\times\C_k\times\C_l$ are in two different buckets ($k<l$) such that $c$ is ranked last among the triplet.
		Similarly as in the previous situation, we obtain:
		$$
		H(a,b,c) \le \frac{2n-1}{n-2}(p_{a,b,c}+p_{b,a,c}) + \frac{2(n-1)}{n-2}(p_{a,c,b}+p_{b,c,a}) + \frac{n-1}{n-2}(p_{c,a,b}+p_{c,b,a}),
		$$
		where the equality holds for $\Sigma'=\Sigma_{\C}$.
	\end{itemize}
	As a conclusion, we proved that: $\min_{P'\in\mathbf{P}_{\C}} W_{d_2,2}(P,P')= W_{d_2,2}(P,P_{\C}) =\E[d_2^2(\Sigma,\Sigma_{\C})]$.
	
\end{proof}

\textbf{The Hamming case.} We also provide a lower bound on the $1$-st Wasserstein metric with the Hamming distance $d_H$ as cost function.

\begin{lemma}\label{lem:hamming} For any probability distributions $P$ and $P'$ on $\mathfrak{S}_n$:
	\begin{equation*}
		W_{d_H,1}(P,P')\ge \sum_{i=1}^{n}\left\{1 - \sum_{j=1}^{n}\min(q_{i,j}, q'_{i,j})\right\},
	\end{equation*}
	where $q_{i,j}=\mathbb{P}_{\Sigma\sim P}\{ \Sigma(i)=j \}$ and $q'_{i,j}=\mathbb{P}_{\Sigma'\sim P'}\{ \Sigma'(i)=j \}$.
\end{lemma}

\begin{proof}
	
	Consider a coupling $(\Sigma,\Sigma')$ of two probability distributions $P$ and $P'$ on $\mathfrak{S}_n$. For all $i,j,k$, set
	$$
	\rho_{i,j,k}=\mathbb{P}\left\{ \Sigma'(i)= k\mid  \Sigma(i)=j \right\} \text{ and } \rho'_{i,j,k}=\mathbb{P}\left\{ \Sigma(i)= k\mid  \Sigma'(i)=j \right\}.$$
	For simplicity, we assume throughout the proof that $\min(q_{i,j}, q'_{i,j})>0$ for all $(i,j)\in\n^2$, the generalization being straightforward.
	We may write
	\begin{equation}
	\label{eq:hamming}
	\begin{split}
	\mathbb{E}\left[ d_{H}\left( \Sigma,\Sigma' \right)\right]
	&=\sum_{i=1}^{n} \Prob{\Sigma(i)\ne\Sigma'(i)} = \sum_{i=1}^{n} \sum_{j=1}^n\sum_{k\neq j} \Prob{\Sigma(i)=j, \Sigma'(i)=k}\\
	&= \sum_{i=1}^{n}\sum_{j=1}^{n}\sum_{k\ne j}\rho_{i,j,k} q_{i,j}
	= \sum_{i=1}^{n}\sum_{j=1}^{n}q_{i,j}\left( 1 - \rho_{i,j,j} \right)
	= n - \sum_{i,j=1}^{n} \rho_{i,j,j}q_{i,j}.
	\end{split}
	\end{equation}
	For $(i,j)\in\n^2$, the quantity $\rho_{i,j,j} q_{i,j}$ is maximized when $\rho_{i,j,j}=1$, which requires that $q_{i,j}\le~ q'_{i,j}$.
	If $q_{i,j}> q'_{i,j}$, rather write in a similar fashion:
	\begin{align*}
		\mathbb{E}\left[ d_{H}\left( \Sigma,\Sigma' \right)\right]&= n - \sum_{i,j=1}^{n} \rho'_{i,j,j}q'_{i,j},
	\end{align*}
	and set $\rho'_{i,j,j}=1$. We thus have from Eq. \eqref{eq:hamming}:
	\begin{equation*}
		\begin{split}
			W_{d_H,1}(P,P') &\ge
			\sum_{i=1}^n \inf_{(\Sigma,\Sigma') \text { s.t. } \mathbb{P}\{\Sigma(i)=j\}=q_{i,j} \text{ and } \mathbb{P}\{\Sigma'(i)=j\}=q'_{i,j} } \left\{1 - \sum_{j=1}^{n} \Prob{\Sigma(i)=\Sigma'(i)=j} \right\}\\
			&= \sum_{i=1}^{n}\left\{1 - \sum_{j=1}^{n}\min(q_{i,j}, q'_{i,j})\right\}.
		\end{split}
	\end{equation*}
	
\end{proof}





