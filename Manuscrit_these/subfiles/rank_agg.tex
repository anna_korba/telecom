\chapter{The Ranking Aggregation Problem}\label{chap:rank_agg}

\begin{chapabstract}
In this chapter, we describe the ranking aggregation problem: given a dataset of (full) rankings, what is the most representative ranking summarizing this dataset? Originally considered in social choice for elections, the ranking aggregation problem appears nowadays in many modern applications implying machine learning (e.g., meta-search engines, information retrieval, biology). This problem has been studied extensively, in particular in two settings. The first possibility is to consider that the dataset is constituted of noisy versions of a true ranking (e.g., realizations of a parameterized distribution centered around some ranking), and the goal is to reconstruct the true ranking thanks to the samples (e.g., with MLE estimation). The second possibility is to formalize this problem as a discrete optimization problem over the set of rankings, and to look for the ranking which is the closest (with respect to some distance) to the rankings observed in the dataset, without stochastic assumptions. These former approaches tackle the problem in a rigorous manner, but can lead to heavy computational costs in practice. Therefore, many other methods have been used in the litterature, such as scoring methods or spectral methods, but with fewer or no theoretical support. In this chapter, we thus explain in detail the ranking aggregation problem, the mathematical challenges it raises and give an overview of the ranking aggregation methods in the litterature.  
\end{chapabstract}

%We elaborate in detail the voting rules used in the paper to approximate Kemeny's rule. Note that if multiple consensuses are returned from a rule on a given dataset, we randomly pick one from these consensuses. \anna{à réécrire. dire state of the art + utilisés ds les chapitres suivants}
\section{Ranking Aggregation}

\subsection{Definition}

Consider a set $\llbracket n \rrbracket = \left\{1,\dots,n\right\}$  of $n$ indexed items and $N$ agents. Suppose that each agent $t$ expresses her preferences as a full ranking over the $n$ items, which, as described Chapter~\ref{chap:background_ranking_data}, can be seen as a permutation $\sigma_t \in \Sn$. Collecting preferences of the agents over the set of items then results in dataset of permutations $\DN=(\sigma_1,\dots,\sigma_N) \in \Sn^N$. The ranking aggregation problem consists in finding a permutation $\sigma^{*}$, called \textit{consensus}, that best summarizes the dataset (sometimes referred to as the \textit{profile}) $\DN=(\sigma_1,\dots,\sigma_N)$. This problem has been studied extensively and a lot of approaches have been developped, in particular in two settings. The first possibility is to consider that the dataset is constituted of noisy versions of a true ranking (e.g., realizations of a parameterized distribution centered around some ranking), and the goal is to reconstruct the true ranking thanks to the samples (e.g., with MLE estimation). The second possibility is to formalize this problem as a discrete optimization problem over the set of rankings, and to look for the ranking which is the closest (with respect to some distance) to the rankings observed in the dataset, without stochastic assumptions. These former approaches tackle the problem in a rigorous manner, but can lead to heavy computational costs in practice. Therefore, many other methods have been used in the literature, such as scoring methods or spectral methods, but with fewer or no theoretical support. In this chapter, we thus give an overview of the ranking aggregation problem, the challenges it raises and the main methods in the literature. 

%\subsection{Applications}

%cite{fagin2003efficient}

\subsection{Voting Rules Axioms}

The ranking aggregation problem arised in the context of elections (see Section~\ref{sec:applications}). 
Hence, many axioms for the consensus have been considered in the social choice litterature, reflecting some aspects of a fair election. 
\begin{itemize}
	\item \textit{Independance to irrelevant alternatives}: the relative order of $i$ and $j$ in $\sigma^{*}$ should only depend on the relative order of $i$ and $j$ in $\sigma_1, \dots \sigma_N$.
	%Ex: if $\sigma_1 = 123$ changes to $132$, the relative order of $1,2$ in should not change in $\sigma^{*}$.
	\item \textit{Neutrality }: no item should be favored to others.
	If two items switch positions in $\sigma_1, \dots \sigma_N$, they should switch positions also in $\sigma^{*}$.
	%\item Anonimity: No item should be favored to others.
	%If two items switch their labels, $\sigma^{*}$ should remain the same.
	\item \textit{Monotonicity}: if the ranking of an item is improved by a voter, its ranking in $\sigma^{*}$ can only improve.
	\item \textit{Consistency}: if voters are split into two disjoint sets, and both the aggregation of voters in the first and second set prefer $i$ to $j$, then $i$ should be ranked above $j$ in $\sigma^{*}$.
	\item \textit{Non-dictatorship}: there is no single voter $t$ with the individual preference order $\sigma_{t}$ such that $\sigma_{t} = \sigma^{*}$, unless all votes are identical to $\sigma_{t}$. %Results of voting cannot simply mirror that of any single person's preferences without consideration of the other voters.
	\item \textit{Unanimity (or Pareto efficiency)}: if all voters prefer item $i$ to  item $j$, then also $\sigma^{*}$ should prefer $i$ to $j$.
	\item \textit{Condorcet criterion}: any item  which wins every other in pairwise simple majority voting should be ranked first (see \cite{Condorcet}). If there is a \textit{Condorcet winner} in the \textit{profile}, the latter is called a \textit{Condorcet profile}.
\end{itemize}
Other criterions exist in the literature and can extend the ones listed previously. A famous example is the \textit{extended Condorcet criterion}, due to Truchon (see \cite{truchon1998extension}) which states that if there exists a partition $(A,B)$ of $\n$, such that for any $i \in A$ and any $j \in B$, the majority prefers $i$ to $j$, then $i$ should be ranked above $j$ in $\sigma^{*}$. However, the following theorem (see \cite{Arrow51}) states the limits of the properties that any election procedure can satisfy.
\begin{theorem}
 (\textbf{Arrow's impossibility theorem}). No voting system can satisfy simultaneously \textit{unanimity, non-dictatorship} and \textit{independance to irrelevant alternatives}.
\end{theorem}
Arrow's theorem states that there exists no universally fair voting rule, implying that there exists no canonical solution to the ranking aggregation problem. One will thus choose a given procedure with respect to the axioms one wants to be satisfied by the output. This problem is thus fondamentally challenging, and very diverse methods were developed in the literature to produce a consensus. These are presented in the next section.


\section{Methods}

\subsection{Kemeny's Consensus}

\noindent One of the most popular formulation of the ranking aggregation problem is the Kemeny definition of a consensus (see \cite{Kemeny59}). Kemeny defines the \textit{consensus ranking} $\sigma^{*}$ as the one that minimizes the sum of the distances to the
rankings in $\DN=(\sigma_1,\dots,\sigma_N)$, i.e. a solution to the minimization problem:
 \begin{equation}\label{eq:ranking_aggregation}
\min_{\sigma\in\Sn} C_{N}(\sigma)
 \end{equation}
where $C_{N}(\sigma)=\min_{\sigma \in \mathfrak{S}_n} \sum_{t=1}^{N} d(\sigma,\sigma_t)$ and $d$ is a given metric
on $\mathfrak{S}_n$ (see section~\ref{sec:distances}).  Such an element always exists, as $\mathfrak{S}_n$ is finite, but is not necessarily unique, and the solution(s) depend on the choice of the distance $d$. %Instead of dealing with the collection of permutations $(\sigma_1,\dots,\sigma_N)$, we can reformulate the consensus ranking problem for any real-valued function $p$ on $\mathfrak{S}_n$ such that the consensuses of $p$ with respect to a metric $d$ are the solutions of:
%\begin{equation*}
%\min_{\sigma \in \mathfrak{S}_n} \sum_{\pi \in \mathfrak{S}_n} d(\sigma,\pi)p(\pi) =: R_{d,p}(\sigma).
%\end{equation*}
%\noindent The set of consensuses is denoted by $C_d(p) := \argmin_{\sigma \in \mathfrak{S}_n} R_{d,p}(\sigma)$. This formulation thus extends the one of a consensus of a collection of permutations $(\sigma_1,\dots,\sigma_N) \in \mathfrak{S}_n$ since:
%\begin{equation*}
%\sum_{t=1}^{N}d(\sigma,\sigma_t)=R_{d,p_N}(\sigma)\quad \text{with}\quad %p_N(\pi)=|\left\{1 \le i  \le N |\pi =\sigma_i \right\}|.
%\end{equation*}
%Naturally comes the question of which distance to choose.

\noindent \textbf{Kemeny's rule} computes the exact consensus(es) for the \textit{Kendall's $\tau$ distance} $d_{\tau}$, which counts the number of pairwise disagreements between two permutations (see section~\ref{sec:distances}). Kemeny's rule thus consists in solving:
\begin{equation}\label{eq:kemenys_rule}
\max_{\sigma \in \mathfrak{S}_n} \sum_{1 \le i \ne j \le n} N_{i,j}\mathbb{I}\left\{ \sigma(i) < \sigma(j)\right\}
\end{equation}
\noindent where for $i \ne j$, $N_{i,j} = 1/N \sum_{t=1}^{N} \mathbb{I}\{\sigma_t(i) < \sigma_t(j)\}$ is the number of times $i$ is preferred over $j$ in the collection ($\sigma_1, \dots, \sigma_N)$. This aggregation method has several justifications.
Firstly, it has a social choice justification since its solution satisfies many voting properties, such as the \textit{Condorcet Criterion}. In fact, the Kemeny's rule is the unique rule that meets all three of following axioms: it satisfies the \textit{Condorcet criterion},  \textit{consistency under elimination}, and \textit{neutrality} (see \cite{young1978consistent}). Then, it has a statistical justification since it outputs the maximum likelihood estimator under the Mallows model defined section~\ref{sec:parametric_models} with the Kendall's $\tau$ distance $d_{\tau}$ (see \citet{young1988condorcet}):
\begin{equation*}
 \argmax_{\sigma \in \mathfrak{S}_n}
\prod_{t=1}^{N}
\frac{\phi^{-d_{\tau} (\sigma_t,\sigma)}}{Z}
= \argmin_{\sigma \in \mathfrak{S}_n}
\sum_{t=1}^{N}
d_{\tau} (\sigma_t,\sigma)
\end{equation*}
\noindent The main drawback of this method is that it is NP-hard in the number of votes $N$ in the worst case (see \cite{BTT89}), even for $N=4$ votes (see \cite{dwork2001rank}). Kemeny ranking aggregation is actually closely related to the (weighted) \textit{Feedback Arc Set Tournament} (FAST) problem, also known to be NP-hard (see \cite{alon2006ranking}, \cite{ailon2008aggregating}). It can be solved by exact algorithms (Integer Linear Programming or Branch and Bound) given enough time, depending on the agreement of the ranking in the dataset $\DN$. In \cite{MPPB07} it is shown that with strong agreements in $\DN$, the Branch and Bound will have a running time complexity of $\mathcal{O}(n^2)$, whereas in \cite{Ali2012Experiments}, the authors identify several regimes (strong, weak or no consensus) and compute data-dependent lower and upper bounds for the running time of these algorithms and when the Kemeny ranking seems (empirically) to be given by other procedures, see Remark~\ref{rk:regimes}. On the other hand, some contributions developed PTAS (Polynomial Time Approximation Scheme, see \cite{coppersmith2006ordering}, \cite{kenyon2007rank}, \cite{karpinski2010faster}). A relaxation of Kemeny's rule, named Local Kemeny Aggregation, satisfying the extended Condorcet criterion and computable in time $\mathcal{O}(N n \log n)$, has also been proposed in \cite{dwork2001rank}. % However, it admits a PTAS (Polynomial Time Approximation Scheme) (see \cite{kenyon2007rank}). Numerous contributions then propose polynomial time algorithms to output approximations of this problem, see \cite{coppersmith2006ordering}, \cite{kenyon2007rank}, \cite{karpinski2010faster}.

\noindent \textbf{Footrule aggregation} computes the exact consensus(es) for the Spearman's footrule distance $d_1$. The footrule consensus can actually be computed in polynomial time since can be solved by the Hungarian algorithm in $\mathcal{O}(n^3)$ (see \cite{Ali2012Experiments}). Moreover, it provides a 2-approximation to Kemeny's rule (see \cite{dwork2001rank}), i.e:  $C_{N}(\sigma^{1}) \le 5 C_{N}(\sigma^{*})$ where $C_{N}$ is the cost defined in~\eqref{eq:ranking_aggregation} with $d$ the Kendall's $\tau$ distance, $\sigma^*$ is a Kemeny consensus and $\sigma^1$ is a solution of~\eqref{eq:ranking_aggregation} with $d$ the Spearman's footrule distance.

\subsection{Scoring Methods}

Scoring methods consists in computing a score for each item, and then rank the items according to these scores (by decreasing order). Ties can occur in the scores, and then one is allowed to break them arbitrarily to output a full ranking in $\Sn$. The most popular scoring methods are Copeland method and Positional scoring rules, including the famous Borda method.

\noindent {\bf Copeland method.} The Copeland score (see \cite{Copeland51}) of each individual item $i$ corresponds to the number of its pairwise victories against the other items:
\begin{equation}\label{eq:copeland_method}
s_{C,N}(i)= \sum_{j \ne i} \mathbb{I}\{ N_{i,j}> \frac{1}{2} \} 
\end{equation}
 where for $i \ne j$, $N_{i,j} = 1/N \sum_{t=1}^{N} \mathbb{I}\{\sigma_t(i) < \sigma_t(j)\}$ is the number of times $i$ is preferred over $j$ in the collection ($\sigma_1, \dots, \sigma_N)$.
For example, the Copeland winner is the alternative that wins the most pairwise elections. The output of Copeland method then naturally verifies the Condorcet criterion. 


\noindent {\bf Positional Scoring rules.} Positional scoring rules rely on the absolute position of items in the ranked lists rather than their relative rankings, and compute scores for items as follows. Given a scoring vector $w = (w_1,\dots,w_n) \in \RR^n$ of weights respectively for each position in $\n$, the $i$th alternative in a vote scores $w_i$. A full ranking is given by sorting the averaged scores over all votes, so that the winner is the alternative with the highest total score over all the votes. The most classic representative of the class of positional ranking methods is the {\bf Borda count} method, firstly proposed for electing members of the French \textit{Académie des sciences} in Paris in 1770 (see \cite{Borda}). In the Borda count aggregation method, the weight vector is $(n, n-1, \dots, 1)$ and thus each individual item $i \in \n$ is awarded with a score which is given according to its position in the permutations:
\begin{equation}\label{eq:borda_count}
s_{B,N}(i)=\sum_{t=1}^{N} (n+1-\sigma_t(i))
\end{equation}
The final ranking is obtained by sorting items by decreasing order of their scores. The higher is the item position in the permutations (i.e., the lower are the values of $\sigma_t(i)$), the greater is the score. This method has appealing properties. Firstly, it is quite intuitive: the score of an item can be seen as its average rank in the dataset. Then, it is computationally efficient since it has complexity $\mathcal{O}(Nn+n log n)$ (the first term comes from the computation of $\eqref{eq:borda_count}$ and the second one from the sorting). Interestingly, the Borda score can also be written with pairwise comparisons:
\begin{equation}
s_{B,N}(i)= 1 + \sum_{j \ne i} N_{i,j}
\end{equation}
where $N_{i,j}= 1/N \sum_{t=1}^N \mathbb{I}\{ \sigma(i)<\sigma(j)\}$ using the formula $n+1-2\sigma(i)=n-\sigma(i) - (\sigma(i)-1)= \sum_{j\ne i} \mathbb{I}\{\sigma(j)>\sigma(i)\} - \mathbb{I}\{\sigma(j)<\sigma(i)\}$. The Borda score  can thus be interpreted as the probability (multiplied by $n-1$) that item $i$ beats an item $j$ drawn uniformly at random within the remaining items. This property has been recently exploited in the online learning setting (see \cite{katariya2018adaptive}) to estimate the Borda scores in a minimum number of queries.

\noindent Other famous examples of positional scoring rules are the \textbf{plurality} rule, which has the weight vector $(1,0,\dots,0)$, and the \textbf{k-approval} rule (also called \textit{Single Non Transferable Vote}) which has $(1,\dots,1,\dots,0)$ containing ones in the first $k$ positions. Many extensions or these scoring rules can be found in the literature, see \cite{diss2016multi} for additional examples. These methods are generally computationally efficient and are thus commonly used in practice. However, their main drawback is that they can produce ties between the scores, which have to be broken in order to output a final ranking in $\Sn$. Some contributions propose solutions to circumvent this problem, such as the second-order Copeland method (see \cite{BTT89}), which use the Copeland scores of the defeated opponents to decide the winner between two items with the same scores. Then, no positional method can produce rankings guaranteed to satisfy the Condorcet criterion (\cite{young1978consistent}), see Figure~\ref{fig:borda_condorcet} for an example from~\cite{levin1995introduction}.


\begin{figure}[ht]
	\includegraphics[width=12cm]{figures/borda_condorcet.png}
	\centering
	\caption{An election where Borda count does not elect the Condorcet winner .}
	\label{fig:borda_condorcet}
\end{figure}
However, it was shown in~\cite{coppersmith2006ordering} that the Borda’s method outputs a 5-approximation to Kemeny's rule:  $C_{N}(\sigma^{B}) \le 5 C_{N}(\sigma^{*})$ where $C_{N}$ is the cost defined in~\eqref{eq:ranking_aggregation} with $d$ the Kendall's $\tau$ distance, $\sigma^*$ is a Kemeny consensus and $\sigma^B$ is an output from Borda's method. The difference between Borda count and Kemeny consensus can be explained in terms of pairwise inconsistencies (i.e. \textit{non-transitivity} or \textit{cycles} in preferences). Indeed, \cite{Sibony2014Borda} shows that $C_{N}(\sigma^{B}) - C_{N}(\sigma^{*})\le F(\DN)$ where $F(\DN)$ is a quantitative measure of the amount of local pairwise inconsistencies
in the dataset $\DN$, and in \cite{JLYY11} it is proven that Borda count corresponds to an $l2$ projection of the data (pairwise probabilities, or "flows") on the space
of \textit{gradient flows} (localizing the global consistencies) orthogonal to the space of \textit{divergence-free flows} (localizing local pairwise inconsistencies), see Figure~\ref{fig:hodge} from the reference therein.
The Borda count thus provides a good trade-off between Kemeny approximation accuracy and computational cost in empirical experiments, see also Remark~\ref{rk:regimes}.

\begin{figure}[ht]
	\includegraphics[width=9cm]{images/hodge.pdf}
	\centering
	\caption{Hodge/Helmotz decomposition of the space of pairwise rankings.}
	\label{fig:hodge}
\end{figure}

\begin{remark} {\sc (Consensus regimes).}\label{rk:regimes}
	\cite{Ali2012Experiments} recommend in practice to use Copeland or Borda in the regime of weak consensus, or simply Borda in the regime of strong consensus. This is coherent with the theoretical results we obtain Chapter~\ref{chap:stat_fram}: if the distribution underlying the data verifies transitivity, Copeland method (on the true pairwise probabilities) outputs a Kemeny consensus; and moreover if it verifies strong transitivity, Borda (also with respect to the true probabilities) does as well. We also prove that under an additional low-noise assumption (see \cite{CKS17}), Copeland method applied to the empirical pairwise probabilities outputs a (true) Kemeny consensus with high probability. Another coherent results assessing this phenomenon is the one in \cite{rajkumar2014statistical}, which proves that if the distribution verifies a low-noise property (thus different from the one we use Chapter~\ref{chap:stat_fram}), the Borda method outputs a Kemeny consensus with high probability. Notice that the latter low-noise property includes the strong transitivity. 
\end{remark}


\subsection{Spectral Methods}

Numerous algorithms, often referred to as Spectral methods are inspired by Markov chains, and propose to compute a consensus from a dataset of rankings (or solely pairwise comparisons) as follows. Markov chain methods for ranking aggregation represent the items as states of a Markov chain, which can be seen as nodes in a graph. The transition probability of going from node $i$ to node $j$ is based on the relative orders of these items in the rankings in the dataset, and decreases as $i$ is more preferred to $j$. The consensus of the dataset is obtained by computing, or approximating, the stationary distribution of the Markov chain and then sort the nodes by decreasing probability in the stationary distribution. The position of an item in the final consensus can thus be interpreted as the probability to be visited by a random walk on this graph. Markov chain methods thus propose a general algorithm for rank aggregation, composed of three steps:
\begin{itemize}
	\item  map the set of ranked lists to a single Markov chain $M$, with one node per item in $\left\{1, \dots, n \right\}$,
	\item compute (or approximate) the stationary distribution $\pi$ on $M$,
	\item rank the items in $\left\{1, \dots, n \right\}$ based on $\pi$.
\end{itemize}
\noindent The key in this method is to define the appropriate mapping in the first step, from the set of ranked lists to a Markov Chain $M$. \cite{dwork2001rank} proposed, analyzed, and tested four mapping schemes, called MC1, MC2, MC3 and MC4. The second step boils down to do power iteration on the transition probability matrix, which can lead to computationally efficient and iterative algorithms (see the RankCentrality algorithm from \cite{negahban2012iterative}).  Many algorithms modeling preferences as random walks on a graph were considered in the literature, such as the famous random surfer model on the webpage graph named PageRank (see \cite{brin1998anatomy}). These methods have been proven to be effective in practice. For instance, Rank Centrality outputs with high probability a Kemeny consensus under the assumption that the observed comparisons are generated by a Bradley-Terry model (see \cite{rajkumar2014statistical}). Recently, extensions to incomplete rankings have been proposed in \cite{MG15b, agarwal2018accelerated} which are consistent under the Plackett-Luce model assumption.

%Although none of these methods necessarily produces a Kemeny consensus, these methods have proven effective in practice. Indeed, many statistical guarantees have been established under some conditions on the distribution $p$ underlying the observed pairwise outcomes. A summary of these conditions and their relationship can be found in \cite{rajkumar2014statistical, rajkumar2016ranking}. In particular, they showed that some of these algorithms, under specific conditions on $P$ output a Kemeny consensus with high probability \cite{rajkumar2014statistical}: Rank Centrality and the Least Squares algorithm from \cite{jiang2011statistical} under a Bradley-Terry model,  Borda and 

%Simple extremal eigenvalue computation, low complexity (roughly $\mathcal{O}(n^2 log n$) in the dense case)


%\noindent Markov chains are well studied models of stochastic processes. A Markov chain $M$ consists of a set of states $S =\left\{1 \dots n \right\}$ and a transition probability matrix $P$ of dimension $n \times n$. Markov chains function in discrete time steps, and $\mathbb{P}[S_t=i]$ is defined as the probability that a random walker on the chain will be at state $i$ at time step $t$. The transition probabilities are defined by
%$P_{i,j}=\mathbb{P}[S_t=j|S_{t-1}=i]$ (Note that each row in $P$ sums to 1.) Thus, Markov chains are memoryless: the probability of moving to any given state at the next time step depends only upon the current state, not on any previous states.
%The stationary distribution on a given Markov chain is the set of probabilities: $\mathbb{P}[S_{\infty}=i]$ for all $i \in S.$ All finite, irreducible, ergodic Markov chains have a single stationary distribution, which may be computed by finding the principle eigenvector (the one corresponding to the eigenvalue of largest magnitude) of the matrix $P$, or closely approximated taking higher powers of $P$ to convergence or by simulating a random walk over $M$ for a large number of steps. For large chains, simulating the random walk reduces computational cost considerably.



%MLE based algorithms: \cite{bradley1952rank}, \cite{luce2005individual} \cite{herbrich2007trueskill}
%\noindent {\bf Plackett-Luce.} A Plackett-Luce ranking model defined for any $\sigma\in\Sn$ by $p_w(\sigma) = \prod_{i=1}^n w_{\sigma(i)}/\br{\sum_{j=i}^n w_{\sigma(j)}}$ parameterized by $w = (w_1,\dots,w_n)\in\RR^n$, fitted to $\DN$ by means of the MM algorithm \citep{Hunter2004MM}. A full ranking is then given by sorting $w$.

\subsection{Other Ranking Aggregation Methods}

Many other methods have been proposed in the literature, that we do not list exhaustively here. However, many of them will be used as baselines in the experiments of Chapter~\ref{chap:icml16}.

\noindent \textbf{MLE and Bayesian approaches.} A common approach is to consider that the observed rankings, for examples pairwise comparisons, are generated according to a given model centered around a true underlying order. The outcomes of the pairwise comparisons are then noisy versions of the true relative orders under some model, and one can perform MLE (Maximum Likelihood Estimation) to recover the underlying central ranking. Several models and settings have been considered. This idea actually originally arised in the work of Condorcet (see \cite{Condorcet}) under a particular noise model (equivalent to Mallows, see \cite{LB11a}): a voter ranks correctly two candidates with probability $p>1/2$. Numerous contributions thus consider a Mallows model (see \cite{fligner1990posterior,MPPB07, braverman2008noisy}), a 
$\gamma$-noise model ($\mathbb{P}\{i \prec j\}=1/2+\gamma$ for all $i<j$, see \cite{braverman2008noisy}),  Bradley-Terry model (see BTL-ML algorithm in \cite{rajkumar2014statistical}), or more original ones such as the coset-permutation distance based stagewise (CPS) model (see \cite{qin2010new}). Some come with statistical guarantees, for instance BTL-ML outputs a Kemeny consensus with high probability under the assumption that the pairwise probabilities verifies the low-noise (bis) assumption (see Section~\ref{sec:non_parametric}). An interesting work is also the one of \cite{conitzer2012common} which states for which voting rules (e.g. scoring rules), there exists (or not) a noise model such that the voting rule is an MLE estimator under this model. Another approach, especially in the domain of chess gaming, use Bayesian statistics to estimate a ranking, assuming some priors on the items preferences (see \cite{glickman1995glicko,HMG06,coulom2008whole}). The obvious limitations of these approaches is that it depends on the unknown underlying noise model. %Voting Rules As Error-Correcting Codes (adversarial noise)


\noindent {\bf Other popular methods.} Many other heuristics can be found in the literature. For instance, \textit{QuickSort} recursively divides the unsorted list of items into two lists: one list comprising alternatives that are preferred to a chosen item (called the \emph{pivot}), and another comprising alternatives that are less preferred, and then sorts each of the two lists. The pivot is always chosen as the first alternative. This method has been proved to be a 2-approximation for Kemeny consensus \cite{ailon2008aggregating}, and several variants have been proposed (see \cite{Ali2012Experiments}). In \cite{Schalekamp2009Rank}, the authors propose \textit{Pick-a-Perm}: a full ranking is picked randomly from $\Sn$ according to the empirical distribution of the dataset $\DN$. Another remarkably simple method, namely \textit{Median Rank Aggregation}, aggregates a set of complete rankings by using the median rank for each item in the dataset $\DN$, and is actually a practical heuristic for footrule aggregation. Indeed, if the median ranks form a permutation, then it is a footrule consensus (see \cite{dwork2001rank}).


%Distance-Based Methods/ 
 %The rankings it produces satisfy the extended Condorcet criterion, and it may be computed efficiently, especially in the case where only the top $k$ aggregate rankings are required (\cite{fagin2003efficient}).


%Optimal Sample Complexity of M-wise Data for Top-K Ranking
%Minje Jang (KAIST) · Sunghyun Kim (ETRI) · Changho Suh (KAIST) · Sewoong Oh (UIUC)


%A Maximum Likelihood Approach For Selecting Sets of Alternatives


%Faire un tableau avec les axiomes satisfaits par chaque règle!!

