\contentsline {chapter}{List of Publications}{ix}{dummy.1}
\contentsline {chapter}{List of Figures}{xi}{dummy.2}
\contentsline {chapter}{List of Tables}{xiii}{dummy.3}
\contentsline {chapter}{List of Symbols}{xv}{section*.6}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Background on Ranking Data}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Ranking Aggregation}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Definition and Context}{3}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}A General Method to Bound the Distance to Kemeny Consensus}{4}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}A Statistical Framework for Ranking Aggregation}{6}{subsection.1.2.3}
\contentsline {section}{\numberline {1.3}Beyond Ranking Aggregation: Dimensionality Reduction and Ranking Regression}{7}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Dimensionality Reduction for Ranking Data: a Mass Transportation Approach}{8}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Ranking Median Regression: Learning to Order through Local Consensus}{10}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}A Structured Prediction Approach for Label Ranking}{12}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}Conclusion}{13}{section.1.4}
\contentsline {section}{\numberline {1.5}Outline of the Thesis}{14}{section.1.5}
\contentsline {chapter}{\numberline {2}Background on Ranking Data}{15}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduction to Ranking Data}{15}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Definitions and Notations}{15}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Ranking Problems}{16}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Applications}{18}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Analysis of Full Rankings}{20}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Parametric Approaches}{21}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Non-parametric Approaches}{23}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Distances on Rankings}{26}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Other Frameworks}{28}{section.2.3}
\contentsline {part}{I\hspace {1em}Ranking Aggregation}{31}{part.1}
\contentsline {chapter}{\numberline {3}The Ranking Aggregation Problem}{33}{chapter.3}
\contentsline {section}{\numberline {3.1}Ranking Aggregation}{33}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Definition}{33}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Voting Rules Axioms}{34}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Methods}{35}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Kemeny's Consensus}{35}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Scoring Methods}{36}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Spectral Methods}{38}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Other Ranking Aggregation Methods}{39}{subsection.3.2.4}
\contentsline {chapter}{\numberline {4}A General Method to Bound the Distance to Kemeny Consensus}{41}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{41}{section.4.1}
\contentsline {section}{\numberline {4.2}Controlling the Distance to a Kemeny Consensus}{42}{section.4.2}
\contentsline {section}{\numberline {4.3}Geometric Analysis of Kemeny Aggregation}{43}{section.4.3}
\contentsline {section}{\numberline {4.4}Main Result}{45}{section.4.4}
\contentsline {section}{\numberline {4.5}Geometric Interpretation and Proof of Theorem \ref {th:method}}{47}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Extended Cost Function}{47}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Interpretation of the Condition in Theorem \ref {th:method}}{48}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Embedding of a Ball}{50}{subsection.4.5.3}
\contentsline {subsection}{\numberline {4.5.4}Proof of Theorem \ref {th:method}}{50}{subsection.4.5.4}
\contentsline {section}{\numberline {4.6}Numerical Experiments}{51}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Tightness of the Bound}{51}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Applicability of the Method}{54}{subsection.4.6.2}
\contentsline {section}{\numberline {4.7}Conclusion and Discussion}{55}{section.4.7}
\contentsline {chapter}{\numberline {5}A Statistical Framework for Ranking Aggregation}{57}{chapter.5}
\contentsline {section}{\numberline {5.1}Introduction}{57}{section.5.1}
\contentsline {section}{\numberline {5.2}Background}{58}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Consensus Ranking}{58}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Statistical Framework}{59}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Connection to Voting Rules}{60}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Optimality}{61}{section.5.3}
\contentsline {section}{\numberline {5.4}Empirical Consensus}{64}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Universal Rates}{64}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Fast Rates in Low Noise}{66}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Computational Issues}{67}{subsection.5.4.3}
\contentsline {section}{\numberline {5.5}Conclusion}{68}{section.5.5}
\contentsline {section}{\numberline {5.6}Proofs}{68}{section.5.6}
\contentsline {part}{II\hspace {1em}Beyond Ranking Aggregation: Dimensionality Reduction and Ranking Regression}{77}{part.2}
\contentsline {chapter}{\numberline {6}Dimensionality Reduction and (Bucket) Ranking:\\ A Mass Transportation Approach}{79}{chapter.6}
\contentsline {section}{\numberline {6.1}Introduction}{79}{section.6.1}
\contentsline {section}{\numberline {6.2}Preliminaries}{80}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Background on Bucket Orders}{80}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}A Mass Transportation Approach to Dimensionality Reduction on $\mathfrak {S}_n$}{81}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Optimal Couplings and Minimal Distortion}{83}{subsection.6.2.3}
\contentsline {subsection}{\numberline {6.2.4}Related Work}{84}{subsection.6.2.4}
\contentsline {section}{\numberline {6.3}Empirical Distortion Minimization - Rate Bounds and Model Selection}{85}{section.6.3}
\contentsline {section}{\numberline {6.4}Numerical Experiments on Real-world Datasets}{88}{section.6.4}
\contentsline {section}{\numberline {6.5}Conclusion}{89}{section.6.5}
\contentsline {section}{\numberline {6.6}Appendix}{89}{section.6.6}
\contentsline {section}{\numberline {6.7}Proofs}{100}{section.6.7}
\contentsline {chapter}{\numberline {7}Ranking Median Regression: Learning to Order through Local Consensus}{105}{chapter.7}
\contentsline {section}{\numberline {7.1}Introduction}{105}{section.7.1}
\contentsline {section}{\numberline {7.2}Preliminaries}{107}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Best Strictly Stochastically Transitive Approximation}{107}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Predictive Ranking and Statistical Conditional Models}{108}{subsection.7.2.2}
\contentsline {section}{\numberline {7.3}Ranking Median Regression}{109}{section.7.3}
\contentsline {section}{\numberline {7.4}Local Consensus Methods for Ranking Median Regression}{112}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}Piecewise Constant Predictive Ranking Rules and Local Consensus}{112}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}Nearest-Neighbor Rules for Ranking Median Regression}{116}{subsection.7.4.2}
\contentsline {subsection}{\numberline {7.4.3}Recursive Partitioning - The {\sc CRIT} algorithm}{118}{subsection.7.4.3}
\contentsline {section}{\numberline {7.5}Numerical Experiments}{121}{section.7.5}
\contentsline {section}{\numberline {7.6}Conclusion and Perspectives}{123}{section.7.6}
\contentsline {section}{\numberline {7.7}Appendix - On Aggregation in Ranking Median Regression}{123}{section.7.7}
\contentsline {section}{\numberline {7.8}Proofs}{126}{section.7.8}
\contentsline {chapter}{\numberline {8}A Structured Prediction Approach for Label Ranking}{137}{chapter.8}
\contentsline {section}{\numberline {8.1}Introduction}{137}{section.8.1}
\contentsline {section}{\numberline {8.2}Preliminaries}{138}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Mathematical Background and Notations}{138}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Related Work}{139}{subsection.8.2.2}
\contentsline {section}{\numberline {8.3}Structured Prediction for Label Ranking}{140}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Learning Problem}{140}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}Losses for Ranking}{141}{subsection.8.3.2}
\contentsline {section}{\numberline {8.4}Output Embeddings for Rankings}{142}{section.8.4}
\contentsline {subsection}{\numberline {8.4.1}The Kemeny Embedding}{142}{subsection.8.4.1}
\contentsline {subsection}{\numberline {8.4.2}The Hamming Embedding}{143}{subsection.8.4.2}
\contentsline {subsection}{\numberline {8.4.3}Lehmer Code}{144}{subsection.8.4.3}
\contentsline {subsection}{\numberline {8.4.4}Extension to Partial and Incomplete Rankings}{145}{subsection.8.4.4}
\contentsline {section}{\numberline {8.5}Computational and Theoretical Analysis}{146}{section.8.5}
\contentsline {subsection}{\numberline {8.5.1}Theoretical Guarantees}{146}{subsection.8.5.1}
\contentsline {subsection}{\numberline {8.5.2}Algorithmic Complexity}{148}{subsection.8.5.2}
\contentsline {section}{\numberline {8.6}Numerical Experiments}{148}{section.8.6}
\contentsline {section}{\numberline {8.7}Conclusion}{149}{section.8.7}
\contentsline {section}{\numberline {8.8}Proofs and Additional Experiments}{150}{section.8.8}
\contentsline {subsection}{\numberline {8.8.1}Proof of Theorem 1}{150}{subsection.8.8.1}
\contentsline {subsection}{\numberline {8.8.2}Lehmer Embedding for Partial Rankings}{152}{subsection.8.8.2}
\contentsline {subsection}{\numberline {8.8.3}Additional Experimental Results}{153}{subsection.8.8.3}
\contentsline {chapter}{\numberline {9}Conclusion, Limitations \& Perspectives}{155}{chapter.9}
\contentsline {chapter}{\numberline {10}R\'esum\'e en fran\c {c}ais}{157}{chapter.10}
\contentsline {section}{\numberline {10.1}Pr\IeC {\'e}liminaires sur les Donn\IeC {\'e}es de Classements}{158}{section.10.1}
\contentsline {section}{\numberline {10.2}L'agr\IeC {\'e}gation de Classements}{159}{section.10.2}
\contentsline {subsection}{\numberline {10.2.1}D\IeC {\'e}finition et Contexte}{159}{subsection.10.2.1}
\contentsline {subsection}{\numberline {10.2.2}Une M\IeC {\'e}thode G\IeC {\'e}n\IeC {\'e}rale pour Borner la Distance au Consensus de Kemeny}{161}{subsection.10.2.2}
\contentsline {subsection}{\numberline {10.2.3}Un Cadre Statistique pour l'Agr\IeC {\'e}gation de Classements}{162}{subsection.10.2.3}
\contentsline {section}{\numberline {10.3}Au-del\IeC {\`a} de l'Agr\IeC {\'e}gation de Classements : la R\IeC {\'e}duction de Dimension et la R\IeC {\'e}gression de Classements}{164}{section.10.3}
\contentsline {subsection}{\numberline {10.3.1}R\IeC {\'e}duction de Dimension pour les Donn\IeC {\'e}es de Classements : une Approche de Transport de Masse}{164}{subsection.10.3.1}
\contentsline {subsection}{\numberline {10.3.2}R\IeC {\'e}gression M\IeC {\'e}diane de Classements: Apprendre \IeC {\`a} Classer \IeC {\`a} travers des Consensus Locaux}{167}{subsection.10.3.2}
\contentsline {subsection}{\numberline {10.3.3}Une Approche de Pr\IeC {\'e}diction Structur\IeC {\'e}e pour la R\IeC {\'e}gression de Classements}{169}{subsection.10.3.3}
\contentsline {section}{\numberline {10.4}Conclusion}{171}{section.10.4}
\contentsline {section}{\numberline {10.5}Plan de la Th\IeC {\`e}se}{171}{section.10.5}
\contentsline {chapter}{Bibliography}{173}{dummy.4}
