%% ----------------------------------------------------------------
%% Thesis.tex
%% ---------------------------------------------------------------- 

\documentclass{ecsthesis}      % Use the Thesis Style
\input{Definitions}            % Include your abbreviations
\usepackage{psfrag}
\usepackage{ragged2e}
\usepackage{comment}


\hypersetup{colorlinks=true}   % Set to false for black/white printing
\setcounter{secnumdepth}{3} % seting level of numbering (default for "report" is 3). With ''-1'' you have non number also for chapters
\setcounter{tocdepth}{2}

% Symbols

\glsxtrnewsymbol[description={Number of objects}]{n}{\ensuremath{n}}
\glsxtrnewsymbol[description={Set of $n$ items $\{1, 2, \dots, n\}$}]{n_}{\ensuremath{\n}}
\glsxtrnewsymbol[description={Number of samples}]{N}{\ensuremath{N}}
\glsxtrnewsymbol[description={Symmetric group over $n$ items}]{s}{\ensuremath{\Sn}}
\glsxtrnewsymbol[description={Kendall's $\tau$ distance}]{d}{\ensuremath{d_{\tau}}}
\glsxtrnewsymbol[description={Any permutation in $\Sn$}]{sigma}{\ensuremath{\sigma}}
\glsxtrnewsymbol[description={Dirac mass at any point $\sigma$}]{dirac}{\ensuremath{\delta_{\sigma}}}
\glsxtrnewsymbol[description={Any random permutation in $\Sn$}]{Sigma}{\ensuremath{\Sigma}}
\glsxtrnewsymbol[description={Any distribution on $\Sn$}]{P_}{\ensuremath{P}}
\glsxtrnewsymbol[description={A feature space}]{X}{\ensuremath{\mathcal{X}}}
\glsxtrnewsymbol[description={Any distribution on $\mathcal{X}$}]{mu}{\ensuremath{\mu}}
\glsxtrnewsymbol[description={Item $a$ is preferred to item $b$}]{pref}{\ensuremath{a \prec b}}
\glsxtrnewsymbol[description={Any finite set}]{C}{\ensuremath{$C$}}
\glsxtrnewsymbol[description={Cardinality of finite set $C$}]{sharpC}{\ensuremath{$\#C$}}
\glsxtrnewsymbol[description={L-2 norm}]{l2}{\ensuremath{\| . \|}}
\glsxtrnewsymbol[description={L-1 norm}]{l1}{\ensuremath{| . |}}
\glsxtrnewsymbol[description={Any function}]{f}{\ensuremath{f}}
\glsxtrnewsymbol[description={Inverse of $f$}]{f_}{\ensuremath{f^{-1}}}
\glsxtrnewsymbol[description={Image of function $f$}]{Im}{\ensuremath{Im(f)}}
\glsxtrnewsymbol[description={Set of real numbers}]{R}{\ensuremath{\mathbb{R}}}
\glsxtrnewsymbol[description={Indicator of an event}]{I}{\ensuremath{\mathbb{I}\{.\}}}
\glsxtrnewsymbol[description={Probability of an event}]{P}{\ensuremath{\mathbb{P}\{.\}}}
\glsxtrnewsymbol[description={Expectation of an event}]{E}{\ensuremath{\mathbb{E}[.]}}



\begin{document}
\begingroup
\makeatletter
\let\BR@bibitem\BRatbibitem
\nobibliography*
\endgroup
\frontmatter
\title      {my title} 
\titre      {mon titre}
\authors    {Anna Charlotte Korba}
\date       {date}
\subject    {PhD Thesis}
\keywords   {}
%\thispagestyle{empty}
\maketitle
%\thispagestyle{empty}	

\input{subfiles/ack}
\tableofcontents
\input{subfiles/publilisting}
\listoffigures
\listoftables
\printunsrtglossary[type=symbols,style=long,title={List of Symbols}]


\mainmatter


\input{subfiles/introduction.tex}
%\part{Background}\label{part:background}
\input{subfiles/background_ranking_data.tex}

\part{Ranking Aggregation}\label{part:rank_agg}
\input{subfiles/rank_agg.tex}
\input{subfiles/icml.tex}
\input{subfiles/stat_fram.tex}
\input{subfiles/subfiles_appendix/proofs_stat_fram.tex}

\part{Beyond Ranking Aggregation: Dimensionality  Reduction and Ranking Regression}\label{part:beyond}
\input{subfiles/dim_red_rank.tex}
\input{subfiles/subfiles_appendix/proofs_dim_red_rank.tex}
\input{subfiles/rank_reg.tex}
\input{subfiles/subfiles_appendix/proofs_rank_reg.tex}
%\input{subfiles/subfiles_appendix/Aggregation_Esann.tex}
\input{subfiles/struc_pred.tex}
\input{subfiles/subfiles_appendix/proofs_struc_pred.tex}
\input{subfiles/conclusion.tex}
\input{subfiles/introduction_francais.tex}

\backmatter
{\small
\bibliographystyle{ecs}
\bibliography{biblio_thesis_anna}
}



\newpage
\cleardoublepage
\thispagestyle{empty}
\begingroup

%\section*{}
\vspace{-5cm}
%\hbox{\includegraphics[width=8.6cm]{images/ED_EDMH-h.jpg}}
%\vspace{-5cm}
\noindent\fbox{\parbox{\textwidth}{
		{\bf Titre : }Apprendre des Données de Classement: Théorie et Méthodes
		
		\medskip
		{\bf Mots Clefs : }Statistiques, Apprentissage automatique, Classements, Agrégation, Permutations, Comparaisons par paires.
		
		\medskip
		{\bf Résumé : }Les données de classement, c.à.d. des listes ordonnées d'objets, apparaissent naturellement dans une grande variété de situations, notamment lorsque les données proviennent d’activités humaines (bulletins de vote d'élections, enquêtes d'opinion, résultats de compétitions) ou dans des applications modernes du traitement de données (moteurs de recherche, systèmes de recommendation). La conception d'algorithmes d'apprentissage automatique, adaptés à ces données, est donc cruciale. Cependant, en raison de l’absence de structure vectorielle de l’espace des classements et de sa cardinalité explosive lorsque le nombre d'objets augmente, la plupart des méthodes classiques issues des statistiques et de l’analyse multivariée ne peuvent être appliquées directement. Par conséquent, la grande majorité de la littérature repose sur des modèles paramétriques. Dans cette thèse, nous proposons une théorie et des méthodes non paramétriques pour traiter les données de classement. Notre analyse repose fortement sur deux astuces principales. La première est l’utilisation poussée de la distance du tau de Kendall, qui décompose les classements en comparaisons par paires. Cela nous permet d'analyser les distributions sur les classements à travers leurs marginales par paires et à travers une hypothèse spécifique appelée transitivité, qui empêche les cycles dans les préférences de se produire. La seconde est l'utilisation des fonctions de représentation adaptées aux données de classements, envoyant ces dernières dans un espace vectoriel. Trois problèmes différents, non supervisés et supervisés, ont été abordés dans ce contexte: l'agrégation de classement, la réduction de dimensionnalité et la prévision de classements avec variables explicatives.
		
		La première partie de cette thèse se concentre sur le problème de l'agrégation de classements, dont l'objectif est de résumer un ensemble de données de classement par un classement consensus. Parmi les méthodes existantes pour ce problème, la méthode d'agrégation de Kemeny se démarque. Ses solutions vérifient de nombreuses propriétés souhaitables, mais peuvent être NP-difficiles à calculer. Dans cette thèse, nous avons étudié la complexité de ce problème de deux manières. Premièrement, nous avons proposé une méthode pour borner la distance du tau de Kendall entre tout candidat pour le consensus (généralement le résultat d'une procédure efficace) et un consensus de Kemeny, sur tout ensemble de données. Nous avons ensuite inscrit le problème d'agrégation de classements dans un cadre statistique rigoureux en le reformulant en termes de distributions sur les classements, et en évaluant la capacité de généralisation de consensus  de Kemeny empiriques.
		
		La deuxième partie de cette thèse est consacrée à des problèmes d'apprentissage automatique, qui se révèlent être étroitement liés à l'agrégation de classement. Le premier est la réduction de la dimensionnalité pour les données de classement, pour lequel nous proposons une approche de transport optimal, pour approximer une distribution sur les classements par une distribution montrant un certain type de parcimonie. Le second est le problème de la prévision des classements avec variables explicatives, pour lesquelles nous avons étudié plusieurs méthodes. Notre première proposition est d’adapter des méthodes constantes par morceaux à ce problème, qui partitionnent l'espace des variables explicatives en régions et assignent à chaque région un label (un consensus). Notre deuxième proposition est une approche de prédiction structurée, reposant sur des fonctions de représentations, aux avantages théoriques et computationnels, pour les données de classements.
		
}}

\endgroup

\newpage
%\cleardoublepage
\thispagestyle{empty}
\begingroup

\noindent\fbox{\parbox{\textwidth}{
		{\bf Title : } Learning from Ranking Data: Theory and Methods
		
		\medskip
		{\bf Keywords : }Statistics, Machine learning, Ranking, Aggregation, Permutations, Pairwise comparisons.
		
		\medskip
		{\bf Abstract : }Ranking data, i.e., ordered list of items, naturally appears in a wide variety of situations, especially when the data comes from human activities (ballots in political elections, survey answers, competition results) or in modern applications of data processing (search engines, recommendation systems). The design of machine-learning algorithms, tailored for these data, is thus crucial.  However, due to the absence of any vectorial structure of the space of rankings, and its explosive cardinality when the number of items increases, most of the classical methods from statistics and multivariate analysis cannot be applied in a direct manner. Hence, a vast majority of the literature rely on parametric models. In this thesis, we propose a non-parametric theory and methods for ranking data. Our analysis heavily relies on two main tricks. The first one is the extensive use of the Kendall’s tau distance, which decomposes rankings into pairwise comparisons. This enables us to analyze distributions over rankings through their pairwise marginals and through a specific assumption called transitivity, which prevents cycles in the preferences from happening. The second one is the extensive use of embeddings tailored to ranking data, mapping rankings to a vector space. Three different problems,  unsupervised and supervised, have been addressed in this context: ranking aggregation, dimensionality reduction and predicting rankings with features.
		
		The first part of this thesis focuses on the ranking aggregation problem, where the goal is to summarize a dataset of rankings by a consensus ranking. Among the many ways to state this problem stands out the Kemeny aggregation method, whose solutions have been shown to satisfy many desirable properties, but can be NP-hard to compute. In this work, we have investigated the hardness of this problem in two ways. Firstly, we proposed a method to upper bound the Kendall’s tau distance between any consensus candidate (typically the output of a tractable procedure) and a Kemeny consensus, on any dataset. Then, we have casted the ranking aggregation problem in a rigorous statistical framework, reformulating it in terms of ranking distributions, and assessed the generalization ability of empirical Kemeny consensus.
		
		The second part of this thesis is dedicated to machine learning problems which are shown to be closely related to ranking aggregation. The first one is dimensionality reduction for ranking data, for which we propose a mass-transportation approach to approximate any distribution on rankings by a distribution exhibiting a specific type of sparsity. The second one is the problem of predicting rankings with features, for which we investigated several methods. Our first proposal is to adapt piecewise constant methods to this problem, partitioning the feature space into regions and locally assigning as final label (a consensus ranking) to each region. Our second proposal is a structured prediction approach, relying on embedding maps for ranking data enjoying theoretical and computational advantages. 
}}


\vfill
\hfill \includegraphics[width=1cm]{images/pictoParis-Saclay.jpg}

\endgroup
%\input{subfiles/abstracts.tex}

\end{document}

