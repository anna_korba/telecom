%%
%% This is file `ecsthesis.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% ecsdocs.dtx  (with options: `thesis')
%% 
%% Copyright (C) 2001 by Steve R. Gunn
%% 
%% This file is part of the ECSDocs class distribution
%% 
\NeedsTeXFormat{LaTeX2e}[1996/12/01]
\ProvidesClass{ecsthesis}
[2003/25/04 v1.6
LaTeX document class]
\def\baseclass{book}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{\baseclass}}
\def\@checkoptions#1#2{
	\edef\@curroptions{\@ptionlist{\@currname.\@currext}}
	\@tempswafalse
	\@tfor\@this:=#2\do{
		\@expandtwoargs\in@{,\@this,}{,\@curroptions,}
		\ifin@ \@tempswatrue \@break@tfor \fi}
	\let\@this\@empty
	\if@tempswa \else \PassOptionsToClass{#1}{\baseclass}\fi
}
\@checkoptions{11pt}{{10pt}{11pt}{12pt}}
\PassOptionsToClass{a4paper}{\baseclass}
\ProcessOptions\relax
\LoadClass{\baseclass}
\newcommand\bhrule{\typeout{------------------------------------------------------------------------------}}
\newcommand\btypeout[1]{\bhrule\typeout{\space #1}\bhrule}
\def\today{\ifcase\month\or
	January\or February\or March\or April\or May\or June\or
	July\or August\or September\or October\or November\or December\fi
	\space \number\year}
\usepackage{setspace}
\onehalfspacing
\setlength{\parindent}{0pt}
\setlength{\parskip}{2.0ex plus0.5ex minus0.2ex}
\usepackage{vmargin}
\setmarginsrb           { 1.5in}  % left margin
{ 0.6in}  % top margin
{ 1.0in}  % right margin
{ 0.8in}  % bottom margin
{  20pt}  % head height
{0.25in}  % head sep
{   9pt}  % foot height
{ 0.3in}  % foot sep
\raggedbottom
\setlength{\topskip}{1\topskip \@plus 5\p@}
\doublehyphendemerits=10000       % No consecutive line hyphens.
\brokenpenalty=10000              % No broken words across columns/pages.
\widowpenalty=9999                % Almost no widows at bottom of page.
\clubpenalty=9999                 % Almost no orphans at top of page.
\interfootnotelinepenalty=9999    % Almost never break footnotes.
\usepackage{fancyhdr}
\lhead[\rm\thepage]{\fancyplain{}{\sl{\rightmark}}}
\rhead[\fancyplain{}{\sl{\leftmark}}]{\rm\thepage}
\chead{}\lfoot{}\rfoot{}\cfoot{}
\pagestyle{fancy}
\renewcommand{\chaptermark}[1]{\btypeout{\thechapter.\quad #1}\markboth{\@chapapp\ \thechapter.\quad #1}{\@chapapp\ \thechapter.\quad #1}}
\renewcommand{\sectionmark}[1]{}
\renewcommand{\subsectionmark}[1]{}
\def\cleardoublepage{\clearpage\if@twoside \ifodd\c@page\else
	\hbox{}
	\thispagestyle{empty}
	\newpage
	\if@twocolumn\hbox{}\newpage\fi\fi\fi}
\usepackage{amsmath,amsfonts,amssymb,amscd,amsthm,xspace}
\theoremstyle{plain}
\newtheorem{example}{Example}[chapter]
\newtheorem{theorem}{Theorem}[chapter]
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{axiom}[theorem]{Axiom}
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\theoremstyle{remark}
\newtheorem{remark}[theorem]{Remark}
\usepackage[small,sc]{caption}
\setlength{\captionmargin}{20pt}
\newcommand{\fref}[1]{Figure~\ref{#1}}
\newcommand{\tref}[1]{Table~\ref{#1}}
\newcommand{\eref}[1]{Equation~\ref{#1}}
\newcommand{\cref}[1]{Chapter~\ref{#1}}
\newcommand{\sref}[1]{Section~\ref{#1}}
\newcommand{\aref}[1]{Appendix~\ref{#1}}
\newcommand{\defref}[1]{Definition~\ref{#1}}
\newcommand{\thref}[1]{Theorem~\ref{#1}}
\newcommand{\lref}[1]{Lemma~\ref{#1}}
\newcommand{\pref}[1]{Part~\ref{#1}}
\newcommand{\exref}[1]{Example~\ref{#1}}
\newcommand{\propref}[1]{Proposition~\ref{#1}}
\renewcommand{\topfraction}{0.85}
\renewcommand{\bottomfraction}{.85}
\renewcommand{\textfraction}{0.1}
\renewcommand{\dbltopfraction}{.85}
\renewcommand{\floatpagefraction}{0.75}
\renewcommand{\dblfloatpagefraction}{.75}
\setcounter{topnumber}{9}
\setcounter{bottomnumber}{9}
\setcounter{totalnumber}{20}
\setcounter{dbltopnumber}{9}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage[scriptsize]{subfigure}
\usepackage{booktabs}
\usepackage{rotating}
\usepackage{listings}
%\usepackage{lstpatch}
\usepackage{color}

\lstdefinestyle{matlab} {
	language=Matlab,
	keywordstyle=\color{blue},
	commentstyle=\color[rgb]{0.13,0.55,0.13}\em,
	stringstyle=\color[rgb]{0.7,0,0} }
\definecolor{linkcol}{rgb}{0,0,0.4}
\definecolor{urlcol}{rgb}{0.4,0,0}
\usepackage[pdfpagemode={UseOutlines},bookmarks=true,bookmarksopen=true,
bookmarksopenlevel=0,bookmarksnumbered=true,hypertexnames=false,
colorlinks,linkcolor={linkcol},citecolor={linkcol},urlcolor={urlcol},
pdfstartview={FitV},unicode,breaklinks=true,hypertexnames=true]{hyperref}
\pdfstringdefDisableCommands{
	\let\\\space
}
\newcommand*{\authors}[1]{\def\authornames{#1}}
\newcommand*{\subject}[1]{\def\subjectname{#1}}
\newcommand*{\keywords}[1]{\def\keywordnames{#1}}
\newcommand*{\titre}[1]{\def\titrename{#1}}
\authors     {}
\subject     {}
\keywords    {}
\renewcommand\maketitle{
	\btypeout{Title Page}
	\hypersetup{pdftitle={\@title}}
	\hypersetup{pdfsubject=\subjectname}
	\hypersetup{pdfauthor=\authornames}
	\hypersetup{pdfkeywords=\keywordnames}
	\thispagestyle{empty}
	\begin{titlepage}
		%\vspace{-2cm}
		%\voffset-10pt
		\noindent
		\hspace*{-1cm}\hbox{\includegraphics[width=8.6cm]{images/ED_EDMH-h.jpg}} 
		\hfill 
		\hspace*{-0.4cm}{\small {\bf NNT : 2018SACLT009}}
		\hfill
		\hbox{\includegraphics[width=2cm]{images/telecom_paristech_imt_grise_rvb.png}}
		\vspace{7mm}
		
		\begin{center}
			{\Large\bf TH\`ESE DE DOCTORAT}
		\end{center}
		\begin{center}
			{de }
		\end{center}
		\begin{center}
			{\Large\sc l'Universit\'e Paris-Saclay}\\
			\vspace*{0.4cm}
			\'Ecole doctorale de math\'ematiques Hadamard (EDMH, ED 574)\\  
			\vspace*{0.4cm} 
			{\small \it \'Etablissement d'inscription : } 
			T\'el\'ecom ParisTech\\
			\vspace*{0.2cm} 
			{\small \it Laboratoire d'accueil : }     
			Laboratoire traitement et communication de l'information (LTCI)\\
			\vspace*{0.2cm}
		\end{center}
		\begin{center}
			{\it Sp\'ecialit\'e de doctorat : } 
			{\large Math\'ematiques appliqu\'ees}
		\end{center}
		\vspace{4mm}
		\begin{center}
			{\large\bf Anna Charlotte Korba}
		\end{center}
		\vspace{2mm}
		\begin{center}
			{\Large Learning from Ranking Data: Theory and Methods}%A statistical framework and methods for ranking data 
		\end{center}%A Non-parametric Framework for Analysis and Methods for Ranking Data
	
		%\vspace{4mm}
		\noindent{\small \it Date de soutenance~: } 25 octobre 2018\\
		%\vspace{2mm}
		
		\noindent
		{\small \it Apr\`es avis des rapporteurs~: }
		\begin{tabular}{l} {\sc Shivani AGARWAL} (University of Pennsylvania)\vspace{1mm}  \\
			{\sc  Eyke H\"ULLERMEIER} (Paderborn University)\\
		\end{tabular}
	
		\vspace{3mm}
		\noindent
		{\small \it Jury de soutenance~: }
		\begin{tabular}{ll}
			{\sc   Stephan CL\'EMEN\c{C}ON}&(T\'el\'ecom ParisTech) {\small Directeur de th\`ese}\vspace{1mm}\\
		    {\sc Shivani AGARWAL}& (University of Pennsylvania) {\small  Rapporteure}\vspace{1mm}\\
			{\sc Eyke H\"ULLERMEIER}& (University of Paderborn) {\small  Rapporteur}\vspace{1mm}\\
			{\sc   Jean-Philippe VERT}& (Mines ParisTech) {\small Pr\'esident du jury}\vspace{1mm}\\
			{\sc   Nicolas VAYATIS}&(ENS Cachan) {\small Examinateur}\vspace{1mm}\\
			{\sc   Florence d'ALCH\'E BUC}&(T\'el\'ecom ParisTech) {\small Examinatrice}\vspace{1mm}\\
		\end{tabular}
		
		
		\vfill
		\noindent
		\hbox{\includegraphics[width=2.2cm]{images/logo_fmjh.jpg}}
		\hfill 
	%	\hbox{\includegraphics[width=2cm]{images/telecom_paristech_imt_grise_rvb.png}}
		\hfill \includegraphics[width=1cm]{images/pictoParis-Saclay.jpg}
	\end{titlepage}
	\setcounter{footnote}{0}%
	\global\let\thanks\relax
	\global\let\maketitle\relax
	\global\let\@thanks\@empty
	\global\let\@author\@empty
	\global\let\@date\@empty
	\global\let\@title\@empty
	\global\let\title\relax
	\global\let\author\relax
	\global\let\date\relax
	\global\let\and\relax
	\cleardoublepage
}
\usepackage{ifthen}

\newcommand{\newevenside}{
	\ifthenelse{\isodd{\thepage}}{
		\newpage
		\phantom{placeholder} % doesn't appear on page
		\thispagestyle{empty} % if want no header/footer
		\newpage
	}{\newpage}
}

\newenvironment{abstract}
{
	\newevenside
	\btypeout{Abstract}
	\thispagestyle{empty}
	
}
{}
\addtocounter{secnumdepth}{1}
\setcounter{tocdepth}{1}
\newcounter{dummy}
\newcommand\addtotoc[1]{
	\refstepcounter{dummy}
	\addcontentsline{toc}{chapter}{#1}}
\renewcommand\tableofcontents{
	\btypeout{Table of Contents}
	\begin{spacing}{1}{
			\setlength{\parskip}{1pt}
			\if@twocolumn
			\@restonecoltrue\onecolumn
			\else
			\@restonecolfalse
			\fi
			\chapter*{\contentsname
				\@mkboth{
					\MakeUppercase\contentsname}{\MakeUppercase\contentsname}}
			\@starttoc{toc}
			\if@restonecol\twocolumn\fi
			\cleardoublepage
	}\end{spacing}
}
\renewcommand\listoffigures{
	\btypeout{List of Figures}
	\addtotoc{List of Figures}
	\begin{spacing}{1}{
			\setlength{\parskip}{1pt}
			\if@twocolumn
			\@restonecoltrue\onecolumn
			\else
			\@restonecolfalse
			\fi
			\chapter*{\listfigurename
				\@mkboth{\MakeUppercase\listfigurename}
				{\MakeUppercase\listfigurename}}
			\@starttoc{lof}
			\if@restonecol\twocolumn\fi
			\cleardoublepage
	}\end{spacing}
}
\renewcommand\listoftables{
	\btypeout{List of Tables}
	\addtotoc{List of Tables}
	\begin{spacing}{1}{
			\setlength{\parskip}{1pt}
			\if@twocolumn
			\@restonecoltrue\onecolumn
			\else
			\@restonecolfalse
			\fi
			\chapter*{\listtablename
				\@mkboth{
					\MakeUppercase\listtablename}{\MakeUppercase\listtablename}}
			\@starttoc{lot}
			\if@restonecol\twocolumn\fi
			\cleardoublepage
	}\end{spacing}
}
\newcommand\listsymbolname{Nomenclature}
\usepackage{longtable}
\newcommand\listofsymbols[2]{
	\btypeout{\listsymbolname}
	\addtotoc{\listsymbolname}
	\chapter*{\listsymbolname
		\@mkboth{
			\MakeUppercase\listsymbolname}{\MakeUppercase\listsymbolname}}
	\begin{longtable}[c]{#1}#2\end{longtable}\par
	\cleardoublepage
}
\newenvironment{acknowledgements}
{
	\btypeout{Acknowledgements}
	\thispagestyle{plain}
	\chapter*{Remerciements
		\@mkboth{
			\MakeUppercase{Remerciements}}{\MakeUppercase{Remerciements}}}
	
}
{
	\vfil\vfil\vfil\null
	\cleardoublepage
}
\newenvironment{listpublis}
{
	\cleardoublepage
	\btypeout{List of Publications}
	\addtotoc{List of Publications}
	\thispagestyle{plain}
	\chapter*{List of Publications
		\@mkboth{
			\MakeUppercase{List of Publications}}{\MakeUppercase{List of Publications}}}
	
}
{
	\vfil\vfil\vfil\null
	\cleardoublepage
}
\newcommand\dedicatory[1]{
	\btypeout{Dedicatory}
	\thispagestyle{plain}
	\null\vfil
	\vskip 60\p@
	\begin{center}{\Large \sl #1}\end{center}
	\vfil\null
	\cleardoublepage
}
\renewcommand\backmatter{
	\if@openright
	\cleardoublepage
	\else
	\clearpage
	\fi
	\addtotoc{\bibname}
	\btypeout{\bibname}
	\@mainmatterfalse}
\endinput
%%
%% End of file `ecsthesis.cls'.



