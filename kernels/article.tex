\documentclass{article} % For LaTeX2e
\usepackage{nips13submit_e,times}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{url}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{stmaryrd} 
\usepackage{dsfont}
\usepackage{comment}
\usepackage{mathrsfs}

\title{A framework for kernels on permutations and ranking aggregation}


\author{
Yunlong Xia \\
\texttt{MINES ParisTech} \\
\AND
Anna Korba \\
LTCI UMR No. 5141, Telecom ParisTech/CNRS \\
Institut Mines-Telecom \\
Paris, 75013, France \\
\texttt{anna.korba@telecom-paristech.fr} \\
\AND
Eric Sibony \\
LTCI UMR No. 5141, Telecom ParisTech/CNRS \\
Institut Mines-Telecom \\
Paris, 75013, France \\
\texttt{anna.korba@telecom-paristech.fr} \\
}

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

\nipsfinalcopy % Uncomment for camera-ready version

\begin{document}


\maketitle

\begin{abstract}
We present a general framework that relates the consensus ranking approximation to kernels on permutations. It provides a deeper understanding of the connection between some well-known ranking aggregation methods and some mappings: we explain that in both cases, the choice of a distance naturally embed rankings into a specific feature space that localizes a certain part of the information. This novel framework opens the way to further similar results.
\end{abstract}


\section{Introduction}

Ranking aggregation has been studied for long in the context of social choice, but has also attracted much attention in the machine learning literature in the last years, due to its numerous modern applications. For example, recommandation systems aim at presenting objects that might interest an user, in an order that sticks best to its preferences.
An optimal aggregated ranking is then one which minimizes the number of disagreements with the observed rankings, and evaluating such a ranking requires some measure of comparison between two rankings. Similarly, kernels on permutations can be thought of as a measure of similarity by implicitly defining an embedding of the input space to a Hilbert space in which in which the kernel becomes an inner product. In this paper, we are interested in the feature spaces of well-known kernels on permutations and ranking aggregation methods.


Let $\llbracket n \rrbracket$ be a set of items to be ranked. We denote $i \succ j$ if the item $i$ is preferred to the item $j$ according to some ranking. A total ranking on $\llbracket n \rrbracket$ is then of the form : $a_1 \succ \dots \succ a_n$ where $a_1$ and $a_n$ are the items respectively ranked first and last. Such an order is usually described as the permutation $\sigma$ on $\llbracket n \rrbracket$ that maps an item to its rank, i.e. such that $\sigma(a_i)=i$ for $i \in \llbracket n \rrbracket$. We denote by $\mathfrak{S}_n$ the set of all permutations over $n$ items. Endowed with the composition operation $\sigma_1\sigma_2(i) = \sigma_1(\sigma_2(i))$, $\mathfrak{S}_n$ is a group called the symmetric group and we denote $e$ its identity element which maps each item $j$ to position $j$. The variability of observations is thus represented by a probability distribution $p$ on $\mathfrak{S}_n$, which can be seen as an element of the euclidean space
$L(\mathfrak{S}_n) = \left\{f: \mathfrak{S}_n \rightarrow \mathbb{R}\right\}$, such that $p(\sigma) \ge 0$ for all $\sigma \in \mathfrak{S}_n$ and $\sum_{\sigma \in \mathfrak{S}_n} p(\sigma) = 1$. The inner product on $L(\mathfrak{S}_n)$ is the usual $\langle f, g \rangle = \sum_{\sigma \in \mathfrak{S}_n}f(\sigma)g(\sigma)$. For a random permutation $\sigma \in \mathfrak{S}_n$ of law $p$ and a subset $S \in \mathfrak{S}_n$, the probability of the event $E= \left\{ \sigma \in S \right\}$, denoted by $\mathbb{P}[E]$, is by definition: $\mathbb{P}[E]= \sum_{\sigma \in S}p(\sigma)= \langle p, \mathds{1}_{S} \rangle$. For example  $\mathbb{P}[\sigma(i)=j]= \sum_{\sigma \in \mathfrak{S}_n, \sigma(i)=j }p(\sigma)$.


However, the available rank data often describe limited information of the form of partial or incomplete rankings. Partial rankings are orders of the form $a_{1,1}, \dots, a_{1,\mu_1} \succ \dots \succ a_{r,1}, \dots , a_{r,\mu_r}$, with $r \ge 1$ and $\sum_{i=1}^{r}\mu_i=n$. They correspond to full rankings with ties, and include the particular case of top-$k$ rankings, of the form $a_1 \succ \dots a_k \succ$ \textit{the rest}. Incomplete rankings are partial orders of the form $a_1 \succ \dots \succ a_k$ with $2 \le k < n$ and thus do not involve all the items. 


\section{Unified Framework}
\subsection{Ranking aggregation}

 If $\llbracket n \rrbracket = \left\{1,\dots,n\right\}$ designates a set of items and $\sigma_1,\dots,\sigma_N$ are ordered lists (seen as permutations) on $\llbracket n \rrbracket$ representing preferences over the collection of items, ranking aggregation consists in finding a permutation $\sigma^{*}$ that best “summarizes” the collection $(\sigma_1,\dots,\sigma_N)$. 

%\subsubsection{Consensus formulation}

One of the most used formulation of this problem is the Kemeny definition of a consensus (1959), is to search the “consensus ranking” $\sigma^{*}$ that minimizes the sum of the distances to the
$\sigma_t$’s, i.e. a solution to the minimization problem: $\min_{\sigma \in \mathfrak{S}_n} \sum_{t=1}^{N} d(\sigma,\sigma_t)$, where $d$ is a given metric
on $\mathfrak{S}_n$, the set of permutations on $\llbracket n \rrbracket$.  Such an element always exists, as $\mathfrak{S}_n$ is finite, but is not necessarily unique. Instead of dealing with the collection of permutations $(\sigma_1,\dots,\sigma_N)$, we can reformulate the consensus ranking problem for any real-valued function $p$ on $\mathfrak{S}_n$ such that the consensuses of $p$ with respect to a metric $d$ are the solutions of:
\begin{equation*}
\min_{\sigma \in \mathfrak{S}_n} \sum_{\pi \in \mathfrak{S}_n} d(\sigma,\pi)p(\pi) =: R_{d,p}(\sigma).
\end{equation*}
%This formulation thus extends the one of a consensus of a collection of permutations $(\sigma_1,\dots,\sigma_N) \in \mathfrak{S}_n$ since:
%\begin{equation*}
%\sum_{t=1}^{N}d(\sigma,\sigma_t)=R_{d,p_N}(\sigma)\quad \text{with}\quad p_N(\pi)=|\left\{1 \le i  \le N |\sigma_i =\pi \right\}|.
%\end{equation*}
For a metric $d$ on $\mathfrak{S}_n$, we denote by $d_{max} = max_{\sigma, \pi \in \mathfrak{S}_n} d(\sigma,\pi)$ its diameter
and we consider the $n!\times n!$ matrix $T_d$ defined by $T_d(\sigma,\pi)$ = $d_{max}-d(\sigma,\pi)$ for $\sigma,\pi \in \mathfrak{S}_n$. The cost function is then equal to $R_{d,p}(\sigma) = C - T_{d}p(\sigma)$ with $C \in \mathbb{R}$ a constant.

\subsection{Kernels on permutations}

$k(\sigma,\pi)= \langle \phi(\sigma), \phi(\pi) \rangle$ +Gram matrices..
 
\subsection{Link}

\textbf{Proposition 1.} \textit{For $(\sigma_1, \dots, \sigma_N) \in \mathfrak{S}_n$ and $d(\sigma, \pi)= d_{max}-k(\sigma,\pi)$, if $\left\| \phi(\sigma)\right\|^2$ is constant,\\
$\min_{\sigma \in \mathfrak{S}_n} \sum_{i=1}^{N} d(\sigma,\sigma_i) = \min_{\sigma \in \mathfrak{S}_n} \left\| \phi(\sigma) - \frac{1}{N} \sum_{i=1}^{N}\phi(\sigma_i)\right\|^2$}

\textit{Proof.} 
\begin{align*}
\min_{\sigma \in \mathfrak{S}_n} \left\| \phi(\sigma) - \frac{1}{N} \sum_{i=1}^{n}\phi(\sigma_i)\right\|^2&=\min_{\sigma \in \mathfrak{S}_n} [\left\| \phi(\sigma)\right\|^2 - \frac{2}{N}\sum_{i=1}^{N}\langle \phi(\sigma), \phi(\sigma_i) \rangle + \frac{1}{N^2}\sum_{i=1}^{N} \left\| \phi(\sigma_i) \right\|^2]\\
&=\min_{\sigma \in \mathfrak{S}_n} [-\frac{2}{N}\sum_{i=1}^{N}\langle \phi(\sigma), \phi(\sigma_i) \rangle ]\\
&=\max_{\sigma \in \mathfrak{S}_n} [\sum_{i=1}^{N}\langle \phi(\sigma), \phi(\sigma_i) \rangle]\\
&=\max_{\sigma \in \mathfrak{S}_n} \sum_{i=1}^{N}k(\sigma,\sigma_i)\\
&=\min_{\sigma \in \mathfrak{S}_n} \sum_{i=1}^{N}d(\sigma,\sigma_i)\\
\end{align*}


\textbf{Proposition 2.} \textit{For $p \in L(\mathfrak{S}_n)$ and $d(\sigma, \pi)= d_{max}-k(\sigma,\pi)$, if $\left\| \phi(\sigma)\right\|^2$ is constant,\\ 
	$\min_{\sigma \in \mathfrak{S}_n} d(\sigma,\pi)p(\pi) = \min_{\sigma \in \mathfrak{S}_n} \left\| \phi(\sigma) -  \sum_{\sigma \in \mathfrak{S}_n}\phi(\pi)p(\pi)\right\|^2$}

\textbf{Conclusion}: We use the same matrices which are matrices of distances.


\section{Results for usual distances}

\subsection{Preliminary on the Fourier decomposition and the MRA}


\noindent Traditional methods in machine-learning and statistics quickly become either intractable or inaccurate in practice for preference data. Summarizing ranking variability is not simple and extending simple concepts such as an average or a median in the context of preference data raises a certain number of deep mathematical and computational problems and call for new constructions. One approach, much documented in the literature consists in exploiting the algebraic structure of the noncommutative group $\mathfrak{S}_n$ and perform a harmonic analysis on $L(\mathfrak{S}_n)=\left\{f: \mathfrak{S}_n \rightarrow \mathbb{R} \right\}$.  

\textbf{Fourier}

We recall that the ordinary Fourier decomposition allows to write any function $h : [0,1] \rightarrow \mathbb{C}$ as a linear combination of trigonometric basis functions: $h(x)=\sum_{m=-\infty}^{\infty} \alpha_m e^{2i\pi m x}$ where $\alpha_m \in \mathbb{C}$ for each $m$. The mapping ($h \mapsto \left\{\alpha_m\right\}$) is called the \textit{Fourier transform}. On finite non-commutative groups the ordinary Fourier transform must be replaced by its non-commutative generalization and in the case of the symmetric group this takes the form of:
\begin{equation*}
\hat{h}_{\lambda} = \sum_{\sigma \in \mathfrak{S}_n} h(\sigma)\rho_{\lambda}(\sigma)
\end{equation*}
where $\lambda$ is a partition of $n$ (we denote $\lambda \vdash n$), that is to say a sequence $\lambda = (\lambda_1, \dots, \lambda_r) \in \llbracket n \rrbracket^{r}$ such that $\lambda_1 \ge \dots \ge \lambda_r$ and $\lambda_1 +\dots+\lambda_r = n$. There are two main differences with the usual Fourier transform. Firstly, instead of frequency, the individual Fourier components are indexed by partitions of $n$. Secondly, the inputs to $\hat{h}$ are complex-valued matrices $\rho_{\lambda}(\sigma)$ which are elements of the \textit{irreducible representation} of $\mathfrak{S}_n$ associated to $\lambda$ denoted $S_{\lambda}$. The definition of $S_{\lambda}$ and the construction of such matrices is deferred to the appendix.  The Fourier transform is thus a sequence $(\hat{h}_{\lambda})_{\lambda \vdash n}$ of matrices of different sizes (instead of a sequence of real numbers). If we denote $d_{\lambda}$ the dimension of $S_{\lambda}$, $\hat{h}_{\lambda} \in \mathbb{C}^{d_{\lambda}\times d_{\lambda}}$.

Some Fourier transforms have simple interpretations. For example, for $\lambda=(n-1,1)$, the $(i,j)$-th entry of the matrix $\hat{h}_{(n-1,1)}$ is
$[\hat{h}_{(n-1,1)}]_{i,j} =\sum_{\sigma \in \mathfrak{S}_n} h(\sigma) \rho_{(n-1,1)}(\sigma)=\sum_{\sigma \in \mathfrak{S}_n, \sigma(i)=j} h(\sigma)$. So if $h$ is a probability distribution, then $\hat{h}_{(n-1,1)}$ is a matrix of \textit{first-order} marginal probabilities of the form $\mathbb{P}[\sigma(i)=j]$, where the $(i, j)$-th element is the marginal probability that a random permutation drawn from $h$ maps element $j$ to position $i$. Then,  $\hat{h}_{(n-2,2)}$ is a matrix of \textit{second-order unordered} marginal probabilities of the form $\mathbb{P}[\sigma(\left\{k,l\right\})=\left\{i,j\right\}]$ and $\hat{h}_{(n-2,1,1)}$ is a matrix of \textit{second-order ordered} marginal probabilities of the form $\mathbb{P}[\sigma(k,l)=(i,j)]$.

\textbf{MRA decomposition}


This framework has been introduced for the statistical analysis of incomplete rankings. 

We denote by $\Gamma_n$ the set of all incomplete rankings on $\llbracket n \rrbracket$ and for any subset $A \subset \llbracket n \rrbracket$, we set $\Gamma(A)=\left\{ \pi \in \Gamma_n | c(\pi) =A \right\}$  the set of all incomplete rankings with content $A$. The set $\Gamma_n$ thus contains
the set $\cup_{A \in \mathscr{P}(\llbracket n \rrbracket)} \Gamma(A)$.
Note that $\Gamma(\llbracket n \rrbracket)$ corresponds to $\mathfrak{S}_n$.  Finally we define $L(\Gamma_n) = \bigoplus_{A \in \mathscr{P}(\llbracket n \rrbracket)} L(\Gamma(A))$ the set of real-valued functions on incomplete rankings on $\llbracket n \rrbracket$.

We define the \textit{marginal operator} $M_A$ : $L(\Gamma_n) \rightarrow L(\Gamma(A))$ on $A \in \mathscr{P}(\llbracket n \rrbracket)$ by
$M_A f(\pi) = \sum_{\sigma \in \Gamma_n, \sigma_{|A}=\pi} f(\sigma) =\langle f, \mathds{1}_{\mathfrak{S}_n(\pi)} \rangle$.


Multiresolution analysis of incomplete rankings relies on one fondamental result: any function $f$ on $\mathfrak{S}_n$ can be decomposed as a sum of components that each localize the specific information of one marginal $M_{A} f$ for $A \in \mathscr{P}(\llbracket n \rrbracket)$, so that the marginal $M_{B} f$ on any subset $B \in \mathscr{P}(\llbracket n \rrbracket)$ only involves the components specific to the subsets $A \in \mathscr{P}(B)$. This is formalized in the following theorem, established in [8]. We denote by $V^0 = \mathbb{R}\mathds{1}\mathfrak{S}_n$ the one-dimensional space of constant functions in $L(\mathfrak{S}_n)$. 

\noindent \textbf{Theorem 1}  (Clémençon et al. (2014)) There exists a collection $(W_B)_{B \in P(\llbracket n \rrbracket)}$ of subspaces of $L(\mathfrak{S}_n)$ such that $L(\mathfrak{S}_n)=V^0 \oplus \bigoplus_{B \in \mathscr{P}(\llbracket n \rrbracket)} W_B$.

For any $A \in \mathscr{P}(\llbracket n \rrbracket)$, the subspace $W_A$ localizes the information that is specific to marginals on $A$ and not to marginals on other subsets

\noindent Multiresolution analysis allows to exploit the natural multiscale structure of the marginals $M_A f$ of any function $f \in L(\mathfrak{S}_n)$. \textbf{Here, the notion of scale corresponds to the number of items in the subset on which the marginal is considered}. For $k \in \left\{2, \dots, n \right\}$, we define the space of scale $k$ by $W^k = \bigoplus_{|A|=k} W_A$. One thus obtains $L(\mathfrak{S}_n)=V^0 \oplus \bigoplus_{k=2}^{n} W^k$. This last decomposition of $L(\mathfrak{S}_n)$  is analogous to classic multiresolution analysis on $L_2(\mathbb{R})$ and can be similarly interpreted: if one projects a function $f \in L(\mathfrak{S}_n)$ on $V^0 \oplus \bigoplus_{k=2}^{K} W^k$ with $K \in \left\{2, \dots, n \right\}$ then he can only “see” the information of $f$ of scale up to $K$. The decomposition of $L(\mathfrak{S}_n)$ given by Theorem 1 not being however orthogonal, no wavelet basis can be orthonormal.



\textbf{Overview}



\noindent According to the Young's rule (see ...) and the MRA decomposition we get a multiple decomposition for $L(\mathfrak{S}_n)$. For example for $n$=4 we get the following:


\begin{equation*}
\begin{array}{cccccccccc} L(\mathfrak{S}_4) =  &  W^{4} &\cong& S^{(3,1)} &\bigoplus& S^{(2,2)} &\bigoplus& S^{(2,1,1)} &\bigoplus& S^{(1,1,1,1)}\\
& \bigoplus &&&\\
& W^{3} &\cong& S^{(3,1)} &\bigoplus& S^{(2,2)} &\bigoplus& S^{(2,1,1)}\\
& \bigoplus &&&\\
& W^{2} &\cong& S^{(3,1)} &\bigoplus& S^{(2,1,1)}\\
& \bigoplus &&&\\
& V^{0} & \cong& S^{(4)}
\end{array}
\end{equation*}

\noindent So:
\begin{equation*}
L(\mathfrak{S}_4)=S^{(4)}\bigoplus 3. S^{(3,1)} \bigoplus 2. S^{(2,2)} \bigoplus 3. S^{(2,1,1)} 
\bigoplus S^{(1,1,1,1)}
\end{equation*}
\noindent That we can write:
\begin{equation*}
L(\mathfrak{S}_4)=U^{(4)}\bigoplus U^{(3,1)} \bigoplus U^{(2,2)} \bigoplus U^{(2,1,1)} 
\bigoplus U^{(1,1,1,1)}
\end{equation*}

\noindent All these spaces correspond to different information levels, and for some methods of ranking aggregation, we make a projection onto one or several of these spaces. 


\subsection{Examples}

\textbf{Kendall= projection on $W^2$}

Ranking aggregation:
\begin{equation*}
R_{d,p}(\sigma)= \sum_{1 \le i \ne j \le n} \langle p, \mathds{1}_{\left\{\sigma(i) > \sigma(j) \right\}} \rangle
\mathds{1}_{\left\{\sigma(i)<\sigma(j)\right\}}(\sigma)
\end{equation*}
Kendall kernel: $k(\sigma,\pi)=\binom {n}{2} - d_{K}(\sigma,\pi)= \langle \phi(\sigma), \phi(\pi) \rangle$ with:
\begin{equation*}
\phi(\sigma)= (\frac{1}{\sqrt{ \binom {n} {2}}}(\mathds{1}_{\left\{\sigma(i) > \sigma(j) \right\}} - \mathds{1}_{\left\{\sigma(i)<\sigma(j)\right\}}))_{1 \le i < j \le n}
\end{equation*}

\textbf{Hamming= projection on $U^{(n-1,1)} $}

Ranking aggregation:
\begin{equation*}
R_{d,p}(\sigma)=\sum_{i,j=1}^{n} \langle p, \mathds{1}_{\left\{\sigma(i) \ne j\right\}} \rangle \mathds{1}_{\left\{\sigma(i) = j\right\}}(\sigma)
\end{equation*}
...

\textbf{Borda = $U^{(n-1,1)} \cap W^2$}
...

BUT mode/$l_2$ distance don't filter.


\section{Applications}



\section{Conclusion}



\section*{References}

\small{
[1] Alexander, J.A. \& Mozer, M.C. (1995) Template-based algorithms
for connectionist rule extraction. In G. Tesauro, D. S. Touretzky
and T.K. Leen (eds.), {\it Advances in Neural Information Processing
Systems 7}, pp. 609-616. Cambridge, MA: MIT Press.

[2] Renteln, P. (2011). The distance spectra of Cayley graphs of Coxeter groups. Discrete Mathematics, 311(8), 738-755.

[3] Reiner, V., Saliola, F., \& Welker, V. (2014). Spectra of symmetrized shuffling operators (Vol. 228, No. 1072). American mathematical society.

[4] Sibony, E. Borda count approximation of Kemeny’s rule and pairwise voting inconsistencies.

[5] Jiao, Y., \& Vert, J. P. (2015). The Kendall and Mallows Kernels for Permutations. In Proceedings of the 32nd International Conference on Machine Learning (ICML-15) (pp. 1935-1944).

[6] Clémençon, S., \& Jakubowicz, J. (2010). Kantorovich distances between rankings with applications to rank aggregation. In Machine Learning and Knowledge Discovery in Databases (pp. 248-263). Springer Berlin Heidelberg.

[7] Kondor, R., \& Barbosa, M. S. (2010, June). Ranking with Kernels in Fourier space. In COLT (pp. 451-463).

[8] Clémençon, S., Jakubowicz, J., \& Sibony, E. (2014). Multiresolution analysis of incomplete rankings. arXiv preprint arXiv:1403.1994.

}




\end{document}