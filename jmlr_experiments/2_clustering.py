# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from itertools import combinations
import csv
import random
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.decomposition import RandomizedPCA,TruncatedSVD
import cPickle as pickle
import scipy.sparse as sps
from scipy.sparse import csr_matrix
from collections import Counter


##########################################
#                                        #
#    Embedding of users rankings         #
#                                        #
##########################################
#n_movies=20
#df2=pd.read_csv('save_data/df2.csv')

#dict_movie_pairs_index={v:u for u,v in enumerate(combinations(range(1,n_movies+1),2))}  

def KemenyEmbedSparse(df):
    n_users=len(np.unique(df['user_id']))
    n_movies=len(np.unique(df['movie_id']))
    n_rows=n_users
    n_columns=(n_movies*(n_movies-1))/2
    rows, cols = n_rows, n_columns
    print n_movies
    print rows,cols
    sps_acc = sps.coo_matrix((rows, cols)) # empty matrix
    for i in range(1,n_rows+1): 
        print i
        rows_i=[]
        phi_i=[]
        columns_i=[]
        df_user=df[df['user_id']==i]
        for (j,k) in combinations(df_user['movie_id'],2):
            rating_j=df_user[df_user['movie_id']==j]['rating'].values[0]
            rating_k=df_user[df_user['movie_id']==k]['rating'].values[0]
            if rating_j<>rating_k:
                rows_i.append(i-1)
                if j<k:
                    pair=(j,k)
                else:
                    pair=(k,j)
                columns_i.append(dict_movie_pairs_index[pair])
                phi_i.append(np.sign(pair[0]-pair[1]))   
        sps_acc = sps_acc + sps.coo_matrix((phi_i, (rows_i, columns_i)), shape=(rows, cols))
    return sps_acc
    
def save_sparse_csr(filename,array):
    np.savez(filename,data = array.data ,indices=array.indices,
             indptr =array.indptr, shape=array.shape )

def load_sparse_csr(filename):
    loader = np.load(filename)
    return csr_matrix((  loader['data'], loader['indices'], loader['indptr']),
                         shape = loader['shape'])


#print 'embedding of users rankings'
#data_embed=KemenyEmbedSparse(df2)
#save_sparse_csr('save_data/data_embed.npz', data_embed)


##########################################
#                                        #
#             Clustering                 #
#                                        #
##########################################


#print 'load data embed'
#data_embed=load_sparse_csr('save_data/data_embed.npz')

print 'fit Kmeans'
K=10
labeler = KMeans(init='k-means++', n_clusters=K, n_init=10) 
# note: Kmeans currently only works with CSR type sparse matrix 
labeler.fit(data_embed.tocsr())  


# print cluster assignments for each row 
#for (row, label) in enumerate(labeler.labels_):   
#  print "row %d has label %d"%(row, label)


def k_means_and_svd(K,matrix):
    csr_x = sps.csr_matrix(matrix)
    #reduced_data = RandomizedPCA(n_components=2).fit_transform(csr_x)
    reduced_data = TruncatedSVD(n_components=2).fit_transform(csr_x)
    kmeans = KMeans(init='k-means++', n_clusters=K, n_init=10)
    kmeans.fit(reduced_data)
    
    # Step size of the mesh. Decrease to increase the quality of the VQ.
    h = .02     # point in the mesh [x_min, m_max]x[y_min, y_max].
    
    # Plot the decision boundary. For that, we will assign a color to each
    x_min, x_max = reduced_data[:, 0].min() + 1, reduced_data[:, 0].max() - 1
    y_min, y_max = reduced_data[:, 1].min() + 1, reduced_data[:, 1].max() - 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    
    # Obtain labels for each point in mesh. Use last trained model.
    Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])
    
    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.figure(1)
    plt.clf()
    plt.imshow(Z, interpolation='nearest',
               extent=(xx.min(), xx.max(), yy.min(), yy.max()),
               cmap=plt.cm.Paired,
               aspect='auto', origin='lower')
    
    plt.plot(reduced_data[:, 0], reduced_data[:, 1], 'k.', markersize=2)
    # Plot the centroids as a white X
    centroids = kmeans.cluster_centers_
    plt.scatter(centroids[:, 0], centroids[:, 1],
                marker='x', s=169, linewidths=3,
                color='w', zorder=10)
    plt.title('K-means clustering on the Movie Lens dataset (SVD-reduced data)\n'
              'Centroids are marked with white cross')
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.xticks(())
    plt.yticks(())
    plt.savefig('save_data/clustering_'+str(K)+'/kmeans.jpg')
    plt.show()


# plot: 
k_means_and_svd(K,data_embed)


# nb of observations by cluster: 
print Counter(labeler.labels_)

##########################################
#                                        #
#     Assign users to clusters           #
#                                        #
##########################################

index_users=np.unique(df2['user_id'])


print 'create cluster datasets and record'
all_rankings=[]
for k in range(K):
    all_rankings.append([])

K_means_indexes=labeler.labels_
for i,j in enumerate(K_means_indexes):
    user=index_users[i]
    print user
    user_rankings=dict_rankings[user]
    all_rankings[j]+=user_rankings

import random
all_rankings_sample=[]
for k in range(K):
    all_rankings_sample.append([])
for k in range(K):
    all_rankings_sample[k]=random.sample(all_rankings[k], 5000)

for k in range(1,K+1):
    print k
    with open('save_data/clustering_'+str(K)+'/all_rankings_sample'+str(k)+'.csv', "wb") as f:
        writer = csv.writer(f,delimiter=';', lineterminator='\n',quoting=csv.QUOTE_NONE)
        writer.writerows(all_rankings_sample[k-1])

"""        
for k in range(1,K+1):
    print k
    with open('save_data/clustering_'+str(K)+'/all_rankings'+str(k)+'.csv', "wb") as f:
        writer = csv.writer(f,delimiter=';', lineterminator='\n',quoting=csv.QUOTE_NONE)
        writer.writerows(all_rankings[k-1])
"""
        
#with open('movielens/all_rankings.csv', 'rb') as handle:
#  all_rankings = pickle.loads(handle.read())


##########################################
#                                        #
#        Clustering with PCC             #
#                                        #
##########################################

"""

def PCC(df,i,j,n): #i,j users
    r=0
    user_i=df[df['user_id']==i]
    user_j=df[df['user_id']==j]
    movies_i=user_i['movie_id'].values
    movies_j=user_j['movie_id'].values
    common_rated=np.intersect1d(movies_i, movies_j)
    if len(common_rated)>=n:
        x=user_i[user_i['movie_id'].isin(common_rated)]['rating'].values
        y=user_j[user_j['movie_id'].isin(common_rated)]['rating'].values
        x_=sum(x)/len(x)
        y_=sum(y)/len(y)
        for i in range(len(x)):
            r+=(x[i]-x_)*(y[i]-y_)
        if r<>0:
            x_sq=np.power(x-x_*np.ones(len(x)),2)
            y_sq=np.power(y-y_*np.ones(len(y)),2)
            r/=(np.sqrt(sum(x_sq))*np.sqrt(sum(y_sq))*1.0)
        return r
    else:
        return False

        
# http://delivery.acm.org/10.1145/1870000/1864733/p119-baltrunas.pdf?ip=137.194.2.124&id=1864733&acc=ACTIVE%20SERVICE&key=7EBF6E77E86B478F%2E2E41AE6F08F798A5%2E4D4702B0C3E38B35%2E4D4702B0C3E38B35&CFID=696740756&CFTOKEN=39031633&__acm__=1483719916_fe152d2b73609a881f5fb7c5da48c3e6
# http://people.uta.fi/~kostas.stefanidis/docs/er12.pdf

def PCC_for_all(df,n):
    n_users=len(np.unique(df['user_id']))
    pcc_for_all=np.empty((n_users,n_users))
    pcc_for_all[:] = numpy.NAN
    for (i,j) in combinations(range(1,n_users+1),2):
        pcc=PCC(df,i,j,n)
        pcc_for_all[i-1,j-1]=pcc
        pcc_for_all[j-1,i-1]=pcc
    #pcc_for_all=pcc_for_all[~np.isnan(pcc_for_all).any(axis=1)]
    return pcc_for_all
    
    
test=PCC_for_all(df2[df2['user_id']<100],3)
"""