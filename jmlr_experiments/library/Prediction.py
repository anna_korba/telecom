# -*- coding: utf-8 -*-
from itertools import *
from math import *
from collections import Counter
from sets import Set



# General functions

def load_data(data_file):
    Data = []
    f = open(data_file,'r')
    for line in f:
        Data.append(eval(line))
    f.close()
    print('Data loaded')
    return Data


def training_test_lists(Data,nb_split,indexes_for_training):   
    N = len(Data)
    Training = []
    Test = []
    for i in range(nb_split):
        if i in indexes_for_training:
            Training.extend(Data[N*i/nb_split:N*(i+1)/nb_split])
        else:
            Test.extend(Data[N*i/nb_split:N*(i+1)/nb_split])
    return (Training,Test)

def mu_and_probabilities(L):
    N = len(L)
    C = Counter()
    mu = {}
    for w in L:
        C[w] += 1
    for w in L:
        t = tuple(sorted(w))
        if t in mu:
            mu[t] += C[w]
        else:
            mu.update({t:C[w]})
    p = dict.fromkeys(mu)
    for A in mu:
        p[A] = {}
        for pi in permutations(A):
            p[A].update({pi:C[pi]/(1.0*mu[A])})
        mu[A] = mu[A]/(1.0*N)
    return (mu,p)


# Analysis functions

def mean_variance(distance,k):
    m = 0
    v = 0
    w = tuple(range(1,k+1))
    for t in permutations(w):
        m += distance(w,t)
        v += distance(w,t)**2
    m = m/(1.0*factorial(k))
    v = v/(1.0*factorial(k))-m**2
    return(m,v)

def Mean_Variance(Distances,Sizes):
    D = dict.fromkeys(Distances)
    for d in Distances:
        MV = dict.fromkeys(Sizes)
        distance = Distance(d)
        for k in Sizes:
            MV[k] = mean_variance(distance,k)
        D[d] = dict(MV)
    return D

def Mean_Standard_Deviation(Distances,MV,L):
    Result = dict.fromkeys(Distances)
    M = dict.fromkeys(Distances,0)
    Sigma = dict.fromkeys(Distances,0)
    for w in L:
        for d in Distances:
            D = MV[d]
            t = D[len(w)]
            M[d] += t[0]
            Sigma[d] += t[1]
    for d in Distances:
        M[d] = M[d]/(1.0*len(L))
        Sigma[d] = sqrt(Sigma[d])/(1.0*len(L))
        Result[d] = (M[d],Sigma[d])
    return Result


def predictions(q,mu,r): # r is the name of the rule
    rule = prediction_rule(r)
    Pi = {}
    for A in mu:
        Pi.update({A:rule(A,q[A])})
    return Pi

def local_risk(A,pi,p,d): # d is the name of the distance
    distance = Distance(d)
    R = 0
    for w in permutations(A):
        R += distance(pi,w)*p[w]
    return R

def sparse_local_risk(A,pi,p,d): # d is the name of the distance
    distance = Distance(d)
    R = 0
    for w in p:
        R += distance(pi,w)*p[w]
    return R

def global_risk(Predictions,mu,p,d):
    R = 0
    for A in mu:
        R += mu[A]*local_risk(A,Predictions[A],p[A],d)
    return R
    
# Computes the risks of a plug-in predictor from mu and probabilities
def risks(q,mu,p,Evaluation): # Evaluation is a dictionary {(prediction rule, [distances])}
    Result = dict.fromkeys(Evaluation)
    for r in Evaluation:
        Result[r] = dict.fromkeys(Evaluation[r])
        Predictions = predictions(q,mu,r)
        for d in Evaluation[r]:
            Result[r][d] = global_risk(Predictions,mu,p,d)
    return Result


# Computes the risk from Test list
"""
def risks_from_list(Estimations,L_test,Evaluation):
    # Evaluation is a dictionary (prediction rule, [distances])
    Result = dict.fromkeys(Evaluation)
    N_test = len(L_test)
    for r in Evaluation:
        Predictions = {}
        rule = prediction_rule(r)
        D = dict.fromkeys(Evaluation[r],0)
        for w in L_test:
            t = tuple(sorted(w))
            if t not in Predictions:
                pi = rule(t,Estimations)
                Predictions.update({t:pi})
            else:
                pi = Predictions[tuple(sorted(w))]
            for d in D:
                distance = Distance(d)
                D[d] += distance(pi,w)
        for d in D:
            D[d] = D[d]/(1.0*N_test)
        Result[r] = dict(D)
    return Result
"""

# Estimation is an empty dictionary
def evaluation_regularized_estimation(n,space,Coefficients,mu,p,Evaluation,Results,m):
    print(space)
    q = dict.fromkeys(mu)
    for A in mu:
        q[A] = {}
        for w in permutations(A):
            q[A].update({w:1/(1.0*factorial(len(w))) + pairwise_estimator(n,w,Coefficients,space)})
    Risks = risks(q,mu,p,Evaluation)
    for r in Evaluation:
        for d in Evaluation[r]:
            Results[m][space][r][d] += Risks[r][d]


def evaluation_Kemeny_estimation(n,Borda,Condorcet,mu,p,Evaluation,Results,m):
    print('Kemeny')
    q = dict.fromkeys(mu)
    for A in mu:
        q[A] = {}
        for w in permutations(A):
            q[A].update({w:1/(1.0*factorial(len(w))) + pairwise_estimator_Kemeny(n,w,Borda,Condorcet)})
    Risks = risks(q,mu,p,Evaluation)
    for r in Evaluation:
        for d in Evaluation[r]:
            Results[m]['Kemeny'][r][d] += Risks[r][d]


def constant_estimator(mu):
    q = dict.fromkeys(mu)
    for A in mu:
        q[A] = {}
        for w in permutations(A):
            q[A].update({w:1/(1.0*factorial(len(w)))})
    return q

def pairwise_estimaton(q,estimator,PM):
    for A in q:
        for w in permutations(A):
            q[A][w] += estimator(w,PM)

def evaluation(q,mu,p,Evaluation,D,name):
    Risks = risks(q,mu,p,Evaluation)
    for r in Evaluation:
        for d in Evaluation[r]:
            D[name][r][d] += Risks[r][d]

    

