# -*- coding: utf-8 -*-
from itertools import *
from math import *
from Sampling import *
from Operators import *
from General_functions import *


def draw_dataset(P,nu,N): # Here P is a dictionary of marginals
    D = []
    for i in range(N):
        A = sample_from(nu)
        pi = sample_from(P[A])
        D.append((A,pi))
    return D

def error(P,nu,Q): # P (resp Q) is the collection of true (resp estimated) marginals
    E = 0
    for A in nu:
        E += nu[A]*squared_l2_norm(chain_soustraction(Q[A],P[A]))
    return E

"""
def mean_error(n,p,nu,estimator,parameter,N,nb_MC_samples):
    Design = []
    for A in nu:
        if nu[A] > 0:
            Design.append(A)
    P = dict.fromkeys(Design)
    for A in P:
        P[A] = dict(marginal(p,A))
    E = 0
    for i in range(nb_MC_samples):
        Dataset = draw_dataset(P,nu,N)
        Q = estimator(Dataset, Design, n, parameter)
        E += error(P,nu,Q)
    return E/(1.0*nb_MC_samples)
"""

# Computes the empirical nu from the Dataset
def empirical_nu(Dataset):
    L = []
    for (A,pi) in Dataset:
        L.append(A)
    return histogram(L)

# Computes the empirical marginals from the Dataset
def empirical_marginals(Dataset):
    D = {}
    for (A,pi) in Dataset:
        if A not in D:
            D.update({A:[pi]})
        else:
            D[A].append(pi)
    P = dict.fromkeys(D)
    for A in D:
        P[A] = histogram(D[A])
    return P

# Computes the empirical marginals for the sets of the Design
def empirical_estimator(Dataset, Design, n, parameter):
    P = empirical_marginals(Dataset)
    Q = dict.fromkeys(Design)
    for A in Design:
        if A in P:
            Q[A] = P[A]
        else:
            Q[A] = dict.fromkeys(permutations(A),1/(1.0*factorial(len(A))))
    return Q
    
