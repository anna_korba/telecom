# -*- coding: utf-8 -*-
from itertools import *
from math import *
from random import *
from General_functions import *


# Draws a sample from a finite probability distribution p over a finite space X (encoded in its keys)
def sample_from(p):
    X = {}
    Q = []
    q = 0
    i = 0
    for x in p:
        if p[x] > 0:
            q += p[x]
            Q.append(q)
            X.update({i:x})
            i += 1
    U = random()
    i = 0
    while U > Q[i]:
        i += 1
    return X[i]


# Draws a random probability distribution uniformly on the simplex
def random_probability(A):
    k = len(A)
    X = [0]
    for i in range(k-1):
        U = random()
        X.append(U)
    X.sort()
    p = dict.fromkeys(A)
    L = list(A)
    for i in range(k-1):
        p[L[i]] = X[i+1]-X[i]
    p[L[k-1]] = 1 - X[k-1]
    return p


# Draws a random probability distribution uniformly on Sn
def random_probability_Sn(n):
    return random_probability(list(permutations(range(1,n+1))))

# Draws a random probability distribution on P([n]) uniformly
def random_probability_Pn(n):
    return random_probability(subsets_of_set(range(1,n+1)))

# Draws a random probability distribution on P([n]) of sparsity k uniformly
def random_sparse_probability_Pn(n,k):
    L = sample(subsets_of_set(range(1,n+1)),k)
    return random_probability(L)

# Draws a random probability distribution on P([n]) with weights for the cardinalities
# W is a probability distribution on {2,...,n}
def random_weighted_probability_Pn(n,W):
    k = sample_from(W)
    return random_probability(subsets_of_size(range(1,n+1),k))


"""
# Draws a random 0_mean function on Sn (not uniformly)
def random_0_mean(n):
    p = dict.fromkeys(permutations(range(1,n+1)))
    N = 0
    for pi in p:
        x = random()
        p[pi] = x
        N += x
    for pi in p:
        p[pi] -= N/(factorial(n)*1.0)
    return p
"""

