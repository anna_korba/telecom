# -*- coding: utf-8 -*-
from itertools import *
from math import *


"""
All distances are defined for two incomplete rankings w and t of same content.
Normalized distances are normalized with respect to the diameter.
"""

# Correspondence between distances and distance names, in order to iterate over a set of distances
def Distance(d):
    if d == '01_loss':
        return normalized_01
    elif d == 'Kendall_loss':
        return normalized_Kendall_tau
    elif d == 'Kendall_tau':
        return Kendall_tau
    elif d == 'L1_loss':
        return normalized_l1
    elif d == 'L2_loss':
        return normalized_l2
    


# 0-1 Loss
def normalized_01(w,t):
    if w == t:
        return 0
    else:
        return 1


# Kendall's tau
def Kendall_tau(w,t):
    d = 0
    r_w = dict.fromkeys(w)
    r_t = dict.fromkeys(t)
    for i in range(len(w)):
        r_w[w[i]] = i+1
        r_t[t[i]] = i+1
    for c in combinations(w,2):
        if (r_w[c[1]]-r_w[c[0]])*(r_t[c[1]]-r_t[c[0]]) < 0:
            d += 1
    return d

def normalized_Kendall_tau(w,t):
    d = Kendall_tau(w,t)
    k = len(w)
    return d*2/(1.0*k*(k-1))


# L1 distance, also called Spearman's footrule
def l1(w,t):
    d = 0
    k = len(w)
    R_w = {}
    R_t = {}
    for i in range(k):
        R_w.update({w[i]:i+1})
        R_t.update({t[i]:i+1})
    for a in w:
        d += fabs(R_w[a]-R_t[a])
    return d

def normalized_l1(w,t):
    d = l1(w,t)
    k = len(w)
    return d/(1.0*2*floor(k/2)*floor((k+1)/2))


# L2 distance, also called Spearman's rho
def l2(w,t):
    d = 0
    k = len(w)
    R_w = {}
    R_t = {}
    for i in range(k):
        R_w.update({w[i]:i+1})
        R_t.update({t[i]:i+1})
    for a in w:
        d += (R_w[a]-R_t[a])**2
    return sqrt(d)

def normalized_l2(w,t):
    k = len(w)
    d = l2(w,t)
    return d*sqrt(3/(1.0*(k-1)*k*(k+1)))


# Hamming distance
def Hamming(w,t):
    d = 0
    k = len(w)
    for i in range(k):
        if w[i] != t[i]:
            d += 1
    return d

# To print the distances
def Distance_name(d):
    if d == normalized_01:
        return '01_loss'
    elif d == Kendall_tau:
        return 'Kendall_tau'
    elif d == l1:
        return 'L1_loss'
    elif d == l2:
        return 'L2_loss'
    elif d == Hamming:
        return 'Hamming'


