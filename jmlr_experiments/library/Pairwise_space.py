# -*- coding: utf-8 -*-
from math import *
from collections import Counter
from itertools import *
from General_functions import *





# Projection d'une fonction f dans L(Sn) sur l'espace Borda
def projection_Borda(f,n):
    theta = dict.fromkeys(range(1,n+1),(n+1)/(2*1.0))
    for pi in f:
        for i in range(n):
            theta[pi[i]] -= (i+1)*f[pi]
    P = dict.fromkeys(permutations(range(1,n+1)),0)
    for pi in P:
        for i in range(n):
            P[pi] += theta[pi[i]]*((n+1)/(2*1.0)-(i+1))
        P[pi] = P[pi]*12/(n*factorial(n+1)*1.0)
    return P


# Projection d'une fonction f dans L(Sn) sur l'espace W2
def projection_W2(f,n):
    X = dict.fromkeys(permutations(range(1,n+1),2),0)
    for pi in f:
        for w in combinations(pi,2):
            Y = projection_X(w)
            for t in Y:
                X[t] += Y[t]*f[pi]
    P = dict.fromkeys(permutations(range(1,n+1)),1/(factorial(n)*1.0))
    for pi in P:
        P[pi] += estimator(pi,X,2)
    return P


def Borda_scores(f,n):
    theta = dict.fromkeys(range(1,n+1),(n+1)/(2*1.0))
    for pi in f:
        for i in range(n):
            theta[pi[i]] -= (i+1)*f[pi]
    return theta

def Borda_scores_from_pairwise(P,n):
    theta = dict.fromkeys(range(1,n+1),0)
    for i in theta:
        for j in set(range(1,n+1)) - set([i]):
            theta[i] += P[(i,j)]-0.5
    return theta
    
    

# Calcul des coefficients pairwise à partir des marginales pairwise P
def pairwise_coefficients(P,n,space):
    if space == 'Borda':
        parameter = {'alpha':2, 'beta':-(n-1), 'gamma':1, 'delta':n*(n+1)}
    elif space == 'Condorcet':
        parameter = {'alpha':(n-2), 'beta':(n-2)/(1.0*2), 'gamma':-1, 'delta':n}
    elif space == 'Kemeny':
        parameter = {'alpha':(n-1), 'beta':(n-3)/(1.0*2), 'gamma':-1, 'delta':(n+1)}
    C = {}
    for (i,j) in combinations(range(1,n+1),2):
        x = parameter['alpha']*P[(i,j)] + parameter['beta']
        for k in set(range(1,n+1))-set((i,j)):
            x += parameter['gamma']*(P[(i,k)] + P[(k,j)])
        x *= (6/(1.0*factorial(n)*parameter['delta']))
        C.update({(i,j):x, (j,i):-x})
    return C


# Calcul de la norme l2 au carré à partir des coefficients pairwise
def pairwise_squared_norm(Coefficients,n,space):
    N = 0
    for c in combinations(range(1,n+1),2):
        N += Coefficients[c]**2
    if space == 'Borda':
        N *= factorial(n+1)/3
    if space == 'Condorcet':
        N *= factorial(n)/3
    return N


# Les coefficients doivent être les mêmes que l'espace
def pairwise_estimator(n,w,Coefficients,space):
    k = len(w)
    p = 0
    if space == 'Borda':
        for i in range(k/2):
            p += (k-1-2*i)*Coefficients[(w[i],w[k-i-1])]
        p *= n
    for (i,j) in combinations(range(k),2):
        p += (k+1-2*(j-i))*Coefficients[(w[i],w[j])]
    p *= factorial(n)/(1.0*factorial(k+1))
    return p


def pairwise_estimator_Kemeny(n,w,Borda,Condorcet):
    k = len(w)
    p = 0
    for i in range(k/2):
        p += (k-1-2*i)*Borda[(w[i],w[k-i-1])]
    p *= n
    for (i,j) in combinations(range(k),2):
        p += (k+1-2*(j-i))*(Borda[(w[i],w[j])] + Condorcet[(w[i],w[j])])
    p *= factorial(n)/(1.0*factorial(k+1))
    return p


def direct_pairwise_estimator_Kemeny(w,PM):
    p = 0
    k = len(w)
    for m in range(1,k):
        x = 0
        for i in range(k-m):
            x += PM[(w[i],w[i+m])]-0.5
        p += (k+1-2*m)*x
    p *= 6/(1.0*factorial(k+1))
    return p






    
