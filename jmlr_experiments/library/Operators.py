# -*- coding: utf-8 -*-
from math import *
from itertools import *



#####################################
#        Subword  operations        #
#####################################


def is_subword(w,w0):
    l0 = list(w0)
    l = []
    for i in w:
        l.append(w0.index(i))
    return l == sorted(l)


def subwords_of_size(w,k):
    if len(w) < k:
        return []
    else:
        l = []
        for c in combinations(range(len(w)),k):
            t = (w[c[0]],)
            for i in range(1,k):
                t = t + (w[c[i]],)
            l.append(t)
        return l


def all_subwords(w):
    l = []
    for k in range(2,len(w)+1):
        l.extend(subwords_of_size(w,k))
    return l


def induced_subword(w,A):
    t = ()
    for i in w:
        if i in A:
            t = t + (i,)
    return t



######################################
#        Marginals operations        #
######################################


def marginal(p,A):
    P = {}
    for w in p:
        t = induced_subword(w,A)
        if t in P:
            P[t] += p[w]
        else:
            P.update({t:p[w]})
    return P


def scale_marginals_from_function(f,k):
    M = {}
    N = 0
    for w in f:
        N += f[w]
        for t in subwords_of_size(w,k):
            if t in M:
                M[t] += f[w]
            else:
                M.update({t:f[w]})
    for t in M:
        M[t] = M[t]/(1.0*N)
    return M


def pairwise_marginals_from_function(f):
    return scale_marginals_from_function(f,2)


# Compute marginals for scales k <= scale <= l
def marginals(L,k,l):
    C = Counter()
    for w in L:
        for j in range(k,l+1):
            for t in subwords_of_size(w,j):
                C[t] += 1
    M = {}
    for w in C:
        w0 = tuple(sorted(w))
        if w0 not in M:
            N = 0
            for t in permutations(w0):
                N += C[t]
            for t in permutations(w0):
                M.update({t:C[t]/(1.0*N)})
    return M


def pairwise_marginals(L):
    return marginals(L,2,2)


def pairwise_marginals_and_higher(L):
    C = Counter()
    for w in L:
        for t in all_subwords(w):
            C[t] += 1
    PM = {}
    M = {}
    for w in C:
        w0 = tuple(sorted(w))
        if (w0 not in PM) or (w0 not in M):
            N = 0
            for t in permutations(w0):
                N += C[t]
            if len(w0) == 2:
                for t in permutations(w0):
                    PM.update({t:C[t]/(1.0*N)})
            else:
                for t in permutations(w0):
                    M.update({t:C[t]/(1.0*N)})
    return (PM,M)


def pairwise_marginals_from_list(L,n):
    C = Counter()
    for w in L:
        for t in subwords_of_size(w,2):
            C[t] += 1
    PM = {}
    for (i,j) in combinations(range(1,n+1),2):
        N = C[(i,j)] + C[(j,i)]
        PM.update({(i,j):C[(i,j)]/(1.0*N),(j,i):C[(j,i)]/(1.0*N)})
    return PM





