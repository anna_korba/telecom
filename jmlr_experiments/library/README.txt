This document explains how to use the library.

The folder 'wavelet analysis' contains old code related to wavelet analysis, we do not consider it here.

The library has different files:
- Mathematical functions and operations:
	. General_functions
	. Operators
	. Sampling
	. Distances_on_rankings
	. Voting_rules
	. MRA_framework
- Functions to be used for specific purposes:
	. Estimation
	. Prediction
- Estimators:
	. Plackett-Luce
	. WLS_estimator
	. Pairwise_space
- An example file for estimation:
	. Example

