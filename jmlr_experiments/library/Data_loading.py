# -*- coding: utf-8 -*-
from itertools import *
from math import *



"""
This file contains functions that can be used to load data from classic datasets.
For each dataset, the function outputs:
- the number of items n
- the set of Items {1,...,n}
- the observation design
- the number of observations N
- the data itself - a distribution over rankings - implemented as a dictionary
"""


