from itertools import *
from math import *
from sets import *
from Operators import *
from Sampling import *
from Estimation import *
from Plackett_Luce import *
from WLS_estimator import *
from Lebanon import *

"""
Definition of the statistical setting
"""
n = 4                           # Number of items
nu = random_probability_Pn(n)   # Probability nu (here drawn at random)
Design = []                     # Observation design
for A in nu:
    if nu[A] > 0:
        Design.append(A)
p = random_probability_Sn(n)    # Ranking model p (here drawn at random)
P = dict.fromkeys(Design)       # Collection of marginals of p on the observation design
for A in P:
    P[A] = dict(marginal(p,A))
N = 1000                        # Number of observations

"""
List of models to test
"""
Models = [WLS_estimator, PL_estimator, empirical_estimator, positive_Lebanon_estimator]
Parameter = {WLS_estimator:0, PL_estimator:50, empirical_estimator:0, positive_Lebanon_estimator:binomial(n,2)}



"""
Computation of the error for each drawing of a dataset
"""
nb_MC_samples = 10              # Defines the number of drawings
Error = dict.fromkeys(Models)
for estimator in Models:
    Error[estimator] = []
for i in range(nb_MC_samples):
    Dataset = draw_dataset(P,nu,N)
    """
    For each model, computation of the estimator from the Dataset and its error on the Dataset
    """
    for estimator in Models:
        Q = estimator(Dataset, Design, n, Parameter[estimator])
        Error[estimator].append(error(P,nu,Q))


"""
Print the mean squared error for each estimator
"""
for estimator in Models:
    print(str(estimator) + ': ' + str(average(Error[estimator])))





