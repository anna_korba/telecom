# -*- coding: utf-8 -*-
from itertools import *
from math import *
from collections import Counter
from Distances_on_rankings import *


# Computes the Mallows distribution p(\sigma) = C.exp(-spread*Kendall(id,sigma))
def Mallows_distribution_centered(n,spread):
    Z = 1
    for j in range(1,n+1):
        Z *= (1-exp(-j*spread))/(1-exp(-spread))
    sigma0 = tuple(range(1,n+1))
    p = dict.fromkeys(permutations(range(1,n+1)))
    for sigma in p:
        p[sigma] = exp(-spread*Kendall_tau(sigma0,sigma))/Z
    return p
