# -*- coding: utf-8 -*-
from itertools import *
from math import *
from Distances_on_rankings import *


"""
All rules are defined for a set of items A and a probability P on the (full) rankings over A.
The set A is represented here as any enumerable over its elements (e.g. list, tuple, set, ...).
The Probability can be sparsely represented (only the non-null values are encoded)
"""

# Correspondence between rules and rule names, in order to iterate over a set of rules
def prediction_rule(r):
    if r == 'mode':
        return mode
    elif r == 'Kendall_consensus':
        return Kendall_consensus
    elif r == 'L1_consensus':
        return L1_consensus
    elif r == 'L2_consensus':
        return L2_consensus
    elif r == 'Borda':
        return Borda_count


# Returns the ranking with highest probability
def mode(A,P):
    pi = tuple(A)
    m = P[pi]
    for t in P:
        if P[t] > m:
            m = P[t]
            pi = t
    return pi


# Returns (one of) the Borda count(s) for P on A
def Borda_count(A,P):
    Borda = dict.fromkeys(A,0)
    for t in P:
        for i in range(len(A)):
            Borda[t[i]] += (i+1)*P[t]
    pi = ()
    for key,value in sorted(Borda.iteritems(), key=lambda (k,v):(v,k)):
        pi = pi + (key,)
    return pi


    # Consensuses

# Computes the consensus cost of a permutation
def consensus_cost(A,P,pi,distance):
    R = 0
    for w in P:
        R += distance(pi,w)*P[w]
    return R
    
# Computes (one of) the consensus(es) for P with respect to the distance 'distance'
def general_consensus(A,P,distance):
    pi0 = A
    m = consensus_cost(A,P,pi0,distance)
    for pi in permutations(A):
        x = 0
        for t in P:
            x += P[t]*distance(pi,t)
        if x < m:
            m = x
            pi0 = pi
    return pi0

def consensuses(A,P,distance):
    D = dict.fromkeys(permutations(A))
    for pi in D:
        D[pi] = consensus_cost(A,P,pi,distance)
    minimal_cost = min(D.values())
    return [k for k in D if D[k] == minimal_cost]
    

def Kendall_consensus(A,P):
    return general_consensus(A,P,Kendall_tau)

def L1_consensus(A,P):
    return general_consensus(A,P,l1)

def L2_consensus(A,P):
    return general_consensus(A,P,l2)

