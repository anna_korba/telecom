# -*- coding: utf-8 -*-
from math import *
from itertools import *
from collections import Counter
from sets import *
from General_functions import *
from Operators import *
from MRA_framework import *
from Estimation import *





# Computes the number of times a subset occurred in a dataset
def nb_occurrences_subset(Dataset):
    C = Counter()
    for (A,pi) in Dataset:
        C[A] += 1
    return C

# Computes the number of times a ranking occurred as a subword in a dataset
def nb_occurrences_as_subword(Dataset):
    C = Counter()
    for (A,pi) in Dataset:
        for t in all_subwords(pi):
            C[t] += 1
    return C

# Computes the WLS wavelet projection estimator indexed by B
def WLS_single_projection_X(B,Counter_subsets,Counter_words):
    X = dict.fromkeys(permutations(B))
    Z = 0
    for A in Counter_subsets:
        if B.issubset(A):
            Z += Counter_subsets[A]
    for pi in X:
        X[pi]=0
        alpha = alpha_coefficients(pi)
        for w in permutations(B):
            X[pi] += alpha[w]*Counter_words[w]
        X[pi] = X[pi]/(1.0*Z)
    return X

# Computes all the WLS wavelet projection estimators from the Dataset
def WLS_projections_X(Dataset):
    C_subsets = nb_occurrences_subset(Dataset)
    C_words = nb_occurrences_as_subword(Dataset)
    S = subsets_of_collection(C_subsets.keys())
    X = dict.fromkeys(S)
    for B in S:
        X[B] = dict(WLS_single_projection_X(B,C_subsets,C_words))
    return X

# Computes the estimated marginals on the Design for the WLS estimator constructed from the Dataset
def WLS_estimator(Dataset, Design, n, parameter):
    X = WLS_projections_X(Dataset)
    M = dict.fromkeys(Design)
    for A in Design:
        M[A] = dict(Marginal_from_projections_X(X,A))
    return M


"""
def linear_projections_X(Dataset):
    nu_emp = empirical_nu(Dataset)
    P_emp = empirical_marginals(Dataset)
    S = subsets_of_collection(nu_emp.keys())
    X = dict.fromkeys(S)
    for B in S:
        for A in 
        X[B] = 
    return X
"""
        


"""

def Projections_X(Marginals):
    P = {}
    for w in Marginals:
        X = alpha_coefficients(w)
        for t in permutations(w):
            if t in P:
                P[t] += Marginals[w]*X[t]
            else:
                P.update({t:Marginals[w]*X[t]})
    return P


def scale_estimator(w,D,scale):
    k = len(w)
    j = scale
    p = 0
    for i in range(k-j+1):
        t = w[i:i+j]
        if t in D:
            p += D[t]/factorial(k-j+1)
    return(p)


def estimator(w,D,scale):
    k = len(w)
    p = 0
    for j in range(2,scale+1):
        for i in range(k-j+1):
            t = w[i:i+j]
            if t in D:
                p += D[t]/factorial(k-j+1)
    return(p)


def expansion(w,A):
    l = []
    L = []
    for i in A:
        if i not in w:
            l.append(i)
    for omega in permutations(l):
        for i in range(len(A)-len(w)+1):
            L.append(omega[0:i] + w + omega[i:len(omega)])
    return L

"""




