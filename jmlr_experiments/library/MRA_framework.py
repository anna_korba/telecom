# -*- coding: utf-8 -*-
from math import *
from itertools import *
from General_functions import *
from Operators import *



# Computes alpha_{B}(pi,.)
def alpha_coefficients(pi):
    k = len(pi)
    alpha = dict.fromkeys(permutations(pi),alternating_power(k-1)*(k-1)/(1.0*factorial(k)))
    alpha[pi] += 1     
    if k == 3:
        for t in alpha:
            alpha[t] -= 0.5*is_subword((t[0],t[1]),pi)
            alpha[t] -= 0.5*is_subword((t[1],t[2]),pi)
    elif k == 4:
        for t in alpha:
            alpha[t] += 1/(12*1.0)*is_subword((pi[0],pi[1]),t)
            alpha[t] += 1/(3*1.0)*is_subword((pi[1],pi[2]),t)
            alpha[t] += 1/(12*1.0)*is_subword((pi[2],pi[3]),t)
            alpha[t] -= 0.5*is_subword((pi[0],pi[1],pi[2]),t)
            alpha[t] -= 0.5*is_subword((pi[1],pi[2],pi[3]),t)
    elif k == 5:
        for t in alpha:
            alpha[t] -= 1/(12*1.0)*is_subword((pi[1],pi[2]),t)
            alpha[t] -= 1/(12*1.0)*is_subword((pi[2],pi[3]),t)
            alpha[t] += 1/(12*1.0)*is_subword((pi[0],pi[1],pi[2]),t)
            alpha[t] += 1/(3*1.0)*is_subword((pi[1],pi[2],pi[3]),t)
            alpha[t] += 1/(12*1.0)*is_subword((pi[2],pi[3],pi[4]),t)
            alpha[t] -= 0.5*is_subword((pi[0],pi[1],pi[2],pi[3]),t)
            alpha[t] -= 0.5*is_subword((pi[1],pi[2],pi[3],pi[4]),t)
    return alpha

# Computes the wavelet projection of f on B
def projection_X(f,B):
    M = marginal(f,B)
    X = dict.fromkeys(permutations(B),0)
    for pi in permutations(B):
        alpha = alpha_coefficients(pi)
        for w in permutations(B):
            X[pi] += alpha[w]*M[w]
    return X

# Computes the wavelet transform of f, function on Rank{A}
def wavelet_transform(f,A):
    X = dict.fromkeys(subsets_of_set(A))
    for B in X:
        X[B] = projection_X(f,B)
    return X

# Computes the marginal on A from the wavelet projections (X is a dictionary)
def Marginal_from_projections_X(X,A):
    k = len(A)
    M = dict.fromkeys(permutations(A))
    for pi in permutations(A):
        M[pi]=1/(1.0*factorial(k))
        for j in range(2,k+1):
            for i in range(k-j+1):
                x = 0
                w = pi[i:i+j]
                B = ImmutableSet(w)
                if B in X:
                    x += X[B][w]
                M[pi] += x/(1.0*factorial(k-j+1))
    return M

# Computes the embedding on A of the wavelet projections
def Embedding_from_projections_X(X,A):
    k = len(A)
    M = dict.fromkeys(permutations(A),0)
    for pi in permutations(A):
        for j in range(2,k+1):
            for i in range(k-j+1):
                x = 0
                w = pi[i:i+j]
                B = ImmutableSet(w)
                if B in X:
                    x += X[B][w]
                M[pi] += x/(1.0*factorial(k-j+1))
    return M
