# -*- coding: utf-8 -*-
from math import *
from itertools import *
from collections import Counter
from sets import *
from General_functions import *
from Estimation import *


# Computes the inner product between Sn(pi) and Sn(ij)
def pairwise_probability_from_word(pi,i,j):
    k = len(pi)
    if i in pi:
        r_i = pi.index(i) + 1
        if j in pi:
            r_j = pi.index(j) + 1
            if r_i < r_j:
                return 1
            else:
                return 0
        else:
            return (k-r_i+1)/(1.0*(k+1))
    else:
        if j in pi:
            r_j = pi.index(j) + 1
            return r_j/(1.0*(k+1))
        else:
            return 0.5


# Computes the mean Kendall's tau distance between Sn(pi) and Sn(w)
def mean_Kendall_tau(pi,w,n):
    d = 0
    for (i,j) in combinations(range(1,n+1),2):
        p_pi = pairwise_probability_from_word(pi,i,j)
        p_w = pairwise_probability_from_word(w,i,j)
        d += (1-2*p_pi)*(1-2*p_w)
    return n*(n-1)/(1.0*4) - 0.5*d

# Lebanon estimator for h > max T(sigma,sigma') or simply h > binom(n,2)
def positive_Lebanon_estimator(Dataset, Design, n, h):
    nu_emp = empirical_nu(Dataset)
    P_emp = empirical_marginals(Dataset)
    Q = dict.fromkeys(Design)
    for A in Design:
        Q[A] = dict.fromkeys(permutations(A))
        S = 0
        for w in permutations(A):
            x = 0
            for B in nu_emp:
                y = 0
                for pi in P_emp[B]:
                    y += (1 - mean_Kendall_tau(pi,w,n)/(1.0*h))*P_emp[B][pi]
                x += nu_emp[B]*factorial(len(B))*y
            Q[A][w] = x
            S += x
        for w in Q[A]:
            Q[A][w] = Q[A][w]/(1.0*S)
    return Q
    

def positive_Lebanon_operator(p,nu,Design,n,h):
    P = dict.fromkeys(Design)
    for A in Design:
        P[A] = marginal(p,A)
    Q = dict.fromkeys(Design)
    for A in Design:
        Q[A] = dict.fromkeys(permutations(A))
        S = 0
        for w in permutations(A):
            x = 0
            for B in Design:
                y = 0
                for pi in P[B]:
                    y += (1 - mean_Kendall_tau(pi,w,n)/(1.0*h))*P[B][pi]
                x += nu[B]*factorial(len(B))*y
            Q[A][w] = x
            S += x
        for w in Q[A]:
            Q[A][w] = Q[A][w]/(1.0*S)
    return Q


n = 3
A = ImmutableSet(range(1,n+1))
p = random_probability_Sn(n)
nu = {ImmutableSet([1,2,3]):1}
Design = [ImmutableSet([1,2,3])]
h = binomial(n,2)+1
q = positive_Lebanon_operator(p,nu,Design,n,h)
for pi in permutations(A):
    print(pi,p[pi],q[A][pi])

    
        
