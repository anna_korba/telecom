# -*- coding: utf-8 -*-
from math import *
from General_functions import *
from itertools import *

"""
A permutation in standard cycle form is encoded as a tuple of tuples
"""


########################################
#        Permutations operations       #
########################################


def permutation_length(tau):
   length = 0
   for cycle in tau:
      length += len(cycle)
   return length

def permutation_support(tau):
   A = set([])
   for cycle in tau:
      A = A.union(set(cycle))
   return A 

def cycles(A):
   L = []
   a = min(A)
   for gamma in permutations(word_deletion(A,a)):
      L.append((a,)+gamma)
   return L



##################################
#        Words operations        #
##################################


def word_concatenate(w1,w2):
   if w2 == 0:
      return w1
   elif w1 == 0:
      return w2
   else:
      if set(w1).intersection(set(w2)) != set():
         return 0
      else:
         return w1+w2


def chain_concatenate(x1,x2):
   X = {}
   if x1 == {}:
      return x2
   elif x2 == {}:
      return x1
   else:
      for w1 in x1:
         for w2 in x2:
            X.update({word_concatenate(w1,w2):x1[w1]*x2[w2]})
      return X


def chain_diamond(x1,x2):
   return chain_soustraction(chain_concatenate(x1,x2),chain_concatenate(x2,x1))


def word_deletion(w,a):
   return tuple(x for x in w if x != a)

def chain_deletion(x,a):
   y = {}
   for w in x:
      if word_deletion(w,a) in y:
         y[word_deletion(w,a)] += x[w]
      else:
         y.update({word_deletion(w,a):x[w]})
   return y

def derived_subword(w):
   return word_deletion(w,max(w))

def word_insertion_index(w,b,i):
   if i == 1:
      return w + (b,)
   elif i == len(w)+1:
      return (b,) + w
   else:
      return w[0:i-1] + (b,) + w[i-1:len(w)]

def word_insertion_value(w,b,i):
   l = list(w)
   index = l.index(i)
   l.insert(index+1,b)
   return tuple(l)



#####################################
#        Wavelets operations        #
#####################################


# Calcule la wavelet chain indexée par la permutation tau
def wavelet_chain(tau):
   if tau == ():
      return {():1}
   else:
      x = {}
      for cycle in tau:
         c = sorted(cycle)
         c.reverse()
         c.pop()
         tree = dict.fromkeys(cycle)
         for i in cycle:
            tree[i] = {(i,):1}
         for i in c:
            tree[i] = chain_diamond(tree[cycle[cycle.index(i)-1]], tree[i])
            t = dict(tree[i])
            L = tree[i].keys()
            for j in L[0]:
               tree[j] = dict(t)
         x = chain_concatenate(x,t)
      return x


def embedding_operator(x,A):
   if x == {():1}:
      return dict.fromkeys(permutations(A),1)
   else:
      phi = {}
      for w in x:
         B = []
         for i in A:
            if i not in w:
               B.append(i)
         for omega in permutations(B):
            for i in range(len(A)-len(w)+1):
               phi.update({omega[0:i] + w + omega[i:len(omega)]:x[w]})
      return phi


def wavelet_function(tau,Design):
   psi = {}
   if tau == ():
      for A in Design:
         psi.update(dict.fromkeys(permutations(A),1))
   else:
      for A in Design:
         if permutation_support(tau) <= set(A):
            psi.update(embedding_operator(wavelet_chain(tau),A))
   return psi


def normalized_wavelet_function(tau,A):
   if tau == ():
      psi = dict.fromkeys(permutations(A),1/(1.0*factorial(len(A))))
   else:
      psi = embedding_operator(chain_product_by_scalar(wavelet_chain(tau),1/(1.0*factorial(len(A)-permutation_length(tau)+1))),A)
   return psi

      

"""
def normalized_wavelet_function(tau,Design):
   psi = {}
   N = wavelet_norm(tau,Design)
   if tau == ():
      for A in Design:
         psi.update(dict.fromkeys(itertools.permutations(A),1/N))
   else:
      for A in Design:
         if permutation_support(tau) <= set(A):
            psi.update(embedding_operator(chain_product_by_scalar(wavelet_chain(tau),1/N),A))
   return psi
"""

def contiguous_word_inner_product(w1,w2,A):
   m = len(set(w1)&set(w2))
   if m == 0:
      return factorial(len(A)-len(w1)-len(w2)+2)
   else:
      k1 = len(w1)
      k2 = len(w2)
      if m < min(k1,k2):
         if w1[0:m] == w2[k2-m:k2] or w1[k1-m:k1] == w2[0:m]:
            return factorial(len(A)-(k1+k2-m)+1)
         else:
            return 0
      else:
         if k1 < k2:
            w_min = w1
            w_max = w2
            k_min = k1
            k_max = k2
         else:
            w_min = w2
            w_max = w1
            k_min = k2
            k_max = k1
         result = 0
         for i in range(k_max-k_min+1):
            if w_max[i:i+k_min] == w_min:
               result = factorial(len(A)-k_max+1)
               break
         return result
         

def wavelet_inner_product(tau1,tau2,Design):
   if tau1 == () and tau2 == ():
      return wavelet_norm((),Design)
   elif tau1 == () or tau2 == ():
      return 0
   else:
      x1 = wavelet_chain(tau1)
      x2 = wavelet_chain(tau2)
      result = 0
      for A in Design:
         if (permutation_support(tau1) | permutation_support(tau2)) <= set(A):
            for w1 in x1:
               for w2 in x2:
                  result += x1[w1]*x2[w2]*contiguous_word_inner_product(w1,w2,A)
      return result

def normalized_wavelet_inner_product(tau1,tau2,Design):
   return wavelet_inner_product(tau1,tau2,Design)/(wavelet_norm(tau1,Design)*wavelet_norm(tau2,Design))

   


# calcul le produit scalaire de deux fonctions exprimées dans la base d'ondelettes normalisées
def inner_product_wavelet_basis(u1,u2,Design):
   result = 0
   for tau1 in u1:
      for tau2 in u2:
         if tau1 == tau2:
            result += u1[tau1]*u2[tau2]
         else:
            result += u1[tau1]*u2[tau2]*normalized_wavelet_inner_product(tau1,tau2,Design)
   return result


def squared_norm_wavelet_basis(u,Design):
   return inner_product_wavelet_basis(u,u,Design)

         
# Prend en entrée une combinaison linéaire d'ondelettes normalisées
def chain_form(u,Design):
   result = {}
   for tau in u:
      result = chain_addition(result, chain_product_by_scalar(normalized_wavelet_function(tau,Design),u[tau]))
   return result

def wavelet_norm(tau,Design):
   N = 0
   if tau == ():
      for A in Design:
         N += Design[A]*factorial(len(A))
   else:
      for A in Design:
         if permutation_support(tau) <= set(A):
            k = permutation_length(tau)
            N += Design[A]*pow(2,k-len(tau))*factorial(len(A)-k+1)
   return sqrt(N)



