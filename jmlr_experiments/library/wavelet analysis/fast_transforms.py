# -*- coding: utf-8 -*-
from math import *
from functions_general import *
import itertools
from collections import Counter
from bases import *



def contiguous_subwords_fixed_size(sigma,k):
    L = []
    for i in range(len(sigma)-k+1):
        L.append(sigma[i:i+k])
    return L


def tree_cycles(sigma):
    n = len(sigma)
    D = dict.fromkeys(range(2,n+1))
    D[n] = {sigma:[]}
    for k in range(1,n-1):
        local_D = {}
        for w in D[n-k+1]:
            w1 = derived_subword(w)
            if w1 not in local_D:
                local_D.update({w1:[w]})
            else:
                local_D[w1].append(w)
        L = contiguous_subwords_fixed_size(sigma,n-k)
        for w in L:
            if w not in local_D:
                local_D.update({w:[]})
        D[n-k] = local_D
    return D


def epsilon(sigma,T):
    L = []
    for k in range(2,len(sigma)+1):
        L.extend(T[k])
    E = dict.fromkeys(L)
    for w in E:
        a = argmax(w)
        if a == 0:
            E[w] = {w[1]:-1}
        elif a == len(w)-1:
            E[w] = {w[a-1]:1}
        else:
            E[w] = {w[a-1]:1, w[a+1]:-1}
    return E   


def Psi_cycles(sigma):
    n = len(sigma)
    T = tree_cycles(sigma)
    L = []
    for k in range(2,n+1):
        L.extend(T[k])
    B = dict.fromkeys(L)
    E = epsilon(sigma,T)
    for w in T[2].keys():
        local_E = E[w]
        B[w] = {(min(w),max(w)):local_E[min(w)]}
    for k in range(2,n):
        local_D = T[k]
        for w0 in local_D:
            C = B[w0]
            for w in local_D[w0]:
                B[w] = {}
                local_E = E[w]
                for gamma in C:
                    for i in local_E:
                        B[w].update({word_insertion_value(gamma,max(w),i):C[gamma]*local_E[i]})
    Psi = {}
    for i in range(n-1):
        for j in range(i+1,n):
            Psi.update({sigma[i:j+1]:B[sigma[i:j+1]]})
    return Psi


def non_null_terms_cycles(n):
    C = Counter()
    for sigma in itertools.permutations(range(1,n+1)):
        Psi = {}
        B = Psi_cycles(sigma)
        for w in B:
            Psi.update(B[w])
        C[len(Psi)] += 1
    return C


def tree_non_cycles(sigma):
    n = len(sigma)
    T = dict.fromkeys(range(1,n/2+1))
    for r in range(1,n/2+1):    
        T[r] = {}
    for i in range(n-3):
        for j in range(i+1,n-2):
            T[1].update({(i-1,j):[]})
    for r in range(1,n/2):
        local_T = T[r]
        for s in T[r]:
            for j in range(s[r]+2,n):
                if min(sigma[s[r-1]+1:s[r]+1]) < min(sigma[s[r]+1:j]):
                    T[r+1].update({s + (j,):[]})
                    local_T[s].append(s + (j,))
    return T


def Psi_non_cycles(sigma,Psi_C):
    n = len(sigma)
    if n < 4:
        return {}
    else:
        T = tree_non_cycles(sigma)
        L = []
        for r in range(2,n/2+1):
            L.extend(T[r])
        B = dict.fromkeys(L)
        for s in T[2]:
            B[s] = {}
            local_Psi1 = Psi_C[sigma[s[0]+1:s[1]+1]]
            local_Psi2 = Psi_C[sigma[s[1]+1:s[2]+1]]
            for gamma1 in local_Psi1:
                for gamma2 in local_Psi2:
                    B[s].update({(gamma1,)+(gamma2,):local_Psi1[gamma1]*local_Psi2[gamma2]})
        for r in range(2,n/2):
            for s in T[r]:
                local_T = T[r]
                local_B = B[s]
                for s2 in local_T[s]:
                    B[s2] = {}
                    local_Psi = Psi_C[sigma[s2[r]+1:s2[r+1]+1]]
                    for x in local_B:
                        for gamma in local_Psi:
                            B[s2].update({x + (gamma,):local_B[x]*local_Psi[gamma]})
        return B


def Psi(sigma):
    D = {}
    Psi1 = Psi_cycles(sigma)
    Psi2 = Psi_non_cycles(sigma,Psi_cycles(sigma))
    for w in Psi1:
        local_Psi1 = Psi1[w]
        for cycle in local_Psi1:
            D.update({(cycle,):local_Psi1[cycle]})
    for s in Psi2:
        D.update(Psi2[s])
    return D

def normalized_Psi(sigma,Design):
    D = Psi(sigma)
    for tau in D:
        D[tau] = D[tau]/wavelet_norm(tau,Design)
    return D

# Computes all the non-null inner products of a chain f with wavelets
def fast_wavelet_transform(f):
    D = {}
    mass = 0
    for sigma in f:
        B = Psi(sigma)
        mass += f[sigma]
        for tau in B:
            if tau in D:
                D[tau] += f[sigma]*B[tau]
            else:
                D.update({tau:f[sigma]*B[tau]})
    W = {():mass}
    for tau in D:
        if D[tau] != 0:
            W.update({tau:D[tau]})
    return W

def fast_normalized_wavelet_transform(f,Design):
    W = fast_wavelet_transform(f)
    for tau in W:
        W[tau] = W[tau]/wavelet_norm(tau,Design)
    return W

def fast_normalized_wavelet_transform_for_wavelet(tau,Design):
    return fast_normalized_wavelet_transform(normalized_wavelet_function(tau,Design),Design)




# J'ai pas encore optimisé les calculs
def fast_OMP(Design,f,nb_iterations):
    Values = []
    Squared_norms = []
    Psi = {}
    Wavelet_transforms = {} # Stocke les transformées en ondelettes normalisées des sigma impliqués
    # Initialisation - D représente la transformée en ondelette normalisée du résidu
    D = {}
    mass = 0
    for sigma in f:
        psi = normalized_Psi(sigma,Design)
        linear_update(D,psi,f[sigma])
        mass += f[sigma]
        Wavelet_transforms.update({sigma:psi})
    clean(D)
    D.update({():mass/wavelet_norm((),Design)})
        # On trouve l'élément de la base avec le plus grand produit scalaire en valeur absolue
    (index,value) = pursuit(D)
    Values.append(value)
    u = [{index:1}]
    Squared_norms.append(1.0)
    approx  = {index:value}
        # On actualise D
    Psi.update({index:fast_normalized_wavelet_transform_for_wavelet(index,Design)})
    local_Psi = Psi[index]
    linear_update(D,local_Psi,-value)
    clean(D)
            
    # Iterations
    M = nb_iterations
    for m in range(1,nb_iterations+1):
        # On trouve l'élément de la base avec le plus grand produit scalaire en valeur absolue
        (index,value) = pursuit(D)
        # On orthonormalise par Gram-Schmidt - v est le nouvel élément de la base
        v = {index:1}
        Inner_products = {} # Stocke les produits scalaires entre (index) et tous les éléments de la base impliqués dans les u[l]
        for l in range(m):
            local_u = u[l]
            # Calcul du produit scalaire entre (index) et u[l]
            inner_product = 0
            for tau in local_u:
                if tau in Inner_products:
                    inner_product += local_u[tau]*Inner_products[tau]
                if tau not in Inner_products:
                    Inner_products.update({tau:normalized_wavelet_inner_product(index,tau,Design)})
                    inner_product += local_u[tau]*Inner_products[tau]
            linear_update(v,local_u,-inner_product/Squared_norms[l])
        clean(v)
        if v == {}:
            M = m
            break
        else:
            u.append(v)
            Values.append(value)
            Squared_norms.append(squared_norm_wavelet_basis(v,Design))
            # On actualise l'approximation
            approx = chain_addition(approx,chain_product_by_scalar(v,value/Squared_norms[m]))
            # On actualise D
            Psi.update({index:fast_normalized_wavelet_transform_for_wavelet(index,Design)})
            for i in v:
                local_Psi = Psi[i]
                linear_update(D,local_Psi,-value*v[i]/Squared_norms[m])
            clean(D)
                        
    # Calcul de l'erreur
    error = [1]
    N = 0
    for w in f:
        N += f[w]*f[w]
    for m in range(M):
        error.append(error[m]- Values[m]*Values[m]/(Squared_norms[m]*N))
    return (approx,error,u,Squared_norms,Values,N,mass)
        
        

    
        
       
    
        
    
    
    
                    



    

    
