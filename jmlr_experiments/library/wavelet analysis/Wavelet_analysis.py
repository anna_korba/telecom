

# -*- coding: utf-8 -*-
from math import *
from itertools import *
from collections import Counter
from sets import *
from General_functions import *
from bases import *
from Operators import *






def wavelet_localized_coefficients(w):
    k = len(w)
    A = tuple(sorted(w))
    X = alpha_coefficients(w)
    C = {}
    if k == 2:
        C.update({((A[0],A[1]),):X[A]})
    elif k == 3:
        C.update({((A[0],A[1],A[2]),):X[A],((A[0],A[2],A[1]),):-X[(A[1],A[0],A[2])]})
    elif k == 4:
        C.update({((A[0],A[1],A[2],A[3]),):-X[(A[3],A[2],A[1],A[0])],
                  ((A[0],A[1],A[3],A[2]),):X[(A[3],A[1],A[2],A[0])]+X[(A[3],A[1],A[0],A[2])],
                  ((A[0],A[2],A[1],A[3]),):X[(A[3],A[1],A[0],A[2])],
                  ((A[0],A[2],A[3],A[1]),):X[(A[3],A[2],A[0],A[1])],
                  ((A[0],A[3],A[1],A[2]),):X[(A[2],A[1],A[0],A[3])],
                  ((A[0],A[3],A[2],A[1]),):X[(A[2],A[3],A[0],A[1])]+X[(A[3],A[2],A[0],A[1])],
                  ((A[0],A[1]),(A[2],A[3])):X[(A[0],A[1],A[2],A[3])]+X[(A[3],A[2],A[1],A[0])],
                  ((A[0],A[2]),(A[1],A[3])):-X[(A[2],A[0],A[1],A[3])]-X[(A[3],A[1],A[0],A[2])],
                  ((A[0],A[3]),(A[1],A[2])):-X[(A[2],A[1],A[0],A[3])]-X[(A[3],A[0],A[1],A[2])]})
    return C

def wavelet_coefficients(w):
    Psi = {():1}
    for t in subwords(w):
        Psi.update(wavelet_localized_coefficients(t))
    return Psi

def wavelet_transform(f):
    Psi = {}
    for w in f:
        C = wavelet_coefficients(w)
        Psi = chain_addition(Psi,chain_product_by_scalar(C,f[w]))
    return(Psi)

def wavelet_synthesis(C,A):
    f = {}
    for tau in C:
        psi = normalized_wavelet_function(tau,A)
        for pi in psi:
            if pi in f:
                f[pi] += C[tau]*psi[pi]
            else:
                f.update({pi:C[tau]*psi[pi]})
    return f
