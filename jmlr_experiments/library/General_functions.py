# -*- coding: utf-8 -*-
from math import *
from itertools import *
from sets import *
from collections import Counter


##########################
#        Printing        #
##########################

def word_to_string(w):
   s = ''
   for a in w:
      s = s + str(a)
   return s


# Maps an ImmutableSet to a string
def set_to_string(A):
   B = sorted(A)
   s = '{'
   for a in B:
      s = s + str(a) + ','
   return s[:-1] + '}'

###############################
#        General maths        #
###############################


# Computes the binomial coefficient 'k among n'
def binomial(n,k):
   if k > n-k: k = n-k  # Use symmetry of Pascal's triangle
   accum = 1
   for i in range(1,k+1):
      accum *= (n - (k - i))
      accum /= i
   return accum

# Computes (-1)^k
def alternating_power(k):
   if k%2 == 0:
      return 1
   else:
      return -1

# Computes the histogram for a list of values L
def histogram(L):
    N = len(L)
    C = Counter()
    for x in L:
        C[x] += 1
    h = dict.fromkeys(C)
    for x in h:
        h[x] = C[x]/(1.0*N)
    return h

# Computes the mean of a list of values
def mean(L):
   m = 0
   for x in L:
      m += x
   return m/(1.0*len(L))

#Computes the standard deviation of a list of values
def standard_deviation(L,m):
   sigma = 0
   for x in L:
      sigma += (x-m)**2
   return sqrt(sigma/(1.0*len(L)))

# Returns the index of the element of highest value in a tuple
def argmax(w):
   l = list(w)
   return l.index(max(w))

# Returns the index and the value of the element of highest absolute value in a dictionary
def pursuit(d):
   value = 0
   for i in d:
      if fabs(d[i]) > fabs(value):
         value = d[i]
         index = i
   return (index,value)



###################################
#        Subsets operations       #
###################################


# Computes the subsets of cardinality >= 2 of a set A
def subsets_of_set(A):
    L = []
    for k in range(2,len(A)+1):
        for c in combinations(A,k):
            B = ImmutableSet(c)
            L.append(B)
    return L

# Computes the subsets of cardinality =k of a set A
def subsets_of_size(A,k):
    L = []
    for c in combinations(A,k):
       B = ImmutableSet(c)
       L.append(B)
    return L

# Computes the reunion of the subsets of the sets of a collection L
def subsets_of_collection(L):
    S = Set()
    for A in L:
        S.update(Set(subsets_of_set(A)))
    return S


###################################
#        Vector operations        #
###################################


# Computes the infinity norm
def infinity_norm(f):
   n = 0
   for x in f:
      if abs(f[x]) > n:
         n = abs(f[x])
   return n

# Computes the squared l2 norm
def squared_l2_norm(f):
   N = 0
   for x in f:
      N += f[x]**2
   return N

# Computes the l1 norm
def l1_norm(f):
   n = 0
   for x in f:
      n += abs(f[x])
   return n



##################################
#        Chain operations        #
##################################


def chain_addition(x, y):
   global_support = set(x.keys())|set(y.keys())
   z = dict.fromkeys(global_support)
   for w in z:
      t = 0
      if w in x:
         t += x[w]
      if w in y:
         t += y[w]
      z[w] = t
   result = {}
   for w in z:
      if z[w] != 0:
         result.update({w:z[w]})
   return result


def chain_soustraction(x, y): #x-y
   global_support = set(x.keys())|set(y.keys())
   z = dict.fromkeys(global_support)
   for w in z:
      t = 0
      if w in x:
         t += x[w]
      if w in y:
         t -= y[w]
      z[w] = t
   result = {}
   for w in z:
      if z[w] != 0:
         result.update({w:z[w]})
   return result


def chain_product_by_scalar(x,alpha):
   y = {}
   for w in x:
      y.update({w:alpha*x[w]})
   return y


def linear_update(x,y,alpha):
   for i in y:
      if i in x:
         x[i] += alpha*y[i]
      else:
         x.update({i:alpha*y[i]})

def clean(d):
   for i in d.keys():
      if d[i] == 0:
         del d[i]


def chain_inner_product(x,y):
   result = 0
   if len(x) <= len(y):
      for w in x:
         if w in y:
            result += x[w]*y[w]
      return result
   else:
      for w in y:
         if w in x:
            result += x[w]*y[w]
      return result



################################
#           Mappings           #
################################


# Mapping int -> tuple
def index_tuple(i,n,k):
   q = n-1
   for j in range(1,k-1):
      q *= n-1-j
   a = (i-1)/q + 1
   r = (i-1)%q
   t = (a,)
   l = [a]
   for j in range(2,k+1):
      q = q/(n-j+1)
      a = r/q + 1
      for b in l:
         if a < b:
            break
         else:
            a +=1
      t = t + (a,)
      l.append(a)
      l.sort()
      r = r%q
   return t


# Mapping tuple -> int
def tuple_index(t,n):
   k = len(t)
   N = 1
   if k > 2:
      x = n*(n-1)
      for j in range(2,k):
         N += x
         x *= n-j
   r = list(sorted(t,reverse=True))
   x = 1
   for j in range(k):
      r = list(sorted(t[:k-j]))
      y = t[k-j-1]-1-r.index(t[k-j-1])
      y *= x
      N += y
      x *= n-k+j+1
   return N 


# Mapping set -> int
def set_int(A,n):
   k = len(A)
   N = 1
   for j in range(2,k):
      N += binomial(n,j)
   for j in range(1,A[0]):
      N += binomial(n-j,k-1)
   for i in range(1,k):
      for j in range(A[i-1]+1,A[i]):
         N += binomial(n-j,k-i-1)
   return(N)


      
