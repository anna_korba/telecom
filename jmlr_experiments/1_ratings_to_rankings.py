# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import itertools
import csv
import random
import cPickle as pickle




##########################################
#                                        #
#              Get Ratings               #
#                                        #
##########################################

# 1 - Import Data 
def import_data(limit = None) :
    df= pd.read_csv("movielens_original_ratings.txt",sep= ",",header=None)
    #df= pd.read_csv("ratings20m.txt",sep= ",",header=None)
    df.columns =["user_id", "movie_id", "rating", "timestamp"]
    df = df.astype(int)
    #df = df.convert_objects(convert_numeric=True)
    df.drop("timestamp", axis=1, inplace=True)
    df.astype(float)
    return df

#userId,movieId,rating,timestamp
print 'loading ratings'
df=import_data()
print 'done'
#df=df.sort(['user_id','rating']) # 6040 users, 3706 movies.


# 2 - Select Data (keep most rated movies)
  
def keep_n_most_rated_movies(df,n):
    freq=df["movie_id"].value_counts()
    keep=list(freq.index[0:n])
    return df[df["movie_id"].isin(keep)]

              
n_movies=20
df2=keep_n_most_rated_movies(df,n_movies)
print len(np.unique(df2['user_id'])) # 6040 users
print len(np.unique(df2['movie_id'])) # 100 movies.

# Re-index movies
for i in range(len(np.unique(df2['movie_id']))):
    df2["movie_id"].replace(np.unique(df2['movie_id'])[i], i+1, inplace=True)              
  
df2.to_csv('save_data/df2.csv')   
df2=pd.read_csv('save_data/df2.csv')
         
# 3- Create dictionary (keys: users) of dictionaries (keys: ratings, values: movies)

def create_dictionary(df):
    x = {}
    for user_id,v in df.groupby(["user_id"])["rating","movie_id"]:
        #print user_id
        x[user_id]={}
        for rating, w in v.groupby(["rating"])["movie_id"]:
            x[user_id][rating]=list(w)
    return x

print 'create dict_ratings'
dict_ratings=create_dictionary(df2)   

with open('save_data/dict_ratings.txt', 'wb') as handle:
  pickle.dump(dict_ratings, handle)    
    
  
  
##########################################
#                                        #
#          Create rankings               #
#                                        #
##########################################

# 4 - Create a dictionary (keys: users, values: rankings) with the dictionary of ratings

def create_all_rankings(d):
    d_rankings={}
    for user in d.keys():
        #print user
        user_movies=d[user].values()
        d_rankings[user]=list(itertools.product(*user_movies))
    return d_rankings

print 'create dict_rankings'
dict_rankings=create_all_rankings(dict_ratings)