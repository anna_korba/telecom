# -*- coding: utf-8 -*-
from library.Plackett_Luce import PL_fitting_list, PL_predict
from library.Operators import is_subword, subwords_of_size, induced_subword
from library.General_functions import alternating_power
import numpy as np
from collections import Counter
from itertools import permutations, combinations
from random import choice 
from math import factorial

########################
#                      #
#       Bayes          #
#                      #
########################
 
         
def Compute_Empirical_Marginals(L,Items, Subset_sizes):
    Probas=dict.fromkeys(Items)
    for a in Items:
        Probas[a]={}
    nu = {}
    Marginals = {}   
    for w in L:
        for k in Subset_sizes:       
            for t in subwords_of_size(w,k):
                A = frozenset(t)
                if A not in Marginals:
                    marginal = Counter()
                    Marginals.update({A:marginal})
                Marginals[A][t] += 1
                if A not in nu:
                    nu[A]=1
                else:
                    nu[A] += 1   
    for A in Marginals:
        Marginals[A]=dict(Marginals[A])
        for a in A:
            Probas[a][A]=0
        for t in Marginals[A]:
            Marginals[A][t]=(Marginals[A][t])/(nu[A]*1.0)
            Probas[t[0]][A]+=Marginals[A][t]
    return Marginals


########################
#                      #
#    Plackett-Luce     #
#                      #
########################

  
def Compute_PL_Marginals(L, Items, Subset_sizes,Design): # Parameter = nb_iterations for PL model
    Gamma = PL_fitting_list(L,len(Items),10)
    M = dict.fromkeys(Design)
    for A in Design:
        M[A] = {}
        for pi in permutations(A):
            M[A].update({pi:PL_predict(Gamma,pi)})
    return M
    
 
def Compute_PL_Classifier(L,Items, Subset_sizes, Design):
    scores_PL=PL_fitting_list(L,len(Items),10)
    Probas=dict.fromkeys(Items)
    for a in Items:
        Probas[a]={}
    for w in L:
        for k in Subset_sizes:   
            for t in subwords_of_size(w,k):
                A = frozenset(t)
                den=0
                for i in range(k):
                    den+=scores_PL[t[i]]
                for i in range(k):
                    p= (scores_PL[t[i]])/((den)*1.0) # on calcule la proba que i dans A soit premier sur A
                    if A not in Probas[t[i]]:
                        Probas[t[i]].update({A:p})
                    else:
                        Probas[t[i]][A]=p                 
    return Probas
    
########################
#                      #
#         MRA          #
#                      #
########################

# Computes alpha_{B}(pi,.)
def alpha_coefficients(pi):
    k = len(pi)
    alpha = dict.fromkeys(permutations(pi),alternating_power(k-1)*(k-1)/(1.0*factorial(k)))
    alpha[pi] += 1     
    if k == 3:
        for t in alpha:
            alpha[t] -= 0.5*is_subword((t[0],t[1]),pi)
            alpha[t] -= 0.5*is_subword((t[1],t[2]),pi)
    elif k == 4:
        for t in alpha:
            alpha[t] += 1/(12*1.0)*is_subword((pi[0],pi[1]),t)
            alpha[t] += 1/(3*1.0)*is_subword((pi[1],pi[2]),t)
            alpha[t] += 1/(12*1.0)*is_subword((pi[2],pi[3]),t)
            alpha[t] -= 0.5*is_subword((pi[0],pi[1],pi[2]),t)
            alpha[t] -= 0.5*is_subword((pi[1],pi[2],pi[3]),t)
    elif k == 5:
        for t in alpha:
            alpha[t] -= 1/(12*1.0)*is_subword((pi[1],pi[2]),t)
            alpha[t] -= 1/(12*1.0)*is_subword((pi[2],pi[3]),t)
            alpha[t] += 1/(12*1.0)*is_subword((pi[0],pi[1],pi[2]),t)
            alpha[t] += 1/(3*1.0)*is_subword((pi[1],pi[2],pi[3]),t)
            alpha[t] += 1/(12*1.0)*is_subword((pi[2],pi[3],pi[4]),t)
            alpha[t] -= 0.5*is_subword((pi[0],pi[1],pi[2],pi[3]),t)
            alpha[t] -= 0.5*is_subword((pi[1],pi[2],pi[3],pi[4]),t)
    return alpha

# Computes the WLS wavelet projection estimator indexed by B
def WLS_single_projection_X(B,Counter_subsets,Counter_words):
    X = dict.fromkeys(permutations(B))
    Z = 0
    for A in Counter_subsets:
        if B.issubset(A):
            Z += Counter_subsets[A]
    for pi in X:
        X[pi]=0
        alpha = alpha_coefficients(pi)
        for w in permutations(B):
            X[pi] += alpha[w]*Counter_words[w]
        X[pi] = X[pi]/(1.0*Z)
    return X
    

# Computes the marginal on A from the wavelet projections (X is a dictionary)
def Marginal_from_projections_X(X,A):
    k = len(A)
    M = dict.fromkeys(permutations(A))
    for pi in permutations(A):
        M[pi]=1/(1.0*factorial(k))
        for j in range(2,k+1):
            for i in range(k-j+1):
                x = 0
                w = pi[i:i+j]
                B = frozenset(w)
                if B in X:
                    x += X[B][w]
                M[pi] += x/(1.0*factorial(k-j+1))
    return M
    
#def Compute_MRA_Marginals(L,Items, Subset_sizes):
def Compute_MRA_Marginals(L,Items, Subset_sizes, Design):
    Probas=dict.fromkeys(Items)
    for a in Items:
        Probas[a]={}
    C_subsets = Counter()
    C_words=Counter() # Computes the number of times a ranking occurred as a subword in a dataset
    S=Counter() # tous les A observes
    for w in L:
        W= frozenset(w)
        C_subsets[W]+=1
        for k in Subset_sizes:   
            for t in subwords_of_size(w,k):
                C_words[t]+=1 
                A = frozenset(t)
                S[A]+=1
    X = dict.fromkeys(S)
    ##X=dict.fromkeys(Design)  
    for B in S:
        X[B] = dict(WLS_single_projection_X(B,C_subsets,C_words))
    ##Design=S.keys()
    ##Marginals = dict.fromkeys(S.keys())
    
    Marginals = dict.fromkeys(Design)
    for A in Design:
        Marginals[A] = dict(Marginal_from_projections_X(X,A))
    #for A in Marginals:
    #    for a in A:
    #        Probas[a][A]=0
    #    for t in Marginals[A]:
    #        Probas[t[0]][A]=Marginals[A][t]
    return Marginals
    
    

########################
#                      #
#    Classification    #
#                      #
########################  
          
def Plug_In_Classifier(Probas_first, Items,Design):
    h = dict.fromkeys(Items)
    Probas_first_normalized= dict.fromkeys(Probas_first) # dict des |A|*P_{a,A}
    for a in Probas_first:
        Probas_first_normalized[a]=dict.fromkeys(Probas_first[a])
        for A in Probas_first[a]:
            Probas_first_normalized[a][A]=len(A)*Probas_first[a][A]
    for a in Items:
        Probas_first_normalized[a]={k: v for k, v in Probas_first_normalized[a].iteritems() if k in Design}
        if len(Probas_first_normalized[a].keys())>0:
            candidates= [key for key,val in Probas_first_normalized[a].iteritems() if val == max(Probas_first_normalized[a].values())]
            if len(candidates)==1:
                h[a]=(candidates[0],max(Probas_first_normalized[a].values()))
            else:
                #print '\n'
                print 'several candidates for item '+str(a)
                #print candidates
                filtered = dict(zip(candidates, [Design[k] for k in candidates]))
                most_frequent=[key for key,val in filtered.iteritems() if val == max(filtered.values())]
                if len(most_frequent)==1:
                    h[a]=(most_frequent[0],max(Probas_first_normalized[a].values()))
                else:
                    #print 'even in frequency'
                    h[a]=(choice(most_frequent),max(Probas_first_normalized[a].values()))
        else:
            h[a]=(frozenset([]),-np.infty)
    return h

def Empirical_Local_Risk(h,L,Items):
    R=dict.fromkeys(Items)
    for a in Items:
        #if h[a] is not None:
        if len(h[a][0])>0:
            A=h[a][0]
            nu_A=0
            count_first=0
            for pi in L:
                if A.issubset(frozenset(pi)):
                    nu_A+=1
                    pi_restrict=induced_subword(pi, A)
                    if pi_restrict[0]==a:
                        count_first+=1
            if nu_A>0: # ce A* n'est peut-etre pas observe dans le test set
                r=(len(A)*count_first)/((nu_A)*1.0)
                R[a]=r
            else:
                raise Exception
    return R
          
    
########################
#                      #
#       Others         #
#                      #
########################          

def Compute_Design(L, Subset_sizes):
    Design=Counter() 
    for w in L:
        for k in Subset_sizes:   
            for t in subwords_of_size(w,k):          
                A = frozenset(t)
                Design[A]+=1
    return Design

    
def tic():
    #Homemade version of matlab tic and toc functions
    import time
    global startTime_for_tictoc
    startTime_for_tictoc = time.time()

def toc():
    import time
    if 'startTime_for_tictoc' in globals():
        print "Elapsed time is " + str(time.time() - startTime_for_tictoc) + " seconds."
    else:
        print "Toc: start time not set"

def subsets_of_set_contains(a,A):
    L = []
    if a in A:
        if len(A)==2:
            L.append(A)
        else:
            A_minus_a=A.difference(set([a]))
            if len(A_minus_a)>=2:
                for k in range(1,len(A_minus_a)+1):
                    for c in combinations(A_minus_a,k):
                        B =set(c).union(set([a]))
                        L.append(frozenset(B))
    return L

def subsets_of_collection_contains(a,L):
    S = Counter()
    for A in L:
        for B in subsets_of_set_contains(a,A):
            S[B]+=1
    return S.keys()
    
    
def Borda_count_incomplete(Items,L):
    Borda = dict.fromkeys(Items,0)
    for w in L:
        for i in range(len(w)):
            Borda[w[i]] += (len(w)-i)
    return Borda