#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from itertools import *
from math import *
from random import *
from sets import *
from collections import *
import operator
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import cPickle as pickle


from library.Voting_rules import *
from library.Pairwise_space import *
from library.General_functions import *
from library.Operators import *
from library.Sampling import *
from library.Distances_on_rankings import *
from library.Estimation import *
from library.Plackett_Luce import *
from library.Estimation import error
from Myfunctions import *
n_movies=1000

def compute_error(path,inputfile):
    
    #print('Data loading')
    rankings=[]
    f = open(path+inputfile, 'r')

    for line in f:
        #print line
        #if N<100:
        w = tuple(map(int,line.split(';')))
        rankings.append(w)
        
    f.close()
    #print('Data loaded')
    
    Subset_sizes = range(2,4)
    Items=list(np.arange(1,n_movies+1))
    
    #print 'Divide in train-test'
    shuffle(rankings)
    rankings=np.asarray(rankings)
    Design_all=Compute_Design(rankings, Subset_sizes)
    
    msk = np.random.rand(len(rankings)) < 0.8 
    rankings_train,rankings_test = rankings[msk],rankings[~msk]
    
    Design_train=Compute_Design(rankings_train, Subset_sizes)
    Design_test=Compute_Design(rankings_test, Subset_sizes) #4476
    Design_test = {k: v for k, v in Design_test.iteritems() if v >= 10} #2101
    #Intersection=frozenset((Design_train).keys()).intersection(frozenset(Design_test.keys()))

    
    # Bayes classifier
    #print 'Bayes'
    Marginals_Bayes=Compute_Empirical_Marginals(rankings_test,Items,Subset_sizes)

    # PL classifier
    #print 'Plackett-Luce'
    Marginals_PL=Compute_PL_Marginals(rankings_train,Items, Subset_sizes,Design_test)

    # MRA classifier
    #print 'MRA'
    #tic()
    Marginals_MRA=Compute_MRA_Marginals(rankings_train,Items, Subset_sizes,Design_test)
    #toc()
    #h_MRA=Plug_In_Classifier(Probas_MRA, Items, Design_test)
    #R_MRA=Empirical_Local_Risk(h_MRA,rankings_test,Items)
    
    # compute error
    error_MRA=error(Marginals_MRA, Design_test, Marginals_Bayes)
    error_PL=error(Marginals_PL, Design_test, Marginals_Bayes)
    print 'error MRA : '+str(error_MRA)
    print 'error PL : '+str(error_PL)

    
    d={'MRA':error_MRA, 'PL':error_PL}

    """

    #PL_results=PL_fitting_list(all_rankings,len(Items),10)
    #PL_sorted=sorted(PL_results.iteritems(), key=lambda (k,v):(v,k), reverse=True)
    Borda_results=Borda_count_incomplete(Items,rankings)
    Borda_sorted=sorted(Borda_results.iteritems(), key=lambda (k,v):(v,k), reverse=True)
    
    
    width=0.5
    #xticks=[t[0] for t in PL_sorted] # j'ordonne les objets par score de PL
    xticks=[t[0] for t in Borda_sorted] # j'ordonne les objets par score de Borda
    y1=[R_Bayes[i] for i in xticks]
    y2=[R_PL[i] for i in xticks]
    y3=[R_MRA[i] for i in xticks]
    
    # quantify how many times MRA beats PL
    test1=[y3[i]>y2[i] for i in range(len(xticks))] #MRA beats PL
    test2=[y2[i]>y3[i] for i in range(len(xticks))] # PL beats MRA
    
    # juste pour le graphe, j'enleve les objets ou je ne peux pas calculer le risque car A* n'est pas dans le test set.
    new_xticks=[]
    new_y1=[]
    new_y2=[]
    new_y3=[]
    for i in xticks:
        if R_PL[i] is not None and R_MRA[i] is not None:
            new_xticks.append(i)
            ind=xticks.index(i)
            new_y1.append(y1[ind])
            new_y2.append(y2[ind])
            new_y3.append(y3[ind])
    
    xticks=new_xticks
    y1,y2,y3=new_y1 ,new_y2 ,new_y3
    plt.style.use('ggplot')
    
    ind=np.arange(0,2*len(xticks),2)
    fig=plt.figure(figsize=(3*len(xticks),5))#figsize=(len(xticks),5)
    ax = fig.add_subplot(111)
    
    rects1 = ax.bar(ind-width, y1, width, color='black',align='center')
    rects2 = ax.bar(ind, y2, width, color='darkblue',align='center')
    rects3 = ax.bar(ind+width, y3, width, color='red',align='center')
    
    ax.set_ylabel('Risk')
    ax.set_xticks(ind)
    ax.set_xticklabels( xticks , rotation= 70)
    ax.set_ylim([0, max(Subset_sizes)+0.5])

    ax.legend((rects1[0], rects2[0], rects3[0]), ('Bayes', 'PL', 'MRA'),loc='upper center', bbox_to_anchor=(0.5, 1.05),
              ncol=3, fancybox=True, shadow=True)          
    plt.xlim([-2,2*len(ind)])
    plt.title('MRA strictly wins vs PL: '+str(sum(test1))+', PL wins vs MRA: '+str(sum(test2))+', total items ='+str(len(xticks)),y=1.06)
    
    # quantify how many times MRA beats PL
    test1=[y3[i]>y2[i] for i in range(len(xticks))] #MRA beats PL
    test2=[y2[i]>y3[i] for i in range(len(xticks))] # PL beats MRA
    
    print len(xticks), sum(test1), sum(test2)
    
    #plt.savefig('/Users/Anna/Desktop/Experiments/simulations-mra/results/'+str(inputfile[64:-4])+'.jpg')
    plt.savefig(path+'results/'+str(inputfile[:-4])+'.png')
    """

########################################
#                                      #
#       Launch on many datasets        #
#                                      #
########################################
for K in [3,5,7,10]:
    print '\n'
    print 'nombre de clusters : '+str(K)
    for inputfile in os.listdir('save_data/clustering_'+str(K)+'/')[1:]:
        if inputfile[-4:]=='.csv':
            print 'cluster '+str(inputfile[-5])
            path='save_data/clustering_'+str(K)+'/'
            compute_error(path,inputfile)
            