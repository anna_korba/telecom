\documentclass{article}
\usepackage{nips_2017}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{xcolor}
\usepackage{float}
\usepackage{tikz}
\usetikzlibrary{decorations.markings}
\usepackage{blkarray}
\usepackage{bbm}
\usepackage{float}
\usepackage{tabularx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{tikz}
\usepackage{stmaryrd}
\usetikzlibrary{matrix,arrows}
\usepackage{url}
\usepackage{comment}
\usepackage{multirow}
\usepackage{arydshln}
%\usepackage{kbordermatrix}
\usepackage{pgfplots}
\pgfmathdeclarefunction{gauss}{2}{%
  \pgfmathparse{1/(#2*sqrt(2*pi))*exp(-((x-#1)^2)/(2*#2^2))}%
}

\newtheorem{theorem}{{\bf Theorem}}
\newtheorem{example}{{\bf Example}}
\newtheorem{proposition}{{\bf Proposition}}
\newtheorem{definition}{{\bf Definition}}
\newtheorem{corollary}{{\bf Corollary}}
\newtheorem{lemma}{{\bf Lemma}}
\newtheorem{remark}{{\bf Remark}}
\newtheorem{assumption}{{\bf Assumption}}
\newcommand{\auc}{\rm  AUC}
\newcommand{\iauc}{\rm  IAUC}
\newcommand{\roc}{\rm  ROC}
\newcommand{\iroc}{\rm  IROC}
\def\vus{{\rm VUS}}
\def\X{{\mathcal X}}
\newcommand{\n}{\llbracket n \rrbracket}

\title{Ranking Median Regression: a MRA Approach}
\author{
  Stephan Cl\'emen\c{c}on\\
  LTCI Telecom ParisTech, Universit\'e Paris-Saclay\\
  75013, Paris, France\\
  \texttt{first.last@telecom-paristech.fr}
}
\begin{document}


\maketitle

\begin{abstract} In the present era of personalized customer services and recommender systems, predicting the preferences of an individual/user over a (possibly very large) set of items indexed by $\n=\{1,\; \ldots,\; n\}$, $n\geq 1$, based on its characteristics, modelled as a r.v. $X$, taking its values in a feature space $\mathcal{X}$ say, is an ubiquitous issue. Though easy to state, this predictive problem is very difficult to solve in practice, due to two major difficulties essentially. The first one lies in the fact that, here, the (discrete) output space is the symmetric group $\mathfrak{S}_n$, composed of all permutations of $\n$, of explosive cardinality $n!$, and which is not a subset of a vector space. It is thus far from straightforward to build effectively predictive rules taking their values in $\mathfrak{S}_n$, with the noteworthy exception of methods implementing ranking aggregation techniques at a local level, as proposed in \cite{AY14} or \cite{CKS17bis}. The nature of the training data that are generally available for this learning task constitutes the other serious difficulty, users generally expressing their preferences over extremely variable subsets of items, of cardinality much smaller than $n!$. It is the purpose of this paper to explain how the notion of \textit{multiresolution analysis} (MRA) of incomplete rankings, recently introduced in \cite{SCK15} may offer a promising solution to this supervised learning problem, when combined with the concept of \textit{local learning}.
%As a by-product, the partition thus obtained is shown to define a regression estimate, much more robust to outliers than that produced by the {\sc CART} algorithm.
%Adaptive discretization of the problem, of the (continuous) label variable
\end{abstract}

%\begin{keywords}
%Integral approximation, exponential inequalities, Kernel estimation, Monte-Carlo method, projection method, rate bounds, statistical sampling, $U$-statistics
%\end{keywords}

\section{Introduction}
In an increasing number of modern applications/interfaces, users are invited to declare their individual characteristics (\textit{e.g.} socio-demographic features), taking the form of a random vector $X$ valued in a an input space $\mathcal{X}\subset \mathbb{R}^d$ say, and express their preferences over subsets of the collection of numbered services/products $\n=\{1,\; \ldots,\; n\}$ offered to them. In this context, the goal pursued is to learn from historical data how to predict the preferences of any user based on her characteristics $X$, the prediction being of the form of a permutation $S(X)$ on $\n$, mapping any item $i$ to its rank $S_i(X)$ on her preference list. Denoting by $\Sigma$ the permutation that truly reflects the preferences of a user with characteristics $X$, the performance of any predictive rule, \textit{i.e.} any measurable function $S:\mathcal{X}\rightarrow \mathfrak{S}_n$, can be measured by the expected Kendall $\tau$ distance between
\begin{equation}\label{eq:risk_theo}
\mathcal{R}(S)=\mathbb{E}\left[d_{\tau}\left(S(X),\Sigma  \right)  \right],
\end{equation}  
where the expectation is taken over the (unknown) distribution of the pair $(X,\Sigma)$ and $d_{\tau}(\sigma,\sigma')=\sum_{i<j}\mathbb{I}\{(\sigma(j)-\sigma(i)) \cdot (\sigma'(j)-\sigma'(i))<0   \}$ for all $(\sigma,\sigma')\in \mathfrak{S}_n^2$, denoting by $\mathbb{I}\{\mathcal{E}  \}$ the indicator function of any event $\mathcal{E}$. Stated this way, the objective is to build a mapping $S$ that minimizes \eqref{eq:risk_theo} and one may easily show with a straightforward conditioning argument that the optimal predictors are the rules that maps any point $X$ in the input space to any (Kemeny) ranking median of $P_X$, $\Sigma$'s conditional distribution given $X$. Recall that a Kemeny median of a probability distribution $P$ on $\mathfrak{S}_n$ is any solution of the optimization problem
\begin{equation}\label{eq:median}
\min_{\sigma\in \mathfrak{S}_n}\mathbb{E}_{\Sigma \sim P}\left[ d_{\tau}(\sigma,\Sigma) \right].
\end{equation}
For this reason, the predictive problem formulated above is referred to as \textit{ranking median regression} in this paper. The latter has been recently investigated in \cite{CKS17bis} in the case where independent copies of $(X,\Sigma)$ form the training dataset and it has also been shown therein that techniques for statistical Kemeny aggregation, \textit{i.e.} for solving \eqref{eq:median}, when implemented at local levels, may produce efficient techniques to minimize \eqref{eq:risk_theo} approximately (namely, nearest-neighbour and decision tree methods). However, in many situations, users do not express their preferences on the whole catalog of products/services and the observations stored in the historical database are not full rankings $\Sigma$, but permutations on small and variable subsets of $\n$, termed \textit{incomplete rankings}. In this context, statistical estimation of the quantity \eqref{eq:risk_theo} is far from immediate and extending the \textit{empirical risk minimization} (ERM) paradigm to ranking median regression is a challenge. It is precisely the purpose of this paper to show that the multiresolution analysis (MRA) of incomplete rankings introduced in \cite{SCK15} in an unsupervised setting enables the design of ranking median regression methods in this situation   

\section{Background and Preliminaries}

\subsection{Multiresolution Analysis of Incomplete Rankings}

\subsection{Statistical Framework for Ranking Median Regression}

\section{The Algorithm}
\bibliographystyle{plain}

\bibliography{biblio-thesis}
\end{document}
