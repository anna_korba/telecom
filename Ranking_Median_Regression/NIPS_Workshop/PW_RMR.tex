\documentclass{article}
\usepackage{nips_2017}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{xcolor}
\usepackage{float}
\usepackage{tikz}
\usetikzlibrary{decorations.markings}
\usepackage{blkarray}
\usepackage{bbm}
\usepackage{float}
\usepackage{tabularx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{tikz}
\usepackage{stmaryrd}
\usetikzlibrary{matrix,arrows}
\usepackage{url}
\usepackage{comment}
\usepackage{multirow}
\usepackage{arydshln}
%\usepackage{kbordermatrix}
\usepackage{pgfplots}
\pgfmathdeclarefunction{gauss}{2}{%
  \pgfmathparse{1/(#2*sqrt(2*pi))*exp(-((x-#1)^2)/(2*#2^2))}%
}

\newtheorem{theorem}{{\bf Theorem}}
\newtheorem{example}{{\bf Example}}
\newtheorem{proposition}{{\bf Proposition}}
\newtheorem{definition}{{\bf Definition}}
\newtheorem{corollary}{{\bf Corollary}}
\newtheorem{lemma}{{\bf Lemma}}
\newtheorem{remark}{{\bf Remark}}
\newtheorem{assumption}{{\bf Assumption}}
\newcommand{\auc}{\rm  AUC}
\newcommand{\iauc}{\rm  IAUC}
\newcommand{\roc}{\rm  ROC}
\newcommand{\iroc}{\rm  IROC}
\def\vus{{\rm VUS}}
\def\X{{\mathcal X}}
\newcommand{\n}{\llbracket n \rrbracket}
\def\e{\mathbf e}
\def\i{\mathbf i}
\def\j{\mathbf j}

\title{Ranking Median Regression with Pairwise Comparisons}
\author{
  Stephan Cl\'emen\c{c}on\\
  LTCI Telecom ParisTech, Universit\'e Paris-Saclay\\
  75013, Paris, France\\
  \texttt{first.last@telecom-paristech.fr}
}
\begin{document}


\maketitle

\begin{abstract} In the present era of personalized customer services and recommender systems, predicting the preferences of an individual/user over a (possibly very large) set of items indexed by $\n=\{1,\; \ldots,\; n\}$, $n\geq 1$, based on its characteristics, modelled as a r.v. $X$, taking its values in a feature space $\mathcal{X}$ say, is an ubiquitous issue. Though easy to state, this predictive problem is very difficult to solve in practice, due to two major difficulties essentially. The first one lies in the fact that, here, the (discrete) output space is the symmetric group $\mathfrak{S}_n$, composed of all permutations of $\n$, of explosive cardinality $n!$, and which is not a subset of a vector space. It is thus far from straightforward to build effectively predictive rules taking their values in $\mathfrak{S}_n$, with the noteworthy exception of methods implementing ranking aggregation techniques at a local level, as proposed in \cite{AY14} or \cite{CKS17bis}. We will consider the framework where the preferences are expressed in the form of pairwise comparisons between items. It is the purpose of this paper to ...
	%explain how the notion of \textit{multiresolution analysis} (MRA) of incomplete rankings, recently introduced in \cite{SCK15} may offer a promising solution to this supervised learning problem, when combined with the concept of \textit{local learning}.
%As a by-product, the partition thus obtained is shown to define a regression estimate, much more robust to outliers than that produced by the {\sc CART} algorithm.
%Adaptive discretization of the problem, of the (continuous) label variable
\end{abstract}

%\begin{keywords}
%Integral approximation, exponential inequalities, Kernel estimation, Monte-Carlo method, projection method, rate bounds, statistical sampling, $U$-statistics
%\end{keywords}

\section{Introduction}
In an increasing number of modern applications/interfaces, users are invited to declare their individual characteristics (\textit{e.g.} socio-demographic features), taking the form of a random vector $X$ valued in a an input space $\mathcal{X}\subset \mathbb{R}^d$ say, and express their preferences over subsets of the collection of numbered services/products $\n=\{1,\; \ldots,\; n\}$ offered to them. In this context, the goal pursued is to learn from historical data how to predict the preferences of any user based on her characteristics $X$, the prediction being of the form of a permutation $S(X)$ on $\n$, mapping any item $i$ to its rank $S_i(X)$ on her preference list. Denoting by $\Sigma$ the permutation that truly reflects the preferences of a user with characteristics $X$, the performance of any predictive rule, \textit{i.e.} any measurable function $S:\mathcal{X}\rightarrow \mathfrak{S}_n$, can be measured by the expected Kendall $\tau$ distance between
\begin{equation}\label{eq:risk_theo}
\mathcal{R}(S)=\mathbb{E}\left[d_{\tau}\left(S(X),\Sigma  \right)  \right],
\end{equation}  
where the expectation is taken over the (unknown) distribution of the pair $(X,\Sigma)$ and $d_{\tau}(\sigma,\sigma')=\sum_{i<j}\mathbb{I}\{(\sigma(j)-\sigma(i)) \cdot (\sigma'(j)-\sigma'(i))<0   \}$ for all $(\sigma,\sigma')\in \mathfrak{S}_n^2$, denoting by $\mathbb{I}\{\mathcal{E}  \}$ the indicator function of any event $\mathcal{E}$. Stated this way, the objective is to build a mapping $S$ that minimizes \eqref{eq:risk_theo} and one may easily show with a straightforward conditioning argument that the optimal predictors are the rules that maps any point $X$ in the input space to any (Kemeny) ranking median of $P_X$, $\Sigma$'s conditional distribution given $X$. Recall that a Kemeny median of a probability distribution $P$ on $\mathfrak{S}_n$ is any solution of the optimization problem
\begin{equation}\label{eq:median}
\min_{\sigma\in \mathfrak{S}_n}L_P(\sigma)
\end{equation}
where $L_P(\sigma)=\mathbb{E}_{\Sigma \sim P}\left[ d_{\tau}(\sigma,\Sigma) \right]$. For this reason, the predictive problem formulated above is referred to as \textit{ranking median regression} in this paper. The latter has been recently investigated in \cite{CKS17bis} in the case where independent copies of $(X,\Sigma)$ form the training dataset and it has also been shown therein that techniques for statistical Kemeny aggregation, \textit{i.e.} for solving \eqref{eq:median}, when implemented at local levels, may produce efficient techniques to minimize \eqref{eq:risk_theo} approximately (namely, nearest-neighbour and decision tree methods). However, in many situations, users do not express their preferences on the whole catalog of products/services and the observations stored in the historical database are not full rankings $\Sigma$, but permutations on small and variable subsets of $\n$, termed \textit{incomplete rankings}. In this paper, we will consider the particuliar case of pairwise comparisons. %In this context, statistical estimation of the quantity \eqref{eq:risk_theo} is far from immediate and extending the \textit{empirical risk minimization} (ERM) paradigm to ranking median regression is a challenge. %It is precisely the purpose of this paper to show that the multiresolution analysis (MRA) of incomplete rankings introduced in \cite{SCK15} in an unsupervised setting enables the design of ranking median regression methods in this situation   

\section{Background and Preliminaries}


\subsection{Statistical Framework for Ranking Median Regression}

\noindent {\bf Statistical setting.} We assume that we observe $(X_1,\; \Sigma_1)\; \ldots,\; (X_1,\; \Sigma_N)$, $N\geq 1$ i.i.d. copies of the pair $(X,\; \Sigma)$ and, based on these training data, the objective is to build a predictive ranking rule $s$ that nearly minimizes $\mathcal{R}(s)$ over the class $\mathcal{S}$ of measurable mappings $s:\mathcal{X}\rightarrow \mathfrak{S}_n$.
Of course, the Empirical Risk Minimization (ERM) paradigm encourages to consider solutions of the empirical minimization problem:
\begin{equation}\label{eq:ERM}
\min_{s\in \mathcal{S}_0}\widehat{\mathcal{R}}_N(s),
\end{equation}
where $\mathcal{S}_0$ is a subset of $\mathcal{S}$, supposed to be rich enough for containing approximate versions of elements of $\mathcal{S}^*$ (\textit{i.e.} so that $\inf_{s\in \mathcal{S}_0}\mathcal{R}(s)- \mathcal{R}^*$ is 'small') and ideally appropriate for continuous or greedy optimization, and
\begin{equation}\label{eq:emp_reg_risk}
\widehat{\mathcal{R}}_N(s)=\frac{1}{N}\sum_{k=1}^N d_{\tau}(s(X_k),\;  \Sigma_k)
\end{equation}
is a statistical version of \eqref{eq:risk_theo} based on the $(X_i,\Sigma_i)$'s. 


Let $\mathcal{E}_n$ be the set $\binom{n}{2}$ pairs of items. Since the computation of Kendall's $\tau$ distance involves pairwise comparisons only, one could compute empirical versions of the risk functional $\mathcal{R}$ in a statistical framework stipulating that the observations are less complete than pairs $(X, \Sigma)$. More formally, now a training example consists of some characteristics $X_k$, and a sequence of i.i.d. pairs $\{(\mathbf{e}_k,\; \epsilon_k),\;  k=1,\; \ldots,\; N\}$, where the $\mathbf{e}_k=(\i_k,\j_k)$'s are independent from the $\Sigma_k$'s  and drawn from an unknown distribution $\nu_{X_k}$ on the set $\mathcal{E}_n$ such that $\nu_{X_k}(e)>0$ for all $e\in \mathcal{E}_n$ and $\epsilon_k=sgn( \Sigma_k(\j_k)- \Sigma_k(\i_k) )$ with $\mathbf{e}_k=(\i_k,\j_k)$ for $1\leq k\leq N$. Based on these observations, an estimate of the risk $\mathcal{R}(s)=\mathbb{E}_{X \sim\mu}\mathbb{E}_{\Sigma \sim P_X}[\mathbb{I}\{ \e=(i,j),\;  \epsilon(\sigma(j)-\sigma(i))<0\}]$ of any ranking rule $s$ is given by:
\begin{equation*}
\widehat{\mathcal{R}}_N(s)=\sum_{i<j}\frac{1}{N_{i,j}}\sum_{k=1}^N\mathbb{I}\{ \e_k=(i,j),\;  \epsilon_k(s(X_k)(j)-s(X_k)(i))<0 \},
\end{equation*}
where $N_{i,j}=\sum_{k=1}^N\mathbb{I}\{ \e_k=(i,j)\}$.
%, see for instance \citet{LB14a} or \citet{RA14} for ranking aggregation results in this setting.
To make the problem simpler, we will make the following hypothesis on the \textit{design distribution} $\nu_x$ for $x \in \mathcal{X}$...


\subsection{Transitivity}

A key property to recover a ranking from pairwise comparisons only is the following.
\begin{definition}\label{def:stoch_trans}
	The probability distribution $P$ on $\mathfrak{S}_n$ is stochastically transitive iff
	$$
	\forall (i,j,k)\in \n^3:\;\;  p_{i,j}\geq 1/2 \text{ and } p_{j,k}\geq 1/2 \; \Rightarrow\; p_{i,k}\geq 1/2.
	$$
	If, in addition, $p_{i,j}\neq 1/2$ for all $i<j$, $P$ is said to be strictly stochastically transitive.  
\end{definition} 
When stochastic transitivity holds true, the set of Kemeny medians (see Theorem 5 in \cite{CKS17}) is the (non empty) set 
\begin{equation}\label{eq:opt_sol}
\{\sigma\in \mathfrak{S}_n:\; (p_{i,j}-1/2)(\sigma(j)-\sigma(i ))>0 \text{ for all } i<j \text{ s.t. } p_{i,j}\neq 1/2  \},
\end{equation}
the minimum is given by
\begin{equation}\label{eq:inf}
L^*_P=\sum_{i<j}\min\{p_{i,j},1-p_{i,j}  \}=\sum_{i<j}\{  1/2-\vert  p_{i,j}-1/2\vert\}
\end{equation}
and, for any $\sigma\in \mathfrak{S}_n$,
$
L_P(\sigma)-L^*_P=2\sum_{i<j}\left\vert p_{i,j}-1/2 \right\vert\cdot \mathbb{I}\{ (\sigma(i)-\sigma(j))\left(p_{i,j}-1/2\right)<0\}.
$
If a strict version of stochastic transitivity is fulfilled, we denote by $\sigma^*_P$ the Kemeny median which is unique and given by the Copeland ranking:
\begin{equation}\label{eq:sol_SST}
\sigma^*_P(i)=1+\sum_{k\neq i}\mathbb{I}\{p_{i,k}<1/2  \} \text{ for } 1\leq i\leq n.
\end{equation}

\section{The Algorithm}

Measuring dispersion of cell $\mathcal{C}$ with:
\begin{equation*}
\gamma_{\widehat{P}_{\mathcal{C}}}=\frac{1}{2} \sum_{i<j}\widehat{p}_{i,j}(\mathcal{C})(1-\widehat{p}_{i,j}(\mathcal{C}))
\end{equation*}
where $\widehat{p}_{i,j}(\mathcal{C})$=$\frac{1}{N_{i,j}(\mathcal{C})}\sum_{k:X_k \in \mathcal{C} } \mathbb{I}\{ \e_k=(i,j),\;  \epsilon_k>0 \}$



\bibliographystyle{plain}

\bibliography{biblio-thesis}
\end{document}
