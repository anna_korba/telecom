\documentclass{article}
\usepackage[final]{nips_2017}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{xcolor}
\usepackage{float}
\usepackage{tikz}
\usetikzlibrary{decorations.markings}
\usepackage{blkarray}
\usepackage{bbm}
\usepackage{float}
\usepackage{tabularx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{tikz}
\usepackage{stmaryrd}
\usetikzlibrary{matrix,arrows}
\usepackage{url}
\usepackage{comment}
\usepackage{multirow}
\usepackage{arydshln}
%\usepackage{kbordermatrix}
\usepackage{pgfplots}
\pgfmathdeclarefunction{gauss}{2}{%
  \pgfmathparse{1/(#2*sqrt(2*pi))*exp(-((x-#1)^2)/(2*#2^2))}%
}

\newtheorem{theorem}{{\bf Theorem}}
\newtheorem{example}{{\bf Example}}
\newtheorem{proposition}{{\bf Proposition}}
\newtheorem{definition}{{\bf Definition}}
\newtheorem{corollary}{{\bf Corollary}}
\newtheorem{lemma}{{\bf Lemma}}
\newtheorem{remark}{{\bf Remark}}
\newtheorem{assumption}{{\bf Assumption}}
\newcommand{\auc}{\rm  AUC}
\newcommand{\iauc}{\rm  IAUC}
\newcommand{\roc}{\rm  ROC}
\newcommand{\iroc}{\rm  IROC}
\def\vus{{\rm VUS}}
\def\X{{\mathcal X}}
\newcommand{\n}{\llbracket n \rrbracket}
\def\e{\mathbf e}
\def\i{\mathbf i}
\def\j{\mathbf j}

\title{Ranking Median Regression: \\Learning to Order through Local Consensus}
\author{
  Stephan Cl\'emen\c{c}on, Anna Korba\\
  LTCI Telecom ParisTech, Universit\'e Paris-Saclay\\
  75013, Paris, France\\
  \texttt{\{stephan.clemencon, anna.korba\}@telecom-paristech.fr}
}
\begin{document}


\maketitle

\begin{abstract} In the present era of personalized customer services and recommender systems, predicting the preferences of an individual/user over a (possibly very large) set of items indexed by $\n=\{1,\; \ldots,\; n\}$, $n\geq 1$, based on its characteristics, modelled as a r.v. $X$, taking its values in a feature space $\mathcal{X}$ say, is an ubiquitous issue. Though easy to state, this predictive problem is very difficult to solve in practice. The major challenge lies in the fact that, here, the (discrete) output space is the symmetric group $\mathfrak{S}_n$, composed of all permutations of $\n$, of explosive cardinality $n!$, and which is not a subset of a vector space. It is the purpose of this paper to explain how to build effectively nearly optimal predictive rules taking their values in $\mathfrak{S}_n$, by means of efficient ranking aggregation/consensus techniques implemented at a local level.
	%explain how the notion of \textit{multiresolution analysis} (MRA) of incomplete rankings, recently introduced in \cite{SCK15} may offer a promising solution to this supervised learning problem, when combined with the concept of \textit{local learning}.
%As a by-product, the partition thus obtained is shown to define a regression estimate, much more robust to outliers than that produced by the {\sc CART} algorithm.
%Adaptive discretization of the problem, of the (continuous) label variable
\end{abstract}

%\begin{keywords}
%Integral approximation, exponential inequalities, Kernel estimation, Monte-Carlo method, projection method, rate bounds, statistical sampling, $U$-statistics
%\end{keywords}

\section{Introduction}
In an increasing number of modern applications/interfaces, users are invited to declare their individual characteristics (\textit{e.g.} socio-demographic features), taking the form of a random vector $X$ valued in an input space $\mathcal{X}\subset \mathbb{R}^d$ say, and express their preferences on a collection of numbered services/products $\n=\{1,\; \ldots,\; n\}$ offered to them. In this context, the goal pursued is to learn from historical data how to predict the preferences of any user based on her characteristics $X$, the prediction being of the form of a permutation $s(X)$ on $\n$, mapping any item $i$ to its rank $s(X)(i)$ on her preference list. Denoting by $\Sigma$ the permutation that truly reflects the preferences of a user with characteristics $X$, the performance of any predictive rule, \textit{i.e.} any measurable function $s:\mathcal{X}\rightarrow \mathfrak{S}_n$, can be measured by the expected Kendall $\tau$ distance between $s(X)$ and $\Sigma$
\begin{equation}\label{eq:risk_theo}
\mathcal{R}(S)=\mathbb{E}\left[d_{\tau}\left(s(X),\Sigma  \right)  \right],
\end{equation}  
where the expectation is taken over the (unknown) distribution of the pair $(X,\Sigma)$ and $d_{\tau}(\sigma,\sigma')=\sum_{i<j}\mathbb{I}\{(\sigma(j)-\sigma(i)) \cdot (\sigma'(j)-\sigma'(i))<0   \}$ for all $(\sigma,\sigma')\in \mathfrak{S}_n^2$, denoting by $\mathbb{I}\{\mathcal{E}  \}$ the indicator function of any event $\mathcal{E}$. Stated this way, the objective is to build a mapping $s$ that minimizes \eqref{eq:risk_theo} and one may easily show with a straightforward conditioning argument that the optimal predictors are the rules that maps any point $X$ in the input space to any (Kemeny) ranking median of $P_X$, $\Sigma$'s conditional distribution given $X$. Recall that a Kemeny median of a probability distribution $P$ on $\mathfrak{S}_n$ is any solution $\sigma^*$ of the optimization problem
\begin{equation}\label{eq:median}
\min_{\sigma\in \mathfrak{S}_n}L_P(\sigma)
\end{equation}
where $L_P(\sigma)=\mathbb{E}_{\Sigma \sim P}\left[ d_{\tau}(\sigma,\Sigma) \right]$. For this reason, the predictive problem formulated above is referred to as \textit{ranking median regression} in this paper. Here we investigate the latter (see also \cite{CKS17bis}) in the case where independent copies of $(X,\Sigma)$ form the training dataset and show that techniques for statistical Kemeny aggregation, \textit{i.e.} for solving \eqref{eq:median}, when implemented at local levels, may produce efficient techniques to minimize \eqref{eq:risk_theo} approximately (namely, nearest-neighbour and decision tree methods). Throughout the article, we denote by $L_P^*=L_P(\sigma^*)$ the minimum of \eqref{eq:median} and by $\mathcal{R}^*=\mathbb{E}_{X\sim \mu}[L^*_{P_X}]$ the minimum of \eqref{eq:risk_theo}. The Dirac mass at any point $a$  is denoted by $\delta_a$. 

\section{Background and Preliminaries}


\subsection{A Statistical View of Consensus Ranking}\label{subsec:consensus}

 Whereas problem \eqref{eq:median} is NP-hard in general, exact solutions, referred to as \textit{Kemeny medians}, can be explicited when the pairwise probabilities $p_{i,j}=\mathbb{P}\{  \Sigma(i)<\Sigma(j)\}$, $1\leq i\neq j\leq n$, fulfill the following property, referred to as \textit{stochastic transitivity}.

\begin{definition}\label{def:stoch_trans}
	The probability distribution $P$ on $\mathfrak{S}_n$ is stochastically transitive iff
	$$
	\forall (i,j,k)\in \n^3:\;\;  p_{i,j}\geq 1/2 \text{ and } p_{j,k}\geq 1/2 \; \Rightarrow\; p_{i,k}\geq 1/2.
	$$
	If, in addition, $p_{i,j}\neq 1/2$ for all $i<j$, $P$ is said to be strictly stochastically transitive.  
\end{definition} 
When stochastic transitivity holds true, the set of Kemeny medians (see Theorem 5 in \cite{CKS17}) is the (non empty) set 
\begin{equation}\label{eq:opt_sol}
\{\sigma\in \mathfrak{S}_n:\; (p_{i,j}-1/2)(\sigma(j)-\sigma(i ))>0 \text{ for all } i<j \text{ s.t. } p_{i,j}\neq 1/2  \},
\end{equation}
%the minimum $L^*_P=L_P(\sigma^*)$ is given by
%\begin{equation}\label{eq:inf}
%L^*_P=\sum_{i<j}\min\{p_{i,j},1-p_{i,j}  \}=\sum_{i<j}\{  1/2-\vert  p_{i,j}-1/2\vert\}
%\end{equation}
%and, for any $\sigma\in \mathfrak{S}_n$,
%$L_P(\sigma)-L^*_P=2\sum_{i<j}\left\vert p_{i,j}-1/2 \right\vert\cdot \mathbb{I}\{ (\sigma(i)-\sigma(j))\left(p_{i,j}-1/2\right)<0\}.$
and, if a strict version of stochastic transitivity is fulfilled (meaning that, in addition, none of the pairwise probabilities is equal to $1/2$), the Kemeny median is unique and given by the Copeland ranking:
\begin{equation}\label{eq:sol_SST}
\sigma^*_P(i)=1+\sum_{k\neq i}\mathbb{I}\{p_{i,k}<1/2  \} \text{ for } 1\leq i\leq n.
\end{equation}

We denote by $\mathcal{T}$ the set of strictly stochastically transitive distributions on $\mathfrak{S}_n$. Assume that we observe i.i.d. copies $\Sigma_1,\dots, \Sigma_N$ of a generic r.v. $\Sigma \sim P$ and let  $\widehat{P}_N=(1/N)\sum_{i=1}^N\delta_{\Sigma_i}$.  Suppose that the underlying distribution $P$ belongs to $\mathcal{T}$ and satisfies the low-noise condition {\bf NA}$(h)$ for a given $h>0$:
\begin{equation}\label{eq:hyp_margin0}
\min_{i<j}\left\vert p_{i,j}-1/2 \right\vert \ge h.
\end{equation}
It is shown in \cite{CKS17} that the empirical distribution $\widehat{P}_N$ is strictly stochastically transitive as well, with overwhelming probability, and that the expectation of the excess of risk of empirical Kemeny medians decays at an exponential rate, see Proposition 14 therein. In this case, the nearly optimal solution $\sigma^*_{\widehat{P}_N}$ can be made explicit and straightforwardly computed using Eq. \eqref{eq:sol_SST} based on the empirical pairwise probabilities $$ \widehat{p}_{i,j}=\frac{1}{N}\sum_{k=1}^N\mathbb{I}\{ \Sigma_k(i)<\Sigma_k(j)  \}, \; i<j.
$$ Otherwise, solving the NP-hard problem $\min_{\sigma \in \mathfrak{S}_n}L_{\widehat{P}_N}(\sigma)$ requires to get an empirical Kemeny median. However, as can be seen by examining Proposition 14's proof in \cite{CKS17}, the exponential rate bound holds true for any candidate $\widetilde{\sigma}_N$ in $\mathfrak{S}_n$ that coincides with $\sigma^*_{\widehat{P}_N}$ when the empirical distribution lies in $\mathcal{T}$ and takes arbitrary values in $\mathfrak{S}_n$ otherwise. In practice, when $\widehat{P}_N$ does not belong to $\mathcal{T}$, we propose to consider as a pseudo-empirical median any permutation $\widetilde{\sigma}^*_{\widehat{P}_N}$ that ranks the objects as the empirical Borda count:
$$ 
\left( \sum_{k=1}^N\Sigma_k(i)-\sum_{k=1}^N\Sigma_k(j)\right)\cdot \left( \widetilde{\sigma}^*_{\widehat{P}_N}(i)-\widetilde{\sigma}^*_{\widehat{P}_N}(j)   \right)>0 \text{ for all } i<j \text{ s.t. } \sum_{k=1}^N\Sigma_k(i) \ne \sum_{k=1}^N\Sigma_k(j) ,
$$
breaking possible ties in an arbitrary fashion. %Alternative choices could also be guided by least-squares approximation of the empirical pairwise probabilities, see \cite{JLYY11}. 

\subsection{Statistical Framework for Ranking Median Regression}\label{subsec:RMR}
 We assume now that we observe $(X_1,\; \Sigma_1)\; \ldots,\; (X_1,\; \Sigma_N)$ i.i.d. copies of the pair $(X,\; \Sigma)$ and, based on these training data, the objective is to build a predictive ranking rule $s$ that nearly minimizes $\mathcal{R}(s)$ over the class $\mathcal{S}$ of measurable mappings $s:\mathcal{X}\rightarrow \mathfrak{S}_n$.
Of course, the Empirical Risk Minimization (ERM) paradigm encourages to consider solutions of the optimization problem:
\begin{equation}\label{eq:ERM}
\min_{s\in \mathcal{S}_0}\widehat{\mathcal{R}}_N(s),
\end{equation}
where $\mathcal{S}_0$ is a subset of $\mathcal{S}$, supposed to be rich enough for containing approximate versions of elements of $\mathcal{S}^*$ (\textit{i.e.} so that $\inf_{s\in \mathcal{S}_0}\mathcal{R}(s)- \mathcal{R}^*$ is 'small') and ideally appropriate for continuous or greedy optimization, and
\begin{equation}\label{eq:emp_reg_risk}
\widehat{\mathcal{R}}_N(s)=\frac{1}{N}\sum_{k=1}^N d_{\tau}(s(X_k),\;  \Sigma_k)
\end{equation}
is a statistical version of \eqref{eq:risk_theo} based on the $(X_i,\Sigma_i)$'s. Extending those established by \cite{CKS17} in the context of ranking aggregation, statistical results describing the generalization capacity of minimizers of \eqref{eq:emp_reg_risk} can be established under classic complexity assumptions for the class $\mathcal{S}_0$, as revealed by the result stated below.


\begin{proposition}\label{prop:upper_bound}
	Suppose that, for all $i<j$, the collection of sets
		$$
		\left\{ \left\{x\in \mathcal{X}:\; s(x)(i)-s(x)(j)>0   \right\}:\; s\in \mathcal{S}_0 \right\}\cup \left\{\left\{x\in \mathcal{X}:\; s(x)(i)-s(x)(j)<0   \right\}:\; s\in \mathcal{S}_0\right\}
		$$
		is of finite {\sc VC} dimension $V<\infty$. Let $\widehat{s}_N$ be any minimizer of the empirical risk \eqref{eq:emp_reg_risk} over $\mathcal{S}_0$. For any $\delta\in (0,1)$, we have with probability at least $1-\delta$: $\forall N\geq 1$,
	\begin{equation}\label{eq:ERM_bound_gen}
	\mathcal{R}(\widehat{s}_N)-\mathcal{R}^*\leq C\sqrt{\frac{V\log(n(n-1) /(2\delta))}{N}}+\left\{  \mathcal{R}^*-\inf_{s\in \mathcal{S}_0}\mathcal{R}(s) \right\},
	\end{equation}
	where $C<+\infty$ is a universal constant.
\end{proposition}
One may also prove that rates of convergence for the excess of risk of empirical Kemeny medians can be much faster than $O_{\mathbb{P}}(1/\sqrt{N})$ under the following hypothesis (generalizing \eqref{eq:hyp_margin0}), involved in the subsequent analysis (\cite{CKS17bis}). 

\begin{assumption}\label{hyp:margin} For all $x\in \mathcal{X}$, $P_x\in \mathcal{T}$ and 
	$ H=\inf_{x\in \mathcal{X}}\min_{i<j}\left\vert p_{i,j}(x)-1/2 \right\vert >0$.
\end{assumption}
This condition is checked in many situations, including most conditional parametric models (see Remark~13 in \cite{CKS17}), and generalizes condition \eqref{eq:hyp_margin0}, which corresponds to Assumption \ref{hyp:margin} when $X$ and $\Sigma$ are independent. The result stated below reveals that a similar fast rate phenomenon occurs for minimizers of the empirical risk \eqref{eq:emp_reg_risk} if Assumption \ref{hyp:margin} is satisfied.

\begin{proposition}\label{prop:fast}
	Suppose that Assumption \ref{hyp:margin} is fulfilled, that the cardinality of class $\mathcal{S}_0$ is equal to $C<+\infty$ and that the unique true risk minimizer $s^*(x)=\sigma^*_{P_x}$ belongs to $\mathcal{S}_0$. Let $\widehat{s}_N$ be any minimizer of the empirical risk \eqref{eq:emp_reg_risk} over $\mathcal{S}_0$. For any $\delta\in (0,1)$, we have with probability at least $1-\delta$:
	\begin{equation}
	\mathcal{R}(\widehat{s}_N)-\mathcal{R}^*\leq \left(\frac{n(n-1)}{2H}\right)\times \frac{\log(C/\delta)}{ N}.
	\end{equation}
\end{proposition}


\section{Local Consensus Methods for Ranking Median Regression}

We start here with introducing notations to describe the class of piecewise constant ranking rules and explore next approximation of a given ranking rule $s(x)$ by elements of this class, based on a local version of the concept of Kemeny median. Two strategies are next investigated in order to generate adaptively a partition tailored to the training data and yielding a ranking rule with nearly minimum predictive error. 
%An iterative refinement strategy is next described to generate adaptively a partition tailored to the training data and yielding a ranking rule with nearly minimum predictive error. 
Throughout this section, for any measurable set $\mathcal{C}\subset \mathcal{X}$ weighted by $\mu(x)$, the conditional distribution of $\Sigma$ given $X\in \mathcal{C}$ is denoted by $P_{\mathcal{C}}$. When it belongs to $\mathcal{T}$, the unique median of distribution $P_{\mathcal{C}}$ is denoted by $\sigma^*_{\mathcal{C}}$ and referred to as the local median on region $\mathcal{C}$.


\subsection{Piecewise Constant Predictive Ranking Rules}
Let $\mathcal{P}$ be a partition of $\mathcal{X}$ composed of $K\geq 1$ cells $\mathcal{C}_1,\; \ldots,\; \mathcal{C}_K$ (\textit{i.e.} the $\mathcal{C}_k$'s are pairwise disjoint and their union is the whole feature space $\mathcal{X}$). Suppose in addition that $\mu(\mathcal{C}_k)>0$ for $k=1,\; \ldots,\; K$. Using the natural embedding $\mathfrak{S}_n\subset \mathbb{R}^n$, any ranking rule $s\in \mathcal{S}$ that is constant on each subset $\mathcal{C}_k$ can be written as 
\begin{equation}\label{eq:piecewise_cst}
s_{\mathcal{P}, \bar{\sigma}}(x)=\sum_{k=1}^K\sigma_k \cdot \mathbb{I}\{x\in \mathcal{C}_k\},
\end{equation}
where $\bar{\sigma}=(\sigma_1,\; \ldots,\; \sigma_K)$ is a collection of $K$ permutations. We denote by $\mathcal{S}_{\mathcal{P}}$ the collection of all ranking rules that are constant on each cell of $\mathcal{P}$. The following result describes the most accurate ranking median regression function in this class.

\begin{proposition}
	If $P_{\mathcal{C}_k}\in \mathcal{T}$ for $1\leq k\leq K$, there exists a unique minimizer given by: $\forall x\in \mathcal{X}$, 
	\begin{equation}\label{eq:piecewise_cst_opt}
	s^*_{\mathcal{P}}(x)=\sum_{k=1}^K\sigma^*_{P_{\mathcal{C}_k}} \cdot \mathbb{I}\{x\in \mathcal{C}_k\}.
	\end{equation}
\end{proposition}
 We now investigate to what extent ranking median regression functions $s^*(x)$ can be well approximated by predictive rules of the form \eqref{eq:piecewise_cst}.
\begin{assumption}\label{hyp:smooth} For all $1\leq i<j\leq n$, the mapping $x\in \mathcal{X}\mapsto p_{i,j}(x)$ is Lipschitz, \textit{i.e.} there exists $M<\infty$ such that:
	\begin{equation}
	\forall (x,x')\in\mathcal{X}^2,\;\; \sum_{i<j}\vert p_{i,j}(x)-p_{i,j}(x') \vert\leq M \cdot \vert\vert x-x'\vert\vert .
	\end{equation}
\end{assumption}
%For simplicity, we assume that $\mathcal{X}=[0,1]^d$.
The following result shows that, under the assumptions above, the optimal prediction rule $\sigma^*_{P_X}$ can be accurately approximated by \eqref{eq:piecewise_cst_opt}, provided that the regions $\mathcal{C}_k$ are 'small' enough.
\begin{theorem}\label{thm:approx}
	Suppose that Assumptions \ref{hyp:margin}-\ref{hyp:smooth} are fulfilled and that $P_{\mathcal{C}}\in \mathcal{T}$ for all $\mathcal{C}\in \mathcal{P}$. Then, we have:
	\begin{equation}
	\mathbb{E}\left[d_{\tau}\left(\sigma^*_{P_X}, s^*_{\mathcal{P}}(X)\right)  \right]\leq \sup_{x\in \mathcal{X}}d_{\tau}\left(\sigma^*_{P_x}, s^*_{\mathcal{P}}(x)\right)\leq (M/H)\cdot \delta_{\mathcal{P}},
	\end{equation}
	where $\delta_{\mathcal{P}}=\max_{\mathcal{C}\in \mathcal{P}}\sup_{(x,x')\in \mathcal{C}^2}\vert\vert x-x'\vert\vert$ is the maximal diameter of $\mathcal{P}$'s cells. Hence, if $(\mathcal{P}_m)_{m\geq 1}$ is a sequence of partitions of $\mathcal{X}$ such that $P_{\mathcal{C}}\in \mathcal{T}$ for all $\mathcal{C}\in \mathcal{P}_m$, $m\geq 1$, and $\delta_{\mathcal{P}_m}\rightarrow 0$ as $m$ tends to infinity, then $$\sup_{x\in \mathcal{X}}d_{\tau}\left(\sigma^*_{P_x}, s^*_{\mathcal{P}_m}(x)\right)\rightarrow 0, \text{ as } m\rightarrow \infty.$$
\end{theorem}

\subsection{Algorithms and Results}\label{sec:alg}

 
\textbf{Nearest-Neighbor Rules for Ranking Median Regression}. 
Fix $k\in\{1,\; \ldots,\; N  \}$ and a query point $x \in \mathcal{X}$. Sort the training data $(X_1, \Sigma_1), \dots, (X_n, \Sigma_n)$ by increasing order of the distance to $x$, measured, for simplicity, by $\| X_i - x \|$ for a certain norm chosen on $\mathcal{X}\subset \mathbb{R}^d$ say: 
% \begin{equation*}
$ \| X_{(1,N)} - x \|\leq \ldots\leq  \| X_{(N,N)} - x \|$.
%\end{equation*}
Consider next the empirical distribution calculated using the $k$ training points closest to $x$
\begin{equation}
\widehat{P}(x)=\frac{1}{k}\sum_{l=1}^k\delta_{\Sigma_{(l,N)}}
\end{equation}
and compute next the (pseudo)-empirical Kemeny median, as described in subsection \ref{subsec:consensus}, yielding
%$$
%\widetilde{p}(x)= \argmin_{p'': G_{p''} acyclic}\sum_{i<j}\left(\widehat{p}_{i,j}(x)-p^{''}_{i,j}(x)\right)^2.
%$$
the $k$-NN prediction at $x$:
\begin{equation}\label{eq:k_NN}
%s_{N}(x)=\argmin_{\sigma \in \mathfrak{S}_n} \sum_{l=1}^{k_N} d_{\tau}(\sigma, \Sigma_{(l,N)}(x))
s_{k,N}(x)\overset{def}{=}\widetilde{\sigma}^*_{\widehat{P}(x)}.
%=\sigma_{\widetilde{p}(x)}^*.
\end{equation}
Observe incidentally that, under Assumptions  \ref{hyp:margin}-\ref{hyp:smooth}, if $\| X_{(k,N)} - x \| < H/M$ then $\widehat{P}$ is necessarily strictly stochastically transitive. The result stated below provides an upper bound for the expected risk excess of \eqref{eq:k_NN}, which reflects the usual bias/variance trade-off ruled by $k$ for fixed $N$ and asymptotically vanishes as soon as $k\rightarrow \infty$ as $N\rightarrow \infty$ such that $k=o(N)$. Notice incidentally that the choice $k\sim N^{2/(d+2)}$ yields the asymptotically optimal upper bound, of order $N^{-1/(2+d)}$.

\begin{theorem}\label{thm:NN_consist}
	Suppose that Assumptions \ref{hyp:margin}-\ref{hyp:smooth} are fulfilled, that the r.v. $X$ is bounded and $d\ge 3$. Then, we have: $\forall N\geq 1$, $\forall k\in\{1,\; \ldots,\; N  \}$,
	\begin{multline}
	%\mathbb{E}\left[  \mathcal{R}(s_{k,N})-\mathcal{R}^*  \right]\leq 2 \left(\frac{n(n-1)}{2}\right)^{3/2} \left( 1/(2\sqrt{k}) + \sqrt{c_1}M \left(k/N\right)^{1/d}\right)\\ +\frac{(n(n-1))}{2}\frac{\sqrt{c_2}M}{H}\frac{1}{(N-k)^{1/d}},\\
	\mathbb{E}\left[  \mathcal{R}(s_{k,N})-\mathcal{R}^*  \right]\leq \frac{n(n-1)}{2} \left( \frac{1}{\sqrt{k}} + 2\sqrt{c_1}M \left(\frac{k}{N}\right)^{1/d} + \frac{2\sqrt{c_2}M}{H}\frac{1}{(N-k)^{1/d}}\right)\\
	\end{multline}
	where $c_1$ and $c_2$ are constant which only depend on $\mu$'s support.
\end{theorem}
 The implementation of this simple local method for ranking median regression does not require to explicit the underlying partition but is classically confronted with the curse of dimensionality. The next subsection explains how another local method, based on the popular tree induction heuristic, scales with the dimension of the input space by contrast.

\textbf{Recursive Partitioning (CRIT)}.
We now describe an iterative scheme for building an appropriate tree-structured partition $\mathcal{P}$, adaptively from the training data. Whereas the splitting criterion in most recursive partitioning methods is heuristically motivated,
the local learning method we describe below relies on the ERM principle formulated in subsection \ref{subsec:RMR}, so as to build by refinement a partition $\mathcal{P}$  based on a training sample $\mathcal{D}_N=\{(\Sigma_1,\; \X_1),\; \ldots,\; (\Sigma_N,\; X_N)  \}$ so that, on each cell $\mathcal{C}$ of $\mathcal{P}$, the $\Sigma_i$'s lying in it exhibit a small variability in the Kendall $\tau$ sense and, consequently, may be accurately approximated by a local Kemeny median. 
The goal pursued is thus to construct recursively a piecewise constant ranking rule associated to a partition $\mathcal{P}$, $s_{\mathcal{P}}(x)=\sum_{\mathcal{C}\in \mathcal{P}}\sigma_{\mathcal{C}}\cdot \mathbb{I}\{x\in \mathcal{C}  \}$, with minimum empirical risk
\begin{equation}\label{eq:train1}
\widehat{L}_N(s_{\mathcal{P}})=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})L_{\widehat{P}_{\mathcal{C}}}(\sigma_{\mathcal{C}}),
\end{equation}
where $\widehat{\mu}_N=(1/N)\sum_{k=1}^N\delta_{X_k}$ is the empirical measure of the $X_k$'s and, for any measurable subset $\mathcal{C}\subset \mathcal{X}$, $N_{\mathcal{C}}=\sum_{k=1}^N \mathbb{I}\{ X_k\in \mathcal{C}\}$ and $\widehat{P}_{\mathcal{C}}=(1/N_{\mathcal{C}})\sum_{k: X_k\in \mathcal{C}}\delta_{\Sigma_k}$ is the empirical version of $\Sigma$'s conditional distribution given $X\in \mathcal{C}$. We also denote by $\widehat{p}_{i,j}(\mathcal{C})=(1/N_{\mathcal{C}})\sum_{k:\; X_k\in \mathcal{C}}\mathbb{I}\{\Sigma_k(i)<\Sigma_k(j)  \}$, $i<j$,
 the local pairwise empirical probabilities. The partition $\mathcal{P}$ being fixed, the quantity \eqref{eq:train1} is minimum when $\sigma_{\mathcal{C}}$ is a Kemeny median of $\widehat{P}_{\mathcal{C}}$ for all $\mathcal{C}\in \mathcal{P}$. It is then equal to
\begin{equation}\label{eq:crit_exp1}
\min_{s\in \mathcal{S}_{\mathcal{P}}}\widehat{L}_N(s)=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})L^*_{\widehat{P}_{\mathcal{C}}}.
\end{equation}
Except in the case where the intra-cell empirical distributions $\widehat{P}_{\mathcal{C}}$'s are all stochastically transitive, computing \eqref{eq:crit_exp1} at each recursion of the algorithm can be very expensive, since it involves the computation of a Kemeny median within each cell $\mathcal{C}$. We propose to measure instead the accuracy of the current partition by the quantity
\begin{equation}\label{eq:crit_exp2}
\widehat{\gamma}_{\mathcal{P}}=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}}}, \text{ where } \gamma_{\widehat{P}_{\mathcal{C}}}=\frac{1}{2}\sum_{i<j}\widehat{p}_{i,j}(\mathcal{C})\left(1-\widehat{p}_{i,j}(\mathcal{C})  \right) \text{ for all } \mathcal{C}\in \mathcal{P},
\end{equation}
which satisfies the double inequality
\begin{equation}\label{eq:double}
\widehat{\gamma}_{\mathcal{P}}\leq \min_{s\in \mathcal{S}_{\mathcal{P}}}\widehat{L}_N(s)\leq 2\widehat{\gamma}_{\mathcal{P}}.
\end{equation}
As shown above, the local variability measure we consider can be connected to the local ranking median regression risk and leads to exactly the same node impurity measure as in the tree induction method proposed in \cite{YWL10}. The algorithm described below differs from it in the method we use to compute the local predictions. The impurity of a cell $\mathcal{C}$ is thus measured by
$\gamma_{\widehat{P}_{\mathcal{C}}}$ and
a ranking median regression tree of maximal depth $J\geq 0$ is grown as follows. One starts from the root node $\mathcal{C}_{0,0}=\mathcal{X}$. 
%and considers as initial median ranking regression rule $s_0$ any empirical ranking median $\sigma_{0,0}$ of the observations $\Sigma_1,\; \ldots,\; \Sigma_N$. 
At depth level $0\leq j <J$, any cell $\mathcal{C}_{j,k}$, $0 \leq k <2^j$ shall be split into two (disjoint) subsets $\mathcal{C}_{j+1,2k}$ and $\mathcal{C}_{j+1,2k+1}$, respectively identified as the left and right children of the interior leaf $(j,k)$ of the ranking median regression tree, according to the following \textit{splitting rule}.

\noindent {\bf Splitting rule.} For any candidate left child $\mathcal{C}\subset \mathcal{C}_{j,k}$, picked in a class $\mathcal{G}$ of 'admissible' subsets (\textit{e.g.} axis perpendicular splits), the relevance of the split $\mathcal{C}_{j,k}=\mathcal{C}\cup (\mathcal{C}_{j,k}\setminus\mathcal{C})$ is naturally evaluated through the quantity:
\begin{equation}
\Lambda_{j,k}(\mathcal{C})\overset{def}{=}
\widehat{\mu}_N(\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}}}+\widehat{\mu}_N(\mathcal{C}_{j,k}\setminus\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}_{j,k}\setminus\mathcal{C}}}.
%=\widehat{\gamma}(\mathcal{C}) + \widehat{\gamma}(\mathcal{C}_{j,k}\setminus \mathcal{C}).
\end{equation}
Finding the best split thus consists in computing a solution $\mathcal{C}_{j+1,2k}$ of the optimization problem
\begin{equation}\label{eq:min_pb}
\min_{\mathcal{C}\in  \mathcal{G},\; \mathcal{C}\subset \mathcal{C}_{j,k}}\Lambda_{j,k}(\mathcal{C}),
\end{equation}
which can be solved very efficiently, in a greedy fashion, when considering axis perpendicular splits for instance.

\noindent {\bf Local medians.} The consensus ranking regression tree is grown until depth $J$ and on each terminal leave $\mathcal{C}_{J,l}$, $0\leq l<2^J$, one computes the local Kemeny median estimate by means of the best strictly stochastically transitive approximation method investigated in subsection \ref{subsec:consensus}
\begin{equation}
\sigma^*_{J,l}\overset{def}{=}  \widetilde{\sigma}^*_{\widehat{P}_{\mathcal{C}_{J,l}}}.
\end{equation}


\begin{table}
	%\noindent\makebox[\textwidth]{
	\centering
	\resizebox{\columnwidth}{!}{%
		\begin{tabular}{|c |ccc | ccc |ccc |} 
			\hline
			\multirow{2}{0.5cm}{$D_i$}&\multicolumn{3}{ c |}{Setting 1}& \multicolumn{3}{ c |}{Setting 2} & \multicolumn{3}{| c |}{Setting 3}\\
			&n=3 & n=5 & n=8 & n=3 & n=5 & n=8 &n=3 & n=5 & n=8 \\
			\hline
			\multirow{3}{0.5cm}{$D_0$}& 0.0698* & 0.1290* & 0.2670* & 0.0173* & 0.0405* & 0.110* & 0.0112* & 0.0372* & 0.0862*\\ 
			& 0.0473**   & 0.136** & 0.324** & 0.0568** & 0.145** &0.2695**& 0.099** & 0.1331** & 0.2188**\\ 
			& (0.578)   & (1.147) & (2.347) & (0.596) & (1.475) &(3.223)& (0.5012) & (1.104) & (2.332)\\ 
			\hline
			\multirow{3}{0.5cm}{$D_1$}& 0.3475
			* & 0.569* & 0.9405
			* & 0.306* & 0.494* & 0.784* & 0.289* & 0.457* & 0.668*\\ 
			& 0.307** & 0.529** & 0.921** &  0.308** & 0.536** &0.862**& 0.3374** & 0.5714** & 0.8544**\\ 
			& (0.719) & (1.349) & (2.606) & (0.727) & (1.634) &(3.424)& (0.5254) & (1.138) & (2.287)\\ 
			\hline
			\multirow{3}{0.5cm}{$D_2$}& 0.8656* & 1.522* & 2.503* & 0.8305
			* & 1.447
			* & 2.359* & 0.8105* & 1.437* & 2.189*\\ 
			& 0.7228** & 1.322** & 2.226**& 0.723** & 1.3305** & 2.163**& 0.7312** & 1.3237** & 2.252**\\ 
			& (0.981) & (1.865) & (3.443) & (1.014) & (2.0945) &(4.086)& (0.8504) & (1.709) & (3.005)\\  
			\hline
		\end{tabular}
	}
	\caption{Empirical risk averaged on 50 trials on simulated data.}
	\label{table:simulated_data}
	
\end{table}


\noindent \textbf{Experiments.} We generated datasets of full rankings on $n$ items according to two explanatory variables, varying the number of items ($n=3,5,8$) and the nature of the features: in Setting 1, both features are numerical; in Setting 2, one is numerical and the other categorical, in Setting 3, both are categorical. For a fixed setting, a partition $\mathcal{P}$ of $\mathcal{X}$ composed of $K$ cells $\mathcal{C}_1, \dots, \mathcal{C}_K$ is fixed. In each trial, $K$ permutations $\sigma_1, \dots, \sigma_K$ (which can be arbitrarily close) are  generated, as well as three datasets of $N$ samples, where on each cell $\mathcal{C}_k$: the first one (denoted $D_0$) is constant (all samples are equal to $\sigma_k$), and the two others (denoted $D_1$ and $D_2$)  are noisy versions of the first one, where the samples follow a Mallows distribution centered on $\sigma_k$ with dispersion parameter $\phi=2$ and $\phi=1$ respectively. % We recall that the greater the dispersion parameter $\phi$, the spikiest the distribution (and closest to piecewise constant). 
We choose $K$=6 and $N$=1000.
%(see \cite{Mallows57}) 
%In each trial, the dataset is divided into a training set ($70\%$) and a test set ($30\%$). Concerning the CRIT algorithm, since the true partition is known and is of depth 3, the maximum depth is set to 3 and the minimum size in a leaf is set to the number of samples in the training set divided by 10. For the k-NN algorithm, the number of neighbors $k$ is fixed to 5. 
The baseline model to which we compare our algorithms is the following: on the train set, we fit a K-means (with $K$=6), train a Plackett-Luce model on each cluster and assign the mode of this learnt distribution as the center ranking of the cluster.
%For each configuration (number of items, characteristics of features and distribution of the dataset), the empirical risk  $\widehat{\mathcal{R}}_N(s)$ is averaged on 50 repetitions of the experiment. 
Results of the k-NN algorithm (indicated with a star *), of the CRIT algorithm (indicated with two stars **) and of the baseline model (between parenthesis) are provided Table~\ref{table:simulated_data}. They show that the methods we develop recover well the underlying partition of the data.%, insofar as the underlying distribution can be well approximated by a piecewise constant function ($\phi \ge 2$ for instance in our simulations).

\section{Conclusions and Perspectives}

The contribution of this article is twofold. The problem of learning to predict preferences, expressed in the form of a permutation, in a supervised setting is formulated and investigated in a rigorous probabilistic framework (optimal elements, learning rate bounds, bias analysis), extending that recently developped for statistical Kemeny ranking aggregation in \cite{CKS17}. Based on this formulation,
%and on the statistical guarantees we established here for the efficient procedure introduced in \citet{JLYY10} to approximate Kemeny medians
it is also shown that predictive methods based on the concept of local Kemeny consensus, variants of nearest-neighbor and tree-induction methods namely, are well-suited for this learning task. This is justified by approximation theoretic arguments and algorithmic simplicity/efficiency both at the same time and illustrated by numerical experiments. Whereas the ranking median regression problem is motivated by many applications in our era of recommender systems and personalized customer services, the output variable may take the form of an \textit{incomplete ranking} rather than a full ranking in many situations. Extension of our results to this more general framework, extended to incomplete rankings, will be the subject of further research.

\bibliographystyle{plain}

\bibliography{biblio-thesis}
\end{document}
