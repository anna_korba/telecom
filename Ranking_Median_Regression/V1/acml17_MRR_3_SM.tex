%\documentclass[wcp,gray]{jmlr} % test grayscale version
\documentclass[wcp]{jmlr}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{amsmath}
%\usepackage{epsfig}
%\usepackage{geometry}
\usepackage{stmaryrd}
\usepackage[noadjust]{cite}
\usepackage{natbib}
\usepackage{enumerate}
\usepackage{multirow}
\usepackage{caption}

% ENVIRONMENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Notations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\DeclareMathOperator*{\argmin}{argmin}
\newcommand{\n}{\llbracket n \rrbracket}
\newcommand{\DN}{\mathcal{D}_{N}}
\newcommand{\Sn}{\mathfrak{S}_n}
\def\e{\mathbf e}
\def\i{\mathbf i}
\def\j{\mathbf j}
% The following packages will be automatically loaded:
% amsmath, amssymb, natbib, graphicx, url, algorithm2e

%\usepackage{rotating}% for sideways figures and tables
\usepackage{longtable}% for long tables

% The booktabs package is used by this sample document
% (it provides \toprule, \midrule and \bottomrule).
% Remove the next line if you don't require it.
\usepackage{booktabs}
% The siunitx package is used by this sample document
% to align numbers in a column by their decimal point.
% Remove the next line if you don't require it.
%\usepackage[load-configurations=version-1]{siunitx} % newer version
%\usepackage{siunitx}
%\usepackage{natbib}

\newtheorem{assumption}{Assumption}

% The following command is just for this sample document:
\newcommand{\cs}[1]{\texttt{\char`\\#1}}
%\DeclareMathOperator{\argmin}{argmin}
\def\bb{\mathbb}
\def\mb{\mathbf}
\def\point{\,\cdot\,}
\def\P{\mathbb{P}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\roc}{{\rm  ROC}}
\newcommand{\mv}{{\rm  MV}}
\newcommand{\cpauc}{{\rm  CPAUC}}
\newcommand{\auc}{{\rm  AUC}}
\newcommand{\Prob}[1]{\mathbb{P}\left\{ #1 \right\} }
\def\argmax{\mathop{\rm arg\, max}}
\def\argmin{\mathop{\rm arg\, min}}
\def\C{{\cal C}}
\def\B{{\cal B}}
\def\Norm{{\cal N}}
\def\A{{\cal A}}
\def\N{{\cal N}}
\def\F{{\cal F}}
\def\S{{\cal S}}
\def\L{{\cal L}}
\def\K{{\cal K}}
\def\P{{\cal P}}
\def\X{{\cal X}}
\def\I{{\mathbb I}}


\jmlrvolume{80}
\jmlryear{2017}
\jmlrworkshop{ACML 2017}

\title[Ranking Median Regression]{Ranking Median Regression:\\ Learning to Order through Local Consensus}

 % Use \Name{Author Name} to specify the name.
 % If the surname contains spaces, enclose the surname
 % in braces, e.g. \Name{John {Smith Jones}} similarly
 % if the name has a "von" part, e.g \Name{Jane {de Winter}}.
 % If the first letter in the forenames is a diacritic
 % enclose the diacritic in braces, e.g. \Name{{\'E}louise Smith}

 % Two authors with the same address
 % \author{\Name{Author Name1} \Email{abc@sample.com}\and
 %  \Name{Author Name2} \Email{xyz@sample.com}\\
 %  \addr Address}

 % Three or more authors with the same address:
 % \author{\Name{Author Name1} \Email{an1@sample.com}\\
 %  \Name{Author Name2} \Email{an2@sample.com}\\
 %  \Name{Author Name3} \Email{an3@sample.com}\\
 %  \Name{Author Name4} \Email{an4@sample.com}\\
 %  \Name{Author Name5} \Email{an5@sample.com}\\
 %  \Name{Author Name6} \Email{an6@sample.com}\\
 %  \Name{Author Name7} \Email{an7@sample.com}\\
 %  \Name{Author Name8} \Email{an8@sample.com}\\
 %  \Name{Author Name9} \Email{an9@sample.com}\\
 %  \Name{Author Name10} \Email{an10@sample.com}\\
 %  \Name{Author Name11} \Email{an11@sample.com}\\
 %  \Name{Author Name12} \Email{an12@sample.com}\\
 %  \Name{Author Name13} \Email{an13@sample.com}\\
 %  \Name{Author Name14} \Email{an14@sample.com}\\
 %  \addr Address}


 % Authors with different addresses:
  %\author{\Name{Stephan Cl\'emen\c{c}on} \Email{stephan.clemencon@telecom-paristech.fr }\\
  %\Name{Anna Korba} \Email{anna.korba@telecom-paristech.fr }\\
  %\addr LTCI, T\'el\'ecom ParisTech, Universit\'e Paris-Saclay\\
  %Paris, France\\
  %\AND
  %\Name{Eric Sibony} \Email{eric.sibony@gmail.com}\\
  %\addr Shift Technologies\\
%Paris, France
% }

%\editors{List of editors' names}

\begin{document}

\maketitle

\begin{abstract}
This article is devoted to the problem of predicting the value taken by a random permutation $\Sigma$, describing the preferences of an individual over a set of numbered items $\{1,\; \ldots,\; n\}$ say, based on the observation of an input/explanatory r.v. $X$ (\textit{e.g.} characteristics of the individual), when error is measured by the Kendall $\tau$ distance. In the probabilistic formulation of the 'Learning to Order' problem we propose, which extends the framework for statistical Kemeny ranking aggregation developped in \citet{CKS17}, this boils down to recovering conditional Kemeny medians of $\Sigma$ given $X$ from i.i.d. training examples $(X_1, \Sigma_1),\; \ldots,\; (X_N, \Sigma_N)$. For this reason, this statistical learning problem is referred to as \textit{ranking median regression} here. Our contribution is twofold. We first propose a probabilistic theory of ranking median regression and investigate the performance of empirical risk minimizers in this context. Next we introduce the concept of local consensus/median, in order to derive a local learning method extending that introduced in \citet{YWL10}, we call the {\sc CRIT} algorithm, based on recursive partitioning of the input space and producing tree-structured piecewise constant Kemeny median regression estimates. Accuracy of such ranking rules is proved under a specific smoothness assumption for $\Sigma$'s conditional distribution given $X$. Beyond its interpretability and computational tractability, the major advantage of the approach we develop lies in its close connection with the widely studied Kemeny aggregation problem. From an algorithmic perspective, this permits to build predictive rules for ranking median regression by implementing techniques for Kemeny median computations at a local level, on regions where the preferences expressed exhibit low variability (measured by means of the Kendall $\tau$ distance).  The results of various numerical experiments are displayed for illustration purpose.
 
\end{abstract}
\begin{keywords}
Consensus ranking, Kemeny median, Kendall tau, local learning, predictive learning, ranking aggregation
\end{keywords}

\input{sections/introduction}
\input{sections/preliminary}
\input{sections/trees}
\input{sections/experiments}
\input{sections/conclusion}


%\bibliographystyle{plain}
\bibliographystyle{apa}
\bibliography{biblio-thesis}

%\appendix

%\input{sections/proofs}
%\input{sections/supplementary}

\end{document}