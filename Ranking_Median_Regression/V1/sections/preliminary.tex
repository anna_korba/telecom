
\section{Background and Preliminaries}\label{sec:background}
As a first go, we start with recalling the metric approach to consensus ranking and give next a rigorous statistical formulation of  ranking aggregation. We also establish statistical guarantees for the generalization capacity of an alternative to the empirical Kemeny median technique studied in \citet{CKS17}, which can be much more easily computed in certain situations and which the algorithm we propose in the subsequent section highly relies on.  From the angle embraced in this paper, \textit{ranking median regression} is then viewed as an extension of statistical ranking aggregation. Here and throughout, the indicator function of any event $\mathcal{E}$ is denoted by $\mathbb{I}\{ \mathcal{E} \}$, the Dirac mass at any point $a$ by $\delta_a$ and the cardinality of any finite set $E$ by $\# E$.
%, and we set $sgn(x)=2\mathbb{I}\{x\geq 0 \}-1$ for all $x\in\mathbb{R}$. 
Let $n\geq 1$, the set of permutations of $\n =\{1,\; \ldots,\; n  \}$ is denoted by $\mathfrak{S}_n$.



\subsection{Consensus Ranking: Probabilistic Framework and Statistical Setup}\label{subsec:consensus}
Throughout the article, a ranking on an ensemble of items indexed by $\n$ is seen as the permutation $\sigma\in \Sn$ that maps any item $i$ to its rank $\sigma(i)$. Given a collection of $N\geq 1$ rankings $\sigma_{1},\; \ldots,\; \sigma_{N}$, the goal of consensus ranking, also referred to as ranking aggregation sometimes, is to find $\sigma^*\in \Sn$ that best summarizes it. A popular way of tackling this problem, the metric-based consensus approach, consists in solving:
\begin{equation}
	\label{eq:ranking_aggregation}
	\min_{\sigma\in \mathfrak{S}_n}\sum_{i=1}^N d(\sigma,\sigma_{i}),
\end{equation}
where $d(.,\; .)$ is a certain metric on $\Sn$. As the set $\Sn$ is of finite cardinality, though not necessarily unique, such a barycentric permutation, called \textit{consensus/median ranking}, always exists.
In Kemeny ranking aggregation, the version of this problem the most widely documented in the literature, one considers the number of pairwise disagreements as metric, namely the Kendall's $\tau$ distance, see \citet{Kemeny59}: $\forall (\sigma,\sigma')\in \mathfrak{S}_n^2$,
\begin{equation}\label{eq:Kendall_tau}
d_{\tau}(\sigma,\sigma')=\sum_{i<j}\mathbb{I}\{(\sigma(i)-\sigma(j)) (\sigma'(i)-\sigma'(j))<0\}.
\end{equation}
%Such a consensus has many interesting properties, but is NP-hard to compute. Various algorithms have been proposed in the literature to compute acceptably good solutions in a reasonable amount of time, their description is beyond the scope of the paper, see for example \cite{AM12} and the references therein. 
The problem \eqref{eq:ranking_aggregation} can be viewed as a $M$-estimation problem in the probabilistic framework stipulating that  the collection of rankings to be aggregated/summarized is composed of $N\geq 1$ independent copies $\Sigma_1,\; \ldots,\; \Sigma_N$ of a generic r.v. $\Sigma$ defined on a probability space $(\Omega,\; \mathcal{F},\; \mathbb{P})$ drawn from an unknown probability distribution $P$ on $\mathfrak{S}_n$ (\textit{i.e.} $P(\sigma)=\mathbb{P}\{ \Sigma=\sigma \}$ for any $\sigma\in \mathfrak{S}_n$). Just like a median of a real valued r.v. $Z$ is any scalar closest to $Z$ in the $L_1$ sense, a (true) median of distribution $P$ w.r.t. a certain metric $d$ on $\mathfrak{S}_n$ is any solution of the minimization problem:
\begin{equation}\label{eq:median_pb}
\min_{\sigma \in \mathfrak{S}_n}L_P(\sigma),
\end{equation}
where $L_P(\sigma)=\mathbb{E}_{\Sigma \sim P}[d(\Sigma,\sigma)  ]
$ denotes the expected distance between any permutation $\sigma$ and $\Sigma$. In this framework, statistical ranking aggregation consists in recovering a solution $\sigma^*$ of this minimization problem, plus an estimate of this minimum $L^*_P=L_P(\sigma^*)$, as accurate as possible, based on the observations $\Sigma_1,\; \ldots,\; \Sigma_N$. A median permutation $\sigma^*$ can be interpreted as a central value for distribution $P$, while the quantity $L^*_P$ may be viewed as a dispersion measure. 
The minimization problem \eqref{eq:median_pb} has always a solution since the cardinality of $\mathfrak{S}_n$ is finite (however exploding with $n$) but can be multimodal.
However, the functional $L_P(.)$ is unknown in practice, just like distribution $P$. Suppose that we would like to avoid rigid parametric assumptions on $P$ such as those stipulated by the Mallows model, see \citet{Mallows57}, and only have access to the dataset $\{\Sigma_1,\; \ldots,\; \Sigma_N  \}$ to find a reasonable approximant of a median. Following the Empirical Risk Minimization (ERM) paradigm \citep[see \textit{e.g.}][]{Vapnik}, one substitutes in \eqref{eq:median_pb} the quantity $L(\sigma)$ with its statistical version
\begin{equation}\label{eq:emp_risk}
\widehat{L}_N(\sigma)=\frac{1}{N}\sum_{i=1}^Nd(\Sigma_i,\sigma)=L_{\widehat{P}_N}(\sigma),
\end{equation}
where $\widehat{P}_N=(1/N)\sum_{i=1}^N\delta_{\Sigma_i}$ denotes the empirical measure.
The performance of empirical consensuses, namely solutions $\widehat{\sigma}_N$ of 
 $\min_{\sigma\in \mathfrak{S}_n}\widehat{L}_N(\sigma)$,
 has been investigated in \citet{CKS17}.  Precisely, rate bounds of order $O_{\mathbb{P}}(1/\sqrt{N})$ for the excess of risk $L_P(\widehat{\sigma}_N)-L^*_P$ in probability/expectation have been established and proved to be sharp in the minimax sense, when $d$ is the Kendall's $\tau$ distance. Whereas problem \eqref{eq:ranking_aggregation} is NP-hard in general (see \citet{Hudry08} for instance), in the Kendall's $\tau$ case, exact solutions, referred to as \textit{Kemeny medians}, can be explicited when the pairwise probabilities $p_{i,j}=\mathbb{P}\{  \Sigma(i)<\Sigma(j)\}$, $1\leq i\neq j\leq n$, fulfill the following property, referred to as \textit{stochastic transitivity}.
 \begin{definition}\label{def:stoch_trans}
 The probability distribution $P$ on $\mathfrak{S}_n$ is stochastically transitive iff
 $$
\forall (i,j,k)\in \n^3:\;\;  p_{i,j}\geq 1/2 \text{ and } p_{j,k}\geq 1/2 \; \Rightarrow\; p_{i,k}\geq 1/2.
 $$
 If, in addition, $p_{i,j}\neq 1/2$ for all $i<j$, $P$ is said to be strictly stochastically transitive.  
 \end{definition} 
 When stochastic transitivity holds true, the set of Kemeny medians (see Theorem 5 in \citet{CKS17}) is the (non empty) set 
 \begin{equation}\label{eq:opt_sol}
 \{\sigma\in \mathfrak{S}_n:\; (p_{i,j}-1/2)(\sigma(j)-\sigma(i ))>0 \text{ for all } i<j \text{ s.t. } p_{i,j}\neq 1/2  \},
 \end{equation}
 the minimum is given by
 \begin{equation}\label{eq:inf}
 L^*_P=\sum_{i<j}\min\{p_{i,j},1-p_{i,j}  \}=\sum_{i<j}\{  1/2-\vert  p_{i,j}-1/2\vert\}
 \end{equation}
  and, for any $\sigma\in \mathfrak{S}_n$,
 $
 L_P(\sigma)-L^*_P=2\sum_{i<j}\left\vert p_{i,j}-1/2 \right\vert\cdot \mathbb{I}\{ (\sigma(i)-\sigma(j))\left(p_{i,j}-1/2\right)<0\}.
 $
 If a strict version of stochastic transitivity is fulfilled, the Kemeny median $\sigma^*_P$ is unique and given by the Copeland ranking:
 \begin{equation}\label{eq:sol_SST}
 \sigma^*_P(i)=1+\sum_{k\neq i}\mathbb{I}\{p_{i,k}<1/2  \} \text{ for } 1\leq i\leq n.
 \end{equation}
 %and the minimum expected Kendall $\tau$ distance is equal to 
 %\begin{equation}\label{eq:inf}
 %L^*_P=L_P(\sigma_P^*)=\sum_{i<j}\min\{p_{i,j},1-p_{i,j}  \}=\sum_{i<j}\left\{ 1/2-\left\vert  p_{i,j}-1/2\right\vert\right\}.
 %\end{equation}
 \begin{remark}\label{rk:dispersion}{\sc (Measuring dispersion)} As noticed in \citet{CKS17}, an alternative measure of dispersion is given by
  $\gamma(P)=(1/2)\mathbb{E}[ d(\Sigma,\Sigma') ]$,
  where $\Sigma'$ is an independent copy of $\Sigma$. When $P$ is not strictly stochastically transitive, the latter can be much more easily estimated than $L_P^*$, insofar as no (approximate) median computation is needed: indeed, a natural estimator is given by the $U$-statistic
 $\widehat{\gamma}_N=2/(N(N-1))\sum_{i<j}d(\Sigma_i,\Sigma_j)$. For this reason, this empirical dispersion measure will be used as a splitting criterion in the partitioning algorithm proposed in subsection \ref{subsec:algo}. Observe in addition that
  $\gamma(P)\leq L^*_P\leq 2\gamma(P)$ and, when $d=d_{\tau}$, we have $\gamma(P)=\sum_{i<j}p_{i,j}(1-p_{i,j})$.
 \end{remark}
Assuming that the underlying distribution $P$ is strictly stochastically transitive, it is shown in \citet{CKS17} that the empirical distribution $\widehat{P}_N$ is strictly stochastically transitive as well, with overwhelming probability, and that the expectation of the excess of risk decays at an exponential rate, see Proposition 14 therein. In this case, the nearly optimal solution $\sigma^*_{\widehat{P}_N}$ can be made explicit and straightforwardly computed using Eq. \eqref{eq:sol_SST} based on the empirical pairwise probabilities
$$
\widehat{p}_{i,j}=\frac{1}{N}\sum_{k=1}^N\mathbb{I}\{ \Sigma_k(i)<\Sigma_k(j)  \}, \; i<j.
$$ Otherwise, solving the NP-hard problem $\min_{\sigma \in \mathfrak{S}_n}L_{\widehat{P}_N}(\sigma)$ is required to get an empirical Kemeny median. Below, we establish statistical guarantees for the performance of a numerically feasible alternative, namely the regression-based method proposed in \citet{JLYY10}, when applied to pairwise probabilities $\widehat{p}_{i,j}$, $i<j$.
\medskip

\noindent {\bf Best strictly stochastically transitive approximation.} We denote by $\mathcal{T}$ the set of strictly stochastically transitive distributions on $\mathfrak{S}_n$ and suppose that $P\in \mathcal{T}$. If the empirical estimation $\widehat{P}_N$ of $P$ does not belong to $\mathcal{T}$, a natural strategy would consist in approximating it by a strictly stochastically transitive probability distribution $\widetilde{P}$ as accurately as possible (in a sense that is specified below) and consider the (unique) Kemeny median of the latter as an approximate median for $\widehat{P}_N$ (for $P$, respectively). It is legitimated by the result below, whose proof is given in the Supplementary Material.
\begin{lemma}\label{lem:bounds}
Let $P'$ and $P^{''}$ be two probability distributions on $\mathfrak{S}_n$. 
\begin{itemize}
\item[(i)] Suppose that $P^{''}\in \mathcal{T}$. Then, we have: 
\begin{equation}
L^*_{P'}\leq L_{P'}(\sigma^*_{P''})\leq L^*_{P'}+2\sum_{i<j}\vert p'_{i,j}-p''_{i,j} \vert ,
\end{equation}
where $p'_{i,j}=\mathbb{P}_{\Sigma \sim P'}\{  \Sigma(i)<\Sigma(j)\}$ for any $i<j$.
\item[(ii)] Suppose that $(P',P^{''})\in \mathcal{T}^2$ and set $h=\min_{i<j}\vert p''_{i,j}-1/2\vert$. Then, we have:
\begin{equation}
d_{\tau}(\sigma^*_{P'}, \sigma^*_{P''})\leq (1/h)\sum_{i<j}\vert p'_{i,j}-p''_{i,j} \vert .
\end{equation}
\end{itemize}
\end{lemma}
The issue of finding a sharp approximation of a probability distribution $P'$ by a strictly stochastically transitive distribution is connected with the \textit{feedback set} problem for directed graphs, see \citet{DETT99}. Indeed, any probability distribution $P''$ in $\mathcal{T}$ defines a directed acyclic graph (DAG) whose vertices are the objects $1,\; \ldots,\; n$ and the directed edges are defined by: $\forall i\neq j$,
\begin{equation}\label{eq:DAG}
i \leftarrow j \Leftrightarrow p_{i,j}>1/2.
\end{equation}
A feedback set (FS) is a set of edges whose reversal makes the directed graph acyclic. Whereas it is difficult in general to find a FS with specific properties (\textit{e.g.} a minimum cardinality FS), the problem of finding the best collection of pairwise probabilities $p''=(p^{''}_{i,j})_{i<j}$ defining  a DAG $G_{p''}$ (see \eqref{eq:DAG}) approximating $(p'_{i,j})_{i<j}$ in the least squares sense, \textit{i.e.} such that $\sum_{i<j}(p'_{i,j}-p^{''}_{i,j})^2$ is minimum, has been adressed in \citet{JLYY10} by means of the Hodge combinatorial theory. As shown in section 5 therein, the computation of the solution boils down to solving a $n\times n$ least-squares problem (with cost $O(n^3)$). Denoting by $\widetilde{\sigma}^*_{P'}$ the permutation defined by the $p^{''}_{i,j}$'s (namely, $\widetilde{\sigma}^*_{P'}(i)=1+\sum_{k\neq i}\mathbb{I}\{p^{''}_{i,k}<1/2  \}$ for all $i\in \n$), we deduce from Lemma \ref{lem:bounds} combined with Cauchy-Schwarz inequality that:
\begin{equation}\label{eq:bound_DAG}
L^*_{P'}\leq L_{P'}(\widetilde{\sigma}^*_{P'})\leq L^*_{P'}+\sqrt{2n(n-1)}\min_{p'':\; G_{p''} \text{  acyclic}}\left(\sum_{i<j}(p'_{i,j}-p^{''}_{i,j})^2\right)^{1/2}.
\end{equation}
When $\widehat{P}\notin \mathcal{T}$, the bound above advocates for using the permutation $\widetilde{\sigma}^*_{\widehat{P}}$ as approximate median of the underlying distribution $P$. The following theorem reveals that its excess of risk is of order $O_{\mathbb{P}}(1/\sqrt{N})$. Refer to the Supplementary Material for the technical proof.
\begin{theorem}\label{thm:approx_med}
Suppose that $P\in \mathcal{T}$. Then, for all $\delta\in (0,1)$, we have with probability at least $1-\delta$: $\forall N\geq 1$,
\begin{equation}
L_{P}(\widetilde{\sigma}^*_{\widehat{P}})-L^*_{P}\leq \left(  \frac{n(n-1)}{2}\right)^{3/2}\sqrt{\frac{\log(2n(n-1)/\delta)}{N}}.
\end{equation}
\end{theorem}

As shall be seen in the next section, this regression approach to ranking aggregation shall play a crucial role in the algorithm we propose for ranking median regression (in order to compute local medians more precisely), which statistical learning problem is formulated below as an extension of consensus ranking.


\subsection{Ranking Median Regression}\label{subsec:rmr}
We suppose now that, in addition to the ranking $\Sigma$, one observes a random vector $X$, defined on the same probability space $(\Omega,\; \mathcal{F},\; \mathbb{P})$, valued in a feature space $\mathcal{X}$ (of possibly high dimension, typically a subset of $\mathbb{R}^d$ with $d\geq 1$) and modelling some information hopefully useful to predict $\Sigma$ (or at least to recover some of its characteristics). The joint distribution of the r.v. $(\Sigma,\; X)$ is described by $(\mu,\; P_X)$, where $\mu$ denotes $X$'s marginal distribution and $P_X$ means the conditional probability distribution of $\Sigma$ given $X$: $\forall \sigma\in \mathfrak{S}_n$,
 $P_X(\sigma)=\mathbb{P}\{ \Sigma=\sigma \mid X\}$ almost-surely.
The marginal distribution of $\Sigma$ is then $P(\sigma)=\int_{\mathcal{X}}P_{x}(\sigma)\mu(x)$. 
Let $d$ be a metric on $\mathfrak{S}_n$, assuming that the quantity $d(\Sigma, \sigma)$ reflects the cost of predicting a value $\sigma$ for the ranking $\Sigma$, one can formulate the predictive problem that consists in finding a measurable mapping $s:\mathcal{X}\rightarrow \mathfrak{S}_n$ with minimum  prediction error:
 \begin{equation}\label{eq:reg_risk}
 \mathcal{R}(s)=\mathbb{E}_{X\sim \mu}[\mathbb{E}_{\Sigma\sim P_X}\left[ d\left(s(X),\Sigma   \right) \right]]=\mathbb{E}_{X\sim \mu}\left[ L_{P_X}(\Sigma, s(X)) \right].
 \end{equation}
 We denote by $\mathcal{S}$ the collection of all measurable mappings $s:\mathcal{X}\rightarrow \mathfrak{S}_n$, its elements will be referred to as \textit{predictive ranking rules}.
%Using a straightforward conditioning argument, one may write $\mathcal{R}(s)=\mathbb{E}_{X\sim \mu}\left[ L_{P_X}(\Sigma, s(X)) \right]$ for any $s\in \mathcal{S}$.
 As the minimum of the quantity inside the expectation is attained as soon as $s(X)$ is a median for $P_X$, the set of optimal predictive rules can be easily made explicit, as shown by the proposition below.
\begin{proposition}{\sc (Optimal elements)} The set $\mathcal{S}^*$ of minimizers of the risk \eqref{eq:reg_risk} is composed of all measurable mappings $s^*:\mathcal{X}\rightarrow \mathfrak{S}_n$ such that $s^*(X)\in \mathcal{M}_X$ with probability one, denoting by $\mathcal{M}_x$ the set of median rankings related to distribution $P_x$, $x\in \mathcal{X}$. 
\end{proposition}
For this reason, the predictive problem formulated above it is referred to as \textit{ranking median regression} and its solutions as \textit{conditional median rankings}. It extends the ranking aggregation problem in the sense that $\mathcal{S}^*$ coincides with the set of medians of the marginal distribution $P$ when $\Sigma$ is independent from $X$.
  Equipped with the notations above, notice incidentally that the minimum prediction error can be written as $\mathcal{R}^*=\mathbb{E}_{X\sim \mu}[L^*_{P_X}]$ and that the risk excess of any $s\in \mathcal{S}$ can be controlled as follows:
  $$
  \mathcal{R}(s)-\mathcal{R}^*\leq \mathbb{E}\left[ d\left(s(X),\; s^*(X)  \right) \right],
  $$ 
for any $s^*\in \mathcal{S}^*$. We assume from now on that $d=d_{\tau}$. If $P_X\in\mathcal{T}$ with probability one, we almost-surely have $s^*(X)=\sigma^*_{P_X}$ and 
  $$
  \mathcal{R}^*=\sum_{i<j}\left\{  1/2-\int_{x\in\mathcal{X}}\left\vert p_{i,j}(x)-1/2 \right\vert \mu(dx)\right\},
  $$
  where $p_{i,j}(x)=\mathbb{P}\{\Sigma(i)<\Sigma(j) \mid X=x \}$ for all $i<j$, $x\in \mathcal{X}$. Observe also that in this case, the excess of risk is given by: $\forall s\in \mathcal{S}$,
  \begin{equation}\label{eq:excess_risk}
\mathcal{R}(s)-\mathcal{R}^*=\sum_{i<j}\int_{x\in \mathcal{X}}\vert p_{i,j}(x)-1/2 \vert\mathbb{I}\{\left(s(x)(j)-s(x)(i)\right) \left(p_{i,j}(x)-1/2\right)<0 \}\mu(dx).
  \end{equation}
 \medskip
 
 \noindent {\bf Statistical setting.} We assume that we observe $(X_1,\; \Sigma_1)\; \ldots,\; (X_1,\; \Sigma_N)$, $N\geq 1$ i.i.d. copies of the pair $(X,\; \Sigma)$ and, based on these training data, the objective is to build a predictive ranking rule $s$ that nearly minimizes $\mathcal{R}(s)$ over the class $\mathcal{S}$ of measurable mappings $s:\mathcal{X}\rightarrow \mathfrak{S}_n$.
 Of course, the Empirical Risk Minimization (ERM) paradigm encourages to consider solutions of the empirical minimization problem:
 \begin{equation}\label{eq:ERM}
 \min_{s\in \mathcal{S}_0}\widehat{\mathcal{R}}_N(s),
 \end{equation}
 where $\mathcal{S}_0$ is a subset of $\mathcal{S}$, supposed to be rich enough for containing approximate versions of elements of $\mathcal{S}^*$ (\textit{i.e.} so that $\inf_{s\in \mathcal{S}_0}\mathcal{R}(s)- \mathcal{R}^*$ is 'small') and ideally appropriate for continuous or greedy optimization, and
 \begin{equation}\label{eq:emp_reg_risk}
 \widehat{\mathcal{R}}_N(s)=\frac{1}{N}\sum_{i=1}^N d_{\tau}(s(X_i),\;  \Sigma_i)
 \end{equation}
 is a statistical version of \eqref{eq:reg_risk} based on $(X_i,\Sigma_i)$'s. 
 Extending those established by \citet{CKS17} in the context of ranking aggregation, statistical results describing the generalization capacity of minimizers of \eqref{eq:emp_reg_risk} can be established under classic complexity assumptions for the class $\mathcal{S}_0$, such as the following one (observe incidentally that it is fulfilled by the class of ranking rules output by the algorithm described in section \ref{sec:main}, \textit{cf} Remark \ref{rk:perp_split}).
 
 \begin{assumption}\label{hyp:complex}
 For all $i<j$, the collection of sets
 $$
 \left\{ \left\{x\in \mathcal{X}:\; s(x)(i)-s(x)(j)>0   \right\}:\; s\in \mathcal{S}_0 \right\}\cup \left\{\left\{x\in \mathcal{X}:\; s(x)(i)-s(x)(j)<0   \right\}:\; s\in \mathcal{S}_0\right\}
 $$
 is of finite {\sc VC} dimension $V<\infty$.
 \end{assumption}
 
 \begin{proposition}\label{prop:upper_bound}
Suppose that the class $\mathcal{S}_0$ fulfills Assumption \ref{hyp:complex}. Let $\widehat{s}_N$ be any minimizer of the empirical risk \eqref{eq:emp_reg_risk} over $\mathcal{S}_0$. For any $\delta\in (0,1)$, we have with probability at least $1-\delta$: $\forall N\geq 1$,
 \begin{equation}\label{eq:ERM_bound_gen}
 \mathcal{R}(\widehat{s}_N)-\mathcal{R}^*\leq C\sqrt{\frac{V\log(n(n-1) /(2\delta))}{N}}+\left\{  \mathcal{R}^*-\inf_{s\in \mathcal{S}_0}\mathcal{R}(s) \right\},
 \end{equation}
 where $C<+\infty$ is a universal constant.
 \end{proposition}
Refer to the Supplementary Material for the technical proof. It is also established there that the rate bound $O_{\mathbb{P}}(1/\sqrt{N})$ is sharp in the minimax sense and that, in certain situations (\textit{i.e.} under Assumption \ref{hyp:margin} introduced in subsection \ref{subsec:piece} namely), faster learning rates of order $O_{\mathbb{P}}(1/N)$ can be attained by empirical risk minimizers. 
 Regarding the minimization problem \eqref{eq:ERM}, attention should be paid to the fact that, in contrast to usual (median/quantile) regression, the set $\mathcal{S}$ of predictive ranking rules is not a vector space, which makes the design of practical optimization strategies challenging and the implementation of certain methods, based on (forward stagewise) additive modelling for instance, unfeasible.
 \medskip

\noindent {\bf State of the Art.} If $\mu$ is continuous (the $X_i$'s are pairwise distinct), it is always possible to find $s\in \mathcal{S}$ such that $\widehat{R}_N(s)=0$ and model selection/regularization issues (\textit{i.e.} choosing an appropriate class $\mathcal{S}_0$) are crucial. In contrast, if $X$ takes discrete values only (corresponding to possible requests in a search engine for instance, like in the usual 'learning to order' setting), in the set $\{1,\; \ldots,\; K \}$ with $K\geq 1$ say, the problem \eqref{eq:ERM} boils down to solving \textit{independently} $K$ empirical ranking median problems. However, $K$ may be large and it may be relevant to use some regularization procedure accounting for the possible amount of similarity shared by certain requests, adding some penalization term to \eqref{eq:emp_reg_risk}. The approach to ranking median regression we develop in this paper, close in spirit to adaptive approximation methods, extends the one introduced in \citet{YWL10} (see also Chapter 10 in \citet{AY14}). Whereas ranking aggregation methods (such as that analyzed in Theorem \ref{thm:approx_med} from a statistical learning perspective) applied to the $\Sigma_i$'s would ignore the information carried by the $X_i$'s for prediction purpose, the learning algorithm we describe in the next section is inspired by the {\sc CART} algorithm, see \citet{cart84}, and builds piecewise constant ranking rules (the complexity of the related classes $\mathcal{S}_0$ can be naturally described by the number of constant pieces involved in the predictive rules), relying on the notion of \textit{local consensus}, by means of a tree-structured adaptive partitioning scheme.



\begin{remark}{\sc (Generative models)} Extending classical approaches for consensus ranking, stipulating a rigid 'parametric' model $P_{\theta}$ for the distribution on $\mathfrak{S}_n$ (see \textit{e.g.} \citet{Mallows57} or \citet{fligner1986distance}), several authors proposed to model the dependence of the parameter $\theta$ w.r.t. the covariate $X$ and rely next on MLE or Bayesian techniques to compute a predictive rule. One may refer to \citet{rendle2009bpr} and \citet{lu2015individualized}.
\end{remark}