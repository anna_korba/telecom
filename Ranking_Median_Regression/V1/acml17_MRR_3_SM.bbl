\begin{thebibliography}{17}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Alvo and Yu(2014)]{AY14}
M.~Alvo and P.~L.~H. Yu.
\newblock \emph{Statistical Methods for Ranking Data}.
\newblock Springer, 2014.

\bibitem[Binev et~al.(2005)Binev, Cohen, Dahmen, DeVore, and Temlyakov]{Binev}
P.~Binev, A.~Cohen, W.~Dahmen, R.~DeVore, and V.~Temlyakov.
\newblock Universal algorithms for learning theory part i: piecewise constant
  functions.
\newblock 2005.

\bibitem[Breiman(1996)]{Br96}
L.~Breiman.
\newblock Bagging predictors.
\newblock \emph{Machine Learning}, 26:\penalty0 123--140, 1996.

\bibitem[Breiman(2001)]{Br01}
L.~Breiman.
\newblock Random forests.
\newblock \emph{Machine Learning}, 45\penalty0 (1):\penalty0 5--32, 2001.

\bibitem[Breiman et~al.(1984)Breiman, Friedman, Olshen, and Stone]{cart84}
L.~Breiman, J.~Friedman, R.~Olshen, and C.~Stone.
\newblock \emph{{Classification and Regression Trees}}.
\newblock Wadsworth and Brooks, 1984.

\bibitem[Di~Battista et~al.(1999)Di~Battista, Eades, Tamasia, and
  Tollis]{DETT99}
G.~Di~Battista, P.~Eades, R~Tamasia, and I.G. Tollis.
\newblock \emph{Graph Drawing}.
\newblock Prentice Hall, 1999.

\bibitem[Fligner and Verducci(1986)]{fligner1986distance}
M.~A Fligner and J.~S Verducci.
\newblock Distance based ranking models.
\newblock \emph{Journal of the Royal Statistical Society. Series B
  (Methodological)}, pages 359--369, 1986.

\bibitem[Friedman(1997)]{friedman}
J.~Friedman.
\newblock Local learning based on recursive covering.
\newblock \emph{Computing Science and Statistics}, pages 123--140, 1997.

\bibitem[Hudry(2008)]{Hudry08}
O.~Hudry.
\newblock {NP}-hardness results for the aggregation of linear orders into
  median orders.
\newblock \emph{Ann. Oper. Res.}, 163:\penalty0 63--88, 2008.

\bibitem[Jiang et~al.(2010)Jiang, Lim, Yao, and Ye]{JLYY10}
X.~Jiang, L.H. Lim, Y.~Yao, and Y.~Ye.
\newblock Statistical ranking and combinatorial {H}odge theory.
\newblock \emph{Math Program. Ser. B}, 2010.

\bibitem[Kemeny(1959)]{Kemeny59}
J.~G. Kemeny.
\newblock Mathematics without numbers.
\newblock \emph{Daedalus}, 88:\penalty0 571--591, 1959.

\bibitem[Korba et~al.(2017)Korba, Cl\'emen\c{c}on, and Sibony]{CKS17}
A.~Korba, S.~Cl\'emen\c{c}on, and E.~Sibony.
\newblock A learning theory of ranking aggregation.
\newblock In \emph{Proceeding of AISTATS 2017}, 2017.

\bibitem[Lu and Negahban(2015)]{lu2015individualized}
Y.~Lu and S.~N. Negahban.
\newblock Individualized rank aggregation using nuclear norm regularization.
\newblock In \emph{Communication, Control, and Computing (Allerton), 2015 53rd
  Annual Allerton Conference on}, pages 1473--1479. IEEE, 2015.

\bibitem[Mallows(1957)]{Mallows57}
C.~L. Mallows.
\newblock Non-null ranking models.
\newblock \emph{Biometrika}, 44\penalty0 (1-2):\penalty0 114--130, 1957.

\bibitem[Rendle et~al.(2009)Rendle, Freudenthaler, Gantner, and
  Schmidt-Thieme]{rendle2009bpr}
S.~Rendle, C.~Freudenthaler, Z.~Gantner, and L.~Schmidt-Thieme.
\newblock Bpr: Bayesian personalized ranking from implicit feedback.
\newblock In \emph{Proceedings of the twenty-fifth conference on uncertainty in
  artificial intelligence}, pages 452--461. AUAI Press, 2009.

\bibitem[Vapnik(2000)]{Vapnik}
V.~N. Vapnik.
\newblock \emph{{The Nature of Statistical Learning Theory}}.
\newblock Lecture Notes in Statistics. Springer, 2000.

\bibitem[Yu et~al.(2010)Yu, Wan, and Lee]{YWL10}
P.~L.~H. Yu, W.~M. Wan, and P.~H. Lee.
\newblock \emph{Preference Learning}, chapter Decision tree modelling for
  ranking data, pages 83--106.
\newblock Springer, New York, 2010.

\end{thebibliography}
