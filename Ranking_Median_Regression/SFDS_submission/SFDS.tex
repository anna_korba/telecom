\documentclass[12pt]{article}
%
%
% Retirez le caractere "%" au debut de la ligne ci--dessous si votre
% editeur de texte utilise des caracteres accentues
% \usepackage[latin1]{inputenc}

%
% Retirez le caractere "%" au debut des lignes ci--dessous si vous
% utiisez les symboles et macros de l'AMS
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{stmaryrd}
\usepackage{multirow}
\usepackage{graphicx}

\newtheorem{theorem}{{\bf Theorem}}
\newtheorem{example}{{\bf Example}}
\newtheorem{proposition}{{\bf Proposition}}
\newtheorem{definition}{{\bf Definition}}
\newtheorem{corollary}{{\bf Corollary}}
\newtheorem{lemma}{{\bf Lemma}}
\newtheorem{remark}{{\bf Remark}}
\newtheorem{assumption}{{\bf Assumption}}

\newcommand{\n}{\llbracket n \rrbracket}

\setlength{\textwidth}{16cm}
\setlength{\textheight}{21cm}
\setlength{\hoffset}{-1.4cm}
%
%
\begin{document}


%--------------------------------------------------------------------------

\begin{center}
{\Large
	{\sc  Ranking Median Regression: \\Learning to Order through Local Consensus}
}
\bigskip

Anna Korba $^{1}$ \& Stephan Cl\'emen\c{c}on$^{2}$ \& Eric Sibony $^{3}$
\bigskip

{\it
$^{1}$ LTCI Telecom ParisTech, Universit\'e Paris-Saclay, anna.korba@telecom-paristech.fr 
 
$^{2}$ LTCI Telecom ParisTech, Universit\'e Paris-Saclay, stephan.clemencon@telecom-paristech.fr

$^{3}$ Shift Technology, esibony@gmail.com
}
\end{center}
\bigskip

%--------------------------------------------------------------------------

{\bf R\'esum\'e.} A l'\`ere des services utilisateurs personnalis\'es et syst\`emes de recommendation, pr\'edire les pr\'ef\'erences d'un individu/utilisateur sur un (peut-\^etre tr\`es large) ensemble d'objets index\'es par $\n=\{1,\; \ldots,\; n\}$, $n\geq 1$, d'apr\`es ses caract\'eristiques mod\'elis\'ees selon une v.a. $X$, prenant ses valeurs dans un espace objet $\mathcal{X}$, est un probl\`eme omnipr\'esent. Pourtant simple \`a formuler, ce probl\`eme pr\'edictif est tr\`es difficile \`a r\'esoudre en pratique. La principale probl\'ematique est le fait qu'ici, l'espace (discret) de sortie est le groupe symm\'etrique $\mathfrak{S}_n$, compos\'e de toutes les permutations de $\n$, de cardinalit\'e explosive  $n!$, et qui n'est pas un sous-espace vectoriel. C'est le but de cet article d'expliquer comment construire efficacement des r\`egles de pr\'ediction presque optimales prenant leur valeurs dans $\mathfrak{S}_n$, au moyen de techniques d'agr\'egation de classements efficaces impl\'ement\'ees au niveau local.
\smallskip

{\bf Mots-cl\'es.} R\'egression de classements, Agr\'egation de classements, R\`egle de Kemeny, Arbres de d\'ecision, Algorithme des K-plus proches voisins, Machine learning
\bigskip\bigskip

{\bf Abstract.} In the present era of personalized customer services and recommender systems, predicting the preferences of an individual/user over a (possibly very large) set of items indexed by $\n=\{1,\; \ldots,\; n\}$, $n\geq 1$, based on its characteristics, modelled as a r.v. $X$, taking its values in a feature space $\mathcal{X}$ say, is an ubiquitous issue. Though easy to state, this predictive problem is very difficult to solve in practice. The major challenge lies in the fact that, here, the (discrete) output space is the symmetric group $\mathfrak{S}_n$, composed of all permutations of $\n$, of explosive cardinality $n!$, and which is not a subset of a vector space. It is the purpose of this paper to explain how to build effectively nearly optimal predictive rules taking their values in $\mathfrak{S}_n$, by means of efficient ranking aggregation/consensus techniques implemented at a local level.
\smallskip

{\bf Keywords.} Regression ranking, Ranking aggregation, Kemeny's rule, Decision trees, K-nearest neighbors algorithm, Apprentissage machine

\section{Summary}
In an increasing number of modern applications, users are invited to declare their individual characteristics (\textit{e.g.} socio-demographic features), taking the form of a r.v. $X$ valued in a an input space $\mathcal{X}\subset \mathbb{R}^d$ say, and may express their preferences over a set of numbered services/products $\n=\{1,\; \ldots,\; n\}$. In this context, the goal pursued is to learn from historical data how to predict the preferences of any user based on her characteristics $X$. In the simplest formulation, the prediction takes the form of a permutation $s(X)$ on $\n$, mapping any item $i$ to its rank $s(X)(i)$ on her preference list. Denoting by $\Sigma$ the permutation that truly reflects the preferences of a user with characteristics $X$, the performance of any predictive rule, \textit{i.e.} any measurable function $s:\mathcal{X}\rightarrow \mathfrak{S}_n$, can be measured by the expected Kendall $\tau$ distance between $s(X)$ and $\Sigma$
\begin{equation}\label{eq:risk_theo}
\mathcal{R}(s)=\mathbb{E}\left[d_{\tau}\left(s(X),\Sigma  \right)  \right],
\end{equation}  
where the expectation is taken over the (unknown) distribution of the pair $(X,\Sigma)$ and $d_{\tau}(\sigma,\sigma')=\sum_{i<j}\mathbb{I}\{(\sigma(j)-\sigma(i)) \cdot (\sigma'(j)-\sigma'(i))<0   \}$ for all $(\sigma,\sigma')\in \mathfrak{S}_n^2$, denoting by $\mathbb{I}\{\mathcal{E}  \}$ the indicator function of any event $\mathcal{E}$. Stated this way, the objective is to build a mapping $s$ that minimizes \eqref{eq:risk_theo} and one may easily show with a straightforward conditioning argument that the optimal predictors are the rules that maps any point $X$ in the input space to any (Kemeny) ranking median of $P_X$, $\Sigma$'s conditional distribution given $X$. Recall that a Kemeny median of a probability distribution $P$ on $\mathfrak{S}_n$ is any solution $\sigma_P^*$ of the optimization problem
\begin{equation}\label{eq:median}
\min_{\sigma\in \mathfrak{S}_n}L_P(\sigma)
\end{equation}
where $L_P(\sigma)=\mathbb{E}_{\Sigma \sim P}\left[ d_{\tau}(\sigma,\Sigma) \right]$. For this reason, the predictive problem formulated above is referred to as \textit{ranking median regression} (RMR in abbreviated form). Regarding the problem of minimizing \eqref{eq:risk_theo}, attention should be paid to the fact that, in contrast to usual (median/quantile) regression, the set $\mathcal{S}$ of predictive ranking rules is not a vector space, which makes the design of practical optimization strategies challenging and the implementation of certain methods, based on (forward stagewise) additive modelling for instance, unfeasible (unless the constraint that predictive rules take their values in $\mathfrak{S}_n$ is relaxed, see \cite{CJ10} or \cite{FJBA13}). Here (see also \cite{CKS18}) we investigate this problem in a rigorous probabilistic framework (optimal elements, learning rate bounds, bias analysis), extending that recently developped for statistical Kemeny ranking aggregation in \cite{CKS17}. Based on this formulation, we propose a practical approach for ranking median regression, relying on the concept of local learning and permiting to derive practical procedures for building piecewise constant ranking rules from efficient (approximate) Kemeny aggregation techniques when implemented at a local level. In particular, nearest-neighbor techniques and decision trees methods are investigated and shown well-suited for this learning task.




%Quelques rappels :
%%
%\begin{center}
%%
%\begin{tabular}{lr} \hline
%%
%Accent aigu :              &  \'e; \\
%Accent grave :             &  \`a;\\
%Accent circonflexe :       &  \^o mais \^{\i};\\
%Tr\'emas :                 &  \"o mais \"{\i};\\
%C\'edille :                &  \c{c}. \\ \hline
%\end{tabular}
%%
%\end{center}

%--------------------------------------------------------------------------

%\section*{Bibliographie}
\bibliographystyle{plain}
\bibliography{biblio-thesis}

%\noindent [1] Korba, Anna and Cl{\'e}men{\c{c}}on, Stephan and Sibony, Eric (2017), A Learning Theory of Ranking Aggregation, Proceedings of Artificial Intelligence and Statistics, Fort Lauderdale.

%\noindent [2] Achin, M. et Quidont, C. (2000), {\it Th\'eorie des
%Catalogues}, Editions du Soleil, Montpellier.

%\noindent [3] Noteur, U. N. (2003), Sur l'int\'er\^et des
%r\'esum\'es, {\it Revue des Organisateurs de Congr\`es}, 34, 67--89.

\end{document}

