\documentclass[12pt]{article}
\usepackage{amssymb}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{epsfig}

\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}[theorem]{Acknowledgement}
\newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{axiom}[theorem]{Axiom}
\newtheorem{case}[theorem]{Case}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{conclusion}[theorem]{Conclusion}
\newtheorem{condition}[theorem]{Condition}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{criterion}[theorem]{Criterion}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{exercise}[theorem]{Exercise}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{notation}[theorem]{Notation}
\newtheorem{problem}[theorem]{Problem}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{assumption}[theorem]{Assumption}
%\newtheorem{remark}[theorem]{Remark}
\newtheorem{rema}{Remark}
\newenvironment{remark}{\begin{rema} \rm}{\end{rema}}
%\newtheorem{example}[theorem]{Example}
\newtheorem{exam}{Example}
\newenvironment{example}{\begin{exam} \rm}{\end{exam}}

\newtheorem{solution}[theorem]{Solution}
\newtheorem{summary}[theorem]{Summary}
\newenvironment{proof}[1][Proof]{\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\def\R{\mathbb{R}}
\def\Rd{\R^d}
\def\E{\mathbb E}
\def\e{\mathbf e}
\def\i{\mathbf i}
\def\j{\mathbf j}
\def\EXP{{\E}}
\def\expec{{\EXP}}
\def\PROB{{\mathbb P}}
\def\S{\mathcal{S}}
\def\var{{\rm Var}}
\def\shat{{\mathbb S}}
\def\IND#1{{\mathbb I}_{{\left[ #1 \right]}}}
%\def\IND#1{{\mathbb I}_{{ #1 }}}
\def\I{{\mathbb I}}
\def\pr{\PROB}
\def\prob{\PROB}
\def\wh{\widehat}
\def\ol{\overline}
\newcommand{\deq}{\stackrel{\scriptscriptstyle\triangle}{=}}
\newcommand{\defeq}{\stackrel{\rm def}{=}}
\def\isdef{\defeq}
\newcommand{\xp}{\mbox{$\{X_i\}$}}
\def\diam{\mathop{\rm diam}}
\def\argmax{\mathop{\rm arg\, max}}
\def\argmin{\mathop{\rm arg\, min}}
\def\essinf{\mathop{\rm ess\, inf}}
\def\esssup{\mathop{\rm ess\, sup}}
\def\supp{\mathop{\rm supp}}
\def\cent{\mathop{\rm cent}}
\def\dim{\mathop{\rm dim}}
\def\sgn{\mathop{\rm sgn}}
\def\logit{\mathop{\rm logit}}
\def\proof{\medskip \par \noindent{\sc proof.}\ }
\def\proofsketch{\medskip \par \noindent{\sc Sketch of proof.}\ }
%% Not in latex2e:
%  \def\qed{\hfill $\Box$ \medskip}
%  \def\qed{$\Box$}
\def\blackslug{\hbox{\hskip 1pt \vrule width 4pt height 8pt depth 1.5pt
\hskip 1pt}}
\def\qed{\quad\blackslug\lower 8.5pt\null\par}
\newcommand{\F}{{\cal F}}
\newcommand{\G}{{\cal G}}
\newcommand{\X}{{\cal X}}
\newcommand{\D}{{\cal D}}
\newcommand{\cR}{{\cal R}}
\newcommand{\COND}{\bigg\vert} % for conditional expectations
%
\def\C{{\cal C}}
\def\roc{\rm ROC}
\def\auc{\rm AUC}
\def\B{{\cal B}}
\def\A{{\cal A}}
\def\N{{\cal N}}
\def\F{{\cal F}}
\def\L{{\cal L}}
\def\eps{{\varepsilon}}
\def\Var{{\rm Var}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\EE}{\mathbb{E}}
\newcommand{\Prob}[1]{\mathbb{P}\left\{ #1 \right\} }

\allowdisplaybreaks

\begin{document}
\begin{center}
\large{Learning to Order: Median Ranking Regression}
\end{center}
%\hline

\bigskip


\noindent {\bf Probabilistic Framework.} Let $n\geq 1$ and consider $n$ instances indexed by $i\in\{1,\; \ldots,\; n  \}$. Consider also a generic random pair $(\Sigma,\; X)$ defined on a probability space $(\Omega,\; \mathcal{F},\; \mathbb{P})$, where the variable $\Sigma$  takes its values in the symmetric group $\mathfrak{S}_n$ and describes a random ranking (\textit{i.e.} list of preferences) of the instances, while $X$ is a random vector, valued in a feature space $\mathcal{X}$ (of possibly high dimension, typically a subset of $\mathbb{R}^d$ with $d\geq 1$) and modelling some information hopefully useful to predict $\Sigma$ (or at least to recover some of its characteristics). The joint distribution of the r.v. $(\Sigma,\; X)$ is described by $(\mu,\; P_X)$, where $\mu$ denotes $X$'s marginal distribution and $P_X$ means the conditional probability distribution of $\Sigma$ given $X$: $\forall \sigma\in \mathfrak{S}_n$,
 $$
 P_X(\sigma)\overset{def}{=}\mathbb{P}\{ \Sigma=\sigma \mid X\}\;\; \text{almost-surely}.
 $$
 We also introduce the marginal probability distribution $P$ of $\Sigma$: $\forall \sigma\in \mathfrak{S}_n$, $P(\sigma)=\int_{x\in \mathcal{X}} P_x(\sigma)\mu(dx)$.
 \medskip
 
 \noindent {\bf (Conditional) Median Rankings.} Let $d$ be a certain distance on $\mathfrak{S}_n$ (\textit{e.g.} the Kendall tau distance). The expected distance between any permutation $\sigma$ and the random permutation $\Sigma$ is denoted by:
 $$
 L_P(\sigma)=\mathbb{E}_{\Sigma \sim P}[d(\Sigma,\sigma)  ].
 $$
 A median of distribution $P$ is any solution $\sigma^*$ of the minimization problem:
 \begin{equation}\label{eq:median_pb}
 \min_{\sigma \in \mathfrak{S}_n}L_P(\sigma). 
 \end{equation}
 The minimum $L_P^*$ also provides a measure of dispersion of distribution $P$. 
Due to the finiteness of $\mathfrak{S}_n$, solutions of \eqref{eq:median_pb} always do exist but are not necessarily unique. We shall denote by $\mathcal{M}_P\subset \mathfrak{S}_n$ the collection of ranking medians (w.r.t. metric $d(.,\; .)$) related to distribution $P$.
 Here, our interest focuses on \textit{conditional median rankings}, cast as solutions of a certain median ranking regression problem. In order to formulate rigorously the statistical learning problem we consider, more notations are needed. Introduce first the conditional expected distance:
 \begin{equation}
 L_X(\sigma)\overset{def}{=}\mathbb{E}[ d(\Sigma,\sigma)   \mid X] \text{ almost-surely}.
 \end{equation}
 Given $X$, a \textit{conditional ranking median} is a solution of the minimization problem: $\min_{\sigma\in \mathfrak{S}_n}L_X(\sigma)$. For notational convenience, we set $\mathcal{M}_X=\mathcal{M}_{P_X}$.
 \medskip
 
 \noindent {\bf Ranking prediction.} Let $\mathcal{S}$ be the set of measurable mappings $s:\mathcal{X}\rightarrow \mathfrak{S}_n$, referred to as \textit{predictive ranking rules} here. Conditional ranking medians provide solutions for the predictive problem, where the goal pursued is to find $s\in \mathcal{S}$ that minimizes the prediction error (related to the metric $d$):
 \begin{equation}\label{eq:risk}
 \mathcal{R}(s)=\mathbb{E}\left[ d\left(s(X),\Sigma   \right) \right].
 \end{equation}
 Indeed, using a straightforward conditioning argument, one may write, for any predictive ranking rule $s$,
 \begin{equation}
 \mathcal{R}(s)=\mathbb{E}_{X\sim \mu}\left[ L_X(\Sigma, s(X)) \right].  
 \end{equation}
 The minimum of the quantity inside the expectation is attained as soon as $s(X)$ is a median for $P_X$, \textit{i.e.} $s(X)\in  \argmin_{\sigma\in \mathfrak{S}_n} \mathbb{E}\left[ L_X(\sigma  ) \mid X\right]$. Hence, for the predictive problem thus formulated, optimal rules coincide with the elements of the set $\mathcal{S}$ formed by the measurable mappings $s^*:\mathcal{X}\rightarrow \mathfrak{S}_n$ s.t. $s^*(X)\in \mathcal{M}_X$ with probability one. Equipped with the notations above, the minimum prediction error can be written as $\mathcal{R}^*=\mathbb{E}_{X\sim \mu}[L^*_X]$.
 \medskip
 
 \noindent {\bf Statistical setting.} We assume that we observe $(X_1,\; \Sigma_1)\; \ldots,\; (X_1,\; \Sigma_N)$, $N\geq 1$ i.i.d. copies of $(X,\; \Sigma)$ and, based on these data, the objective is to build a predictive ranking rule $s$ that nearly minimizes $\mathcal{R}(s)$ over the class $\mathcal{S}$.
 
 \begin{remark}{\sc (Quantile regression)}
 The problem mentioned above can be somehow viewed as an extension of the usual quantile regression problem, that can also be classically formulated as a $M$-estimation problem in functional space.
 \end{remark}
 
 Of course, the Empirical Risk Minimization (ERM) paradigm encourages to consider solutions of the empirical minimization problem:
 \begin{equation}\label{eq:ERM}
 \min_{s\in \mathcal{S}_0}\widehat{\mathcal{R}}_N(s),
 \end{equation}
 where $\mathcal{S}_0$ is a subset of $\mathcal{S}$, supposed to be rich enough for containing approximate versions of elements of $\mathcal{S}^*$ (\textit{i.e.} so that $\inf_{s\in \mathcal{S}_0}\mathcal{R}(s)- \mathcal{R}^*$ is 'small') and ideally appropriate for continuous or greedy optimization, and
 \begin{equation}\label{eq:emp_risk}
 \widehat{\mathcal{R}}_N(s)=\frac{1}{N}\sum_{i=1}^N d(s(X_i),\;  \Sigma_i)
 \end{equation}
 is a statistical version of \eqref{eq:risk} based on the data available. If $\mu$ is continuous (the $X_i$'s are pairwise distinct), it is always possible to find $s\in \mathcal{S}$ such that $\widehat{R}_N(s)=0$ and model selection/regularization issues (\textit{i.e.} choosing an appropriate class $\mathcal{G}$) are crucial. In contrast, if $X$ takes discrete values only (corresponding to possible requests in a search engine for instance, like in the LETOR setting), in the set $\{1,\; \ldots,\; K \}$ with $K\geq 1$ say, the problem \eqref{eq:ERM} boils down to solving \textit{independently} $K$ empirical ranking median problems. However, $K$ may be large and it may be relevant to use some regularization procedure accounting for the possible amount of similarity shared by certain requests, adding some penalization term to \eqref{eq:emp_risk}.
 \medskip
 
 \noindent {\bf Local Ranking Medians.} A framework halfway between these two situations consists in finding a median ranking regression rule that is \text{piecewise constant}.
 Precisely, one may try to find a rule of the form:
 \begin{equation}\label{eq:piecewise_cst}
 s_{\mathcal{P}}(x)=\sigma_k \text{ iff } x\in \mathcal{C}_k
 \end{equation}
 where $\mathcal{P}$ is a partition of $\mathcal{X}$ composed of $K\geq 1$ cells $\mathcal{C}_1,\; \ldots,\; \mathcal{C}_K$ and $\{\sigma_1,\; \ldots,\; \sigma_K  \}$ is a set of $K$ distinct permutations. Observe that solutions of the risk minimization problem
 $$
 \min_{s\in \mathcal{S}}\mathcal{R}(s)
 $$
 are of the same form as \eqref{eq:piecewise_cst} as soon as $P_. : x\in \mathcal{X}\mapsto P_x$ is constant on each cell $\mathcal{C}_k$. Since any mapping $P_.$ can be approximated by a piecewise constant function (investigate the bias however), we propose to solve the median ranking regression problem by means of a recursive method in the spirit of the {\sc CART} algorithm for regression, we call the {\sc Ranking Median Regression Tree} algorithm, that produces binary tree structured piecewise constant conditional ranking median estimates based on a training sample $\mathcal{D}_N=\{(\Sigma_1,\; \X_1),\; \ldots,\; (\Sigma_N,\; X_N)  \}$.
 One starts from the root node $\mathcal{C}_{0,0}=\mathcal{X}$ and considers as initial median ranking regression rule $s_0$ any empirical ranking median $\sigma_{0,0}$ of the observations $\Sigma_1,\; \ldots,\; \Sigma_N$. At depth level $j\geq 0$, any cell $\mathcal{C}_{j,k}$, $0 \leq k <2^j$ shall be split into two (disjoint) subsets $\mathcal{C}_{j+1,2k}$ and $\mathcal{C}_{j+1,2k+1}$, respectively identified as the left and right children of the interior leaf $(j,k)$ of the ranking median regression tree, according to the following \textit{splitting rule}.\\
 
 \noindent {\it Splitting rule.} For any (non empty) candidate left child $\mathcal{C}\subset \mathcal{C}_{j,k}$, picked in a class of 'admissible' subsets (from a computational viewpoint, in particular), compute solutions $\widehat{\sigma}_{\mathcal{C}}$ and $\widehat{\sigma}_{\mathcal{C}_{j,k}\setminus \mathcal{C}}$ of the median ranking problems:
 \begin{equation}
 \min_{\sigma \in \mathfrak{S}_n}\widehat{\mathcal{R}}_{\mathcal{C}}(\sigma) \text{ and }  \min_{\sigma \in \mathfrak{S}_n}\widehat{\mathcal{R}}_{\mathcal{C}_{j,k}\setminus \mathcal{C}}(\sigma),
 \end{equation}
 where, for all $\sigma\in \mathfrak{S}_n$ and any measurable $\mathcal{C}'\subset \mathcal{X}$, 
 $$
 \widehat{\mathcal{R}}_{\mathcal{C}'}(\sigma)=\sum_{i=1}^N\mathbb{I}\{ X_i\in \mathcal{C}' \}\cdot d\left(\Sigma_i,\; \sigma  \right).
 $$
 The relevance of the split $\mathcal{C}_{j,k}=\mathcal{C}'\cup (\mathcal{C}_{j,k}\setminus\mathcal{C})$ is naturally evaluated through the quantity:
 \begin{equation}
\mathcal{I}_{j,k}(\mathcal{C})= \widehat{L}_{\mathcal{C}}(\widehat{\sigma}_{\mathcal{C}})+ \widehat{L}_{\mathcal{C}_{j,k}\setminus\mathcal{C}}(\widehat{\sigma}_{\mathcal{C}_{j,k}\setminus \mathcal{C}}),
 \end{equation}
 where, for all $\sigma\in \mathfrak{S}_n$ and any measurable $\mathcal{C}'\subset \mathcal{X}$, 
 $$
 \widehat{L}_{\mathcal{C}'}(\sigma)=\sum_{i=1}^N \mathbb{I}\{ X_i\in \mathcal{C}' \}\cdot d\left(\Sigma_i  ,\; \sigma\right)
 $$
is a measure of the quality of the consensus that $\sigma$ provides for obervations $\Sigma_i$ such that $X_i$ lies in the set $\mathcal{C}'$. Choosing $\mathcal{C}_{j+1,2k+1}$ as a solution of the problem
$$
\min_{\mathcal{C}\subset \mathcal{C}_{j,k}}\mathcal{I}_{j,k}(\mathcal{C}),
$$
and setting 
$$
\sigma_{j+1,2k}=\widehat{\sigma}_{\mathcal{C}_{j+1,2k}} \text{ and } \sigma_{j+1,2k+1}=\widehat{\sigma}_{\mathcal{C}_{j+1,2k+1}} 
$$
we build a refinement of the median ranking regression rule $s_{2^j+k-1}(x)$ obtained at step $2^j+k-1$ of the algorithm by defining $s_{2^j+k}(x)$ as follows:
\begin{equation}
s_{2^j+k}(x)=\left\{ 
\begin{array}{lll}
s_{2^j+k-1}(x) & if & x\notin \mathcal{C}_{j,k}\\
\sigma_{j+1,2k}& if & x\in \mathcal{C}_{j+1,2k}\\
\sigma_{j+1,2k+1}& if & x\in \mathcal{C}_{j+1,2k+1}
\end{array}
\right.
.
\end{equation}
Hence, proceeding recursively this way, we necessarily have:
$$
\widehat{\mathcal{R}}_N(s_{2^j+k})\leq \widehat{\mathcal{R}}_N(s_{2^j+k-1})
$$
 \medskip
 
 \noindent {\bf Hierarchical Clustering of Rankings.} 
 
 \noindent {\bf Noise models.}
 
 \bigskip
 
 \noindent {\bf Lines of Research.}
 
 
 \begin{enumerate}
 \item {\bf Parametric conditional preference models.} There exist parametric models for preference data (\textit{e.g.} (mixtures of) Mallows distributions, Placket-Luce, Bradley-Terry), allowing for a 'direct' computation of median ranking estimates (\textit{e.g.} by MLE). Would it be adequate to consider conditional versions of such models, stipulating a certain (parametric) form for the dependency on the input information $X$? Does such a statistical literature, inspired by that devoted to (quantile) regression, already exist? Whereas in the usual (quantile) regression framework, modeling variability in the output space ($\mathbb{R}$ or $\mathbb{R}^d$) is straightforward using basic algebra operations (considering \textit{linear models} for instance), this is not the case for $\mathfrak{S}_n$ as output space.
 
 \item {\bf Generalization properties of empirical risk minimizers.} If $\mathcal{X}$ is infinite, the cardinality of $\mathcal{S}$ is infinite too and complexity assumptions on $\mathcal{S}_0$ are needed to control the generalization ability of solutions of $\min_{s\in \mathcal{S}_0}\widehat{\mathcal{R}}_n(s)$.
 
 
 \item {\bf Heterogeneous ranking data and MRA}. In practice, the $\mathcal{\Sigma}_i'$'s are not fully available, only partial information can be observed (through a design $\nu$ possibly depending on $X$). The MRA representation may permit to formulate the predictive problem in this setup and solve it efficiently.
 \end{enumerate}
 
 
\noindent {\bf Parametric conditional preference models.}\\

\noindent In \cite{rendle2009bpr}, the authors consider the problem of personalized ranking, that is to provide a user with a ranked list of items, based on implicit behavior (eg purchases in the past). Formally, let $U$ be the set of users, $I$ the set of items, and $S \in U \times I$ the implicit feedback available. The task is to provide the user with a personalized total ranking $<_{u}$ of all items. Let $I^{+}_{u}=\left\{ i \in I : (u,i) \in S \right\}$, $U^{+}_{i}=\left\{ u \in U : (u,i) \in S \right\}$ and $D_{S}=\left\{ (u,i,j): i \in I^{+}_{u} \wedge j \in I \backslash I^{+}_{u} \right\}$. The Bayesian formulation of finding the correct personalized ranking for all items is to maximize the posterior probability where $\Theta$ represents the parameter vector of an arbitrary model class (eg matrix factorization, knn):
\begin{equation*}
p(\Theta| >_{u}) \propto p(>_{u}| \Theta) p(\Theta)
\end{equation*}
\noindent All users are presumed to act independently of each other, and the ordering of each pair of items $(i,j)$ for a specific user is idenpendent of the ordering of every other pair. The user-specific likelihood function is thus:
\begin{equation*}
\prod_{u\in U} p(>_{u}| \Theta) = \prod_{(u,i,j) \in D_S} p(i >_{u} j| \Theta)
\end{equation*}
\noindent The authors then define the individual probability that a user prefers item $i$ to item $j$ as:
\begin{equation*}
 p(i >_{u} j| \Theta)= \sigma(\hat{x}_{uij}(\Theta))
\end{equation*}
\noindent where $\sigma$ is the logistic sigmoid:
\begin{equation*}
\sigma(w)=\frac{1}{1+ e^{-x}}
\end{equation*}
Here $\hat{x}_{uij}(\Theta)$ is an arbitrary real-valued function of the model parameter vector $\Theta$ which captures the special relationship between user $u$, item $i$ and item $j$. The estimation of $\hat{x}_{uij}(\Theta)$ depends on the underlying model class chosen for modeling the relationship between $u$, $i$ and $j$. To complete the Bayesian modeling, the authors introduce a general prior density $p(\Theta) \sim \mathcal{N}(O, \Sigma_{\Theta})$ (normal distribution with zeo mean and covariance matrix $\Sigma_{\Theta}$. The goal is then to optimize with MLE the following criterion:
\begin{align*}
BPR-OPT&= ln p(\Theta | >_{u}) \\
&= ln p(>_{u}| \Theta) p(\Theta)\\
&= ln \prod_{(u,i,j) \in D_S} \sigma(\hat{x}_{uij}(\Theta))p(\Theta)\\
&= \sum_{(u,i,j) \in D_S} ln \sigma(\hat{x}_{uij}(\Theta)) + ln p(\Theta)\\
&= \sum_{(u,i,j) \in D_S} ln \sigma(\hat{x}_{uij}(\Theta)) - \lambda_{\Theta} \| \Theta \|^2
\end{align*}

\noindent The authors then apply this framework in the case of matrix factorization and k-nearest-neighbor. Both classes model the hidden preferences of a user on an item, and predict a real number $\hat{x}_{ul}$ per user-item pair $(u,l)$. So they decompose the estimator $\hat{x}_{uij}$ as :$\hat{x}_{uij}= \hat{x}_{ui} -\hat{x}_{uj}$. The individual probability that the user $u$ prefers item $i$ to item $j$ can thus be seen as a conditional Bradley-Terry-Luce model since:
\begin{equation*}
p(i >_{u} j| \Theta)= \sigma(\hat{x}_{uij}(\Theta))=\frac{1}{1+ e^{-\hat{x}_{uij}}}=\frac{e^{-\hat{x}_{ui}}}{e^{-\hat{x}_{ui}}+ e^{-\hat{x}_{uj}}}
\end{equation*}\\

\noindent In \cite{lu2015individualized}, the authors propose a method to compute individual total rankings. They study a version of the problem known as "collaborative ranking". They assume that users give pairwise preferences, so that the data is of the form $(X^{(i)}, y_i)$ where $X^{(i)} \in \mathbb{R}^{d_1 \times d_2}$. The $i$-th piece of data is a query to user $k(i)$ asking if she
prefers item $l(i)$ to item $j(i)$, where $y_i$ = 1 if the user prefers
item l(i) to item j(i), otherwise $y_i$ = 0. This gives $X^{(i)} = \sqrt{d_1 d_2} e^{(k(i))}(e^{(l(i))}-e^{(j(i))})^T$ where $e^{(a)}$ is the
standard basis vector that takes on the value 1 in the $a$-th
entry and zeros everywhere else. The first assumption that the authors make is that the user preferences are encoded in
a matrix $\Theta^{*} \in \mathbb{R}^{d_1 \times d_2}$ such that $\Theta^{*}_{k,j}$ is the score that user $k$ places on item $j$. The relative preference that user $k(i)$ has for item
$l(i)$ versus $j(i)$ is thus denoted by the trace inner product
between $\Theta^{*}$ and $X^(i)$: $\left\langle \Theta^{*} , X^(i) \right\rangle= \sqrt{d_1 d_2} (\Theta^{*}_{k(i), l(i)}-\Theta^{*}_{k(i), j(i)})$. Then, they also assume that the pairwise preferences follow a Bradley-Terry-Luce model:
\begin{equation*}
\mathbb{P}(y_i=1 | l(i)=l, j(i) = j, k(i) = k ) = \frac{exp(\left\langle \Theta^{*} , X^{(i)} \right\rangle)}{1+ exp(\left\langle \Theta^{*} , X^{(i)} \right\rangle)}
\end{equation*}
Furthermore, they want $\Theta^{*}$
to be low-rank or well approximated by a
low-rank matrix (this is analogous to the matrix completion
literature and models the fact that the underlying preferences
are derived from latent low-dimensional factors). They thus choose the following regularized M-estimator:
\begin{equation*}
\hat{\Theta}= \argmin_{\Theta \in \Omega} L_n(\Theta) +\lambda \| \Theta \|_{norm}
\end{equation*}
where $L_n(\Theta)=\frac{1}{n} \sum_{i=1}^{n} log (1 +exp(\left\langle \Theta , X^{(i)} \right\rangle)) - y_i (\left\langle \Theta , X^{(i)} \right\rangle)$ is the random loss function and
$\Omega = \left\{ A \in \mathbb{R}^{d_1 \times d_2} | \| A \|_{\infty} \le \alpha, \text{ and } \forall j \in [d_1] \text{ we have } \sum_{k=1}^{d_2} A_{jk}=0 \right\}$.\\


\noindent In \cite{liu2009probabilistic}, the authors argue that for the ranking task (for example, generate a personnalized top-k list of items for each user), it is better to see collaborative filtering as a ranking problem than as a regression problem (ie, rating prediction).
\bibliographystyle{apalike}
\bibliography{biblio-thesis}
 \end{document}
 