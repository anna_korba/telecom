\section{Local Consensus Methods for Ranking Median Regression}\label{sec:main}
We start here with introducing notations to describe the class of piecewise constant ranking rules and explore next approximation of a given ranking rule $s(x)$ by elements of this class, based on a local version of the concept of ranking median recalled in the previous section. %Two strategies are investigated in the Appendix in order to generate adaptively a partition tailored to the training data and yielding a ranking rule with nearly minimum predictive error. 
Throughout this section, for any measurable set $\mathcal{C}\subset \mathcal{X}$ weighted by $\mu(x)$, the conditional distribution of $\Sigma$ given $X\in \mathcal{C}$ is denoted by $P_{\mathcal{C}}$. When it belongs to $\mathcal{T}$, the unique median of distribution $P_{\mathcal{C}}$ is denoted by $\sigma^*_{\mathcal{C}}$ and referred to as the local median on region $\mathcal{C}$.

\subsection {Piecewise Constant Predictive Ranking Rules and Local Consensus}\label{subsec:piece}
Let $\mathcal{P}$ be a partition of $\mathcal{X}$ composed of $K\geq 1$ cells $\mathcal{C}_1,\; \ldots,\; \mathcal{C}_K$ (\textit{i.e.} the $\mathcal{C}_k$'s are pairwise disjoint, their union is the feature space $\mathcal{X}$). Suppose in addition that $\mu(\mathcal{C}_k)>0$ for $k=1,\; \ldots,\; K$. %Using the natural embedding $\mathfrak{S}_n\subset \mathbb{R}^n$, 
Any $s\in \mathcal{S}$ that is constant on each subset $\mathcal{C}_k$ can be written as 
\begin{equation}\label{eq:piecewise_cst}
 s_{\mathcal{P}, \bar{\sigma}}(x)=\sum_{k=1}^K\sigma_k \cdot \mathbb{I}\{x\in \mathcal{C}_k\},
 \end{equation}
where $\bar{\sigma}=(\sigma_1,\; \ldots,\; \sigma_K)$ is a collection of $K$ permutations. We denote by $\mathcal{S}_{\mathcal{P}}$ the collection of all ranking rules that are constant on each cell of $\mathcal{P}$. Notice that $\# \mathcal{S}_{\mathcal{P}}=K\times n!$.
\medskip

 \noindent {\bf Local Ranking Medians.} The next result describes the most accurate ranking median regression function in this class. The values it takes correspond to \textit{local Kemeny medians}, \textit{i.e.} medians of the $P_{\mathcal{C}_k}$'s. The proof is straightforward and postponed to the Appendix.
\begin{proposition}\label{prop:opt_subclass}
The set $\mathcal{S}^*_{\mathcal{P}}$ of solutions of the risk minimization problem $\min_{s\in \mathcal{S}_{\mathcal{P}}}\mathcal{R}(s)$
is composed of all scoring functions $s_{\mathcal{P}, \bar{\sigma}}(x)$ such that, for all $k\in \{1,\; \ldots,\; K  \}$, the permutation $\sigma_k$ is a Kemeny median of distribution $P_{ \mathcal{C}_k}$
and 
$$
\min_{s\in \mathcal{S}_{\mathcal{P}}}\mathcal{R}(s) =\sum_{k=1}^K\mu(\mathcal{C}_k)L^*_{P_{\mathcal{C}_k}}.
$$
 If $P_{\mathcal{C}_k}\in \mathcal{T}$ for $1\leq k\leq K$, there exists a unique risk minimizer over class $\mathcal{S}_{\mathcal{P}}$ given by: 
\begin{equation}\label{eq:piecewise_cst_opt}\forall x\in \mathcal{X},\;\;
 s^*_{\mathcal{P}}(x)=\sum_{k=1}^K\sigma^*_{P_{\mathcal{C}_k}} \cdot \mathbb{I}\{x\in \mathcal{C}_k\}.
 \end{equation}
\end{proposition}
 
\noindent Attention should be paid to the fact that the bound
  \begin{equation}\label{eq:bias1}
  \min_{s\in \mathcal{S}_{\mathcal{P}}}\mathcal{R}(s) - \mathcal{R}^*\leq \inf_{s\in \mathcal{S}_{\mathcal{P}}}\mathbb{E}_X\left[ d_{\tau}\left(s^*(X), s(X)\right) \right]
  \end{equation}
is valid for all $s^*\in \mathcal{S}^*$,
  shows that the bias of ERM over class $\mathcal{S}_{\mathcal{P}}$ can be controlled by the approximation rate of optimal ranking rules by elements of $\mathcal{S}_{\mathcal{P}}$ when error is measured by the integrated Kendall $\tau$ distance and $X$'s marginal distribution, $\mu(x)$ namely, is the integration measure.
 \medskip
 
 \noindent {\bf Approximation.} We now investigate to what extent ranking median regression functions $s^*(x)$ can be well approximated by predictive rules of the form \eqref{eq:piecewise_cst}. 
 %The proposition below provides the best approximation of a given ranking rule $s(x)$ in terms of $\mu$-integrated Kendall $\tau$ distance.
% \begin{proposition}\label{prop:opt}
%Let $s\in \mathcal{S}$. The best approximations to $s(x)$ from $\mathcal{S}_{\mathcal{P}}$ in terms of $\mu$-integrated Kendall $\tau$ distance are the ranking rules of the form  $s_{\mathcal{P}, \bar{\sigma}}(x)$, where $\sigma_k$ is a Kemeny median of distribution $P_{s,\mathcal{C}_k}$, the conditional distribution of the r.v. $s(X)$ given $X\in \mathcal{C}_k$, for $k\in\{1,\; \ldots,\; n \}$. The minimum error is given by:
 %$$
 %\mathcal{E}_{\mathcal{P}}(s)=\sum_{k=1}^K\mu(\mathcal{C}_k)L^*_{P_{s,\mathcal{C}_k}}.
 %$$
 %\end{proposition}
 We assume that $\mathcal{X}\subset \mathbb{R}^d$ with $d\geq 1$ and denote by $\vert\vert .\vert\vert$ any norm on $\mathbb{R}^d$. The following hypothesis is a classic smoothness assumption on the conditional pairwise probabilities.

 \begin{assumption}\label{hyp:smooth} For all $1\leq i<j\leq n$, the mapping $x\in \mathcal{X}\mapsto p_{i,j}(x)$ is Lipschitz, \textit{i.e.} there exists $M<\infty$ such that:
$
\forall (x,x')\in\mathcal{X}^2,\;\; \sum_{i<j}\vert p_{i,j}(x)-p_{i,j}(x') \vert\leq M \cdot \vert\vert x-x'\vert\vert $.
 \end{assumption}
 %For simplicity, we assume that $\mathcal{X}=[0,1]^d$.
  The next result shows that, under the assumptions above, the optimal prediction rule $\sigma^*_{P_X}$ can be accurately approximated by \eqref{eq:piecewise_cst_opt}, provided that the regions $\mathcal{C}_k$ are 'small' enough.
 \begin{theorem}\label{thm:approx}
 Suppose that $P_x\in \mathcal{T}$ for all $x\in \X$ and that Assumption \ref{hyp:smooth} is fulfilled. Then,
  \begin{equation}\label{eq:RMR_bound} \forall s_{\mathcal{P}}\in \mathcal{S}^*_{\mathcal{P}},\;\;
 \mathcal{R}(s_{\mathcal{P}})-\mathcal{R}^*\leq M\cdot \delta_{\mathcal{P}},
  \end{equation}
  where $\delta_{\mathcal{P}}=\max_{\mathcal{C}\in \mathcal{P}}\sup_{(x,x')\in \mathcal{C}^2}\vert\vert x-x'\vert\vert$ is the maximal diameter of $\mathcal{P}$'s cells. If $(\mathcal{P}_m)_{m\geq 1}$ is a sequence of partitions of $\mathcal{X}$ such that $\delta_{\mathcal{P}_m}\rightarrow 0$, then $\mathcal{R}(s_{\mathcal{P}_m})\rightarrow \mathcal{R}^*$ as $m\rightarrow \infty$.
 Suppose in addition that Assumption \ref{hyp:margin} is fulfilled and that $P_{\mathcal{C}}\in \mathcal{T}$ for all $\mathcal{C}\in \mathcal{P}$. Then,
 \begin{equation}\label{eq:RMR_bound2}
\mathbb{E}\left[d_{\tau}\left(\sigma^*_{P_X}, s^*_{\mathcal{P}}(X)\right)  \right]\leq \sup_{x\in \mathcal{X}}d_{\tau}\left(\sigma^*_{P_x}, s^*_{\mathcal{P}}(x)\right)\leq (M/H)\cdot \delta_{\mathcal{P}}.
 \end{equation}
 %where $\delta_{\mathcal{P}}=\max_{\mathcal{C}\in \mathcal{P}}\sup_{(x,x')\in \mathcal{C}^2}\vert\vert x-x'\vert\vert$ is the maximal diameter of $\mathcal{P}$'s cells. Hence, if $(\mathcal{P}_m)_{m\geq 1}$ is a sequence of partitions of $\mathcal{X}$ such that $P_{\mathcal{C}}\in \mathcal{T}$ for all $\mathcal{C}\in \mathcal{P}_m$, $m\geq 1$, and $\delta_{\mathcal{P}_m}\rightarrow 0$ as $m$ tends to infinity, then $$\sup_{x\in \mathcal{X}}d_{\tau}\left(\sigma^*_{P_x}, s^*_{\mathcal{P}_m}(x)\right)\rightarrow 0, \text{ as } m\rightarrow \infty.$$
 \end{theorem}
 %Triangular setting for smoothness assumptions? $K$ should be much smaller than $n!$.
The upper bounds above reflect the fact that the the smaller the Lipschitz constant $M$, the easier the ranking median regression problem and that the larger the quantity $H$, the easier the recovery of the optimal RMR rule. In the Appendix, examples of distributions $(\mu(dx), P_x)$ satisfying Assumptions \ref{hyp:margin}-\ref{hyp:smooth} both at the same time are given.

\subsection{On the Consistency of RMR Rules Based on Data-Driven Partitioning}

The next result, proved in the Appendix, states a general consistency theorem for a wide class of RMR rules based on data-based partitioning, in the spirit of \citet{LN96} for classification. For simplicity, we assume that $\mathcal{X}$ is compact, equal to $[0,1]^d$ say. Let $N\geq 1$, a $N$-sample partitioning rule $\pi_N$ maps any possible training sample $\mathcal{D}_N=((X_1,\Sigma_1),\; \ldots,\; (X_N,\Sigma_N) )\in (\mathcal{X}\times \mathfrak{S}_n)^N$ to a partition $\pi_N(\mathcal{D}_N)$ of $[0,1]^d$ composed of borelian cells. The associated collection of partitions is denoted by
$
\mathcal{F}_N=\{ \pi_N(\mathcal{D}_N):  \mathcal{D}_N\in(\mathcal{X}\times \mathfrak{S}_n)^N\}$. As in \citet{LN96}, the complexity of $\mathcal{F}_N$ is measured by the $N$-order shatter coefficient of the class of sets that can be obtained as unions of cells of a partition in $\mathcal{F}_N$, denoted by $\Delta_N(\mathcal{F}_N)$. An estimate of this quantity can be found in \textit{e.g.} Chapter 21 of \citet{DGL96}  for various data-dependent partitioning rules (including the recursive partitioning scheme described in subsection \ref{subsec:algo}, when implemented with axis-parallel splits). When $\pi_N$ is applied to a training sample $\mathcal{D}_N$, it produces a random partition $\mathcal{P}_N=\pi_N(\mathcal{D}_N)$ associated with a RMR prediction rule: $\forall x\in \mathcal{X}$,
\begin{equation}\label{eq:rule_theor}
s_N(x)=\sum_{\mathcal{C}\in \mathcal{P}_N}\widehat{\sigma}_{\mathcal{C}}\cdot \mathbb{I}\{x\in \mathcal{C}  \}
% =\widehat{ \sigma}}_{\mathcal{C}_N(x)},
\end{equation}
where $\widehat{\sigma}_{\mathcal{C}}$ denotes a Kemeny median of the empirical version of $\Sigma$'s distribution given $X\in \mathcal{C}$, $\widehat{P}_{\mathcal{C}}=(1/N_{\mathcal{C}})\sum_{i:\; X_i\in \mathcal{C}}\delta_{\Sigma_i}$ with $N_{\mathcal{C}}=\sum_{i}\mathbb{I}\{ X_i\in \mathcal{C} \}$ and the convention $0/0=0$, for any measurable set $\mathcal{C}$ s.t. $\mu(\mathcal{C})>0$.  Although we have $\widehat{\sigma}_{\mathcal{C}}=\sigma^*_{\widehat{P}_{\mathcal{C}}}$ if $\widehat{P}_{\mathcal{C}}\in \mathcal{T}$, the rule \eqref{eq:rule_theor} is somehow theoretical, since the way the Kemeny medians $\widehat{\sigma}_{\mathcal{C}}$ are obtained is not specified in general. Alternatively, using the notations of subsection \ref{subsec:consensus}, one may consider
\begin{equation}\label{eq:rule_pract}
\widetilde{s}_N(x)=\sum_{\mathcal{C}\in \mathcal{P}_N}\widetilde{\sigma}^*_{\widehat{P}_{\mathcal{C}}}\cdot \mathbb{I}\{x\in \mathcal{C}  \},
\end{equation}
which takes values that are not necessarily local empirical Kemeny medians but can always be easily computed. Observe incidentally that, for any $\mathcal{C}\in \mathcal{P}_N$ s.t. $\widehat{P}_{\mathcal{C}}\in \mathcal{T}$, $\widetilde{s}_N(x)=s_N(x)$ for all $x\in \mathcal{C}$. The theorem below establishes the consistency of these RMR rules in situations where the diameter of the cells of the data-dependent partition and their $\mu$-measure decay to zero but not too fast, w.r.t. the rate at which the quantity $\sqrt{N/\log (\Delta_n(\mathcal{F}_N))}$ increases. 
% and \mathcal{C}_N(x)$ is the (unique) cell of $\mathcal{P}_N$ containing $x$.
\begin{theorem}\label{th:partition_consist}
	Let $(\pi_1, \pi_2,\; \ldots)$ be a fixed sequence of partitioning rules  and for each $N$ let $\mathcal{F}_N$ be the collection of partitions associated with the $N-$sample partitioning rule $\pi_N$. Suppose that $P_x\in \mathcal{T}$ for all $x\in \X$ and that Assumption \ref{hyp:smooth} is satisfied. Assume also that the following conditions are fulfilled: $(i)$ 
		$\lim_{n \rightarrow \infty} \log(\Delta_N(\mathcal{F}_{N}))/N=0$, %where $\Delta_N(\mathcal{F}_{N})$ is the $N$-th shattering coefficient %of 
	%	For all $N\geq 1$, the family of subsets corresponding to cells of partitions in $\mathcal{F}_N$ is of finite {\sc VC} dimension $V_N<+\infty$,
		%$\pi_N$ is based upon at most $m_N-1$ splits, where $m_N=\mathcal{O}(N/\log N)$
		% relate depth of the tree with shattering coeff.
		$(ii)$ we have $\delta_{\mathcal{P}_N}\rightarrow 0$ in probability and $
		1/\kappa_N= o_{\mathbb{P}}(\sqrt{N/\log \Delta_N(\mathcal{F}_N) })$ as $N\rightarrow \infty$,
		where $
		\kappa_N=\inf\{ \mu(\mathcal{C}):\; \mathcal{C}\in \mathcal{P}_N\}$.
		%for all balls $S_B$ and all $\gamma>0$, $\lim_{n \rightarrow \infty} \mu(\left\{ x: diam (C_N(x) \cap S_M) > \gamma \right\})=0$,
Any RMR rule $s_N$ of the form \eqref{eq:rule_theor} is then consistent, \textit{i.e.} $\mathcal{R}(s_N)\rightarrow \mathcal{R}^*$ in probability as $N\rightarrow \infty$.
Suppose in addition that Assumption \ref{hyp:margin} is satisfied. Then, the RMR rule  $\widetilde{s}_N(x)$ given by $\eqref{eq:rule_pract}$ is also consistent.
\end{theorem}

%\begin{remark}\label{rk:alternative}
%As shown in the Appendix (see Theorem \ref{th:partition_consist2}), this last condition can be avoided when supposing in addition that Assumption \ref{hyp:margin} is fulfilled.
%\end{remark}
In the Appendix, two approaches for building a partition $\mathcal{P}$ of the input space are presented: a version of the nearest neighbor methods tailored to ranking median regression first and a second algorithm that constructs $\mathcal{P}$ recursively depending on the local variability of the $\Sigma_i$'s similarly as the CART algorithm.

 %Consistency of the {\sc CRIT} algorithm can be established following in the footsteps of the argument of Theorem 21.2 in \citet{DGL96}, which applies to rules that partition the space and decide by majority vote, and its corollary Theorem 21.8 which applies to binary search trees. 
 
 
 