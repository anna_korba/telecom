
\section{Two Data-Based Partitioning RMR Algorithms}
\label{sec:appendix_algos}

Two approaches for building a partition $\mathcal{P}$ of the predictor variable space in a data-driven fashion are proposed here. The first method is a version of the nearest neighbor methods tailored to ranking median regression, whereas the second algorithm constructs $\mathcal{P}$ recursively, depending on the local variability of the $\Sigma_i$'s, and scales with the dimension of the input space.

\subsection{Nearest-Neighbor Rules for Ranking Median Regression}\label{subsec:algoNN}
\begin{figure}[!h]
	
	\begin{center}
		\fbox{
			\begin{minipage}[t]{13cm}
				\medskip
				
				\begin{center}
					{\sc Ranking Median Regression: the $k$-NN Algorithm}
				\end{center}
				
				\medskip
				{\small
					
					\begin{enumerate}
						\item[] {\bf Inputs.} Training dataset $\mathcal{D}_N=\{(X_1,\Sigma_1),\; \ldots,\, (X_N,\Sigma_N)  \}$. Norm $\vert\vert .\vert\vert$ on the input space $\mathcal{X}\subset \mathbb{R}^d$. Number $k\in \{1,\; \ldots,\; N  \}$ of neighbours. Query point $x\in \mathcal{X}$.
						\item ({\sc Sort.}) Sort the training points by increasing order of distance to $x$:
						$$
						\| X_{(1,N)} - x \|\leq \ldots\leq  \| X_{(N,N)} - x \|.
						$$
						
						\medskip
						
						\item ({\sc Estimation/approximation.}) Compute the marginal empirical distribution based on the $k$-nearest neighbors in the input space:
						$$
						\widehat{P}(x)=\frac{1}{k}\sum_{l=1}^k\delta_{\Sigma_{(k,N)}}
						$$
						\medskip
						
						\item[] {\bf Output.} Compute the local consensus in order to get the prediction at $x$:
						$$
						s_{k,N}(x)=\widetilde{\sigma}^*_{\widehat{P}(x)}.
						$$
						\medskip
					\end{enumerate}
				}
			\end{minipage}
		}
		\label{fig:trpseudoknn}
		\caption{Pseudo-code for the $k$-NN algorithm.}
	\end{center}
\end{figure}

\noindent Fix $k\in\{1,\; \ldots,\; N  \}$ and a query point $x \in \mathcal{X}$. The $k$-nearest neighbor RMR rule prediction $s_{k,N}(x)$ (summarized in the pseudocode~\ref{fig:trpseudoknn}) is obtained as follows. Sort the training data $(X_1, \Sigma_1), \dots, (X_N, \Sigma_N)$ by increasing order of the distance to $x$, measured, for simplicity, by $\| X_i - x \|$ for a certain norm chosen on $\mathcal{X}\subset \mathbb{R}^d$ say: 
% \begin{equation*}
$ \| X_{(1,N)} - x \|\leq \ldots\leq  \| X_{(N,N)} - x \|$.
 %\end{equation*}
 Consider next the empirical distribution calculated using the $k$ training points closest to $x$
 \begin{equation}\label{eq:local_distr}
 \widehat{P}(x)=\frac{1}{k}\sum_{l=1}^k\delta_{\Sigma_{(l,N)}}
 \end{equation}
 and then set
  \begin{equation}\label{eq:k_NNtheo}
 %s_{N}(x)=\argmin_{\sigma \in \mathfrak{S}_n} \sum_{l=1}^{k_N} d_{\tau}(\sigma, \Sigma_{(l,N)}(x))
 s_{k,N}(x)=\sigma_{\widehat{P}(x)},
 %=\sigma_{\widetilde{p}(x)}^*,
 \end{equation}
where $\sigma_{\widehat{P}(x)}$ is a Kemeny median of distribution \eqref{eq:local_distr}. Alternatively, one may compute next the pseudo-empirical Kemeny median, as described in subsection \ref{subsec:consensus}, yielding
 %$$
 %\widetilde{p}(x)= \argmin_{p'': G_{p''} acyclic}\sum_{i<j}\left(\widehat{p}_{i,j}(x)-p^{''}_{i,j}(x)\right)^2.
 %$$
 the $k$-NN prediction at $x$:
 \begin{equation}\label{eq:k_NNpract}
 %s_{N}(x)=\argmin_{\sigma \in \mathfrak{S}_n} \sum_{l=1}^{k_N} d_{\tau}(\sigma, \Sigma_{(l,N)}(x))
 \widetilde{s}_{k,N}(x)=\widetilde{\sigma}^*_{\widehat{P}(x)}.
 %=\sigma_{\widetilde{p}(x)}^*.
 \end{equation}
 Observe incidentally that $ s_{k,N}(x)= \widetilde{s}_{k,N}(x)$ when $\widehat{P}(x)$ is strictly stochastically transitive. The result stated below provides an upper bound for the expected risk excess of the RMR rules \eqref{eq:k_NNtheo} and \eqref{eq:k_NNpract}, which reflects the usual bias/variance trade-off ruled by $k$ for fixed $N$ and asymptotically vanishes as soon as $k\rightarrow \infty$ as $N\rightarrow \infty$ such that $k=o(N)$. Notice incidentally that the choice $k\sim N^{2/(d+2)}$ yields the asymptotically optimal upper bound, of order $N^{-1/(2+d)}$.

 \begin{theorem}\label{thm:NN_consist}
 Suppose that Assumption \ref{hyp:smooth} is fulfilled, that the r.v. $X$ is bounded and $d\ge 3$. Then, we have: $\forall N\geq 1$, $\forall k\in\{1,\; \ldots,\; N  \}$,
 \begin{multline}
 %\mathbb{E}\left[  \mathcal{R}(s_{k,N})-\mathcal{R}^*  \right]\leq 2 \left(\frac{n(n-1)}{2}\right)^{3/2} \left( 1/(2\sqrt{k}) + \sqrt{c_1}M \left(k/N\right)^{1/d}\right)\\ +\frac{(n(n-1))}{2}\frac{\sqrt{c_2}M}{H}\frac{1}{(N-k)^{1/d}},\\
 \mathbb{E}\left[  \mathcal{R}(s_{k,N})-\mathcal{R}^*  \right]\leq \frac{n(n-1)}{2} \left( \frac{1}{\sqrt{k}} + 2\sqrt{c_1}M \left(\frac{k}{N}\right)^{1/d} \right)\\
 \end{multline}
 where $c_1$ is a constant which only depends on $\mu$'s support.
 
 Suppose in addition that Assumption \ref{hyp:margin} is satisfied. We then have: $\forall N\geq 1$, $\forall k\in\{1,\; \ldots,\; N  \}$,
 \begin{multline}
 %\mathbb{E}\left[  \mathcal{R}(s_{k,N})-\mathcal{R}^*  \right]\leq 2 \left(\frac{n(n-1)}{2}\right)^{3/2} \left( 1/(2\sqrt{k}) + \sqrt{c_1}M \left(k/N\right)^{1/d}\right)\\ +\frac{(n(n-1))}{2}\frac{\sqrt{c_2}M}{H}\frac{1}{(N-k)^{1/d}},\\
 \mathbb{E}\left[  \mathcal{R}( \widetilde{s}_{k,N})-\mathcal{R}^*  \right]\leq \frac{n(n-1)}{2} \left( \frac{1}{\sqrt{k}} + 2\sqrt{c_1}M \left(\frac{k}{N}\right)^{1/d}\right)\left(1+n(n-1)/(4H)  \right).
 \end{multline}
 \end{theorem}
 Refer to the Appendix~\ref{sec:appendix_proofs} for the technical proof. In addition, for $d\le2$ the rate stated in Theorem \ref{thm:NN_consist} still holds true, under additional conditions on $\mu$, see also the Appendix~\ref{sec:appendix_proofs} for further details. In practice, as for nearest-neighbor methods in classification/regression, the success of the technique above for fixed $N$ highly depends on the number $k$ of neighbors involved in the computation of the local prediction. The latter can be picked by means of classic model selection methods, based on data segmentation/resampling techniques. It may also crucially depend on the distance chosen (which could be learned from the data as well, see \textit{e.g.} \citet{BHS14}) and/or appropriate preprocessing stages, see \textit{e.g.} the discussion in chapter 13 of \citet{FHTbook}). The implementation of this simple local method for ranking median regression does not require to explicit the underlying partition but is classically confronted with the curse of dimensionality. The next subsection explains how another local method, based on the popular tree induction heuristic, scales with the dimension of the input space by contrast. Due to space limitations, extensions of other data-dependent partitioning methods, such as those investigated in Chapter 21 of \citet{DGL96} for instance, to local RMR are left to the reader.

\subsection{Recursive Partitioning - The {\sc CRIT} algorithm}\label{subsec:algo}

We now describe an iterative scheme for building an appropriate tree-structured partition $\mathcal{P}$, adaptively from the training data. Whereas the splitting criterion in most recursive partitioning methods is heuristically motivated (see \citet{friedman}),
the local learning method we describe below relies on the Empirical Risk Minimization principle formulated in Section \ref{sec:rmr}, so as to build by refinement a partition $\mathcal{P}$  based on a training sample $\mathcal{D}_N=\{(\Sigma_1,\; X_1),\; \ldots,\; (\Sigma_N,\; X_N)  \}$ so that, on each cell $\mathcal{C}$ of $\mathcal{P}$, the $\Sigma_i$'s lying in it exhibit a small variability in the Kendall $\tau$ sense and, consequently, may be accurately approximated by a local Kemeny median. As shown below, the local variability measure we consider can be connected to the local ranking median regression risk (see Eq. \eqref{eq:double}) and leads to exactly the same node impurity measure as in the tree induction method proposed in \citet{YWL10}, see Remark~\ref{rk:related}. The algorithm described below differs from it in the method we use to compute the local predictions. More precisely, the goal pursued is to construct recursively a piecewise constant ranking rule associated to a partition $\mathcal{P}$, $s_{\mathcal{P}}(x)=\sum_{\mathcal{C}\in \mathcal{P}}\sigma_{\mathcal{C}}\cdot \mathbb{I}\{x\in \mathcal{C}  \}$, with minimum empirical risk
\begin{equation}\label{eq:train1}
\widehat{R}_N(s_{\mathcal{P}})=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})L_{\widehat{P}_{\mathcal{C}}}(\sigma_{\mathcal{C}}),
\end{equation}
where $\widehat{\mu}_N=(1/N)\sum_{k=1}^N\delta_{X_k}$ is the empirical measure of the $X_k$'s. The partition $\mathcal{P}$ being fixed, as noticed in Proposition \ref{prop:opt_subclass}, the quantity \eqref{eq:train1} is minimum when $\sigma_{\mathcal{C}}$ is a Kemeny median of $\widehat{P}_{\mathcal{C}}$ for all $\mathcal{C}\in \mathcal{P}$. It is then equal to
\begin{equation}\label{eq:crit_exp1}
\min_{s\in \mathcal{S}_{\mathcal{P}}}\widehat{R}_N(s)=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})L^*_{\widehat{P}_{\mathcal{C}}}.
\end{equation}
Except in the case where the intra-cell empirical distributions $\widehat{P}_{\mathcal{C}}$'s are all stochastically transitive (each $L^*_{\widehat{P}_{\mathcal{C}}}$ can be then computed using formula \eqref{eq:inf}), computing \eqref{eq:crit_exp1} at each recursion of the algorithm can be very expensive, since it involves the computation of a Kemeny median within each cell $\mathcal{C}$. We propose to measure instead the accuracy of the current partition by the quantity
\begin{equation}\label{eq:crit_exp2}
\widehat{\gamma}_{\mathcal{P}}=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}}},
\end{equation}
which satisfies the double inequality (see Remark \ref{rk:dispersion})
\begin{equation}\label{eq:double}
\widehat{\gamma}_{\mathcal{P}}\leq \min_{s\in \mathcal{S}_{\mathcal{P}}}\widehat{R}_N(s)\leq 2\widehat{\gamma}_{\mathcal{P}},
\end{equation}
and whose computation is straightforward: $\forall \mathcal{C}\in \mathcal{P}$,
\begin{equation}\label{eq:crit_exp3}
\gamma_{\widehat{P}_{\mathcal{C}}}=\frac{1}{2}\sum_{i<j}\widehat{p}_{i,j}(\mathcal{C})\left(1-\widehat{p}_{i,j}(\mathcal{C})  \right),
\end{equation}
where $\widehat{p}_{i,j}(\mathcal{C})=(1/N_{\mathcal{C}})\sum_{k:\; X_k\in \mathcal{C}}\mathbb{I}\{\Sigma_k(i)<\Sigma_k(j)  \}$, $i<j$,
denote the local pairwise empirical probabilities, with $N_{\mathcal{C}}=\sum_{k=1}^{N}\mathbb{I}\{ X_k \in \mathcal{C}\}$.
%For computational reasons (in order to avoid costly median computations at each recursive step), variability is measured through the criterion $\gamma$, rather than $L^*$, see Remark \ref{rk:dispersion}. 
%For any measurable subset $\mathcal{C}\subset \mathcal{X}$, the local empirical dispersion measure considered is
%\begin{eqnarray}
%\widehat{\gamma}(\mathcal{C})=\sum_{i<j}\widehat{p}_{i,j}(\mathcal{C})\left(1- \widehat{p}_{i,j}(\mathcal{C})  \right),
%\end{eqnarray}
%where, for all $i<j$, we set
%\begin{equation}\label{eq:p_hat}
%\widehat{p}_{i,j}(\mathcal{C})=\frac{1}{N_{\mathcal{C}}}\sum_{k=1}^N\mathbb{I}\{ X_k\in \mathcal{C},\; \Sigma_k(i)<\Sigma_k(j)   \},
%\end{equation}
%with $N_{\mathcal{C}}=\sum_{k=1}^N \mathbb{I}\{ X_k\in \mathcal{C}\}$.
%The quantity \eqref{eq:p_hat} is an empirical estimate of the dispersion measure of $\Sigma$ given $X\in \mathcal{C}$, namely $\gamma(P_{\mathcal{C}})$, see Remark \ref{rk:dispersion}.
%More precisely, we propose to solve the median ranking regression problem by means of a recursive method in the spirit of the {\sc CART} algorithm for regression, we call the {\sc CRIT} algorithm (standing for {\sc Consensus RankIng Tree}), that produces a binary tree structured partition $\mathcal{P}$ of the input space (and, consequently, a piecewise constant ranking rule, see subsection \ref{subsec:piece}) that minimizes the quantity
%\begin{equation}
%N(N-1)\widehat{\gamma}_{\mathcal{P}}= \sum_{\mathcal{C}\in \mathcal{P}}\sum_{1\leq k<l\leq N}\mathbb{I}\{ (X_k,X_l)\in \mathcal{C}^2 \}\cdot d_{\tau}\left( \Sigma_k,\Sigma_l \right),
%\end{equation}
%where $\widehat{\gamma}_{\mathcal{P}}$ is the natural empirical estimate of the intra-cell dispersion measure
%\begin{equation}
%\gamma_{\mathcal{\mathcal{P}}}\overset{def}{=}\frac 1 2 \sum_{\mathcal{C}\in \mathcal{P}}\mathbb{E}\left[d_{\tau}(\Sigma,\Sigma')\cdot \mathbb{I}\{ (X,X')\in \mathcal{C}^2 \}  \right]= \frac 1 2\sum_{\mathcal{C}\in \mathcal{P}} \mu(\mathcal{C})^2\gamma(P_{\mathcal{C}}).
%\end{equation}
 A ranking median regression tree of maximal depth $J\geq 0$ is grown as follows. One starts from the root node $\mathcal{C}_{0,0}=\mathcal{X}$. 
 %and considers as initial median ranking regression rule $s_0$ any empirical ranking median $\sigma_{0,0}$ of the observations $\Sigma_1,\; \ldots,\; \Sigma_N$. 
 At depth level $0\leq j <J$, any cell $\mathcal{C}_{j,k}$, $0 \leq k <2^j$ shall be split into two (disjoint) subsets $\mathcal{C}_{j+1,2k}$ and $\mathcal{C}_{j+1,2k+1}$, respectively identified as the left and right children of the interior leaf $(j,k)$ of the ranking median regression tree, according to the following \textit{splitting rule}.\\
 
 \noindent {\bf Splitting rule.} For any candidate left child $\mathcal{C}\subset \mathcal{C}_{j,k}$, picked in a class $\mathcal{G}$ of 'admissible' subsets (see Remark \ref{rk:perp_split}, Appendix~\ref{sec:appendix_crit}), the relevance of the split $\mathcal{C}_{j,k}=\mathcal{C}\cup (\mathcal{C}_{j,k}\setminus\mathcal{C})$ is naturally evaluated through the quantity:
 \begin{equation}
  \Lambda_{j,k}(\mathcal{C})\overset{def}{=}
  \widehat{\mu}_N(\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}}}+\widehat{\mu}_N(\mathcal{C}_{j,k}\setminus\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}_{j,k}\setminus\mathcal{C}}}.
  %=\widehat{\gamma}(\mathcal{C}) + \widehat{\gamma}(\mathcal{C}_{j,k}\setminus \mathcal{C}).
 \end{equation}
 The determination of the splitting thus consists in computing a solution $\mathcal{C}_{j+1,2k}$ of the optimization problem
 \begin{equation}\label{eq:min_pb}
 \min_{\mathcal{C}\in  \mathcal{G},\; \mathcal{C}\subset \mathcal{C}_{j,k}}\Lambda_{j,k}(\mathcal{C})
 \end{equation}
   As explained in the Appendix~\ref{sec:appendix_crit}, an appropriate choice for class $\mathcal{G}$ permits to solve exactly the optimization problem very efficiently, in a greedy fashion.\\
   
   \noindent {\bf Local medians.} The consensus ranking regression tree is grown until depth $J$ and on each terminal leave $\mathcal{C}_{J,l}$, $0\leq l<2^J$, one computes the local Kemeny median estimate by means of the best strictly stochastically transitive approximation method investigated in subsection \ref{subsec:consensus}
   \begin{equation}
 \sigma^*_{J,l}\overset{def}{=}  \widetilde{\sigma}^*_{\widehat{P}_{\mathcal{C}_{J,l}}}.
   \end{equation}
%where $\widehat{P}_{\mathcal{C}}=(1/N_{\mathcal{C}})\sum_{l=1}^N\mathbb{I}\{  X_l\in \mathcal{C}\}\cdot\delta_{\Sigma_l}$ and $N_{\mathcal{C}}=\sum_{k=1}^N \mathbb{I}\{ X_k\in \mathcal{C}\}$ for all measurable $\mathcal{C}\subset \mathcal{X}$. 
If $\widehat{P}_{\mathcal{C}_{J,l}}\in \mathcal{T}$, $\sigma^*_{J,l}$ is straightforwardly obtained from formula \eqref{eq:sol_SST} and otherwise, one uses 
%the regression technique proposed in \citet{JLYY10}, 
the pseudo-empirical Kemeny median described in subsection \ref{subsec:consensus}. The ranking median regression rule related to the binary tree $T_{2^J}$ thus constructed is given by:
\begin{equation}
s^*_{T_{2^J}}(x)=\sum_{l=0}^{2^J-1}\sigma^*_{J,l}\mathbb{I}\{ x\in \mathcal{C}_{J,l} \}.
\end{equation}
 Its training prediction error is equal to $ \widehat{L}_N(s^*_{T_{2^J}})$,
%\begin{equation}
 %\widehat{L}_N(s^*_D)=\sum_{l=0}^{2^D-1}\widehat{\mu}_N(\mathcal{C}_{D,l})L^*_{\widehat{P}_{\mathcal{C}_{D,l}}},
 %\end{equation}
% where $\widehat{\mu}_N=(1/N)\sum_{k=1}^N\delta_{X_i}$ denotes the empirical version of $X$'s marginal distribution, 
while the training accuracy measure of the final partition is given by
 \begin{equation}
 \widehat{\gamma}_{T_{2^J}}=\sum_{l=0}^{2^J-1}\widehat{\mu}_N(\mathcal{C}_{J,l})\gamma_{\widehat{P}_{\mathcal{C}_{J,l}}}.
 %\sum_{l=0}^{2^{J-1}-1}\Lambda_{J-1,l}(\mathcal{C}_{J, 2l}).
 \end{equation}
 
 \begin{remark}\label{rk:related}
 We point out that the impurity measure \eqref{eq:crit_exp2} corresponds (up to a constant factor) to that considered in \citet{YWL10}, where it is referred to as the pairwise Gini criterion. Borrowing their notation, one may indeed write: for any measurable $\mathcal{C}\subset \mathcal{X}$, 
  $i_w^{(2)}(\mathcal{C})=8/(n(n-1))\times  \gamma_{\widehat{P}_{\mathcal{C}}}$.
 \end{remark}
 
 \noindent The tree growing stage is summarized in the pseudocode~\ref{fig:trpseudocrit}. The pruning procedure generally following it to avoid overfitting, as well as additional comments on the advantages of this method regarding interpretability and computational feasibility can also be found in Appendix~\ref{sec:appendix_crit}, together with a preliminary analysis of a specific bootstrap aggregation technique that can remedy the instability of such a hierarchical predictive method. 
 
 
 
% \subsection*{The {\sc CRIT} algorithm}
 
 \begin{figure}[!h]
 	
 	\begin{center}
 		\fbox{
 			\begin{minipage}[t]{13cm}
 				\medskip
 				
 				\begin{center}
 					{\sc The CRIT Algorithm}
 				\end{center}
 				
 				\medskip
 				{\small
 					
 					\begin{enumerate}
 						\item[] {\bf Inputs.} Training dataset $\mathcal{D}_N=\{(X_1,\Sigma_1),\; \ldots,\, (X_N,\Sigma_N)  \}$. Depth $J\geq 0$. Class of admissible subsets $\mathcal{G}$.
 						\item ({\sc Initialization.}) Set $C_{0,0}=\mathcal{X}$.
 						
 						\medskip
 						
 						\item ({\sc Iterations.}) For $j=0,\;\ldots,\;J-1$ and $k=0,\;\ldots,\; 2^{j}-1$:
 						
 						\medskip
 						
 						Solve
 						\[
 						\min_{\mathcal{C}\in  \mathcal{G},\; \mathcal{C}\subset \mathcal{C}_{j,k}}\Lambda_{j,k}(\mathcal{C}),
 						\]
 						yielding the  region $\mathcal{C}_{j+1,2k}$.
 						Then, set $\mathcal{C}_{j+1,2k+1}=\mathcal{C}_{j,k} \setminus \mathcal{C}_{j+1,2k}$.
 						
 						
 						
 						\medskip
 						
 						\item ({\sc Local consensus.}) After $2^J$ iterations, for each terminal cell $\mathcal{C}_{J,k}$ with $k\in\{0,\; \ldots,\; 2^J-1\}$, compute the Kemeny median estimate $\sigma^*_{J,k}=\widetilde{\sigma}^*_{\widehat{P}_{\mathcal{C}_{J,k}}}$ .
 						%of the best approximation in $\mathcal{T}$ of the empirical distribution $\widehat{P}_{\mathcal{C}_{J,k}}$.
 						\item[] {\bf Outputs.} Compute the piecewise constant ranking median regression rule:
 						$$
 						s^*_{T_{2^J}}(x)=\sum_{l=0}^{2^J-1}\sigma^*_{J,l}\cdot \mathbb{I}\{ x\in \mathcal{C}_{J,l} \}.
 						$$
 					\end{enumerate}
 					\medskip
 					
 				}
 			\end{minipage}
 		}
 		\label{fig:trpseudocrit}
 		\caption{Pseudo-code for the {\sc CRIT} algorithm.}
 	\end{center}
 \end{figure}
 
 

 \subsection{Numerical Experiments}



%\section{Numerical Experiments}\label{sec:num}
For illustration purpose, experimental results based on simulated/real data are displayed.

\medskip

\begin{sidewaystable}[ph!]
	\begin{tabular}{|c |ccc | ccc |ccc |} 
		\hline
		\multirow{2}{3.5cm}{Dataset distribution}&\multicolumn{3}{| c |}{Setting 1}& \multicolumn{3}{| c |}{Setting 2} & \multicolumn{3}{| c |}{Setting 3}\\
		&n=3 & n=5 & n=8 & n=3 & n=5 & n=8 &n=3 & n=5 & n=8 \\
		\hline
		\multirow{3}{3.5cm}{Piecewise constant}& 0.0698* & 0.1290* & 0.2670* & 0.0173* & 0.0405* & 0.110* & 0.0112* & 0.0372* & 0.0862*\\ 
		& 0.0473**   & 0.136** & 0.324** & 0.0568** & 0.145** &0.2695**& 0.099** & 0.1331** & 0.2188**\\ 
		& (0.578)   & (1.147) & (2.347) & (0.596) & (1.475) &(3.223)& (0.5012) & (1.104) & (2.332)\\ 
		
		\hline
		\multirow{3}{3.5cm}{Mallows with $\phi$=2}& 0.3475
		* & 0.569* & 0.9405
		* & 0.306* & 0.494* & 0.784* & 0.289* & 0.457* & 0.668*\\ 
		& 0.307** & 0.529** & 0.921** &  0.308** & 0.536** &0.862**& 0.3374** & 0.5714** & 0.8544**\\ 
		& (0.719) & (1.349) & (2.606) & (0.727) & (1.634) &(3.424)& (0.5254) & (1.138) & (2.287)\\ 
		\hline
		\multirow{3}{3.5cm}{Mallows with $\phi$=1}& 0.8656* & 1.522* & 2.503* & 0.8305
		* & 1.447
		* & 2.359* & 0.8105* & 1.437* & 2.189*\\ 
		& 0.7228** & 1.322** & 2.226**& 0.723** & 1.3305** & 2.163**& 0.7312** & 1.3237** & 2.252**\\ 
		& (0.981) & (1.865) & (3.443) & (1.014) & (2.0945) &(4.086)& (0.8504) & (1.709) & (3.005)\\  
		\hline
	\end{tabular}
	\caption{Empirical risk averaged on 50 trials on simulated data.}
	\label{table:simulated_data}
\end{sidewaystable}




\noindent \textbf{Results on simulated data.} Here, datasets of full rankings on $n$ items are generated according to two explanatory variables. We carried out several experiments by varying the number of items ($n=3,5,8$) and the nature of the features. In Setting 1, both features are numerical; in Setting 2, one is numerical and the other categorical, while, in Setting 3, both are categorical. For a fixed setting, a partition $\mathcal{P}$ of $\mathcal{X}$ composed of $K$ cells $\mathcal{C}_1, \dots, \mathcal{C}_K$ is fixed. In each trial, $K$ permutations $\sigma_1, \dots, \sigma_K$ (which can be arbitrarily close) are  generated, as well as three datasets of $N$ samples, where on each cell $\mathcal{C}_k$: the first one is constant (all samples are equal to $\sigma_k$), and the two others are noisy versions of the first one, where the samples follow a Mallows distribution (see \citet{Mallows57}) centered on $\sigma_k$ with dispersion parameter $\phi$. We recall that the greater the dispersion parameter $\phi$, the spikiest the distribution (and closest to piecewise constant). We choose $K$=6 and $N$=1000. In each trial, the dataset is divided into a training set ($70\%$) and a test set ($30\%$). Concerning the CRIT algorithm, since the true partition is known and is of depth 3, the maximum depth is set to 3 and the minimum size in a leaf is set to the number of samples in the training set divided by 10. For the k-NN algorithm, the number of neighbors $k$ is fixed to 5. The baseline model to which we compare our algorithms is the following: on the train set, we fit a K-means (with $K$=6), train a Plackett-Luce model on each cluster and assign the mode of this learnt distribution as the center ranking of the cluster.
For each configuration (number of items, characteristics of feature and distribution of the dataset), the empirical risk  (see \ref{sec:rmr}, denoted as $\widehat{\mathcal{R}}_N(s)$) is averaged on 50 repetitions of the experiment. Results of the k-NN algorithm (indicated with a star *), of the CRIT algorithm (indicated with two stars **) and of the baseline model (between parenthesis) on the various configurations are provided in Table~\ref{table:simulated_data}. They show that the methods we develop recover the true partition of the data, insofar as the underlying distribution can be well approximated by a piecewise constant function ($\phi \ge 2$ for instance in our simulations).

\medskip

\noindent \textbf{Analysis of GSS data on job value preferences.} We test our algorithm on the full rankings dataset which was obtained by the US General Social Survey (GSS) and which is already used in \citet{AY14}. This multidimensional survey collects across years socio-demographic attributes and answers of respondents to numerous questions, including societal opinions. In particular, participants were asked to rank in order of preference five aspects about a job: "high income", "no danger of being fired", "short working hours", "chances for advancement", and "work important and gives a feeling of accomplishment". The dataset we consider contains answers collected between 1973 and 2014. As in \citet{AY14}, for each individual, we consider eight individual attributes (sex, race, birth cohort, highest educational degree attained, family income, marital status, number of children that the respondent ever had, and household size) and three properties of work conditions (working status, employment status, and occupation). After preprocessing, the full dataset contains $18544$ samples. We average the results of our algorithms over 10 experiments: each time, a bootstrap sample of size 1000 is drawn, then randomly divided in a training set ($70\%$) and a test set ($30\%$), and the model is trained on the training set and evaluated on the test set. The results are stable among the experiments. Concerning the k-NN algorithm, we obtain an average empirical risk of $2.842$ (for the best $k=22$). For the CRIT algorithm, we obtain an average empirical risk of $2.763$ (recall that the maximum Kendall distance is 10) and splits coherent with the analysis in \citet{AY14}: the first splitting variable is occupation (managerial, professional, sales workers and related vs services, natural resources, production, construction and transportation occupations), then at the second level the race is the most important factor in both groups (black respondents vs others in the first group, white respondents vs others in the second group). At the lower level the degree obtained seems to play an important role  (higher than high school, or higher than bachelor's for example in some groups); then other discriminating variables among lower levels are birth cohort, family income or working status. 


% Previous studies (see for instance \citet{johnson2002social}) have shown that the difference of appreciation of such characteristics come as a result of a socialization process during the individual's life. 
% Moreover, if we drop the variable "short working hour", so that the number of items drops to $n=4$ (and maximal Kendall distance to 8), the average empirical risk is 1.91.
%As in \citet{AY14}, given the limited significance of JOBHOUR, we remove it and the full rankings are now considered on $n=4$ items.