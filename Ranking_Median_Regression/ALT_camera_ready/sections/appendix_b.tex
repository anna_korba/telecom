\section{Technical Proofs and Additional Remarks}
\label{sec:appendix_proofs}

\subsection{Preliminaries}

\subsection*{Proof of Lemma \ref{lem:bounds}}
Observe first that: $\forall \sigma \in \mathfrak{S}_n$,
\begin{equation}
L_{P'}(\sigma)=\sum_{i<j} p'_{i,j}\mathbb{I}\{\sigma(i)>\sigma(j) \} +\sum_{i<j} (1-p'_{i,j})\mathbb{I}\{\sigma(i)<\sigma(j) \}.
\end{equation}
We deduce from the equality above, applied twice, that: $\forall \sigma \in \mathfrak{S}_n$,
\begin{equation}
\left\vert  L_{P'}(\sigma))-L_{P''}(\sigma)\right\vert \leq \sum_{i<j}\left\vert p'_{i,j}-p''_{i,j} \right\vert .
\end{equation}
Hence, we may write:
\begin{multline*}
L^*_{P'}=\inf_{\sigma\in \mathfrak{S}_n}L_{P'}(\sigma)\leq L_{P'}(\sigma_{P''})=L_{P''}(\sigma_{P''})+ \left(L_{P'}(\sigma_{P''})-L_{P''}(\sigma_{P''})\right)\\
\leq L^*_{P''}+\sum_{i<j}\left\vert p'_{i,j}-p''_{i,j} \right\vert.
\end{multline*}
In a similar fashion, we have $L^*_{P''}\leq L^*_{P'}+\sum_{i<j}\vert p'_{i,j}-p''_{i,j} \vert$, which yields assertion (i) when combined with the inequality above.
 
We turn to (ii) and assume now that both $P'$ and $P''$ belong to $\mathcal{T}$. Let $i<j$. Suppose that $\sigma^*_{P'}(i)<\sigma^*_{P'}(j)$ and $\sigma^*_{P''}(i)>\sigma^*_{P''}(j)$. In this case, we have $p'_{i,j}>1/2$ and $p''_{i,j}<1/2$, so that 
$$
\vert p'_{i,j}-p''_{i,j}\vert /h= \left(p'_{i,j}-1/2\right)/h + \left(1/2-p''_{i,j} \right)/h\geq 1.$$
More generally, we have 
$$
\mathbb{I}\left\{\left(\sigma^*_{P'}(i)-\sigma^*_{P'}(j)\right)  \left(\sigma^*_{P'}(i)-\sigma^*_{P'}(j)\right)  <0 \right\}\leq \vert p'_{i,j}-p''_{i,j}\vert /h
$$ for all $i<j$. Summing over the pairs $(i,j)$ establishes assertion (ii).

\bigskip
%\noindent{\bf Connection with the Wasserstein metric \eqref{eq:metric}.}

\subsection*{On Mass Transportation and Kemeny Median Approximation}

We point out that the quantity $\sum_{i<j}\vert p'_{i,j}-p''_{i,j} \vert$ involved in Lemma \ref{lem:bounds} can be interpreted as a Wasserstein metric with Kendall $\tau$ distance as cost function. Indeed, a natural way of measuring the distance between two probability distributions $P$ and $P'$ on $\mathfrak{S}_n$ is to compute the quantity
\begin{equation} \label{eq:metric}
D_{\tau}\left(P,P'  \right)=\inf_{\Sigma\sim P,\; \Sigma' \sim P' }\mathbb{E}\left[ d_{\tau}(\Sigma,\Sigma') \right],
\end{equation}
where the infimum is taken over all possible couplings\footnote{Recall that a coupling of two probability distributions $Q$ and $Q'$ is a pair $(U,U')$ of random variables defined on the same probability space such that the marginal distributions of $U$ and $U'$ are $Q$ and $Q'$.} $(\Sigma,\Sigma')$ of $(P,P')$, see \textit{e.g.} \citet{CJ10}. As can be shown by means of a straightforward computation (see the details below), we have:
$
D_{\tau}(P,P' )=\sum_{i<j}\vert p'_{i,j}-p_{i,j} \vert$.
Observe that, equipped with this notation, we have: $\forall \sigma\in \mathfrak{S}_n$,
$L_{P}(\sigma)=D_{\tau}\left(P,\delta_{\sigma}  \right)$.
Hence, the Kemeny medians $\sigma^*$ of a probability distribution $P$ correspond to the Dirac distributions $\delta_{\sigma^*}$ closest to $P$ in the sense of the Wasserstein metric \eqref{eq:metric}. 

\medskip

\noindent {\bf Proof of Eq. \eqref{eq:metric}.}
 Consider a coupling $(\Sigma,\Sigma')$ of two probability distributions $P$ and $P'$ on $\mathfrak{S}_n$. For all $i\neq j$, set
$$
\pi_{i,j}=\mathbb{P}\left\{ \Sigma'(i)<\Sigma'(j)\mid  \Sigma(i)<\Sigma(j) \right\}.
$$
Equipped with this notation, by the law of total probability, we have: for all $i\neq j$,
\begin{equation}\label{eq:margin}
p'_{i,j}=p_{i,j}\pi_{i,j}+(1-p_{i,j})(1-\pi_{j,i}).
\end{equation}
In addition, we may write
$$
\mathbb{E}\left[ d_{\tau}\left( \Sigma,\Sigma' \right)\right]=\sum_{i<j}\left\{ p_{i,j}(1-\pi_{i,j})+(1-p_{i,j})(1-\pi_{j,i})  \right\}.
$$
Fix $i<j$. Suppose that $p_{i,j}<p'_{i,j}$. Using \eqref{eq:margin}, we have $p_{i,j}(1-\pi_{i,j})+(1-p_{i,j})(1-\pi_{j,i})=p'_{i,j}+(1-2\pi_{i,j})p_{i,j}$, which quantity is minimum when $\pi_{i,j}=1$ (and in this case $\pi_{j,i}=(1-p'_{i,j})/(1-p_{i,j})$), and then equal to $\vert p_{i,j}-p'_{i,j}\vert$. In a similar fashion, if $p_{i,j}>p'_{i,j}$, we have $p_{i,j}(1-\pi_{i,j})+(1-p_{i,j})(1-\pi_{j,i})=2(1-p_{i,j})(1-\pi_{j,i})+p_{i,j}-p'_{i,j}$, which is minimum for $\pi_{j,i}=1$ (we have incidentally $\pi_{i,j}=p'_{i,j}/p_{i,j}$ in this case) and then equal to $\vert p_{i,j}-p'_{i,j}\vert$. This proves that
$$
D_{\tau}(P,P' )=\sum_{i<j}\vert p'_{i,j}-p_{i,j} \vert.
$$
%\bigskip

\subsection{Ranking Median Regression}

\subsection*{Proof of Proposition \ref{prop:upper_bound}}

First, observe that, using the definition of empirical risk minimizers and the union bound, we have with probability one: $\forall N\geq 1$,
\begin{multline*}
\mathcal{R}(\widehat{s}_N)-\mathcal{R}^*\leq 2\sup_{s\in \mathcal{S}_0}\left\vert \widehat{\mathcal{R}}_N(s)-\mathcal{R}(s) \right\vert +\left\{\inf_{s\in \mathcal{S}_0}\mathcal{R}(s)-\mathcal{R}^*\right\}\\
\leq 2\sum_{i<j}\sup_{s\in \mathcal{S}_0}\left\vert \frac{1}{N}\sum_{k=1}^N \mathbb{I}\left\{\left(\Sigma_k(i)-\Sigma_k(j)\right) \left(s(X_k)(i)-s(X_k)(j)\right)<0  \right\} -\mathcal{R}_{i,j}(s) \right\vert \\+\left\{\inf_{s\in \mathcal{S}_0}\mathcal{R}(s)-\mathcal{R}^*\right\},
\end{multline*}
where $\mathcal{R}_{i,j}(s)=\mathbb{P}\{(\Sigma(i)-\Sigma(j)) (s(X)(i)-s(X)(j))<0   \}$ for $i<j$ and $s\in \mathcal{S}$. 
Since Assumption \ref{hyp:complex} is satisfied, by virtue of Vapnik-Chervonenkis inequality (see \textit{e.g.} \citet{DGL96}), for all $i<j$ and any $\delta\in (0,1)$, we have with probability at least $1-\delta$:
\begin{equation}
\sup_{s\in \mathcal{S}_0}\left\vert \frac{1}{N}\sum_{k=1}^N \mathbb{I}\left\{\left(\Sigma_k(i)-\Sigma_k(j)\right) \left(s(X_k)(i)-s(X_k)(j)\right)<0  \right\} -\mathcal{R}_{i,j}(s) \right\vert \leq c\sqrt{V\log(1/\delta)/N},
\end{equation}
where $c<+\infty$ is a universal constant. The desired bound then results from the combination of the bound above and the union bound.


\bigskip



\begin{remark}\label{rk:minimaxity}{\sc (On minimaxity)} Observing that, when $X$ and $\Sigma$ are independent, the best predictions are $P$'s Kemeny medians, it follows from Proposition 11 in \citet{CKS17} that the minimax risk can be bounded by below as follows:
	$$
	\inf_{s_N}\sup_{\mathcal{L}}\mathbb{E}_{\mathcal{L}}\left[ \mathcal{R}_{\mathcal{L}}(s_N)-\mathcal{R}^*_{\mathcal{L}}  \right]\geq \frac{1}{16e\sqrt{N}},
	$$
	where the supremum is taken over all possible probability distributions $\mathcal{L}=\mu(dx)\otimes P_x(d\sigma)$ for $(X,\Sigma)$ (including the independent case) and the minimum is taken over all mappings that map a dataset $(X_1,\Sigma_1),\; \ldots,\; (X_N,\Sigma_N)$ made of independent copies of $(X,\Sigma)$ to a ranking rule in $\mathcal{S}$.
\end{remark}


\medskip


\subsection*{Proof of Proposition \ref{prop:fast}}

The subsequent fast rate analysis mainly relies on the lemma below.
  
\begin{lemma}\label{lem:var_control}
 	Suppose that Assumption \ref{hyp:margin} is fulfilled. Let $s\in \mathcal{S}$ and set
 	\begin{multline*}
 	Z(s)=\sum_{i<j}\left\{\mathbb{I}\left\{ \left(\Sigma(i)-\Sigma(j)\right)\left(s(X)(i)-s(X)(j)  \right)<0 \right.\right\}-\\
 	\sum_{i<j}\mathbb{I}\left\{ \left. \left(\Sigma(i)-\Sigma(j)\right)\left(\sigma^*_{P_X}(i)-\sigma^*_{P_X}(j)  \right)<0 \right\} \right\}.
 	\end{multline*}
 	Then, we have: 
 	\begin{equation*}
 	Var\left( Z(s)\right)
 	\leq \left(\frac{n(n-1)}{2H}\right)\times \left(\mathcal{R}(s)-\mathcal{R}^*\right).
 	\end{equation*}
\end{lemma}
\begin{proof}
 Recall first that it follows from \eqref{eq:opt_sol} that, for all $i<j$,
$$
\left(\sigma^*_{P_X}(j)-\sigma^*_{P_X}(i)\right) \left( p_{i,j}(X)-1/2 \right)>0.
$$
Hence, we have:
\begin{multline*}
Var\left( Z(s)\right)
\leq \frac{n(n-1)}{2}\times 
\sum_{i<j}Var\left(\mathbb{I}\left\{ \left(p_{i,j}(X)-1/2\right)\left(s(X)(j)-s(X)(i)  \right) <0\right\} \right)\\
\leq \frac{n(n-1)}{2}\times 
\sum_{i<j}\mathbb{E}\left[\mathbb{I}\left\{ \left(p_{i,j}(X)-1/2\right)\left(s(X)(j)-s(X)(i)  \right) <0\right\}\right].
\end{multline*}
In addition, it follows from formula \eqref{eq:excess_risk} for the risk excess that:
\begin{equation*}
\mathcal{R}(s)-\mathcal{R}^*\geq H\times \sum_{i<j}\mathbb{E}\left[\mathbb{I}\left\{ \left(p_{i,j}(X)-1/2\right)\left(s(X)(j)-s(X)(i)  \right) <0\right\}\right].
\end{equation*}
Combined with the previous inequality, this establishes the lemma.

\end{proof}
Since the goal is to give the main ideas, we assume for simplicity that the $\mathcal{S}_0$ is of finite cardinality and that the optimal ranking median regression rule $s^*(x)=\sigma^*_{P_x}$ belongs to it.
Applying Bernstein's inequality to the i.i.d. average $(1/N)\sum_{k=1}^NZ_k(s)$, where
\begin{multline*}
Z_k(s)=\sum_{i<j}\left\{\mathbb{I}\left\{ \left(\Sigma_k(i)-\Sigma_k(j)\right)\left(s(X_k)(i)-s(X_k)(j)  \right)<0 \right. \right\}-\\
\sum_{i<j}\mathbb{I}\left\{ \left. \left(\Sigma_k(i)-\Sigma_k(j)\right)\left(\sigma^*_{P_{X_k}}(i)-\sigma^*_{P_{X_k}}(j)  \right)<0 \right\} \right\},
\end{multline*}
for $1\leq k\leq N$ and the union bound over the ranking rules $s$ in $\mathcal{S}_0$, we obtain that, for all $\delta\in (0,1)$, we have with probability larger than $1-\delta$: $\forall s\in \mathcal{S}_0$,
\begin{equation*}
\mathbb{E}[Z(s)]=\mathcal{R}(s)-\mathcal{R}^*\leq \widehat{\mathcal{R}}_N(s)-\widehat{\mathcal{R}}_N(s^*)+\sqrt{\frac{2Var(Z(s))\log(C/\delta)}{N}}+\frac{4\log(C/\delta)}{3N}.
\end{equation*}
Since $\widehat{\mathcal{R}}_N(\widehat{s}_N)-\widehat{\mathcal{R}}_N(s^*)\leq 0$ by assumption and using the variance control provided by Lemma \ref{lem:var_control} above, we obtain that, with probability at least $1-\delta$, we have:
\begin{equation*}
\mathcal{R}(\widehat{s}_N)-\mathcal{R}^*\leq\sqrt{\frac{\frac{n(n-1)}{H}\left(\mathcal{R}(\widehat{s}_N)-\mathcal{R}^*\right)/H \times \log(C/\delta)}{N}}+\frac{4\log(C/\delta)}{3N}.
\end{equation*}
Finally, solving this inequality in $\mathcal{R}(\widehat{s}_N)-\mathcal{R}^*$ yields the desired result.

\medskip

\subsection{Local Consensus Methods for Ranking Median Regression}

\subsection*{Proof of Proposition \ref{prop:opt_subclass}}
Let $s(x)=\sum_{k=1}^K\sigma_k \mathbb{I}\{ x\in \mathcal{C}_k \}$ in $\mathcal{S}_{\mathcal{P}}$. It suffices to observe that we have
\begin{equation}
	\mathcal{R}(s)=\sum_{k=1}^K\mathbb{E}\left[  \mathbb{I}\{ X\in \mathcal{C}_k \}d_{\tau}(\sigma_k,\Sigma) \right]=\sum_{k=1}^K\mu(\mathcal{C}_k)\mathbb{E}\left[d_{\tau}\left(\sigma_k  ,\Sigma\right) \mid X\in \mathcal{C}_k \right],
\end{equation}
and that each term involved in the summation above is minimum for $\sigma_k\in \mathcal{M}_{P_{\mathcal{C}_k}}$, $1\leq k\leq K$.

\medskip

\subsection*{An example of a distribution satisfying Assumptions \ref{hyp:margin}-\ref{hyp:smooth}}



\tikzstyle{stuff_nofill}=[rectangle,draw,node contents={A}]
\tikzstyle{stuff_fill1}=[node contents={M1}]
\tikzstyle{stuff_fill2}=[node contents={M2}]
\tikzstyle{stuff_fill3}=[node contents={M3}]
\tikzstyle{stuff_fill4}=[node contents={M4}]
\begin{figure}[!h]
	\centering
\begin{tikzpicture}
\draw[help lines, color=gray!30, dashed] (-2.9,-2.9) grid (2.9,2.9);
\draw[->, very thin] (-3,0)--(3,0) node[right]{$x$};
\draw[->, very thin] (0,-3)--(0,3) node[above]{$y$};

%cell1
\draw[-,blue!40!white,very thin] (-3,0.5)--(-0.5,0.5);
\draw[-,blue!40!white,very thin] (-0.5,0.5)--(-0.5,3);
preaction=\fill[blue!40!white, opacity=0.3] (-3,0.5) rectangle (-0.5,3);
\node at (-2,2) [stuff_fill1] {};

%cell2
\draw[-,blue!40!white,very thin] (0.5,0.5)--(3,0.5);
\draw[-,blue!40!white,very thin] (0.5,0.5)--(0.5,3);
preaction=\fill[blue!40!white, opacity=0.3] (0.5,0.5) rectangle (3,3);
\node at (2,2) [stuff_fill2] {};

%cell3
\draw[-,blue!40!white,very thin] (0.5,-0.5)--(3,-0.5);
\draw[-,blue!40!white,very thin] (0.5,-0.5)--(0.5,-3);
preaction=\fill[blue!40!white, opacity=0.3] (0.5,-0.5) rectangle (3,-3);
\node at (2,-2) [stuff_fill3] {}; 

%cell4
\draw[-,blue!40!white,very thin, name path = A] (-3,-0.5)--(-0.5,-0.5);
\draw[-,blue!40!white,very thin, name path = B] (-0.5,-0.5)--(-0.5,-3);
preaction=\fill[blue!40!white, opacity=0.3] (-0.5,-0.5) rectangle (-3,-3);
\node at (-2,-2) [stuff_fill4] {};
\end{tikzpicture}


\caption{Example of a distribution satisfying Assumptions \ref{hyp:margin}-\ref{hyp:smooth} in $\mathbb{R}^2$ .}
\label{fig:example_partition}
\end{figure}

\noindent We will give an example in dimension 2. Let $\mathcal{P}$ a partition of $\mathbb{R}^2$ represented Figure \ref{fig:example_partition}. Suppose that for $x \in \mathcal{X}$, $\mu(x)$ is null outside the colored areas $(M_k)_{k=1, \dots,4}$, and that on each  $M_k$ for $k=1, \dots, 4$, $P_X$ is constant and equals to $P_{M_k}$, the conditional distribution of $\Sigma$ given $X \in M_k$. Suppose then that $P_{M_k}$ is a Mallows distribution with parameters $(\pi_k, \phi_k)$ for $k=1, \dots, 4$. Firstly, if for each $k=1, \dots, 4$, $\phi_k \le (1-2H)/(1+2H)$, Assumption \ref{hyp:margin} is verified. Secondly, Assumption \ref{hyp:smooth} is satisfied given that the $M_k$'s cells are far from each other enough. Indeed, for any pair $(x,x')$, it is trivial if $x$ and $x'$ are in the same cell. Then the $M$-Lipschitz condition is always satisfied, as soon as the partition $\mathcal{P}$ is such that $d(x,x') \ge n(n-1)/2M$ for any $(x,x')$ not in the same cell.

\bigskip

\subsection*{Proof of Theorem \ref{thm:approx}}
Consider $s_{\mathcal{P}}(x)=\sum_{\mathcal{C}\in \mathcal{P}}\sigma_{P_{\mathcal{C}}}\cdot \mathbb{I}\{x\in \mathcal{C}\}$ in $\mathcal{S}^*_{\mathcal{P}}$, \textit{i.e.} $\sigma_{P_{\mathcal{C}}}\in \mathcal{M}_{P_{\mathcal{C}}}$ for all $\mathcal{C}\in \mathcal{P}$. 
\begin{equation*}
\mathcal{R}(s_N)-\mathcal{R}^*=\int_{x\in \X}\left\{ L_{P_x}(s_N(x))-L_{P_x}^* \right\}\mu(dx)=\sum_{\mathcal{C}\in \mathcal{P}}\int_{x\in \mathcal{C}}\left\{ L_{P_x}(\sigma_{P_{\mathcal{C}}})-L_{P_x}^* \right\}\mu(dx).
\end{equation*}
Now, by virtue of assertion $(i)$ of Lemma \ref{lem:bounds}, we have
\begin{equation*}
\mathcal{R}(s_{\mathcal{P}})-\mathcal{R}^*\leq 2\sum_{i<j}\sum_{\mathcal{C}\in\mathcal{P}}\int_{x\in \mathcal{C}}\left\vert p_{i,j}(C)-p_{i,j}(x) \right\vert\mu(dx).
\end{equation*}
Now, observe that, for any $\mathcal{C}\in \mathcal{P}$, all $x\in \mathcal{C}$ and $i<j$, it results from Jensen's inequality and Assumption \ref{hyp:smooth} that
\begin{equation*}
\left\vert p_{i,j}(\mathcal{C})-p_{i,j}(x) \right\vert \leq \int_{x\in \mathcal{C}}\left\vert p_{i,j}(x')-p_{i,j}(x) \right\vert \mu(dx') /\mu(\mathcal{C})\leq M \delta_{\mathcal{P}},
\end{equation*}
which establishes \eqref{eq:RMR_bound}.

We now prove the second assertion. For any measurable set $\mathcal{C}\subset \mathcal{X}$ such that $\mu(\mathcal{C})>0$, we set $p_{i,j}(\mathcal{C})=\mathbb{P}\{ \Sigma(i)<\Sigma(j) \mid X\in \mathcal{C} \}$ for $i<j$. Suppose that $x\in \mathcal{C}_k$, $k\in\{1,\; \ldots,\; K  \}$. It follows from assertion (ii) in Lemma \ref{lem:bounds} combined with Jensen's inequality and Assumption \ref{hyp:smooth} that:
\begin{multline*}
d_{\tau}\left(\sigma^*_{P_x}, s^*_{\mathcal{P}}(x)  \right)=d_{\tau}\left(\sigma^*_{P_x}, \sigma^*_{P_{\mathcal{C}_k}}  \right)\leq (1/H)\sum_{i<j}\left\vert p_{i,j}(x)-p_{i,j}(\mathcal{C}_k) \right\vert\\
\leq (1/H)\sum_{i<j}\mathbb{E}\left[\left\vert p_{i,j}(x)-p_{i,j}(X) \right\vert \mid X\in \mathcal{C}_k  \right]\leq (M/H)\sup_{x'\in \mathcal{C}_k}\vert\vert x-x'\vert\vert\leq (M/H)\cdot \delta_{\mathcal{P}}.
\end{multline*}

\begin{remark} {\sc (On learning rates)} For simplicity, assume that $\mathcal{X}=[0,1]^d$  and that $\mathcal{P}_m$ is a partition with $m^d$ cells with diameter less than $C\times 1/m$ each, where $C$ is a constant. Provided the assumptions it stipulates are fulfilled, Theorem \ref{thm:approx} shows that the bias of the ERM method over the class $\mathcal{S}_{\mathcal{P}_m}$ is of order $1/m$. Combined with Proposition \ref{prop:upper_bound}, choosing $m\sim \sqrt{N}$ gives a nearly optimal learning rate, of order $O_{\mathbb{P}}((\log N)/N)$ namely.
\end{remark}
\begin{remark}{\sc (On smoothness assumptions)} We point out that the analysis above could be naturally refined, insofar as the accuracy of a piecewise constant median ranking regression rule is actually controlled by its capacity to approximate an optimal rule $s^*(x)$ in the $\mu$-integrated Kendall $\tau$ sense, as shown by Eq. \eqref{eq:bias1}. Like in \citet{Binev} for distribution-free regression, learning rates for ranking median regression could be investigated under the assumption that $s^*$ belongs to a certain smoothness class defined in terms of approximation rate, specifying the decay rate of $\inf_{s\in\mathcal{S}_m}\mathbb{E}[d_{\tau}(s^*(X), s(X))]$ for a certain sequence $(\mathcal{S}_m)_{m\geq 1}$ of classes of piecewise constant ranking rules. This is beyond the scope of the present paper and will be the subject of future work.
\end{remark}

\bigskip

\subsection*{Proof of Theorem \ref{th:partition_consist}}

We start with proving the first assertion and consider a RMR rule $s_N$ of the form \eqref{eq:rule_theor}. With the notations of Theorem \ref{thm:approx}, we have the following decomposition:
\begin{equation}\label{eq:decompos}
\mathcal{R}(s_N)-\mathcal{R}^*= \left(\mathcal{R}(s_N)-\mathcal{R}(s_{\mathcal{P}_N})\right)+\left(\mathcal{R}(s_{\mathcal{P}_N})-\mathcal{R}^*\right).
\end{equation}
Consider first the second term on the right hand side of the equation above. It results from the argument of Theorem \ref{thm:approx}'s that:
\begin{equation}\label{eq:bound_diam}
\mathcal{R}(s_{\mathcal{P}_N})-\mathcal{R}^*\leq M  \delta_{\mathcal{P}_N}\rightarrow 0\text{ in probability as } N\rightarrow \infty.
\end{equation}

We now turn to the first term. Notice that, by virtue of Lemma \ref{lem:bounds},
\begin{equation}
\mathcal{R}(s_N)-\mathcal{R}(s_{\mathcal{P}_N})=\sum_{\mathcal{C}\in \mathcal{P}_N}\left\{L_{P_{\mathcal{C}}}(\widehat{\sigma}_{\mathcal{C}})-L^*_{P_{\mathcal{C}}}  \right\}\mu(\mathcal{C})\leq 2\sum_{i<j}\sum_{\mathcal{C}\in \mathcal{P}_N}\left\vert \widehat{p}_{i,j}(\mathcal{C})-p_{i,j}(\mathcal{C}) \right\vert \mu(\mathcal{C}),
\end{equation}
where, for any $i<j$ and all measurable $\mathcal{C}\subset \mathcal{X}$, we set $$\widehat{p}_{i,j}(\mathcal{C})=(1/(N\widehat{\mu}_N(\mathcal{C})))\sum_{k=1}^N \mathbb{I}\{ X_k\in \mathcal{C},\; \Sigma_k(i)<\Sigma_k(j)\}  $$ and
$\widehat{\mu}_N(\mathcal{C})=(1/N)\sum_{k=1}^N \mathbb{I}\{ X_k\in \mathcal{C} \}=N_{\mathcal{C}}/N$, with the convention that $\widehat{p}_{i,j}(\mathcal{C})=0$ when $\widehat{\mu}_N(\mathcal{C})=0$. We incidentally point out that the $\widehat{p}_{i,j}(\mathcal{C})$'s are the pairwise probabilities related to the distribution $\widehat{P}_{\mathcal{C}}=(1/(N\widehat{\mu}_N(\mathcal{C})))\sum_{k:\; X_k\in \mathcal{C}} \delta_{\Sigma_k}$.
Observe that for all $i<j$ and $\mathcal{C}\in\mathcal{P}_N$, we have:
\begin{multline*}
\mu(\mathcal{C})\left( \widehat{p}_{i,j}(\mathcal{C})-p_{i,j}(\mathcal{C})  \right)=\left\{\frac{1}{N}\sum_{k=1}^N\mathbb{I}\{ X_k\in \mathcal{C},\; \Sigma_k(i)<\Sigma_k(j)\} -\mathbb{E}\left[ \mathbb{I}\{ X\in \mathcal{C},\; \Sigma(i)<\Sigma(j)\} \right]  \right\}\\ + \left\{ \left(\frac{\mu(\mathcal{C})}{-\widehat{\mu}_N(\mathcal{C})+\mu(\mathcal{C})}-1  \right)^{-1}\times \frac{1}{N}\sum_{k=1}^N\mathbb{I}\{ X_k\in \mathcal{C},\; \Sigma_k(i)<\Sigma_k(j)\}  \right\}.  
\end{multline*}
Combining this equality with the previous bound yields
\begin{equation}\label{eq:bound_term1}
\mathcal{R}(s_N)-\mathcal{R}(s_{\mathcal{P}_N})\leq 2\sum_{i<j}\left\{
A_N(i,j)+  B_N/\kappa_N\right\},
\end{equation}
where we set
\begin{eqnarray*}
A_N(i,j)&=&\sup_{\mathcal{P} \in \mathcal{F}_{N}} \sum_{ C \in \mathcal{P}} \left\vert\frac{1}{N}\sum_{k=1}^N\mathbb{I}\{ X_k\in \mathcal{C},\; \Sigma_k(i)<\Sigma_k(j)\} -\mathbb{E}\left[ \mathbb{I}\{ X\in \mathcal{C},\; \Sigma(i)<\Sigma(j)\} \right]  \right\vert,\\
B_N&=&\sup_{\mathcal{P} \in \mathcal{F}_{N}} \sum_{ C \in \mathcal{P}} \left\vert \widehat{\mu}_N(C)- \mu(C) \right\vert
\end{eqnarray*}
The following result is a straightforward application of the {\sc VC} inequality for data-dependent partitions stated in Theorem 21.1 of \citet{DGL96}.

\begin{lemma}\label{lem:VCbounds}
Under the hypotheses of Theorem \ref{th:partition_consist}, the following bounds hold true: $\forall \epsilon>0$, $\forall N \geq 1$,
\begin{eqnarray*}
	\mathbb{P}\left\{ A_N(i,j) > \epsilon \right\}& \le &  8 \log(\Delta_N(\mathcal{F}_N))e^{-N\epsilon^2 /512} + e^{-N \epsilon^2 /2},\\
	\mathbb{P}\left\{ B_N> \epsilon \right\} &\le &
	8 \log(\Delta_N(\mathcal{F}_N))e^{-N\epsilon^2 /512} + e^{-N \epsilon^2 /2}.
	\end{eqnarray*}
\end{lemma}
The terms $A_N(i,j)$ and $B_N$ are both of order $O_{\mathbb{P}}(\sqrt{\log(\Delta_N(\mathcal{F}_N))/N)})$, as shown by the lemma above. Hence, using Eq. \eqref{eq:bound_term1} and the assumption that $\kappa_N\rightarrow 0$ in probability as $N\rightarrow \infty$, so that $1/\kappa_N=o_{\mathbb{P}}(\sqrt{N/\log \Delta_N(\mathcal{F}_N)})$, we obtain that $\mathcal{R}(s_N)-\mathcal{R}(s_{\mathcal{P}_N})\rightarrow 0$ in probability as $N\rightarrow \infty$, which concludes the proof of the first assertion of the theorem.\\

We now consider the RMR rule \eqref{eq:rule_pract}. Observe that
\begin{multline}\label{eq:indicator}
\mathcal{R}(\widetilde{s}_N)-\mathcal{R}(s_{\mathcal{P}_N})=\sum_{\mathcal{C}\in \mathcal{P}_N}\left\{L_{P_{\mathcal{C}}}(\widetilde{\sigma}^*_{\widehat{P}_{\mathcal{C}}})-L^*_{P_{\mathcal{C}}}  \right\}\mu(\mathcal{C})\\
=\sum_{\mathcal{C}\in \mathcal{P}_N}\mathbb{I}\{\widehat{P}_{\mathcal{C}}\in \mathcal{T} \}\left\{L_{P_{\mathcal{C}}}(\sigma^*_{\widehat{P}_{\mathcal{C}}})-L^*_{P_{\mathcal{C}}}  \right\}\mu(\mathcal{C})+
\sum_{\mathcal{C}\in \mathcal{P}_N}\mathbb{I}\{\widehat{P}_{\mathcal{C}}\notin \mathcal{T} \}\left\{L_{P_{\mathcal{C}}}(\widetilde{\sigma}^*_{\widehat{P}_{\mathcal{C}}})-L^*_{P_{\mathcal{C}}}  \right\}\mu(\mathcal{C})\\
\leq  \mathcal{R}(s_N)-\mathcal{R}(s_{\mathcal{P}_N})+\frac{n(n-1)}{2}\sum_{\mathcal{C}\in \mathcal{P}_N}\mathbb{I}\{\widehat{P}_{\mathcal{C}}\notin \mathcal{T} \}\mu(\mathcal{C}).
\end{multline}
Recall that it has been proved previously that $\mathcal{R}(s_N)-\mathcal{R}(s_{\mathcal{P}_N})\rightarrow 0$ in probability as $N\rightarrow \infty$. Observe in addition that
\begin{equation*}
\mathbb{I}\{\widehat{P}_{\mathcal{C}}\notin \mathcal{T} \}\leq \mathbb{I}\{P_{\mathcal{C}}\notin \mathcal{T} \}+\mathbb{I}\{\widehat{P}_{\mathcal{C}}\notin \mathcal{T} \text{ and } P_{\mathcal{C}}\in \mathcal{T}\}
\end{equation*}
and, under Assumption \ref{hyp:margin},
\begin{eqnarray*}
\{ P_{\mathcal{C}}\notin \mathcal{T} \} &\subset & \{ \delta_{\mathcal{P}_N}\geq M/H\},\\
\{\widehat{P}_{\mathcal{C}}\notin \mathcal{T} \text{ and } P_{\mathcal{C}}\in \mathcal{T}\} &\subset & \cup_{i<j}\{  \vert \widehat{p}_{i,j}(\mathcal{C})-p_{i,j}(\mathcal{C}) \vert\geq H \},
\end{eqnarray*}
so that $\sum_{\mathcal{C}\in \mathcal{P}_N} \mathbb{I}\{ \widehat{P}_{\mathcal{C}}\notin \mathcal{T} \} \mu(\mathcal{C})$ is bounded by
\begin{multline*}
 \mathbb{I}\{ \delta_{\mathcal{P}_N}\geq M/H\} + \sum_{i<j}\sum_{\mathcal{C}\in \mathcal{P}_N}\mathbb{I}\{  \vert \widehat{p}_{i,j}(\mathcal{C})-p_{i,j}(\mathcal{C}) \vert\geq H \}\mu(\mathcal{C})\\
\leq \mathbb{I}\{ \delta_{\mathcal{P}_N}\geq M/H\} + \sum_{i<j}\sum_{\mathcal{C}\in \mathcal{P}_N}\vert \widehat{p}_{i,j}(\mathcal{C})-p_{i,j}(\mathcal{C}) \vert \mu(\mathcal{C})/H\\
\leq \mathbb{I}\{ \delta_{\mathcal{P}_N}\geq M/H\}+\frac{1}{H}\sum_{i<j}\left\{
A_N(i,j)+  B_N/\kappa_N\right\},
\end{multline*}
re-using the argument that previously lead to \eqref{eq:bound_term1}.
This bound clearly converges to zero in probability, which implies that $\mathcal{R}(\widetilde{s}_N)-\mathcal{R}(s_{\mathcal{P}_N})\rightarrow 0$ in probability when combined with \eqref{eq:indicator} and concludes the proof of the second assertion of the theorem.

%\bigskip

\subsection{Appendix~\ref{sec:appendix_algos}}


\subsection*{Proof of Theorem \ref{thm:NN_consist}}
Denote by $\widehat{p}_{i,j}(x)$'s the pairwise probabilities related to distribution $\widehat{P}(x)$. It follows from Lemma \ref{lem:bounds} combined with Jensen's inequality, that
\begin{multline*}
\mathbb{E}\left[\mathcal{R}(s_{k,N})-\mathcal{R}^*\right] =   \mathbb{E}\left[ \int_{x\in \mathcal{X}} (L_{P_x}(s_{k,N}(x))- L_{P_x}^*) \mu(dx) \right]
 \le 2\sum_{i<j}\int_{x\in \mathcal{X}} \mathbb{E}\left[  \left\vert  p_{i,j}(x)- \widehat{p}_{i,j}(x) \right\vert \right]\\
 \leq 2\sum_{i<j} \int_{x\in \mathcal{X}} \left(\mathbb{E}\left[  \left(  p_{i,j}(x)- \widehat{p}_{i,j}(x) \right)^2 \right]\right)^{1/2}
\end{multline*}
Following the argument of Theorem 6.2's proof in \citet{gyorfi2006distribution}, write:
\begin{multline*}
\mathbb{E}\left[  \left( \widehat{p}_{i,j}(x) -  p_{i,j}(x)  \right)^2 \right]  = \mathbb{E}\left[  \left(  \widehat{p}_{i,j}(x)-  \mathbb{E}\left[\widehat{p}_{i,j}(x) | X_1, \dots, X_N \right] \right)^2 \right] \\+ \mathbb{E}\left[  \left( \mathbb{E}\left[\widehat{p}_{i,j}(x) | X_1, \dots, X_N \right] - p_{i,j}(x)\right)^2 \right]
  = I_1(x)+I_2(x).
\end{multline*}
The first term can be upper bounded as follows:
\begin{multline*}
I_1(x) = \mathbb{E}\left[ \left( \frac{1}{k} \sum_{l=1}^{k} \left( \mathbb{I}\left\{\Sigma_{(l,N)}(i)<\Sigma_{(l,N)}(j) \right\} - p_{i,j}(X_{(l,N)}) \right)\right) ^2 \right]\\
=  \mathbb{E}\left[ \frac{1}{k^2} \sum_{l=1}^{k} Var(\mathbb{I}\left\{\Sigma(i)<\Sigma(j)\right\} | X=X_{(l,N)}  )\right]
 \le \frac{1}{4k}.
\end{multline*}
For the second term, we use the following result.
\begin{lemma}\label{lem:x_bounded} (Lemma 6.4, \citet{gyorfi2006distribution})
	Assume that the r.v. $X$ is bounded. If $d\ge3$, then:
	\begin{equation*}
	\mathbb{E} \left[ \| X_{(1,N)}(x) -x \|^2  \right] \le \frac{c_1}{N^{2/d}},
	\end{equation*}
	where $c_1$ is a constant that depends on $\mu$'s support only.
\end{lemma}
Observe first that, following line by line the argument of Theorem 6.2's proof in \citet{gyorfi2006distribution} (see p.95 therein), we have:
\begin{multline*}
I_2(x)=  \mathbb{E}\left[ \frac{1}{k} \left( \sum_{l=1}^{k} \left( p_{i,j}(X_{(l,N)}) - p_{i,j}(x) \right) \right)^2 \right] \le  \mathbb{E}\left[  \left( \frac{1}{k} \sum_{l=1}^{k} M \| X_{(l,N)} - x\| \right)^2 \right]\\
 \le  M^2 \mathbb{E} \left[ \| X_{(1, \floor{N/k})}(x) -x \|^2  \right].
\end{multline*}
Next, by virtue of Lemma \ref{lem:x_bounded}, we have:
\begin{equation*}
\frac{1}{M^2} \floor{N/k}^{2/d}\int_{x \in \mathcal{X}}I_2(x)\mu(dx) \le c_1.
\end{equation*}
Finally, we have:
\begin{multline*}
\mathbb{E}\left[\mathcal{R}(s_{k,N})-\mathcal{R}^*\right]
\le 2 \sum_{i<j}\int_{x\in \mathcal{X}} \sqrt{I_1(x)+ I_2(x)}\mu(dx) \\
\le  \frac{n(n-1)}{2} \left( \frac{1}{\sqrt{k}} + 2\sqrt{c_1}M \left(\frac{k}{N}\right)^{1/d} \right).
\end{multline*}

We now consider the problem of bounding the expectation of the excess of risk of the RMR rule $\widetilde{s}_{k,N}$. Observing that $s_{k,N}(x)=\widetilde{s}_{k,N}(x)$ when $\widehat{P}(x)\in \mathcal{T}$, we have:
\begin{multline*}
\mathbb{E}\left[\mathcal{R}(\widetilde{s}_{k,N})-\mathcal{R}^*\right] =   \mathbb{E}\left[ \int_{x\in \mathcal{X}} \mathbb{I}\{ \widehat{P}(x)\in \mathcal{T}\}(L_{P_x}(\widetilde{s}_{k,N}(x))- L_{P_x}^*) \mu(dx) \right] +\\
\mathbb{E}\left[ \int_{x\in \mathcal{X}} \mathbb{I}\{ \widehat{P}(x)\notin \mathcal{T}\}(L_{P_x}(\widetilde{s}_{k,N}(x))- L_{P_x}^*) \mu(dx) \right]\\
\leq \mathbb{E}\left[\mathcal{R}(s_{k,N})-\mathcal{R}^*\right] +\frac{n(n-1)}{2}\mathbb{E}\left[ \int_{x\in \mathcal{X}} \mathbb{I}\{ \widehat{P}(x)\notin \mathcal{T}\} \mu(dx) \right].
\end{multline*}
Notice in addition that, under Assumption \ref{hyp:margin}, we have, for all $x\in \mathcal{X}$,
\begin{equation}
\{ \widehat{P}(x)\notin \mathcal{T}\} \subset \cup_{i<j}\left\{  \left\vert \widehat{p}_{i,j}(x)-p_{i,j}(x)\geq H \right\vert  \right\},
\end{equation}
so that 
\begin{equation}
\mathbb{I}\{ \widehat{P}(x)\notin \mathcal{T}\} \leq\sum_{i<j}\frac{  \left\vert \widehat{p}_{i,j}(x)-p_{i,j}(x) \right\vert }{H}.
\end{equation}
Hence, the second assertion finally results directly from the bounds established to prove the first one.\\

\noindent Let $S_{x,\epsilon}$ denote the closed ball centered at $x$ of radius $\epsilon >0$. For $d \le 2$, the rates of convergence hold under the following additional conditions on $\mu$ (see \citet{gyorfi2006distribution}): there exists $\epsilon_0>0$, a non negative $g$ such that for all $x \in \mathbb{R}^d$ and $0<\epsilon \le \epsilon_0$, $\mu(S_{x,\epsilon}) >g(x)\epsilon^d$ and $\int 1/g(x)^{2/d} \mu(dx)<\infty$.

