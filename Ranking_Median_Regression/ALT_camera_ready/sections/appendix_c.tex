\section{Further comments on CRIT algorithm and Aggregation}\label{sec:appendix_crit}

\noindent \textbf{Pruning.} From the original tree $T_{2^J}$, one recursively merges children of a same parent node until the root $T_1$ is reached in a bottom up fashion. Precisely, the \textit{weakest link pruning} consists here in sequentially merging the children $\mathcal{C}_{j+1,2l}$ and $\mathcal{C}_{j+1,2l+1}$ producing the smallest dispersion increase: $$\widehat{\mu}_N(\mathcal{C}_{j,l})\gamma_{\widehat{P}_{\mathcal{C}_{j,l}}}-\Lambda_{j,l}(\mathcal{C}_{j+1,2l}).$$
One thus obtains a sequence of ranking median regression trees $T_{2^J}\supset T_{2^J-1}\supset \cdots \supset T_1$, the subtree $T_m$ corresponding to a partition with $\# T_m=m$ cells. The final subtree $T$ is selected by minimizing the complexity penalized intra-cell dispersion:
\begin{equation}
\widetilde{\gamma}_{T}=\widehat{\gamma}_{T}+\lambda\times \#T,
\end{equation}
where $\lambda\geq 0$ is a parameter that rules the trade-off between the complexity of the ranking median regression tree, as measured by $\#T$, and intra-cell dispersion. In practice, model selection can be performed by means of common resampling techniques.


\begin{remark}{\sc (Early stopping)} One stops the splitting process if no improvement can be achieved by splitting the current node $\mathcal{C}_{j,l}$, \textit{i.e.} if $\min_{\mathcal{C}\in \mathcal{G}}\Lambda(\mathcal{C})=\sum_{1\leq k<l\leq N}\mathbb{I}\{ (X_k,X_l)\in \mathcal{C}_{j,l}^2 \}\cdot d_{\tau}\left( \Sigma_k,\Sigma_l \right)$ (one then set $\mathcal{C}_{j+1,2l}=\mathcal{C}_{j,l}$ by convention), or if a minimum node size, specified in advance, is attained.
\end{remark}
\begin{remark}\label{rk:perp_split} {\sc (On class $\mathcal{G}$)} The choice of class $\mathcal{G}$ involves a trade-off between computational cost and flexibility: a rich class (of controlled complexity though) may permit to capture the conditional variability of $\Sigma$ given $X$ appropriately but might significantly increase the cost of solving \eqref{eq:min_pb}. Typically, as proposed in \citet{cart84}, subsets can be built by means of axis parallel splits, leading to partitions whose cells are finite union of hyperrectangles. This corresponds to the case where $\mathcal{G}$ is stable by intersection, \textit{i.e.} $\forall (\mathcal{C},\mathcal{C}')\in \mathcal{G}^2$, $\mathcal{C}\cap \mathcal{C}'\in \mathcal{G}$, and admissible subsets of any $\mathcal{C}\in \mathcal{G}$ are of the form $\mathcal{C}\cap \{X^{(m)}\geq s  \}$ or $\mathcal{C}\cap \{X^{(m)}\leq s  \}$, where $X^{(m)}$ can be any component of $X$ and $s\in \mathbb{R}$ any threshold value. In this case, the minimization problem can be efficiently solved by means of a double loop (over the $d$ coordinates of the input vector $X$ and over the data lying in the current parent node), see \textit{e.g.} \citet{cart84}.
\end{remark}

\noindent {\bf Interpretability and computational feasability.} The fact that the computation of (local) Kemeny medians takes place 
at the level of terminal nodes of the ranking median regression tree $T$ only makes the {\sc CRIT} algorithm very attractive from a practical perspective. In addition, it produces predictive rules that can be easily interpreted by means of a binary tree graphic representation and, when implemented with axis parallel splits, provides, as a by-product, indicators quantifying the impact of each input variable. The relative importance of a variable can be measured by summing the decreases of empirical $\gamma$-dispersion induced by all splits involving it as splitting variable. More generally, the {\sc CRIT} algorithm inherits the appealing properties of tree induction methods: it easily adapts to categorical predictor variables, training and prediction are fast and it is not affected by monotone transformations of the predictor variables.
\medskip

\noindent {\bf Aggregation.} Just like other tree-based methods, the {\sc CRIT} algorithm may suffer from instability, meaning that, due to its hierarchical structure, the rules it produces can be much affected by a small change in the training dataset. As proposed in \citet{Br96}, {\bf b}oostrap {\bf agg}regat{\bf ing} techniques may remedy to instability of ranking median regression trees. Applied to the {\sc CRIT} method, bagging consists in generating $B\geq 1$ bootstrap samples by drawing with replacement in the original data sample and running next the learning algorithm from each of these training datasets, yielding $B$ predictive rules $s_1,\; \ldots,\; s_B$. For any prediction point $x$, the ensemble of predictions $s_1(x),\; \ldots,\; s_B(x)$ are combined in the sense of Kemeny ranking aggregation, so as to produce a consensus $\bar{s}_B(x)$ in $\mathfrak{S}_n$. Observe that a crucial advantage of dealing with piecewise constant ranking rules is that computing a Kemeny median for each new prediction point can be avoided: one may aggregate the ranking rules rather than the rankings in this case, refer to the analysis below for further details. We finally point out that a certain amount of randomization can be incorporated in each bootstrap tree growing procedure, following in the footsteps of the random forest procedure proposed in \citet{Br01}, so as to increase flexibility and hopefully improve accuracy.

\subsection*{Aggregation of piecewise constant predictive ranking rules}
Let $\mathcal{P}$ be a partition of the input space $\mathcal{X}$. By definition, a subpartition of $\mathcal{P}$ is any partition $\mathcal{P}'$ of $\mathcal{X}$ with the property that, for any $\mathcal{C}'\in \mathcal{P}'$, there exists $\mathcal{C}\in \mathcal{P}$ such that $\mathcal{C}'\subset \mathcal{C}$. Given a collection $\mathcal{P}_1,\;  \ldots,\; \mathcal{P}_B$ of $B\geq 1$ partitions of $\mathcal{X}$, we call the 'largest' subpartition $\bar{\mathcal{P}}_B$ of the $\mathcal{P}_b$'s the partition of $\mathcal{X}$ that is a subpartition of each $\mathcal{P}_b$ and is such that, any subpartition of all the $\mathcal{P}_b$'s is also a subpartition of $\bar{\mathcal{P}}_B$ (notice incidentally that the cells of $\bar{\mathcal{P}}_B$ are of the form $\mathcal{C}_1\cap \cdots\cap \mathcal{C}_B$, where $(\mathcal{C}_1,\; \ldots,\; \mathcal{C}_B)\in \mathcal{P}_1\times \cdots\times \mathcal{P}_B$).
Considering now $B\geq 1$ piecewise constant ranking rules $s_1,\; \ldots,\; s_B$ associated with partitions $\mathcal{P}_1,\;  \ldots,\; \mathcal{P}_B$ respectively, we observe that the $s_b$'s are constant on each cell of $\bar{\mathcal{P}}_B$. One may thus write: $\forall b\in \{1,\; \ldots,\; B \}$,
$$
s_b(x)= \sum_{\mathcal{C}\in \bar{\mathcal{P}}_B}\sigma_{\mathcal{C}, b}\cdot \mathbb{I}\{x\in \mathcal{C}, b  \}.
$$
For any arbitrary $s(x)=\sum_{\mathcal{C}\in \bar{\mathcal{P}}_{B}}\sigma_{\mathcal{C}}\mathbb{I}\{ x\in \mathcal{C} \}$ in $\mathcal{S}_{\bar{\mathcal{P}}_B}$, we have:
\begin{equation*}
\mathbb{E}_{X\sim \mu}\left[\sum_{b=1}^B d_{\tau}\left(s(X),s_b(X)  \right)  \right]=\sum_{b=1}^B\sum_{\mathcal{C}\in \bar{\mathcal{P}}_B}\mu(\mathcal{C}  )d_{\tau}\left(\sigma_{\mathcal{C}}, \sigma_{\mathcal{C},b} \right)=\sum_{\mathcal{C}\in \bar{\mathcal{P}}_B}\sum_{b=1}^B\mu(\mathcal{C}  )d_{\tau}\left(\sigma_{\mathcal{C}}, \sigma_{\mathcal{C},b} \right).
\end{equation*}
Hence, the quantity above is minimum when, for each $\mathcal{C}$, the permutation $\sigma_{\mathcal{C}}$ is a Kemeny median of the probability distribution $(1/B)\sum_{b=1}^B\delta_{\sigma_{\mathcal{C},b}}$.

\subsection*{Consistency preservation and aggregation}
We now state and prove a result showing that the (possibly randomized) aggregation procedure previously proposed is theoretically founded. Precisely, mimicking the argument in \citet{BDL08} for standard regression/classification and borrowing some of their notations, consistency of ranking rules that are obtained through empirical Kemeny aggregation over a profile of consistent \textit{randomized ranking median regression rules} is investigated. Here, a randomized scoring function is of the form $\mathbf{S}_{\mathcal{D}_n}(., Z)$, where $\mathcal{D}_N=\{(X_1,\Sigma_1),\; \ldots,\; (X_N,\Sigma_N)\}$ is the training dataset and $Z$ is a r.v. taking its values in some measurable space $\mathcal{Z}$ that describes the randomization mechanism. 
A randomized ranking rule $\mathbf{S}_{\mathcal{D}_n}(., Z):\mathcal{X}\rightarrow \mathfrak{S}_N$ is given and consider its ranking median regression risk, $\mathcal{R}(\mathbf{S}_{\mathcal{D}_N}(., Z))$ namely, which is given by:
\begin{equation*}
\mathcal{R}(\mathbf{S}_{\mathcal{D}_N}(., Z))=\sum_{i<j}\mathbb{P}\left\{ \left(\mathbf{S}_{\mathcal{D}_N}(X, Z)(j)-\mathbf{S}_{\mathcal{D}_N}(X, Z)(i)  \right)\left( \Sigma(j)-\Sigma(i)\right)<0   \right\},
\end{equation*}
where the conditional probabilities above are taken over a random pair $(X,\Sigma)$, independent from $\mathcal{D}_N$. It is said to be consistent iff, as $N\rightarrow \infty$,
$$
\mathcal{R}(\mathbf{S}_{\mathcal{D}_N}(., Z))\rightarrow \mathcal{R}^*,
$$
in probability. When the convergence holds with probability one, one says that the randomized ranking rule is strongly consistent. Fix $B\geq 1$. Given $\mathcal{D}_N$, one can draw $B$ independent copies $Z_1,\; \ldots,\; Z_B$ of $Z$, yielding the ranking rules $\mathbf{S}_{\mathcal{D}_N}(., Z_b)$, $1\leq b\leq B$. Suppose that the ranking rule $\bar{\mathbf{S}}_B(.)$ minimizes 
\begin{equation}\label{eq:sum_scoring}
\sum_{b=1}^B\mathbb{E}\left[ d_{\tau}\left(s(X),\mathbf{S}_{\mathcal{D}_N}(X, Z_b)\right)\mid \mathcal{D}_N,\; Z_1,\; \ldots,\; Z_B \right]
\end{equation}
over $s\in \mathcal{S}$. The next result shows that, under Assumption \ref{hyp:margin} (strong) consistency is preserved for the ranking rule $\bar{\mathbf{S}}_B(X)$ (and the convergence rate as well, by examining its proof).
\begin{theorem}\label{thm:consist} ({\sc Consistency and aggregation.})
	Assume that the randomized ranking rule $\mathbf{S}_{\mathcal{D}_N}(., Z)$ is consistent (respectively, strongly consistent) and suppose that Assumption \ref{hyp:margin} is satisfied. Let $B\geq 1$ and, for all $N\geq 1$,  let $\bar{\mathbf{S}}_B(x)$ be a Kemeny median of $B$ independent replications of $\mathbf{S}_{\mathcal{D}_N}(x, Z)$ given $\mathcal{D}_N$. Then, the aggregated ranking rule $\bar{\mathbf{S}}_B(X)$ is consistent (respectively, strongly consistent).
\end{theorem}
 \begin{proof}
 	Let $s^*(x)$ be an optimal ranking rule, \textit{i.e} $\mathcal{R}(s^*)=\mathcal{R}^*$. Observe that, with probability one, we have: $\forall b\in\{1,\; \ldots,\; B  \}$,
 	\begin{equation*}
 	d_{\tau}\left(s^*(X),  \bar{\mathbf{S}}_B(X) \right) \leq  d_{\tau}\left(s^*(X),  \mathbf{S}_{\mathcal{D}_N}(X, Z_b) \right)+  d_{\tau}\left(\mathbf{S}_{\mathcal{D}_N}(X, Z_b),  \bar{\mathbf{S}}_B(X) \right),
 	\end{equation*}
 	and thus, by averaging,
 	\begin{multline*}
 	d_{\tau}\left(s^*(X),  \bar{\mathbf{S}}_B(X) \right) \leq \frac{1}{B}\sum_{b=1}^B  d_{\tau}\left(s^*(X),  \mathbf{S}_{\mathcal{D}_N}(X, Z_b) \right)+  \frac{1}{B}\sum_{b=1}^B  d_{\tau}\left(\mathbf{S}_{\mathcal{D}_N}(X, Z_b),  \bar{\mathbf{S}}_B(X) \right)\\
 	\leq \frac{2}{B}\sum_{b=1}^B  d_{\tau}\left(s^*(X),  \mathbf{S}_{\mathcal{D}_N}(X, Z_b) \right).
 	\end{multline*}
 	Taking the expectation w.r.t. to $X$, one gets:
 	\begin{multline*}
 	\mathcal{R}(\bar{\mathbf{S}}_B)-\mathcal{R}^*\leq \mathbb{E}\left[  d_{\tau}\left(s^*(X),  \bar{\mathbf{S}}_B(X) \right)  \mid \mathcal{D}_N,\; Z_1,\; \ldots,\; Z_B \right]\\
 	\leq \frac{1}{B}\sum_{b=1}^B\mathbb{E}\left[ d_{\tau}\left(s^*(X),  \mathbf{S}_{\mathcal{D}_N}(X, Z_b) \right) \mid \mathcal{D}_N,\; Z_1,\; \ldots,\; Z_B \right]
 	\leq \frac{1}{BH}\sum_{b=1}^B\left\{\mathcal{R}(\mathbf{S}_{\mathcal{D}_N}(., Z_b)) -\mathcal{R}^* \right\} 
 	\end{multline*}
 	using \eqref{eq:excess_risk} and Assumption \ref{hyp:margin}. This bound combined with the (strong) consistency assumption yields the desired result.
 \end{proof}


Of course, the quantity \eqref{eq:sum_scoring} is unknown in practice, just like distribution $\mu$, and should be replaced by a statistical version based on an extra sample composed of independent copies of $X$. Statistical guarantees for the minimizer of the empirical variant over a subclass $\mathcal{S}_0\subset \mathcal{S}$ of controlled complexity (\textit{i.e.} under Assumption \ref{hyp:complex}) could be established by adapting the argument of Theorem \ref{thm:consist}'s proof. However, from a practical perspective, aggregating the predictions (at a given point $x\in \mathcal{X}$) rather than the predictors would be much more tractable from a computational perspective (since it would permit to exploit the machinery for approximate Kemeny aggregation of subsection \ref{subsec:best}). This will be investigated in a forthcoming paper.