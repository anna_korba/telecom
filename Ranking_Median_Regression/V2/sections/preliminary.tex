
\section{Preliminaries}\label{sec:background}
As a first go, we start with recalling the metric approach to consensus ranking and give next a rigorous statistical formulation of  ranking aggregation. 
We also establish statistical guarantees for the generalization capacity of an alternative to the empirical Kemeny median technique studied in \citet{CKS17}, which can be much more easily computed in certain situations and which the algorithms we propose in the subsequent section highly rely on.  
From the angle embraced in this paper, \textit{ranking median regression} is then viewed as an extension of statistical ranking aggregation. Here and throughout, the indicator function of any event $\mathcal{E}$ is denoted by $\mathbb{I}\{ \mathcal{E} \}$, the Dirac mass at any point $a$ by $\delta_a$ and the cardinality of any finite set $E$ by $\# E$.
%, and we set $sgn(x)=2\mathbb{I}\{x\geq 0 \}-1$ for all $x\in\mathbb{R}$. 
Let $n\geq 1$, the set of permutations of $\n =\{1,\; \ldots,\; n  \}$ is denoted by $\mathfrak{S}_n$.



\subsection{Consensus Ranking: Probabilistic Framework and Statistical Setup}\label{subsec:consensus}
Throughout the article, a ranking on a set of items indexed by $\n$ is seen as the permutation $\sigma\in \Sn$ that maps any item $i$ to its rank $\sigma(i)$. Given a collection of $N\geq 1$ rankings $\sigma_{1},\; \ldots,\; \sigma_{N}$, the goal of consensus ranking, also referred to as ranking aggregation sometimes, is to find $\sigma^*\in \Sn$ that best summarizes it. A popular way of tackling this problem, the metric-based consensus approach, consists in solving:
\begin{equation}
	\label{eq:ranking_aggregation}
	\min_{\sigma\in \mathfrak{S}_n}\sum_{i=1}^N d(\sigma,\sigma_{i}),
\end{equation}
where $d(.,\; .)$ is a certain metric on $\Sn$. As the set $\Sn$ is of finite cardinality, though not necessarily unique, such a barycentric permutation, called \textit{consensus/median ranking}, always exists.
In Kemeny ranking aggregation, the version of this problem the most widely documented in the literature, one considers the number of pairwise disagreements as metric, namely the Kendall's $\tau$ distance, see \citet{Kemeny59}: $\forall (\sigma,\sigma')\in \mathfrak{S}_n^2$,
\begin{equation}\label{eq:Kendall_tau}
d_{\tau}(\sigma,\sigma')=\sum_{i<j}\mathbb{I}\{(\sigma(i)-\sigma(j)) (\sigma'(i)-\sigma'(j))<0\}.
\end{equation}
%Such a consensus has many interesting properties, but is NP-hard to compute. Various algorithms have been proposed in the literature to compute acceptably good solutions in a reasonable amount of time, their description is beyond the scope of the paper, see for example \cite{AM12} and the references therein. 
The problem \eqref{eq:ranking_aggregation} can be viewed as a $M$-estimation problem in the probabilistic framework stipulating that  the collection of rankings to be aggregated/summarized is composed of $N\geq 1$ independent copies $\Sigma_1,\; \ldots,\; \Sigma_N$ of a generic r.v. $\Sigma$, defined on a probability space $(\Omega,\; \mathcal{F},\; \mathbb{P})$ and drawn from an unknown probability distribution $P$ on $\mathfrak{S}_n$ (\textit{i.e.} $P(\sigma)=\mathbb{P}\{ \Sigma=\sigma \}$ for any $\sigma\in \mathfrak{S}_n$). Just like a median of a real valued r.v. $Z$ is any scalar closest to $Z$ in the $L_1$ sense, a (true) median of distribution $P$ w.r.t. a certain metric $d$ on $\mathfrak{S}_n$ is any solution of the minimization problem:
\begin{equation}\label{eq:median_pb}
\min_{\sigma \in \mathfrak{S}_n}L_P(\sigma),
\end{equation}
where $L_P(\sigma)=\mathbb{E}_{\Sigma \sim P}[d(\Sigma,\sigma)  ]
$ denotes the expected distance between any permutation $\sigma$ and $\Sigma$. In this framework, statistical ranking aggregation consists in recovering a solution $\sigma^*$ of this minimization problem, plus an estimate of this minimum $L^*_P=L_P(\sigma^*)$, as accurate as possible, based on the observations $\Sigma_1,\; \ldots,\; \Sigma_N$. A median permutation $\sigma^*$ can be interpreted as a central value for distribution $P$, while the quantity $L^*_P$ may be viewed as a dispersion measure. 
Like problem \eqref{eq:ranking_aggregation}, the minimization problem \eqref{eq:median_pb} has always a solution but can be multimodal.
However, the functional $L_P(.)$ is unknown in practice, just like distribution $P$. Suppose that we would like to avoid rigid parametric assumptions on $P$ such as those stipulated by the Mallows model, see \citet{Mallows57}, and only have access to the dataset $\{\Sigma_1,\; \ldots,\; \Sigma_N  \}$ to find a reasonable approximant of a median. Following the Empirical Risk Minimization (ERM) paradigm \citep[see \textit{e.g.}][]{Vapnik}, one substitutes in \eqref{eq:median_pb} the quantity $L(\sigma)$ with its statistical version
\begin{equation}\label{eq:emp_risk}
\widehat{L}_N(\sigma)=\frac{1}{N}\sum_{i=1}^Nd(\Sigma_i,\sigma)=L_{\widehat{P}_N}(\sigma),
\end{equation}
where $\widehat{P}_N=(1/N)\sum_{i=1}^N\delta_{\Sigma_i}$ denotes the empirical measure.
The performance of empirical consensus rules, namely solutions $\widehat{\sigma}_N$ of 
 $\min_{\sigma\in \mathfrak{S}_n}\widehat{L}_N(\sigma)$,
 has been investigated in \citet{CKS17}.  Precisely, rate bounds of order $O_{\mathbb{P}}(1/\sqrt{N})$ for the excess of risk $L_P(\widehat{\sigma}_N)-L^*_P$ in probability/expectation have been established and proved to be sharp in the minimax sense, when $d$ is the Kendall's $\tau$ distance. Whereas problem \eqref{eq:ranking_aggregation} is NP-hard in general (see \citet{Hudry08} for instance), in the Kendall's $\tau$ case, exact solutions, referred to as \textit{Kemeny medians}, can be explicited when the pairwise probabilities $p_{i,j}=\mathbb{P}\{  \Sigma(i)<\Sigma(j)\}$, $1\leq i\neq j\leq n$, fulfill the following property, referred to as \textit{stochastic transitivity}.
 \begin{definition}\label{def:stoch_trans}
 The probability distribution $P$ on $\mathfrak{S}_n$ is stochastically transitive iff
 $$
\forall (i,j,k)\in \n^3:\;\;  p_{i,j}\geq 1/2 \text{ and } p_{j,k}\geq 1/2 \; \Rightarrow\; p_{i,k}\geq 1/2.
 $$
 If, in addition, $p_{i,j}\neq 1/2$ for all $i<j$, $P$ is said to be strictly stochastically transitive.  
 \end{definition} 
 When stochastic transitivity holds true, the set of Kemeny medians (see Theorem 5 in \citet{CKS17}) is the (non empty) set 
 \begin{equation}\label{eq:opt_sol}
 \{\sigma\in \mathfrak{S}_n:\; (p_{i,j}-1/2)(\sigma(j)-\sigma(i ))>0 \text{ for all } i<j \text{ s.t. } p_{i,j}\neq 1/2  \},
 \end{equation}
 the minimum is given by
 \begin{equation}\label{eq:inf}
 L^*_P=\sum_{i<j}\min\{p_{i,j},1-p_{i,j}  \}=\sum_{i<j}\{  1/2-\vert  p_{i,j}-1/2\vert\}
 \end{equation}
  and, for any $\sigma\in \mathfrak{S}_n$,
 $
 L_P(\sigma)-L^*_P=2\sum_{i<j}\left\vert p_{i,j}-1/2 \right\vert\cdot \mathbb{I}\{ (\sigma(i)-\sigma(j))\left(p_{i,j}-1/2\right)<0\}.
 $
 If a strict version of stochastic transitivity is fulfilled, we denote by $\sigma^*_P$ the Kemeny median which is unique and given by the Copeland ranking:
 \begin{equation}\label{eq:sol_SST}
 \sigma^*_P(i)=1+\sum_{k\neq i}\mathbb{I}\{p_{i,k}<1/2  \} \text{ for } 1\leq i\leq n.
 \end{equation}
 %and the minimum expected Kendall $\tau$ distance is equal to 
 %\begin{equation}\label{eq:inf}
 %L^*_P=L_P(\sigma_P^*)=\sum_{i<j}\min\{p_{i,j},1-p_{i,j}  \}=\sum_{i<j}\left\{ 1/2-\left\vert  p_{i,j}-1/2\right\vert\right\}.
 %\end{equation}
 Recall also that examples of stochastically transitive distributions on $\mathfrak{S}_n$ are numerous and include most popular parametric models such as Mallows or Bradley-Terry-Luce-Plackett models, see \textit{e.g.} \citet{Mallows57} or \citet{Plackett75}. 
 \begin{remark}\label{rk:dispersion}{\sc (Measuring dispersion)} As noticed in \citet{CKS17}, an alternative measure of dispersion is given by
  $\gamma(P)=(1/2)\mathbb{E}[ d(\Sigma,\Sigma') ]$,
  where $\Sigma'$ is an independent copy of $\Sigma$. When $P$ is not strictly stochastically transitive, the latter can be much more easily estimated than $L_P^*$, insofar as no (approximate) median computation is needed: indeed, a natural estimator is given by the $U$-statistic
 $\widehat{\gamma}_N=2/(N(N-1))\sum_{i<j}d(\Sigma_i,\Sigma_j)$. For this reason, this empirical dispersion measure will be used as a splitting criterion in the partitioning algorithm proposed in subsection \ref{subsec:algo}. Observe in addition that
  $\gamma(P)\leq L^*_P\leq 2\gamma(P)$ and, when $d=d_{\tau}$, we have $\gamma(P)=\sum_{i<j}p_{i,j}(1-p_{i,j})$.
 \end{remark}
We denote by $\mathcal{T}$ the set of strictly stochastically transitive distributions on $\mathfrak{S}_n$. Assume that the underlying distribution $P$ belongs to $\mathcal{T}$ and verifies a certain low-noise condition {\bf NA}$(h)$, defined for $h>0$ by:
\begin{equation}\label{eq:hyp_margin0}
 \min_{i<j}\left\vert p_{i,j}-1/2 \right\vert \ge h
\end{equation}
This condition is checked in many situations, including most conditional parametric models (see Remark~13 in \citet{CKS17}). It may be considered as analogous to that introduced in \citet{KB05} in binary classification, and was used to prove fast rates also in ranking, for the estimation of the matrix of pairwise probabilities (see  \citet{SBGW15}) or ranking aggregation (see \citet{CKS17}). Indeed it is shown in \citet{CKS17} that under condition \eqref{eq:hyp_margin0}, the empirical distribution $\widehat{P}_N \in \mathcal{T}$ as well with overwhelming probability, and that the expectation of the excess of risk of empirical Kemeny medians decays at an exponential rate, see Proposition 14 therein. In this case, the nearly optimal solution $\sigma^*_{\widehat{P}_N}$ can be made explicit and straightforwardly computed using Eq. \eqref{eq:sol_SST} based on the empirical pairwise probabilities
$$
\widehat{p}_{i,j}=\frac{1}{N}\sum_{k=1}^N\mathbb{I}\{ \Sigma_k(i)<\Sigma_k(j)  \}, \; i<j.
$$ Otherwise, solving the NP-hard problem $\min_{\sigma \in \mathfrak{S}_n}L_{\widehat{P}_N}(\sigma)$ requires to get an empirical Kemeny median. However, as can be seen by examining the argument of Proposition 14's proof in \citet{CKS17}, the exponential rate bound holds true for any candidate $\widetilde{\sigma}_N$ in $\mathfrak{S}_n$ that coincides with $\sigma^*_{\widehat{P}_N}$ when the empirical distribution lies in $\mathcal{T}$. 
\begin{theorem}\label{thm:approx_med} (\citet{CKS17}, Proposition 14)
%\label{thm:approx_med}
Suppose that $P\in \mathcal{T}$ and fulfills condition {\bf NA}$(h)$. On the event $\{\widehat{P}_N \in \mathcal{T} \}$, define $\widetilde{\sigma}_{\widehat{P}_N}=\sigma^*_{\widehat{P}_N}$ and set $\widetilde{\sigma}_{\widehat{P}_N}=\sigma$, for $\sigma\in \mathfrak{S}_n$ arbitrarily chosen, on the complementary event. Then, for all $\delta\in (0,1)$, we have with probability at least $1-\delta$: $\forall N\geq 1$,
\begin{equation*}
L_{P}(\widetilde{\sigma}_{\widehat{P}_N})-L^*_{P}\leq \frac{n^2(n-1)^2}{8} \exp\left(-\frac{N}{2}\log \left(\frac{1}{1-4h^2}\right)\right).
\end{equation*}
\end{theorem}
In practice, when $\widehat{P}_N$ does not belong to $\mathcal{T}$, we propose to consider as a pseudo-empirical median any permutation $\widetilde{\sigma}^*_{\widehat{P}_N}$ that ranks the objects as the empirical Borda count:
$$ 
\left( \sum_{k=1}^N\Sigma_k(i)-\sum_{k=1}^N\Sigma_k(j)\right)\cdot \left( \widetilde{\sigma}^*_{\widehat{P}_N}(i)-\widetilde{\sigma}^*_{\widehat{P}_N}(j)   \right)>0 \text{ for all } i<j \text{ s.t. } \sum_{k=1}^N\Sigma_k(i) \ne \sum_{k=1}^N\Sigma_k(j) ,
$$
breaking possible ties in an arbitrary fashion. Alternative choices could also be guided by least-squares approximation of the empirical pairwise probabilities, as revealed by the following analysis. 
% Below, we establish statistical guarantees for the performance of a numerically feasible alternative, namely the regression-based method proposed in \citet{JLYY10}, when applied to pairwise probabilities $\widehat{p}_{i,j}$, $i<j$.
%\medskip

\subsection{Best strictly stochastically transitive approximation} \label{subsec:best}
 We suppose that $P\in \mathcal{T}$. If the empirical estimation $\widehat{P}_N$ of $P$ does not belong to $\mathcal{T}$, a natural strategy would consist in approximating it by a strictly stochastically transitive probability distribution $\widetilde{P}$ as accurately as possible (in a sense that is specified below) and consider the (unique) Kemeny median of the latter as an approximate median for $\widehat{P}_N$ (for $P$, respectively). It is legitimated by the result below, whose proof is given in the Appendix.
\begin{lemma}\label{lem:bounds}
Let $P'$ and $P^{''}$ be two probability distributions on $\mathfrak{S}_n$. 
\begin{itemize}
\item[(i)] Let $\sigma_{P''}$ ba any Kemeny median of distribution $P^{''}$. Then, we have: 
\begin{equation}
L^*_{P'}\leq L_{P'}(\sigma_{P''})\leq L^*_{P'}+2\sum_{i<j}\vert p'_{i,j}-p''_{i,j} \vert ,
\end{equation}
where $p'_{i,j}=\mathbb{P}_{\Sigma \sim P'}\{  \Sigma(i)<\Sigma(j)\}$ and $p''_{i,j}=\mathbb{P}_{\Sigma \sim P''}\{  \Sigma(i)<\Sigma(j)\}$  for any $i<j$.
\item[(ii)] Suppose that $(P',P^{''})\in \mathcal{T}^2$ and set $h=\min_{i<j}\vert p''_{i,j}-1/2\vert$. Then, we have:
\begin{equation}
d_{\tau}(\sigma^*_{P'}, \sigma^*_{P''})\leq (1/h)\sum_{i<j}\vert p'_{i,j}-p''_{i,j} \vert .
\end{equation}
\end{itemize}
\end{lemma}

\begin{remark}\label{rk:wassertein}{\sc (Mass transportation)} We point out that the quantity $\sum_{i<j}\vert p'_{i,j}-p''_{i,j} \vert$ involved in the lemma stated above can be interpreted as a Wasserstein metric with Kendall $\tau$ distance as cost function. Indeed, a natural way of measuring the distance between two probability distributions $P$ and $P'$ on $\mathfrak{S}_n$ is to compute the quantity
\begin{equation} \label{eq:metric}
D_{\tau}\left(P,P'  \right)=\inf_{\Sigma\sim P,\; \Sigma' \sim P' }\mathbb{E}\left[ d_{\tau}(\Sigma,\Sigma') \right],
\end{equation}
where the infimum is taken over all possible couplings\footnote{Recall that a coupling of two probability distributions $Q$ and $Q'$ is a pair $(U,U')$ of random variables defined on the same probability space such that the marginal distributions of $U$ and $U'$ are $Q$ and $Q'$.} $(\Sigma,\Sigma')$ of $(P,P')$, see \textit{e.g.} \citet{CJ10}. As can be shown by means of a straightforward computation (see the Appendix for further details), we have:
$
D_{\tau}(P,P' )=\sum_{i<j}\vert p'_{i,j}-p_{i,j} \vert$.
Observe that, equipped with this notation, we have: $\forall \sigma\in \mathfrak{S}_n$,
$L_{P}(\sigma)=D_{\tau}\left(P,\delta_{\sigma}  \right)$.
Hence, the Kemeny medians $\sigma^*$ of a probability distribution $P$ correspond to the Dirac distributions $\delta_{\sigma^*}$ closest to $P$ in the sense of the Wasserstein metric \eqref{eq:metric}. 
\end{remark}

We go back to the approximate Kemeny aggregation problem and suppose that it is known \textit{a priori} that the underlying probability $P$ belongs to a certain subset $\mathcal{T}'$ of $\mathcal{T}$, on which the quadratic minimization problem
\begin{equation}\label{eq:min_ls}
\min_{P'\in \mathcal{T}'}\sum_{i<j}(p'_{i,j}-\widehat{p}_{i,j})^2
\end{equation}
can be solved efficiently (by orthogonal projection typically, when $\mathcal{T}'$ is a vector space or a convex set, up to an appropriate reparametrization). In \citet{JLYY10}, the case $$\mathcal{T}'=\{ P':\;\; (p_{i,j}-1/2)+(p_{j,k}-1/2)+(p_{k,i}-1/2)=0 \text{ for all 3-tuple } (i,j,k)\}\subset \mathcal{T}$$ has been investigated at length in particular.
 Denoting by $\widetilde{P}$ the solution of \eqref{eq:min_ls}, we deduce from Lemma \ref{lem:bounds} combined with Cauchy-Schwarz inequality that
\begin{multline*}
L^*_{\widehat{P}_N}\leq L_{\widehat{P}_N}(\sigma^*_{\widetilde{P}})\leq L^*_{\widehat{P}_N}+\sqrt{2n(n-1)}\left(\sum_{i<j}(\widetilde{p}_{i,j}-\widehat{p}_{i,j})^2\right)^{1/2}\\ \leq
L^*_{\widehat{P}_N}+\sqrt{2n(n-1)}\left(\sum_{i<j}(p_{i,j}-\widehat{p}_{i,j})^2\right)^{1/2},
\end{multline*}
where the final upper bound can be easily shown to be of order $O_{\mathbb{P}}(1/\sqrt{N})$.

%Let $p''=(p^{''}_{i,j})_{i<j}$ the best collection of pairwise probabilities verifying the strict stochastic transitiviy condition, approximating  $(p'_{i,j})_{i<j}$ in the least squares sense, \textit{i.e.} such that $\sum_{i<j}(p'_{i,j}-p^{''}_{i,j})^2$ is minimum. Denoting by $\widetilde{\sigma}^*_{P'}$ the permutation defined by the $p^{''}_{i,j}$'s (namely, $\widetilde{\sigma}^*_{P'}(i)=1+\sum_{k\neq i}\mathbb{I}\{p^{''}_{i,k}<1/2  \}$ for all $i\in \n$), we deduce from Lemma \ref{lem:bounds} combined with Cauchy-Schwarz inequality that:
%\begin{equation}\label{eq:bound_DAG}
%L^*_{P'}\leq L_{P'}(\widetilde{\sigma}^*_{P'})\leq L^*_{P'}+\sqrt{2n(n-1)}\min_{p'':\; G_{p''} \text{  acyclic}}\left(\sum_{i<j}(p'_{i,j}-p^{''}_{i,j})^2\right)^{1/2}.
%\end{equation}
%When $\widehat{P}\notin \mathcal{T}$, the bound above advocates for using the permutation $\widetilde{\sigma}^*_{\widehat{P}}$ as approximate median of the underlying distribution $P$. The following theorem reveals that its excess of risk is of order $O_{\mathbb{P}}(1/\sqrt{N})$. Refer to the Appendix for the technical proof.
%\begin{theorem}\label{thm:approx_med}
%Suppose that $P\in \mathcal{T}$. Then, for all $\delta\in (0,1)$, we have with probability at least $1-\delta$: $\forall N\geq 1$,
%\begin{equation}
%L_{P}(\widetilde{\sigma}^*_{\widehat{P}})-L^*_{P}\leq \left(  n(n-1)\right)^{3/2}\sqrt{\frac{\log(n(n-1)/\delta)}{N}}
%\end{equation}
%\end{theorem}
%\noindent As shall be seen in Section~\ref{sec:main}, this regression approach to ranking aggregation shall play a crucial role in the algorithms we propose for ranking median regression (in order to compute approximate local medians more precisely), which statistical learning problem is formulated in what follows as an extension of consensus ranking. We now discuss how to compute $\widetilde{\sigma}^*_{\widehat{P}}$, given $\widehat{P}$, in practice.\\
%\noindent The issue of finding a sharp approximation of a probability distribution $P'$ by a strictly stochastically transitive distribution comes back to the problem of approximating a weighted, complete graph by a weighted, complete \textit{acyclic} graph. Indeed, any probability distribution $P''$ in $\mathcal{T}$ defines a complete directed acyclic graph (DAG) whose vertices are the objects $1,\; \ldots,\; n$ and the directed edges are defined by: $\forall i\neq j$,
%\begin{equation}\label{eq:DAG}
%i \rightarrow j \Leftrightarrow p''_{i,j}>1/2.
%\end{equation}
%The problem of finding the best collection of pairwise probabilities $p''=(p^{''}_{i,j})_{i<j}$ defining  a DAG $G_{p''}$ (see \eqref{eq:DAG}) approximating $(p'_{i,j})_{i<j}$ in the least squares sense has been adressed in \citet{JLYY10} by means of the Hodge combinatorial theory recalled in Appendix.
%is connected with the \textit{feedback set} problem for directed graphs, see \citet{DETT99} (to complete). A feedback set (FS) is a set of edges whose removal makes the directed graph acyclic. Whereas it is difficult in general to find a FS with specific properties (\textit{e.g.} a minimum cardinality FS).\\
%As shown in section 5 therein, the computation of the solution boils down to solving a $n\times n$ least-squares problem (with cost $O(n^3)$). 

\subsection{Predictive Ranking and Statistical Conditional Models}\label{subsec:cdt_models}

We suppose now that, in addition to the ranking $\Sigma$, one observes a random vector $X$, defined on the same probability space $(\Omega,\; \mathcal{F},\; \mathbb{P})$, valued in a feature space $\mathcal{X}$ (of possibly high dimension, typically a subset of $\mathbb{R}^d$ with $d\geq 1$) and modelling some information hopefully useful to predict $\Sigma$ (or at least to recover some of its characteristics). The joint distribution of the r.v. $(\Sigma,\; X)$ is described by $(\mu,\; P_X)$, where $\mu$ denotes $X$'s marginal distribution and $P_X$ means the conditional probability distribution of $\Sigma$ given $X$: $\forall \sigma\in \mathfrak{S}_n$,
$P_X(\sigma)=\mathbb{P}\{ \Sigma=\sigma \mid X\}$ almost-surely.
The marginal distribution of $\Sigma$ is then $P(\sigma)=\int_{\mathcal{X}}P_{x}(\sigma)\mu(x)$. Whereas ranking aggregation methods (such as that analyzed in Theorem \ref{thm:approx_med} from a statistical learning perspective) applied to the $\Sigma_i$'s would ignore the information carried by the $X_i$'s for prediction purpose, our goal is to learn a predictive function $s$ that maps any point $X$ in the input space to a permutation $s(X)$ in $\mathfrak{S}_n$. This problem can be seen as a generalization of multiclass classification and has been referred to as \textit{label ranking} in \citet{tsoumakas2009mining} and \citet{vembu2010label} for instance. Some approaches are rule-based (see \citet{gurrieri2012label}), while certain others adapt classic algorithms such as those investigated in section \ref{sec:main} to this problem (see \citet{YWL10}), but most of the methods documented in the literature rely on parametric modeling (see \citet{cheng2009new}, \citet{CHH09}, \citet{CDH10}). In parallel,  several authors proposed to model explicitly the dependence of the parameter $\theta$ w.r.t. the covariate $X$ and rely next on MLE or Bayesian techniques to compute a predictive rule. One may refer to \citet{rendle2009bpr} or \citet{lu2015individualized}. In contrast, the approach we develop in the next section aims at formulating the ranking regression problem, free of any parametric assumptions, in a general statistical framework. 



%This problem can be seen as a specific form of multilabel classification (see \citet{tsoumakas2009mining}). These methods have many real-world applications, such as tagging messages, categorizing items of a catalog or annotating images. Recent works considered more structured output prediction problems, assuming that each class label is presented with an ordered scale for instance, see \textit{e.g.} \citet{brinker2014graded} or \citet{cheng2010graded}. However, to the best of our knowledge, in none of these nonparametric methods, the output vector is required to be a permutation of a set of objects. 
%as classical approaches address the ranking aggregation problem by stipulating a rigid 'parametric' model $P_{\theta}$ for the distribution on $\mathfrak{S}_n$ (see \textit{e.g.} \citet{Mallows57} or \citet{fligner1986distance}),

