\section{Local Consensus Methods for Ranking Median Regression}\label{sec:main}
We start here with introducing notations to describe the class of piecewise constant ranking rules and explore next approximation of a given ranking rule $s(x)$ by elements of this class, based on a local version of the concept of ranking median recalled in the previous section. Two strategies are next investigated in order to generate adaptively a partition tailored to the training data and yielding a ranking rule with nearly minimum predictive error. 
%An iterative refinement strategy is next described to generate adaptively a partition tailored to the training data and yielding a ranking rule with nearly minimum predictive error. 
Throughout this section, for any measurable set $\mathcal{C}\subset \mathcal{X}$ weighted by $\mu(x)$, the conditional distribution of $\Sigma$ given $X\in \mathcal{C}$ is denoted by $P_{\mathcal{C}}$. When it belongs to $\mathcal{T}$, the unique median of distribution $P_{\mathcal{C}}$ is denoted by $\sigma^*_{\mathcal{C}}$ and referred to as the local median on region $\mathcal{C}$.

\subsection {Piecewise Constant Predictive Ranking Rules and Local Consensus}\label{subsec:piece}
Let $\mathcal{P}$ be a partition of $\mathcal{X}$ composed of $K\geq 1$ cells $\mathcal{C}_1,\; \ldots,\; \mathcal{C}_K$ (\textit{i.e.} the $\mathcal{C}_k$'s are pairwise disjoint and their union is the whole feature space $\mathcal{X}$). Suppose in addition that $\mu(\mathcal{C}_k)>0$ for $k=1,\; \ldots,\; K$. Using the natural embedding $\mathfrak{S}_n\subset \mathbb{R}^n$, any ranking rule $s\in \mathcal{S}$ that is constant on each subset $\mathcal{C}_k$ can be written as 
\begin{equation}\label{eq:piecewise_cst}
 s_{\mathcal{P}, \bar{\sigma}}(x)=\sum_{k=1}^K\sigma_k \cdot \mathbb{I}\{x\in \mathcal{C}_k\},
 \end{equation}
where $\bar{\sigma}=(\sigma_1,\; \ldots,\; \sigma_K)$ is a collection of $K$ permutations. We denote by $\mathcal{S}_{\mathcal{P}}$ the collection of all ranking rules that are constant on each cell of $\mathcal{P}$. Notice that $\# \mathcal{S}_{\mathcal{P}}=K\times n!$.
\medskip

 \noindent {\bf Local Ranking Medians.} The following result describes the most accurate ranking median regression function in this class. The values it takes correspond to \textit{local Kemeny medians}, \textit{i.e.} medians of the $P_{\mathcal{C}_k}$'s. The proof is straightforward and postponed to the Appendix.
\begin{proposition}\label{prop:opt_subclass}
The set $\mathcal{S}^*_{\mathcal{P}}$ of solutions of the risk minimization problem $\min_{s\in \mathcal{S}_{\mathcal{P}}}\mathcal{R}(s)$
is composed of all scoring functions $s_{\mathcal{P}, \bar{\sigma}}(x)$ such that, for all $k\in \{1,\; \ldots,\; K  \}$, the permutation $\sigma_k$ is a Kemeny median of distribution $P_{ \mathcal{C}_k}$
and 
$$
\min_{s\in \mathcal{S}_{\mathcal{P}}}\mathcal{R}(s) =\sum_{k=1}^K\mu(\mathcal{C}_k)L^*_{P_{\mathcal{C}_k}}.
$$
 If $P_{\mathcal{C}_k}\in \mathcal{T}$ for $1\leq k\leq K$, there exists a unique risk minimizer over class $\mathcal{S}_{\mathcal{P}}$ given by: $\forall x\in \mathcal{X}$, 
\begin{equation}\label{eq:piecewise_cst_opt}
 s^*_{\mathcal{P}}(x)=\sum_{k=1}^K\sigma^*_{P_{\mathcal{C}_k}} \cdot \mathbb{I}\{x\in \mathcal{C}_k\}.
 \end{equation}
\end{proposition}
 
\noindent Attention should be paid to the fact that the bound
  \begin{equation}\label{eq:bias1}
  \min_{s\in \mathcal{S}_{\mathcal{P}}}\mathcal{R}(s) - \mathcal{R}^*\leq \inf_{s\in \mathcal{S}_{\mathcal{P}}}\mathbb{E}_X\left[ d_{\tau}\left(s^*(X), s(X)\right) \right]
  \end{equation}
is valid for all $s^*\in \mathcal{S}^*$,
  shows in particular that the bias of ERM over the class $\mathcal{S}_{\mathcal{P}}$ can be controlled by the approximation rate of optimal ranking rules by elements of $\mathcal{S}_{\mathcal{P}}$ when error is measured by the integrated Kendall $\tau$ distance and $X$'s marginal distribution, $\mu(x)$ namely, is the integration measure.
 \medskip
 
 \noindent {\bf Approximation.} We now investigate to what extent ranking median regression functions $s^*(x)$ can be well approximated by predictive rules of the form \eqref{eq:piecewise_cst}. 
 %The proposition below provides the best approximation of a given ranking rule $s(x)$ in terms of $\mu$-integrated Kendall $\tau$ distance.
% \begin{proposition}\label{prop:opt}
%Let $s\in \mathcal{S}$. The best approximations to $s(x)$ from $\mathcal{S}_{\mathcal{P}}$ in terms of $\mu$-integrated Kendall $\tau$ distance are the ranking rules of the form  $s_{\mathcal{P}, \bar{\sigma}}(x)$, where $\sigma_k$ is a Kemeny median of distribution $P_{s,\mathcal{C}_k}$, the conditional distribution of the r.v. $s(X)$ given $X\in \mathcal{C}_k$, for $k\in\{1,\; \ldots,\; n \}$. The minimum error is given by:
 %$$
 %\mathcal{E}_{\mathcal{P}}(s)=\sum_{k=1}^K\mu(\mathcal{C}_k)L^*_{P_{s,\mathcal{C}_k}}.
 %$$
 %\end{proposition}
 We assume that $\mathcal{X}\subset \mathbb{R}^d$ with $d\geq 1$ and denote by $\vert\vert .\vert\vert$ any norm on $\mathbb{R}^d$. The following hypothesis is a classic smoothness assumption on the conditional pairwise probabilities.

 \begin{assumption}\label{hyp:smooth} For all $1\leq i<j\leq n$, the mapping $x\in \mathcal{X}\mapsto p_{i,j}(x)$ is Lipschitz, \textit{i.e.} there exists $M<\infty$ such that:
 \begin{equation}
\forall (x,x')\in\mathcal{X}^2,\;\; \sum_{i<j}\vert p_{i,j}(x)-p_{i,j}(x') \vert\leq M \cdot \vert\vert x-x'\vert\vert .
 \end{equation}
 \end{assumption}
 %For simplicity, we assume that $\mathcal{X}=[0,1]^d$.
  The following result shows that, under the assumptions above, the optimal prediction rule $\sigma^*_{P_X}$ can be accurately approximated by \eqref{eq:piecewise_cst_opt}, provided that the regions $\mathcal{C}_k$ are 'small' enough.
 \begin{theorem}\label{thm:approx}
 Suppose that $P_x\in \mathcal{T}$ for all $x\in \X$ and that Assumption \ref{hyp:smooth} is fulfilled. Then, we have: $\forall s_{\mathcal{P}}\in \mathcal{S}^*_{\mathcal{P}}$.
  \begin{equation}\label{eq:RMR_bound}
 \mathcal{R}(s_{\mathcal{P}})-\mathcal{R}^*\leq M\cdot \delta_{\mathcal{P}},
  \end{equation}
  where $\delta_{\mathcal{P}}=\max_{\mathcal{C}\in \mathcal{P}}\sup_{(x,x')\in \mathcal{C}^2}\vert\vert x-x'\vert\vert$ is the maximal diameter of $\mathcal{P}$'s cells. Hence, if $(\mathcal{P}_m)_{m\geq 1}$ is a sequence of partitions of $\mathcal{X}$ such that $\delta_{\mathcal{P}_m}\rightarrow 0$ as $m$ tends to infinity, then $\mathcal{R}(s_{\mathcal{P}_m})\rightarrow \mathcal{R}^*$ as $m\rightarrow \infty$.
 
 Suppose in addition that Assumption \ref{hyp:margin} is fulfilled and that $P_{\mathcal{C}}\in \mathcal{T}$ for all $\mathcal{C}\in \mathcal{P}$. Then, we have:
 \begin{equation}\label{eq:RMR_bound2}
\mathbb{E}\left[d_{\tau}\left(\sigma^*_{P_X}, s^*_{\mathcal{P}}(X)\right)  \right]\leq \sup_{x\in \mathcal{X}}d_{\tau}\left(\sigma^*_{P_x}, s^*_{\mathcal{P}}(x)\right)\leq (M/H)\cdot \delta_{\mathcal{P}}.
 \end{equation}
 %where $\delta_{\mathcal{P}}=\max_{\mathcal{C}\in \mathcal{P}}\sup_{(x,x')\in \mathcal{C}^2}\vert\vert x-x'\vert\vert$ is the maximal diameter of $\mathcal{P}$'s cells. Hence, if $(\mathcal{P}_m)_{m\geq 1}$ is a sequence of partitions of $\mathcal{X}$ such that $P_{\mathcal{C}}\in \mathcal{T}$ for all $\mathcal{C}\in \mathcal{P}_m$, $m\geq 1$, and $\delta_{\mathcal{P}_m}\rightarrow 0$ as $m$ tends to infinity, then $$\sup_{x\in \mathcal{X}}d_{\tau}\left(\sigma^*_{P_x}, s^*_{\mathcal{P}_m}(x)\right)\rightarrow 0, \text{ as } m\rightarrow \infty.$$
 \end{theorem}
 %Triangular setting for smoothness assumptions? $K$ should be much smaller than $n!$.
The upper bounds above reflect the fact that the the smaller the Lipschitz constant $M$, the easier the ranking median regression problem and that the larger the quantity $H$, the easier the recovery of the optimal RMR rule. In the Appendix, examples of distributions $(\mu(dx), P_x)$ satisfying Assumptions \ref{hyp:margin}-\ref{hyp:smooth} both at the same time are given.
\begin{remark} {\sc (On learning rates)} For simplicity, assume that $\mathcal{X}=[0,1]^d$  and that $\mathcal{P}_m$ is a partition with $m^d$ cells with diameter less than $C\times 1/m$ each, where $C$ is a constant. Provided the assumptions it stipulates are fulfilled, Theorem \ref{thm:approx} shows that the bias of the ERM method over the class $\mathcal{S}_{\mathcal{P}_m}$ is of order $1/m$. Combined with Proposition \ref{prop:upper_bound}, choosing $m\sim \sqrt{N}$ gives a nearly optimal learning rate, of order $O_{\mathbb{P}}((\log N)/N)$ namely.
\end{remark}
\begin{remark}{\sc (On smoothness assumptions)} We point out that the analysis above could be naturally refined, insofar as the accuracy of a piecewise constant median ranking regression rule is actually controlled by its capacity to approximate an optimal rule $s^*(x)$ in the $\mu$-integrated Kendall $\tau$ sense, as shown by Eq. \eqref{eq:bias1}. Like in \citet{Binev} for distribution-free regression, learning rates for ranking median regression could be investigated under the assumption that $s^*$ belongs to a certain smoothness class defined in terms of approximation rate, specifying the decay rate of $\inf_{s\in\mathcal{S}_m}\mathbb{E}[d_{\tau}(s^*(X), s(X))]$ for a certain sequence $(\mathcal{S}_m)_{m\geq 1}$ of classes of piecewise constant ranking rules. This is beyond the scope of the present paper and will be the subject of future work.
\end{remark}

The next result, proved in the Appendix section, states a very general consistency theorem for a wide class of RMR rules based on data-based partitioning, in the spirit of \citet{LN96} for classification. For simplicity's sake, we assume that $\mathcal{X}$ is compact, equal to $[0,1]^d$ say. Let $N\geq 1$, a $N$-sample partitioning rule $\pi_N$ maps any possible training sample $\mathcal{D}_N=((x_1,\sigma_1),\; \ldots,\; (x_N,\sigma_N) )\in (\mathcal{X}\times \mathfrak{S}_n)^N$ to a partition $\pi_N(\mathcal{D}_N)$ of $[0,1]^d$ composed of borelian cells. The associated collection of partitions is denoted by
$
\mathcal{F}_N=\{ \pi_N(\mathcal{D}_N):  \mathcal{D}_N\in(\mathcal{X}\times \mathfrak{S}_n)^N\}$. As in \citet{LN96}, the complexity of $\mathcal{F}_N$ is measured by the $N$-order shatter coefficient of the class of sets that can be obtained as unions of cells of a partition in $\mathcal{F}_N$, denoted by $\Delta_N(\mathcal{F}_N)$. An estimate of this quantity can be found in \textit{e.g.} Chapter 21 of \citet{DGL96}  for various data-dependent partitioning rules (including the recursive partitioning scheme described in subsection \ref{subsec:algo}, when implemented with axis-parallel splits). When $\pi_N$ is applied to a training sample $\mathcal{D}_N$, it produces a partition $\mathcal{P}_N=\pi_N(\mathcal{D}_N)$ (that is random in the sense that it depends on $\mathcal{D}_N$) associated with a RMR prediction rule: $\forall x\in \mathcal{X}$,
\begin{equation}\label{eq:rule_theor}
s_N(x)=\sum_{\mathcal{C}\in \mathcal{P}_N}\widehat{\sigma}_{\mathcal{C}}\cdot \mathbb{I}\{x\in \mathcal{C}  \}
% =\widehat{ \sigma}}_{\mathcal{C}_N(x)},
\end{equation}
where $\widehat{\sigma}_{\mathcal{C}}$ denotes a Kemeny median of the empirical version of $\Sigma$'s distribution given $X\in \mathcal{C}$, $\widehat{P}_{\mathcal{C}}=(1/N_{\mathcal{C}})\sum_{i:\; X_i\in \mathcal{C}}\delta_{\Sigma_i}$ with $N_{\mathcal{C}}=\sum_{i}\mathbb{I}\{ X_i\in \mathcal{C} \}$ and the convention $0/0=0$, for any measurable set $\mathcal{C}$ s.t. $\mu(\mathcal{C})>0$.  Notice that, although we have $\widehat{\sigma}_{\mathcal{C}}=\sigma^*_{\widehat{P}_{\mathcal{C}}}$ if $\widehat{P}_{\mathcal{C}}\in \mathcal{T}$, the rule \ref{eq:rule_theor} is somehow theoretical, since the way the Kemeny medians $\widehat{\sigma}_{\mathcal{C}}$ are obtained is not specified in general. Alternatively, using the notations of subsection \ref{subsec:consensus}, one may consider the RMR rule
\begin{equation}\label{eq:rule_pract}
\widetilde{s}_N(x)=\sum_{\mathcal{C}\in \mathcal{P}_N}\widetilde{\sigma}^*_{\widehat{P}_{\mathcal{C}}}\cdot \mathbb{I}\{x\in \mathcal{C}  \},
\end{equation}
which takes values that are not necessarily local empirical Kemeny medians but can always be easily computed. Observe incidentally that, for any $\mathcal{C}\in \mathcal{P}_N$ s.t. $\widehat{P}_{\mathcal{C}}\in \mathcal{T}$, we have $\widetilde{s}_N(x)=s_N(x)$ for all $x\in \mathcal{C}$. The theorem below establishes the consistency of these RMR rules in situations where the diameter of the cells of the data-dependent partition and their $\mu$-measure decay to zero but not too fast, with respect to the rate at which the quantity $\sqrt{N/\log (\Delta_n(\mathcal{F}_N))}$ increases. 
% and \mathcal{C}_N(x)$ is the (unique) cell of $\mathcal{P}_N$ containing $x$.
\begin{theorem}\label{th:partition_consist}
	Let $(\pi_1, \pi_2,\; \ldots)$ be a fixed sequence of partitioning rules  and for each $N$ let $\mathcal{F}_N$ be the collection of partitions associated with the $N-$sample partitioning rule $\pi_N$. Suppose that $P_x\in \mathcal{T}$ for all $x\in \X$ and that Assumption \ref{hyp:smooth} is satisfied. Assume also that the conditions below are fulfilled:
	\begin{itemize}
		\item[(i)] 
		$\lim_{n \rightarrow \infty} \log(\Delta_N(\mathcal{F}_{N}))/N=0$, %where $\Delta_N(\mathcal{F}_{N})$ is the $N$-th shattering coefficient %of 
	%	For all $N\geq 1$, the family of subsets corresponding to cells of partitions in $\mathcal{F}_N$ is of finite {\sc VC} dimension $V_N<+\infty$,
		%$\pi_N$ is based upon at most $m_N-1$ splits, where $m_N=\mathcal{O}(N/\log N)$
		% relate depth of the tree with shattering coeff.
		\item[(ii)] we have $\delta_{\mathcal{P}_N}\rightarrow 0$ in probability as $N\rightarrow \infty$ and $$
		1/\kappa_N= o_{\mathbb{P}}(\sqrt{N/\log \Delta_N(\mathcal{F}_N) })\text{ as } N\rightarrow \infty,
		$$
		where $
		\kappa_N=\inf\{ \mu(\mathcal{C}):\; \mathcal{C}\in \mathcal{P}_N\}$.
		%for all balls $S_B$ and all $\gamma>0$, $\lim_{n \rightarrow \infty} \mu(\left\{ x: diam (C_N(x) \cap S_M) > \gamma \right\})=0$,
	\end{itemize} Then any RMR rule $s_N$ of the form \eqref{eq:rule_theor} is consistent, \textit{i.e.} $\mathcal{R}(s_N)\rightarrow \mathcal{R}^*$ in probability as $N\rightarrow \infty$.\\
Suppose in addition that Assumption \ref{hyp:margin} is satisfied. Then, the RMR rule  $\widetilde{s}_N(x)$ given by $\eqref{eq:rule_pract}$ is also consistent.
\end{theorem}

%\begin{remark}\label{rk:alternative}
%As shown in the Appendix (see Theorem \ref{th:partition_consist2}), this last condition can be avoided when supposing in addition that Assumption \ref{hyp:margin} is fulfilled.
%\end{remark}
The next section presents two approaches for building a partition $\mathcal{P}$ of the predictor variable space in a data-driven fashion. The first method is a version of the nearest neighbor methods tailored to ranking median regression, whereas the second algorithm constructs $\mathcal{P}$ recursively, depending on the local variability of the $\Sigma_i$'s, and scales with the dimension of the input space.

\subsection{Nearest-Neighbor Rules for Ranking Median Regression}\label{subsec:algoNN}
Fix $k\in\{1,\; \ldots,\; N  \}$ and a query point $x \in \mathcal{X}$. The $k$-nearest neighbor RMR rule prediction $s_{k,N}(x)$ is obtained as follows. Sort the training data $(X_1, \Sigma_1), \dots, (X_n, \Sigma_n)$ by increasing order of the distance to $x$, measured, for simplicity, by $\| X_i - x \|$ for a certain norm chosen on $\mathcal{X}\subset \mathbb{R}^d$ say: 
% \begin{equation*}
$ \| X_{(1,N)} - x \|\leq \ldots\leq  \| X_{(N,N)} - x \|$.
 %\end{equation*}
 Consider next the empirical distribution calculated using the $k$ training points closest to $x$
 \begin{equation}\label{eq:local_distr}
 \widehat{P}(x)=\frac{1}{k}\sum_{l=1}^k\delta_{\Sigma_{(l,N)}}
 \end{equation}
 and then set
  \begin{equation}\label{eq:k_NNtheo}
 %s_{N}(x)=\argmin_{\sigma \in \mathfrak{S}_n} \sum_{l=1}^{k_N} d_{\tau}(\sigma, \Sigma_{(l,N)}(x))
 s_{k,N}(x)=\sigma_{\widehat{P}(x)},
 %=\sigma_{\widetilde{p}(x)}^*,
 \end{equation}
where $\sigma_{\widehat{P}(x)}$ is a Kemeny median of distribution \eqref{eq:local_distr}. Alternatively, one may compute next the pseudo-empirical Kemeny median, as described in subsection \ref{subsec:consensus}, yielding
 %$$
 %\widetilde{p}(x)= \argmin_{p'': G_{p''} acyclic}\sum_{i<j}\left(\widehat{p}_{i,j}(x)-p^{''}_{i,j}(x)\right)^2.
 %$$
 the $k$-NN prediction at $x$:
 \begin{equation}\label{eq:k_NNpract}
 %s_{N}(x)=\argmin_{\sigma \in \mathfrak{S}_n} \sum_{l=1}^{k_N} d_{\tau}(\sigma, \Sigma_{(l,N)}(x))
 \widetilde{s}_{k,N}(x)=\widetilde{\sigma}^*_{\widehat{P}(x)}.
 %=\sigma_{\widetilde{p}(x)}^*.
 \end{equation}
 Observe incidentally that $ s_{k,N}(x)= \widetilde{s}_{k,N}(x)$ when $\widehat{P}(x)$ is strictly stochastically transitive. The result stated below provides an upper bound for the expected risk excess of the RMR rules \eqref{eq:k_NNtheo} and \eqref{eq:k_NNpract}, which reflects the usual bias/variance trade-off ruled by $k$ for fixed $N$ and asymptotically vanishes as soon as $k\rightarrow \infty$ as $N\rightarrow \infty$ such that $k=o(N)$. Notice incidentally that the choice $k\sim N^{2/(d+2)}$ yields the asymptotically optimal upper bound, of order $N^{-1/(2+d)}$.

 \begin{theorem}\label{thm:NN_consist}
 Suppose that Assumption \ref{hyp:smooth} is fulfilled, that the r.v. $X$ is bounded and $d\ge 3$. Then, we have: $\forall N\geq 1$, $\forall k\in\{1,\; \ldots,\; N  \}$,
 \begin{multline}
 %\mathbb{E}\left[  \mathcal{R}(s_{k,N})-\mathcal{R}^*  \right]\leq 2 \left(\frac{n(n-1)}{2}\right)^{3/2} \left( 1/(2\sqrt{k}) + \sqrt{c_1}M \left(k/N\right)^{1/d}\right)\\ +\frac{(n(n-1))}{2}\frac{\sqrt{c_2}M}{H}\frac{1}{(N-k)^{1/d}},\\
 \mathbb{E}\left[  \mathcal{R}(s_{k,N})-\mathcal{R}^*  \right]\leq \frac{n(n-1)}{2} \left( \frac{1}{\sqrt{k}} + 2\sqrt{c_1}M \left(\frac{k}{N}\right)^{1/d} \right)\\
 \end{multline}
 where $c_1$ is a constant which only depends on $\mu$'s support.
 
 Suppose in addition that Assumption \ref{hyp:margin} is satisfied. We then have: $\forall N\geq 1$, $\forall k\in\{1,\; \ldots,\; N  \}$,
 \begin{multline}
 %\mathbb{E}\left[  \mathcal{R}(s_{k,N})-\mathcal{R}^*  \right]\leq 2 \left(\frac{n(n-1)}{2}\right)^{3/2} \left( 1/(2\sqrt{k}) + \sqrt{c_1}M \left(k/N\right)^{1/d}\right)\\ +\frac{(n(n-1))}{2}\frac{\sqrt{c_2}M}{H}\frac{1}{(N-k)^{1/d}},\\
 \mathbb{E}\left[  \mathcal{R}( \widetilde{s}_{k,N})-\mathcal{R}^*  \right]\leq \frac{n(n-1)}{2} \left( \frac{1}{\sqrt{k}} + 2\sqrt{c_1}M \left(\frac{k}{N}\right)^{1/d}\right)\left(1+n(n-1)/(4H)  \right).
 \end{multline}
 \end{theorem}
 Refer to the Appendix for the technical proof. In addition, for $d\le2$ the rate stated in Theorem \ref{thm:NN_consist} still holds true, under additional conditions on $\mu$, see the Appendix for further details. In practice, as for nearest-neighbor methods in classification/regression, the success of the technique above for fixed $N$ highly depends on the number $k$ of neighbors involved in the computation of the local prediction. The latter can be picked by means of classic model selection methods, based on data segmentation/resampling techniques. It may also crucially depend on the distance chosen (which could be learned from the data as well, see \textit{e.g.} \citet{BHS14}) and/or appropriate preprocessing stages, see \textit{e.g.} the discussion in chapter 13 of \citet{FHTbook}). The implementation of this simple local method for ranking median regression does not require to explicit the underlying partition but is classically confronted with the curse of dimensionality. The next subsection explains how another local method, based on the popular tree induction heuristic, scales with the dimension of the input space by contrast. Due to space limitations, extensions of other data-dependent partitioning methods, such as those investigated in Chapter 21 of \citet{DGL96} for instance, to local RMR are left to the reader.

\subsection{Recursive Partitioning - The {\sc CRIT} algorithm}\label{subsec:algo}
We now describe an iterative scheme for building an appropriate tree-structured partition $\mathcal{P}$, adaptively from the training data. Whereas the splitting criterion in most recursive partitioning methods is heuristically motivated (see \citet{friedman}),
the local learning method we describe below relies on the Empirical Risk Minimization principle formulated in Section \ref{sec:rmr}, so as to build by refinement a partition $\mathcal{P}$  based on a training sample $\mathcal{D}_N=\{(\Sigma_1,\; \X_1),\; \ldots,\; (\Sigma_N,\; X_N)  \}$ so that, on each cell $\mathcal{C}$ of $\mathcal{P}$, the $\Sigma_i$'s lying in it exhibit a small variability in the Kendall $\tau$ sense and, consequently, may be accurately approximated by a local Kemeny median. As shown below, the local variability measure we consider can be connected to the local ranking median regression risk (see Eq. \eqref{eq:double}) and leads to exactly the same node impurity measure as in the tree induction method proposed in \citet{YWL10}, see Remark~\ref{rk:related}. The algorithm described below differs from it in the method we use to compute the local predictions. More precisely, the goal pursued is to construct recursively a piecewise constant ranking rule associated to a partition $\mathcal{P}$, $s_{\mathcal{P}}(x)=\sum_{\mathcal{C}\in \mathcal{P}}\sigma_{\mathcal{C}}\cdot \mathbb{I}\{x\in \mathcal{C}  \}$, with minimum empirical risk
\begin{equation}\label{eq:train1}
\widehat{R}_N(s_{\mathcal{P}})=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})L_{\widehat{P}_{\mathcal{C}}}(\sigma_{\mathcal{C}}),
\end{equation}
where $\widehat{\mu}_N=(1/N)\sum_{k=1}^N\delta_{X_k}$ is the empirical measure of the $X_k$'s. The partition $\mathcal{P}$ being fixed, as noticed in Proposition \ref{prop:opt_subclass}, the quantity \eqref{eq:train1} is minimum when $\sigma_{\mathcal{C}}$ is a Kemeny median of $\widehat{P}_{\mathcal{C}}$ for all $\mathcal{C}\in \mathcal{P}$. It is then equal to
\begin{equation}\label{eq:crit_exp1}
\min_{s\in \mathcal{S}_{\mathcal{P}}}\widehat{R}_N(s)=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})L^*_{\widehat{P}_{\mathcal{C}}}.
\end{equation}
Except in the case where the intra-cell empirical distributions $\widehat{P}_{\mathcal{C}}$'s are all stochastically transitive (each $L^*_{\widehat{P}_{\mathcal{C}}}$ can be then computed using formula \eqref{eq:inf}), computing \eqref{eq:crit_exp1} at each recursion of the algorithm can be very expensive, since it involves the computation of a Kemeny median within each cell $\mathcal{C}$. We propose to measure instead the accuracy of the current partition by the quantity
\begin{equation}\label{eq:crit_exp2}
\widehat{\gamma}_{\mathcal{P}}=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}}},
\end{equation}
which satisfies the double inequality (see Remark \ref{rk:dispersion})
\begin{equation}\label{eq:double}
\widehat{\gamma}_{\mathcal{P}}\leq \min_{s\in \mathcal{S}_{\mathcal{P}}}\widehat{R}_N(s)\leq 2\widehat{\gamma}_{\mathcal{P}},
\end{equation}
and whose computation is straightforward: $\forall \mathcal{C}\in \mathcal{P}$,
\begin{equation}\label{eq:crit_exp3}
\gamma_{\widehat{P}_{\mathcal{C}}}=\frac{1}{2}\sum_{i<j}\widehat{p}_{i,j}(\mathcal{C})\left(1-\widehat{p}_{i,j}(\mathcal{C})  \right),
\end{equation}
where $\widehat{p}_{i,j}(\mathcal{C})=(1/N_{\mathcal{C}})\sum_{k:\; X_k\in \mathcal{C}}\mathbb{I}\{\Sigma_k(i)<\Sigma_k(j)  \}$, $i<j$,
denote the local pairwise empirical probabilities, with $N_{\mathcal{C}}=\sum_{k=1}^{N}\mathbb{I}\{ X_k \in \mathcal{C}\}$.
%For computational reasons (in order to avoid costly median computations at each recursive step), variability is measured through the criterion $\gamma$, rather than $L^*$, see Remark \ref{rk:dispersion}. 
%For any measurable subset $\mathcal{C}\subset \mathcal{X}$, the local empirical dispersion measure considered is
%\begin{eqnarray}
%\widehat{\gamma}(\mathcal{C})=\sum_{i<j}\widehat{p}_{i,j}(\mathcal{C})\left(1- \widehat{p}_{i,j}(\mathcal{C})  \right),
%\end{eqnarray}
%where, for all $i<j$, we set
%\begin{equation}\label{eq:p_hat}
%\widehat{p}_{i,j}(\mathcal{C})=\frac{1}{N_{\mathcal{C}}}\sum_{k=1}^N\mathbb{I}\{ X_k\in \mathcal{C},\; \Sigma_k(i)<\Sigma_k(j)   \},
%\end{equation}
%with $N_{\mathcal{C}}=\sum_{k=1}^N \mathbb{I}\{ X_k\in \mathcal{C}\}$.
%The quantity \eqref{eq:p_hat} is an empirical estimate of the dispersion measure of $\Sigma$ given $X\in \mathcal{C}$, namely $\gamma(P_{\mathcal{C}})$, see Remark \ref{rk:dispersion}.
%More precisely, we propose to solve the median ranking regression problem by means of a recursive method in the spirit of the {\sc CART} algorithm for regression, we call the {\sc CRIT} algorithm (standing for {\sc Consensus RankIng Tree}), that produces a binary tree structured partition $\mathcal{P}$ of the input space (and, consequently, a piecewise constant ranking rule, see subsection \ref{subsec:piece}) that minimizes the quantity
%\begin{equation}
%N(N-1)\widehat{\gamma}_{\mathcal{P}}= \sum_{\mathcal{C}\in \mathcal{P}}\sum_{1\leq k<l\leq N}\mathbb{I}\{ (X_k,X_l)\in \mathcal{C}^2 \}\cdot d_{\tau}\left( \Sigma_k,\Sigma_l \right),
%\end{equation}
%where $\widehat{\gamma}_{\mathcal{P}}$ is the natural empirical estimate of the intra-cell dispersion measure
%\begin{equation}
%\gamma_{\mathcal{\mathcal{P}}}\overset{def}{=}\frac 1 2 \sum_{\mathcal{C}\in \mathcal{P}}\mathbb{E}\left[d_{\tau}(\Sigma,\Sigma')\cdot \mathbb{I}\{ (X,X')\in \mathcal{C}^2 \}  \right]= \frac 1 2\sum_{\mathcal{C}\in \mathcal{P}} \mu(\mathcal{C})^2\gamma(P_{\mathcal{C}}).
%\end{equation}
 A ranking median regression tree of maximal depth $J\geq 0$ is grown as follows. One starts from the root node $\mathcal{C}_{0,0}=\mathcal{X}$. 
 %and considers as initial median ranking regression rule $s_0$ any empirical ranking median $\sigma_{0,0}$ of the observations $\Sigma_1,\; \ldots,\; \Sigma_N$. 
 At depth level $0\leq j <J$, any cell $\mathcal{C}_{j,k}$, $0 \leq k <2^j$ shall be split into two (disjoint) subsets $\mathcal{C}_{j+1,2k}$ and $\mathcal{C}_{j+1,2k+1}$, respectively identified as the left and right children of the interior leaf $(j,k)$ of the ranking median regression tree, according to the following \textit{splitting rule}.\\
 
 \noindent {\bf Splitting rule.} For any candidate left child $\mathcal{C}\subset \mathcal{C}_{j,k}$, picked in a class $\mathcal{G}$ of 'admissible' subsets (see Remark \ref{rk:perp_split}), the relevance of the split $\mathcal{C}_{j,k}=\mathcal{C}\cup (\mathcal{C}_{j,k}\setminus\mathcal{C})$ is naturally evaluated through the quantity:
 \begin{equation}
  \Lambda_{j,k}(\mathcal{C})\overset{def}{=}
  \widehat{\mu}_N(\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}}}+\widehat{\mu}_N(\mathcal{C}_{j,k}\setminus\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}_{j,k}\setminus\mathcal{C}}}.
  %=\widehat{\gamma}(\mathcal{C}) + \widehat{\gamma}(\mathcal{C}_{j,k}\setminus \mathcal{C}).
 \end{equation}
 The determination of the splitting thus consists in computing a solution $\mathcal{C}_{j+1,2k}$ of the optimization problem
 \begin{equation}\label{eq:min_pb}
 \min_{\mathcal{C}\in  \mathcal{G},\; \mathcal{C}\subset \mathcal{C}_{j,k}}\Lambda_{j,k}(\mathcal{C})
 \end{equation}
   As explained in the Appendix, an appropriate choice for class $\mathcal{G}$ permits to solve exactly the optimization problem very efficiently, in a greedy fashion.\\
   
   \noindent {\bf Local medians.} The consensus ranking regression tree is grown until depth $J$ and on each terminal leave $\mathcal{C}_{J,l}$, $0\leq l<2^J$, one computes the local Kemeny median estimate by means of the best strictly stochastically transitive approximation method investigated in subsection \ref{subsec:consensus}
   \begin{equation}
 \sigma^*_{J,l}\overset{def}{=}  \widetilde{\sigma}^*_{\widehat{P}_{\mathcal{C}_{J,l}}}.
   \end{equation}
%where $\widehat{P}_{\mathcal{C}}=(1/N_{\mathcal{C}})\sum_{l=1}^N\mathbb{I}\{  X_l\in \mathcal{C}\}\cdot\delta_{\Sigma_l}$ and $N_{\mathcal{C}}=\sum_{k=1}^N \mathbb{I}\{ X_k\in \mathcal{C}\}$ for all measurable $\mathcal{C}\subset \mathcal{X}$. 
If $\widehat{P}_{\mathcal{C}_{J,l}}\in \mathcal{T}$, $\sigma^*_{J,l}$ is straightforwardly obtained from formula \eqref{eq:sol_SST} and otherwise, one uses 
%the regression technique proposed in \citet{JLYY10}, 
the pseudo-empirical Kemeny median described in subsection \ref{subsec:consensus}. The ranking median regression rule related to the binary tree $T_{2^J}$ thus constructed is given by:
\begin{equation}
s^*_{T_{2^J}}(x)=\sum_{l=0}^{2^J-1}\sigma^*_{J,l}\mathbb{I}\{ x\in \mathcal{C}_{J,l} \}.
\end{equation}
 Its training prediction error is equal to $ \widehat{L}_N(s^*_{T_{2^J}})$,
%\begin{equation}
 %\widehat{L}_N(s^*_D)=\sum_{l=0}^{2^D-1}\widehat{\mu}_N(\mathcal{C}_{D,l})L^*_{\widehat{P}_{\mathcal{C}_{D,l}}},
 %\end{equation}
% where $\widehat{\mu}_N=(1/N)\sum_{k=1}^N\delta_{X_i}$ denotes the empirical version of $X$'s marginal distribution, 
while the training accuracy measure of the final partition is given by
 \begin{equation}
 \widehat{\gamma}_{T_{2^J}}=\sum_{l=0}^{2^J-1}\widehat{\mu}_N(\mathcal{C}_{J,l})\gamma_{\widehat{P}_{\mathcal{C}_{J,l}}}.
 %\sum_{l=0}^{2^{J-1}-1}\Lambda_{J-1,l}(\mathcal{C}_{J, 2l}).
 \end{equation}
 
 \begin{remark}\label{rk:related}
 We point out that the impurity measure \eqref{eq:crit_exp2} corresponds (up to a constant factor) to that considered in \citet{YWL10}, where it is referred to as the pairwise Gini criterion. Borrowing their notation, one may indeed write: for any measurable $\mathcal{C}\subset \mathcal{X}$, 
  $i_w^{(2)}(\mathcal{C})=8/(n(n-1))\times  \gamma_{\widehat{P}_{\mathcal{C}}}$.
 \end{remark}
 
 \noindent The tree growing stage is summarized in the Appendix, as well as the pruning procedure generally following it to avoid overfitting.  Additional comments on the advantages of this method regarding interpretability and computational feasibility can also be found in the Appendix, together with a preliminary analysis of a specific bootstrap aggregation technique that can remedy the instability of such a hierarchical predictive method. 
 %Consistency of the {\sc CRIT} algorithm can be established following in the footsteps of the argument of Theorem 21.2 in \citet{DGL96}, which applies to rules that partition the space and decide by majority vote, and its corollary Theorem 21.8 which applies to binary search trees. 
 
 
 