
\section{Ranking Median Regression}\label{sec:rmr}

Let $d$ be a metric on $\mathfrak{S}_n$, assuming that the quantity $d(\Sigma, \sigma)$ reflects the cost of predicting a value $\sigma$ for the ranking $\Sigma$, one can formulate the predictive problem that consists in finding a measurable mapping $s:\mathcal{X}\rightarrow \mathfrak{S}_n$ with minimum  prediction error:
 \begin{equation}\label{eq:reg_risk}
 \mathcal{R}(s)=\mathbb{E}_{X\sim \mu}[\mathbb{E}_{\Sigma\sim P_X}\left[ d\left(s(X),\Sigma   \right) \right]]=\mathbb{E}_{X\sim \mu}\left[ L_{P_X}(s(X)) \right].
 \end{equation}
 We denote by $\mathcal{S}$ the collection of all measurable mappings $s:\mathcal{X}\rightarrow \mathfrak{S}_n$, its elements will be referred to as \textit{predictive ranking rules}.
%Using a straightforward conditioning argument, one may write $\mathcal{R}(s)=\mathbb{E}_{X\sim \mu}\left[ L_{P_X}(\Sigma, s(X)) \right]$ for any $s\in \mathcal{S}$.
 As the minimum of the quantity inside the expectation is attained as soon as $s(X)$ is a median for $P_X$, the set of optimal predictive rules can be easily made explicit, as shown by the proposition below.
\begin{proposition}{\sc (Optimal elements)} The set $\mathcal{S}^*$ of minimizers of the risk \eqref{eq:reg_risk} is composed of all measurable mappings $s^*:\mathcal{X}\rightarrow \mathfrak{S}_n$ such that $s^*(X)\in \mathcal{M}_X$ with probability one, denoting by $\mathcal{M}_x$ the set of median rankings related to distribution $P_x$, $x\in \mathcal{X}$. 
\end{proposition}
For this reason, the predictive problem formulated above is referred to as \textit{ranking median regression} and its solutions as \textit{conditional median rankings}. It extends the ranking aggregation problem in the sense that $\mathcal{S}^*$ coincides with the set of medians of the marginal distribution $P$ when $\Sigma$ is independent from $X$.
  Equipped with the notations above, notice incidentally that the minimum prediction error can be written as $\mathcal{R}^*=\mathbb{E}_{X\sim \mu}[L^*_{P_X}]$ and that the risk excess of any $s\in \mathcal{S}$ can be controlled as follows:
  $$
  \mathcal{R}(s)-\mathcal{R}^*\leq \mathbb{E}\left[ d\left(s(X),\; s^*(X)  \right) \right],
  $$ 
for any $s^*\in \mathcal{S}^*$. We assume from now on that $d=d_{\tau}$. If $P_X\in\mathcal{T}$ with probability one, we almost-surely have $s^*(X)=\sigma^*_{P_X}$ and 
  $$
  \mathcal{R}^*=\sum_{i<j}\left\{  1/2-\int_{x\in\mathcal{X}}\left\vert p_{i,j}(x)-1/2 \right\vert \mu(dx)\right\},
  $$
  where $p_{i,j}(x)=\mathbb{P}\{\Sigma(i)<\Sigma(j) \mid X=x \}$ for all $i<j$, $x\in \mathcal{X}$. Observe also that in this case, the excess of risk is given by: $\forall s\in \mathcal{S}$,
  \begin{equation}\label{eq:excess_risk}
\mathcal{R}(s)-\mathcal{R}^*=\sum_{i<j}\int_{x\in \mathcal{X}}\vert p_{i,j}(x)-1/2 \vert\mathbb{I}\{\left(s(x)(j)-s(x)(i)\right) \left(p_{i,j}(x)-1/2\right)<0 \}\mu(dx).
  \end{equation}
  The equation above shall play a crucial role in the subsequent fast rate analysis, see Proposition \ref{prop:fast}'s proof in the Appendix.
 \medskip
 
 \noindent {\bf Statistical setting.} We assume that we observe $(X_1,\; \Sigma_1)\; \ldots,\; (X_1,\; \Sigma_N)$, $N\geq 1$ i.i.d. copies of the pair $(X,\; \Sigma)$ and, based on these training data, the objective is to build a predictive ranking rule $s$ that nearly minimizes $\mathcal{R}(s)$ over the class $\mathcal{S}$ of measurable mappings $s:\mathcal{X}\rightarrow \mathfrak{S}_n$.
 Of course, the Empirical Risk Minimization (ERM) paradigm encourages to consider solutions of the empirical minimization problem:
 \begin{equation}\label{eq:ERM}
 \min_{s\in \mathcal{S}_0}\widehat{\mathcal{R}}_N(s),
 \end{equation}
 where $\mathcal{S}_0$ is a subset of $\mathcal{S}$, supposed to be rich enough for containing approximate versions of elements of $\mathcal{S}^*$ (\textit{i.e.} so that $\inf_{s\in \mathcal{S}_0}\mathcal{R}(s)- \mathcal{R}^*$ is 'small') and ideally appropriate for continuous or greedy optimization, and
 \begin{equation}\label{eq:emp_reg_risk}
 \widehat{\mathcal{R}}_N(s)=\frac{1}{N}\sum_{i=1}^N d_{\tau}(s(X_i),\;  \Sigma_i)
 \end{equation}
 is a statistical version of \eqref{eq:reg_risk} based on the $(X_i,\Sigma_i)$'s. 
 Extending those established by \citet{CKS17} in the context of ranking aggregation, statistical results describing the generalization capacity of minimizers of \eqref{eq:emp_reg_risk} can be established under classic complexity assumptions for the class $\mathcal{S}_0$, such as the following one (observe incidentally that it is fulfilled by the class of ranking rules output by the algorithm described in subsection \ref{subsec:algo}, \textit{cf} Remark \ref{rk:perp_split}).
 
 \begin{assumption}\label{hyp:complex}
 For all $i<j$, the collection of sets
 $$
 \left\{ \left\{x\in \mathcal{X}:\; s(x)(i)-s(x)(j)>0   \right\}:\; s\in \mathcal{S}_0 \right\}\cup \left\{\left\{x\in \mathcal{X}:\; s(x)(i)-s(x)(j)<0   \right\}:\; s\in \mathcal{S}_0\right\}
 $$
 is of finite {\sc VC} dimension $V<\infty$.
 \end{assumption}
 
 \begin{proposition}\label{prop:upper_bound}
Suppose that the class $\mathcal{S}_0$ fulfills Assumption \ref{hyp:complex}. Let $\widehat{s}_N$ be any minimizer of the empirical risk \eqref{eq:emp_reg_risk} over $\mathcal{S}_0$. For any $\delta\in (0,1)$, we have with probability at least $1-\delta$: $\forall N\geq 1$,
 \begin{equation}\label{eq:ERM_bound_gen}
 \mathcal{R}(\widehat{s}_N)-\mathcal{R}^*\leq C\sqrt{\frac{V\log(n(n-1) /(2\delta))}{N}}+\left\{  \mathcal{R}^*-\inf_{s\in \mathcal{S}_0}\mathcal{R}(s) \right\},
 \end{equation}
 where $C<+\infty$ is a universal constant.
 \end{proposition}
Refer to the Appendix for the technical proof. It is also established there that the rate bound $O_{\mathbb{P}}(1/\sqrt{N})$ is sharp in the minimax sense, see Remark~\ref{rk:minimaxity}.
\medskip

\noindent {\bf Faster learning rates.} As recalled in Section~\ref{sec:background}, it is proved that rates of convergence for the excess of risk of empirical Kemeny medians can be much faster than $O_{\mathbb{P}}(1/\sqrt{N})$ under transitivity and a certain noise condition \eqref{eq:hyp_margin0}, see Theorem~\ref{thm:approx_med}. We now introduce the following hypothesis, involved in the subsequent analysis. 

\begin{assumption}\label{hyp:margin} For all $x\in \mathcal{X}$, $P_x\in \mathcal{T}$ and 
$ H=\inf_{x\in \mathcal{X}}\min_{i<j}\left\vert p_{i,j}(x)-1/2 \right\vert >0$.
\end{assumption}

\noindent This condition  generalizes condition \eqref{eq:hyp_margin0}, which corresponds to Assumption \ref{hyp:margin} when $X$ and $\Sigma$ are independent. The result stated below reveals that a similar fast rate phenomenon occurs for minimizers of the empirical risk \eqref{eq:emp_reg_risk} if Assumption \ref{hyp:margin} is satisfied. Refer to the Appendix for the technical proof. Since the goal is to give the main ideas, it is assumed for simplicity that the class $\mathcal{S}_0$ is of finite cardinality and that the optimal ranking median regression rule $\sigma^*_{P_x}$ belongs to it.

\begin{proposition}\label{prop:fast}
Suppose that Assumption \ref{hyp:margin} is fulfilled, that the cardinality of class $\mathcal{S}_0$ is equal to $C<+\infty$ and that the unique true risk minimizer $s^*(x)=\sigma^*_{P_x}$ belongs to $\mathcal{S}_0$. Let $\widehat{s}_N$ be any minimizer of the empirical risk \eqref{eq:emp_reg_risk} over $\mathcal{S}_0$. For any $\delta\in (0,1)$, we have with probability at least $1-\delta$:
\begin{equation}
\mathcal{R}(\widehat{s}_N)-\mathcal{R}^*\leq \left(\frac{n(n-1)}{2H}\right)\times \frac{\log(C/\delta)}{ N}.
\end{equation}
\end{proposition}


\noindent Regarding the minimization problem \eqref{eq:ERM}, attention should be paid to the fact that, in contrast to usual (median/quantile) regression, the set $\mathcal{S}$ of predictive ranking rules is not a vector space, which makes the design of practical optimization strategies challenging and the implementation of certain methods, based on (forward stagewise) additive modelling for instance, unfeasible (unless the constraint that predictive rules take their values in $\mathfrak{S}_n$ is relaxed, see \citet{CJ10} or \citet{FJBA13}). If $\mu$ is continuous (the $X_i$'s are pairwise distinct), it is always possible to find $s\in \mathcal{S}$ such that $\widehat{R}_N(s)=0$ and model selection/regularization issues (\textit{i.e.} choosing an appropriate class $\mathcal{S}_0$) are crucial. In contrast, if $X$ takes discrete values only (corresponding to possible requests in a search engine for instance, like in the usual 'learning to order' setting), in the set $\{1,\; \ldots,\; K \}$ with $K\geq 1$ say, the problem \eqref{eq:ERM} boils down to solving \textit{independently} $K$ empirical ranking median problems. However, $K$ may be large and it may be relevant to use some regularization procedure accounting for the possible amount of similarity shared by certain requests/tasks, adding some penalization term to \eqref{eq:emp_reg_risk}. The approach to ranking median regression we develop in this paper, close in spirit to adaptive approximation methods, relies on the concept of local learning and permits to derive practical procedures for building piecewise constant ranking rules (the complexity of the related classes $\mathcal{S}_0$ can be naturally described by the number of constant pieces involved in the predictive rules) from efficient (approximate) Kemeny aggregation (such as that investigated in subsection \ref{subsec:consensus}), when implemented at a local level. The first method is a version of the popular nearest-neighbor technique, tailored to the ranking median regression setup, while the second algorithm is inspired by the {\sc CART} algorithm and extends that introduced in \citet{YWL10}, see also Chapter 10 in \citet{AY14}.
