
\section{Local Consensus Methods for Ranking Median Regression}\label{sec:main}
We start here with introducing notations to describe the class of piecewise constant ranking rules and explore next approximation of a given ranking rule $s(x)$ by elements of this class, based on a local version of the concept of ranking median recalled in the previous section. An iterative refinement strategy is next described to generate adaptively a partition tailored to the training data and yielding a ranking rule with nearly minimum predictive error. Throughout this section, for any measurable set $\mathcal{C}\subset \mathcal{X}$ weighted by $\mu(x)$, the conditional distribution of $\Sigma$ given $X\in \mathcal{C}$ is denoted by $P_{\mathcal{C}}$. When it belongs to $\mathcal{T}$, the unique median of distribution $P_{\mathcal{C}}$ is denoted by $\sigma^*_{\mathcal{C}}$ and referred to as the local median on region $\mathcal{C}$.

\subsection {Piecewise Constant Predictive Ranking Rules and Local Consensus}\label{subsec:piece}
Let $\mathcal{P}$ be a partition of $\mathcal{X}$ composed of $K\geq 1$ cells $\mathcal{C}_1,\; \ldots,\; \mathcal{C}_K$ (\textit{i.e.} the $\mathcal{C}_k$'s are pairwise disjoint and their union is the whole feature space $\mathcal{X}$). Suppose in addition that $\mu(\mathcal{C}_k)>0$ for $k=1,\; \ldots,\; K$. Using the natural embedding $\mathfrak{S}_n\subset \mathbb{R}^n$, any ranking rule $s\in \mathcal{S}$ that is constant on each subset $\mathcal{C}_k$ can be written as 
\begin{equation}\label{eq:piecewise_cst}
 s_{\mathcal{P}, \bar{\sigma}}(x)=\sum_{k=1}^K\sigma_k \cdot \mathbb{I}\{x\in \mathcal{C}_k\},
 \end{equation}
where $\bar{\sigma}=(\sigma_1,\; \ldots,\; \sigma_K)$ is a collection of $K$ permutations. We denote by $\mathcal{S}_{\mathcal{P}}$ the collection of all ranking rules that are constant on each cell of $\mathcal{P}$. Notice that $\# \mathcal{S}_{\mathcal{P}}=K\times n!$.
\medskip

 \noindent {\bf Local Ranking Medians.} The following result describes the most accurate ranking median regression function in this class. The values it takes correspond to \textit{local Kemeny medians}, \textit{i.e.} medians of the $P_{\mathcal{C}_k}$'s. The proof is straightforward and postponed to the Supplementary Material.
\begin{proposition}\label{prop:opt_subclass}
The set $\mathcal{S}^*_{\mathcal{P}}$ of solutions of the risk minimization problem $\min_{s\in \mathcal{S}_{\mathcal{P}}}\mathcal{R}(s)$
is composed of all scoring functions $s_{\mathcal{P}, \bar{\sigma}}(x)$ such that, for all $k\in \{1,\; \ldots,\; K  \}$, the permutation $\sigma_k$ is a Kemeny median of distribution $P_{ \mathcal{C}_k}$ and 
$$
\min_{s\in \mathcal{S}_{\mathcal{P}}}\mathcal{R}(s) =\sum_{k=1}^K\mu(\mathcal{C}_k)L^*_{P_{\mathcal{C}_k}}.
$$
 If $P_{\mathcal{C}_k}\in \mathcal{T}$ for $1\leq k\leq K$, there exists a unique minimizer given by: $\forall x\in \mathcal{X}$, 
\begin{equation}\label{eq:piecewise_cst_opt}
 s^*_{\mathcal{P}}(x)=\sum_{k=1}^K\sigma^*_{P_{\mathcal{C}_k}} \cdot \mathbb{I}\{x\in \mathcal{C}_k\}.
 \end{equation}
\end{proposition}
 
\noindent Attention should be paid to the fact that the bound
  \begin{equation}\label{eq:bias1}
  \min_{s\in \mathcal{S}_{\mathcal{P}}}\mathcal{R}(s) - \mathcal{R}^*\leq \inf_{s\in \mathcal{S}_{\mathcal{P}}}\mathbb{E}_X\left[ d_{\tau}\left(s^*(X), s(X)\right) \right].
  \end{equation}
 valid for all $s^*\in \mathcal{S}^*$,
  shows in particular that the bias of ERM over the class $\mathcal{S}_{\mathcal{P}}$ can be controlled by the approximation rate of optimal ranking rules by elements of $\mathcal{S}_{\mathcal{P}}$ when error is measured by the integrated Kendall $\tau$ distance and $X$'s marginal distribution, $\mu(x)$ namely, is the integration measure.
 \medskip
 
 \noindent {\bf Approximation.} We now investigate to what extent ranking median regression functions $s^*(x)$ can be well approximated by predictive rules of the form \eqref{eq:piecewise_cst}. The proposition below provides the best approximation of a given ranking rule $s(x)$ in terms of $\mu$-integrated Kendall $\tau$ distance.
 \begin{proposition}
Let $s\in \mathcal{S}$. The best approximations to $s(x)$ from $\mathcal{S}_{\mathcal{P}}$ in terms of $\mu$-integrated Kendall $\tau$ distance are the ranking rules of the form  $s_{\mathcal{P}, \bar{\sigma}}(x)$, where $\sigma_k$ is a Kemeny median of distribution $P_{s,\mathcal{C}_k}$, the conditional distribution of the r.v. $s(X)$ given $X\in \mathcal{C}_k$, for $k\in\{1,\; \ldots,\; n \}$. The minimum error is given by:
 $$
 \mathcal{E}_{\mathcal{P}}(s)=\sum_{k=1}^K\mu(\mathcal{C}_k)L^*_{P_{s,\mathcal{C}_k}}.
 $$
 \end{proposition}
 We assume now that $\mathcal{X}\subset \mathbb{R}^d$ with $d\geq 1$ and denote by $\vert\vert .\vert\vert$ any norm on $\mathbb{R}^d$. The following hypothesis is a classic smoothness assumption on the conditional pairwise probabilities.

 \begin{assumption}\label{hyp:smooth} For all $1\leq i<j\leq n$, the mapping $x\in \mathcal{X}\mapsto p_{i,j}(x)$ is Lipschitz, \textit{i.e.} there exists $M<\infty$ such that:
 \begin{equation}
\forall (x,x')\in\mathcal{X}^2,\;\; \sum_{i<j}\vert p_{i,j}(x)-p_{i,j}(x') \vert\leq M \cdot \vert\vert x-x'\vert\vert .
 \end{equation}
 \end{assumption}
 %For simplicity, we assume that $\mathcal{X}=[0,1]^d$.
  The following result shows that, under the assumptions above, the optimal prediction rule $\sigma^*_x$ can be accurately approximated by \eqref{eq:piecewise_cst_opt}, provided that the regions $\mathcal{C}_k$ are 'small' enough.
 \begin{theorem}\label{thm:approx}
 Suppose that Assumptions \ref{hyp:margin}-\ref{hyp:smooth} are fulfilled and that $P_{\mathcal{C}}\in \mathcal{T}$ for all $\mathcal{C}\in \mathcal{P}$. Then, we have:
 \begin{equation}
\mathbb{E}\left[d_{\tau}\left(\sigma^*_{P_X}, s^*_{\mathcal{P}}(X)\right)  \right]\leq \sup_{x\in \mathcal{X}}d_{\tau}\left(\sigma^*_{P_x}, s^*_{\mathcal{P}}(x)\right)\leq (M/H)\cdot \delta_{\mathcal{P}},
 \end{equation}
 where $\delta_{\mathcal{P}}=\max_{\mathcal{C}\in \mathcal{P}}\sup_{(x,x')\in \mathcal{C}^2}\vert\vert x-x'\vert\vert$ is the maximal diameter of $\mathcal{P}$'s cells. Hence, if $(\mathcal{P}_m)_{m\geq 1}$ is a sequence of partitions of $\mathcal{X}$ such that $P_{\mathcal{C}}\in \mathcal{T}$ for all $\mathcal{C}\in \mathcal{P}_m$, $m\geq 1$, and $\delta_{\mathcal{P}_m}\rightarrow 0$ as $m$ tends to infinity, then $$\sup_{x\in \mathcal{X}}d_{\tau}\left(\sigma^*_{P_x}, s^*_{\mathcal{P}_m}(x)\right)\rightarrow 0, \text{ as } m\rightarrow \infty.$$
 \end{theorem}
 %Triangular setting for smoothness assumptions? $K$ should be much smaller than $n!$.
The next section presents a variant of the tree-based algorithm proposed in \citet{YWL10} to build recursively a partition $\mathcal{P}$ of the predictor variable space, adapted to the local variability of the $\Sigma_i$'s. 
\begin{remark} {\sc (On learning rates)} For simplicity, assume that $\mathcal{X}=[0,1]^d$  and that $\mathcal{P}_m$ is a sequence of partitions with $m^d$ cells with diameter less than $C\times 1/m$ each, where $C$ is a constant. Provided the assumptions it stipulates are fulfilled, Theorem \ref{thm:approx} shows that the bias of the ERM method over the class $\mathcal{S}_{\mathcal{P}_m}$ is of order $1/m$. Combined with Proposition \ref{prop:upper_bound}, choosing $m\sim \sqrt{N}$ gives a nearly optimal learning rate, of order $O_{\mathbb{P}}((\log N)/N)$ namely.
\end{remark}
\begin{remark}{\sc (On smoothness assumptions)} We point out that the analysis above could be naturally refined, insofar as the accuracy of a piecewise constant median ranking regression rule is actually controlled by its capacity to approximate an optimal rule $s^*(x)$ in the $\mu$-integrated Kendall $\tau$ sense, as shown by Eq. \eqref{eq:bias1}. Like in \citet{Binev} for distribution-free regression, learning rates for ranking median regression could be investigated under the assumption that $s^*$ belongs to a certain smoothness class defined in terms of approximation rate, specifying the decay rate of $\inf_{s\in\mathcal{S}_m}\mathbb{E}[d_{\tau}(s^*(X), s(X))]$ for a certain sequence $(\mathcal{S}_m)_{m\geq 1}$ of classes of piecewise constant ranking rules. This is beyond the scope of the present paper and will be the subject of future work.
\end{remark}
\subsection{Recursive Partitioning - The {\sc CRIT} algorithm}\label{subsec:algo}
Whereas the splitting criterion in most recursive partitioning methods is heuristically motivated (see \citet{friedman}),
the local learning method we describe below relies on the Empirical Risk Minimization principle formulated in subsection \ref{subsec:rmr}, so as to build by refinement a partition $\mathcal{P}$  based on a training sample $\mathcal{D}_N=\{(\Sigma_1,\; \X_1),\; \ldots,\; (\Sigma_N,\; X_N)  \}$ such that, on each cell $\mathcal{C}$ of $\mathcal{P}$, the $\Sigma_i$'s lying in it exhibit a small variability in the Kendall $\tau$ sense and, consequently, may be accurately approximated by a local Kemeny median. As shown below, the local variability measure we consider can be connected to the local ranking median regression risk (see Eq. \eqref{eq:double}) and leads to exactly the same node impurity measure as in the tree induction method proposed in \citet{YWL10}. The algorithm described below differs from it in the method we use to compute the local predictions, see Remark \ref{rk:related}. More precisely, the goal pursued is to construct recursively a piecewise constant ranking rule associated to a partition $\mathcal{P}$, $s_{\mathcal{P}}(x)=\sum_{\mathcal{C}\in \mathcal{P}}\sigma_{\mathcal{C}}\cdot \mathbb{I}\{x\in \mathcal{C}  \}$, with minimum empirical risk
\begin{equation}\label{eq:train1}
\widehat{L}_N(s_{\mathcal{P}})=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})L_{\widehat{P}_{\mathcal{C}}}(\sigma_{\mathcal{C}}),
\end{equation}
where $\widehat{\mu}_N=(1/N)\sum_{k=1}^N\delta_{X_k}$ is the empirical measure of the $X_k$'s and, for any measurable subset $\mathcal{C}\subset \mathcal{X}$, $N_{\mathcal{C}}=\sum_{k=1}^N \mathbb{I}\{ X_k\in \mathcal{C}\}$ and $\widehat{P}_{\mathcal{C}}=(1/N_{\mathcal{C}})\sum_{k: X_k\in \mathcal{C}}\delta_{\Sigma_k}$ is the empirical version of $\Sigma$'s conditional distribution given $X\in \mathcal{C}$. The partition $\mathcal{P}$ being fixed, as noticed in Proposition \ref{prop:opt_subclass}, the quantity \eqref{eq:train1} is minimum when $\sigma_{\mathcal{C}}$ is a Kemeny median of $\widehat{P}_{\mathcal{C}}$ for all $\mathcal{C}\in \mathcal{P}$. It is then equal to
\begin{equation}\label{eq:crit_exp1}
\min_{s\in \mathcal{S}_{\mathcal{P}}}\widehat{L}_N(s)=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})L^*_{\widehat{P}_{\mathcal{C}}}.
\end{equation}
Except in the case where the intra-cell empirical distributions $\widehat{P}_{\mathcal{C}}$'s are all stochastically transitive (each $L^*_{\widehat{P}_{\mathcal{C}}}$ can be then computed using formula \eqref{eq:inf}), computing \eqref{eq:crit_exp1} at each recursion of the algorithm can be very expensive, since it involves the computation of a Kemeny median within each cell $\mathcal{C}$. We propose to measure instead the accuracy of the current partition by the quantity
\begin{equation}\label{eq:crit_exp2}
\widehat{\gamma}_{\mathcal{P}}=\sum_{\mathcal{C}\in \mathcal{P}}\widehat{\mu}_N(\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}}},
\end{equation}
which satisfies the double inequality (see Remark \ref{rk:dispersion})
\begin{equation}\label{eq:double}
\widehat{\gamma}_{\mathcal{P}}\leq \min_{s\in \mathcal{S}_{\mathcal{P}}}\widehat{L}_N(s)\leq 2\widehat{\gamma}_{\mathcal{P}},
\end{equation}
and whose computation is straightforward: $\forall \mathcal{C}\in \mathcal{P}$,
\begin{equation}\label{eq:crit_exp2}
\gamma_{\widehat{P}_{\mathcal{C}}}=\frac{1}{2}\sum_{i<j}\widehat{p}_{i,j}(\mathcal{C})\left(1-\widehat{p}_{i,j}(\mathcal{C})  \right),
\end{equation}
where $\widehat{p}_{i,j}(\mathcal{C})=(1/N_{\mathcal{C}})\sum_{k:\; X_k\in \mathcal{C}}\mathbb{I}\{\Sigma_k(i)<\Sigma_k(j)  \}$, $i<j$,
denote the local pairwise empirical probabilities.
%For computational reasons (in order to avoid costly median computations at each recursive step), variability is measured through the criterion $\gamma$, rather than $L^*$, see Remark \ref{rk:dispersion}. 
%For any measurable subset $\mathcal{C}\subset \mathcal{X}$, the local empirical dispersion measure considered is
%\begin{eqnarray}
%\widehat{\gamma}(\mathcal{C})=\sum_{i<j}\widehat{p}_{i,j}(\mathcal{C})\left(1- \widehat{p}_{i,j}(\mathcal{C})  \right),
%\end{eqnarray}
%where, for all $i<j$, we set
%\begin{equation}\label{eq:p_hat}
%\widehat{p}_{i,j}(\mathcal{C})=\frac{1}{N_{\mathcal{C}}}\sum_{k=1}^N\mathbb{I}\{ X_k\in \mathcal{C},\; \Sigma_k(i)<\Sigma_k(j)   \},
%\end{equation}
%with $N_{\mathcal{C}}=\sum_{k=1}^N \mathbb{I}\{ X_k\in \mathcal{C}\}$.
%The quantity \eqref{eq:p_hat} is an empirical estimate of the dispersion measure of $\Sigma$ given $X\in \mathcal{C}$, namely $\gamma(P_{\mathcal{C}})$, see Remark \ref{rk:dispersion}.
%More precisely, we propose to solve the median ranking regression problem by means of a recursive method in the spirit of the {\sc CART} algorithm for regression, we call the {\sc CRIT} algorithm (standing for {\sc Consensus RankIng Tree}), that produces a binary tree structured partition $\mathcal{P}$ of the input space (and, consequently, a piecewise constant ranking rule, see subsection \ref{subsec:piece}) that minimizes the quantity
%\begin{equation}
%N(N-1)\widehat{\gamma}_{\mathcal{P}}= \sum_{\mathcal{C}\in \mathcal{P}}\sum_{1\leq k<l\leq N}\mathbb{I}\{ (X_k,X_l)\in \mathcal{C}^2 \}\cdot d_{\tau}\left( \Sigma_k,\Sigma_l \right),
%\end{equation}
%where $\widehat{\gamma}_{\mathcal{P}}$ is the natural empirical estimate of the intra-cell dispersion measure
%\begin{equation}
%\gamma_{\mathcal{\mathcal{P}}}\overset{def}{=}\frac 1 2 \sum_{\mathcal{C}\in \mathcal{P}}\mathbb{E}\left[d_{\tau}(\Sigma,\Sigma')\cdot \mathbb{I}\{ (X,X')\in \mathcal{C}^2 \}  \right]= \frac 1 2\sum_{\mathcal{C}\in \mathcal{P}} \mu(\mathcal{C})^2\gamma(P_{\mathcal{C}}).
%\end{equation}
 A ranking median regression tree of maximal depth $J\geq 0$ is grown as follows. One starts from the root node $\mathcal{C}_{0,0}=\mathcal{X}$. 
 %and considers as initial median ranking regression rule $s_0$ any empirical ranking median $\sigma_{0,0}$ of the observations $\Sigma_1,\; \ldots,\; \Sigma_N$. 
 At depth level $0\leq j <J$, any cell $\mathcal{C}_{j,k}$, $0 \leq k <2^j$ shall be split into two (disjoint) subsets $\mathcal{C}_{j+1,2k}$ and $\mathcal{C}_{j+1,2k+1}$, respectively identified as the left and right children of the interior leaf $(j,k)$ of the ranking median regression tree, according to the following \textit{splitting rule}.\\
 
 \noindent {\bf Splitting rule.} For any candidate left child $\mathcal{C}\subset \mathcal{C}_{j,k}$, picked in a class $\mathcal{G}$ of 'admissible' subsets (see Remark \ref{rk:perp_split}), the relevance of the split $\mathcal{C}_{j,k}=\mathcal{C}\cup (\mathcal{C}_{j,k}\setminus\mathcal{C})$ is naturally evaluated through the quantity:
 \begin{equation}
  \Lambda_{j,k}(\mathcal{C})\overset{def}{=}
  \widehat{\mu}_N(\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}}}+\widehat{\mu}_N(\mathcal{C}_{j,k}\setminus\mathcal{C})\gamma_{\widehat{P}_{\mathcal{C}_{j,k}\setminus\mathcal{C}}}.
  %=\widehat{\gamma}(\mathcal{C}) + \widehat{\gamma}(\mathcal{C}_{j,k}\setminus \mathcal{C}).
 \end{equation}
 The determination of the splitting thus consists in computing a solution $\mathcal{C}_{j+1,2k}$ of the optimization problem
 \begin{equation}\label{eq:min_pb}
 \min_{\mathcal{C}\in  \mathcal{G},\; \mathcal{C}\subset \mathcal{C}_{j,k}}\Lambda_{j,k}(\mathcal{C})
 \end{equation}
   
   
   \noindent {\bf Local medians.} The consensus ranking regression tree is grown until depth $J$ and on each terminal leave $\mathcal{C}_{J,l}$, $0\leq l<2^J$, one computes the local Kemeny median estimate by means of the best strictly stochastically transitive approximation method investigated in subsection \ref{subsec:consensus}
   \begin{equation}
 \sigma^*_{J,l}\overset{def}{=}  \widetilde{\sigma}^*_{\widehat{P}_{\mathcal{C}_{J,l}}}.
   \end{equation}
%where $\widehat{P}_{\mathcal{C}}=(1/N_{\mathcal{C}})\sum_{l=1}^N\mathbb{I}\{  X_l\in \mathcal{C}\}\cdot\delta_{\Sigma_l}$ and $N_{\mathcal{C}}=\sum_{k=1}^N \mathbb{I}\{ X_k\in \mathcal{C}\}$ for all measurable $\mathcal{C}\subset \mathcal{X}$. 
If $\widehat{P}_{\mathcal{C}_{J,l}}\in \mathcal{T}$, $\sigma^*_{J,l}$ is straightforwardly obtained from formula \eqref{eq:sol_SST} and otherwise, one uses the regression technique proposed in \citet{JLYY10}, see subsection \ref{subsec:consensus}.

The ranking median regression rule related to the binary tree $T_{2^J}$ thus constructed is given by:
\begin{equation}
s^*_{T_{2^J}}(x)=\sum_{l=0}^{2^J-1}\sigma^*_{J,l}\mathbb{I}\{ x\in \mathcal{C}_{J,l} \}.
\end{equation}
 Its training prediction error is equal to $ \widehat{L}_N(s^*_{T_{2^J}})$,
%\begin{equation}
 %\widehat{L}_N(s^*_D)=\sum_{l=0}^{2^D-1}\widehat{\mu}_N(\mathcal{C}_{D,l})L^*_{\widehat{P}_{\mathcal{C}_{D,l}}},
 %\end{equation}
% where $\widehat{\mu}_N=(1/N)\sum_{k=1}^N\delta_{X_i}$ denotes the empirical version of $X$'s marginal distribution, 
while the training accuracy measure of the final partition is given by
 \begin{equation}
 \widehat{\gamma}_{T_{2^J}}=\sum_{l=0}^{2^J-1}\widehat{\mu}_N(\mathcal{C}_{J,l})\gamma_{\widehat{P}_{\mathcal{C}_{J,l}}}.
 %\sum_{l=0}^{2^{J-1}-1}\Lambda_{J-1,l}(\mathcal{C}_{J, 2l}).
 \end{equation}
 
 \begin{remark}\label{rk:related}
 We point out that the impurity measure \eqref{eq:crit_exp2} corresponds (up to a constant factor) to that considered in \citet{YWL10}, where it is referred to as the pairwise Gini criterion. Borrowing their notation, one may indeed write: for any measurable $\mathcal{C}\subset \mathcal{X}$, 
  $i_w^{(2)}(\mathcal{C})=8/(n(n-1))\times  \gamma_{\widehat{P}_{\mathcal{C}}}$.
 \end{remark}
 
 
\noindent {\bf Pruning the ranking median regression tree.} The tree growing stage is summarized in the Supplementary Material. Of course, if class $\mathcal{G}$ is sufficiently rich to yield flexible partitions (see Remark \ref{rk:perp_split}) and $J$ is large enough (such that $2^J\geq N$), one may obtain $ \widehat{\gamma}_J=0$, no more than a single data point lying in each terminal cell. It is well-known that recursive partitioning methods fragment the data, the splitting process becoming more and more unstable as depth level $j$ increases. For this reason, the tree growing stage is generally followed by a pruning procedure. From the original tree $T_{2^J}$, one recursively merges children of a same parent node until the root $T_1$ is reached in a bottom up fashion. Precisely, the \textit{weakest link pruning} consists here in sequentially merging the children $\mathcal{C}_{j+1,2l}$ and $\mathcal{C}_{j+1,2l+1}$ producing the smallest dispersion increase: $$\widehat{\mu}_N(\mathcal{C}_{j,l})\gamma_{\widehat{P}_{\mathcal{C}_{j,l}}}-\Lambda_{j,l}(\mathcal{C}_{j+1,2l}).$$
One thus obtains a sequence of ranking median regression trees $T_{2^J}\supset T_{2^J-1}\supset \cdots \supset T_1$, the subtree $T_m$ corresponding to a partition with $\# T_m=m$ cells. The final subtree $T$ is selected by minimizing the complexity penalized intra-cell dispersion:
\begin{equation}
\widetilde{\gamma}_{T}=\widehat{\gamma}_{T}+\lambda\times \#T,
\end{equation}
where $\lambda\geq 0$ is a parameter that rules the trade-off between the complexity of the ranking median regression tree, as measured by $\#T$, and intra-cell dispersion. In practice, model selection can be performed by means of common resampling techniques.
 
 \begin{remark}{\sc (Early stopping)} One stops the splitting process if no improvement can be achieved by splitting the current node $\mathcal{C}_{j,l}$, \textit{i.e.} if $\min_{\mathcal{C}\in \mathcal{G}}\Lambda(\mathcal{C})=\sum_{1\leq k<l\leq N}\mathbb{I}\{ (X_k,X_l)\in \mathcal{C}_{j,l}^2 \}\cdot d_{\tau}\left( \Sigma_k,\Sigma_l \right)$ (one then set $\mathcal{C}_{j+1,2l}=\mathcal{C}_{j,l}$ by convention), or if a minimum node size, specified in advance, is attained.
 \end{remark}
 \begin{remark}\label{rk:perp_split} {\sc (On class $\mathcal{G}$)} The choice of class $\mathcal{G}$ involves a trade-off between computational cost and flexibility: a rich class (of controlled complexity though) may permit to capture the conditional variability of $\Sigma$ given $X$ appropriately but might significantly increase the cost of solving \eqref{eq:min_pb}. Typically, as proposed in \citet{cart84}, subsets can be built by means of axis parallel splits, leading to partitions whose cells are finite union of hyperrectangles. This corresponds to the case where $\mathcal{G}$ is stable by intersection, \textit{i.e.} $\forall (\mathcal{C},\mathcal{C}')\in \mathcal{G}^2$, $\mathcal{C}\cap \mathcal{C}'\in \mathcal{G}$) and admissible subsets of any $\mathcal{C}\in \mathcal{G}$ are of the form $\mathcal{C}\cap \{X^{(m)}\geq s  \}$ or $\mathcal{C}\cap \{X^{(m)}\leq s  \}$, where $X^{(m)}$ can be any component of $X$ and $s\in \mathbb{R}$ any threshold value. In this case, the minimization problem can be efficiently solved by means of a double loop (over the $d$ coordinates of the input vector $X$ and over the data lying in the current parent node), see \textit{e.g.} \citet{cart84}.
\end{remark}

\noindent {\bf Interpretability and computational feasability.} The fact that the computation of (local) Kemeny medians takes place 
at the level of terminal nodes of the ranking median regression tree $T$ only makes the {\sc CRIT} algorithm very attractive from a practical perspective. In addition, it produces predictive rules that can be easily interpreted by means of a binary tree graphic representation and, when implemented with axis parallel splits, provides, as a by-product, indicators quantifying the impact of each input variable. The relative importance of the variable $X^{(m)}$ can be measured by summing the decreases of empirical $\gamma$-dispersion induced by all splits involving it as splitting variable. More generally, the {\sc CRIT} algorithm inherits the appealing properties of tree induction methods: it easily adapts to categorical predictor variables, training and prediction are fast and it is not affected by monotone transformations of the predictor variables $X^{(m)}$.
\medskip

\noindent {\bf Aggregation.} Just like other tree-based methods, the {\sc CRIT} algorithm may suffer from instability, meaning that, due to its hierarchical structure, the rules it produces can be much affected by a small change in the training dataset. As proposed in \citet{Br96}, {\bf b}oostrap {\bf agg}regat{\bf ing} techniques may remedy to instability of ranking median regression trees. Applied to the {\sc CRIT} method, bagging consists in generating $B\geq 1$ bootstrap samples by drawing with replacement in the original data sample and running next the learning algorithm from each of these training datasets, yielding $B$ predictive rules $s_1,\; \ldots,\; s_B$. For any prediction point $x$, the ensemble of predictions $s_1(x),\; \ldots,\; s_B(x)$ are combined in the sense of Kemeny ranking aggregation, so as to produce a consensus $\bar{s}_B(x)$ in $\mathfrak{S}_n$. Observe that a crucial advantage of dealing with piecewise constant ranking rules is that computing a Kemeny median for each new prediction point can be avoided: one may aggregate the ranking rules rather than the rankings in this case, refer to the Supplementary Material for further details. We finally point out that a certain amount of randomization can be incorporated in each bootstrap tree growing procedure, following in the footsteps of the random forest procedure proposed in \citet{Br01}, so as to increase flexibility and hopefully improve accuracy.
