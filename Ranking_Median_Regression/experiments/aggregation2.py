#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 17:02:47 2017

@author: Anna
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from random import *
import numpy as np
from Mallows import ML_distribution
#from crit_algorithm import *
from randomized_crit import *
from Plackett import *
from copeland import *

from sklearn.cluster import KMeans
import warnings
warnings.filterwarnings("ignore")


#############################
#                           #
#      Evaluation           #
#                           #
#############################
n_samples=1000

max_depth=3
min_size = int(n_samples/float(max_depth*20))


n_features=3



#########################
#                       #
#        Data           #
#                       #
#########################


def sample_from(d):
    r = uniform(0, sum(d.itervalues()))
    s = 0.0
    for k, w in d.iteritems():
        s += w
        if r < s: return k
    return k





n_experiments=10

c=0
for n_items in [8]:
    list_results1=[] # tree alone
    list_results2=[]

     
    #list_results1bis=[] # agg Borda
    list_results1bisbis=[] # agg Copeland
    
    #list_results2=[]
    #list_results2bis=[]
    #list_results2bisbis=[]
    
    items=range(n_items)
    n_pairs=int((n_items*(n_items-1))/float(2))
    index_pairs=range(n_features, n_features+n_pairs)

    for i in range(n_experiments):
        c+=1
        print 'n expe '+str(c)
        
        # Numerical features 
        index_categorical=[]
        index_discrete=[]
        index_numerical=[0,1,2]
        X1=np.random.random(n_samples)
        X2=np.random.random(n_samples)
        X3=np.random.random(n_samples)

        
        sigma1=tuple(np.random.permutation(range(0,n_items)))
        sigma2=tuple(np.random.permutation(range(0,n_items)))
        sigma3=tuple(np.random.permutation(range(0,n_items)))
        sigma4=tuple(np.random.permutation(range(0,n_items)))
        sigma5=tuple(np.random.permutation(range(0,n_items)))
        sigma6=tuple(np.random.permutation(range(0,n_items)))
        
    
          
        #print 'mallows 1'
        
        spread=2
        ML1=ML_distribution(sigma1,spread)
        ML2=ML_distribution(sigma2,spread)
        ML3=ML_distribution(sigma3,spread)
        ML4=ML_distribution(sigma4,spread)
        ML5=ML_distribution(sigma5,spread)
        ML6=ML_distribution(sigma6,spread)
    
        Y1=[]
        for i in range(n_samples):
            if (X1[i]<.3) and (X2[i]<.5):
                sigma=sample_from(ML1)
            elif (X1[i]<.3) and (X2[i]>.5): 
                sigma=sample_from(ML2)
            elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<.3):
                sigma=sample_from(ML3)
            elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]>.3):    
                sigma=sample_from(ML4)
            elif (X1[i]>.85) and (X2[i]<.7):
                sigma=sample_from(ML5)
            else:
                sigma=sample_from(ML6)
            Y1.append(sigma)
    
        orders1=[]
        for i in range(n_samples):
            preferences=[]
            l=Y1[i]
            for (i,j) in combinations(items,2):
                if l.index(i)<l.index(j):
                    preferences.append(1)
                else:
                    preferences.append(0)
            preferences.append(tuple(l))
            orders1.append(preferences)
        dataset1=[[X1[i],X2[i],X3[i]]+orders1[i] for i in range(n_samples)]

        # train test
        data=dataset1
        
        index=int(0.7*len(data))
        train_set=data[:index]
        test_set=data[index:]
        
        
        tree = build_tree(train_set, max_depth, min_size, index_categorical, index_discrete, index_pairs, 1)
        trees=[]
        n_models=10
        for i in range(n_models):
            train_setbis=[random.choice(train_set) for i in range(int(0.9*len(train_set)))]
            treebis=build_tree(train_setbis, max_depth, min_size, index_categorical, index_discrete, index_pairs, 1)
            trees.append(treebis)
        
        predicted_tree = list()
        predicted_tree_agg = list()
        predicted_tree_agg2 = list()
        
        distance2=list()
        distance2bis=list()
        distance2bisbis=list()

        for row in test_set:
            prediction_tree = predict(tree, row[:n_features],index_categorical, index_discrete, 1)
            predicted_tree.append(prediction_tree)
            predictions_row = list()
            for i in range(n_models):
                tree_to_agg=trees[i]
                prediction_model = predict(tree_to_agg, row[:n_features],index_categorical, index_discrete, 1)
                predictions_row.append(prediction_model)
            P_row=dict(Counter(predictions_row)) 
            for s in P_row.keys():
                P_row[s]=(P_row[s])/(n_models*1.0)
            #prediction_agg=Borda_countbis(P_row)[0]
            prediction_agg2=Copelandbis(P_row)[0]
            #if not prediction_agg==prediction_tree:
            #    print 'prediction tree '+str(prediction_tree)
            #    print 'prediction agg '+str(prediction_agg)
            #    print 'predictions to agg '+str(Counter(predictions_row))
            #    print 'Copeland gives '+str(Copelandbis(P_row))
            #    print 'Borda gives '+str(Borda_countbis(P_row))
            #    print 'actual '+str(row[-1])
            #    print '\n'
            #predicted_tree_agg.append(prediction_agg)
            predicted_tree_agg2.append(prediction_agg2)
 
            
            
        actual = [row[-1] for row in test_set]
        accuracy_tree = accuracy_kendall_tau(actual, predicted_tree)
        list_results1.append(accuracy_tree)
        #accuracy_tree_agg = accuracy_kendall_tau(actual, predicted_tree_agg)
        #list_results1bis.append(accuracy_tree_agg)
        accuracy_tree_agg2 = accuracy_kendall_tau(actual, predicted_tree_agg2)
        list_results1bisbis.append(accuracy_tree_agg2)

            
        
    print 'n items' +str(n_items)
    print 'results of tree vs agg \n'
    #print 'pw'
    #print list_results0
    #print np.mean(list_results0)
    #print list_results0bis
    #print np.mean(list_results0bis)

    
    print 'mallows1'
    #print list_results1
    print 'one tree \n'
    print str(np.mean(list_results1))+' +/ - '+str(np.std(list_results1))
    print '\n'
    #print list_results1bis
    #print 'Borda'
    #print str(np.mean(list_results1bis))+' +/ - '+str(np.std(list_results1bis))
    #print '\n'
    #print list_results1bisbis
    print 'Copeland \n'
    print str(np.mean(list_results1bisbis))+' +/ - '+str(np.std(list_results1bisbis))

