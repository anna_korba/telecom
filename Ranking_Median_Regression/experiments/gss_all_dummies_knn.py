#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from random import *
import pandas as pd
import numpy as np
#from crit_algorithm import *
from sklearn import preprocessing
from knn_algorithm import knn_prediction
import random


#############################
#                           #
#         Data              #
#                           #
#############################


#df_original=pd.read_stata('data/GSS/GSS7216_R1a.dta', convert_categoricals=False, convert_missing=False)

df=df_original

#df=df_original[(df_original['year']==1973) |(df_original['year']==1985) | (df_original['year']==2006)]

print 'df original '+str(df.shape)

columns_job=['jobinc', 'jobsec', 'jobhour', 'jobpromo', 'jobmeans']

df = df[np.isfinite(df['jobinc'])]
df = df[np.isfinite(df['jobsec'])]
df = df[np.isfinite(df['jobhour'])]

df = df[np.isfinite(df['jobpromo'])]
df = df[np.isfinite(df['jobmeans'])]

n_items=len(columns_job)
print 'shape after job ' +str(df.shape)

columns_to_keep_categorical=['wrkstat','wrkslf','occ10', 'sex','race' ,'marital']

columns_to_keep_discrete=['cohort', 'degree', 'income' ,'childs', 'hompop']


columns_to_keep_numerical=[]

columns_to_keep1= columns_to_keep_categorical+columns_to_keep_discrete+columns_to_keep_numerical+ columns_job

df=df[columns_to_keep1]


# drop na

df=df[np.isfinite(df)]
df=df.dropna( axis=0, how='any', thresh=None, subset=None)
print 'final shape '+str(df.shape)



# OCCUPATION

df.occ10[(df.occ10 >=10) & (df.occ10 <= 430)] =1
df.occ10[(df.occ10 >=500) & (df.occ10 <= 950)] =2
df.occ10[(df.occ10 >=1000) & (df.occ10 <= 3540)] =3
df.occ10[(df.occ10 >=3600) & (df.occ10 <= 4650)] =4
df.occ10[(df.occ10 >=4700) & (df.occ10 <= 5940)] =5
df.occ10[(df.occ10 >=6000) & (df.occ10 <= 7630)] =6
df.occ10[(df.occ10 >=7700) & (df.occ10 <= 9830)] =7
df.occ10[df.occ10 ==9997] =8


# COHORT

df.cohort[(df.cohort >=1883) & (df.cohort <= 1889)] =1
df.cohort[(df.cohort >1889) & (df.cohort <= 1899)] =2
df.cohort[(df.cohort >1899) & (df.cohort <= 1909)] =3
df.cohort[(df.cohort >1909) & (df.cohort <= 1919)] =4
df.cohort[(df.cohort >1919) & (df.cohort <= 1929)] =5
df.cohort[(df.cohort >1929) & (df.cohort <= 1939)] =6
df.cohort[(df.cohort >1939) & (df.cohort <= 1949)] =7
df.cohort[(df.cohort >1949) & (df.cohort <= 1959)] =8
df.cohort[(df.cohort >1959) & (df.cohort <= 1969)] =9
df.cohort[(df.cohort >1969) & (df.cohort <= 1979)] =10
df.cohort[(df.cohort >1979) & (df.cohort <= 1989)] =11
df.cohort[(df.cohort >1989) & (df.cohort <= 1999)] =12


df=pd.get_dummies(df, prefix=columns_to_keep_categorical, columns=columns_to_keep_categorical)


items=range(0,n_items)

job_orders=[]
for m in range(df.shape[0]):
    row=list(df[columns_job].iloc[m,:])
    preferences=[]
    l=sorted(range(len(row)), key=lambda k: row[k])
    #for (i,j) in combinations(items,2):
    #    if l.index(i)<l.index(j):
    #        preferences.append(1)
    #    else:
    #        preferences.append(0)
    preferences.append(tuple(l))
    job_orders.append(preferences)

df.drop(columns_job, axis=1, inplace=True)
scaler = preprocessing.MinMaxScaler()
df= scaler.fit_transform(df)
print 'df shape, number of features'+str(df.shape)


#job_users=df.values.tolist()
job_users=df.tolist()
n_features=len(job_users[0])


dataset=[job_users[i]+job_orders[i] for i in range(len(job_orders))]



# study
shuffle(dataset)
#data=list(random.sample(dataset,1000))
#data=dataset
index=int(0.75*len(data))
train_set=data[:index]
test_set=data[index:]

n_tests=10
list_results_final=[]
for k in range(1,100,10):
    print k
    results=[]
    for i in range(n_tests):
        #random.shuffle(data)
        data=list(random.sample(dataset,2000))
        print 'data done'
        index=int(0.75*len(data))
        train_set=data[:index]
        test_set=data[index:]
        predicted_knn = list()
        for row in test_set:
            prediction=knn_prediction(train_set,row[:n_features],k)
            predicted_knn.append(prediction)
        actual = [row[-1] for row in test_set]
        accuracy_knn = accuracy_kendall_tau(actual, predicted_knn)
        results.append(accuracy_knn)
    print 'k :'+str(k)+' accuracy :'+str(np.mean(results))
    print results
    list_results_final.append(np.mean(results))

x=range(1,100,10)
y=list_results_final
plt.plot(x,y)
