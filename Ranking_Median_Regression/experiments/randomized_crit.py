#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 10:26:54 2017

@author: Anna
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from random import *
import pandas as pd
import numpy as np
from itertools import *
from collections import Counter
from copeland import Copeland, Borda_count, Borda_countbis, Plackett_count
import random

#############################
#                           #
#    Create split           #
#                           #
#############################

def compute_gamma(group, index_pairs):
    gamma=0
    size=len(group) 
    if size==0:
        gamma=np.inf
    else:
        for m in index_pairs:
            proportion= [row[m] for row in group].count(1) / float(size)
            gamma += (proportion * (1.0 - proportion))/float(2)
    return gamma

def entropy_index(groups, index_pairs): 
    entropy=0 
    i=0
    for group in groups:
        size=len(group) 
        gamma=compute_gamma(group, index_pairs)
        entropy+=size*gamma
        i+=1
    return entropy



def test_split(index, value, dataset):
	left, right = list(), list()
	for row in dataset:
		if row[index] < value:
			left.append(row)
		else:
			right.append(row)
	return left, right

# Split a dataset based on an attribute and an attribute value
def test_split_categorical_dummy(index, value, dataset):
	left, right = list(), list()
	for row in dataset:
		if row[index] ==value:
			left.append(row)
		else:
			right.append(row)
	return left, right


def get_split_dummy(dataset, min_size, index_categorical, index_discrete, index_pairs):
    n_features=len(dataset[0])-len(index_pairs)-1
    b_index, b_value, b_score, b_groups = 999, 999, np.inf, None
    randomized_index=[choice(range(n_features))]
    for index in randomized_index: #index i of the variable for the split
        if index in index_categorical:
            values=np.unique([row[index] for row in dataset])
            for value in values:
                groups = test_split_categorical_dummy(index, value, dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups, index_pairs)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, value, entropy, groups
        elif index in index_discrete:
            values=np.unique([row[index] for row in dataset])
            for value in values:
                groups = test_split(index, value, dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups, index_pairs)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, value, entropy, groups        
        else:
            for row in dataset: # value of X_i for the split
                groups = test_split(index, row[index], dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups, index_pairs)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, row[index], entropy, groups
    #if b_groups is not None:
    #    print 'finally split with index '+str(b_index)+ 'and value '+str(b_value) + \
    #    ' and entropy '+str(np.round(b_score,5))
    return {'index':b_index, 'value':b_value, 'groups':b_groups}


def test_split_categorical_greedy(index, subset, dataset):
	left, right = list(), list()
	for row in dataset:
		if row[index] in subset:
			left.append(row)
		else:
			right.append(row)
	return left, right

def get_split_greedy(dataset, min_size, index_categorical, index_discrete, index_pairs):
    n_features=len(dataset[0])-len(index_pairs)-1
    b_index, b_value, b_score, b_groups = 999, 999, np.inf, None
    randomized_index=[choice(range(n_features))]
    for index in randomized_index: #index i of the variable for the split
        if index in index_categorical:
            values=set(np.unique([row[index] for row in dataset]))
            possible_subsets= [z for z  in chain.from_iterable(combinations(values,r) for r in range(len(values)+1))]
            for subset in possible_subsets:
                groups = test_split_categorical_greedy(index, subset, dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups, index_pairs)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, subset, entropy, groups
        elif index in index_discrete:
            values=np.unique([row[index] for row in dataset])
            for value in values:
                groups = test_split(index, value, dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups, index_pairs)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, value, entropy, groups        
        else:
            #print 'other'
            for row in dataset: # value of X_i for the split
                groups = test_split(index, row[index], dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups, index_pairs)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, row[index], entropy, groups
    #if b_groups is not None:
        #print 'finally split with ' +str(df.columns[b_index])+' and value '+str(b_value) + ' and entropy '+str(b_score)
        #print len(b_groups[0]), len(b_groups[1])
    return {'index':b_index, 'value':b_value, 'groups':b_groups}

def get_split(dataset, min_size, index_categorical, index_discrete, index_pairs,greedy):
    if greedy==0:
        return get_split_dummy(dataset, min_size, index_categorical, index_discrete, index_pairs)
    else:
        return get_split_greedy(dataset, min_size, index_categorical, index_discrete, index_pairs)

#############################
#                           #
#    Build Tree             #
#                           #
#############################

# Create a terminal node value
def to_terminal(group):
    outcomes = [row[-1] for row in group]
    P=dict(Counter(outcomes)) 
    Nsample=len(outcomes)
    for s in P.keys():
        P[s]=(P[s])/(Nsample*1.0)
    consensus=Copeland(P)
    return consensus

max_entropies=[]        

# Create child splits for a node or make terminal
def split(node, max_depth, min_size, depth, index_categorical, index_discrete, index_pairs, greedy):
    left, right = node['groups']
    #print 'len left, len right '+str(len(left))+' , '+str(len(right))
    #print 'depth '+str(depth)
    del(node['groups'])
    # check for a no split
    if not left or not right:
        node['left'] = node['right'] = to_terminal(left + right)
        return
	# check for max depth
    if depth >= max_depth:
        groups=left, right
        entropy=entropy_index(groups, index_pairs)
        #print 'max depth here, entropy : '+str(entropy) 
        max_entropies.append(entropy)
        node['left'], node['right'] = to_terminal(left), to_terminal(right)
        return
	# process left child
    if len(left) <= min_size:
        node['left'] = to_terminal(left)
    else:
        splitt=get_split(left, min_size, index_categorical, index_discrete, index_pairs, greedy)
        if splitt['groups'] is not None:
            node['left'] = splitt
            split(node['left'], max_depth, min_size, depth+1,index_categorical, index_discrete, index_pairs, greedy)
        else:
            node['left'] = to_terminal(left)
	# process right child
    if len(right) <= min_size:
        node['right'] = to_terminal(right)
    else:
        splitt=get_split(right, min_size,index_categorical, index_discrete, index_pairs, greedy)
        if splitt['groups'] is not None:
            node['right'] = splitt
            split(node['right'], max_depth, min_size, depth+1,index_categorical, index_discrete, index_pairs, greedy)
        else:
            node['right'] = to_terminal(right)




# Build a decision tree
def build_tree(train, max_depth, min_size,index_categorical, index_discrete, index_pairs, greedy):
    root = get_split(train, min_size,index_categorical, index_discrete, index_pairs, greedy)
    split(root, max_depth, min_size, 1,index_categorical, index_discrete, index_pairs, greedy)
    return root

# Print a decision tree
def print_tree(node, index_categorical, depth=0):
    if isinstance(node, dict):
        if node['index'] in index_categorical:
            print('%s[X%d = %.3f]' % ((depth*' ', (node['index']), node['value'])))
            print_tree(node['left'],index_categorical, depth+1)
            print_tree(node['right'],index_categorical, depth+1)
        else:
            print('%s[X%d < %.3f]' % ((depth*' ', (node['index']), node['value'])))
            print_tree(node['left'], index_categorical, depth+1)
            print_tree(node['right'], index_categorical, depth+1)
    else:
        print('%s[%s]' % ((depth*' ', node)))
        

def graph_node(f, node):
    if isinstance(node, dict):
        f.write(' %d [label=\"[X%d %d;\n' % ((id(node), id(node['groups'][0]))))
        f.write(' %d -> %d;\n' % ((id(node), id(node['groups'][1]))))
        graph_node(f, node['groups'][0])
        graph_node(f, node['groups'][1])
    else:
        f.write(' %d [label=\"[%s]\"];\n' % ((id(node), node)))

def graph_tree(node):
    if not hasattr(graph_tree, 'n'): 
        graph_tree.n = 0
    graph_tree.n += 1
    fn = 'graph' + str(graph_tree.n) + '.dot'
    f = open(fn, 'w')
    f.write('digraph {\n')
    f.write(' node[shape=box];\n')
    graph_node(f, node)
    f.write('}\n')        
    

        
#############################
#                           #
#    Predict                #
#                           #
#############################

# Make a prediction with a decision tree

#recursive function: we want to predict the class of a row; we go down the tree and node is the current node

def predict(node, row,index_categorical, index_discrete, greedy):
    index=node['index']
    if index in index_categorical:
        if greedy==0:
            if row[node['index']] == node['value']:
                if isinstance(node['left'], dict):
                    return predict(node['left'], row,index_categorical, index_discrete,greedy)
                else:
                    return node['left']
            else:
                if isinstance(node['right'], dict):
                    return predict(node['right'], row,index_categorical, index_discrete,greedy)
                else:
                    return node['right']
        else:
            if row[node['index']] in node['value']:
                if isinstance(node['left'], dict):
                    return predict(node['left'], row,index_categorical, index_discrete,greedy)
                else:
                    return node['left']
            else:
                if isinstance(node['right'], dict):
                    return predict(node['right'], row,index_categorical, index_discrete,greedy)
                else:
                    return node['right']
    else: 
        if row[node['index']] < node['value']:
            if isinstance(node['left'], dict):
                return predict(node['left'], row,index_categorical, index_discrete,greedy)
            else:
                return node['left']
        else:
            if isinstance(node['right'], dict):
                return predict(node['right'], row,index_categorical, index_discrete,greedy)
            else:
                return node['right']
    

#############################
#                           #
#         Algorithm         #
#                           #
#############################


# Split a dataset into k folds
def cross_validation_split(dataset, n_folds):
	dataset_split = list()
	dataset_copy = list(dataset)
	fold_size = int(len(dataset) / n_folds)
	for i in range(n_folds):
		fold = list()
		while len(fold) < fold_size:
			index = randrange(len(dataset_copy))
			fold.append(dataset_copy.pop(index))
		dataset_split.append(fold)
	return dataset_split

# Calculate accuracy percentage
def accuracy_metric(actual, predicted):
	correct = 0
	for i in range(len(actual)):
		if actual[i] == predicted[i]:
			correct += 1
	return ( correct / float(len(actual)) )* 100.0

def kendall_tau(w,t):
    d = 0
    r_w = dict.fromkeys(w)
    r_t = dict.fromkeys(t)
    for i in range(len(w)):
        r_w[w[i]] = i+1
        r_t[t[i]] = i+1
    for c in combinations(w,2):
        if (r_w[c[1]]-r_w[c[0]])*(r_t[c[1]]-r_t[c[0]]) < 0:
            d += 1
    return d

def kendall_tau2(w,t):
    d = 0
    for c in combinations(set(w),2):
        if (w.index(c[0])-w.index(c[1]))*(t.index(c[0])-t.index(c[1])) < 0:
            d += 1
    return d

def accuracy_kendall_tau(actual, predicted):
	avg = 0
	for i in range(len(actual)):
		avg+=kendall_tau(actual[i],predicted[i])
	return avg / float(len(actual)) 

def accuracy_first(actual, predicted):
    correct=0
    for i in range(len(actual)):
        if actual[i][0] ==predicted[i][0]:
            correct+=1
    return ( correct / float(len(actual)) )* 100.0

def accuracy_second(actual, predicted):
    correct=0
    for i in range(len(actual)):
        if actual[i][1] ==predicted[i][1]:
            correct+=1
    return ( correct / float(len(actual)) )* 100.0


def accuracy_gamma(actual, predicted):
	correct = 0
	for i in range(len(actual)):
		if actual[i] == predicted[i]:
			correct += 1
	return ( correct / float(len(actual)) )* 100.0

# Evaluate an algorithm using a cross validation split
def evaluate_algorithm(dataset, algorithm, n_folds, *args):
	folds = cross_validation_split(dataset, n_folds)
	scores = list()
	scores2 = list()
	scores3 = list()
	scores4 = list()

	index_fold=0
	for fold in folds:
		index_fold+=1
		print 'fold index'+str(index_fold)        
		train_set = list(folds)
		train_set.remove(fold)
		train_set = sum(train_set, [])
		test_set = list()
		for row in fold:
			row_copy = list(row)
			test_set.append(row_copy)
			row_copy[-1] = None
		predicted = algorithm(train_set, test_set, *args)
		actual = [row[-1] for row in fold]
		accuracy = accuracy_metric(actual, predicted)
		scores.append(accuracy)
		accuracy2=accuracy_kendall_tau(actual, predicted)
		scores2.append(accuracy2)
		accuracy3=accuracy_first(actual, predicted)
		scores3.append(accuracy3)
		accuracy4=accuracy_second(actual, predicted)
		scores4.append(accuracy4)
	return scores,scores2, scores3, scores4


# Classification and Regression Tree Algorithm
def decision_tree(train, test, max_depth, min_size,index_categorical, index_discrete, index_pairs, n_features):
	tree = build_tree(train, max_depth, min_size,index_categorical, index_discrete, index_pairs,1)
	predictions = list()
	for row in test:
		prediction = predict(tree, row[:n_features],index_categorical, index_discrete,1)
		predictions.append(prediction)
	return(predictions)


def decision_tree_agg(train, test, max_depth, min_size,index_categorical, index_discrete, index_pairs, nb_models, n_features):
    trees=[]
    for i in range(nb_models):
        train_bis=[random.choice(train) for i in range(len(train))]
        tree = build_tree(train_bis, max_depth, min_size,index_categorical, index_discrete, index_pairs, 1)
        trees.append(tree)
    predictions = list()
    for row in test:
        predictions_row=[]
        for i in range(nb_models):
            tree=trees[i]
            prediction_model = predict(tree, row[:n_features],index_categorical, index_discrete, 1)
            predictions_row.append(prediction_model)
        
        P_row=dict(Counter(predictions_row)) 
        for s in P_row.keys():
            P_row[s]=(P_row[s])/(nb_models*1.0)
        #prediction=Copeland(P_row)
        prediction=Borda_count(P_row)
        #if len([list(x) for x in set(tuple(x) for x in predictions_row)])>1:
        #    print predictions_row
        #    print prediction
        predictions.append(prediction)
    return(predictions)
