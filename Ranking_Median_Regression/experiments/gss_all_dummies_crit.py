#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from random import *
import pandas as pd
import numpy as np
from crit_algorithm import *

#############################
#                           #
#         Data              #
#                           #
#############################

#np.random.seed(0)

#df_original=pd.read_stata('data/GSS/GSS7216_R1a.dta', convert_categoricals=False, convert_missing=False)

#df=df_original
"""
df=df_original[(df_original['year']==1973) |(df_original['year']==1985) | (df_original['year']==2006)]

print 'df original '+str(df.shape)

columns_job=['jobinc', 'jobsec', 'jobhour', 'jobpromo', 'jobmeans']

df = df[np.isfinite(df['jobinc'])]
df = df[np.isfinite(df['jobsec'])]
df = df[np.isfinite(df['jobhour'])]

df = df[np.isfinite(df['jobpromo'])]
df = df[np.isfinite(df['jobmeans'])]

n_items=len(columns_job)
print 'shape after job ' +str(df.shape)

columns_to_keep_categorical=['wrkstat','wrkslf','occ10', 'sex','race' ,'marital']

columns_to_keep_discrete=['cohort', 'degree', 'income' ,'childs', 'hompop']


columns_to_keep_numerical=[]

columns_to_keep1= columns_to_keep_categorical+columns_to_keep_discrete+columns_to_keep_numerical+ columns_job

df=df[columns_to_keep1]

"""
# drop na

df=df[np.isfinite(df)]
df=df.dropna( axis=0, how='any', thresh=None, subset=None)
print 'final shape '+str(df.shape)



# OCCUPATION

df.occ10[(df.occ10 >=10) & (df.occ10 <= 430)] =1
df.occ10[(df.occ10 >=500) & (df.occ10 <= 950)] =2
df.occ10[(df.occ10 >=1000) & (df.occ10 <= 3540)] =3
df.occ10[(df.occ10 >=3600) & (df.occ10 <= 4650)] =4
df.occ10[(df.occ10 >=4700) & (df.occ10 <= 5940)] =5
df.occ10[(df.occ10 >=6000) & (df.occ10 <= 7630)] =6
df.occ10[(df.occ10 >=7700) & (df.occ10 <= 9830)] =7
df.occ10[df.occ10 ==9997] =8


# COHORT

df.cohort[(df.cohort >=1883) & (df.cohort <= 1889)] =1
df.cohort[(df.cohort >1889) & (df.cohort <= 1899)] =2
df.cohort[(df.cohort >1899) & (df.cohort <= 1909)] =3
df.cohort[(df.cohort >1909) & (df.cohort <= 1919)] =4
df.cohort[(df.cohort >1919) & (df.cohort <= 1929)] =5
df.cohort[(df.cohort >1929) & (df.cohort <= 1939)] =6
df.cohort[(df.cohort >1939) & (df.cohort <= 1949)] =7
df.cohort[(df.cohort >1949) & (df.cohort <= 1959)] =8
df.cohort[(df.cohort >1959) & (df.cohort <= 1969)] =9
df.cohort[(df.cohort >1969) & (df.cohort <= 1979)] =10
df.cohort[(df.cohort >1979) & (df.cohort <= 1989)] =11
df.cohort[(df.cohort >1989) & (df.cohort <= 1999)] =12



df=pd.get_dummies(df, prefix=columns_to_keep_categorical, columns=columns_to_keep_categorical)


items=range(0,n_items)

job_orders=[]
for m in range(df.shape[0]):
    row=list(df[columns_job].iloc[m,:])
    preferences=[]
    #l=tuple([int(j-1) for j in df[columns_job].iloc[i,:]])
    l=sorted(range(len(row)), key=lambda k: row[k])
    for (i,j) in combinations(items,2):
        if l.index(i)<l.index(j):
            preferences.append(1)
        else:
            preferences.append(0)
    preferences.append(tuple(l))
    job_orders.append(preferences)

df.drop(columns_job, axis=1, inplace=True)
print 'df shape, number of features'+str(df.shape)


job_users=df.values.tolist()
n_features=len(job_users[0])


#categorical_features=list(set(columns_to_keep_categorical).intersection(set(columns_to_keep1)))

#categorical_features=columns_to_keep_categorical
categorical_features=[name for name in df.columns if any([name.startswith(name_original) for name_original in columns_to_keep_categorical])]
values_categorical_features=[np.unique(df[[column]]) for column in categorical_features ]
index_categorical=[list(df.columns).index(x) for x in categorical_features]

#discrete_features=list(set(columns_to_keep_discrete).intersection(set(columns_to_keep1)))
discrete_features=columns_to_keep_discrete
values_discrete_features=[np.unique(df[[column]]) for column in discrete_features ]
index_discrete=[list(df.columns).index(x) for x in discrete_features]


dataset=[job_users[i]+job_orders[i] for i in range(len(job_orders))]

n_pairs=int(((n_items)*(n_items-1))/float(2))
index_pairs=range(n_features, n_features+n_pairs)




max_depth =5
min_size = 100

#in size= 50 et max_depth=10 pas mal
# idem avec min_size=100

# works well but not true depth
#max_depth = int(n_samples/float(1000)+2)
#max_depth=int(np.log(n_samples)-3)

#max_depth=3
#min_size = int(n_samples/float(max_depth*20))
#min_size=10

# study
shuffle(dataset)
#data=list(random.sample(dataset,10000))
data=dataset
index=int(0.75*len(data))
train_set=data[:index]
n_samples=len(train_set)
test_set=data[index:]

print '\n final tree'
tree = build_tree(train_set, max_depth, min_size,index_categorical, index_discrete, index_pairs, 0)

predicted = list()
for row in test_set:
	prediction = predict(tree, row[:n_features],index_categorical, index_discrete, 0)
	predicted.append(prediction)


print_tree(tree, index_categorical)

actual = [row[-1] for row in test_set]

print 'mean kendall distance'
accuracy= accuracy_kendall_tau(actual, predicted)
print accuracy


unique_data_actual = [list(x) for x in set(tuple(x) for x in actual)]
unique_data_predicted = [list(x) for x in set(tuple(x) for x in predicted)]
print len(unique_data_actual)
print len(unique_data_predicted)

test_actual=[len([row for row in actual if row[0]==i]) for i in range(n_items)]
test_predicted=[len([row for row in predicted if row[0]==i]) for i in range(n_items)]

print test_actual
print test_predicted

# row[-100][25]=1 donc ca devrait etre 0,2,3,1


#print 'accuracy on total rankings'
#accuracy = accuracy_metric(actual, predicted)
#print accuracy
#print 'accuracy first'
#accuracy3 = accuracy_first(actual, predicted)
#print accuracy3
#print 'accuracy second'
#accuracy4 = accuracy_second(actual, predicted)
#print accuracy4


from Plackett import *

print '\n PLACKETT LUCE'
# compare with Plackett Luce

L_train=[row[-1] for row in train_set]
Gamma=PL_fitting_list(L_train,n_items,2000)
PL_model=PL_distribution(Gamma,n_items)

def sample_from(d):
    r = uniform(0, sum(d.itervalues()))
    s = 0.0
    for k, w in d.iteritems():
        s += w
        if r < s: return k
    return k



predicted = list()
for row in test_set:
	#prediction = sample_from(PL_model)
    prediction=tuple(sorted(range(len(Gamma)), key=lambda k: Gamma[k], reverse=True))
    predicted.append(prediction)



actual = [row[-1] for row in test_set]

print 'mean kendall distance'
accuracy= accuracy_kendall_tau(actual, predicted)
print accuracy


unique_data_actual = [list(x) for x in set(tuple(x) for x in actual)]
unique_data_predicted = [list(x) for x in set(tuple(x) for x in predicted)]
print len(unique_data_actual)
print len(unique_data_predicted)

test_actual=[len([row for row in actual if row[0]==i]) for i in range(n_items)]
test_predicted=[len([row for row in predicted if row[0]==i]) for i in range(n_items)]

print test_actual
print test_predicted

