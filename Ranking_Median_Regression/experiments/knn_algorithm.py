#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from random import *
import pandas as pd
import numpy as np
from itertools import *
from collections import Counter
from copeland import Copeland,Copelandbis, Borda_countbis, Borda_countbis, Plackett_count
import operator 
import math
from Mallows import ML_distribution
from crit_algorithm import accuracy_kendall_tau


def euclideanDistance(instance1, instance2, length):
	distance = 0
	for x in range(length):
		distance += pow((instance1[x] - instance2[x]), 2)
	return math.sqrt(distance)

data1 = [2, 2, 2, (1,2,3)]
data2 = [4, 4, 4, (2,1,3)]
#distance = euclideanDistance(data1, data2, 3)
#print 'Distance: ' + repr(distance)


def getNeighbors(trainingSet, testInstance, k):
	distances = []
	length = len(testInstance)
	#print length
	for x in range(len(trainingSet)):
		dist = euclideanDistance(testInstance, trainingSet[x], length)
		distances.append((trainingSet[x], dist))
	distances.sort(key=operator.itemgetter(1))
	neighbors = []
	for x in range(k):
		neighbors.append(distances[x][0])
	return neighbors

#trainSet = [[2, 2, 2, (1,2,3)], [4, 4, 4, (2,3,1)]]
#testInstance = [5, 5, 5]
#k = 1
#neighbors = getNeighbors(trainSet, testInstance, 1)
#print(neighbors)

def getResponse(neighbors):
     outcomes = [row[-1] for row in neighbors]
     P=dict(Counter(outcomes)) 
     Nsample=len(outcomes)
     for s in P.keys():
         P[s]=(P[s])/(Nsample*1.0)
     Copeland_dict=Copelandbis(P)[1]
     if len(Copeland_dict.values()) == len(set(Copeland_dict.values())):
         consensus=Copelandbis(P)[0]
     else:
         consensus=Borda_countbis(P)[0]
         Borda_dict=Borda_countbis(P)[1]
     return consensus

#neighbors = [[1,1,1,(2,1,3)], [2,2,2,(2,1,3)], [3,3,3,(2,3,1)]]
#response = getResponse(neighbors)
#print(response)

def knn_prediction(trainingSet,testInstance, k):
    neighbors=getNeighbors(trainingSet, testInstance,k)
    prediction=getResponse(neighbors)
    return prediction
