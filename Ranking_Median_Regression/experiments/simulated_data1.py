#!/usr/bin/env python2
# -*- coding: utf-8 -*-


from random import *
import numpy as np
from Plackett import PL_distribution
from Mallows import ML_distribution
from crit_algorithm import *


#########################
#                       #
#        Data           #
#                       #
#########################


def sample_from(d):
    r = uniform(0, sum(d.itervalues()))
    s = 0.0
    for k, w in d.iteritems():
        s += w
        if r < s: return k
    return k

#np.random.seed(0) 

n_items=3
items=range(n_items)

n_samples=1000

n_features=2
X1=np.random.random(n_samples)
X2=np.random.random(n_samples)

gamma1=np.random.permutation(range(1,n_items+1))
gamma2=np.random.permutation(range(1,n_items+1))
gamma3=np.random.permutation(range(1,n_items+1))
gamma4=np.random.permutation(range(1,n_items+1))
gamma5=np.random.permutation(range(1,n_items+1))
gamma6=np.random.permutation(range(1,n_items+1))

gamma1*=100
gamma2*=100
gamma3*=100
gamma4*=100
gamma5*=100
gamma6*=100


#gamma1=np.random.random((n_items,1))
#gamma2=np.random.random((n_items,1))
#gamma3=np.random.random((n_items,1))
#gamma4=np.random.random((n_items,1))
#gamma5=np.random.random((n_items,1))
#gamma6=np.random.random((n_items,1))

sigma1=tuple(sorted(range(len(gamma1)), key=lambda k: gamma1[k], reverse=True))
sigma2=tuple(sorted(range(len(gamma2)), key=lambda k: gamma2[k], reverse=True))
sigma3=tuple(sorted(range(len(gamma3)), key=lambda k: gamma3[k], reverse=True))
sigma4=tuple(sorted(range(len(gamma4)), key=lambda k: gamma4[k], reverse=True))
sigma5=tuple(sorted(range(len(gamma5)), key=lambda k: gamma5[k], reverse=True))
sigma6=tuple(sorted(range(len(gamma6)), key=lambda k: gamma6[k], reverse=True))
"""
PL1=PL_distribution(gamma1,n_items)
PL2=PL_distribution(gamma2,n_items)
PL3=PL_distribution(gamma3,n_items)
PL4=PL_distribution(gamma4,n_items)
PL5=PL_distribution(gamma5,n_items)
PL6=PL_distribution(gamma6,n_items)
"""
print 'mallows'

spread=2
ML1=ML_distribution(sigma1,spread)
ML2=ML_distribution(sigma2,spread)
ML3=ML_distribution(sigma3,spread)
ML4=ML_distribution(sigma4,spread)
ML5=ML_distribution(sigma5,spread)
ML6=ML_distribution(sigma6,spread)

# from https://dzone.com/articles/regression-tree-using-gini%E2%80%99s
# http://freakonometrics.hypotheses.org/tag/cart

P=.8*(X1<.3)*(X2<.5)\
+   .2*(X1<.3)*(X2>.5)\
+   .8*(X1>.3)*(X1<.85)*(X2<.3)\
+   .2*(X1>.3)*(X1<.85)*(X2>.3)\
+   .8*(X1>.85)*(X2<.7)\
+   .2*(X1>.85)*(X2>.7) 


Y1=[]
for i in range(n_samples):
    if (X1[i]<.3) and (X2[i]<.5): # C1
        Y1.append(sigma1)
    elif (X1[i]<.3) and (X2[i]>.5): # C2
        Y1.append(sigma2)
    elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<.3): # C3
        Y1.append(sigma3)
    elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]>.3): # C4
        Y1.append(sigma4)
    elif (X1[i]>.85) and (X2[i]<.7): # C5
        Y1.append(sigma5)
    else: # C6
        Y1.append(sigma6)
  
# count

C1,C2,C3,C4,C5,C6=[],[],[],[],[],[]

for i in range(n_samples):
    if (X1[i]<.3) and (X2[i]<.5):
        C1.append(i)        
    elif (X1[i]<.3) and (X2[i]>.5):
        C2.append(i)        
    elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<.3):
        C3.append(i)        
    elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]>.3):
        C4.append(i)        
    elif (X1[i]>.85) and (X2[i]<.7):
        C5.append(i)        
    else:
        C6.append(i)        
print len(C1), len(C2), len(C3), len(C4), len(C5), len(C6)
print 'first split should divide in ' +str(len(C1)+len(C2))+ ', '+str(len(C3)+len(C4)+len(C5))
      
# plackett luce
"""
Y2=[]
for i in range(n_samples):
    if (X1[i]<.3) and (X2[i]<.5):
        sigma=sample_from(PL1)
        print sigma1,sigma
        #sigma=sample_from(ML1)
    elif (X1[i]<.3) and (X2[i]>.5):
        sigma=sample_from(PL2)
    elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<.3):
        sigma=sample_from(PL3)
    elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]>.3):
        sigma=sample_from(PL4)
    elif (X1[i]>.85) and (X2[i]<.7):
        sigma=sample_from(PL5)
    else:
        sigma=sample_from(PL6)
    Y2.append(sigma)
"""
# mallows   

Y2=[]
for i in range(n_samples):
    if (X1[i]<.3) and (X2[i]<.5):
        sigma=sample_from(ML1)
    elif (X1[i]<.3) and (X2[i]>.5):
        sigma=sample_from(ML2)
    elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<.3):
        sigma=sample_from(ML3)
    elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]>.3):
        sigma=sample_from(ML4)
    elif (X1[i]>.85) and (X2[i]<.7):
        sigma=sample_from(ML5)
    else:
        sigma=sample_from(ML6)
    Y2.append(sigma)

# if Y=Y1, all labels are identical in a cell, elsewhere they follow a centered distribution.
Y=Y1

orders=[] # with pairs
for i in range(n_samples):
    preferences=[]
    l=Y[i]
    for (i,j) in combinations(items,2):
        if l.index(i)<l.index(j):
            preferences.append(1)
        else:
            preferences.append(0)
    preferences.append(tuple(l))
    orders.append(preferences)


dataset=[[X1[i],X2[i]]+orders[i] for i in range(n_samples)]
print 'dataset finished'

n_pairs=int((n_items*(n_items-1))/float(2))
index_pairs=range(n_features, n_features+n_pairs)

index_categorical=[]
index_discrete=[]
index_numerical=[0,1]

#############################
#                           #
#      Evaluation           #
#                           #
#############################

# works well but not true depth
max_depth = int(n_samples/float(1000)+2)
max_depth=int(np.log(n_samples)-3)

max_depth=3
min_size = int(n_samples/float(max_depth*20))
#min_size=10

# one launch


data=dataset
#random.shuffle(dataset)
index=int(0.7*len(data))
train_set=data[:index]
n_samples=len(train_set)
test_set=data[index:]

print '\n build tree'
tree = build_tree(train_set, max_depth, min_size, index_categorical, index_discrete, index_pairs)
#print 'split 2'
#test=get_split2(train_set, min_size)

predicted = list()
for row in test_set:
	prediction = predict(tree, row[:n_features],index_categorical, index_discrete)
	predicted.append(prediction)

actual = [row[-1] for row in test_set]
print 'accuracy'
accuracy = accuracy_metric(actual, predicted)
print accuracy
print 'mean kendall distance'
accuracy2 = accuracy_kendall_tau(actual, predicted)
print accuracy2

print_tree(tree, index_categorical)


unique_data_actual = [list(x) for x in set(tuple(x) for x in actual)]
unique_data_predicted = [list(x) for x in set(tuple(x) for x in predicted)]
print len(unique_data_actual)
print len(unique_data_predicted)




"""
# evaluate algorithm
n_folds = 5
scores,scores2 = evaluate_algorithm(dataset, decision_tree, n_folds, max_depth, min_size)
print('Scores: %s' % scores)
print('Mean Accuracy: %.3f%%' % (sum(scores)/float(len(scores))))

print('Scores Kendall: %s' % scores2)
print('Mean Accuracy Kendall: %.3f' % (sum(scores2)/float(len(scores2))))
"""