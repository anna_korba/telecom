#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from random import seed
from random import randrange
import csv
import pandas as pd
import numpy as np
from itertools import chain, combinations
import random
from collections import Counter
from itertools import *
import copy
from copeland import Copeland, Borda_count, Borda_countbis, Plackett_count

#############################
#                           #
#         Data              #
#                           #
#############################

f = open('data/sushi3a.5000.10.order','r') 
unique_users=[]
n_items=10

sushi_orders=[]
for line in f:
    preferences=[]
    l = map(int,line.split(' '))
    for (i,j) in combinations(range(0,10),2):
        if l[2:].index(i)<l[2:].index(j):
            preferences.append(1)
        else:
            preferences.append(0)
    preferences.append(tuple(l[2:]))
    sushi_orders.append(preferences)

items=range(0,10)
pairs=[(i,j) for (i,j) in combinations(range(0,10),2)]
pairs_index=dict.fromkeys(items,[])
for i in items:
    pairs_index[i]=[(pairs.index(pair),pair.index(i)) for pair in pairs if i in pair]

# 0.  the user ID
#1.  gender   0:male 1:female
#2.  age      0:15-19  1:20-29   2:30-39   3:40-49   4:50-59    5:60-
#3.  the total time need to fill questionnaire form
#4.  prefecture ID at which you have been the most longly lived until 15 years old
#5.  region ID at which you have been the most longly lived until 15 years old
#6.  east/west ID at which you have been the most longly lived until 15 years old
#7.  prefecture ID at which you currently live
#8.  regional ID at which you currently live
#9. east/west ID at which you currently live
#10. 0 if features 5 and 8 are equal; 1 otherwise

sushi_users=pd.read_csv('data/sushi3.udata', sep='\t', header=None)
sushi_users.columns=['id','gender', 'age', 'time_fill','prefecture1ID', 'regionID', 'eastwest1ID', 'prefecture2ID', 'regionalID', 'eastwest2ID','equality' ]
sushi_users.drop(sushi_users.columns[[0,3, 4,7]], axis=1, inplace=True) 

columns_to_keep_categorical=['gender', 'regionID', 'eastwest1ID',  'regionalID', 'eastwest2ID','equality' ]

sushi_users=pd.get_dummies(sushi_users, prefix=columns_to_keep_categorical, columns=columns_to_keep_categorical)


#categorical_features=['gender', 'prefecture1ID', 'regionID', 'eastwest1ID', 'prefecture2ID', 'regionalID', 'eastwest2ID','equality' ]
categorical_features=[name for name in sushi_users.columns if any([name.startswith(name_original) for name_original in columns_to_keep_categorical])]
values_categorical_features=[np.unique(sushi_users[[column]]) for column in categorical_features ]
index_categorical=[list(sushi_users.columns).index(x) for x in categorical_features]

discrete_features=['age']
values_discrete_features=[np.unique(sushi_users[[column]])  for column in discrete_features ]
index_discrete=[list(sushi_users.columns).index(x) for x in discrete_features]

df_sushi_users=sushi_users

sushi_users=sushi_users.values.tolist()
n_features=len(sushi_users[0])

#seed(1)
dataset=[sushi_users[i]+sushi_orders[i] for i in range(len(sushi_orders))]
dataset2=random.sample(dataset,1000)

# where are the pairs in the vector
n_pairs=45
index_pairs=range(n_features, n_features+n_pairs)


#############################
#                           #
#    Create split           #
#                           #
#############################

def compute_gamma(group):
    gamma=0
    size=len(group) 
    if size==0:
        gamma=np.inf
    else:
        for m in index_pairs:
            proportion= [row[m] for row in group].count(1) / float(size)
            gamma += (proportion * (1.0 - proportion))/float(2)
    return gamma

def entropy_index(groups): 
    entropy=0 
    i=0
    for group in groups:
        size=len(group) 
        gamma=compute_gamma(group)
        entropy+=size*gamma
        i+=1
    entropy=entropy/float(n_samples)
    return entropy

# Split a dataset based on an attribute and an attribute value
def test_split_categorical(index, value, dataset):
	left, right = list(), list()
	for row in dataset:
		if row[index] ==value:
			left.append(row)
		else:
			right.append(row)
	return left, right

def test_split_categorical_v2(index, subset, dataset):
	left, right = list(), list()
	for row in dataset:
		if row[index] in subset:
			left.append(row)
		else:
			right.append(row)
	return left, right


def test_split(index, value, dataset):
	left, right = list(), list()
	for row in dataset:
		if row[index] < value:
			left.append(row)
		else:
			right.append(row)
	return left, right


def get_split(dataset, min_size):
    b_index, b_value, b_score, b_groups = 999, 999, 999, None
    for index in range(n_features): #index i of the variable for the split
        if index in index_categorical:
            values=np.unique([row[index] for row in dataset])
            for value in values:
                groups = test_split_categorical(index, value, dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, value, entropy, groups
        elif index in index_discrete:
            values=np.unique([row[index] for row in dataset])
            for value in values:
                groups = test_split(index, value, dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, value, entropy, groups        
        else:
            print 'other'
            for row in dataset: # value of X_i for the split
                groups = test_split(index, row[index], dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, row[index], entropy, groups
    if b_groups is not None:
        print 'finally split with ' +str(df.columns[b_index])+' and value '+str(b_value) + ' and entropy '+str(b_score)
        print len(b_groups[0]), len(b_groups[1])
    return {'index':b_index, 'value':b_value, 'groups':b_groups}

def get_split_v2(dataset, min_size):
    b_index, b_value, b_score, b_groups = 999, 999, 999, None
    for index in range(n_features): #index i of the variable for the split
        if index in index_categorical:
            print 'discrete, '+str(df_sushi_users.columns[index])
            values=set(np.unique([row[index] for row in dataset]))
            possible_subsets= [z for z  in chain.from_iterable(combinations(values,r) for r in range(len(values)+1))]
            for subset in possible_subsets:
                print subset
                groups = test_split_categorical(index, subset, dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, subset, entropy, groups
        elif index in index_discrete:
            values=np.unique([row[index] for row in dataset])
            for value in values:
                groups = test_split(index, value, dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, value, entropy, groups        
        else:
            print 'other'
            for row in dataset: # value of X_i for the split
                groups = test_split(index, row[index], dataset) 
                if len(groups[0])>=min_size and len(groups[1])>=min_size:
                    entropy = entropy_index(groups)
                    if entropy < b_score:
                        b_index, b_value, b_score, b_groups = index, row[index], entropy, groups
    #if b_groups is not None:
        #print 'finally split with ' +str(df.columns[b_index])+' and value '+str(b_value) + ' and entropy '+str(b_score)
        #print len(b_groups[0]), len(b_groups[1])
    return {'index':b_index, 'value':b_value, 'groups':b_groups}


#splitt = get_split(dataset, 10)
#print('Split: [X%d < %.3f]' % ((splitt['index']), splitt['value']))



#############################
#                           #
#    Build Tree             #
#                           #
#############################

# Create a terminal node value
def to_terminal(group):
    outcomes = [row[-1] for row in group]
    P=dict(Counter(outcomes)) 
    Nsample=len(outcomes)
    for s in P.keys():
            P[s]=(P[s])/(Nsample*1.0)
    consensus=Copeland(P)
    return consensus

        
# Create child splits for a node or make terminal
def split(node, max_depth, min_size, depth):
    left, right = node['groups']
    print 'len left, len right '+str(len(left))+' , '+str(len(right))
    print 'depth '+str(depth)
    del(node['groups'])
    # check for a no split
    if not left or not right:
        node['left'] = node['right'] = to_terminal(left + right)
        return
	# check for max depth
    if depth >= max_depth:
        print 'groups'
        groups=left, right
        entropy=entropy_index(groups)
        print 'max depth here, entropy : '+str(entropy) 
        node['left'], node['right'] = to_terminal(left), to_terminal(right)
        return
	# process left child
    if len(left) <= min_size:
        node['left'] = to_terminal(left)
    else:
        if get_split(left, min_size)['groups'] is not None:
            node['left'] = get_split(left, min_size)
            split(node['left'], max_depth, min_size, depth+1)
        else:
            node['left'] = to_terminal(left)
	# process right child
    if len(right) <= min_size:
        node['right'] = to_terminal(right)
    else:
        if get_split(right, min_size)['groups'] is not None:
            node['right'] = get_split(right, min_size)
            split(node['right'], max_depth, min_size, depth+1)
        else:
            node['right'] = to_terminal(right)
	

# Build a decision tree
def build_tree(train, max_depth, min_size):
	root = get_split(train, min_size)
	split(root, max_depth, min_size, 1)
	return root

# Print a decision tree
def print_tree(node, depth=0):
    if isinstance(node, dict):
        if node['index'] in index_categorical:
            print('%s[X%d = %.3f]' % ((depth*' ', (node['index']), node['value'])))
            print_tree(node['left'], depth+1)
            print_tree(node['right'], depth+1)
        else:
            print('%s[X%d < %.3f]' % ((depth*' ', (node['index']), node['value'])))
            print_tree(node['left'], depth+1)
            print_tree(node['right'], depth+1)
    else:
        print('%s[%s]' % ((depth*' ', node)))
        

def graph_node(f, node):
    if isinstance(node, dict):
        f.write(' %d [label=\"[X%d %d;\n' % ((id(node), id(node['groups'][0]))))
        f.write(' %d -> %d;\n' % ((id(node), id(node['groups'][1]))))
        graph_node(f, node['groups'][0])
        graph_node(f, node['groups'][1])
    else:
        f.write(' %d [label=\"[%s]\"];\n' % ((id(node), node)))

def graph_tree(node):
    if not hasattr(graph_tree, 'n'): 
        graph_tree.n = 0
    graph_tree.n += 1
    fn = 'graph' + str(graph_tree.n) + '.dot'
    f = open(fn, 'w')
    f.write('digraph {\n')
    f.write(' node[shape=box];\n')
    graph_node(f, node)
    f.write('}\n')        
    

        
#############################
#                           #
#    Predict                #
#                           #
#############################

# Make a prediction with a decision tree

#recursive function: we want to predict the class of a row; we go down the tree and node is the current node


def predict(node, row):
    index=node['index']
    if index in index_categorical:
        if row[node['index']] == node['value']:
            if isinstance(node['left'], dict):
                return predict(node['left'], row)
            else:
                return node['left']
        else:
            if isinstance(node['right'], dict):
                return predict(node['right'], row)
            else:
                return node['right']
    elif index in index_discrete:
        if row[node['index']] < node['value']:
            if isinstance(node['left'], dict):
                return predict(node['left'], row)
            else:
                return node['left']
        else:
            if isinstance(node['right'], dict):
                return predict(node['right'], row)
            else:
                return node['right']
    else:
        raise Exception('index neither categorical nor discrete')
    


#  predict with a stump
# index is 0 because thats what we found in the  previous part
#print 'test prediction'
#for row in dataset[:1]:     
#	prediction = predict(tree, row[:n_features+1])
#	print('Expected :'+str(row[-1])+', Got : ' +str(prediction))
    



#############################
#                           #
#         Algorithm         #
#                           #
#############################


# Split a dataset into k folds
def cross_validation_split(dataset, n_folds):
	dataset_split = list()
	dataset_copy = list(dataset)
	fold_size = int(len(dataset) / n_folds)
	for i in range(n_folds):
		fold = list()
		while len(fold) < fold_size:
			index = randrange(len(dataset_copy))
			fold.append(dataset_copy.pop(index))
		dataset_split.append(fold)
	return dataset_split

# Calculate accuracy percentage
def accuracy_metric(actual, predicted):
	correct = 0
	for i in range(len(actual)):
		if actual[i] == predicted[i]:
			correct += 1
	return correct / float(len(actual)) * 100.0

def kendall_tau(w,t):
    d = 0
    r_w = dict.fromkeys(w)
    r_t = dict.fromkeys(t)
    for i in range(len(w)):
        r_w[w[i]] = i+1
        r_t[t[i]] = i+1
    for c in combinations(w,2):
        if (r_w[c[1]]-r_w[c[0]])*(r_t[c[1]]-r_t[c[0]]) < 0:
            d += 1
    return d

def accuracy_kendall_tau(actual, predicted):
	avg = 0
	for i in range(len(actual)):
		avg+=kendall_tau(actual[i],predicted[i])
	return avg / float(len(actual)) 


# Evaluate an algorithm using a cross validation split
def evaluate_algorithm(dataset, algorithm, n_folds, *args):
	folds = cross_validation_split(dataset, n_folds)
	scores = list()
	scores2 = list()
	index_fold=0
	for fold in folds:
		index_fold+=1
		print 'fold index'+str(index_fold)        
		train_set = list(folds)
		train_set.remove(fold)
		train_set = sum(train_set, [])
		test_set = list()
		for row in fold:
			row_copy = list(row)
			test_set.append(row_copy)
			row_copy[-1] = None
		predicted = algorithm(train_set, test_set, *args)
		actual = [row[-1] for row in fold]
		accuracy = accuracy_metric(actual, predicted)
		scores.append(accuracy)
		accuracy2=accuracy_kendall_tau(actual, predicted)
		scores2.append(accuracy2)
	return scores,scores2


# Classification and Regression Tree Algorithm
def decision_tree(train, test, max_depth, min_size):
	tree = build_tree(train, max_depth, min_size)
	predictions = list()
	for row in test:
		prediction = predict(tree, row[:n_features])
		predictions.append(prediction)
	return(predictions)


# study


max_depth = 20
min_size = 10

#data=list(random.sample(dataset,500))
data=dataset
index=int(0.8*len(data))
train_set=data[:index]
n_samples=len(train_set)
test_set=data[index:]

print '\n final tree'
tree = build_tree(train_set, max_depth, min_size)

print_tree(tree)

print 'predict'
predicted = list()
for row in test_set:
	prediction = predict(tree, row[:n_features])
	predicted.append(prediction)

actual = [row[-1] for row in test_set]
accuracy = accuracy_metric(actual, predicted)

print accuracy
accuracy2 = accuracy_kendall_tau(actual, predicted)
print accuracy2


unique_data_actual = [list(x) for x in set(tuple(x) for x in actual)]
unique_data_predicted = [list(x) for x in set(tuple(x) for x in predicted)]
print len(unique_data_actual)
print len(unique_data_predicted)




# Test CART on dataset
#seed(1)

# evaluate algorithm
"""
n_folds = 5

scores,scores2 = evaluate_algorithm(dataset, decision_tree, n_folds, max_depth, min_size)
print('Scores: %s' % scores)
print('Mean Accuracy: %.3f%%' % (sum(scores)/float(len(scores))))

print('Scores Kendall: %s' % scores2)
print('Mean Accuracy Kendall: %.3f' % (sum(scores2)/float(len(scores2))))
"""