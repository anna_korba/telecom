#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from random import *
import numpy as np
from Mallows import ML_distribution
from crit_algorithm import *
from Plackett import *

from sklearn.cluster import KMeans
import warnings
warnings.filterwarnings("ignore")



#############################
#                           #
#      Evaluation           #
#                           #
#############################
n_samples=1000

max_depth=3
min_size = int(n_samples/float(max_depth*20))


n_features=2



#########################
#                       #
#        Data           #
#                       #
#########################


def sample_from(d):
    r = uniform(0, sum(d.itervalues()))
    s = 0.0
    for k, w in d.iteritems():
        s += w
        if r < s: return k
    return k



list_results0=[]
list_results1=[]
list_results2=[]


list_results0PL=[]
list_results1PL=[]
list_results2PL=[]

n_experiments=20

c=0
for n_items in [3,5,8]:
        
    items=range(n_items)
    n_pairs=int((n_items*(n_items-1))/float(2))
    index_pairs=range(n_features, n_features+n_pairs)

    for i in range(n_experiments):
        c+=1
        #print 'n expe '+str(c)
                
        
        # Categorical features 
        index_categorical=[0,1]
        index_discrete=[]
        index_numerical=[]
        X1=np.random.randint(0,3,n_samples)
        X2=np.random.randint(0,5,n_samples)
        
        gamma1=np.random.permutation(range(1,n_items+1))
        gamma2=np.random.permutation(range(1,n_items+1))
        gamma3=np.random.permutation(range(1,n_items+1))
        gamma4=np.random.permutation(range(1,n_items+1))
        gamma5=np.random.permutation(range(1,n_items+1))
        gamma6=np.random.permutation(range(1,n_items+1))
        
        
        sigma1=tuple(sorted(range(len(gamma1)), key=lambda k: gamma1[k], reverse=True))
        sigma2=tuple(sorted(range(len(gamma2)), key=lambda k: gamma2[k], reverse=True))
        sigma3=tuple(sorted(range(len(gamma3)), key=lambda k: gamma3[k], reverse=True))
        sigma4=tuple(sorted(range(len(gamma4)), key=lambda k: gamma4[k], reverse=True))
        sigma5=tuple(sorted(range(len(gamma5)), key=lambda k: gamma5[k], reverse=True))
        sigma6=tuple(sorted(range(len(gamma6)), key=lambda k: gamma6[k], reverse=True))
        
            
        Y0=[]
        for i in range(n_samples):        
            #if (X1[i]<.3) and (X2[i]<.5):
            #if (X1[i]<.3) and (X2[i]<=3): 
            if (X1[i]==0) and (X2[i]<=3): 
                Y0.append(sigma1)
            #elif (X1[i]<.3) and (X2[i]>.5):
            #elif (X1[i]<.3) and (X2[i]>3): 
            elif (X1[i]==0) and (X2[i]>3): 
                Y0.append(sigma2)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]==1 or X2[i]==3): 
            elif (X1[i]==1) and (X2[i]==1 or X2[i]==3): 
                Y0.append(sigma3)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]>.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<>1 and X2[i]<>3): 
            elif (X1[i]==1) and (X2[i]<>1 and X2[i]<>3):        
                Y0.append(sigma4)
            #elif (X1[i]>.85) and (X2[i]<.7):
            #elif (X1[i]>.85) and (X2[i]==1 or X2[i]==5): 
            elif (X1[i]==2) and (X2[i]==1 or X2[i]==4): 
                Y0.append(sigma5)
            else:
                Y0.append(sigma6)
    
        
        #print 'mallows 1'
        
        spread=2
        ML1=ML_distribution(sigma1,spread)
        ML2=ML_distribution(sigma2,spread)
        ML3=ML_distribution(sigma3,spread)
        ML4=ML_distribution(sigma4,spread)
        ML5=ML_distribution(sigma5,spread)
        ML6=ML_distribution(sigma6,spread)
    
        Y1=[]
        for i in range(n_samples):
            #if (X1[i]<.3) and (X2[i]<.5):
            #if (X1[i]<.3) and (X2[i]<=3): 
            if (X1[i]==0) and (X2[i]<=3): 
                sigma=sample_from(ML1)
            #elif (X1[i]<.3) and (X2[i]>.5):
            #elif (X1[i]<.3) and (X2[i]>3): 
            elif (X1[i]==0) and (X2[i]>3): 
                sigma=sample_from(ML2)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]==1 or X2[i]==3): 
            elif (X1[i]==1) and (X2[i]==1 or X2[i]==3): 
                sigma=sample_from(ML3)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]>.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<>1 and X2[i]<>3): 
            elif (X1[i]==1) and (X2[i]<>1 and X2[i]<>3):        
                sigma=sample_from(ML4)
            #elif (X1[i]>.85) and (X2[i]<.7):
            #elif (X1[i]>.85) and (X2[i]==1 or X2[i]==5): 
            elif (X1[i]==2) and (X2[i]==1 or X2[i]==4): 
                sigma=sample_from(ML5)
            else:
                sigma=sample_from(ML6)
            Y1.append(sigma)
    
        #print 'mallows 2'
        
        spread=1
        ML1=ML_distribution(sigma1,spread)
        ML2=ML_distribution(sigma2,spread)
        ML3=ML_distribution(sigma3,spread)
        ML4=ML_distribution(sigma4,spread)
        ML5=ML_distribution(sigma5,spread)
        ML6=ML_distribution(sigma6,spread)
        
        Y2=[]
        for i in range(n_samples):
            #if (X1[i]<.3) and (X2[i]<.5):
            #if (X1[i]<.3) and (X2[i]<=3): 
            if (X1[i]==0) and (X2[i]<=3): 
                sigma=sample_from(ML1)
            #elif (X1[i]<.3) and (X2[i]>.5):
            #elif (X1[i]<.3) and (X2[i]>3): `
            elif (X1[i]==0) and (X2[i]>3): 
                sigma=sample_from(ML2)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]==1 or X2[i]==3): 
            elif (X1[i]==1) and (X2[i]==1 or X2[i]==3): 
                sigma=sample_from(ML3)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]>.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<>1 and X2[i]<>3):   
            elif (X1[i]==1) and (X2[i]<>1 and X2[i]<>3):        
                sigma=sample_from(ML4)
            #elif (X1[i]>.85) and (X2[i]<.7):
            #elif (X1[i]>.85) and (X2[i]==1 or X2[i]==5): 
            elif (X1[i]==2) and (X2[i]==1 or X2[i]==4): 
                sigma=sample_from(ML5)
            else:
                sigma=sample_from(ML6)
            Y2.append(sigma)
        
        orders0=[]
        orders1=[]
        orders2=[]
        for i in range(n_samples):
            preferences=[]
            l=Y0[i]
            for (i,j) in combinations(items,2):
                if l.index(i)<l.index(j):
                    preferences.append(1)
                else:
                    preferences.append(0)
            preferences.append(tuple(l))
            orders0.append(preferences)
            
        for i in range(n_samples):
            preferences=[]
            l=Y1[i]
            for (i,j) in combinations(items,2):
                if l.index(i)<l.index(j):
                    preferences.append(1)
                else:
                    preferences.append(0)
            preferences.append(tuple(l))
            orders1.append(preferences)
            
        for i in range(n_samples):
            preferences=[]
            l=Y2[i]
            for (i,j) in combinations(items,2):
                if l.index(i)<l.index(j):
                    preferences.append(1)
                else:
                    preferences.append(0)
            preferences.append(tuple(l))
            orders2.append(preferences)
            
        dataset0=[[X1[i],X2[i]]+orders0[i] for i in range(n_samples)]
        dataset1=[[X1[i],X2[i]]+orders1[i] for i in range(n_samples)]
        dataset2=[[X1[i],X2[i]]+orders2[i] for i in range(n_samples)]
        
              
        # train test
        data=dataset0
        
        index=int(0.7*len(data))
        train_set=data[:index]
        test_set=data[index:]
        
        #fit models
        tree = build_tree(train_set, max_depth, min_size, index_categorical, index_discrete, index_pairs, 1)
        
        #one hot encoding
        nb_classes = 3
        targets = np.array([row[index_categorical[0]] for row in train_set]).reshape(-1)
        one_hot_targets = np.eye(nb_classes)[targets]
        nb_classes = 5
        targets2 = np.array([row[index_categorical[1]] for row in train_set]).reshape(-1)
        one_hot_targets2 = np.eye(nb_classes)[targets2]
        X_train=np.asarray([list(one_hot_targets[i])+list(one_hot_targets2[i]) for i in range(len(train_set))])
        
        nb_classes = 3
        targets = np.array([row[index_categorical[0]] for row in test_set]).reshape(-1)
        one_hot_targets = np.eye(nb_classes)[targets]
        nb_classes = 5
        targets2 = np.array([row[index_categorical[1]] for row in test_set]).reshape(-1)
        one_hot_targets2 = np.eye(nb_classes)[targets2]
        X_test=np.asarray([list(one_hot_targets[i])+list(one_hot_targets2[i]) for i in range(len(test_set))])
        
        
        Y_train=np.asarray([row[-1] for row in train_set])
        kmeans = KMeans(n_clusters=6, random_state=0).fit(X_train)
        PL_centers=list()
        for k in range(6):
            rankings=Y_train[kmeans.labels_==k]
            Gamma=PL_fitting_list(rankings,n_items,100)
            center=tuple(sorted(range(len(Gamma)), key=lambda k: Gamma[k], reverse=True))
            PL_centers.append(center)
        
        predicted_tree = list()
        predicted_km = list()
        for i in range(len(test_set)):
            row1=test_set[i]
            prediction_tree = predict(tree, row1[:n_features],index_categorical, index_discrete, 1)
            predicted_tree.append(prediction_tree)
            row2=X_test[i]
            predict_cluster=kmeans.predict(row2[:8])[0] #HERE !!!!
            prediction_km=PL_centers[predict_cluster]
            predicted_km.append(prediction_km)
        actual = [row[-1] for row in test_set]
        accuracy_tree = accuracy_kendall_tau(actual, predicted_tree)
        list_results0.append(accuracy_tree)
        accuracy_km= accuracy_kendall_tau(actual, predicted_km)
        list_results0PL.append(accuracy_km)
        
                   
        # train test
        data=dataset1
        
        index=int(0.7*len(data))
        train_set=data[:index]
        test_set=data[index:]
        
        #fit models
        tree = build_tree(train_set, max_depth, min_size, index_categorical, index_discrete, index_pairs, 1)
        
        #one hot encoding
        nb_classes = 3
        targets = np.array([row[index_categorical[0]] for row in train_set]).reshape(-1)
        one_hot_targets = np.eye(nb_classes)[targets]
        nb_classes = 5
        targets2 = np.array([row[index_categorical[1]] for row in train_set]).reshape(-1)
        one_hot_targets2 = np.eye(nb_classes)[targets2]
        X_train=np.asarray([list(one_hot_targets[i])+list(one_hot_targets2[i]) for i in range(len(train_set))])
        
        nb_classes = 3
        targets = np.array([row[index_categorical[0]] for row in test_set]).reshape(-1)
        one_hot_targets = np.eye(nb_classes)[targets]
        nb_classes = 5
        targets2 = np.array([row[index_categorical[1]] for row in test_set]).reshape(-1)
        one_hot_targets2 = np.eye(nb_classes)[targets2]
        X_test=np.asarray([list(one_hot_targets[i])+list(one_hot_targets2[i]) for i in range(len(test_set))])
        
        
        Y_train=np.asarray([row[-1] for row in train_set])
        kmeans = KMeans(n_clusters=6, random_state=0).fit(X_train)
        PL_centers=list()
        for k in range(6):
            rankings=Y_train[kmeans.labels_==k]
            Gamma=PL_fitting_list(rankings,n_items,100)
            center=tuple(sorted(range(len(Gamma)), key=lambda k: Gamma[k], reverse=True))
            PL_centers.append(center)
        
        predicted_tree = list()
        predicted_km = list()
        for i in range(len(test_set)):
            row1=test_set[i]
            prediction_tree = predict(tree, row1[:n_features],index_categorical, index_discrete, 1)
            predicted_tree.append(prediction_tree)
            row2=X_test[i]
            predict_cluster=kmeans.predict(row2[:8])[0] #HERE !!!!
            prediction_km=PL_centers[predict_cluster]
            predicted_km.append(prediction_km)
        actual = [row[-1] for row in test_set]
        accuracy_tree = accuracy_kendall_tau(actual, predicted_tree)
        list_results1.append(accuracy_tree)
        accuracy_km= accuracy_kendall_tau(actual, predicted_km)
        list_results1PL.append(accuracy_km)
        
        
                     
        # train test
        data=dataset2
        
        index=int(0.7*len(data))
        train_set=data[:index]
        test_set=data[index:]
        
        #fit models
        tree = build_tree(train_set, max_depth, min_size, index_categorical, index_discrete, index_pairs, 1)
        
        #one hot encoding
        nb_classes = 3
        targets = np.array([row[index_categorical[0]] for row in train_set]).reshape(-1)
        one_hot_targets = np.eye(nb_classes)[targets]
        nb_classes = 5
        targets2 = np.array([row[index_categorical[1]] for row in train_set]).reshape(-1)
        one_hot_targets2 = np.eye(nb_classes)[targets2]
        X_train=np.asarray([list(one_hot_targets[i])+list(one_hot_targets2[i]) for i in range(len(train_set))])
        
        nb_classes = 3
        targets = np.array([row[index_categorical[0]] for row in test_set]).reshape(-1)
        one_hot_targets = np.eye(nb_classes)[targets]
        nb_classes = 5
        targets2 = np.array([row[index_categorical[1]] for row in test_set]).reshape(-1)
        one_hot_targets2 = np.eye(nb_classes)[targets2]
        X_test=np.asarray([list(one_hot_targets[i])+list(one_hot_targets2[i]) for i in range(len(test_set))])
        
        
        Y_train=np.asarray([row[-1] for row in train_set])
        kmeans = KMeans(n_clusters=6, random_state=0).fit(X_train)
        PL_centers=list()
        for k in range(6):
            rankings=Y_train[kmeans.labels_==k]
            Gamma=PL_fitting_list(rankings,n_items,100)
            center=tuple(sorted(range(len(Gamma)), key=lambda k: Gamma[k], reverse=True))
            PL_centers.append(center)
        
        predicted_tree = list()
        predicted_km = list()
        for i in range(len(test_set)):
            row1=test_set[i]
            prediction_tree = predict(tree, row1[:n_features],index_categorical, index_discrete, 1)
            predicted_tree.append(prediction_tree)
            row2=X_test[i]
            predict_cluster=kmeans.predict(row2[:8])[0] #HERE !!!!
            prediction_km=PL_centers[predict_cluster]
            predicted_km.append(prediction_km)
        actual = [row[-1] for row in test_set]
        accuracy_tree = accuracy_kendall_tau(actual, predicted_tree)
        list_results2.append(accuracy_tree)
        accuracy_km= accuracy_kendall_tau(actual, predicted_km)
        list_results2PL.append(accuracy_km)
        
        
        
        
    print 'n items' +str(n_items)
    print 'results of CRIT vs PL \n'
    print 'pw'
    print np.mean(list_results0)
    print np.mean(list_results0PL)
    print 'mallows1'
    print np.mean(list_results1)
    print np.mean(list_results1PL)
    print 'mallows2'
    print np.mean(list_results2)
    print np.mean(list_results2PL)
    print '\n'
