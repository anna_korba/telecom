#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from random import *
import pandas as pd
import numpy as np
from itertools import *
from collections import Counter
from copeland import Copeland, Borda_count, Borda_countbis, Plackett_count
import operator 
import math
from Mallows import ML_distribution
from knn_algorithm import knn_prediction
from crit_algorithm import accuracy_kendall_tau



#############################
#                           #
#      Evaluation           #
#                           #
#############################


n_samples=1000
n_items=5

k=1
n_features=2


def sample_from(d):
    r = uniform(0, sum(d.itervalues()))
    s = 0.0
    for k, w in d.iteritems():
        s += w
        if r < s: return k
    return k



#########################
#                       #
#    Setting 3          #
#                       #
#########################

list_results0=[]
list_results1=[]
list_results2=[]

n_experiments=20

for n_items in [3, 5,8]:
        
    for i in range(n_experiments):
        
        nb_classes1=3
        nb_classes2=5
        
        X1=np.random.randint(0,nb_classes1,n_samples)
        X2=np.random.randint(0,nb_classes2,n_samples)
                
        sigma1=tuple(np.random.permutation(range(0,n_items)))
        sigma2=tuple(np.random.permutation(range(0,n_items)))
        sigma3=tuple(np.random.permutation(range(0,n_items)))
        sigma4=tuple(np.random.permutation(range(0,n_items)))
        sigma5=tuple(np.random.permutation(range(0,n_items)))
        sigma6=tuple(np.random.permutation(range(0,n_items)))
        
        Y0=[]
        for i in range(n_samples):
            #if (X1[i]<.3) and (X2[i]<.5):
            #if (X1[i]<.3) and (X2[i]<=3): 
            if (X1[i]==0) and (X2[i]<=3): 
                Y0.append(sigma1)
            #elif (X1[i]<.3) and (X2[i]>.5):
            #elif (X1[i]<.3) and (X2[i]>3): 
            elif (X1[i]==0) and (X2[i]>3): 
                Y0.append(sigma2)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]==1 or X2[i]==3): 
            elif (X1[i]==1) and (X2[i]==1 or X2[i]==3): 
                Y0.append(sigma3)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]>.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<>1 and X2[i]<>3): 
            elif (X1[i]==1) and (X2[i]<>1 and X2[i]<>3):        
                Y0.append(sigma4)
            #elif (X1[i]>.85) and (X2[i]<.7):
            #elif (X1[i]>.85) and (X2[i]==1 or X2[i]==5): 
            elif (X1[i]==2) and (X2[i]==1 or X2[i]==4): 
                Y0.append(sigma5)
            else:
                Y0.append(sigma6)
        
        spread=2
        ML1=ML_distribution(sigma1,spread)
        ML2=ML_distribution(sigma2,spread)
        ML3=ML_distribution(sigma3,spread)
        ML4=ML_distribution(sigma4,spread)
        ML5=ML_distribution(sigma5,spread)
        ML6=ML_distribution(sigma6,spread)
        
        
        Y1=[]
        for i in range(n_samples):
            #if (X1[i]<.3) and (X2[i]<.5):
            #if (X1[i]<.3) and (X2[i]<=3): 
            if (X1[i]==0) and (X2[i]<=3): 
                sigma=sample_from(ML1)
            #elif (X1[i]<.3) and (X2[i]>.5):
            #elif (X1[i]<.3) and (X2[i]>3): 
            elif (X1[i]==0) and (X2[i]>3): 
                sigma=sample_from(ML2)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]==1 or X2[i]==3): 
            elif (X1[i]==1) and (X2[i]==1 or X2[i]==3): 
                sigma=sample_from(ML3)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]>.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<>1 and X2[i]<>3): 
            elif (X1[i]==1) and (X2[i]<>1 and X2[i]<>3):        
                sigma=sample_from(ML4)
            #elif (X1[i]>.85) and (X2[i]<.7):
            #elif (X1[i]>.85) and (X2[i]==1 or X2[i]==5): 
            elif (X1[i]==2) and (X2[i]==1 or X2[i]==4): 
                sigma=sample_from(ML5)
            else:
                sigma=sample_from(ML6)
            Y1.append(sigma)
        
        spread=1
        ML1=ML_distribution(sigma1,spread)
        ML2=ML_distribution(sigma2,spread)
        ML3=ML_distribution(sigma3,spread)
        ML4=ML_distribution(sigma4,spread)
        ML5=ML_distribution(sigma5,spread)
        ML6=ML_distribution(sigma6,spread)
        
        
        Y2=[]
        for i in range(n_samples):
            #if (X1[i]<.3) and (X2[i]<.5):
            #if (X1[i]<.3) and (X2[i]<=3): 
            if (X1[i]==0) and (X2[i]<=3): 
                sigma=sample_from(ML1)
            #elif (X1[i]<.3) and (X2[i]>.5):
            #elif (X1[i]<.3) and (X2[i]>3): 
            elif (X1[i]==0) and (X2[i]>3): 
                sigma=sample_from(ML2)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]==1 or X2[i]==3): 
            elif (X1[i]==1) and (X2[i]==1 or X2[i]==3): 
                sigma=sample_from(ML3)
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]>.3):
            #elif (X1[i]>.3) and (X1[i]<.85) and (X2[i]<>1 and X2[i]<>3): 
            elif (X1[i]==1) and (X2[i]<>1 and X2[i]<>3):        
                sigma=sample_from(ML4)
            #elif (X1[i]>.85) and (X2[i]<.7):
            #elif (X1[i]>.85) and (X2[i]==1 or X2[i]==5): 
            elif (X1[i]==2) and (X2[i]==1 or X2[i]==4): 
                sigma=sample_from(ML5)
            else:
                sigma=sample_from(ML6)
            Y2.append(sigma)
        
        #one hot encoding
        targets = X1.reshape(-1)
        X1 = np.eye(nb_classes1)[targets]
        targets = X2.reshape(-1)
        X2 = np.eye(nb_classes2)[targets]
                
        n_features=nb_classes1+nb_classes2
        
        dataset0=[list(X1[i])+list(X2[i])+[Y0[i]] for i in range(n_samples)]
        dataset1=[list(X1[i])+list(X2[i])+[Y1[i]] for i in range(n_samples)]
        dataset2=[list(X1[i])+list(X2[i])+[Y2[i]] for i in range(n_samples)]

        data=dataset0
        index=int(0.7*len(data))
        train_set=data[:index]
        test_set=data[index:]
        
        predicted_knn = list()
        for row in test_set:
            prediction=knn_prediction(train_set,row[:n_features],k)
            predicted_knn.append(prediction)
        actual = [row[-1] for row in test_set]
        accuracy_knn = accuracy_kendall_tau(actual, predicted_knn)
        list_results0.append(accuracy_knn)
        
        data=dataset1
        index=int(0.7*len(data))
        train_set=data[:index]
        test_set=data[index:]
        
        predicted_knn = list()
        for row in test_set:
            prediction=knn_prediction(train_set,row[:n_features],k)
            predicted_knn.append(prediction)
        actual = [row[-1] for row in test_set]
        accuracy_knn = accuracy_kendall_tau(actual, predicted_knn)
        list_results1.append(accuracy_knn)
        
        data=dataset2
        index=int(0.7*len(data))
        train_set=data[:index]
        test_set=data[index:]
        
        predicted_knn = list()
        for row in test_set:
            prediction=knn_prediction(train_set,row[:n_features],k)
            predicted_knn.append(prediction)
        actual = [row[-1] for row in test_set]
        accuracy_knn = accuracy_kendall_tau(actual, predicted_knn)
        list_results2.append(accuracy_knn)
        
    print 'Setting 3, n items' +str(n_items)
    print 'results of knn \n'
    print 'pw'
    print np.mean(list_results0)
    print 'mallows1'
    print np.mean(list_results1)
    print 'mallows2'
    print np.mean(list_results2)
