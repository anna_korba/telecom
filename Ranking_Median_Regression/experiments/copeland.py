#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from random import seed
from random import randrange
import csv
import pandas as pd
import numpy as np
from itertools import combinations
import random
from collections import Counter
from itertools import *
import copy

from Plackett import *


def swap(i,j,sigma):
    result=list(sigma)
    a=sigma.index(i)
    b=sigma.index(j)
    result[a]=j
    result[b]=i
    return tuple(result)


def induced_subword(w,A):
    t = ()
    for i in w:
        if i in A:
            t = t + (i,)
    return t

def Restrict(h, A):
    h_=dict.fromkeys(permutations(A),0)
    for sigma in h.keys():
        sigma_=induced_subword(sigma,A)
        h_[sigma_]+=h[sigma]
    return h_

def Copeland(P):
    A=set(P.keys()[0])
    Copeland = dict.fromkeys(A,0)
    for i in A:
        Abis=list(copy.copy(A))
        Abis.remove(i)
        for j in Abis:
            p_pair=Restrict(P,(i,j))
            if p_pair[(i,j)] < 0.5:
                Copeland[i] += 1
    for i in A:
        Copeland[i]= np.around(Copeland[i], decimals=10)
    pi = ()
    
    for key,value in sorted(Copeland.iteritems(), key=lambda (k,v):(v,k)):
        
        pi = pi + (key,)
    
    #if len(Copeland.values()) <> len(set(Copeland.values())):
        #print 'equalities for Copeland...'
    #return pi, Copeland
    return pi
        
    #    return pi
    #else:
    #    outputs=[pi]
    #    rev_multidict = {}
    #    for key, value in Copeland.items():
    #        rev_multidict.setdefault(value, set()).add(key)
    #    l=list(chain.from_iterable(values for key, values in rev_multidict.items() if len(values) > 1))
    #    for (i,j) in combinations(l,2):
    #        pi_=swap(i,j,pi)
    #        outputs.append(pi_)
    #    return outputs
    
def Copelandbis(P):
    A=set(P.keys()[0])
    Copeland = dict.fromkeys(A,0)
    for i in A:
        Abis=list(copy.copy(A))
        Abis.remove(i)
        for j in Abis:
            p_pair=Restrict(P,(i,j))
            if p_pair[(i,j)] < 0.5:
                Copeland[i] += 1
    for i in A:
        Copeland[i]= np.around(Copeland[i], decimals=10)
    pi = ()
    
    for key,value in sorted(Copeland.iteritems(), key=lambda (k,v):(v,k)):
        
        pi = pi + (key,)
    return pi, Copeland
    
def Kendall_tau(w,t):
    d = 0
    r_w = dict.fromkeys(w)
    r_t = dict.fromkeys(t)
    for i in range(len(w)):
        r_w[w[i]] = i+1
        r_t[t[i]] = i+1
    for c in combinations(w,2):
        if (r_w[c[1]]-r_w[c[0]])*(r_t[c[1]]-r_t[c[0]]) < 0:
            d += 1
    return d

# Returns (one of) the Borda count(s) for P on A
def Borda_count(P):
    A=set(P.keys()[0])
    Borda = dict.fromkeys(A,0)
    for t in P:
        for i in range(len(A)):
            Borda[t[i]] += (i+1)*P[t]
    for i in A:
        Borda[i]= np.around(Borda[i], decimals=10)
    pi = ()
    
    #if len(Borda.values()) <> len(set(Borda.values())):
    #    raise Exception('cycles')
    #if len(Borda.values()) == len(set(Borda.values())):
     #   print 'scores borda '+str(sorted(Borda.iteritems(), key=lambda (k,v):(v,k)))
    for key,value in sorted(Borda.iteritems(), key=lambda (k,v):(v,k)):
        pi = pi + (key,)
    #return pi, Borda
    return pi
    
def Borda_countbis(P):
    A=set(P.keys()[0])
    Borda = dict.fromkeys(A,0)
    for t in P:
        for i in range(len(A)):
            Borda[t[i]] += (i+1)*P[t]
    for i in A:
        Borda[i]= np.around(Borda[i], decimals=10)
    pi = ()
    
    #if len(Borda.values()) <> len(set(Borda.values())):
    #    raise Exception('cycles')
    #if len(Borda.values()) == len(set(Borda.values())):
     #   print 'scores borda '+str(sorted(Borda.iteritems(), key=lambda (k,v):(v,k)))
    for key,value in sorted(Borda.iteritems(), key=lambda (k,v):(v,k)):
        pi = pi + (key,)
    return pi, Borda
    #return pi

def Borda_count2(P):
    A=set(P.keys()[0])
    Borda = dict.fromkeys(A,0)
    for i in A:
        Abis=list(copy.copy(A))
        Abis.remove(i)
        for j in Abis:
            p_pair=Restrict(P,(i,j))
            Borda[i] += p_pair[(i,j)]-p_pair[(j,i)]
    for i in A:
        Borda[i]= np.around(Borda[i], decimals=10)
    pi = ()
    for key,value in sorted(Borda.iteritems(), key=lambda (k,v):(v,k), reverse=True):
        pi = pi + (key,)
    return pi, Borda
    """
    #print 'borda approval '+str(sorted(Borda.iteritems(), key=lambda (k,v):(v,k)))
    for key,value in sorted(Borda.iteritems(), reverse= True, key=lambda (k,v):(v,k)):
        pi = pi + (key,)    
    if len(Borda.values()) == len(set(Borda.values())):
        return pi
    else:
        outputs=[pi]
        rev_multidict = {}
        for key, value in Borda.items():
            rev_multidict.setdefault(value, set()).add(key)
        l=list(chain.from_iterable(values for key, values in rev_multidict.items() if len(values) > 1))
        for (i,j) in combinations(l,2):
            pi_=swap(i,j,pi)
            outputs.append(pi_)
        return outputs    
    """
    
def Borda_countbisbis(outcomes):
    A=set(outcomes[0])
    Borda = dict.fromkeys(A,0)
    for i in A:
        Abis=list(copy.copy(A))
        Abis.remove(i)
        for l in outcomes:
            for j in Abis:
                Borda[i]+=np.sign(l.index(i)-l.index(j))
    for i in A:
        Borda[i]= np.around(Borda[i], decimals=10)
    pi = ()
    for key,value in sorted(Borda.iteritems(), key=lambda (k,v):(v,k)):
        pi = pi + (key,)
    return pi, Borda
 
    
def Plackett_count(P):
    niter=10
    n=len(P.keys()[0])
    D=PL_fitting_dict(P,n,niter)
    D_niter=D[niter]
    for i in D_niter.keys():
        D_niter[i]= np.around(D_niter[i], decimals=10)
    pi = ()
    
    #print 'scores plackett '+str(sorted(D_niter.iteritems(), key=lambda (k,v):(v,k)))
    for key,value in sorted(D_niter.iteritems(), reverse= True, key=lambda (k,v):(v,k)):
            pi = pi + (key,) 
    #if len(D_niter.values()) == len(set(D_niter.values())):
    return pi
    #else:
    #    outputs=[pi]
    #    rev_multidict = {}
    #    for key, value in D_niter.items():
    #        rev_multidict.setdefault(value, set()).add(key)
    #    l=list(chain.from_iterable(values for key, values in rev_multidict.items() if len(values) > 1))
    #    for (i,j) in combinations(l,2):
    #        pi_=swap(i,j,pi)
    #        outputs.append(pi_)
    #    return outputs


"""
group1=[[0, 1, 165, 11, 3, 0, 11, 3, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, (7, 2, 3, 0, 8, 1, 6, 5, 4, 9)], [0, 1, 165, 11, 3, 0, 11, 3, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, (4, 7, 2, 0, 3, 8, 5, 9, 6, 1)]]
group2=[[0, 2, 167, 25, 6, 1, 25, 6, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, (7, 2, 5, 8, 1, 9, 0, 6, 4, 3)]]

l1=[x[-1] for x in group1]
l2=[x[-1] for x in group2]

P1=dict(Counter(l1))
P2=dict(Counter(l2))

s1=Copeland(P1)
s2=Copeland(P2)

"""

