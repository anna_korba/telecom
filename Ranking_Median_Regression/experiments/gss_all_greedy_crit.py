
#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from random import *
import pandas as pd
import numpy as np
from itertools import *
from collections import Counter
from itertools import *
import csv

from crit_algorithm import *

#############################
#                           #
#         Data              #
#                           #
#############################

import warnings
warnings.filterwarnings("ignore")

#df_original=pd.read_stata('data/GSS/GSS7216_R1a.dta', convert_categoricals=False, convert_missing=False)

df=df_original
df2=df_original[(df_original['year']==1973) |(df_original['year']==1985) | (df_original['year']==2006)]

print 'df original '+str(df.shape)

columns_job=['jobinc', 'jobsec', 'jobhour','jobpromo', 'jobmeans']
#columns_job=['jobinc', 'jobsec', 'jobpromo', 'jobmeans']

df = df[np.isfinite(df['jobinc'])]
df = df[np.isfinite(df['jobsec'])]
df = df[np.isfinite(df['jobhour'])]
df = df[np.isfinite(df['jobpromo'])]
df = df[np.isfinite(df['jobmeans'])]


n_items=len(columns_job)


print 'shape after job ' +str(df.shape)

columns_to_keep_categorical=['wrkstat','wrkslf','occ10', 'sex','race' ,'marital']

columns_to_keep_discrete=['cohort', 'degree', 'income' ,'childs', 'hompop']


columns_to_keep_numerical=[]

columns_to_keep1= columns_to_keep_categorical+columns_to_keep_discrete+columns_to_keep_numerical+ columns_job

df=df[columns_to_keep1]


# drop na

df=df[np.isfinite(df)]
df=df.dropna( axis=0, how='any', thresh=None, subset=None)
print 'final shape '+str(df.shape)



# OCCUPATION

df.occ10[(df.occ10 >=10) & (df.occ10 <= 950)] =2 #managerial and professionnal
df.occ10[(df.occ10 >=1000) & (df.occ10 <= 3540)] =1 #professional, related
df.occ10[(df.occ10 >=3600) & (df.occ10 <= 4650)] =5 # service occupations
df.occ10[(df.occ10 >=4700) & (df.occ10 <= 5940)] =4 # sales workers
df.occ10[(df.occ10 >=6000) & (df.occ10 <= 7630)] =6 #NATURAL RESOURCES, CONSTRUCTION, AND construction
df.occ10[(df.occ10 >=7700) & (df.occ10 <= 9830)] =7 #PRODUCTION, TRANSPORTATION, AND
df.occ10[df.occ10 ==9997] =8 #clerical?



# COHORT

df.cohort[(df.cohort >=1883) & (df.cohort <= 1889)] =1
df.cohort[(df.cohort >1889) & (df.cohort <= 1899)] =2
df.cohort[(df.cohort >1899) & (df.cohort <= 1909)] =3
df.cohort[(df.cohort >1909) & (df.cohort <= 1919)] =4
df.cohort[(df.cohort >1919) & (df.cohort <= 1929)] =5
df.cohort[(df.cohort >1929) & (df.cohort <= 1939)] =6
df.cohort[(df.cohort >1939) & (df.cohort <= 1949)] =7
df.cohort[(df.cohort >1949) & (df.cohort <= 1959)] =8
df.cohort[(df.cohort >1959) & (df.cohort <= 1969)] =9
df.cohort[(df.cohort >1969) & (df.cohort <= 1979)] =10
df.cohort[(df.cohort >1979) & (df.cohort <= 1989)] =11
df.cohort[(df.cohort >1989) & (df.cohort <= 1999)] =12



items=range(0,n_items)

job_orders=[]
for m in range(df.shape[0]):
    row=list(df[columns_job].iloc[m,:])
    preferences=[]
    #l=tuple([int(j-1) for j in df[columns_job].iloc[i,:]])
    l=sorted(range(len(row)), key=lambda k: row[k])
    for (i,j) in combinations(items,2):
        if l.index(i)<l.index(j):
            preferences.append(1)
        else:
            preferences.append(0)
    preferences.append(tuple(l))
    job_orders.append(preferences)

df.drop(columns_job, axis=1, inplace=True)
print 'df shape, number of features'+str(df.shape)


job_users=df.values.tolist()
n_features=len(job_users[0])


#categorical_features=columns_to_keep_categorical
categorical_features=[name for name in df.columns if any([name.startswith(name_original) for name_original in columns_to_keep_categorical])]
values_categorical_features=[np.unique(df[[column]]) for column in categorical_features ]
index_categorical=[list(df.columns).index(x) for x in categorical_features]

#discrete_features=list(set(columns_to_keep_discrete).intersection(set(columns_to_keep1)))
discrete_features=columns_to_keep_discrete
values_discrete_features=[np.unique(df[[column]]) for column in discrete_features ]
index_discrete=[list(df.columns).index(x) for x in discrete_features]


dataset=[job_users[i]+job_orders[i] for i in range(len(job_orders))]

n_pairs=int((n_items*(n_items-1))/float(2))
index_pairs=range(n_features, n_features+n_pairs)



max_depth =10
min_size = 100

max_depths=[5,10,20,30, 40, 50, 60, 70, 80, 90, 100]
max_depths=[1,2,3,4,5, 6, 7, 8, 9, 10]
max_depths=[5]

#min_sizes=[5,10, 20, 50, 100, 200, 400]
min_sizes=[100,200,400]
min_sizes=[100]
 
for max_depth in max_depths:
    for min_size in min_sizes:
        print max_depth, min_size
        #data=list(random.sample(dataset,6000))
        data=dataset
        shuffle(data)
        index=int(0.75*len(data))
        train_set=data[:index]
        n_samples=len(train_set)
        test_set=data[index:]
        
        print '\n final tree'
        tree = build_tree(train_set, max_depth, min_size, index_categorical, index_discrete, index_pairs, 1)
        
        predicted = list()
        for row in test_set:
        	prediction = predict(tree, row[:n_features], index_categorical, index_discrete, 1)
        	predicted.append(prediction)
        
        
        #print_tree(tree)
        
        actual = [row[-1] for row in test_set]
        print 'max depth and min size '+str(max_depth)+ ' , '+str(min_size) 
        print 'mean kendall distance'
        accuracy = accuracy_kendall_tau(actual, predicted)
        print accuracy
        
        unique_data_actual = [list(x) for x in set(tuple(x) for x in actual)]
        unique_data_predicted = [list(x) for x in set(tuple(x) for x in predicted)]
        print len(unique_data_actual)
        print len(unique_data_predicted)
        
        test_actual=[len([row for row in actual if row[0]==i]) for i in range(n_items)]
        test_predicted=[len([row for row in predicted if row[0]==i]) for i in range(n_items)]
        
        print test_actual
        print test_predicted
        

        
        #f = open('resultsn5_2.csv','a')
        #f.write('max_depth, min_size '+str(max_depth)+' ,' +str(min_size)+'\n')
        #f.write('accuracy '+str(accuracy)+'\n')
        #f.write('unique actual , unique predicted '+str(len(unique_data_actual))+' , '+str(len(unique_data_predicted))+'\n')
        #f.write('proportions actual, proportions predicted '+str(test_actual)+' , '+str(test_predicted)+'\n')
        #f.write('\n') #Give your csv text here.
        #f.close()
        

# row[-100][25]=1 donc ca devrait etre 0,2,3,1

    #print 'accuracy on total rankings'
    #accuracy2 = accuracy_metric(actual, predicted)
    print 'accuracy first'
    accuracy3 = accuracy_first(actual, predicted)
    print accuracy3
    print 'accuracy second'
    accuracy4 = accuracy_second(actual, predicted)
    print accuracy4
