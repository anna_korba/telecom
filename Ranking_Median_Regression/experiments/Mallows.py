# -*- coding: utf-8 -*-
from itertools import *
from math import *
from collections import Counter
from Distances_on_rankings import *


# Computes the Mallows distribution p(\sigma) = C.exp(-spread*Kendall(id,sigma))

# big spread makes the distribution more spiky

def ML_distribution(sigma0,spread):
    n=len(sigma0)
    Z = 1
    for j in range(1,n+1):
        Z *= (1-exp(-j*spread))/(1-exp(-spread))
    #sigma0 = tuple(range(1,n+1))
    p = dict.fromkeys(permutations(range(0,n)))
    for sigma in p:
        p[sigma] = exp(-spread*Kendall_tau(sigma0,sigma))/Z
    return p
