\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Why ranking data?}{2}{0}{1}
\beamer@sectionintoc {2}{The analysis of ranking data}{12}{0}{2}
\beamer@sectionintoc {3}{Harmonic analysis on $\mathfrak {S}_{n}$}{31}{0}{3}
\beamer@sectionintoc {4}{The need for a new representation}{50}{0}{4}
\beamer@sectionintoc {5}{The MRA representation}{62}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{77}{0}{6}
