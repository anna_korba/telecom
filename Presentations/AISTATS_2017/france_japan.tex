\documentclass[10pt]{beamer}
\usepackage[utf8]{inputenc}

\usetheme{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usepackage{xspace}
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}





\usepackage{bbm}
\usepackage{float}
\usepackage{tabularx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{empheq}
\usepackage{tikz}
\usetikzlibrary{positioning}
\tikzset{main node/.style={circle,fill=blue!20,draw,minimum size=0.6cm,inner sep=0pt}, }

\usepackage{stmaryrd}
\usetikzlibrary{matrix,arrows}
\usepackage{url}
\usepackage{comment}
\usepackage{multirow}
\usepackage{arydshln}
%\usepackage{kbordermatrix}

% Shortcuts
\newcommand{\fixme}[1]{{\bf [FIXME: #1]}}
\newcommand{\OMIT}[1]{}	% to ignore things
\newcommand{\Sn}{\mathfrak{S}_n}
\newcommand{\FF}{\mathcal{F}}
\newcommand{\dFF}{d_{\FF}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\Sphere}{\mathbb{S}}
\newcommand{\n}{\llbracket n \rrbracket}
\newcommand{\hollowone}{\mathds{1}}
\newcommand{\br}[1]{\left(#1\right)}
\newcommand{\sbr}[1]{\left[#1\right]}
\newcommand{\cbr}[1]{\left\{#1 \right\}}
\newcommand{\abs}[1]{\left\vert #1 \right\vert}
\newcommand{\nm}[1]{\left\Vert #1 \right\Vert}
\newcommand{\innerprod}[1]{\langle #1 \rangle}
\newcommand{\norm}[1]{\Vert #1 \Vert}
\newcommand{\sigstar}{\sigma^{*}}
\newcommand{\sigstarstar}{\sigma^{**}}
\newcommand{\sighat}{\hat{\sigma}}
\newcommand{\Eff}{\operatorname{Eff}}
\newcommand{\sign}{\operatorname{sign}}
\newcommand{\Span}{\operatorname{span}}
\newcommand{\DN}{\mathcal{D}_{N}}
\newcommand{\KN}{\mathcal{K}_{N}}
\newcommand{\CN}{\mathcal{C}_{N}}
\newcommand{\II}{\mathfrak{I}}	% injection opposed to \Pi
\newcommand{\Ctil}{\widetilde{C}}

\newcommand\Sym[1]{\mathfrak{S}_{#1}}
\newcommand\Rank[1]{\Gamma(#1)}
\newcommand\1[1]{\mathds{1}_{#1}}
\newcommand\set[1]{\llbracket #1 \rrbracket}
\newcommand{\argmin}{\operatornamewithlimits{argmin}}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}
\newcommand{\supp}{\operatorname{supp}}
\newcommand\Subsets[1]{\mathcal{P}(#1)}
\newcommand\SubsetsWE[1]{\mathcal{\bar{P}}(#1)}
\newcommand{\EOD}{\widehat{\mathcal{A}}_{N}}
\newcommand{\Space}[1]{L(#1)}
\newcommand{\formula}[1]{\textcolor{myorange}{#1}}
\newcommand{\precision}[1]{\textcolor{myblue}{#1}}
\newcommand{\attention}[1]{\textcolor{red}{#1}}
\newcommand{\illustration}[1]{\textcolor{mygreen}{#1}}

%\definecolor{myorange}{rgb}{0.7,0.3,0.3}
\definecolor{myorange}{rgb}{0.9,0.5,0.2}
\definecolor{mygrey}{rgb}{0.7,0.7,0.7}
\definecolor{mygreen}{rgb}{0,0.5,0}
\definecolor{myblue}{rgb}{0.2,0.2,0.7}

% pour le background derriere les equations
\definecolor{myblue2}{rgb}{.8, .8, 1}
\newcommand*\mybluebox[1]{%
\colorbox{myblue2}{\hspace{1em}#1\hspace{1em}}}


\definecolor{myred}{rgb}{0.93, 0.64, 0.6}
\newcommand*\myredbox[1]{%
\colorbox{myred}{\hspace{1em}#1\hspace{1em}}}


\title{A Learning Theory of Ranking Aggregation}
\subtitle{France/Japan Machine Learning Workshop}
\date{\today}
\author{Anna Korba, Stéphan Clémençon, Eric Sibony}
\institute{Télécom ParisTech}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}


\begin{filecontents}{\jobname.bib}
@inproceedings{KB05,
	Title = {Exponential Convergence Rates in Classification},
	Author = {Koltchinskii, V. and Beznosova, O.},
	Booktitle = {Proceedings of COLT 2005},
	Year = {2005},
}

@inproceedings{dwork2001rank,
  Title={Rank aggregation methods for the web},
  Author={Dwork, Cynthia and Kumar, Ravi and Naor, Moni and Sivakumar, Dandapani},
  booktitle={Proceedings of the 10th international conference on World Wide Web},
  pages={613--622},
  year={2001},
  organization={ACM}
}

@article{bartholdi1989voting,
  Title={Voting schemes for which it can be difficult to tell who won the election},
  Author={Bartholdi, John and Tovey, Craig A and Trick, Michael A},
  journal={Social Choice and welfare},
  volume={6},
  number={2},
  pages={157--165},
  year={1989},
  publisher={Springer}
}
\end{filecontents}
\usepackage[style=authoryear]{biblatex}
\renewcommand*{\nameyeardelim}{\addcomma\addspace}

\addbibresource{\jobname.bib}



\begin{document}

\maketitle

\begin{frame}{Outline}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents[hideallsubsections]
\end{frame}

\section{The Ranking Aggregation Problem}

\begin{frame}[fragile]{The Ranking Aggregation Problem}
	\begin{block}{Framework}
		\vspace{-0.2cm}
		\begin{itemize}
			\item \textcolor{myorange}{$n$} items: \textcolor{myorange}{$\{1,\dots,n\}$}.
			\item \textcolor{myorange}{$N$} agents rank all the items according to their preferences:\\
			\textcolor{myorange}{$i_1 \succ i_2\succ \dots \succ i_n$}.\\
			
			%\textcolor{mygreen}{Example:} $2 \succ 3\succ 1 \succ 4$
			 \item \textcolor{myorange}{$i_1 \succ \dots \succ i_n$} $\Longleftrightarrow$ permutation \textcolor{myorange}{$\sigma$} on $\{1,\dots,n\}$ s.t. \textcolor{myorange}{$\sigma(i_j) = j}$}.\\
			 % \textcolor{mygreen}{Example:} $\sigma(2)=1, \sigma(3)=2 \dots$
		\end{itemize}
	\end{block}
	\pause
%	\vspace{-0.2cm}
    
%	\begin{itemize}
%	    \item A dataset of \textcolor{myorange}{$N$} rankings is thus a dataset of permutations \textcolor{myorange}{$(\sigma_1,\dots,\sigma_N)} \in \mathfrak{S}_n^N$.
%	\end{itemize}
%	\pause
	\metroset{block=fill}
	\begin{block}{Consensus Ranking} 
	    Suppose we have a dataset of rankings/permutations \textcolor{myorange}{$(\sigma_1,\dots,\sigma_N)} \in \mathfrak{S}_n^N$}. We want to find a global order ("consensus") $\sigma^{*}$ on the $n$ items that best represents the dataset.
	\end{block}
\end{frame}


\begin{frame}[fragile]{Methods for Ranking Aggregation}
\textbf{Copeland Rule}.\\
Sort the items according to their Copeland score, defined for each item $i$ by:
%\begin{itemize}
%\item Borda score: $s_B(i)=\frac{1}{N} \sum_{t=1}^{N}(n+1-\sigma_t(i))$
%\vspace{0.2cm}
%\item Copeland score: $s_C(i)=\frac{1}{N} \sum\limits_{t=1}^{N} \sum\limits_{{\substack{j=1 \\ j\ne i}}}^{n}\mathbb{I}[\sigma_t(i)<\sigma_t(j)]$
%\end{itemize}
\begin{equation*}
    s_C(i)=\frac{1}{N} \sum\limits_{t=1}^{N} \sum\limits_{{\substack{j=1 \\ j\ne i}}}^{n}\mathbb{I}[\sigma_t(i)<\sigma_t(j)]
\end{equation*}
\pause

\textbf{Kemeny's rule (1959)}.\\
Find the solution of :
    \begin{empheq}[box=\mybluebox]{align}
min_{\sigma \in \Sn} \sum_{i=1}^{N} d(\sigma,\sigma_t)
\end{empheq}
where $d$ is the Kendall's tau distance:
        \begin{empheq}[box=\mybluebox]{equation*}
		d_{\tau}(\sigma,\sigma')=\sum_{i<j}\mathbb{I}\{(\sigma(i)-\sigma(j)) (\sigma'(i)-\sigma'(j))<0   \},
		\end{empheq}
%\textcolor{mygreen}{Example:}
%$\sigma$= 1234, $\sigma'$= 2413 $\Rightarrow  d(\sigma, \sigma')= 3$  (disagree on 12,14,34).}
Kemeny's consensus has a lot of interesting properties, but it is NP-hard to compute.
\end{frame}




\section{Statistical Framework}



\begin{frame}[fragile]{Statistical reformulation}
    Suppose the dataset is composed of $N$ i.i.d. copies  $\Sigma_1, \dots, \Sigma_N$ of a r.v. $\Sigma \sim P$. A (Kemeny) \textbf{median} of $P$ w.r.t. $d$ is solution of:
	\begin{empheq}[box=\mybluebox]{equation*}
	\min_{\sigma \in \mathfrak{S}_n}\mathbb{E}_{\Sigma \sim P}[d(\Sigma,\sigma)  ],
	\end{empheq}
	where $L(\sigma)=\mathbb{E}_{\Sigma \sim P}[d(\Sigma,\sigma)  ]$ is \textbf{the risk} of $\sigma$.
	\vspace{0.2cm}
	\pause
	
	
	
	Let $\widehat{L}_N(\sigma)=\frac{1}{N}\sum_{t=1}^Nd(\Sigma_t,\sigma)$.
	
\pause
	\begin{alertblock}{Goal of our analysis:}
    Study the performance of \textbf{Kemeny empirical medians}, i.e. solutions $\widehat{\sigma}_N$ of:
	\begin{equation*}
	\min_{\sigma\in \mathfrak{S}_n}\widehat{L}_N(\sigma),
	\end{equation*}
 through the excess of risk $\mathbb{E}\left[L(\widehat{\sigma}_N)-L^*\right]$.
	\end{alertblock}
	%\textcolor{myorange}{$\Rightarrow$ We also establish links with Copeland and Borda methods.}
\end{frame}



\begin{frame}[fragile]{Risk of Ranking Aggregation}
The risk of a median $\sigma$ is $
    L(\sigma)=\mathbb{E}_{\Sigma \sim P}[d(\Sigma,\sigma)  ]$,
where $d$ is the Kendall's tau distance:
        \begin{equation*}
		d(\sigma, \sigma') = \sum_{\{i,j\} \subset \n} \{(\sigma(i)- \sigma(j))(\sigma'(i)-\sigma'(j))<0 \}
		\end{equation*}
\\
Let \textcolor{red}{$p_{i,j}= \mathbb{P}[\Sigma(i)<\Sigma(j)]$} the probability that item $i$ is preferred to item $j$.\\		
\pause
\vspace{0.2cm}
The risk can be rewritten:
	\begin{multline*}%\label{eq:loss-1}
	L(\text{\textcolor{blue}{$\sigma$}})=\sum_{i<j} \text{\textcolor{red}{$p_{i,j}$}}\mathbb{I}\{\text{\textcolor{blue}{$\sigma(i)$}}>\text{\textcolor{blue}{$\sigma(j)$}} \} +	\sum_{i<j} (1-\text{\textcolor{red}{$p_{i,j}$}})\mathbb{I}\{\text{\textcolor{blue}{$\sigma(i)$}}<\text{\textcolor{blue}{$\sigma(j)$}} \}.
	\end{multline*}
	%$\mathbb{P}[Y\ne \text{\textcolor{blue}{$h(x)$}}|X=x]$ = \textcolor{red}{$\eta(x)$}$\mathbb{I}[\text{\textcolor{blue}{$h(x)$}} = 0]$ + (1-\textcolor{red}{$\eta(x)$}) $\mathbb{I}[\text{\textcolor{blue}{$h(x)$}} = 1]$\\
\pause
So if there exists a permutation $\sigma$ verifying: $\forall i<j$ s.t. $p_{i,j}\neq 1/2$,
	\begin{equation}\label{eq:cond_opt}
	\left(\text{\textcolor{blue}{$\sigma(j)$}}-\text{\textcolor{blue}{$\sigma(i)$}}\right)\cdot \left( \text{\textcolor{red}{$p_{i,j}$}}- 1/2\right)>0,
	\end{equation}
it would be necessary of median for $P$.
\end{frame}


\section{Convergence Rates}


\begin{frame}{Universal Rates}
%Let  $\widehat{L}_N(\sigma)=\frac{1}{N}\sum_{t=1}^Nd(\Sigma_t,\sigma)$ et  $L(\sigma)=\mathbb{E}_{\Sigma \sim P}[d(\Sigma,\sigma)  ]$.
%\begin{alertblock}{We want to compare the theoretical risk $L$ of:}
%\vspace{-0.3cm}
%\begin{itemize}
%    \item the (an) empirical solution: $\widehat{\sigma}_N=\argmin_{\sigma\in \mathfrak{S}_n}\widehat{L}_N(\sigma)$
%    \item the (an) optimal solution: $\sigma^*=\argmin_{\sigma\in \mathfrak{S}_n}L(\sigma) $
%\end{itemize}
%Donc $\mathbb{E}\left[L(\widehat{\sigma}_N)-L^*\right]$. Dans le cas général (pas d'hypothèses sur la distribution $P$), nous obtenons...
%\end{alertblock}
%\pause
%\begin{block}{Des vitesses de convergence classiques:}
%$\mathbb{E}\left[L(\widehat{\sigma}_N)-L^*\right]$ décroît à vitesse $\frac{1}{\sqrt{n}}$.
%\end{alertblock}

%\textbf{Results.}
The excess risk of $\widehat{\sigma}_N$ is upper bounded:
		\vspace{-0.3cm}
		\begin{itemize}
			\itemsep-0.5em
			\item[(i)] In expectation by
			\[
			\mathbb{E}\left[L(\widehat{\sigma}_N)-L^*\right]\leq \frac{n(n-1)}{2\sqrt{N}}
			\]
			\item[(ii)] With probability higher than $1-\delta$ for any $\delta\in(0,1)$ by
			\begin{equation*}
			L(\widehat{\sigma}_{N}) - L^{\ast} \leq  \frac{n(n-1)}{2}\sqrt{\frac{2\log(n(n-1)/\delta)}{N}}.
			\end{equation*}
		\end{itemize}
%These rates are optimal since we also prove a minimax lower bound: 
%\begin{equation*}
%		\mathcal{R}_N \geq  \frac{1}{16e\sqrt{N}}.
%		\end{equation*}	
\end{frame}

\setlength{\abovedisplayskip}{3pt}
\setlength{\belowdisplayskip}{3pt}

\begin{frame}{Conditions for Fast Rates}

Suppose that $P$ verifies:
\begin{itemize}
\item the \textcolor{myorange}{Stochastic Transitivity} condition:
\begin{empheq}[box=\myredbox]{align}
		p_{i,j} \ge 1/2 \text{ and } p_{j,k}\ge 1/2 \Rightarrow
		p_{i,k}\ge 1/2.
\end{empheq}
\item the \textcolor{myorange}{Low-Noise} condition \textcolor{myorange}{\textbf{NA}$(h)$} for some $h>0$:
	\begin{empheq}[box=\myredbox]{align}
	\min_{i<j} \vert p_{i,j}-1/2 \vert \geq h.
	\end{empheq}
\end{itemize}
%\pause
%\begin{itemize}
%    \item This condition may be considered as analogous to that introduced in \cite{KB05}.
%    \item bla 
%\end{itemize}
\end{frame}

\begin{frame}{Fast rates}

Let \textcolor{myblue}{$\alpha_{h} = \frac{1}{2}\log \left(1/(1-4h^2)\right)$}.
%and \textcolor{myblue}{$\beta_{h} = 2h\log((1+2h)/(1-2h))$}.

\vspace{0.1cm}

Assume that $P$ satisfies the previous conditions. 
		\vspace{-0.2cm}
		\begin{itemize}
			\itemsep-0.3em
			\item[(i)] For any empirical Kemeny median $\widehat{\sigma}_N$, we have: 
			\[
			\mathbb{E}\left[L(\widehat{\sigma}_N)-L^*\right]\leq \frac{n^2(n-1)^2}{8} e^{-\textcolor{myblue}{\alpha_h}N}.
			\]
			\item[(ii)]	With probability at least $1-(n(n-1)/4) e^{-\textcolor{myblue}{\alpha_h}N}$, the empirical Copeland score
			$$\widehat{s}_N(i)=1+\sum_{k\ne i}\mathbb{I}\{\widehat{p}_{i,k}<\frac{1}{2} \}$$ for $1\leq i\leq n$ belongs to $\Sn$ and is the unique solution of Kemeny empirical minimization. 
		\end{itemize}
%Again, (almost) optimal rates:
%\begin{equation*}
%\label{eq:minimax_lownoise}
%\widetilde{\mathcal{R}}_N(h)\geq \frac{h}{4} e^{- \textcolor{myblue}{\beta_h}N}.
%\end{equation*}
	
%	We have \textcolor{myblue}{$\alpha_h \sim \frac{1}{2}\beta_h$} when \textcolor{myblue}{$h \rightarrow \frac{1}{2}$}.
\textcolor{myorange}{$\Rightarrow$ In practice: under the needed conditions, Copeland method ($\mathcal{O}$($N \binom{n}{2})$) outputs the Kemeny consensus (NP-hard) with high prob.}

\end{frame}
 %Conséquence pratique du dernier résultat: sous la condition low-noise, la méthode de Copeland ($\mathcal{O}$($N \binom{n}{2})$) donne le consensus de Kemeny (NP-difficile) avec très grande probabilité.
 

\begin{frame}{Conclusion and future directions}

\begin{itemize}
\setlength\itemsep{1em}
    \item We introduced a general statistical framework for ranking aggregation and established rates of convergence.
    \pause
    \item The computation of Kendall's $\tau$ distance involves pairwise comparisons only $\Rightarrow$ one could compute empirical versions of the risk functional $L$ when the observations are only pairwise comparisons between items.
    \pause
    \item The transitivity/low noise condition have good chance to be satisfied when similar agents produce the rankings 
    \pause
\end{itemize}


\end{frame}

\end{document}


