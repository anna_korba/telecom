\beamer@sectionintoc {1}{Introduction to ranking data}{3}{0}{1}
\beamer@sectionintoc {2}{Label ranking}{21}{0}{2}
\beamer@sectionintoc {3}{Structured prediction for label ranking}{28}{0}{3}
\beamer@sectionintoc {4}{Ranking embeddings}{36}{0}{4}
\beamer@sectionintoc {5}{Computational and theoretical analysis}{40}{0}{5}
\beamer@sectionintoc {6}{Openings and conclusion}{52}{0}{6}
