\beamer@sectionintoc {1}{Introduction and tools}{3}{0}{1}
\beamer@sectionintoc {2}{MMD Gradient flow}{7}{0}{2}
\beamer@sectionintoc {3}{Background on gradient flows}{9}{0}{3}
\beamer@sectionintoc {4}{Maximum Mean Discrepancy Gradient Flow}{14}{0}{4}
\beamer@sectionintoc {5}{Investigating MMD gradient flow convergence}{17}{0}{5}
\beamer@sectionintoc {6}{A practical algorithm to descend the MMD flow}{25}{0}{6}
\beamer@sectionintoc {7}{Applications}{33}{0}{7}
\beamer@sectionintoc {8}{Conclusion}{36}{0}{8}
