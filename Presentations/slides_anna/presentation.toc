\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Background on gradient flows}{8}{0}{2}
\beamer@sectionintoc {3}{Maximum Mean Discrepancy Gradient Flow}{14}{0}{3}
\beamer@sectionintoc {4}{Convergence properties}{16}{0}{4}
\beamer@sectionintoc {5}{A practical algorithm to descend the MMD flow}{19}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{25}{0}{6}
