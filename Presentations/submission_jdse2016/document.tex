
% Use the following line _only_ if you're still using LaTeX 2.09.
%\documentstyle[icml2016,epsf,natbib]{article}
% If you rely on Latex2e packages, like most moden people use this:
\documentclass{article}

% use Times
\usepackage{times}
% For figures
\usepackage{graphicx} % more modern
%\usepackage{epsfig} % less modern
\usepackage{subfigure} 

% For citations
\usepackage{natbib}

% For algorithms
\usepackage{algorithm}
\usepackage{algorithmic}

% As of 2011, we use the hyperref package to produce hyperlinks in the
% resulting PDF.  If this breaks your system, please commend out the
% following usepackage line and replace \usepackage{icml2016} with
% \usepackage[nohyperref]{icml2016} above.
\usepackage{hyperref}

% Packages hyperref and algorithmic misbehave sometimes.  We can fix
% this with the following command.
\newcommand{\theHalgorithm}{\arabic{algorithm}}

% Employ the following version of the ``usepackage'' statement for
% submitting the draft version of the paper for review.  This will set
% the note in the first column to ``Under review.  Do not distribute.''
%\usepackage{icml2016}

% Employ this version of the ``usepackage'' statement after the paper has
% been accepted, when creating the final version.  This will set the
% note in the first column to ``Proceedings of the...''


\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{dsfont} % for hollow 1 with Type 1 fonts
\usepackage{stmaryrd} 
\usepackage{comment}
\usepackage{mathrsfs}
\usepackage[inline]{enumitem}
\DeclareMathOperator*{\argmin}{argmin}
\DeclareMathOperator*{\Ima}{Im}

% Shortcuts
\newcommand{\fixme}[1]{{\bf [FIXME: #1]}}
\newcommand{\OMIT}[1]{}	% to ignore things
\newcommand{\Sn}{\mathfrak{S}_n}
\newcommand{\FF}{\mathcal{F}}
\newcommand{\dFF}{d_{\FF}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\Sphere}{\mathbb{S}}
\newcommand{\n}{\llbracket n \rrbracket}
\newcommand{\hollowone}{\mathds{1}}
\newcommand{\br}[1]{\left(#1\right)}
\newcommand{\sbr}[1]{\left[#1\right]}
\newcommand{\cbr}[1]{\left\{#1 \right\}}
\newcommand{\abs}[1]{\left\vert #1 \right\vert}
\newcommand{\nm}[1]{\left\Vert #1 \right\Vert}
\newcommand{\innerprod}[1]{\langle #1 \rangle}
\newcommand{\norm}[1]{\Vert #1 \Vert}
\newcommand{\sigstar}{\sigma^{*}}
\newcommand{\sigstarstar}{\sigma^{**}}
\newcommand{\sighat}{\hat{\sigma}}
\newcommand{\Eff}{\operatorname{Eff}}
\newcommand{\sign}{\operatorname{sign}}
\newcommand{\Span}{\operatorname{span}}
\newcommand{\DN}{\mathcal{D}_{N}}
\newcommand{\KN}{\mathcal{K}_{N}}
\newcommand{\CN}{\mathcal{C}_{N}}
\newcommand{\II}{\mathfrak{I}}	% injection opposed to \Pi
\newcommand{\Ctil}{\widetilde{C}}

% Theorems
\theoremstyle{plain}
\newtheorem*{problem*}{The Problem}
\newtheorem{theorem}{Theorem}
\newtheorem{proposition}{Proposition}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}
\theoremstyle{definition}
\newtheorem{definition}{Definition}
\newtheorem{problem}{Problem}
\newtheorem{method}{Method}
\theoremstyle{remark}
\newtheorem{example}{Example}
\newtheorem{remark}{Remark}

\AtBeginDocument{%
	\addtolength\abovedisplayskip{-0.5\baselineskip}%
	\addtolength\belowdisplayskip{-0.5\baselineskip}%
	%  \addtolength\abovedisplayshortskip{-0.5\baselineskip}%
	%  \addtolength\belowdisplayshortskip{-0.5\baselineskip}%
}

\title{Controlling the distance to a Kemeny consensus without computing it}
\author{Yunlong Jiao${}^\star$, Anna Korba${}^\dagger$ and Eric Sibony${}^\dagger$\\
	$\star$ Mines ParisTech -- CBIO $\dagger$ T\'el\'ecom ParisTech, CNRS LTCI}
\date{}


\begin{document}

\maketitle

\noindent This work was presented at ICML 2016 : \url{http://jmlr.org/proceedings/papers/v48/korba16.html}\\

We study the problem of \textbf{ranking aggregation}: agents provide a collection of ranked preferences over a set of alternatives, and we wish to aggregate them into one consensus ranking.
One popular approach in ranking aggregation follows Kemeny's rule. Given a collection of rankings/permutations $\DN := \br{\sigma_1,\dots,\sigma_N} \in \Sn^N$ over $n$ alternatives, \textbf{Kemeny consensus(es)} are the solution to
\begin{equation*}
\KN := \argmin_{\sigma \in \Sn} \sum_{i=1}^{N} d(\sigma,\sigma_t) \,,
\end{equation*}
where $d$ is the Kendall's tau distance between permutations, i.e.,
\begin{equation*}
d(\sigma,\sigma') = \sum_{1\leq i < j \leq n}\mathbb{I}\{(\sigma(j)-\sigma(i))(\sigma'(j)-\sigma'(i))<0\} \,.
\end{equation*}

\noindent We denote by $\KN$ the set of Kemeny consensuses on the dataset $\DN$. Kemeny consensuses satisfy many desirable properties but are NP-hard to compute even for $n=4$. It thus calls for study on apprehending the complexity of Kemeny aggregation and theoretical guarantees of approximation procedures commonly used in practice.\\

In this paper we introduce a practical method to predict, for a ranking and a dataset, how close the Kemeny consensus(es) are to this ranking. \\


\noindent \textbf{The problem:} Let $\DN\in\Sn^N$ be a dataset and $\sigma\in\Sn$ be a permutation, typically output by a computationally efficient aggregation procedure on $\DN$. Can we give a (tractable) upper bound for the distance $d(\sigma,\sigma^{\ast})$ between $\sigma$ and a Kemeny consensus $\sigma^{\ast}\in\KN$?\\

\noindent \textbf{Definition 1 :} The Kemeny embedding is the mapping $\phi : \Sn \rightarrow \RR^{\binom{n}{2}}$ defined by:
\[
\phi:\sigma \mapsto 
\left(\begin{array}{c}
\vdots\\
\sign(\sigma(j)-\sigma(i))\\
\vdots
\end{array}\right)_{1\leq i < j \leq n} \,,
\]
where $\sign(x) = 1$ if $x \geq 0$ and $-1$ otherwise.\\


\noindent \textbf{Definition 2 :} The mean embedding of a dataset $\DN: = \br{\sigma_1,\dots,\sigma_N} \in \Sn^N$ is:
\begin{equation*}
\label{eq:mean-embedding}
\phi\br{\DN} := \frac{1}{N}\sum_{t=1}^{N}\phi\br{\sigma_t}.
\end{equation*}\\


\noindent \textbf{The bound:} Denote by $0 \leq \theta_N(\sigma) \leq \pi$ the angle between the Kemeny embeddings $\phi(\sigma)$ and $\phi(\DN)$ in an Euclidean space. If $\theta_{N}(\sigma) < \frac{\pi}{2},$ then for all $\sigstar \in \KN$,
\begin{equation*}
d(\sigma,\sigma^{\ast}) \leq \left\lfloor \binom{n}{2}\sin^{2}(\theta_{N}(\sigma))\right\rfloor =: k_{min}(\sigma;\DN) \,,
\end{equation*}
where $\lfloor x\rfloor$ denotes the integer part of the real $x$.\\

 In practice, this provides a simple and general method to predict, for any ranking aggregation procedure, how close its output on a dataset is from the Kemeny consensuses. The tightness of this bound was tested empirically, for several datasets and different voting rules. From a broader perspective, it constitutes a novel approach to apprehend the complexity of Kemeny aggregation.
Our results rely on the geometric properties of the Kemeny embedding. Though it has rarely been used in the literature, it provides a powerful framework to analyze Kemeny aggregation. We therefore believe that it could lead to other profound results.

\nocite{*}.

\nocite{*}
\bibliographystyle{unsrt}%Used BibTeX style is unsrt
\bibliography{mybib}

\end{document}
