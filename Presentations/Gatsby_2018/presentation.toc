\beamer@sectionintoc {1}{Introduction to Ranking Data}{3}{0}{1}
\beamer@sectionintoc {2}{Ranking Regression}{22}{0}{2}
\beamer@sectionintoc {3}{Background on Ranking Aggregation/Medians}{34}{0}{3}
\beamer@sectionintoc {4}{Risk Minimization for Ranking (Median) Regression}{48}{0}{4}
\beamer@sectionintoc {5}{Algorithms - Local Median Methods}{57}{0}{5}
\beamer@sectionintoc {6}{Ongoing work - Structured prediction methods}{66}{0}{6}
\beamer@sectionintoc {7}{Conclusion}{72}{0}{7}
