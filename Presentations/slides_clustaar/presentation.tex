\documentclass{beamer}

\usepackage{booktabs}
\usepackage[scale=2]{ccicons}
%\usepackage{minted}

%\usemintedstyle{trac}

%\usetheme{m}
\usetheme{default}

\usepackage{bbm}
\usepackage{float}
\usepackage{tabularx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{tikz}
\usepackage{stmaryrd}
\usetikzlibrary{matrix,arrows}
\usepackage{url}
\usepackage{comment}
\usepackage{multirow}
\usepackage{arydshln}
\usepackage{kbordermatrix}

% Shortcuts
\newcommand\Sym[1]{\mathfrak{S}_{#1}}
\newcommand\Rank[1]{\Gamma(#1)}
\newcommand\Sn{\mathfrak{S}_{n}}
\newcommand\1[1]{\mathds{1}_{#1}}
\newcommand\n{\llbracket n \rrbracket}
\newcommand\set[1]{\llbracket #1 \rrbracket}
\newcommand{\argmin}{\operatornamewithlimits{argmin}}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}
\newcommand{\supp}{\operatorname{supp}}
\newcommand\Subsets[1]{\mathcal{P}(#1)}
\newcommand\SubsetsWE[1]{\mathcal{\bar{P}}(#1)}
\newcommand{\EOD}{\widehat{\mathcal{A}}_{N}}
\newcommand{\Space}[1]{L(#1)}
\newcommand{\formula}[1]{\textcolor{myorange}{#1}}
\newcommand{\precision}[1]{\textcolor{myblue}{#1}}
\newcommand{\attention}[1]{\textcolor{red}{#1}}
\newcommand{\illustration}[1]{\textcolor{mygreen}{#1}}

%\definecolor{myorange}{rgb}{0.7,0.3,0.3}
\definecolor{myorange}{rgb}{0.9,0.5,0.2}
\definecolor{mygrey}{rgb}{0.7,0.7,0.7}
\definecolor{mygreen}{rgb}{0,0.5,0}
\definecolor{myblue}{rgb}{0.2,0.2,0.7}

% Theorems
\theoremstyle{theorem}
\newtheorem{proposition}[theorem]{Proposition}
%\newtheorem{lemma}[theorem]{Lemma}
\theoremstyle{remark}
\newtheorem{remark}[theorem]{Remark}

\title{Statistical analysis of ranking data}
\author{Eric Sibony \and St\'ephan Cl\'emen\c{c}on \and J\'er\'emie Jakubowicz \and Anna Korba}
\institute{LTCI UMR 5141, Telecom ParisTech/CNRS}
\date{Clustaar\\ February 18 2016}

\let\oldcite=\cite                                                              
\renewcommand{\cite}[1]{\textcolor{mygrey}{\oldcite{#1}}}

\AtBeginSection[] {
	\begin{frame}<beamer>
		\frametitle{Outline}
		\tableofcontents[currentsection]
	\end{frame}
}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}


\section{Why ranking data?}


\begin{frame}{Many modern systems gather feedback data about the preferences of their users}
\begin{center}
	\renewcommand\tabcolsep{10pt}
	\renewcommand\arraystretch{2}
	\begin{tabular}{ccc}
	\includegraphics[width=8em]{images/google.jpg} 	& \includegraphics[width=8em]{images/amazon.jpg} 	& \includegraphics[width=8em]{images/facebook.png}\\
	\includegraphics[width=8em]{images/alibaba.png} 	& \includegraphics[width=8em]{images/netflix.png} 	& \includegraphics[width=8em]{images/spotify.png}\\
	\includegraphics[width=8em]{images/booking.png} 	& \includegraphics[width=8em]{images/yelp.jpg} 	&
	\includegraphics[width=8em]{images/criteo.png} 	
	\end{tabular}
\end{center}
\end{frame}


\begin{frame}{Many modern systems gather feedback data about the preferences of their users}
\begin{center}
	\begin{tabular}{cc}
		\includegraphics[height = 4em]{images/avatar.jpg} & \raisebox{1.5em}{3}\\
		\includegraphics[height = 4em]{images/casablanca.jpg} & \raisebox{1.5em}{4}\\
		\includegraphics[height = 4em]{images/gladiator.jpg} & \raisebox{1.5em}{5}\\
		\includegraphics[height = 4em]{images/matrix.jpg} & \raisebox{1.5em}{4}\\
	\end{tabular}
	$\qquad\Rightarrow\qquad$
	\begin{tabular}{c}
		\includegraphics[height = 4em]{images/gladiator.jpg}\\
		\rotatebox{90}{$\prec$}\\
		\includegraphics[height = 4em]{images/casablanca.jpg} $\quad$ \includegraphics[height = 4em]{images/matrix.jpg}\\
		\rotatebox{90}{$\prec$}\\
		\includegraphics[height = 4em]{images/avatar.jpg}\\
	\end{tabular}
\end{center}
\end{frame}


\begin{frame}{Many modern systems gather feedback data about the preferences of their users}
\begin{center}
	\begin{tabular}{c}
		\includegraphics[width = 10em]{images/produits-choisis.png}
	\end{tabular}
	$\qquad\Rightarrow\qquad$
	\begin{tabular}{c}
		\includegraphics[width = 2.5em]{images/choisi-1.png}\includegraphics[width = 2.5em]{images/choisi-2.png}\\
		\rotatebox{90}{$\prec$}\\
		\includegraphics[width = 2.5em]{images/non-choisi-1.png}\includegraphics[width = 2.5em]{images/non-choisi-2.png}\includegraphics[width = 2.5em]{images/non-choisi-3.png}\includegraphics[width = 2.5em]{images/non-choisi-4.png}
	\end{tabular}
\end{center}
\end{frame}


\begin{frame}{Many modern systems gather feedback data about the preferences of their users}
	\textbf{This ranking data only asks to be analyzed.}\\
	\vspace{1cm}
	\begin{center}
		\precision{$\Rightarrow$ Over the past 15 years, the statistical analysis of ranking data has become a subfield of the machine learning literature.}
	\end{center}
\end{frame}


\begin{frame}{However, ranking data also arise in many other applications}
\begin{exampleblock}{Example 1: Elections}
Set of candidates $\{A,B,C,D\}$. A voter can give for instance
\begin{itemize}
	\item her favorite candidate, e.g. \illustration{$B\succ A,C,D$}.
	\item a full ordering of the candidates, e.g. \illustration{$B\succ D\succ A\succ C$}
\end{itemize}
The collection of ballots in an election is a \textbf{dataset of rankings}.\\
$\Rightarrow$ How to elect the winner(s)?
\end{exampleblock}
\begin{center}
\begin{columns}
	\begin{column}{0.3\linewidth}
		\begin{center}
			\textit{Borda-Condorcet debate since the $18^{th}$ century}
		\end{center}
	\end{column}
	\begin{column}{0.7\linewidth}
	\begin{tabular}{cc}
		\small{Jean-Charles de Borda} & \small{Nicolas de Condorcet}\\
		\includegraphics[height = 7em]{images/Borda.jpg} & \includegraphics[height = 7em]{images/Condorcet.png}
	\end{tabular}
	\end{column}
\end{columns}
\end{center}
\end{frame}


\begin{frame}{However, ranking data also arise in many other applications}
	\begin{exampleblock}{Example 2: Competitions}
		\begin{columns}
			\begin{column}{0.7\linewidth}
				Set of participants $\{1, \dots, n\}$.\\ 
				The results of a game can be for instance
				\begin{itemize}
					\item Victory of $i$ against $j$: \illustration{$i\succ j$}\\ (e.g. in football, chess, \dots)
					\item Ranking of the participants of the game: \illustration{$i_{1}\succ\dots\succ i_{k}$}\\ (e.g. races, video games, \dots)
				\end{itemize}
				The results of the games of a competition form a \textbf{dataset of rankings}.
				\begin{center}
					$\Rightarrow$ What is the ranking of the competition?
				\end{center}
			\end{column}
			\begin{column}{0.3\linewidth}
				\begin{center}
					\includegraphics[height=4em]{images/football.jpg}\\
					\includegraphics[height=4.8em]{images/chess.jpg}\\
					\includegraphics[height=4em]{images/horse-racing.jpg}\\
					\includegraphics[height=4em]{images/halo.jpg}
				\end{center}
			\end{column}
		\end{columns}
	\end{exampleblock}
\end{frame}


\begin{frame}{However, ranking data also arise in many other applications}
\begin{exampleblock}{Example 3: Surveys}
Set of items $\{1,\dots, n\}$.\\ 
A respondent can be asked to give for instance
\begin{itemize}
	\item Her top-$3$ items: \illustration{$i_{1}, i_{2}, i_{3} \succ \textit{the rest}$}
	\item Her top-$3$ ranking of items: \illustration{$i_{1}\succ i_{2}\succ i_{3}\succ \textit{the rest}$}
\end{itemize}
The answers to a survey constitute a \textbf{dataset of rankings}.\\
\vspace{0.2cm}
$\Rightarrow$ How to summarize the results?\\ 
$\Rightarrow$ How to segment respondents based on their answers?
\end{exampleblock}
\begin{center}
	\includegraphics[height=4em]{images/university-ranking.png}
	$\qquad$
	\includegraphics[height=5em]{images/ranking-the-brands.jpg}
\end{center}
\end{frame}


\section{The analysis of ranking data}


\begin{frame}{Definitions}
Set of items \formula{$\n := \{1,\dots,n\}$}
\vfill
\begin{definition}[Ranking]
A ranking is a strict partial order $\prec$ over $\n$, \textit{i.e.} a binary relation satisfying the following properties:
\begin{description}
	\item[Irreflexivity] For all $a\in\n$, $a\not\prec a$
	\item[Transitivity] \textbf{For all $\mathbf{a,b,c\in\n}$, if $\mathbf{a\prec b}$ and $\mathbf{b\prec c}$ then $\mathbf{a\prec c}$}
	\item[Asymmetry] For all $a,b\in\n$, if $a\prec b$ then $b\not\prec a$
\end{description}
\end{definition}
Shortcut notations:
\begin{itemize}
	\item $a\succ b\succ c$ instead of $(a\succ b, a\succ c, b\succ c)$
	\item $a\succ b,c$ instead of $(a\succ b, a\succ c)$
\end{itemize}
\end{frame}


\begin{frame}{Main types of rankings}
\begin{itemize}
		\item {\bf Full ranking.} All the items are ranked, without ties
		\[
		\formula{a_{1}\succ\dots\succ a_{n}}
		\]
		\item {\bf Partial ranking.} All the items are ranked, with ties
		\[
		\formula{a_{1,1},\dots,a_{1,n_{1}}\succ\dots\succ a_{r,1},\dots,a_{r,n_{r}} \quad\text{with}\quad \sum_{i=1}^{r}n_{i}=n}
		\]
		\item {\bf Incomplete ranking.} Only a subset of items are ranked, without ties 
		\[
		\formula{a_{1}\succ\dots\succ a_{k} \quad\text{with}\quad k < n}
		\]
\end{itemize}
\end{frame}


\begin{frame}{General setting}
Perform some task on a dataset of $N$ rankings \formula{$\mathcal{D}_{N} = (\prec_{1},\dots,\prec_{N})$}.
\begin{exampleblock}{Examples}
\begin{itemize}
	\item \textbf{Top-1 recovery:} Find the ``most preferred'' item in $\mathcal{D}_{N}$\\
	\illustration{e.g. Output of an election}
	\item \textbf{Aggregation:} Find a full ranking that ``best summarizes'' $\mathcal{D}_{N}$\\
	\illustration{e.g. Ranking of a competition}
	\item \textbf{Clustering:} Split $\mathcal{D}_{N}$ into clusters\\
	\illustration{e.g. Segment customers based on their answers to a survey}
	\item \textbf{Prediction:} Predict the outcome of a missing pairwise comparison in a ranking $\prec$\\
	\illustration{e.g. In a recommendation setting}
\end{itemize}
\end{exampleblock}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	\textbf{Notation.} 
	\begin{itemize}
		\item The full ranking \formula{$a_{1}\succ \dots \succ a_{n}$} is denoted by \formula{$a_{1} \dots a_{n}$}
		\item Also seen as the permutation \formula{$\sigma$} that maps an item to its rank:
		\[
		\formula{a_{1}\succ \dots \succ a_{n}} \quad\Leftrightarrow\quad \formula{\sigma\in\Sn} \text{ such that } \formula{\sigma(a_{i}) = i}
		\]
		\formula{$\Sn$}: set of permutations of $\n$, the symmetric group.
	\end{itemize}
	\vspace{1cm}
	\textbf{Probabilistic Modeling.} The dataset is a collection of random permutations drawn IID from a probability distribution $p$ over $\Sn$: 
	\[
	\formula{\mathcal{D}_{N} = (\Sigma^{(1)},\dots,\Sigma^{(N)}) \qquad\text{with}\qquad \Sigma^{(i)}\sim p}
	\]
	\textit{$p$ is called a ranking model.}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
\begin{exampleblock}{Example, dataset from \cite{Croon1989}}
After the fall of the Berlin wall a survey of German citizens was conducted where they were asked to rank four political goals
\begin{enumerate}
	\item Maintain order
	\item Give people more say in government
	\item Fight rising prices
	\item Protect freedom of speech
\end{enumerate}
\end{exampleblock}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	\begin{exampleblock}{Example, dataset from \cite{Croon1989}}
		They collected 2,262 answers
		\begin{center}
			\begin{tabular}{crccr}
				Ranking & Answers & \vphantom{aaaaaaaaaa} & Ranking & Answers\\
				\cline{1-2}
				\cline{4-5}
				1234 & \precision{137} 	& & 3124 & \precision{330}\\
				1243 & \precision{29} 	& & 3142 & \precision{294}\\
				1324 & \precision{309} 	& & 3214 & \precision{117}\\
				1342 & \precision{255} 	& & 3241 & \precision{69}\\
				1423 & \precision{52} 	& & 3412 & \precision{70}\\
				1432 & \precision{93} 	& & 3421 & \precision{34}\\
				2134 & \precision{48} 	& & 4123 & \precision{21}\\
				2143 & \precision{23} 	& & 4132 & \precision{30}\\
				2314 & \precision{61} 	& & 4213 & \precision{29}\\
				2341 & \precision{55} 	& & 4231 & \precision{52}\\
				2413 & \precision{33} 	& & 4312 & \precision{35}\\
				2431 & \precision{39} 	& & 4321 & \precision{27}\\				
			\end{tabular}
		\end{center}
	\end{exampleblock}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	\begin{block}{Questions}
		\begin{itemize}
			\item How to analyze a dataset of permutations $\formula{\mathcal{D}_{N} = (\Sigma^{(1)},\dots,\Sigma^{(N)})}$?
			\item How to characterize the variability?
			\item What can be inferred?
		\end{itemize}
	\end{block}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	Notation:
	\[
	\sigma =
	\left(
	\begin{array}{cccc}
	1 & 2 & \dots & n\\
	\sigma(1) & \sigma(2) & \dots & \sigma(n)
	\end{array}\right)
	=
	(\sigma(1),\sigma(2),\dots,\sigma(n))
	\]
	A random permutation $\precision{\Sigma}$ can be seen as a random vector
	\[
	\precision{(\Sigma(1), \dots, \Sigma(n))\in\mathbb{R}^{n}}
	\]
	\vspace{-0.5cm}
	\begin{alertblock}{But}
		\begin{itemize}
			\item The random variables $\Sigma(1),\dots,\Sigma(n)$ are \attention{highly dependent}
			\item The sum $\Sigma + \Sigma'$ is not a random permutation\\
			\attention{$\Rightarrow$ No law of large numbers nor central limit theorem on $\Sn$}
			\item \attention{No natural notion of variance for $\Sigma$}
		\end{itemize}
	\end{alertblock}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	The set of permutations $\Sn$ is finite, compute the histogram:
	\begin{center}
		\includegraphics[scale=0.6]{images/p-German.pdf}		
	\end{center}
	\begin{alertblock}{But}
		\begin{itemize}
			\item Exploding cardinality: \formula{$\vert\Sn\vert = n!$}. \textit{E.g.} $20! = 2.4\times 10^{18}$\\
			\attention{$\Rightarrow$ Few statistical relevance}
		\end{itemize}
	\end{alertblock}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	Apply a method from p.d.f. estimation (e.g. kernel density estimation):
	\begin{center}
		\includegraphics[scale=0.5]{images/1_German-approximation.pdf}		
	\end{center}
	\begin{alertblock}{But}
		\begin{itemize}
			\item No canonical ordering of the rankings
		\end{itemize}
	\end{alertblock}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	\attention{No canonical ordering of the rankings}\\
	Lexicographical ordering\\
	\includegraphics[scale=0.5]{images/2_German-approximation.pdf}\\
	Random ordering\\
	\includegraphics[scale=0.5]{images/3_German-approximation.pdf}
\end{frame}

\begin{frame}{Detailed example: analysis of full rankings}
	\begin{alertblock}{More generally: many possible distances on $\Sn$}
		\begin{itemize}
			\item Kendall's tau distance
			\[
			d(\sigma,\tau) = \sum_{1\leq i < j \leq n}\mathbb{I}\{\sigma \text{ and } \tau \text{ disagree on } \{i,j\} \}
			\]
			\item Footrule distance ($l^{1}$ norm)
			\[
			d(\sigma,\tau) = \sum_{i=1}^{n}\vert\sigma(i)-\tau(i)\vert
			\]
			\item Many others: Spearman, Hamming...\dots
		\end{itemize}
	\end{alertblock}
\end{frame}



\begin{frame}{Detailed example: analysis of full rankings}
	\begin{alertblock}{More generally: many possible graph structures on $\Sn$}
		\begin{itemize}
			\item Adjacent transpositions
			\[
			\sigma \text{ and } \pi \text{ are neighbors if } \pi = (i\ i+1)\sigma \text{ with } 1\leq i\leq n-1
			\]
			\item All transpositions
			\[
			\sigma \text{ and } \pi \text{ are neighbors if } \pi = (i\ j)\sigma \text{ with } 1\leq i\neq j\leq n		
			\]
			\item Star graph
			\[
			\sigma \text{ and } \pi \text{ are neighbors if } \pi = (1\ k)\sigma \text{ with } 2\leq k\leq n		
			\]
			\item ...
		\end{itemize}
	\end{alertblock}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	\begin{alertblock}{More generally: many possible embeddings of $\Sn$}
		\begin{itemize}
			\item Permutation matrices
			\[
			\Sn\rightarrow\mathbb{R}^{n\times n},\quad\sigma\mapsto P_{\sigma}\quad \text{with } P_{\sigma}(i,j) = \mathbb{I}\{\sigma(i) = j\}
			\]
			\item embedding in a sphere
			\item embedding as angles
			\item \dots
		\end{itemize}
	\end{alertblock}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	Exploit any of the combinatorial or algebraic properties of $\Sn$\\
	\vspace{1cm}
	\begin{alertblock}{But}
		\begin{itemize}
			\item Ranking data are very natural for human beings\\
			\attention{$\Rightarrow$ Statistical modeling should capture some interpretable structure}
		\end{itemize}
	\end{alertblock}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	\textbf{``Parametric'' approach}
	\begin{itemize}
		\item Fit a predefined generative model on the data
		\item Analyze the data through that model
		\item Infer knowledge with respect to that model
	\end{itemize}
	\vspace{1cm}
	\textbf{``Nonparametric'' approach}
	\begin{itemize}
		\item Choose a structure on $\Sn$
		\item Analyze the data with respect to that structure
		\item Infer knowledge through a ``regularity'' assumption
	\end{itemize}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	\begin{exampleblock}{Parametric approach - classic models}
		\begin{itemize}
			\item Mallows model \cite{Mallows57}
			\[
			p(\sigma) = Ce^{-\gamma d(\sigma_{0},\sigma)} \qquad\text{with }\sigma\in\Sn\text{ and }\gamma\in\mathbb{R}^{+}
			\]
			\item Plackett-Luce model \cite{Luce59}, \cite{Plackett75}
			\[
			p(\sigma) = \prod_{i=1}^{n}\frac{w_{\sigma_{i}}}{\sum_{j=i}^{n}w_{\sigma_{j}}} \qquad\text{with }w_{i}\in\mathbb{R}^{+}
			\]
			\item Thurstone model \cite{Thurstone27}
			\[
			p(\sigma) = \int_{x_{1} > \dots > x_{n}}\prod_{i=1}^{n}f_{i}(x_{i})dx_{i} \qquad\text{with }f_{i}\text{ p.d.f. on }\mathbb{R}
			\]
		\end{itemize}
	\end{exampleblock}
\end{frame}


\begin{frame}{Detailed example: analysis of full rankings}
	\begin{exampleblock}{Examples of nonparametric approaches}
		\begin{itemize}
			\item Distance-based modeling
			\item Independence modeling
			\item Embedding in euclidean space
			\item Pairwise decomposition
			\item Sparsity assumption
			\item Sampling-based models
			\item Algebraic toric models
			\item \textbf{Harmonic analysis}
		\end{itemize}
	\end{exampleblock}
\end{frame}

\section{Ranking aggregation}

\subsection{Problem}

\begin{frame}{Problem}
	\begin{block}{Ranking aggregation}
		Given orders $\sigma_1, \dots \sigma_N$ on a fixed list of items $\llbracket n \rrbracket$, find a global order  $\sigma^{*}$ on this list, the most coherent with the collection observed.
	\end{block}
	\vspace{0.5cm}
	\begin{center}
		\begin{description}[font=\color{blue}\sffamily \em] \item[$\sigma_1$ :] 1234 (1 $\succ$ 2 $\succ$ 3 $\succ$ 4)
			\item[$\sigma_2$ :] 2413 (2 $\succ$  4 $\succ$ 1 $\succ$ 3)\\
			...\\
			\item[$\sigma_N$ :]  3412 (3 $\succ$ 4 $\succ$ 1 $\succ$ 2)\\
			\item[\alert{$\Rightarrow$ :}]\alert{$\sigma^{*}$??}
		\end{description}
	\end{center}
\end{frame}

\begin{frame}{Desirable properties for a consensus}
	\begin{itemize}%[font=\color{blue}]
		\item \textcolor{gray}{Independance to irrelevant alternatives}: Relative order of $i$ and $j$ in $\sigma^{*}$ should depend only on relative order of $i$ and $j$ in $\sigma_1, \dots \sigma_N$.\\
		Ex: if $\sigma_1 = 123$ changes to $132$, relative order of $1,2$ in should not change in $\sigma^{*}$.
		\item \textcolor{gray}{Neutrality }: No item should be favored to others.
		If two items switch positions in $\sigma_1, \dots \sigma_N$, they should switch positions also in $\sigma^{*}$.
		%\item Anonimity: No item should be favored to others.
		%If two items switch their labels, $\sigma^{*}$ should remain the same.
		\item \textcolor{gray}{Monotonicity}: If the ranking of an item is improved by a voter, its ranking in $\sigma^{*}$ can only improve.
	\end{itemize}
\end{frame}

\begin{frame}{Desirable properties for a consensus}
	\begin{itemize}
		\item \textcolor{gray}{ Consistency}: If voters are split into two disjoint sets, and both the aggregation of voters in the first and second set prefer $i$ to $j$, then also $\sigma^{*}$  should prefer $i$ to $j$.
		\item \textcolor{gray}{Non-dictatorship}: There is no single voter $k$ with the individual preference order $\sigma_{k}$ such that $\sigma_{k}$ =$\sigma^{*}$, unless all votes are $\sigma_{k}$. %Results of voting cannot simply mirror that of any single person's preferences without consideration of the other voters.
		\item \textcolor{gray}{Unanimity (or Pareto efficiency)}: If all voters prefer item $i$ to  item $j$, then also $\sigma^{*}$ should prefer $i$ to $j$.
		\item \textcolor{gray}{Condorcet criterion}: any item  which wins every other in pairwise simple majority voting should be ranked first.
		%extended condorcet criterion
	\end{itemize}
\end{frame}

\begin{frame}{No "good" voting rule}
	\begin{itemize}%[itemsep=10pt]
		\item \textcolor{red}{Arrow's impossibility theorem (1963)}: No voting system can satisfy simultaneously \textcolor{gray}{unanimity, non-dictatorship} and \textcolor{gray}{independance to irrelevant alternatives}.
		\item Many different properties/axiomes that we can desire for a voting rule, reflecting some aspects of a "just" vote. 
	\end{itemize}
\end{frame}



\subsection{Unsupervised methods}

\begin{frame}{Positional based methods - Example of Borda}
	\begin{itemize}
		\item Compute score for each item $i$ based on the $N$ rankings: $s_N(i)=\sum_{t=1}^{N} (n+1-\sigma_t(i))$ à chaque objet $i$,
		\item Items are sorted in decreasing order of the scores.
	\end{itemize}
	\begin{figure}
		\centering
		\includegraphics[width=0.6\textwidth]{images/Borda_agg.png}
	\end{figure}
\end{frame}




\begin{frame}{Kemeny optimal aggregation}
	\begin{itemize}
		\item Collection of rankings ($\sigma_1, \dots, \sigma_N)$ ,$d$ Kendall tau distance
		\item Find $\sigma^{*} \in \mathfrak{S}_n$ solution of :
		\begin{equation*}
		\displaystyle \min_{\sigma \in \mathfrak{S}_n} \sum_{t=1}^{N} d(\sigma,\sigma_t)
		\end{equation*} 
		\item Satisfies \textcolor{gray}{neutrality}, \textcolor{gray}{consistency},\textcolor{gray}{condorcet criterion} but \textcolor{red}{computationally hard}
	\end{itemize}
	\begin{figure}
		\centering
		\includegraphics[width=0.6\textwidth]{images/Kemeny_agg.png}
	\end{figure}
\end{frame}

\begin{frame}{Kemeny aggregation}
	Satisfies \textcolor{gray}{neutrality} and \textcolor{gray}{consistency} but \textcolor{red}{computationally hard}
	\begin{itemize}
	
		
		\item Borda and Footrule (polynomial time) aggregation are good approximators.
		\item Locally Kemeny optimal aggregation (relaxation of Kemeny optimality, $N \mathrm{O}(nlogn)$)
		
	\end{itemize}
	
\end{frame}

\begin{frame}{Markov chain methods}
 \begin{itemize}
 	\item States=candidates
	\item Transitions depend on the preference orders given by voters
	\item Basic idea: probabilistically switch to a “better candidate”
	\item Rank candidates based on stationary probabilities!
\end{itemize}
\end{frame}

\begin{frame}{Markov chain methods}
	\begin{itemize}
		\item Can handle partial lists, and is computationnaly efficient (linear time).
		\item Four ways to compute the transition matrix : MC1, MC2, MC3, \textcolor{red}{MC4}.
	\end{itemize}
\end{frame}

\begin{frame}{Preference graph}
	\begin{itemize}
		\item Construct graphs for rankings as:
		\begin{figure}
			\centering
			\includegraphics[width=0.6\textwidth]{images/preference_graph.png}
		\end{figure}
		\item Aggregate graphs 
		\item Induce linear order: \textit{minimum feedback arc set problem }, high complexity\\
		\textcolor{gray}{Example}: Unweighted aggregation of the graphs and sort nodes by their weighted indegree (\textcolor{myblue}{Copeland method}))
		\end{itemize}
\end{frame}



\subsection{Learning to rank (supervised)}

\begin{frame}{Learning to rank}
	\begin{itemize}
		\item Input: scores given to different query-document pairs, eventually other features
		\item Learn ranking function
		\item Different methods have been proposed: boosting trees, feature-weighted linear aggregation..
	\end{itemize}
\end{frame}



\begin{frame}{Experiments}
	\begin{itemize}
		\item \textcolor{myblue}{Dataset} : Yahoo Learning to Rank Challenge (LTRC), Microsoft Learning to Rank (MSLR-WEB10K) and LETOR 4.0 rank aggregation (MQ2007-agg, MQ2008-agg) 
		\item \textcolor{myblue}{Evaluation}: Average Kendall tau distance, Cumulative Gain (supervised):
		\begin{equation*}{\text{Discounted CG at position } p \ :\ }
		 \mathrm{DCG_{p}} = \sum_{i=1}^{p} \frac{ 2^{rel_{i}} - 1 }{ \log_{2}(i+1)} 
		\end{equation*}
		\textcolor{gray}{\textit{Penalizes if highly relevant documents appear lower in a search result list}}
		\begin{equation*}{\text{Normalized DCG at position } p \ :\ }
		\mathrm{nDCG_{p}} = \frac{DCG_{p}}{IDCG_{p}}
		\end{equation*}
		\textcolor{gray}{\textit{Normalized with respect to the length of $\ne$ search result lists}}
	\end{itemize}
\end{frame}
%task has as input scores given to differ- ent query–document pairs by different ranking features (Chapelle & Chang, 2011; Li, 2011). Given a collection of such scores and also the relevance labels for different query–document pairs, rank- ing functions are learned. Given a new query, feature scores for documents are first determined. Then the learned ranking func- tions are applied on these feature scores to determine the final ranking. Different methods have been proposed for the task.
\section{Conclusion}



\begin{frame}{Conclusion}
Ranking data is fun!\\
\pause
\vspace{1cm}
Its analysis presents great and interesting challenges:
\begin{itemize}
	\item Most of the maths from euclidean spaces cannot be applied
	\item But many intuitions/interpretations possible
\end{itemize}
\end{frame}


\begin{frame}{Conclusion}
	\begin{block}{What I did not talk about}
		\begin{itemize}
			\item Difficulties induced by incomplete and partial rankings
			\item Many other connections (social choice theory, shuffling...)
		\end{itemize}
	\end{block}
	\begin{block}{What we are working on (future directions)}
		\begin{itemize}
			\item Harmonic analysis on incomplete rankings
			\item How to extend to incomplete rankings with ties?
			\item How to predict if two voting rules will give the same result on a dataset? (submitted to ICML)
		\end{itemize}
	\end{block}
\end{frame}


\begin{frame}
\begin{center}
Thank you
\end{center}
\end{frame}



\bibliographystyle{apalike}
\bibliography{biblio}


\end{document}
