\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Why ranking data?}{2}{0}{1}
\beamer@sectionintoc {2}{The analysis of ranking data}{10}{0}{2}
\beamer@sectionintoc {3}{Ranking aggregation}{29}{0}{3}
\beamer@subsectionintoc {3}{1}{Problem}{30}{0}{3}
\beamer@subsectionintoc {3}{2}{Unsupervised methods}{34}{0}{3}
\beamer@subsectionintoc {3}{3}{Learning to rank (supervised)}{40}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{42}{0}{4}
