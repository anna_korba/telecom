\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{7}{section.1}
\contentsline {section}{\numberline {2}Preliminary background on rankings}{8}{section.2}
\contentsline {subsection}{\numberline {2.1}Rankings - Definition}{8}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Distances on rankings}{8}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Probabilistic modeling on rankings}{9}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Mallows model}{10}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Thurstone model}{10}{subsubsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.3}Plackett-Luce model}{10}{subsubsection.2.3.3}
\contentsline {subsubsection}{\numberline {2.3.4}Non-parametric models}{11}{subsubsection.2.3.4}
\contentsline {subsection}{\numberline {2.4}Harmonic analysis on $L(\mathfrak {S}_n)$}{11}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Partitions of $n$, Young and Specht modules}{11}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}Young's rule}{12}{subsubsection.2.4.2}
\contentsline {section}{\numberline {3}MRA Framework}{13}{section.3}
\contentsline {subsection}{\numberline {3.1}Notations}{13}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}The ranking assumption}{14}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Statistical setting for the observation of incomplete rankings}{15}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}General MRA-based approach}{15}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Background on multiresolution analysis of incomplete rankings}{15}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}Construction of the MRA representation}{16}{subsubsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.3}Numerical illustrations}{18}{subsubsection.3.4.3}
\contentsline {section}{\numberline {4}Ranking aggregation}{24}{section.4}
\contentsline {subsection}{\numberline {4.1}Ranking aggregation Methods}{24}{subsection.4.1}
\contentsline {subsubsection}{\numberline {4.1.1}Kemeny's rule}{24}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {4.1.2}Spearman footrule aggregation}{25}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.3}Borda count}{25}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.4}Comments}{25}{subsubsection.4.1.4}
\contentsline {subsection}{\numberline {4.2}Consensus reformulation}{26}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}A tentative of algorithm}{27}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}A hierarchical classification method}{27}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Results}{28}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Noise influence}{30}{subsubsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.4}Current limitations}{31}{subsubsection.4.3.4}
\contentsline {section}{\numberline {5}More reflexions on consensus}{31}{section.5}
\contentsline {subsection}{\numberline {5.1}Decomposition of $L(\mathfrak {S}_n)$ and information levels}{31}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Borda count}{32}{subsection.5.2}
\contentsline {section}{\numberline {6}Conclusion}{33}{section.6}
\contentsline {section}{\numberline {7}Appendix}{36}{section.7}
